<?php

use Illuminate\Database\Seeder;

class MskTeacherStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new \App\Models\MskTeacherStatus();
        $new->name = "İbtidai";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\MskTeacherStatus();
        $new->name = "Laborant";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\MskTeacherStatus();
        $new->name = "Asistent";
        $new->tenant_id = 1;
        $new->save();
    }
}
