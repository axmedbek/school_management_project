<?php

use Illuminate\Database\Seeder;

class StudyLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new \App\Models\MskStudyLevels();
        $level->name = "Ela";
        $level->tenant_id = 1;
        $level->save();
    }
}
