<?php

use Illuminate\Database\Seeder;

class CorpusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Corpus::class, 2)->create();
    }
}
