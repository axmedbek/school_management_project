<?php

use Illuminate\Database\Seeder;

class ClassTimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            foreach (\App\Models\Corpus::all() as $corpus)
                for ($i=1; $i < 10; $i++)
                {
                    $newClassLetter = new \App\Models\ClassTime();
                    $newClassLetter->start_time = "0$i:00";
                    $newClassLetter->end_time = "0$i:45";
                    $newClassLetter->corpus_id = $corpus->id;
                    $newClassLetter->tenant_id = 1;
                    $newClassLetter->save();
                }
    }
}
