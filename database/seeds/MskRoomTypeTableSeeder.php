<?php

use Illuminate\Database\Seeder;

class MskRoomTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msk_room_types')->truncate();

        $new = new \App\Models\MskRoomType();
        $new->name = "Dərs otağı";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\MskRoomType();
        $new->name = "Hovuz";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\MskRoomType();
        $new->name = "Laboratoriya";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\MskRoomType();
        $new->name = "İdman zalı";
        $new->tenant_id = 1;
        $new->save();
    }
}
