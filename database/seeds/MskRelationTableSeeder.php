<?php

use Illuminate\Database\Seeder;

class MskRelationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new \App\Models\MskRelation();
        $new->tenant_id = 1;
        $new->name = "Ana";
        $new->save();

        $new = new \App\Models\MskRelation();
        $new->tenant_id = 1;
        $new->name = "Ata";
        $new->save();

        $new = new \App\Models\MskRelation();
        $new->tenant_id = 1;
        $new->name = "Xala";
        $new->save();


    }
}
