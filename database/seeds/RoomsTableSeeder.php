<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Room::class, 50)->create()
        ->each(function ($u){
            $u->subjects()->attach(\App\Models\Subject::all()->random()->id);
        });
    }
}
