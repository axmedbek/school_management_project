<?php

use Illuminate\Database\Seeder;

class UserTableSeeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(\App\User::class)->make();
        $user->name = "Admin";
        $user->username = "admin";
        $user->user_type = "user";
        $user->group_id = 1;
        $user->password = bcrypt("123456");
        $user->save();

        factory(\App\User::class, 50)->create()->each(function($u) {
            $personStudy = factory(\App\Models\PersonStudy::class)->make();
            //$personLanguage = factory(\App\Models\PersonLanguage::class)->make();
            $personLaborActivity = factory(\App\Models\PersonLaborActivity::class)->make();

            $u->person_studies()->save($personStudy);
            //$u->person_languages()->save($personLanguage);
            $u->person_labot_activities()->save($personLaborActivity);
        });
    }
}
