<?php

use Illuminate\Database\Seeder;

class YearTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year = new \App\Models\Year();
        $year->tenant_id = 1;
        $year->start_date = date("Y-09-15");
        $year->end_date = date('Y-06-15', strtotime('+1 years'));
        $year->save();

        $period = new \App\Models\YearPeriod();
        $period->tenant_id = 1;
        $period->start_date = date("Y-09-15");
        $period->end_date = date('Y-01-15', strtotime('+1 years'));
        $period->name = "Period 1";
        $year->periods()->save($period);

        $period = new \App\Models\YearPeriod();
        $period->tenant_id = 1;
        $period->start_date = date('Y-01-16', strtotime('+1 years'));
        $period->end_date = date('Y-06-15', strtotime('+1 years'));
        $period->name = "Period 2";
        $year->periods()->save($period);

        $year->save();

        (new \App\Library\YearDays($year))->calculateDays();
    }
}
