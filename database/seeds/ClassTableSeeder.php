<?php

use Illuminate\Database\Seeder;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = \App\Models\Subject::all()->pluck('id')->toArray();

        for ($i=1;$i<=11;$i++){
            $class = new \App\Models\MskClass();
            $class->name = $i;
            $class->order = $i;
            $class->tenant_id = 1;
            $class->save();
            //subjects
            shuffle($subjects);
            $class->subjects()->attach(array_slice($subjects,0, 4));
        }
    }
}
