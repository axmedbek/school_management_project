<?php

use Illuminate\Database\Seeder;

class SeasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new \Faker\Generator();
        $groupTypes = \App\Models\GroupType::all()->pluck('id')->toArray();

        foreach (\App\Models\MskClass::all() as $class)
            foreach ($class->subjects as $subject){
                $newSeason = new \App\Models\Season();
                $newSeason->tenant_id = 1;
                $newSeason->msk_class_id = $class->id;
                $newSeason->subject_id = $subject->id;
                $newSeason->name = "Season {$class->id}{$subject->id}";
                $newSeason->save();

                $newSeason->group_types()->attach( array_slice($groupTypes, 0, random_int(1, 3)) );
            }
    }
}
