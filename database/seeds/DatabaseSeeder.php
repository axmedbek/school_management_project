<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MarkTypeTableSeeder::class);
        $this->call(MskRelationTableSeeder::class);
        $this->call(MskTeacherStatusTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(UserTableSeeader::class);
        $this->call(CorpusTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(ClassTableSeeder::class);
        $this->call(GroupTypeTableSeeder::class);
        $this->call(SeasonTableSeeder::class);
        $this->call(YearTableSeeder::class);
        $this->call(ClassLetterSeeder::class);
        $this->call(ClassTimeTableSeeder::class);
        $this->call(SmsTemplatesSeeder::class);
        $this->call(MskHeyetlerTableSeeder::class);
        $this->call(MskRoomTypeTableSeeder::class);
        $this->call(EquipmentTypeTableSeeder::class);
        $this->call(StudyLevelSeeder::class);
    }
}
