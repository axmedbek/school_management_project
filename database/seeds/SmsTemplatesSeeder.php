<?php

use Illuminate\Database\Seeder;

class SmsTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $smsTemplate = new \App\Models\SmsTemplates();
        $smsTemplate->name = "məktəbə daxil olmaqla bağlı";
        $smsTemplate->text = "";
        $smsTemplate->type="entered_school";
        $smsTemplate->tenant_id = 1;
        $smsTemplate->save();

        $smsTemplate = new \App\Models\SmsTemplates();
        $smsTemplate->name = "məktəbi tərk etməklə bağlı";
        $smsTemplate->text = "";
        $smsTemplate->type="left_school";
        $smsTemplate->tenant_id = 1;
        $smsTemplate->save();

        $smsTemplate = new \App\Models\SmsTemplates();
        $smsTemplate->name = "qiymət almaqla bağlı";
        $smsTemplate->text = "";
        $smsTemplate->type="evaluated";
        $smsTemplate->tenant_id = 1;
        $smsTemplate->save();
    }
}
