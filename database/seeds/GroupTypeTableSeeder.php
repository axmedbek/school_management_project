<?php

use Illuminate\Database\Seeder;

class GroupTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new \App\Models\GroupType();
        $new->name = "Standart";
        $new->tenant_id = 1;
        $new->default = 1;
        $new->save();

        $new = new \App\Models\GroupType();
        $new->name = "Humanitar";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\GroupType();
        $new->name = "Texniki";
        $new->tenant_id = 1;
        $new->save();
    }
}
