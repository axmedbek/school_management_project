<?php

use App\Models\EquipmentTypes;
use Illuminate\Database\Seeder;

class EquipmentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EquipmentTypes::truncate();


        $new = new EquipmentTypes();
        $new->tenant_id = 1;
        $new->name = "FP";
        $new->save();

        $new = new EquipmentTypes();
        $new->tenant_id = 1;
        $new->name = "Kamera";
        $new->save();

        $new = new EquipmentTypes();
        $new->tenant_id = 1;
        $new->name = "Telefon";
        $new->save();

        $new = new EquipmentTypes();
        $new->tenant_id = 1;
        $new->name = "Printer";
        $new->save();


        \App\Models\EqpInnerType::truncate();


        $new = new \App\Models\EqpInnerType();
        $new->tenant_id = 1;
        $new->name = "In";
        $new->save();

        $new = new \App\Models\EqpInnerType();
        $new->tenant_id = 1;
        $new->name = "Out";
        $new->save();
    }
}
