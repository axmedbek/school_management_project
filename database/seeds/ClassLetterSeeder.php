<?php

use Illuminate\Database\Seeder;

class ClassLetterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupTypes = \App\Models\GroupType::all();

        foreach (\App\Models\Year::all() as $year)
            foreach (\App\Models\Corpus::all() as $corpus)
                foreach (\App\Models\MskClass::all() as $class)
                {
                    $newClassLetter = new \App\Models\ClassLetter();
                    $newClassLetter->name = "A";
                    $newClassLetter->year_id = $year->id;
                    $newClassLetter->corpus_id = $corpus->id;
                    $newClassLetter->msk_class_id = $class->id;
                    $newClassLetter->tenant_id = 1;
                    $newClassLetter->save();

                    foreach ($groupTypes as $groupType)
                    {
                        $newLetterGroup = new \App\Models\LetterGroup();
                        $newLetterGroup->name = $groupType->name;
                        $newLetterGroup->tenant_id = 1;
                        $newLetterGroup->group_type_id = $groupType->id;

                        $newClassLetter->groups()->save($newLetterGroup);
                    }
                }
    }
}
