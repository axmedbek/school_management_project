<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MskHeyetlerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('msk_heyetlers')->truncate();

        $new = new \App\Models\MskHeyetler();
        $new->name = "Texniki";
        $new->start_time = "09:00";
        $new->end_time = "18:00";
        $new->tenant_id = 1;
        $new->save();

        $new = new \App\Models\MskHeyetler();
        $new->name = "İnzibati";
        $new->start_time = "13:00";
        $new->end_time = "20:00";
        $new->tenant_id = 1;
        $new->save();


    }
}
