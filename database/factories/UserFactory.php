<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    $timestamp = mt_rand(1, time());
    $randomDate = date("Y-m-d", $timestamp);

    $type = array_rand(\App\Library\Standarts::$userTypes, 1);

    return [
        'name' => str_limit($faker->name, 30, ""),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$K9Ik8MRWSMSkCxUl4OP9n.1I2uUyS/ERmFSi/4OuVSoW.0QzNfPPy', //123456
        "username" => str_limit($faker->userName, 20, ""),
        "tenant_id" => 1,
        "user_type" => $type,
        "birthday" => $randomDate,
        "birth_place" => $faker->city,
        "mobil_tel" => str_limit($faker->phoneNumber, 20, ""),
        "home_tel" => str_limit($faker->phoneNumber, 20, ""),
        "enroll_date" => date("Y-m-01"),
        "gender" => "m",
        "internship" => $type == "teacher" ? random_int(1,10) : null,
        "msk_teacher_status_id" => $type == "teacher" ? \App\Models\MskTeacherStatus::all()->random()->id : null,
        "group_id" => $type == "user" ? 1 : null,
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Models\Corpus::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'tenant_id' => 1,
    ];
});

$factory->define(\App\Models\Room::class, function (Faker $faker) {
    return [
        'name' => "Room" . random_int(1,100),
        "info" => $faker->text(20),
        "type" => random_int(1,4),
        "corpus_id" => \App\Models\Corpus::all()->random()->id,
        "capacity" => random_int(1, 50),
        'tenant_id' => 1,
    ];
});

$factory->define(\App\Models\Subject::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
        'tenant_id' => 1,
    ];
});

$factory->define(App\Models\Group::class, function (Faker $faker) {

    $privArr = [];
    foreach (\App\Library\Standarts::$modules as $module)
    {
        if( isset($module['child']) )
        {
            foreach ($module['child'] as $m)
            {
                $privArr[$m['route']] = 3;
            }
        }
        else
        {
            $privArr[$module['route']] = 3;
        }
    }

    return [
        'group_name' => "Admin",
        'available_modules' => json_encode($privArr),
        'tenant_id' => 1,
    ];
});

$factory->define(\App\Models\PersonStudy::class, function (Faker $faker) {
    return [
        'tenant_id' => 1,
        'place' => $faker->text(100),
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'document' => $faker->text(50),
        'document_number' => $faker->numberBetween(1, 1000000),
        'user_id' => 0,
    ];
});

//$factory->define(\App\Models\PersonLanguage::class, function (Faker $faker) {
//    return [
//        'tenant_id' => 1,
//        'name' => $faker->text(30),
//        'mark_types_id' => \App\Models\MarkType::all()->random()->id,
//        'user_id' => 0,
//    ];
//});

$factory->define(\App\Models\PersonLaborActivity::class, function (Faker $faker) {
    return [
        'tenant_id' => 1,
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'user_id' => 0,
        'workplace' => str_limit($faker->address(),60, ''),
        'position' => $faker->word,
    ];
});


