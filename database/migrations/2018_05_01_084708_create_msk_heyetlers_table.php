<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMskHeyetlersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msk_heyetlers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60);
            $table->unsignedInteger('tenant_id');
            $table->time('start_time');
            $table->time('end_time');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msk_heyetlers');
    }
}
