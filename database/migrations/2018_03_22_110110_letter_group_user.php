<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LetterGroupUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_group_user', function (Blueprint $table) {
            $table->primary(['user_id','letter_group_id','year_id']);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('letter_group_id');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('tenant_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('letter_group_id')->references('id')->on('letter_groups')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_group_user');
    }
}
