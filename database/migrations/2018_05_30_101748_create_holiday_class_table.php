<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidayClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday_msk_class', function (Blueprint $table) {
            $table->primary(['msk_class_id','holiday_id']);
            $table->unsignedInteger('holiday_id');
            $table->unsignedInteger('msk_class_id');

            $table->foreign('msk_class_id')->references('id')->on('msk_classes')->onDelete('CASCADE');
            $table->foreign('holiday_id')->references('id')->on('holidays')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holiday_msk_class');
    }
}
