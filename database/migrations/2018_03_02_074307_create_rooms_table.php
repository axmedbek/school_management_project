<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('info', 255)->nullable();
            $table->integer('type');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('corpus_id');
            $table->smallInteger("capacity");
            $table->softDeletes();

            $table->foreign('corpus_id')->references('id')->on('corpuses')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
