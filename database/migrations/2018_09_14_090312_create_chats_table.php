<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->unsignedInteger('sender_id');
            $table->unsignedInteger('reciever_id')->nullable();
            $table->dateTime('date');
            $table->unsignedInteger('letter_group_id')->nullable();
            $table->boolean('read')->default(0);
            $table->unsignedInteger('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
