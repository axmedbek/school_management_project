<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment', function (Blueprint $table) {
            $table->increments('id');

//          //name of equipment
            $table->string('name',120);
            //type of equipment such as ip camera , phone and i.e
            $table->integer('type');
            //floor where equipment is allocated
            $table->integer('floor')->nullable();
            //ip address of equipment
            $table->ipAddress('ip')->nullable();
            //model of equipment
            $table->string('model',120)->nullable();
            //firmware version of equipment
            $table->string('version',120)->nullable();
            //destination of equipment such as input , output and i.e
            $table->integer('eqp_type')->nullable();
            $table->string('destination',120)->nullable();
            $table->string('number',120)->nullable();
            $table->unsignedInteger('room_id')->nullable();
            $table->unsignedInteger('corpus_id');
            $table->unsignedInteger('tenant_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment');
    }
}
