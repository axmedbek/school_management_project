<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClassSubject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msk_class_subject', function (Blueprint $table) {
            $table->primary(['msk_class_id','subject_id']);
            $table->unsignedInteger('msk_class_id');
            $table->unsignedInteger('subject_id');
            $table->foreign('msk_class_id')->references('id')->on('msk_classes')->onDelete('CASCADE');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msk_class_subject');
    }
}
