<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMskRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msk_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',120);
            $table->unsignedInteger('msk_cities_id');
            $table->unsignedInteger('tenant_id');

            $table->foreign('msk_cities_id')->references('id')->on('msk_cities')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msk_regions');
    }
}
