<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('letter_group_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('week_day');
            $table->unsignedInteger('class_time_id');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('corpus_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('letter_group_id')->references('id')->on('letter_groups')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            //$table->foreign('room_id')->references('id')->on('rooms')->onDelete('CASCADE');
            //$table->foreign('corpus_id')->references('id')->on('corpuses')->onDelete('CASCADE');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('CASCADE');
            $table->foreign('class_time_id')->references('id')->on('class_times')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
