<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('corpus_id');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('msk_class_id');
            $table->string('name', 60);

            $table->foreign('corpus_id')->references('id')->on('corpuses')->onDelete('CASCADE');
            $table->foreign('msk_class_id')->references('id')->on('msk_classes')->onDelete('CASCADE');
            $table->foreign('year_id')->references('id')->on('years')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_letters');
    }
}
