<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('range');
            $table->date('date_of_show');
            $table->date('date_of_event')->nullable();
            $table->integer('adding_user');
            $table->text('content');
            $table->string('link')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advert_tasks');
    }
}
