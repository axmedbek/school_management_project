<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMskMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msk_marks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60);
            $table->unsignedInteger('mark_type_id');
            $table->unsignedInteger('tenant_id');

            $table->foreign('mark_type_id')->references('id')->on('mark_types')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msk_marks');
    }
}
