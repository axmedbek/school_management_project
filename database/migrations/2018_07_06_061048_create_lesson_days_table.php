<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_days', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->date('date');
            $table->tinyInteger('order')->default(0);
            $table->unsignedInteger('year_id');
            $table->string('type', 10)->default('lesson');
            $table->unsignedInteger('lesson_id')->nullable();
            $table->unsignedInteger('letter_group_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('substitution_lesson_id')->nullable();

            //$table->unsignedInteger('subject_id')->nullable();
            //$table->unsignedInteger('msk_class_id')->nullable();
            //$table->unsignedInteger('class_letter_id')->nullable();

            $table->foreign('year_id')->references('id')->on('years')->onDelete('CASCADE');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onDelete('CASCADE');
            //$table->foreign('letter_group_id')->references('id')->on('letter_groups')->onDelete('CASCADE');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            //$table->foreign('substitution_lesson_id')->references('id')->on('substitution_lessons')->onDelete('CASCADE');

            //$table->foreign('subject_id')->references('id')->on('subjects')->onDelete('CASCADE');
            //$table->foreign('class_letter_id')->references('id')->on('class_letters')->onDelete('CASCADE');
            //$table->foreign('msk_class_id')->references('id')->on('msk_classes')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_days');
    }
}
