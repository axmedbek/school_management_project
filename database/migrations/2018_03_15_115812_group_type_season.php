<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupTypeSeason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_type_season', function (Blueprint $table) {
            $table->primary(['group_type_id','season_id']);
            $table->unsignedInteger('group_type_id');
            $table->unsignedInteger('season_id');

            $table->foreign('group_type_id')->references('id')->on('group_types')->onDelete('CASCADE');
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_type_season');
    }
}
