<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('surname', 30)->nullable();
            $table->string('middle_name', 30)->nullable();
            $table->string('mobil_tel', 20)->nullable();
            $table->string('home_tel', 20)->nullable();
            $table->unsignedInteger("group_id")->nullable();
            $table->date('enroll_date')->nullable();
            $table->date('birthday')->nullable();
            $table->string('birth_place', 60)->nullable();
            $table->string('thumb', 30)->nullable();
            $table->string('email')->nullable();
            $table->string('username',20)->nullable();
            $table->string('password')->nullable();
            $table->string('user_type', 10);
            $table->integer('personal_heyeti')->nullable();
            $table->string('gender', 1);
            $table->integer('msk_study_levels_id')->nullable();
            $table->boolean('restrict_edit')->nullable();

            $table->integer('lived_city')->nullable();
            $table->integer('lived_region')->nullable();
            $table->string('lived_address',120)->nullable();

            $table->integer('current_city')->nullable();
            $table->integer('current_region')->nullable();
            $table->string('current_address',120)->nullable();

            $table->string('position',60)->nullable();
            $table->unsignedInteger('msk_teacher_status_id')->nullable();//
            $table->integer('internship')->nullable();
            $table->unsignedInteger('tenant_id');
            $table->timestamps();
            $table->rememberToken();
            $table->softDeletes();

            //$table->unique('email');
            $table->foreign("group_id")->references("id")->on("groups");
            $table->foreign("msk_teacher_status_id")->references("id")->on("msk_teacher_statuses");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
