<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('class_letter_id');
            $table->string('name', 60);
            $table->integer('isMainGroup')->default(0);
            $table->unsignedInteger('group_type_id');

            $table->foreign('class_letter_id')->references('id')->on('class_letters')->onDelete('NO ACTION');
            $table->foreign('group_type_id')->references('id')->on('group_types')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_groups');
    }
}
