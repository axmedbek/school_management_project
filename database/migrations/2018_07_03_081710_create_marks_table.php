<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('teacher_id')->nullable();
            $table->unsignedInteger('student_id');
            $table->date('date');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('class_letter_id');
            $table->unsignedInteger('class_time_id')->nullable();
            $table->unsignedInteger('msk_mark_id')->nullable();
            $table->boolean('qb')->default(0);
            $table->string('type', 10);
            $table->integer('value')->nullable();
            $table->timestamp('created_at')->nullable();

            $table->foreign('teacher_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('msk_mark_id')->references('id')->on('msk_marks')->onDelete('CASCADE');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('CASCADE');
            $table->foreign('class_letter_id')->references('id')->on('class_letters')->onDelete('CASCADE');
            $table->foreign('class_time_id')->references('id')->on('class_times')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
    }
}
