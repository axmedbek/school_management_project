const PORT = 2222;
const io = require('socket.io')(PORT),
    _ = require('underscore'),
    md5 = require('md5'),
    http = require('http'),
    path = require('path'),
    querystring = require('querystring');
require('dotenv').config({path: path.join(__dirname, "/.env")});

const API_HOST = process.env.API_HOST;
const API_PORT = process.env.API_PORT;
const API_PATH_PREFIX = process.env.API_PATH_PREFIX;
var users = Object.create(null);

io.use(function (socket, next) {
    let handshakeQuery = socket.handshake.query;
    var postedUserInfoDataQueryString = querystring.stringify({'authType' : handshakeQuery.authType});
    let options = {
        host: API_HOST,
        port: API_PORT,
        path: API_PATH_PREFIX + '/chat/get_my_info',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postedUserInfoDataQueryString),
            'Cookie': 'school_management_system_session=' + handshakeQuery.token

        }
    };

    let req = http.request(options, function (response) {
        let data = '';
        response.on('error', function () {
            console.log(" io use 1 was happened unexpected error");
            return next(new Error("not authorized"));
        });

        response.on('data', function (chunk) {
            data += chunk.toString();
        });

        response.on('end', function () {
            try {
                var userData = JSON.parse(data);
                if (userData.status !== "success") {
                    console.log('error');
                }
            } catch (e) {
                console.log(e);
                return next(new Error("not authorized"));
            }

            let connectedUserId = userData.user.id;

            if (_.isUndefined(users[connectedUserId])) {
                users[connectedUserId] = userData.user;
                users[connectedUserId].socketIds = [];
            }

            users[connectedUserId].socketIds.push(socket.id);
            next();
        });
    });
    req.write(postedUserInfoDataQueryString);
    req.on('error', function (e) {
        console.log(" io use 2 was happened unexpected error");
        console.log(e);
        return next(new Error("not authorized"));
    });

    req.end();

});

io.on('connection', function (socket) {
    let userInfo = getUserInformationBySocketId(socket.id);
    if (!_.isNull(users)) {

        makeOfflineOrOnlineUser({
            'user_id': userInfo.id,
            'school_management_system_session': userInfo.school_management_system_session,
            'status' : 1
        });

        _.each(users, function (user) {
            user.socketIds.forEach(function (socketId) {
                io.to(socketId).emit('user online', {
                    'user_id': userInfo.id,
                });
            });
        });
    }
    socket.on('new message', function (response) {
        saveMesssage({
            'sender_id': response.sender_id,
            'recivier_id': response.user_id,
            'message': response.message,
            '_token': response._token,
            'school_management_system_session': response.school_management_system_session,
            'type': response.type,
        });

        if (response.type == "private") {

            if (_.isUndefined(users[response.user_id])) {
                return;
            }

            users[response.user_id].socketIds.forEach(function (socketId) {
                io.to(socketId).emit('new message', {
                    'user_id' : response.sender_id,
                    'name': users[response.sender_id].name,
                    'surname': users[response.sender_id].surname,
                    'middle_name': users[response.sender_id].middle_name,
                    'thumb': users[response.sender_id].thumb,
                    'connection_id': response.sender_id,
                    'content': response.message,
                    'type': 'private'
                });
            });
        } else if (response.type == "group") {
            _.each(getUserByGroupId(response.user_id, response.sender_id), function (user) {
                user.socketIds.forEach(function (socketId) {
                    io.to(socketId).emit('new message', {
                        'name': users[response.sender_id].name,
                        'surname': users[response.sender_id].surname,
                        'middle_name': users[response.sender_id].middle_name,
                        'thumb': users[response.sender_id].thumb,
                        'connection_id': response.user_id,
                        'content': response.message,
                        'type': 'group'
                    });
                });
            });
            _.each(getAdminUsers(response.sender_id),function (user) {
                if (_.isUndefined(user)) {
                    return;
                }
                user.socketIds.forEach(function (socketId) {
                    io.to(socketId).emit('new message', {
                        'name': users[response.sender_id].name,
                        'surname': users[response.sender_id].surname,
                        'middle_name': users[response.sender_id].middle_name,
                        'thumb': users[response.sender_id].thumb,
                        'connection_id': response.user_id,
                        'content': response.message,
                        'type': 'group'
                    });
                });
            });
        }

    });
    socket.on('disconnect', function () {
        if (_.isNull(userInfo)) {
            return;
        }
        // removing socket from user
        userInfo.socketIds = _.without(userInfo.socketIds, socket.id);

        if (userInfo.socketIds.length === 0) {
            makeOfflineOrOnlineUser({
                'user_id': userInfo.id,
                'school_management_system_session': userInfo.school_management_system_session,
                'status' : 0
            });

            _.each(users, function (user) {
                user.socketIds.forEach(function (socketId) {
                    io.to(socketId).emit('user offline', {
                        'user_id': userInfo.id,
                        'last_active_date': userInfo.last_active_date
                    });
                });
            });

            delete users[userInfo.id];
        }
    });
});

function saveMesssage(postedData) {
    var postedDataQueryString = querystring.stringify(postedData);
    let options = {
        host: API_HOST,
        port: API_PORT,
        path: API_PATH_PREFIX + '/chat/save_message',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postedDataQueryString),
            'Cookie': 'school_management_system_session=' + postedData.school_management_system_session

        }
    };
    let req = http.request(options, function (response) {
        var data = '';
        response.on('data', function (chunk) {
            data += chunk.toString();
        });

        response.on('end', function () {
            if (data.status === "error") {
                throw new Error("validation error");
            }
            else{
                data = JSON.parse(data);
                if (data.status === "success"){
                    if (_.isUndefined(users[data.notifyObj.user_id])) {
                        return;
                    }
                    users[data.notifyObj.user_id].socketIds.forEach(function (socketId) {
                        io.to(socketId).emit('new notify', {
                            'id' : data.notifyObj.id,
                            'icon' : data.notifyObj.icon,
                            'content' : data.notifyObj.content,
                            'color' : data.notifyObj.color,
                            'route' : data.notifyObj.route
                        });
                    });
                }
            }
        });
    });
    req.write(postedDataQueryString);
    req.on('error', function (e) {
        console.log(e);
        // console.log('There is an error with saving message');
    });
    req.end();
}

function makeOfflineOrOnlineUser(postedData) {
    var postedOfflineDataQueryString = querystring.stringify(postedData);
    let options = {
        host: API_HOST,
        port: API_PORT,
        path: API_PATH_PREFIX + '/chat/make_offline_user',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postedOfflineDataQueryString),
            'Cookie': 'school_management_system_session=' + postedData.school_management_system_session

        }
    };

    let req = http.request(options, function (response) {
        let data = '';
        response.on('data', function (chunk) {
            data += chunk.toString();
        });
        response.on('end', function () {

        });
    });

    req.write(postedOfflineDataQueryString);
    req.on('error', function () {

    });
    req.end();
}

function getUserByGroupId(group_id, sender_id) {
    var filteredGroupUsers = {};
    _.each(users, function (user, key) {
        _.each(user.groups,function (group) {
            if (group.group_id == group_id && user.id != sender_id) {
                filteredGroupUsers[key] = user;
            }
        });
    });
    return filteredGroupUsers;
}

function getAdminUsers(sender_id){
    var filteredAdminUsers = {};
    _.each(users, function (user, key) {
        if (user.user_type === 'user' && user.id !== sender_id) {
            filteredAdminUsers[key] = user;
        }
    });
    return filteredAdminUsers;
}

function getUserInformationBySocketId(socketId) {
    for (let userId in users) {
        let userSockets = users[userId].socketIds;
        for (var i = 0, len = userSockets.length; i < len; ++i) {
            if (socketId === userSockets[i]) {
                return users[userId];
            }
        }
    }
    return null;
}

console.log(API_HOST + " " + API_PORT + " is listening");
