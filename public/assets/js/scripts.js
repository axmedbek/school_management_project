(function ($) {

    'use strict';

    $(document).ready(function () {
        // Initializes search overlay plugin.
        // Replace onSearchSubmit() and onKeyEnter() with 
        // your logic to perform a search and display results
        $(".list-view-wrapper").scrollbar();

        $('[data-pages="search"]').search({
            // Bind elements that are included inside search overlay
            searchField: '#overlay-search',
            closeButton: '.overlay-close',
            suggestions: '#overlay-suggestions',
            brand: '.brand',
            // Callback that will be run when you hit ENTER button on search box
            onSearchSubmit: function (searchString) {
                console.log("Search for: " + searchString);
            },
            // Callback that will be run whenever you enter a key into search box. 
            // Perform any live search here.  
            onKeyEnter: function (searchString) {
                console.log("Live search for: " + searchString);
                var searchField = $('#overlay-search');
                var searchResults = $('.search-results');

                /* 
                    Do AJAX call here to get search results
                    and update DOM and use the following block 
                    'searchResults.find('.result-name').each(function() {...}'
                    inside the AJAX callback to update the DOM
                */

                // Timeout is used for DEMO purpose only to simulate an AJAX call
                clearTimeout($.data(this, 'timer'));
                searchResults.fadeOut("fast"); // hide previously returned results until server returns new results
                var wait = setTimeout(function () {

                    searchResults.find('.result-name').each(function () {
                        if (searchField.val().length != 0) {
                            $(this).html(searchField.val());
                            searchResults.fadeIn("fast"); // reveal updated results
                        }
                    });
                }, 500);
                $(this).data('timer', wait);

            }
        })

    });


    $('.panel-collapse label').on('click', function (e) {
        e.stopPropagation();
    });
})(window.jQuery);

//delete
$(document).on("click", ".deleteAction", function () {
    $elm = $(this);

    $.confirm({
        title: 'Təsdiq',
        content: 'Silmək istədiyinizə əminsiniz?',
        type: 'red',
        typeAnimated: true,
        buttons: {

            formSubmit: {
                text: 'Bəli',
                btnClass: 'btn-green',
                action: function () {
                    window.location.href = $elm.attr("url");
                }
            },
            formCancel: {
                text: 'Xeyr',
                btnClass: 'btn-red',
                action: function () {

                }
            }
        }
    });

    return false;
});

function pageLoading(action) {
    if (action == 'show') $("#page_loading").show();
    else $("#page_loading").hide();
}

//modal
var loadingModal = $('#myModal #loading').html();
var loadingModal2 = $('#myModal2 #loading2').html();

function openModal(route, data, type, count) {
    if (count == 2) {
        type = typeof type !== 'undefined' ? type : 'stick-up';
        $('#myModal2').attr("class", "modal fade disable-scroll " + type);

        $('#myModal2').html(loadingModal2).modal('show');

        $.get(route, data, function (response) {
            $('#myModal2').html(response);
            $('#myModal').modal('hide');
            $('#myModal2').attr('modalCount', count);
            $('#myModal2').on('hidden.bs.modal', function () {
                $('#myModal').modal('show');
            });
        });
    }
    else if (count == 1) {
        type = typeof type !== 'undefined' ? type : 'stick-up';
        $('#myModal2').attr("class", "modal fade disable-scroll " + type);

        $('#myModal2').html(loadingModal).modal('show');
        $('#myModal2').attr('modalCount', count);
        $.get(route, data, function (response) {
            $('#myModal2').html(response);
        });
    }

    else {
        type = typeof type !== 'undefined' ? type : 'stick-up';
        $('#myModal').attr("class", "modal fade disable-scroll " + type);

        $('#myModal').html(loadingModal).modal('show');

        $.get(route, data, function (response) {
            $('#myModal').html(response);
        });
    }

}

//page
function openPage(taget, route, data) {
    pageLoading('show');

    if (!data) data = {};
    data['_token'] = _token;

    $.post(route, data).done(function (response) {
        $(taget).html(response);
        pageLoading('hide');
    });
}

//form
$('.form_find').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).parents("form").submit();
    }
});

$(".menu-items .sub-menu:not(:has(li))").parents("li:eq(0)").remove();

//menu
$(".sidebar-header-controls .visible-lg-inline").click(function () {
    // setCookie("menuPin", !$("body").hasClass("menu-pin"), 120);
});

//cookie
function setCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}