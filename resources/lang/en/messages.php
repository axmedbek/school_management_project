<?php

return [
    //
    'standarts_user_single' => 'User',
    'standarts_student_single' => 'Student',
    'standarts_staff_single' => 'Staff',
    'standarts_teacher_single' => 'Teacher',
    'standarts_parent_single' => 'Parent',

    'months' => [
        'January' =>  'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December',
    ],

    'monthsShort' => ['Yan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
    'daysMin' => ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],

    ];