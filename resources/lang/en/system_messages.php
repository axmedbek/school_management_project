<?php
/**
 * Created by PhpStorm.
 * User: axmedbek
 * Date: 8/27/18
 * Time: 10:33 AM
 */

return [

    'clear' => 'Clean',
    'showBtn' => 'Show',
    'closeBtn' => 'Close',
    'refreshBtn' => 'Refresh',
    'addBtn' => 'Add',
    'makeBtn' => 'Apply',


    'login' => [
        'username' => 'Username',
        'password' => 'Password',
        'loginBtn' => 'Sign in'
    ],

    'weeks' => [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday',
    ],

    //home page
    'dashboard' => [
        'lesson' => [
            'lesson_table' => 'Lessons table',
            'weekly' => 'Weekly',
            'subject' => 'Subject',
            'class_room' => 'Class/Room',
            'time' => 'Time'
        ],

        'advert_task' => [
            'advert_task_table' => 'Advert and tasks table',
            'show_all' => 'Show all',
            'date_of_event' => 'Event date',
            'content' => 'Content',
            'inserting_user' => 'Adding user',
            'status' => [
                'name' => 'Status',
                'waiting' => 'Next one',
                'all' => 'All',
                'ending' => 'Finished',
                'last_day' => 'Last day',
            ],
            'select_status' => 'Select the status',
            'task_not_found' => 'Task and event were not found',
        ],

        'holiday' => [
            'holiday_table' => 'Days off table',
            'name' => 'Name',
            'date' => 'Date of the beginning/termination',
            'type' => 'Type',
        ],

        'in_out' => [
            'title' => 'Input/Output',
            'today' => 'Today',
            'in' => 'Input',
            'out' => 'Output',
        ],

        'ring_table' => [
            'title' => 'Calls timetable',
            'start' => 'Begins',
            'end' => 'Terminates',
        ],
    ],

    //lesson materials
    'lesson_materials' => [
        'lessons' => 'Lessons',
        'homeworks' => 'Homework',
        'subject' => 'Subject',
        'date' => 'Date',
        'text' => 'Topic',
        'task' => 'Job',
        'files' => 'The electronic manual',
    ],

    //student attendance
    'student_attendance' => [
        'date' => 'Date',
        'enter' => 'Entry',
        'enter_status' => 'Entry Status',
        'exit' => 'Left',
        'exit_status' => 'Left Status',
        'interval' => 'Interval',
    ],

    //galery
    'galery' => [
        'search' => 'Search',
        'search_folder' => 'Search folder',
        'images' => 'FOTO',
    ],

    //teacher request
    'teacher_request' => [
        'title' => 'Address of the teacher',
        'request_date' => 'Date of address',
        'teacher' => 'The teacher',
        'student' => 'Pupil`s full name',
        'teacher_custom_request' => 'Address of the teacher',
        'text' => 'Content',
        'info' => 'Click for more information',
        'teacher_request_notify' => 'Notifications',
        'teacher_request_notify_info' => 'At the moment there are no notification',
    ],

    //video camera
    'video_camera' => [
        'title' => 'Camera',
        'lesson_info' => 'Lesson information',
        'next_lesson' => 'Teacher of the following lesson',
        'current_lesson' => 'Teacher of the current lesson',
        'subject' => 'Subject',
        'lesson' => 'Lesson',
        'room' => 'Room',
        'time' => 'Time',
        'start_date' => 'Start time',
        'end_date' => 'End time',
        'start' => 'Begins',
        'end' => 'Terminates',
        'not_lesson_info' => 'At the moment there is no information for show',
        'refresh_cam_info' => 'In the presence of problems with display, please refresh',
        'lesson_over' => 'Lesson is over.',
    ],

    //final evaluation
    'final_evaluation' => [
        'subject' => 'Name of a subject',
        'yearly' => 'Final assessment',
        'half_yearly' => 'In a half-year',
        'education_year' => 'School year',
    ],

    //profile
    'profile' => [
        'status' => 'Status',
        'adding_date' => 'Date of addition',
        'newBtn' => 'New',
        'changePasswordBtn' => 'Change password',
        'number' => 'Number',
        'operation' => 'Operation',
        'sms_numbers' => 'SMS numbers',
        'changePasswordInfo' => 'Update password',
        'insert_current_password' => 'Enter the current password',
        'insert_new_password' => 'Enter the new password',
        'insert_new_password_confirm' => 'Enter the new password again',
    ],

    //jurnal
    'jurnal' => [
        'month' => 'Month',
        'subject' => 'Subject',
        'select_month' => 'Select subject',
        'class' => 'Class',
        'select_class' => 'Select class',
        'fullname' => 'Full name',
        'homework' => [
            'title' => 'Homework',
            'teacher' => 'Teacher',
            'file' => 'File',
        ],
        'sms' => [
            'title' => 'Message',
            'sms' => 'SMS',
        ],
        'new_lesson' => [
            'title' => 'New lesson',
            'season' => 'Chapter',
            'topic' => 'Topic',
        ],
        'mark' => [
            'title' => 'Mark',
            'mark_type' => 'Mark type',
            'marks' => 'Value',
            'qb' => 'Absent',
            'delete' => 'Delete',
        ],
    ],

    //topics
    'topics' => [
        'classes' => 'Classes',
        'subjects' => 'Subjects',
        'season' => 'Chapter',
        'paraqraf' => 'Paragraph',
        'standart' => 'Standart',
        'time' => 'Hours',
    ],

    //modules
    'modules' => [
        'name' => 'Modules',

        'portal_dashboard' => 'Dashboard',
        'portal_lesson_materials' => 'Tasks',
        'portal_jurnal' => 'Class register',
        'portal_attendance' => 'Attendance',
        'portal_topics' => 'Curriculum',
        'portal_galery' => 'Galery',
        'portal_teacher_request' => 'Teacher requests',
        //video_camera
        'portal_camera' => 'Video observation',
        //chat
        'portal_chat' => 'Chat',
        //reports
        'portal_final_evaluation' => 'Total estimation of school year',
        //upgrade
        'portal_library' => 'Library',
        'portal_test' => 'Examination',
        'portal_online_payment' => 'Online payment',
    ],
    'notification'=>[
        'galery' => [
            'image_added' => 'New photos are added to the folder :folder'
        ],
        'chat' => [
            'new' => 'You received the message'
        ]
    ]


];
