<?php
/**
 * Created by PhpStorm.
 * User: axmedbek
 * Date: 8/27/18
 * Time: 10:33 AM
 */

return [

    'clear' => 'Очистить',
    'showBtn' => 'Показать',
    'closeBtn' => 'Закрыть',
    'refreshBtn' => 'Обновить',
    'addBtn' => 'Добавить',
    'makeBtn' => 'Применить',


    'login' => [
        'username' => 'Имя пользователя',
        'password' => 'Пароль',
        'loginBtn' => 'Войти'
    ],

    'weeks' => [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
        'Воскресенье',
    ],

    //home page
    'dashboard' => [
        'lesson' => [
            'lesson_table' => 'Расписание уроков',
            'weekly' => 'Неделя',
            'subject' => 'Предмет',
            'class_room' => 'Класс/Комната',
            'time' => 'Время'
        ],

        'advert_task' => [
            'advert_task_table' => 'Таблица объявлений и задач',
            'show_all' => 'Показать все',
            'date_of_event' => 'Дата проведения',
            'content' => 'Содержание',
            'inserting_user' => 'Добавил',
            'status' => [
                'name' => 'Статус',
                'waiting' => 'Следующий',
                'all' => 'Все',
                'ending' => 'Окончен',
                'last_day' => 'Последний день',
            ],
            'select_status' => 'Выберете статус',
            'task_not_found' => 'Объявление и задание не найдены ',
        ],

        'holiday' => [
            'holiday_table' => 'Таблица нерабочих дней',
            'name' => 'Название',
            'date' => 'Дата начала/окончания',
            'type' => 'Тип',
        ],

        'in_out' => [
            'title' => 'Вход/Выход',
            'today' => 'Сегодня',
            'in' => 'Вход',
            'out' => 'Выход',
        ],

        'ring_table' => [
            'title' => 'Расписание звонков',
            'start' => 'Начинается',
            'end' => 'Оканчивается',
        ],
    ],

    //lesson materials
    'lesson_materials' => [
        'lessons' => 'Уроки',
        'homeworks' => 'Домашнее задание',
        'subject' => 'Предмет',
        'date' => 'Дата',
        'text' => 'Тема',
        'task' => 'Задание',
        'files' => 'Электронное пособие',
    ],

    //student attendance
    'student_attendance' => [
        'date' => 'Дата',
        'enter' => 'Пришел',
        'enter_status' => 'Статус входа',
        'exit' => 'Ушел',
        'exit_status' => 'Статус выхода',
        'interval' => 'Интервал',
    ],

    //galery
    'galery' => [
        'search' => 'Поиск',
        'search_folder' => 'Найти папку',
        'images' => 'ФОТО',
    ],

    //teacher request
    'teacher_request' => [
        'title' => 'Обращение преподавателя',
        'request_date' => 'Дата обращения',
        'teacher' => 'Преподаватель',
        'student' => 'ИФО ученика',
        'teacher_custom_request' => 'Обращение преподавателя',
        'text' => 'Содержание',
        'info' => 'Для большей информации',
        'teacher_request_notify' => 'Yведомления',
        'teacher_request_notify_info' => 'На данный момент уведомление нет',
    ],

    //video camera
    'video_camera' => [
        'title' => 'Камера',
        'lesson_info' => 'Информация об уроке',
        'next_lesson' => 'Преподаватель следующего урока',
        'current_lesson' => 'Преподователь текущего урока',
        'subject' => 'Предмет',
        'lesson' => 'Урок',
        'room' => 'Комната',
        'time' => 'Время',
        'start_date' => 'Время начала',
        'end_date' => 'Время окончания',
        'start' => 'Начинается',
        'end' => 'Оканчивается',
        'not_lesson_info' => 'На данный момент нет информации для показа',
        'refresh_cam_info' => 'При наличии проблем с отображением, пожалуйста обновите',
        'lesson_over' => 'Урок окончен.',
    ],

    //final evaluation
    'final_evaluation' => [
        'subject' => 'Наименование предмета',
        'yearly' => 'Итоговая',
        'half_yearly' => 'За полугодие',
        'education_year' => 'Учебный год',
    ],

    //profile
    'profile' => [
        'status' => 'Статус',
        'adding_date' => 'Дата добавления',
        'newBtn' => 'Новый',
        'changePasswordBtn' => 'Сменить пароль',
        'number' => 'Номер',
        'operation' => 'Операция',
        'sms_numbers' => 'SMS номера',
        'changePasswordInfo' => 'Обновите пароль',
        'insert_current_password' => 'Введите текущий пароль',
        'insert_new_password' => 'Введите новый пароль',
        'insert_new_password_confirm' => 'Введите повторно новый пароль',
    ],

    //jurnal
    'jurnal' => [
        'month' => 'Месяц',
        'subject' => 'Предмет',
        'select_month' => 'Выберете предмет',
        'class' => 'Класс',
        'select_class' => 'Выберете класс',
        'fullname' => 'Имя Фамилия Отчество',
        'homework' => [
            'title' => 'Домашнее задание',
            'teacher' => 'Преподаватель',
            'file' => 'Фаил',
        ],
        'sms' => [
            'title' => 'Сообщение',
            'sms' => 'SMS',
        ],
        'new_lesson' => [
            'title' => 'Новый урок',
            'season' => 'Глава',
            'topic' => 'Тема',
        ],
        'mark' => [
            'title' => 'Оценка',
            'mark_type' => 'Тип оценки',
            'marks' => 'Значения',
            'qb' => 'НБ',
            'delete' => 'Удалить',
        ],
    ],

    //topics
    'topics' => [
        'classes' => 'Классы',
        'subjects' => 'Предметы',
        'season' => 'Глава',
        'paraqraf' => 'Параграф',
        'standart' => 'Стандарт',
        'time' => 'Часы',
    ],

    //modules
    'modules' => [
        'name' => 'Mодули',

        'portal_dashboard' => 'Главная страница',
        'portal_lesson_materials' => 'Задачи',
        'portal_jurnal' => 'Журнал',
        'portal_attendance' => 'Успеваемость',
        'portal_topics' => 'Учебный план',
        'portal_galery' => 'Галерея',
        'portal_teacher_request' => 'Обращение преподавателя',
        //video_camera
        'portal_camera' => 'Видео наблюдение',
        //chat
        'portal_chat' => 'Чат',
        //reports
        'portal_final_evaluation' => 'Итоговое оценивание учебного года',
        //upgrade
        'portal_library' => 'Библиотека',
        'portal_test' => 'Экзамен',
        'portal_online_payment' => 'Онлаин оплата',
    ],

    'notification'=>[
        'galery' => [
            'image_added' => 'В папку :folder добавлены фотографии'
        ],
        'chat' => [
            'new' => 'Вы получили сообщение '
        ]
    ]

];