<?php

return [
    //
    'standarts.user.single' => 'Пользователь',
    'standarts.student.single' => 'Ученик',
    'standarts.staff.single' => 'Персонал',
    'standarts.teacher.single' => 'Учитель',
    'standarts_parent_single' => 'Родитель',

    'months' => [
        'January' => 'Январь',
        'February' => 'Февраль',
        'March' => 'Март',
        'April' => 'Апрель',
        'May' => 'Май',
        'June' => 'Июнь',
        'July' => 'Июль',
        'August' => 'Август',
        'September' => 'Сентябрь',
        'October' => 'Октябрь',
        'November' => 'Ноябрь',
        'December' => 'Декабрь',
    ],

    'monthsShort' => ['Ян','Фев','Мар','Апр','Май','Июнь','Июль','Авг','Сен','Окт','Ноя','Дек'],
    'daysMin' => ['Во','По','Вт','Ср','Че','Пя','Су'],
];