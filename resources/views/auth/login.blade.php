@extends('layouts.app')

<style>
    .login-wrapper .row {
        display: flex;
        align-items: center;
        justify-content: center;
        align-self: center;
        align-content: center;
    }

    .adminLoginBtn {
        color: #ffffff;
        background-color: #ff847b !important;
        border-color: #ff847b !important;
        width: 100%;
        border-radius: 4px !important;
        height: 50px; !important;
    }

    .login-content-holder {
        width: 350px !important;
    }

    .login-content-holder .title {
        color: white;
        font-weight: bold;
    }

    .login-content-holder .description {
        color: white;
        margin-bottom: 30px;
        font-style: italic;
    }

    .form-group-default {
        border-radius: 4px !important;
    }
</style>

@section('content')
    <div class="login-wrapper">
        <div class="row">
            <div class="col-md-12 login-content-holder">
                <h3 class="title">Welcome Admin !</h3>
                <p class="description">Here you have a new access</p>
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
                <form method="post" action="{{ route('admin_login') }}">
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>İSTİFADƏÇİ ADI</label>
                        <div class="controls">
                            <input type="text" value="{{ old('username') }}" name="username" placeholder="İstifadəçi adı" class="form-control" required>
                        </div>
                    </div>
                    <!-- END Form Control-->
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>ŞİFRƏ</label>
                        <div class="controls">
                            <input type="password" class="form-control" name="password" placeholder="Şifrə" required>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <button class="btn btn-primary btn-cons m-t-10 adminLoginBtn" type="submit">Giriş</button>
                </form>
            </div>
        </div>
    </div>
@endsection
