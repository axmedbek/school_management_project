<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <link href="{{ asset('favicon.ico') }}" rel="shortcut icon">
    <link href="{{ asset('portal/apple-touch-icon.png') }}" rel="apple-touch-icon">
    <link href="{{ asset('portal/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('portal/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('portal/css/maince5a.css?version=4.4.1') }}" rel="stylesheet">

    <style>
        body {
            padding: 0;
        }

        .auth-wrapper .all-wrapper {
            padding: 0 !important;
            margin: 0;
            max-width: 100%;
            height: 100vh;
        }

        @font-face {
            font-family: EduCatClientFont;
            src: url({{ asset('fonts/GeorgiaRef.ttf') }});
        }

        @font-face {
            font-family: EduCatFont;
            src: url({{ asset('fonts/Constantia_l.ttf') }});
        }

        .font-argue span {
            font-size: 12px;
            font-weight: 600;
        }

        .login_bg_holder {
            background: url('{{ asset('portal/img/login_bg.svg') }}');
            background-size: cover !important;
            background-repeat: no-repeat;
        }

        .login-container {
            height: 100%;
        }

        .login_form_holder {
            background-color: white;
            display: inline-flex;
            justify-content: center;
            align-items: center;
        }

        .login_form_holder form {
            width: 80%;
        }

        .login_form_holder .login_btn {
            margin-top: 20px;
            width: 100%;
            height: 40px;
        }

        .login_form_holder .description {
            margin-bottom: 30px;
        }
    </style>
</head>
<body class="auth-wrapper">
<div class="all-wrapper menu-side with-pattern">
    <div class="row login-container">
        <div class="col-md-9 login_bg_holder"></div>
        <div class="col-md-3 login_form_holder">
            <form action="{{ route('login') }}" method="post">
                <h2 class="title">Welcome back</h2>
                <p class="description">Try to login your account</p>
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
                <div class="form-group">
                    <label for="">{{ trans('system_messages.login.username') }}</label>
                    <input class="form-control"
                           placeholder="İstifadəçi adı"
                           type="text"
                           name="username">
                </div>
                <div class="form-group">
                    <label for="">{{ trans('system_messages.login.password') }}</label>
                    <input class="form-control"
                           placeholder="Şifrə"
                           type="password"
                           name="password">
                </div>
                {{ csrf_field() }}
                <div class="buttons-w text-center">
                    <button class="btn btn-primary login_btn" type="submit">{{ trans('system_messages.login.loginBtn') }}</button>
                </div>
            </form>

        </div>
    </div>
</div>
</body>
</html>