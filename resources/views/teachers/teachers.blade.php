@extends('base')
@section('container')
    <div class="row">
        <div class="col-md-3" style="float: right">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('str_teacher',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('str_teachers_journal_editing_priv_modal')}}')" type="button" class="btn btn-warning">
                                  <span class="p-t-5 p-b-5">
                                      <i class="fa fa-user-md fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Nizamlama</span>
                        </a>
                    @endif
                </div>
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('str_teacher',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('str_teachers_add_edit',['user' => 0]) }}')" type="button" class="btn btn-success">
                                  <span class="p-t-5 p-b-5">
                                      <i class="pg-plus_circle fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Yeni</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
            <li class="{{ $request->get('type', 'active') == 'active' ? 'active':'' }}"><a href="?type=active" role="tab" aria-expanded="true">Aktİv</a></li>
            <li class="{{ $request->get('type', 'active') == 'deactive' ? 'active':'' }}"><a href="?type=deactive" role="tab" aria-expanded="false">Deaktİv</a></li>
        </ul>
        <form action="" method="get">
            <table class="table table-hover" id="basicTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Ad Soyad</th>
                    {{--<th>Doğum tarixi</th>--}}
                    {{--<th>Qeydiyyat tarixi</th>--}}
                    <th>Ev tel</th>
                    <th>Mobil tel</th>
                    <th>Email</th>
                    <th style="width: 185px;">Əməliyyat</th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <td></td>
                    <td><input class="form-control form_find" name="name" placeholder="Ad Soyad" value="{{ $request->get('name') }}" ></td>
                    {{--<td><input class="form-control form_find datepicker" name="birthday" placeholder="Doğum tarixi" value="{{ $request->get('birthday') }}" ></td>--}}
                    {{--<td><input class="form-control form_find datepicker" name="enroll_date" placeholder="Qeydiyyat tarixi" value="{{ $request->get('enroll_date') }}" ></td>--}}
                    <td><input class="form-control form_find" name="home_tel" placeholder="Ev tel" value="{{ $request->get('home_tel') }}" ></td>
                    <td><input class="form-control form_find" name="mobil_tel" placeholder="Mobil tel" value="{{ $request->get('mobil_tel') }}" ></td>
                    <td><input class="form-control form_find" name="email" placeholder="Email" value="{{ $request->get('email') }}" ></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                    @foreach($teachers as $teacher)
                        <tr>
                            <td>{{ $teachers->perPage() * ($teachers->currentPage() - 1) + $loop->iteration }}</td>
                            <td>{{ $teacher->fullname() }}</td>
                            {{--<td>{{ empty($teacher->birthday) ? '' : date("d-m-Y", strtotime($teacher->birthday)) }}</td>--}}
                            {{--<td>{{ empty($teacher->enroll_date) ? '' : date("d-m-Y", strtotime($teacher->enroll_date)) }}</td>--}}
                            <td>{{ $teacher->home_tel }}</td>
                            <td>{{ $teacher->mobil_tel }}</td>
                            <td>{{ $teacher->email }}</td>
                            <td>
                                <div class="btn-group-sm">
                                    <a type="button" href="javascript:openModal('{{ route('str_teachers_info',['teacher' => $teacher->id]) }}')" class="btn btn-info col-lg-4 col-md-12"><i class="fa fa-info-circle"></i></a>
                                    @if(\App\Library\Helper::has_priv('str_teacher',\App\Library\Standarts::PRIV_CAN_EDIT))
                                        <a type="button" href="javascript:openModal('{{ route('str_teachers_add_edit',['teacher' => $teacher->id]) }}')" class="btn btn-primary col-lg-4 col-md-12"><i class="fa fa-pencil"></i></a>
                                        @if($teacher->id != \Illuminate\Support\Facades\Auth::user()->id)
                                            @if(request()->get('type','active') == 'active')
                                                <a type="button" url="{{ route('str_teachers_delete',['teacher' => $teacher->id]) }}" class="btn btn-danger deleteAction col-lg-4 col-md-12" data-toggle="tooltip" title="İstifadəçini deaktiv edin"><i class="fa fa-close"></i></a>
                                            @else
                                                <a type="button" href="{{ route('str_teachers_delete',['teacher' => $teacher->id]) }}" class="btn btn-success  col-lg-4 col-md-12"><i class="fa fa-check-circle"></i></a>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $teachers->appends($request->except('page')) }}
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        #basicTable thead tr th{
            font-family: Segoe UI, sans-serif !important;
            font-weight: bold;
            color: #075177;
            text-align: center;
            /*border: 1px #373636 solid;*/
        }
        #basicTable thead tr td{
            font-family: Segoe UI, sans-serif !important;
            font-weight: bold;
            text-align: center;
            /* border: 1px #373636 solid;*/
        }
        #basicTable tbody tr td{
            font-family: Segoe UI, sans-serif !important;
            text-align: center;
            /*font-weight: bold;*/
            /*border: 1px #373636 solid;*/
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
    <script>
        $("#basicTable>tbody>tr").find('a[data-toggle="tooltip"]').tooltip();
    </script>
@endsection