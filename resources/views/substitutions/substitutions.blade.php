@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-2 col-lg-1" style="float: right">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('substitution',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('substitution_add_edit_modal',['sub' => 0]) }}')" type="button" class="btn btn-success">
                                  <span class="p-t-5 p-b-5">
                                      <i class="pg-plus_circle fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Yeni</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
            <table class="table table-hover" id="basicTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Əvəzetmə tarİxİ</th>
                    <th>Əvəz edİləcək müəllİm</th>
                    <th>Əvəz edİləcək dərs </th>
                    <th>Əvəz edən müəllİm</th>
                    <th style="width: 185px;">Əməlİyyat</th>
                </tr>
                </thead>
                <tbody>
                @foreach($subs as $sub)
                    <tr>
                        <td>{{ $subs->perPage() * ($subs->currentPage() - 1) + $loop->iteration }}</td>
                        <td>{{ $sub->date }}</td>
                        <td>
                            {{ $sub->user->name }}
                        </td>
                        <td>
                            @foreach($sub->substitutionLessons as $sublessson)

                                    {{ $sublessson->lesson->room->corpus->name }}
                                    @php $class = \App\Models\LetterGroup::realData()->find($sublessson->lesson->letter_group_id)->class_letter @endphp
                                      {{  $class->msk_class->name }}{{  $class->name }}
                                      {{ $sublessson->lesson->subject->name }}
                                      ({{ (new DateTime($sublessson->lesson->class_time->start_time))->format('H:i') }}-
                                    {{ (new DateTime($sublessson->lesson->class_time->end_time))->format('H:i') }})

                            @endforeach
                        </td>
                        <td>
                            @foreach($sub->substitutionLessons as $sublessson)
                                {{ $sublessson->user->name }}
                            @endforeach
                        </td>
                        <td>
                            <div class="btn-group-sm">
                                <a type="button" href="javascript:openModal('{{ route('substitution_info',['sub' => $sub->id]) }}')" class="btn btn-info col-lg-4 col-md-12"><i class="fa fa-info-circle"></i></a>

                                @if(\App\Library\Helper::has_priv('substitution',\App\Library\Standarts::PRIV_CAN_EDIT))
                                    <a type="button" href="javascript:openModal('{{ route('substitution_add_edit_modal',['sub' => $sub->id]) }}')" class="btn btn-primary col-lg-4 col-md-12"><i class="fa fa-pencil"></i></a>
                                    <a type="button" url="{{ route('substitution_delete',['sub' => $sub->id]) }}" class="btn btn-danger deleteAction col-lg-4 col-md-12"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $subs->appends($request->except('page')) }}
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.5/lodash.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
@endsection
