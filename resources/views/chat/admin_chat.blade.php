@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-4">
            <ul class="nav nav-tabs" role="tablist" style="border: 1px solid #e0d6d6;border-radius: 4px;">
                <li class="">
                    <a class="active show" href="#quickview-chat" data-toggle="tab" role="tab"
                       aria-selected="true">Chat</a>
                </li>
                <li class="">
                    <a href="#quickview-notes" data-target="#quickview-notes" data-toggle="tab" role="tab" class=""
                       aria-selected="false">Groups</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane no-padding active show" id="quickview-chat">
                    <div class="view-port clearfix" id="chat">
                        <div class="view bg-white">

                            <div class="navbar navbar-default">
                                <div class="navbar-inner">
                                    <input type="text" class="form-control" name="search_user" placeholder="Axtar">
                                </div>
                            </div>

                            <div>
                                <ul style="height: 420px;overflow: auto;">
                                    @foreach($users as $user)
                                        <li data-id="{{ $user->id }}" class="chat-user-list clearfix" style="margin-top: 10px;
                                            border-radius: 4px;
                                            border: 2px solid #d7deda;
                                            list-style-type: none;
                                            margin-left: -40px !important;
                                            cursor: pointer;
                                            background-color: #efeff0;">
                                            <a>
<span class="thumbnail-wrapper d32 circular bg-success" style="margin-left: 10px;margin-top: 10px;">
<img width="34" height="34" alt="" data-src-retina="{{ asset('images/user/thumb/'.$user->thumb) }}"
     data-src="{{ asset('images/user/thumb/'.$user->thumb) }}" src="{{ asset('images/user/thumb/'.$user->thumb) }}"
     class="col-top">
</span>
                                                <p class="p-l-10 "
                                                   style="margin-top: 5px;padding-left: 60px !important;">
                                                    <span class="text-master name-title">{{ $user->fullname() }}</span>
                                                    <span class="block text-master hint-text fs-12 status-title">
                                                        @switch($user->user_type)
                                                            @case('student')
                                                            Şagird
                                                            @break
                                                            @case('teacher')
                                                            Müəllim
                                                            @break
                                                            @case('user')
                                                            Admin
                                                            @break
                                                            @case('parent')
                                                            Valideyn
                                                            @break
                                                        @endswitch
                                                    </span>
                                                </p>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane no-padding" id="quickview-notes">
                    <div class="view-port clearfix quickview-notes" id="note-views">

                        <div class="view note" id="quick-note">
                            <div class="navbar navbar-default">
                                <div class="navbar-inner">
                                    <input type="text" class="form-control" name="search_group" placeholder="Axtar">
                                </div>
                            </div>
                            <div>
                                <ul style="height: 420px;overflow: auto;">
                                    @foreach($groups as $group)
                                        <li data-id="{{ $group->id }}" class="chat-user-list clearfix" style="margin-top: 10px;
                                            border-radius: 4px;
                                            border: 2px solid #d7deda;
                                            list-style-type: none;
                                            margin-left: -40px !important;
                                            cursor: pointer;
                                            background-color: #efeff0;">
                                            <a>
                                                <p class="p-l-10 "
                                                   style="margin-top: 5px;padding: 4px;">
                                                    <span class="text-master name-title">
                                                        {{ $group->msk_class."/".$group->class_letter." ( ".$group->name." ) qrupu" }}
                                                    </span>
                                                </p>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="view chat-view bg-white clearfix">

                <div class="navbar navbar-default" style="border: 1px solid #e0d6d6;border-radius: 4px;">
                    <div class="navbar-inner">
                        <div class="view-heading message-name-title" style="margin-left: 20px;margin-top: 5px;">
                        </div>
                        <div class="fs-11 hint-text message-status-title"
                             style="color: #69ca9d;font-weight: bold;margin-left: 20px;"></div>
                    </div>
                </div>


                <div class="chat-inner" id="my-conversation" style="height: 430px;">

                </div>


                <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;">
                            <div class="col-md-11">
                                <input type="text" class="form-control" placeholder="Mesaj yaz..."
                                       name="chat_enter_input">
                            </div>
                            <div class="col-md-1" style="padding: 0px;">
                                <button class="btn btn-sm sendMessageBtn" style="background-color: #69ca9d;color: white;">Mesaj
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
@endsection

@section('css')
    <style>
        .activeUserTab {
            background-color: #69ca9d !important;
        }

        .activeGroupTab {
            background-color: #69ca9d !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            border-color: #e6e6e6 !important;
            color: #ededec !important;
            background-color: #69ca9d !important;
        }

        .nav-tabs > li > a:hover, .nav-tabs > li > a:focus {
            background: white !important;
            border: 2px solid #69ca9d !important;
            border-radius: 2% !important;
            color: #69ca9d !important;
        }

        .content {
            font-family: Roboto;
        }
    </style>
@endsection

@section('script')
    <script>

        socket.on('new message', function (response) {

            if (response.type == 'private') {
                if (response.connection_id == $('.activeUserTab').attr('data-id')) {
                    $('#my-conversation').append('<div class="message clearfix" style="padding: 15px;">'+
                    '<div style="margin-left: 8px;background-color: antiquewhite;width: 230px;'+
                    'padding: 5px;padding-left: 15px;border-radius: 30px;">' + response.name + ' ' + response.surname + ' ' + response.middle_name +'</div>'+
                    '<div class="profile-img-wrapper m-t-5 inline">'+
                    '<img class="col-top" width="30" height="30" src=""  alt="" data-src=""'+
                    'data-src-retina=""></div>'+
                    '<div class="chat-bubble from-them">'+response.content+'</div><br>'+
                    '<div style="font-size: 10px;margin-left: 40px;">{{ Carbon\Carbon::now()->format('Y-m-d H:i') }}</div></div>');
                    var $messages_w = $('#my-conversation');
                    $messages_w.scrollTop($messages_w[0].scrollHeight);
                }
            }
            else if (response.type == 'group') {
                if (response.connection_id == $('.activeGroupTab').attr('data-id')) {
                    $('#my-conversation').append('<div class="message clearfix" style="padding: 15px;">'+
                        '<div style="margin-left: 8px;background-color: antiquewhite;width: 230px;'+
                        'padding: 5px;padding-left: 15px;border-radius: 30px;">' + response.name + ' ' + response.surname + ' ' + response.middle_name +'</div>'+
                        '<div class="profile-img-wrapper m-t-5 inline">'+
                        '<img class="col-top" width="30" height="30" src=""  alt="" data-src=""'+
                        'data-src-retina=""></div>'+
                        '<div class="chat-bubble from-them">'+response.content+'</div><br>'+
                        '<div style="font-size: 10px;margin-left: 40px;">{{ Carbon\Carbon::now()->format('Y-m-d H:i') }}</div></div>');
                    var $messages_w = $('#my-conversation');
                    $messages_w.scrollTop($messages_w[0].scrollHeight);
                }
            }
        });


        $('[name="chat_enter_input"]').on('keypress', function (e) {
            if (e.which == 13) {
                addMessage();
            }
        });

        $('.sendMessageBtn').on('click',function () {
            addMessage();
        });



        clickChatOrGroup('[href="#quickview-chat"]','#quickview-chat','#quickview-notes','activeUserTab','activeGroupTab');
        clickChatOrGroup('[href="#quickview-notes"]','#quickview-notes','#quickview-chat','activeGroupTab','activeUserTab');

        chatOrGroupListing('#quickview-chat','activeUserTab','private');
        chatOrGroupListing('#quickview-notes','activeGroupTab','group');

        searchUserOrGroup('[name="search_user"]','#quickview-chat','activeUserTab','private');
        searchUserOrGroup('[name="search_group"]','#quickview-notes','activeGroupTab','group');


        function clickChatOrGroup(tab,tab_first_content,tab_second_content,activeFirstTab,activeSecondTab){
            $(tab).on('click', function () {
                $(tab_second_content).css('cssText', 'display:none !important');
                $(tab_first_content).css('cssText', 'display:block !important');
                $(tab_first_content + ' .chat-user-list').eq(0).click();
                $(tab_first_content + ' .' + activeFirstTab).removeClass(activeFirstTab);
                $(tab_second_content).find('.' + activeSecondTab +' .name-title').css('cssText', 'color : #626262 !important');
                $(tab_second_content).find('.' + activeSecondTab +' .status-title').css('cssText', 'color : #626262 !important');
                $(tab_second_content + ' .' + activeSecondTab).removeClass(activeSecondTab);
                $(tab_first_content + ' .chat-user-list').eq(0).addClass(activeFirstTab);
            });
        }

        function chatOrGroupListing(tab,activeTab,type){
            $(tab + ' .chat-user-list').each(function () {
                let li = $(this);
                li.on('click', function () {
                    if (li.hasClass(activeTab)) return;
                    pageLoading('show');
                    let sender_id = {{ auth()->user()->id }},
                        recivier_id = li.attr('data-id');
                    $(tab).find('.' + activeTab +' .name-title').css('cssText', 'color : #626262 !important');
                    $(tab).find('.' + activeTab +' .status-title').css('cssText', 'color : #626262 !important');
                    $(tab).find('.'+activeTab).removeClass(activeTab);
                    li.addClass(activeTab);
                    let title = li.find('.name-title').text();
                    li.find('.name-title').css('cssText', 'color : white !important');
                    li.find('.status-title').css('cssText', 'color : white !important');
                    let status = li.find('.status-title').text();
                    $('.message-name-title').text(title);
                    $('.message-status-title').text(status);
                    pageLoading('show');
                    $.post('{{ route('admin_chat_messages') }}', {
                        'sender_id': sender_id,
                        'recivier_id': recivier_id,
                        'type': type,
                        _token: _token
                    }, function (response) {
                        $('#my-conversation').html('');
                        $('#my-conversation').html(response.messages);
                        pageLoading();
                        var $messages_w = $('#my-conversation');
                        $messages_w.scrollTop($messages_w[0].scrollHeight);
                    });
                });
            });
        }

        // CHAT USER SEARCH START HERE
       function searchUserOrGroup(input,tab,activeTab,type){
           $(input).on('keyup', function () {
               let val = $(this).val();
               delay(function () {
                   pageLoading('show');
                   $.get('{{ route('admin_chat') }}', {
                       'searchedVal': val,
                       'type': 'ajax',
                       'searchType' : type
                   }, function (response) {
                       pageLoading();
                       if (response.status == "ok") {
                           $(tab + ' ul').html('');
                           $(tab + ' ul').html(response.data);
                           chatOrGroupListing(tab,activeTab,type);
                           $(tab + ' .chat-user-list').eq(0).click();
                       }
                   });
               }, 1000);
           });
       }

        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        // CHAT USER SEARCH END HERE

        function addMessage() {
            let message = $('[name="chat_enter_input"]');
            let sender_id = {{ auth()->user()->id }},
                rec_user_id = $('.activeUserTab').attr('data-id'),
                rec_group_id = $('.activeGroupTab').attr('data-id');
            console.log(rec_group_id);
            if (message.val().length < 1) return;

            $('#my-conversation').append('<div class="message clearfix">' +
                '                        <div class="chat-bubble from-me">' + message.val() +
                '                        </div><br>' +
                '                        <div style="float:right;font-size:11px;">' + '{{ Carbon\Carbon::now()->format('Y-m-d H:i') }}' + '</div>' +
                '                    </div>');
            if ($('[href="#quickview-notes"]').parents('li:eq(0)').hasClass('active')) {
                socket.emit('new message', {
                    'user_id': rec_group_id,
                    'sender_id': sender_id,
                    'message': message.val(),
                    'laravel_session': '{{ $_COOKIE["school_management_system_session"] }}',
                    'type': 'group'
                });
            }
            else if ($('[href="#quickview-chat"]').parents('li:eq(0)').hasClass('active')) {
                socket.emit('new message', {
                    'user_id': rec_user_id,
                    'sender_id': sender_id,
                    'message': message.val(),
                    'laravel_session': '{{ $_COOKIE["school_management_system_session"] }}',
                    'type': 'private'
                });
            }
            message.val("");
            var $messages_w = $('#my-conversation');
            $messages_w.scrollTop($messages_w[0].scrollHeight);
        }

        $('[href="#quickview-chat"]').click();
    </script>
@endsection