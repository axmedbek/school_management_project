@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-2 col-lg-1" style="float: right">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('advert_task',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('advert_task.add.modal',['task_id' => 0]) }}')" type="button" class="btn btn-success">
                                  <span class="p-t-5 p-b-5">
                                      <i class="pg-plus_circle fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Yeni</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover" id="task_table">
            <thead>
            <tr>
                <th>#</th>
                <th style="min-width: 140px;">Keçirilmə tarixi</th>
                <th style="min-width: 150px;">Adı</th>
                <th style="min-width: 150px;">Əhatəsi</th>
                <th>Məzmun</th>
                <th>Əlavə edən</th>
                <th>Link</th>
                <th style="min-width: 150px;">Fayl</th>
                <th style="width: 185px;">Əməliyyat</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr tr_id="{{ $task->id }}">
                    <td></td>
                    <td>{{ $task->date_of_event ? date('d-m-Y',strtotime($task->date_of_event))  : '-' }}</td>
                    <td>{{ $task->name }}</td>
                    <td style="min-width: 150px;">{{ $task->range }}</td>
                    <td style="word-break: break-all;">{{ $task->content }}</td>
                    <td user_id="{{ $task->user->id }}">{{ $task->user->name." ".$task->user->surname }}</td>
                    <td>{{ $task->link }}</td>
                    <td>
                        @if(isset($task->file) && file_exists(public_path().'/option_files/'.$task->file))
                            <a href="{{ asset('option_files/'.$task->file) }}" download><i
                                        class="fa fa-download"></i></a>
                        @endif
                    </td>
                    <td style="min-width: 180px;">
                        @if(\App\Library\Helper::has_priv('advert_task',\App\Library\Standarts::PRIV_CAN_EDIT))
                            <div class="btn-group-sm">
                                <a type="button" href="javascript:openModal('{{ route('advert_task.add.modal',['task_id' => $task->id]) }}')"
                                   class="btn btn-primary col-lg-4 col-md-12"><i class="fa fa-pencil"></i></a>
                                <a type="button" class="btn btn-danger btn-sm deleteTask"><i class="fa fa-trash-o"></i></a>
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">

        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets\plugins\bt_timepicker\bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/multi-select/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="{{ asset('assets\plugins\bt_timepicker\bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/multi-select/jquery.quicksearch.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/multi-select/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script>
        $('[data-toggle="saveTask"]').tooltip();

        $("#task_table>tbody").on("click", ".deleteTask", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post("{{ route('advert_task.delete') }}", {id: id, _token: _token}, function (response) {
                            if (response['status'] == 'error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                orderTable();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });

        orderTable();

        function orderTable() {
            $('#task_table>tbody>tr').each(function (i) {
                $(this).find('td:eq(0)').text(i + 1);
            });
        }
    </script>
@endsection
