<!DOCTYPE html>
<html lang="az">
<head>
    @include('header')
</head>
<body class="fixed-header {{ isset($_COOKIE['menuPin']) && $_COOKIE['menuPin'] == "true" ? 'menu-pin':'' }}">
<!-- BEGIN LOADING-->
<div class="text-center" style="position: fixed;z-index: 999999999;width: 100%;height: 100%;background-color: #c8c4c42e;display: none" id="page_loading">
    <div class="progress-circle-indeterminate m-t-45" style="display: block;">
    </div>
    <br>
    <p class="small hint-text" style="font-size: 20px;font-weight: bold;color: white;">Please wait...</p>
</div>
<!-- END LOADING-->
<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <div class="sidebar-header">
            <a class="side-educat-logo" href="{{ route('dashboard') }}"><code>Schoolary</code></a>
            <div class="sidebar-header-controls" style="visibility: hidden">
                <button type="button" class="btn btn-link hidden-md-down" data-toggle-pin="sidebar"><i class="fa fs-12" style="margin-bottom: 30px;"></i>
                </button>
            </div>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    @include('menu')
    <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->
<!-- START PAGE-CONTAINER -->
<div class="page-container ">
    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
            <!-- LEFT SIDE -->
            <div class="pull-left full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                    <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                        <span class="icon-set menu-hambuger"></span>
                    </a>
                </div>
                <!-- END ACTION BAR -->
            </div>
            <div class="pull-center hidden-md hidden-lg">
                <div class="header-inner">
                    <div class="brand inline">
                        <a class="side-educat-logo" href="{{ route('dashboard') }}"><code>Schoolary</code></a>
                    </div>
                </div>
            </div>
            <!-- RIGHT SIDE -->
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
            <div class="header-inner">
                <div class="brand inline">
                    Sch
                </div>
            </div>
        </div>
        <div class="pull-right">
            <!-- START User Info-->
            <div class="visible-lg visible-md m-t-10">
                <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
                    <span class="semi-bold">{{ Auth::user()->fullname() }}</span>
                </div>
                <div class="dropdown pull-right">
                    <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                    @if(file_exists(public_path().'/images/user/thumb/'.Auth::user()->thumb))
                        <img src="{{ asset(\App\Library\Standarts::$userThumbir . Auth::user()->thumb) }}" alt="" width="32" height="32">
                    @else
                        <img src="{{ asset(\App\Library\Standarts::$userThumbir) }}" alt="" width="32" height="32">
                    @endif
            </span>
                    </button>
                    <ul class="dropdown-menu profile-dropdown" role="menu">
                        <li class="bg-master-lighter">
                            <a href="{{ route('admin_logout') }}" class="clearfix">
                                <span class="pull-left">Logout</span>
                                <span class="pull-right"><i class="pg-power"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END User Info-->
        </div>
    </div>
    <!-- END HEADER -->
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
            <!-- START JUMBOTRON -->
            <div class="jumbotron" data-pages="parallax">
                <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                    <div class="inner">
                        <!-- START BREADCRUMB -->
                        <ul class="breadcrumb">
                            {!! \App\Library\Helper::makeLinker() !!}
                        </ul>
                        <!-- END BREADCRUMB -->
                    </div>
                </div>
            </div>
            <!-- END JUMBOTRON -->
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                @yield('container')
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
            <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text"> &copy; Schoolary {{ \Carbon\Carbon::now()->year }} </span><br>
                    <a href="https://schoolary.com/" class="font-montserrat" target="_blank" >www.schoolary.com</a>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- END COPYRIGHT -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->
@include('footer')
@include('modals.modal')

<script>
    $(document).ready(function (){
        $('button[data-toggle-pin="sidebar"]').trigger('click');
    })
</script>
</body>
</html>