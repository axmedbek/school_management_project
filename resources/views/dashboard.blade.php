@extends('base')

@section('container')
    <div class="row">
        @include('errors')
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="widget-8 panel no-border bg-success" style="background-color: #f9d302;">
                    <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                            <div class="col-xs-height col-top col-md-8">
                                <div class="row" style="margin-left: 4%;margin-top: 10%;">
                                    <div class="col-md-12">
                                        <div class="panel-title text-black hint-text">
                                            <span class="font-montserrat fs-11 all-caps" style="color: #74594e;font-weight: bold;font-size: 12px !important;">{{ __('pages/dashboard.cards.total_student') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h3 class="no-margin p-b-5 text-white" style="color: #74594e !important;font-size: 18px;">{{ $totalStudent }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="margin-top: 10%;margin-left: 15%;">
                                <img src="{{ asset('assets/img/dashboard/student.png') }}" alt="schoolary student icon" width="66">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget-8 panel no-border bg-success " style="background-color: #3bbea6;">
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="col-xs-height col-top col-md-8">
                            <div class="row" style="margin-left: 4%;margin-top: 10%;">
                                <div class="col-md-12">
                                    <div class="panel-title text-black hint-text">
                                        <span class="font-montserrat fs-11 all-caps" style="color: #295d66;font-weight: bold;font-size: 12px !important;">{{ __('pages/dashboard.cards.total_teacher') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h3 class="no-margin p-b-5 text-white" style="color : #295d66 !important;font-size: 18px;">{{ $totalTeacher }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 5%;margin-left: 15%;">
                            <img src="{{ asset('assets/img/dashboard/teacher.png') }}" alt="schoolary teacher icon" width="70">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-3">
                <div class="widget-8 panel no-border bg-success " style="background-color: #2f5a78;">
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="col-xs-height col-top col-md-8">
                            <div class="row" style="margin-left: 4%;margin-top: 10%;">
                                <div class="col-md-12">
                                    <div class="panel-title text-black hint-text">
                                        <span class="font-montserrat fs-11 all-caps" style="color: #f8ad60;font-weight: bold;font-size: 12px !important;">{{ __('pages/dashboard.cards.total_personal') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h3 class="no-margin p-b-5 text-white" style="color : #f8ad60 !important;font-size: 18px;">{{ $totalEmployee }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 5%;margin-left: 15%;">
                            <img src="{{ asset('assets/img/dashboard/staff.png') }}" alt="schoolary staff icon" width="70">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-3">
                <div class="widget-8 panel no-border bg-success " style="background-color: #f46a5a;">
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="col-xs-height col-top col-md-8">
                            <div class="row" style="margin-left: 4%;margin-top: 10%;">
                                <div class="col-md-12">
                                    <div class="panel-title text-black hint-text">
                                        <span class="font-montserrat fs-11 all-caps" style="color: #ffffff;font-weight: bold;font-size: 12px !important;">{{ __('pages/dashboard.cards.total_attented_students') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h3 class="no-margin p-b-5 text-white" style="color : #ffffff !important;font-size: 18px;">{{ $attendedStudents }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 5%;margin-left: 15%;">
                            <img src="{{ asset('assets/img/dashboard/attented_student.png') }}" alt="schoolary attented student icon" width="75">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 hidden-xlg m-b-10">
            <!-- START WIDGET widget_tableWidgetBasic-->
            <div class="widget-11-2 panel no-border panel-condensed no-margin widget-loader-circle">
                <div class="panel-heading top-right">
                    <div class="panel-controls">
                        <ul>
                            <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="padding-25">
                    <div class="pull-left">
                        <h2 class="text-success no-margin">{{ __('pages/dashboard.taks') }}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="auto-overflow widget-11-2-table">
                    <table class="table table-condensed table-hover">
                        <tbody>
                            @foreach(\App\Models\Duty::realData()->get() as $duty)
                                <tr tr_id="{{ $duty->id }}">
                                    <td class="font-montserrat all-caps fs-12 col-lg-6">{{ $duty->text }}</td>
                                    <td class="text-right b-r b-dashed b-grey col-lg-3">
                                        <span class="hint-text small">{{ $duty->comment }}</span>
                                    </td>
                                    <td class="col-lg-3">
                                        <a type="button" url="{{ route('duty_delete',['duty'=> $duty->id]) }}" class="btn btn-danger deleteAction col-lg-4 col-md-12"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .widget-8{
            height: 90px !important;
            //margin-right: 5px;
        }
        .hint-text {
            opacity: 1 !important;
        }

        .bg-success{
            border-radius: 5px !important;
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
@endsection