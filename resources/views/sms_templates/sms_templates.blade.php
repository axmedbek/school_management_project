@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-hover" id="basicTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Şablon adı</th>
                        <th></th>
                        <th style="width: 185px;">Əməlİyyat</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($smsTemplates as $smsTemplate)
                    <tr data-id="{{ $smsTemplate->id }}">
                        <td>1</td>
                        <td>{{ $smsTemplate->name }}</td>
                        <td style="width: 40px;">
                            {{--<label class="custom-control custom-checkbox" style="font-size: 10px;">--}}
                                {{--<span>{{ $smsTemplate->active == 0 ? 'Aktiv et' : 'Deaktiv et' }}</span>--}}
                                {{--<input type="checkbox" class="custom-control-input" name="isActiveSmsTemplate" {{ $smsTemplate->active == 0 ? 'checked' : '' }} value="{{ $smsTemplate->active == 0 ? 'off' : 'on' }}" style="display: none;">--}}
                                {{--<span class="custom-control-indicator"></span>--}}
                            {{--</label>--}}
                            <div class="inline">
                                <span>{{ $smsTemplate->active == 1 ? 'Aktiv' : 'Deaktiv' }}</span>
                                <input name="isActiveSmsTemplate" id="rtl-switch" type="checkbox" {{ $smsTemplate->active == 1 ? 'checked' : '' }}  data-size="small" data-init-plugin="switchery" data-switchery="true" style="display: none;">
                            </div>
                        </td>
                        <td>
                            <div class="btn-group-sm">
                                <a type="button" href="javascript:openModal('{{ route('sms_templates_edit_modal',['smsTemplateId' => $smsTemplate->id]) }}')" class="btn btn-primary col-lg-4 col-md-12"><i class="fa fa-pencil"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('script')

    <script>

        $("input[name='isActiveSmsTemplate']").next().on('click',function(){
            let isActive = $(this).prev('input'),
                active = isActive.is(":checked") ? 1 : 0,
                sms_template_id = $(this).closest('tr').attr('data-id'),
                formData = new FormData();

            formData.append('id',sms_template_id);
            formData.append('active',active);
            formData.append('_token',_token);

            //console.log(active);

            $.ajax({
                url: "{{ route('sms_template_do_deactive') }}",
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') {
                        location.reload();
                    }
                    else {
                        console.log(response);
                        var isActiveMsg = response.active == 0 ? 'Deaktiv' : 'Aktiv';
                        isActive.prev('span').text(isActiveMsg);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });

        {{--$("input[name='isActiveSmsTemplate']").on('click',function(){--}}
            {{--var isActiveSmsTemplate = $(this).closest('label').find('span:eq(0)');--}}
            {{--if($(this).val() == 'on' ){--}}

                {{--$(this).val('off');--}}
            {{--}--}}
            {{--else{--}}
                {{--$(this).val('on');--}}
            {{--}--}}

            {{--var active = $(this).val() == 'on' ? 1 : 0,--}}
                {{--sms_template_id = $(this).closest('tr').attr('data-id'),--}}
                {{--formData = new FormData();--}}


            {{--formData.append('id',sms_template_id);--}}
            {{--formData.append('active',active);--}}
            {{--formData.append('_token',_token);--}}

            {{--//console.log(active);--}}


            {{--$.ajax({--}}
                {{--url: "{{ route('sms_template_do_deactive') }}",--}}
                {{--type: "POST",--}}
                {{--data: formData,--}}
                {{--async: false,--}}
                {{--success: function (response) {--}}
                   {{--if(response['status'] == 'error') {--}}
                        {{--location.reload();--}}
                    {{--}--}}
                    {{--else {--}}
                        {{--var isActive = response.active == 1 ? 'Deaktiv et' : 'Aktiv et';--}}
                        {{--isActiveSmsTemplate.text(isActive);--}}

                    {{--}--}}
                {{--},--}}
                {{--cache: false,--}}
                {{--contentType: false,--}}
                {{--processData: false--}}
            {{--});--}}
        {{--});--}}
    </script>

@endsection
