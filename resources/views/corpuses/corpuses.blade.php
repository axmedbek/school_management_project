@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-2 col-lg-1" style="float: right">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('corpuses',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('corpuses_add_edit',['corpus' => 0]) }}')" type="button" class="btn btn-success">
                                  <span class="p-t-5 p-b-5">
                                      <i class="pg-plus_circle fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Yeni</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="gallery">
                @foreach($corpuses as $corpus)
                    <div class="gallery-item col-lg-3 col-md-4" data-width="1" data-height="1" style="background-color: #0000003b;">
                        <!-- START PREVIEW -->
                        <img src="{{ asset(\App\Library\Standarts::$corpusImageDir . $corpus->thumb) }}" alt="" class="image-responsive-height" onclick="openModal('{{ route('corpuses_info',['corpus' => $corpus->id]) }}')">
                        <!-- END PREVIEW -->
                        <!-- START ITEM OVERLAY DESCRIPTION -->
                        <div class="overlayer bottom-left full-width">
                            <div class="overlayer-wrapper item-info ">
                                <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                                    <div class="">
                                        <p class="pull-left bold text-white fs-14 p-t-10">{{ $corpus->name }}</p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="m-t-10" style="margin-top: 0;">
                                        <div class="pull-left m-t-10">
                                            @if(\App\Library\Helper::has_priv('corpuses',\App\Library\Standarts::PRIV_CAN_EDIT))
                                                <button onclick="openModal('{{ route('corpuses_add_edit',['corpus' => $corpus->id]) }}')" class="btn btn-primary btn-xs btn-mini bold fs-14" type="button"><i class="fa fa-edit"></i></button>
                                                @php
                                                    $letterClass = \App\Models\ClassLetter::realData()->where('corpus_id',$corpus->id)->first();
                                                    $rooms = \App\Models\Room::realData()->where('corpus_id',$corpus->id)->first();
                                                    $classTimes = App\Models\ClassTime::realData()->where('corpus_id',$corpus->id)->first();
                                                @endphp
                                                @if(!isset($letterClass) && !isset($rooms) && !isset($classTimes))
                                                    <button url="{{ route('corpuses_delete',['corpus' => $corpus->id]) }}" class="btn btn-danger btn-xs btn-mini bold fs-14 deleteAction" type="button"><i class="fa fa-trash"></i></button>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PRODUCT OVERLAY DESCRIPTION -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .gallery-item{
            padding: 0;
            margin: 10px;
        }
    </style>
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
@endsection