@extends('base')

@section('container')
    <div class="row">
            <form class="col-md-12" role="form">
                <div class="col-md-3 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Tədrİs İlİ</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="year">
                        @foreach(\App\Models\Year::realData()->where('active', 1)->orderBy('id')->get() as $year)
                            <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Korpus</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="corpus">
                        @foreach(\App\Models\Corpus::realData()->orderBy('id')->get() as $corpus)
                            <option value="{{ $corpus->id }}">{{ $corpus->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Sİnİf</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="class">
                        @foreach(\App\Models\MskClass::realData()->orderBy('order')->get() as $class)
                            <option value="{{ $class->id }}">{{ $class->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Sİnİf hərfİ</label>
                    <input class="full-width" data-placeholder="Sinif hərfi" id="select2ClassLetter">
                </div>
                <div class="col-md-2 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Qrup</label>
                    <input class="full-width" data-placeholder="Qrup" id="select2Group">
                </div>
            </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        $(".select2").select2();

        $("#year, #corpus, #class").on('change',function () {
            $("#select2ClassLetter").select2("val", null);
            $("#select2Group").select2("val", null);
        });

        $("#select2ClassLetter").select2({
            placeholder: "Sinif hərfi",
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('get_class_letters') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        year_id: $("#year").val(),
                        corpus_id: $("#corpus").val(),
                        class_id: $("#class").val(),
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on("change", function () {
            $("#select2Group").select2("val", null);
        });

        $("#select2Group").select2({
            placeholder: "Sinif hərfi",
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('get_letters_group') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        letter_id: $("#select2ClassLetter").val()
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on("change", function () {
            let group_id = $(this).val();

            if(group_id > 0) openPage('#page-inside', '{{ route("table_page") }}', {group_id: group_id});
        });
    </script>
@endsection