<script>
    var _token = "{{ csrf_token() }}";
</script>
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/boostrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-bez/jquery.bez.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('pages/js/pages.min.js') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('assets/js/scripts.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/socket.io.js') }}" type="text/javascript"></script>
<script>
    {{--var socket = io(location.hostname + ':2222', {--}}
    {{--    query: 'token={{ $_COOKIE["school_management_system_session"] }}&authType=web',--}}
    {{--    transports: ['websocket'],--}}
    {{--    reconnection: false,--}}
    {{--    secure: true--}}
    {{--});--}}

    {{--socket.on('connect', function () {--}}
    {{--});--}}

    {{--socket.on('disconnect', function () {--}}
    {{--});--}}
</script>
@yield('script')