@extends('base')

@section('container')
    <div class="row">
            <form class="col-md-12" role="form">
                <div class="col-md-4 form-group form-group-default form-group-default-select2">
                    <label class="">Tədris ili</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="year">
                        @foreach(\App\Models\Year::realData()->orderBy('id')->get() as $year)
                            <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . '-' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4 form-group form-group-default form-group-default-select2">
                    <label class="">Korpus</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="corpus">
                        @foreach(\App\Models\Corpus::realData()->orderBy('id')->get() as $corpus)
                            <option value="{{ $corpus->id }}">{{ $corpus->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4 form-group form-group-default form-group-default-select2">
                    <label class="">Sinif</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="class">
                        @foreach(\App\Models\MskClass::realData()->orderBy('order')->get() as $class)
                            <option value="{{ $class->id }}">{{ $class->name }}</option>
                        @endforeach
                    </select>
                </div>
            </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $(document).ready(function () {
            $(".select2").trigger('change');
        });


        $(".select2").select2().on('change', function () {
            let year_id = $("#year").val(),
                corpus_id = $("#corpus").val(),
                class_id = $("#class").val();

            openPage('#page-inside', '{{ route("class_groups_page") }}', {year_id: year_id, corpus_id: corpus_id, class_id: class_id});
        });

        
    </script>
@endsection