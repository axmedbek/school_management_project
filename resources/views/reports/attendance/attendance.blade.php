@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form" id="first_form">
            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;">
                <label>Tarix</label>
                <select name="date_type" id="date_type" class="form-control select2">
                    <option value="date">Günlük</option>
                    <option value="month">Aylıq</option>
                </select>
            </div>
            <div class="col-md-2 form-group form-group-default" id="date_input" style="height: 55px;">
                <label>Gün</label>
                <input type="text" class="input-sm form-control datepicker" value="{{ date("d-m-Y") }}" name="date">
            </div>
            <div class="col-md-2 form-group form-group-default" id="monthly_input" style="display: none">
                <label>Ay</label>
                <input type="text" class="form-control" id="month" value="{{ date('M-Y',strtotime(\Carbon\Carbon::today())) }}" aria-invalid="false">
            </div>

            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;">
                <label>Qruplar</label>
                <select name="groups" id="groups" class="form-control select2">
                    <option value="teacher">Müəllim</option>
                    <option value="staff">Personal</option>
                </select>
            </div>
            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;display: none;">
                <label>Heyətlər</label>
                <select name="heyetler" id="heyetler" class="form-control select2">
                    <option value="">Hamisi</option>
                    @foreach(\App\Models\MskHeyetler::realData()->get() as $heyet)
                        <option value="{{ $heyet->id }}">{{ $heyet->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button" data-toggle="dropdown" style="height: 53px;">Export
                            <span class="caret"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .btn-top-my{
            padding: 4px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $('select[name="groups"]').on('change',function(){
            if($(this).val() == "staff"){
                $('select[name="heyetler"]').parent('div').css('display','');
            }
            else{
                $('select[name="heyetler"]').parent('div').css('display','none');
            }
        });
        $("#date_type").change(function () {
            $("#date_input").hide();
            $("#monthly_input").hide();

            if($(this).val() == "date") $("#date_input").show();
            else $("#monthly_input").show();
        });
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        $('#month').datepicker({
            format: 'MM-yyyy',
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });
        $(".select2").select2({ allowClear: true, placeholder: 'Hamısı' });

        function getTable() {
            let data = {},
                fData = [],
                name = '';

            $("#filter_form [name],#first_form [name]").each(function () {
                name = $(this).attr('name');

                if($(this)[0].hasAttribute('multiple')){
                    data[name] = [];
                    fData = $(this).select2("val");
                    for(let n in fData)
                        data[name].push(fData[n]);
                }
                else if($(this).is("[type='radio']")){
                    if($(this).is(":checked")) data[name] = $(this).val();
                }
                else{
                    if($(this).val() != '') data[name] = $(this).val();
                }
            });

            data['month'] = $('#month').data('datepicker').getFormattedDate('dd-mm-yyyy');

            openPage('#page-inside', '{{ route("report_attendance_page") }}', data);
            $('.excel-export-btn').css('display','');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu">' +
                '            <li><a href="{{ route('report_attendance_export',["xlsx"]) }}">Excel</a></li>' +
                '            </ul>');
        }

        $.fn.datepicker.dates['en'] = {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["B", "B.E", "Ç.A", "Ç", "C.A", "C", "Ş"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: JSON.parse('{!! json_encode(\App\Library\Standarts::$months) !!}'),
            monthsShort: ["Yan", "Fev", "Mar", "Apr", "May", "Iyn", "Iyl", "Avq", "Sen", "Okt", "Noy", "Dek"],
            today: "Today",
            clear: "Clear",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };
    </script>

@endsection