@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form" id="first_form">
            <div class="col-md-2 form-group form-group-default">
                <label>Ay</label>
                <input type="text" class="form-control datepicker" value="{{ date('M-Y',strtotime(\Carbon\Carbon::today())) }}" name="month" aria-invalid="false">
            </div>
            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;">
                <label>Qruplar</label>
                <select name="groups" id="groups" class="form-control">
                    <option value="teacher">Müəllim</option>
                    <option value="staff">Personal</option>
                </select>
            </div>
            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;display: none;">
                <label>Heyətlər</label>
                <select name="heyetler" id="heyetler" class="form-control">
                    <option value="0">Hamisi</option>
                    @foreach(\App\Models\MskHeyetler::realData()->get() as $heyet)
                        <option value="{{ $heyet->id }}">{{ $heyet->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button" data-toggle="dropdown" style="height: 53px;">Export
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('report_tabel_export',["pdf"]) }}">PDF</a></li>
                            <li><a href="{{ route('report_tabel_export',["xlsx"]) }}">Excel</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .btn-top-my{
            padding: 4px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        {{--var currentDate = "{{ \App\Library\Standarts::$months[Carbon\Carbon::now()->month - 1]}}" + "-" + "{{ Carbon\Carbon::now()->year }}";--}}

        $('.datepicker').datepicker({
            format: 'MM-yyyy',
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });
        // $('.datepicker').val(currentDate);
         getTable({{ Carbon\Carbon::now()->month }});

        $('select[name="groups"]').on('change',function(){
            if($(this).val() == "staff"){
                $('select[name="heyetler"]').parent('div').css('display','');
            }
            else{
                $('select[name="heyetler"]').parent('div').css('display','none');
            }
        });

        $(".select2").select2({ allowClear: true, placeholder: 'Hamısı' });

        function getTable(ay = null) {
            let data = {},
                fData = [],
                name = '';

            $("#filter_form [name],#first_form [name]").each(function () {
                name = $(this).attr('name');

                if($(this)[0].hasAttribute('multiple')){
                    data[name] = [];
                    fData = $(this).select2("val");
                    for(let n in fData)
                        data[name].push(fData[n]);
                }
                else if($(this).is("[type='radio']")){
                    if($(this).is(":checked")) data[name] = $(this).val();
                }
                else if($(this).hasClass('datepicker')){
                    var date = $(this).datepicker('getDate');
                    var month = date.getMonth() + 1;
                    if(ay != null){
                        if (parseInt(ay) > 9) {
                            month = ay;
                        }
                        else{
                            month = "0"+ay;
                        }
                        data[name] = $(this).val().split("-")[1]+"-"+month+"-01";
                    }
                    else{
                        if (parseInt(date.getMonth()) < 9) month = "0"+month;
                        data[name] = $(this).val() != '' ? date.getFullYear() + "-" +month+"-01" : '';
                    }
                }
                else{
                    if($(this).val() != '') data[name] = $(this).val();
                }
            });

            openPage('#page-inside', '{{ route("report_tabel_page") }}', data);
            $('.excel-export-btn').css('display','');
        }

        $.fn.datepicker.dates['en'] = {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["B", "B.E", "Ç.A", "Ç", "C.A", "C", "Ş"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "İyun", "İyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
            monthsShort: ["Yan", "Fev", "Mar", "Apr", "May", "Iyn", "Iyl", "Avq", "Sen", "Okt", "Noy", "Dek"],
            today: "Today",
            clear: "Clear",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };
    </script>

@endsection