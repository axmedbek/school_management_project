@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form">
            <div class="col-md-3 col-sm-6 form-group form-group-default form-group-default-select2">
                <label class="">Tədris ili</label>
                <select class="full-width select2" data-placeholder="Sinif" id="year">
                    @foreach(\App\Models\Year::realData()->orderBy('id', 'desc')->get() as $year)
                        <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-9 col-sm-6 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                    <a href="javascript:filtr()" type="button" class="btn btn-primary btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Filtr</span>
                    </a>
                    {{--<a href="{{ route('report_staff_export') }}" type="button" class="btn btn-warning btn-top-my excel-export-btn" style="display: none">--}}
                    {{--<span class="p-t-5 p-b-5">--}}
                    {{--<i class="fa fa-file-excel-o fs-15"></i>--}}
                    {{--</span>--}}
                    {{--<br>--}}
                    {{--<span class="fs-11 font-montserrat text-uppercase">Export</span>--}}
                    {{--</a>--}}
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button"
                                data-toggle="dropdown" style="height: 53px;">Export
                            <span class="caret"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel panel-default" id="filtrs_div" style="display: none">
        <div class="panel-heading">
            <div class="panel-title">Filtirlər</div>
            <div class="tools">
                <a class="collapse" href="javascript:;"></a>
                <a class="config" data-toggle="modal" href="#grid-config"></a>
                <a class="reload" href="javascript:;"></a>
                <a class="remove" href="javascript:;"></a>
            </div>
        </div>
        <div class="panel-body">
            <form class="" role="form" id="filter_form">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Personal</label>
                            <select class="form-control select2" name="personal" multiple>
                                @foreach(\App\User::realData('staff')->get() as $data)
                                    <option value="{{ $data->id }}">{{ $data->fullname() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vəzifə</label>
                            <input type="text" name="position" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Yaş aralığı</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="min_age">
                                <span class="input-group-addon">-</span>
                                <input type="text" class="form-control" name="max_age">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Heyyət</label>
                            <select class="form-control select2" name="heyyet">
                                <option value="">Hamısı</option>
                                @foreach(\App\Models\MskHeyetler::realData()->get() as $data)
                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Cinsi</label>
                            <select class="form-control select2" name="gender">
                                <option value="">Hamısı</option>
                                <option value="m">Kişi</option>
                                <option value="f">Qadın</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Statusu</label>
                            <select class="form-control select2" name="status">
                                <option value="">Hamısı</option>
                                <option value="1">Aktiv</option>
                                <option value="0">Deaktiv</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Faktiki ünvanı Şəhər</label>
                            <select class="form-control select2" name="current_city" multiple>
                                @foreach(\App\Models\MskCities::realData()->get() as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Faktiki ünvan Rayon</label>
                            <select class="form-control select2" name="current_region" multiple>
                                @foreach(\App\Models\MskRegions::realData()->get() as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Qeydiyyat ünvanı Şəhər</label>
                            <select class="form-control select2" name="enrolled_city" multiple>
                                @foreach(\App\Models\MskCities::realData()->get() as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Qeydiyyat ünvanı Rayon</label>
                            <select class="form-control select2" name="enrolled_region" multiple>
                                @foreach(\App\Models\MskRegions::realData()->get() as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
    <style>
        .btn-top-my {
            padding: 4px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $(".select2").select2({allowClear: true, placeholder: 'Hamısı'});

        function filtr() {
            $("#filtrs_div").toggle('fast');
        }

        function getTable() {
            let data = {},
                fData = [],
                name = '';

            data['year_id'] = $("#year").val();

            $("#filter_form [name]").each(function () {
                name = $(this).attr('name');

                if ($(this)[0].hasAttribute('multiple')) {
                    data[name] = [];
                    fData = $(this).select2("val");
                    for (let n in fData)
                        data[name].push(fData[n]);
                }
                //else if()
                else {
                    if ($(this).val() != '') data[name] = $(this).val();
                }
            });

            openPage('#page-inside', '{{ route("report_staff_page") }}', data);
            $('.excel-export-btn').css('display', '');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu">' +
                '                <li><a href="{{ route('report_staff_export',["pdf"]) }}">PDF</a></li>' +
                '            <li><a href="{{ route('report_staff_export',["xlsx"]) }}">Excel</a></li>' +
                '            </ul>');


            $('.excel-export-btn').next('ul').find('li a').click(function (e) {
                    e.preventDefault();
                    let url = $(this).attr('href');
                    pageLoading('show');
                    $.get(url, {}, function (response) {
                        var a = document.createElement("a");
                        a.href = response.path;
                        a.download = response.filename;
                        document.body.appendChild(a);
                        a.click();
                        a.remove();
                        pageLoading('hide');
                    });
            });
        }
    </script>

@endsection