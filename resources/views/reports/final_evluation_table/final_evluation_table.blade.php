@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form" id="first_form">
            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;">
                <label>Tədris ili </label>
                <select class="form-control" name="years" id="years">
                    @foreach(\App\Models\Year::realData()->where('active', 1)->orderBy('id')->get() as $year)
                        <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2 form-group form-group-default" style="height: 55px;">
                <label>Sinif </label>
                <input type="text" name="classes" class="input-sm form-control">
            </div>
            <div class="col-md-2 form-group form-group-default">
                <label>Qrup </label>
                <select name="groups" id="groups" class="form-control select2">
                    <option value="1,0">Hamısı</option>
                    <option value="1" selected>Əsas</option>
                    <option value="0">Digər</option>
                </select>
            </div>

            <div class="col-md-2 form-group form-group-default" style="margin-left: 10px;">
                <label>Fənnlər</label>
                <input type="text" class="full-width" name="subjects" multiple>
            </div>
            <div class="col-md-3 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button" data-toggle="dropdown" style="height: 53px;">
                            <i class="fa fa-print"></i> <br> İxrac et
                            <span class="caret"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .btn-top-my{
            padding: 4px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $('.select2').select2();

        $('input[name="classes"]').select2({
            allowClear:true,
            placeholder:'Sinif seçin',
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_classes_for_final_evaluation_report',
                        year_id : $("#years").val(),
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            }
        });

        $('input[name="subjects"]').select2({
            allowClear:true,
            multiple : true,
            placeholder:'Fənn seçin',
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_subjects_for_final_evaluation_report',
                        year_id : $("#years").val(),
                        class_id : $('input[name="classes"]').val(),
                        group_id : $('#groups').val(),
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            }
        });

        function getTable() {
            let data = {},
                year_id = $('#years').val(),
                class_id = $('input[name="classes"]').val(),
                group_id = $('#groups').val(),
                subjects = $('input[name="subjects"]').val();

            data['year_id'] = year_id;
            data['group_id'] = group_id;
            data['class_id'] = class_id;
            data['subjects'] = subjects;

            openPage('#page-inside', '{{ route("reports_final_evaluation_table_page") }}', data);
            $('.excel-export-btn').css('display','');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu" style="margin-top: 3px;border: 1px solid #efe5b3;">' +
                '                <li><a style="color: green;" href="{{ route('report_final_evaluation_export',["xlsx"]) }}"><i class="fa fa-file-excel-o"></i> Excel</a></li>' +
                '            <li><a style="color: tomato;" href="{{ route('report_final_evaluation_export',["pdf"]) }}"><i class="fa fa-file-pdf-o"></i> PDF</a></li>' +
                '            </ul>');
        }
    </script>

@endsection