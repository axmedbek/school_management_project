@extends('base')

@section('container')
    <div class="row operation_row">
        <form class="col-md-12" role="form">
            <div class="col-md-3 col-sm-6 form-group form-group-default form-group-default-select2">
                <label class="">Tədris ili</label>
                <select class="full-width select2" data-placeholder="Sinif" id="year">
                    @foreach(\App\Models\Year::realData()->orderBy('id', 'desc')->get() as $year)
                        <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-9 col-sm-6 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                    <a href="javascript:filtr()" type="button" class="btn btn-primary btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Filtr</span>
                    </a>
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button" data-toggle="dropdown" style="height: 53px;">Export
                            <span class="caret"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel panel-default" id="filtrs_div" style="display: none">
        <div class="panel-heading">
            <div class="panel-title">Filtirlər</div>
            <div class="tools">
                <a class="collapse" href="javascript:;"></a>
                <a class="config" data-toggle="modal" href="#grid-config"></a>
                <a class="reload" href="javascript:;"></a>
                <a class="remove" href="javascript:;"></a>
            </div>
        </div>
        <div class="panel-body">
            <form class="" role="form" id="filter_form">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Şagird</label>
                            <select class="form-control select2" name="student" multiple>
                                @foreach(\App\User::realData('student')->withTrashed()->get() as $data)
                                    <option value="{{ $data->id }}">{{ $data->fullname() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Yaş aralığı</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="min_age">
                                <span class="input-group-addon">-</span>
                                <input type="text" class="form-control" name="max_age">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Doğulduğu ay</label>
                            <div class="form-group form-group-default">
                                <input type="text" class="form-control datepicker" name="month_in_birth" aria-invalid="false">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sinif</label>
                            <select class="form-control select2" name="class_names" multiple>
                                <option value="0">Bağlı olmayanlar</option>
                                @foreach(\App\Models\MskClass::realData()->orderBy('order','ASC')->get() as $data)
                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sinif Hərfi</label>
                            <input type="text" class="full-width" name="class_letter" multiple>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Təmayül</label>
                            <select class="form-control select2" name="temayul" multiple>
                                @foreach(\App\Models\GroupType::realData()->get() as $data)
                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Qrupu</label>
                            <input type="text" class="full-width" multiple name="qrupu">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Cinsi</label>
                            <select class="form-control select2" name="gender">
                                <option value="">Hamısı</option>
                                <option value="m">Kişi</option>
                                <option value="f">Qadın</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Statusu</label>
                            <select class="form-control select2" name="status">
                                <option value="">Hamısı</option>
                                <option value="1">Aktiv</option>
                                <option value="0">Deaktiv</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Faktiki ünvanı Şəhər</label>
                            <select class="form-control select2" name="current_city" multiple>
                                @foreach(\App\Models\MskCities::realData()->get() as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Faktiki ünvan Rayon</label>
                            <select class="form-control select2" name="current_region" multiple>
                                @foreach(\App\Models\MskRegions::realData()->get() as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Qeydiyyat ünvanı Şəhər</label>
                            <select class="form-control select2" name="enrolled_city" multiple>
                                @foreach(\App\Models\MskCities::realData()->get() as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Qeydiyyat ünvanı Rayon</label>
                            <select class="form-control select2" name="enrolled_region" multiple>
                                @foreach(\App\Models\MskRegions::realData()->get() as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .btn-top-my{
            padding: 4px 12px;
        }
        @media print{
            #filtrs_div,.select2-hidden-accessible,.breadcrumb,.operation_row,.copyright{
                display: none !important;
            }
            #page-inside,.titleOne,.titleTwo{
                display : block !important;
            }
            @page
            {
                /*size: landscape;*/
                margin: 0;
                /*size: 75cm 105cm;*/
                /*margin: 5mm 5mm 5mm 0;*/
            }

            #student_report_table tbody tr td,#student_report_table thead tr th{
                font-size: 10px;
                padding: 0;
            }
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $('input[name="month_in_birth"]').datepicker({
            autoclose:true,
            viewMode: "months",
            minViewMode: "months",
            format:'MM',
        });

        $('input[name="class_letter"]').select2({
            allowClear: true,
            multiple:true,
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_letter_for_student_report',
                        class_name: $('select[name="class_names"]').val(),
                        year_id: $("#year").val(),
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        });

        $('input[name="qrupu"]').select2({
            allowClear: true,
            placeholder:'Qrup',
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_groups_for_student_report',
                        temayul: $('select[name="temayul"]').val(),
                        class_letter: $('input[name="class_letter"]').select2('val'),
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        });

        $(".select2").select2({ allowClear: true, placeholder: 'Hamısı' });

        function filtr()
        {
            $("#filtrs_div").toggle('fast');
        }

        function getTable() {

            let data = {},
                fData = [],
                name = '';

            data['year_id'] = $("#year").val();

            $("#filter_form [name]").each(function () {
                name = $(this).attr('name');

                if($(this)[0].hasAttribute('multiple')){
                    data[name] = [];
                    fData = $(this).select2("val");
                    for(let n in fData)
                        data[name].push(fData[n]);
                }
                //else if()
                else{
                    if($(this).val() != '') data[name] = $(this).val();
                }
            });

            openPage('#page-inside', '{{ route("report_student_page") }}', data);
            $('.excel-export-btn').css('display','');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu" style="margin-top: 3px;border: 1px solid #efe5b3;">' +
                '                <li><a style="color: green;" href="{{ route('report_student_export',["xlsx"]) }}"><i class="fa fa-file-excel-o"></i> Excel</a></li>' +
                // '            <li><a style="color: tomato;cursor:pointer;" onclick="printToPrint()"><i class="fa fa-file-pdf-o"></i> PDF</a></li>' +
                '            </ul>');

            $('.excel-export-btn').next('ul').find('li:eq(0) a').click(function (e) {
                e.preventDefault();
                let url = $(this).attr('href');
                pageLoading('show');
                $.get(url, {}, function (response) {
                    var a = document.createElement("a");
                    a.href = response.path;
                    a.download = response.filename;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                    pageLoading('hide');
                });
            });

            // $.confirm({
            //     title: 'Məlumat',
            //     content: 'Bu əməliyyat bir qədər vaxt ala bilər.Zəhmət olmasa gözləyin.',
            //     type: 'yellow',
            //     typeAnimated: true,
            //     buttons: {
            //         formSubmit : {
            //             text:'OK',
            //             btnClass:'btn-warning',
            //             action:function(){
            //
            //             }
            //         },
            //     }
            // });
        }

        // onclick="printToPrint()"

        $.fn.datepicker.dates['en'] = {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["B", "B.E", "Ç.A", "Ç", "C.A", "C", "Ş"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "İyun", "İyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
            monthsShort: ["Yan", "Fev", "Mar", "Apr", "May", "Iyn", "Iyl", "Avq", "Sen", "Okt", "Noy", "Dek"],
            today: "Today",
            clear: "Clear",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        function printToPrint(){
            $('title').text('sagird_hesabati');
            window.print();
        }
    </script>

@endsection