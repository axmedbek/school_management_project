@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form" id="first_form">
            <div class="col-md-2 col-sm-2 form-group form-group-default form-group-default-select2">
                <label class="">Tarix</label>
                <select class="full-width select2" data-placeholder="Tarix" id="interval_type" name="interval_type">
                    <option value="interval">Interval</option>
                    <option value="monthly">Aylıq</option>
                </select>
            </div>
            <div class="col-md-3 col-sm-4 form-group" id="interval_input">
                <div class="input-daterange input-group" id="datepicker-range">
                    <input type="text" class="input-sm form-control" value="{{ date("01-m-Y") }}" name="start_date">
                    <span class="input-group-addon">to</span>
                    <input type="text" class="input-sm form-control" value="{{ (new DateTime('today'))->format("d-m-Y") }}" name="end_date">
                </div>
            </div>
            <div class="col-md-3 col-sm-4 form-group" id="monthly_input" style="display: none">
                <select class="full-width select2" data-placeholder="Tarix" name="month">
                    @foreach(\App\Library\Standarts::$months as $key => $month)
                        <option value="{{ $key + 1 }}">{{ $month }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-7 col-sm-6 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                    <a href="javascript:filtr()" type="button" class="btn btn-primary btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Qruplar</span>
                    </a>
                    <!--<a href="" type="button" class="btn btn-warning btn-top-my excel-export-btn" style="display: none">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-file-excel-o fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Export</span>
                    </a>-->
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button" data-toggle="dropdown" style="height: 53px;">Export
                            <span class="caret"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel panel-default" id="filtrs_div" style="display: none">
        <div class="panel-heading">
            <div class="panel-title">Qruplar</div>
            <div class="tools">
                <a class="collapse" href="javascript:;"></a>
                <a class="config" data-toggle="modal" href="#grid-config"></a>
                <a class="reload" href="javascript:;"></a>
                <a class="remove" href="javascript:;"></a>
            </div>
        </div>
        <div class="panel-body">
            <form class="" role="form" id="filter_form">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio radio-success">
                            <input type="radio" value="status" checked name="group" id="id_status">
                            <label for="id_status">Status</label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" value="heyet" name="group" id="id_heyet">
                            <label for="id_heyet">Heyyət</label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" value="age" name="group" id="id_age">
                            <label for="id_age">Yaş aralığı</label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" value="gender" name="group" id="id_gender">
                            <label for="id_gender">Cinsi</label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" value="current_region" name="group" id="id_current_region">
                            <label for="id_current_region">Faktiki rayon</label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" value="lived_region" name="group" id="id_lived_region">
                            <label for="id_lived_region">Qeydiyyat rayon</label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .btn-top-my{
            padding: 4px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        $("#datepicker-range").datepicker({format: 'dd-mm-yyyy'});

        $(".select2").select2({ allowClear: true, placeholder: 'Hamısı' });

        $("#interval_type").change(function () {
            $("#interval_input").hide();
            $("#monthly_input").hide();

            if($(this).val() == "interval") $("#interval_input").show();
            else $("#monthly_input").show();
        });

        function filtr()
        {
            $("#filtrs_div").toggle('fast');
        }

        function getTable() {
            let data = {},
                fData = [],
                name = '';

            $("#filter_form [name],#first_form [name]").each(function () {
                name = $(this).attr('name');

                if($(this)[0].hasAttribute('multiple')){
                    data[name] = [];
                    fData = $(this).select2("val");
                    for(let n in fData)
                        data[name].push(fData[n]);
                }
                else if($(this).is("[type='radio']")){
                    if($(this).is(":checked")) data[name] = $(this).val();
                }
                else{
                    if($(this).val() != '') data[name] = $(this).val();
                }
            });

            openPage('#page-inside', '{{ route("report_staff_statistic_page") }}', data);
            $('.excel-export-btn').css('display','');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu">' +
                '                <li><a href="{{ route('report_staff_statistic_export',["pdf"]) }}">PDF</a></li>' +
                '            <li><a href="{{ route('report_staff_statistic_export',["xlsx"]) }}">Excel</a></li>' +
                '            </ul>');
        }
    </script>

@endsection