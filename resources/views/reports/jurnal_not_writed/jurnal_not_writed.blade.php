@extends('base')

@section('container')
    <form class="operation_form">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-4 form-group form-group-default text-center">
                    <label style="width: 100%">Tarix</label>
                    <input class="form-control text-center" id="date">
                </div>
                {{--<div class="col-md-3 form-group form-group-default form-group-default-select2">--}}
                    {{--<label style="width: 100%">Müəllim</label>--}}
                    {{--<input class="full-width" data-placeholder="Müəllim" id="teachers">--}}
                {{--</div>--}}
                {{--<div class="col-md-3 form-group form-group-default form-group-default-select2 text-center">--}}
                    {{--<label style="width: 100%">Sİnİf</label>--}}
                    {{--<input class="full-width" data-placeholder="Sinif" id="class">--}}
                {{--</div>--}}
                {{--<div class="col-md-2 form-group form-group-default form-group-default-select2 text-center">--}}
                    {{--<label style="width: 100%">Fənn</label>--}}
                    {{--<input class="full-width" id="subject">--}}
                {{--</div>--}}
                <div class="col-md-8 form-group">
                    <div class="btn-group">
                        <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my" style="height: 53px;">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <div class="dropdown">
                            <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button"
                                    data-toggle="dropdown" style="height: 53px;">
                                <i class="fa fa-print"></i> <br> İxrac et
                                <span class="caret"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="col-md-4 table-errors alert alert-danger alert-dismissable" style="display: none;">
        <span>Zehmet olmasa düzgün format daxil edin</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-right: 10px">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="data-inside" style="margin-top: 50px;">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
    <style>
        #date-sel {
            display: none;
        }
        .select2-search-choice-close{
            margin-top: 12px;
        }

        @media print{

            .operation_form {
                display: none;
            }

            #jurnal_filter_head tr th{
                display: none !important;
            }
            .breadcrumb{
                display: none !important;
            }
            #jurnal_title {
                display: block !important;
                font-size: 14px !important;
                position: absolute;
                top: 25px !important;
            }
            .select2-hidden-accessible{
                display: none !important;
            }

            .copyright{
                display: none;
            }

            #jurnal_not_writed_table thead tr th {
                font-size: 10px;
                padding: 0;
                border: 1px solid grey;
            }

            #jurnal_not_writed_table tbody tr td {
                font-size: 10px;
                padding: 0;
                border: 1px solid grey;
            }

            @page
            {
                margin-top: 0;
                margin-bottom: 0;
            }
            body {
                margin-top: 1.6cm;
                margin-bottom: 1.6cm;
                margin-left: 50px;
                margin-right: 50px;
            }

        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        //        $("#year").select2().on("change", function () {
        //            $("#class").select2("val", null).trigger("change");
        //        });
        // $('#date').on('changeDate',function(){
        //     getTable();
        // });

        $("#teachers").select2({
            allowClear: true,
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        ne: 'getTeachers'
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        }).on("change", function () {
            $("#class").select2("val", null);
            getTable();
        });

        $("#class").select2({
            allowClear: true,
            ajax: {
                url: '{{ route('reports_jurnal_not_writed_year_classes') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        teacher_id: $("#teachers").val()
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        }).on("change", function () {
            $("#subject").select2("val", null);
            getTable();
        });


        $("#subject").select2({
            allowClear: true,
            placeholder: 'Fənn seçin',
            ajax: {
                url: '{{ route('reports_jurnal_not_writed_class_subjects') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        letter_group_id: $("#class").val(),
                        teacher_id: $("#teachers").val()
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        }).on("change", function () {
            getTable();
        });

        function getTable() {
            let data = {},
                date                 = $('#date').data('datepicker').getFormattedDate('yyyy-mm-dd');
                teacher              = $('#teachers').val(),
                letter_group_id      = $('#class').val(),
                subject_id           = $('#subject').val(),
                filter_date          = $('[name="filter_date"]').val();




            if (teacher > 0) {
                let teacher_name         = $('#teachers').select2('data').text;
                data['teacher_name'] = teacher_name;
            }

            if (letter_group_id > 0) {
                let letter_group_name    = $('#class').select2('data').text;
                data['letter_group_name'] = letter_group_name;
            }

            if (subject_id > 0) {
                let subject_name         = $('#subject').select2('data').text;
                data['subject_name'] = subject_name;
            }


            data['date'] = date;
            data['teacher'] = teacher;
            data['letter_group_id'] = letter_group_id;
            data['subject_id'] = subject_id;
            data['filter_date'] = filter_date;


            openPage('#data-inside', '{{ route("reports_jurnal_not_writed_table_page") }}', data);
            $('.excel-export-btn').css('display', '');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu" style="margin-top: 3px;border: 1px solid #efe5b3;">' +
                '                <li><a style="color: green;" href="{{ route('reports_jurnal_not_writed_table_export',["xlsx"]) }}"><i class="fa fa-file-excel-o"></i> Excel</a></li>' +
                '            <li><a style="color: tomato;cursor:pointer;" onclick="printToPrint()"><i class="fa fa-file-pdf-o"></i> PDF</a></li>' +
                '            </ul>');
        }

        $.fn.datepicker.dates.en.months = {!! json_encode(\App\Library\Standarts::$months) !!};

        $('#date').datepicker({
            format: 'MM / yyyy',
            viewMode: "months",
            minViewMode: "months",
            allowClear: false,
            autoclose: true,
        });

        $('#date').datepicker('update', new Date());

        function printToPrint(){
            $('title').text('jurnal_yazilmamasi_hesabati');
            window.print();
        }

    </script>
@endsection