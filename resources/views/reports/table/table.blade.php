@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form">
            <div class="col-md-3 form-group form-group-default form-group-default-select2">
                <label class="">Tədrİs İlİ</label>
                <select class="full-width select2" data-placeholder="Sinif" id="year">
                    @foreach(\App\Models\Year::realData()->where('active',1)->orderBy('id')->get() as $year)
                        <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 form-group form-group-default form-group-default-select2">
                <label class="">Korpus</label>
                <select class="full-width select2" data-placeholder="Sinif" id="corpus">
                    @foreach(\App\Models\Corpus::realData()->orderBy('id')->get() as $corpus)
                        <option value="{{ $corpus->id }}">{{ $corpus->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2 form-group form-group-default form-group-default-select2">
                <label class="">Sİnİf</label>
                <input class="full-width" type="text" data-placeholder="Sinif" name="msk_class[]" multiple>
            </div>
            <div class="col-md-2 form-group form-group-default form-group-default-select2">
                <label class="">Sİnİf hərfİ</label>
                <input class="full-width" data-placeholder="Sinif hərfi" name="class_letter[]" multiple>
            </div>
            <div class="col-md-2 form-group form-group-default form-group-default-select2">
                <label class="">Qrup</label>
                <input class="full-width" data-placeholder="Qrup" name="group[]" multiple>
            </div>
        </form>
    </div>
    <div id="page-inside">
        <div class="panel bg-white">
            <div class="panel-heading">
                <div class="btn-group pull-right m-b-10">
                    <div class="col-md-3 col-lg-2" style="float: right">
                        <div class="btn-group btn-group-justified">
                            <div class="btn-group excel-export-btn" style="display: {{ empty(Illuminate\Support\Facades\Cache::get('ders_cedveli')['groups']) == null ? 'none' : '' }};">
                                <a href="javascript:void(0)" type="button" class="btn btn-warning">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-file-excel-o fs-15"></i>
                              </span>
                                    <br>
                                    <span class="fs-11 font-montserrat text-uppercase">Export</span>
                                </a>
                            </div>
                            <div class="btn-group">
                                <a href="javascript:getTable()" type="button" class="btn btn-success">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-check-circle fs-15"></i>
                              </span>
                                    <br>
                                    <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body" id="page-inside-body">
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .week-day-th{
            text-align: center;
            background: #d6c7c7;
        }
        .panel-title a{
            font-weight: bold !important;
        }
        .panel-default{
            background: #f5f5f5;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $('.excel-export-btn').on('click',function(e){
            pageLoading('show');
            e.preventDefault();
            $.get('{{ route('all_data_export_to_excel') }}',{},function(response){
                var a = document.createElement("a");
                a.href = response.path;
                a.download = response.filename;
                document.body.appendChild(a);
                a.click();
                a.remove();
                pageLoading('hide');
            });
        });

        $(".select2").select2();

        $("#year, #corpus").change(function () {
            $('input[name="msk_class[]"]').select2("val", null).trigger("change");
        });

        $('input[name="msk_class[]"]').select2({
            multiple:true,
            placeholder: "Sinif",
            allowClear:true,
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('reports_table_get_class') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        ne : "getClasses",
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on('change',function(){
            $('input[name="class_letter[]"]').select2("val", null);
            $('input[name="group[]"]').select2("val", null);
        });

        $('input[name="class_letter[]"]').select2({
            multiple:true,
            placeholder: "Sinif hərfi",
            allowClear:true,
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('reports_table_get_class') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        ne : "getClassLetters",
                        classes : $('input[name="msk_class[]"]').val(),
                        year_id: $("#year").val(),
                        corpus_id: $("#corpus").val()
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on('change',function(){
            $('input[name="group[]"]').select2("val", null);
        });

        $('input[name="group[]"]').select2({
            multiple:true,
            placeholder: "Qrup",
            allowClear:true,
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('reports_table_get_class') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        ne : "getGroups",
                        letters : $('input[name="class_letter[]"]').val(),
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        });

        function getTable() {
            let data = {};

            data['year_id'] = $("#year").val();
            data['corpus_id'] = $("#corpus").val();

            data['class_id'] = [];
            let fData = $('input[name="msk_class[]"]').select2("val");
            for(let n in fData)
                data['class_id'].push(fData[n]);

            data['letter_id'] = [];
            fData = $('input[name="class_letter[]"]').select2("val");
            for(let n in fData)
                data['letter_id'].push(fData[n]);

            data['group_id'] = [];
            fData = $('input[name="group[]"]').select2("val");
            for(let n in fData)
                data['group_id'].push(fData[n]);
            openPage('#page-inside-body', '{{ route("report_table_page") }}', data);
            $('.excel-export-btn').css('display','');
        }
    </script>

@endsection