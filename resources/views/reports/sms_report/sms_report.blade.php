@extends('base')

@section('container')
    <div class="row">
        <form class="col-md-12" role="form" id="first_form">
            <div class="col-md-2 form-group form-group-default" style="height: 55px;">
                <label>Sinif </label>
                <input type="text" name="classes" class="input-sm form-control">
            </div>
            <div class="col-md-3 form-group form-group-default" style="height: 55px;">
                <label>Sinif hərfi </label>
                <input type="text" name="class_letters" class="input-sm form-control">
            </div>
            <div class="col-md-2 form-group form-group-default" style="height: 55px;">
                <label>Tarix </label>
                <input type="text" name="sms_date" class="input-sm form-control" placeholder="SMS tarixi" value="{{ Carbon\Carbon::now()->format('d-m-Y') }}">
            </div>
            <div class="col-md-3 form-group">
                <div class="btn-group">
                    <a href="javascript:getTable()" type="button" class="btn btn-success btn-top-my">
                        <span class="p-t-5 p-b-5">
                            <i class="fa fa-check-circle fs-15"></i>
                        </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Göstər</span>
                    </a>
                </div>
                <div class="btn-group">
                    <div class="dropdown">
                        <button class="btn btn-warning btn-top-my excel-export-btn dropdown-toggle" type="button" data-toggle="dropdown" style="height: 53px;">
                            <i class="fa fa-print"></i> <br> İxrac et
                            <span class="caret"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .btn-top-my{
            padding: 4px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $('input[name="sms_date"]').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });

        $('.select2').select2();

        $('input[name="classes"]').select2({
            allowClear:true,
            placeholder:'Sinif seçin',
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_classes',
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            }
        }).on('change',function(){
            $('input[name="class_letters"]').select2('val',null);
        });

        $('input[name="class_letters"]').select2({
            allowClear:true,
            placeholder:'Sinif hərfi seçin',
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_class_letters',
                        class_id : $('input[name="classes"]').val(),
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            }
        });

        function getTable() {
            let data = {},
                class_letter_id = $('input[name="class_letters"]').val(),
                sms_date = $('input[name="sms_date"]').val();

            data['class_letter_id'] = class_letter_id;
            data['sms_date'] = sms_date;

            openPage('#page-inside', '{{ route("reports_sms_table_page") }}', data);
            $('.excel-export-btn').css('display','');
            $('.excel-export-btn').nextAll().remove();
            $('.excel-export-btn').after('<ul class="dropdown-menu" style="margin-top: 3px;border: 1px solid #efe5b3;">' +
                '                <li><a style="color: green;" href="{{ route('reports_sms_table_export',["xlsx"]) }}"><i class="fa fa-file-excel-o"></i> Excel</a></li>' +
                '            <li><a style="color: tomato;" href="{{ route('reports_sms_table_export',["pdf"]) }}"><i class="fa fa-file-pdf-o"></i> PDF</a></li>' +
                '            </ul>');
        }
    </script>

@endsection