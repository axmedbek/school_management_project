@extends('base')

@section('container')
    <div class="col-lg-8 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_subjects',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif
        <table class="table table-hover" id="basicTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Fənn</th>
                <th colspan="2" style="min-width: 300px;"></th>
                <th style="width: 185px;">Əməliyyat</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subjects as $subject)
                <tr tr_id="{{ $subject->id }}" data-date-of-use="{{ $subject->date_of_use }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $subject->name }}</td>
                    <td></td>
                    <td></td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_subjects',\App\Library\Standarts::PRIV_CAN_EDIT))
                            <div class="btn-group-sm">
                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                @php $checkSubjectHasInLessons = \App\Models\Lesson::realData()->where('subject_id',$subject->id)->get() @endphp
                                @if(!isset($checkSubjectHasInLessons[0]))
                                   <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>


    <script>
        $(".add_new").click(function () {
            let tbody = $("tbody");

            tbody.prepend('<tr tr_id="0" data-date-of-use="">' +
                '<td></td>' +
                '<td><input type="text" maxlength="255" required name="name" class="form-control"></td>' +
                '<td>' +
                    '<label class="custom-control custom-checkbox" style="display: none;">' +
                         '<input type="checkbox" class="custom-control-input" name="checkDateOfUse" value="0"  style="display: none;"> ' +
                         '<span class="custom-control-indicator"></span> ' +
                    '</label>' +
                '</td>'+
                '<td>' +
                    '<div class="form-group" style="display:none;">' +
                        '<input type="text" name="dateOfUse" class="form-control datepicker" required="">' +
                    '</div>' +
                '</td>'+
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();
        });

        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                dateOfUse = tr.find('[name="dateOfUse"]').val(),
                name = tr.find('[name="name"]').val(),
                checkDateOfUse = tr.find('[name="checkDateOfUse"]').val(),
                data = {},
                id = tr.attr("tr_id");

            if(isValid(tr)){
                data['id'] = id;
                data['name'] = name;
                data['dateOfUse'] = dateOfUse;
                data['checkDateOfUse'] = checkDateOfUse ;
                data['_token'] = _token;

                $.post( "{{ route('msk_subjects_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });
        //Qüvvəyə minmə tarixi seç

        $("tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                dateOfUse = tr.attr('data-date-of-use') ,
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="255" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +
                '<td>' +
                    '<label class="custom-control custom-checkbox">' +
                        '<input type="checkbox" class="custom-control-input" name="checkDateOfUse" value="'+ (dateOfUse != "" ? 1 : 0)+'"  style="display: none;"> ' +
                        '<span class="custom-control-indicator"></span> ' +
                    '</label>' +
                '</td>'+
                '<td>' +
                    '<div class="form-group" style="display:none;">' +
                        '<input type="text" name="dateOfUse" class="form-control datepicker" required="">' +
                    '</div>' +
                '</td>'+
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            if (dateOfUse != "") {
                tr.find('[name="dateOfUse"]').parent().show();
                tr.find('[name="dateOfUse"]').datepicker({
                    format : 'yyyy-mm-dd',
                    autoclose : true
                });
                tr.find('[name="dateOfUse"]').val(dateOfUse);
                tr.find('[name="checkDateOfUse"]').attr('checked','checked');
            }
            tr.parents('table:eq(0)').find('th:eq(2)').text("Qüvvəyə minmə tarixi");
            tr.find("[name='checkDateOfUse']").on('click',function(){
                if ($(this).val() == '0') {
                    $(this).val(1);
                    $(this).parents('tr:eq(0)').find('[name="dateOfUse"]').parent().show();
                }
                else {
                    $(this).val(0);
                    $(this).parents('tr:eq(0)').find('[name="dateOfUse"]').parent().hide();
                }
            });

            tr.find("[name='dateOfUse']").datepicker({
                autoclose : true,
                format : 'dd-mm-yyyy',
            });



            tr.find(".cancel").click(function () {
                tr.html(html);
                tr.parents('table:eq(0)').find('th:eq(2)').text("");
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post( "{{ route('msk_subjects_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });

        function isValid(tr) {
            let name = tr.find("[name='name']"),
                dateOfUse = tr.find("[name='dateOfUse']"),
                status = true;

            name.css("border", "");
            dateOfUse.css("border","");

            if(name.val().trim().length > 255 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }
            if(dateOfUse.parent().css('display') != 'none'){
                if (dateOfUse.val().trim().length > 255 || dateOfUse.val().trim().length == 0)
                {
                    dateOfUse.css("border", "1px dotted red");
                    status = false;
                }
            }

            return status;
        }

        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                    '<label class="custom-control custom-checkbox" style="display: none;">' +
                        '<input type="checkbox" class="custom-control-input" name="checkDateOfUse"  style="display: none;"> ' +
                        '<span class="custom-control-indicator"></span> ' +
                    '</label>' +
                '</td>'+
                '<td>' +
                    '<div class="form-group" style="display:none;">' +
                        '<input type="text" name="dateOfUse" class="form-control datepicker" required="">' +
                    '</div>' +
                '</td>'+
                '<td>' +
                '<div class="btn-group-sm">' + data['buttons'] +
                '</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            tr.attr("data-date-of-use",!data['date_of_use'] ? "" : data['date_of_use'] );
            tr.parents('table:eq(0)').find('th:eq(2)').text("");
            order();
        }

        function order() {
            $("#basicTable>tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }
    </script>
@endsection