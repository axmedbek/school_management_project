@extends('base')

@section('container')
    <style>
        .activeTr {
            color: white;
            background-color: rgba(62, 112, 170, 0.59) !important;
        }

        .loader{
            position: absolute;
            height: 100%;
            width: 100%;
            z-index: 9999;
        }

        table>tbody>tr> {
            cursor: pointer;
        }
    </style>
    <div class="col-lg-6 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_mark_types',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new_type"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif
        <table class="table" id="mark_types_table">
            <thead>
            <tr>
                <th>#</th>
                <th>TİP</th>
                <th style="width: 185px;">Əməliyyat</th>
            </tr>
            </thead>
            <tbody>
            @foreach($mark_types as $mark_type)
                <tr tr_id="{{ $mark_type->id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $mark_type->name }}</td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_mark_types',\App\Library\Standarts::PRIV_CAN_EDIT))

                            <div class="btn-group-sm">
                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-lg-6 col-md-12" style="display: none;">
        <div>
            <img src="{{ asset('images/loader.gif') }}" alt="" style="width: 60px;height: 60px;margin-left: 280px;margin-top: 160px;">
        </div>
        <div>
            <div id="errors" class="row">

            </div>
            @if(\App\Library\Helper::has_priv('msk_mark_types',\App\Library\Standarts::PRIV_CAN_EDIT))
                <button type="button" class="btn btn-success pull-right add_new_marks"><i class="pg-plus_circle"> </i> Yeni</button>
            @endif
            <table class="table table-hover" id="marks_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>QİYMƏTLƏR</th>
                    <th style="width: 185px;">Əməliyyat</th>
                </tr>
                </thead>
                <tbody>
                @foreach($marks as $mark)
                    <tr tr_id="{{ $mark->id }}">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $mark->name }}</td>
                        <td>
                            @if(\App\Library\Helper::has_priv('msk_mark_types',\App\Library\Standarts::PRIV_CAN_EDIT))

                                <div class="btn-group-sm">
                                    <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                    <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                </div>
                            @else
                                <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js" type="text/javascript"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>


    <script>

        $('[data-toggle="save"]').tooltip();

        $(".add_new_type").click(function () {
            let tbody = $("#mark_types_table>tbody");

            tbody.prepend('<tr tr_id="0">' +
                '<td></td>' +
                '<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order("#mark_types_table");
            });
            order("#mark_types_table");
        });

        $("#mark_types_table>tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                id = tr.attr("tr_id");

            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['_token'] = _token;

                $.post( "{{ route('msk_mark_types_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("#mark_types_table>tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="30" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            tr.find(".cancel").click(function () {
                tr.html(html);
                order("#mark_types_table");
            });
            order("#mark_types_table");
        });

        $("#mark_types_table>tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsiniz?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit : {
                        text : "Sil",
                        btnClass : 'btn-green',
                        action:function(){
                            $.post( "{{ route('msk_mark_types_delete') }}", { id: id, _token: _token }, function( response ) {
                                if(response['status']=='error') $("#errors").html(response['errors']);
                                else {
                                    tr.remove();
                                    $("#mark_types_table>tbody").find("tr:first").trigger('click');
                                    order('#mark_types_table');
                                }
                            });
                        }

                    },
                    formCancel:{
                        text : "İmtina et",
                        btnClass : 'btn-red',
                        action:function() {

                        }

                    }
                }
            });
        });

        function isValid(tr) {
            let name = tr.find("[name='name']"),
                status = true;

            name.css("border", "");

            if(name.val().trim().length > 60 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                '<div class="btn-group-sm">' +
                '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>' +
                '<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            order("#mark_types_table");
        }

        // regions


        $(".add_new_marks").click(function () {
            let tbody = $("#marks_table>tbody"),
                data_mark_type_id = $("#mark_types_table").find('.activeTr').attr('tr_id');

            tbody.prepend('<tr tr-id="0" data-mark-type-id="'+data_mark_type_id+'">' +
                '<td></td>' +
                '<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 saveMark" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancelMark"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancelMark").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order("#marks_table");
            });
            order("#marks_table");
        });



        $("#mark_types_table>tbody").on('click','tr[tr_id!=0]',function () {
            $("#marks_table").parent('div').parent('div').show();
            $("#mark_types_table").find('.activeTr').removeClass('activeTr');
            $(this).addClass('activeTr');
            let mark_type_id = $(this).attr('tr_id');
            loaderDisplay(1);
            $.post("{{ route('msk_mark_types_get_marks') }}",{'mark_type_id':mark_type_id,'_token':_token},function (response) {
                if(response.status == 'ok'){
                    loaderDisplay (0);
                    $("#marks_table>tbody").html("");
                    _.forEach(response.data,function (val,key) {

                        $("#marks_table>tbody").append(
                            '<tr tr-id="'+val.id+'" data-mark-type-id="'+val.mark_type_id+'">' +
                            '<td></td>'+
                            '<td>'+val.name+'</td>' +
                            '<td>' +
                            '<div class="btn-group-sm">' +
                            '<a type="button" class="btn btn-primary col-lg-4 col-md-12 editMark"><i class="fa fa-pencil"></i></a>' +
                            '<a type="button" class="btn btn-danger col-lg-4 col-md-12 deleteMark"><i class="fa fa-trash-o"></i></a>' +
                            '</div>' +
                            '</td>'+
                            '</tr>');
                    });
                }
                order('#marks_table');
            });
        });

        $("#mark_types_table>tbody").find("tr:first").trigger('click');

        $("#marks_table>tbody").on("click", ".saveMark", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                id = tr.attr("tr-id"),
                mark_type_id = tr.attr("data-mark-type-id");


            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['mark_type_id'] = mark_type_id;
                data['_token'] = _token;

                $.post( "{{ route('msk_marks_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveMarkTr(tr, response['data']);
                });
            }
        });

        function saveMarkTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                '<div class="btn-group-sm">' +
                '<a type="button" class="btn btn-primary col-lg-4 col-md-12 editMark"><i class="fa fa-pencil"></i></a>' +
                '<a type="button" class="btn btn-danger col-lg-4 col-md-12 deleteMark"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</td>'
            );
            tr.attr("tr-id", data['id']);
            tr.attr("data-mark-type-id", data['mark_type_id']);
            order("#marks_table");
        }

        $("#marks_table>tbody").on("click", ".editMark", function () {
            let tr = $(this).parents("tr:eq(0)"),
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="30" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 saveMark" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancelMark"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            tr.find(".cancelMark").click(function () {
                tr.html(html);
                order("#marks_table");
            });
            order("#marks_table");
        });

        $("#marks_table>tbody").on("click", ".deleteMark", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr-id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsiniz?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit : {
                        text : "Sil",
                        btnClass : 'btn-green',
                        action:function(){
                            $.post( "{{ route('msk_marks_delete') }}", { id: id, _token: _token }, function( response ) {
                                if(response['status']=='error') $("#errors").html(response['errors']);
                                else {
                                    tr.remove();
                                    order('#marks_table');
                                }
                            });
                        }
                    },
                    formCancel:{
                        text : "İmtina et",
                        btnClass : 'btn-red',
                        action:function() {

                        }

                    }
                }
            });
        });


        function order(tableName) {
            $(tableName).find("tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }


        function loaderDisplay(val = 0){
            if(val){
                $("#marks_table").parent('div').parent('div').find('div:eq(0)').find('img').show();
                $("#marks_table").parent('div').parent('div').find('div:eq(0)').addClass('loader');
                $("#marks_table").parent('div').css('opacity','0.5');
            }
            else{
                $("#marks_table").parent('div').parent('div').find('div:eq(0)').removeClass('loader');
                $("#marks_table").parent('div').css('opacity','1');
                $("#marks_table").parent('div').parent('div').find('div:eq(0)').find('img').hide();
            }
        }
    </script>
@endsection









































{{--@extends('base')--}}

{{--@section('container')--}}
    {{--<div class="col-lg-6 col-md-12">--}}
        {{--<div id="errors" class="row">--}}

        {{--</div>--}}
        {{--@if(\App\Library\Helper::has_priv('msk_mark_types',\App\Library\Standarts::PRIV_CAN_EDIT))--}}
            {{--<button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>--}}
        {{--@endif--}}
        {{--<table class="table table-hover" id="basicTable">--}}
            {{--<thead>--}}
            {{--<tr>--}}
                {{--<th>#</th>--}}
                {{--<th>Tip</th>--}}
                {{--<th style="width: 185px;">Əməliyyat</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}
            {{--<tbody>--}}
            {{--@foreach($mark_types as $mark_type)--}}
                {{--<tr tr_id="{{ $mark_type->id }}">--}}
                    {{--<td>{{ $loop->iteration }}</td>--}}
                    {{--<td>{{ $mark_type->name }}</td>--}}
                    {{--<td>--}}
                        {{--@if(\App\Library\Helper::has_priv('msk_mark_types',\App\Library\Standarts::PRIV_CAN_EDIT))--}}
                            {{--<div class="btn-group-sm">--}}
                                {{--<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>--}}
                                {{--<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>--}}
                            {{--</div>--}}
                        {{--@else--}}
                            {{--<span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>--}}
                        {{--@endif--}}
                    {{--</td>--}}
                {{--</tr>--}}
            {{--@endforeach--}}
            {{--</tbody>--}}
        {{--</table>--}}
    {{--</div>--}}
{{--@endsection--}}

{{--@section('css')--}}
    {{--<link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />--}}
{{--@endsection--}}

{{--@section('script')--}}
    {{--<script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>--}}


    {{--<script>--}}
        {{--$(".add_new").click(function () {--}}
            {{--let tbody = $("tbody");--}}

            {{--tbody.prepend('<tr tr_id="0">' +--}}
                {{--'<td></td>' +--}}
                {{--'<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +--}}
                {{--'<td>' +--}}
                {{--'   <div class="btn-group-sm">' +--}}
                {{--'       <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>' +--}}
                {{--'       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +--}}
                {{--'   </div>' +--}}
                {{--'</td>' +--}}
                {{--'</tr>');--}}
            {{--tbody.find("tr:first").hide().show('fast');--}}

            {{--tbody.find("tr:first .cancel").click(function () {--}}
                {{--$(this).parents("tr:eq(0)").remove();--}}
                {{--order();--}}
            {{--});--}}
            {{--order();--}}
        {{--});--}}

        {{--$("tbody").on("click", ".save", function () {--}}
            {{--let tr = $(this).parents("tr:eq(0)"),--}}
                {{--data = {},--}}
                {{--id = tr.attr("tr_id");--}}

            {{--if(isValid(tr)){--}}
                {{--tr.find("[name]").each(function () {--}}
                    {{--data[$(this).attr("name")] = $(this).val();--}}
                {{--});--}}
                {{--data['id'] = id;--}}
                {{--data['_token'] = _token;--}}

                {{--$.post( "{{ route('msk_mark_types_add_edit_action') }}", data, function( response ) {--}}
                    {{--if(response['status']=='error') $("#errors").html(response['errors']);--}}
                    {{--else saveTr(tr, response['data']);--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}

        {{--$("tbody").on("click", ".edit_data", function () {--}}
            {{--let tr = $(this).parents("tr:eq(0)"),--}}
                {{--html = tr.html();--}}

            {{--tr.html('<td></td>' +--}}
                {{--'<td><input type="text" maxlength="30" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +--}}
                {{--'<td>' +--}}
                {{--'   <div class="btn-group-sm">' +--}}
                {{--'       <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>' +--}}
                {{--'       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +--}}
                {{--'   </div>' +--}}
                {{--'</td>');--}}

            {{--tr.find(".cancel").click(function () {--}}
                {{--tr.html(html);--}}
                {{--order();--}}
            {{--});--}}
            {{--order();--}}
        {{--});--}}

        {{--$("tbody").on("click", ".data_delete", function () {--}}
            {{--let tr = $(this).parents("tr:eq(0)"),--}}
                {{--id = tr.attr("tr_id");--}}

            {{--$.confirm({--}}
                {{--title: 'Təsdiq',--}}
                {{--content: 'Silmək istədiyinizə əminsizin?',--}}
                {{--type: 'red',--}}
                {{--typeAnimated: true,--}}
                {{--buttons: {--}}
                    {{--"Sil": function () {--}}
                        {{--$.post( "{{ route('msk_mark_types_delete') }}", { id: id, _token: _token }, function( response ) {--}}
                            {{--if(response['status']=='error') $("#errors").html(response['errors']);--}}
                            {{--else {--}}
                                {{--tr.remove();--}}
                                {{--order();--}}
                            {{--}--}}
                        {{--});--}}
                    {{--},--}}
                    {{--"İmtina": function () {--}}

                    {{--},--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}

        {{--function isValid(tr) {--}}
            {{--let name = tr.find("[name='name']"),--}}
                {{--status = true;--}}

            {{--name.css("border", "");--}}

            {{--if(name.val().trim().length > 60 || name.val().trim().length == 0){--}}
                {{--name.css("border", "1px dotted red");--}}
                {{--status = false;--}}
            {{--}--}}

            {{--return status;--}}
        {{--}--}}

        {{--function saveTr(tr, data) {--}}
            {{--tr.html('<td> </td>' +--}}
                {{--'<td>'+data['name']+'</td>' +--}}
                {{--'<td>' +--}}
                {{--'<div class="btn-group-sm">' +--}}
                {{--'<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>' +--}}
                {{--'<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>' +--}}
                {{--'</div>' +--}}
                {{--'</td>'--}}
            {{--);--}}
            {{--tr.attr("tr_id", data['id']);--}}
            {{--order();--}}
        {{--}--}}

        {{--function order() {--}}
            {{--$("#basicTable>tbody>tr").each(function(i){--}}
                {{--$(this).find("td:eq(0)").text(i+1);--}}
            {{--});--}}
        {{--}--}}
    {{--</script>--}}
{{--@endsection--}}