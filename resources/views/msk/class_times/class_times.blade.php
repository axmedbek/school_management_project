@extends('base')

@section('container')
    <div class="col-lg-12">
        <div class="panel panel-transparent">
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                <h4 style="padding-left:15px;margin:5px;border-bottom:2px solid #d9d9d9;">Korpuslar</h4>
                @foreach($corpuses as $corpus)
                    <li corpus_id="{{ $corpus->id }}">
                        <a data-toggle="tab" href="#tab{{ $corpus->id }}">{{ $corpus->name }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content bg-white">
                <div id="errors" class="row">

                </div>
                @foreach($corpuses as $corpus)
                    <div class="tab-pane" id="tab{{ $corpus->id }}">
                        @if(\App\Library\Helper::has_priv('msk_class_times',\App\Library\Standarts::PRIV_CAN_EDIT))
                            <button type="button" class="btn btn-success pull-right add_new"><i
                                        class="pg-plus_circle"> </i> Yeni
                            </button>
                        @endif
                        <table class="table table-hover" id="basicTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="min-width: 125px;">Başlama vaxtı</th>
                                <th style="min-width: 125px;">Bitmə vaxtı</th>
                                <th style="min-width: 180px;">Əməliyyat</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($corpus->class_times as $class_time)
                                <tr tr_id="{{ $class_time->id }}">
                                    <td></td>
                                    <td style="text-align: center;">{{ date('H:i',strtotime($class_time->start_time)) }}</td>
                                    <td style="text-align: center;">{{ date('H:i',strtotime($class_time->end_time)) }}</td>
                                    <td>
                                        @if(\App\Library\Helper::has_priv('msk_class_times',\App\Library\Standarts::PRIV_CAN_EDIT))
                                            <div class="btn-group-sm">
                                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i
                                                            class="fa fa-pencil"></i></a>
                                                @php $checkClassTimes = \App\Models\Lesson::realData()->where('class_time_id',$class_time->id)->first(); @endphp
                                                @if(!isset($checkClassTimes))
                                                    <a type="button"
                                                       class="btn btn-danger col-lg-4 col-md-12 data_delete"><i
                                                                class="fa fa-trash-o"></i></a>
                                                @endif
                                            </div>
                                        @else
                                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <script type="text/html" id="row">

        <td></td>

        <td>
            <div class='form-group'>
                <input type="text" name="start_time" class="form-control" required="">
            </div>
        </td>

        <td>
            <div class='form-group'>
                <input type="text" name="end_time" class="form-control" required="">
            </div>
        </td>

        <td>
            <div class="btn-group-sm">
                <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-pencil"></i></a>
                <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>
            </div>
        </td>

    </script>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
    <link href="{{ asset('assets\plugins\bt_timepicker\bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css" media="screen">
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets\plugins\bt_timepicker\bootstrap-timepicker.js') }}"></script>

    <script>

        // $(".datepicker").timepicker({
        //     minuteStep: 1,
        //     showMeridian: true
        // });

        $("[name='start_time']").timepicker({
            defaultDate : 'current',
            minuteStep: 1,
            explicitMode: true,
            showMeridian: false
        });

        $(".add_new").click(function () {
            let tbody = $(this).parents(".tab-pane:eq(0)").find("tbody");

            tbody.parents(".tab-pane:eq(0)").find("tbody").prepend('<tr tr_id="0">' + $("#row").html() + '</tr>');
            $("[name='start_time'],[name='end_time']").timepicker({
                defaultDate : 'current',
                minuteStep: 1,
                explicitMode: true,
                showMeridian: false
            });

            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();
        });

        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                corpus_id = $("#tab-3 li.active").attr("corpus_id"),
                start_time = tr.find('[name="start_time"]').val(),
                voiceOne = tr.find('[name="voiceOne"]').get(0).files[0],
                end_time = tr.find('[name="end_time"]').val(),
                voiceTwo = tr.find('[name="voiceTwo"]').get(0).files[0],
                id = tr.attr("tr_id");

            var formData = new FormData();
            formData.append('id', id);
            formData.append('start_time', start_time);
            formData.append('voiceOne', voiceOne);
            formData.append('end_time', end_time);
            formData.append('voiceTwo', voiceTwo);
            formData.append('corpus_id', corpus_id);
            formData.append('_token', _token);

            if (isValid(tr)) {
                loader(1);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('msk_class_times_add_edit_action') }}',
                    dataType: 'json',
                    data: formData,
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function (response) {
                        if (response['status'] == 'error') {
                            $("#errors").html(response['errors']);
                            loader(0);
                        }
                        else{
                            saveTr(tr, response['data']);
                            loader(0);
                        }
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {


            let tr = $(this).parents("tr:eq(0)"),
                start_time = tr.find("td:eq(1)").text().trim(),

                end_time = tr.find("td:eq(3)").text().trim(),
                html = tr.html();

            tr.html($("#row").html());

            //vals
            tr.find("[name='start_time']").val(start_time);

            tr.find("[name='start_time']").timepicker({
                minuteStep: 1,
                defaultDate : 'current',
                explicitMode: true,
                showMeridian: false
            });
            tr.find("[name='end_time']").val(end_time);
            tr.find("[name='end_time']").timepicker({
                minuteStep: 1,
                defaultDate : 'current',
                explicitMode: true,
                showMeridian: false
            });

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");
            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit: {
                        text: 'Bəli',
                        btnClass: 'btn-green',
                        action: function () {
                            loader(1);
                            $.post("{{ route('msk_class_times_delete') }}", {
                                id: id,
                                _token: _token
                            }, function (response) {
                                if (response['status'] == 'error') {
                                    loader(0);
                                    $("#errors").html(response['errors']);
                                }
                                else {
                                    tr.remove();
                                    loader(0);
                                    order();
                                }
                            });
                        }
                    },
                    formCancel: {

                        text: 'Xeyr',
                        btnClass: 'btn-red',
                        action: function () {

                        },
                    }
                }
            });
        });

        function isValid(tr) {
            let start_time = tr.find("[name='start_time']"),
                start_sound = tr.find("[name='voiceOne']"),
                end_time = tr.find("[name='end_time']"),
                end_sound = tr.find("[name='voiceTwo']"),
                status = true;

            start_time.css("border", "");
            start_sound.css("border", "");
            end_time.css("border", "");
            end_sound.css("border", "");

            if (start_time.val().length <= 0) {
                start_time.css("border", "1px dotted red");
                status = false;
            }
            if (end_time.val().length <= 0) {
                end_time.css("border", "1px dotted red");
                status = false;
            }

            if(hourCompare(start_time.val(),end_time.val())){
                status = false;
            }


            if (start_sound.val() == "") {
                var tr = start_sound.parents("tr:eq(0)");
                if (tr.attr('tr_id') == 0) {
                    start_sound.css("border", "1px dotted red");
                    status = false;
                }
            }

            if (end_sound.val() == "") {
                var tr = end_sound.parents("tr:eq(0)");
                if (tr.attr('tr_id') == 0) {
                    end_sound.css("border", "1px dotted red");
                    status = false;
                }
            }

            if (checkFileSize(start_sound,"Başlama səs") || checkFileSize(end_sound,"Bitmə səs") ) {
                status = false;
            }

            return status;
        }


        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td style="text-align:center;">' + data['start_time'] + '</td>' +
                '<td>' +
                '<audio controls style="width:120px;">' +
                '<source src="{{ asset('option_files') }}/' + data['start_sound'] + '">' +
                '</audio>' +
                '</td>' +
                '<td style="text-align:center;">' + data['end_time'] + '</td>' +
                '<td>' +
                '<audio controls style="width:120px;">' +
                '<source src="{{ asset('option_files') }}/' + data['end_sound'] + '">' +
                '</audio>' +
                '</td>' +
                '<td>' +
                '<div class="btn-group-sm">' + data['buttons'] + '</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            order();
        }

        function order() {
            $("table").each(function () {
                $(this).find('tbody>tr').each(function (i) {
                    $(this).find('td:eq(0)').text(i+1);
                });
            });
        }

        $(document).ready(function () {
            $('ul.nav').find('li:eq(0)').find('a').trigger('click');
        });

        function hourCompare(h1,h2){
            let status = false;
            var startTime = h1+":00";
            var endTime = h2+":00";
            var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
            if(!(parseInt(endTime .replace(regExp, "$1$2$3")) > parseInt(startTime .replace(regExp, "$1$2$3")))){
                $.confirm({
                    title:'Vaxt xətası!',
                    content: 'Başlama vaxtı bitmə vaxtın böyük ola bilməz!',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'OK',
                            btnClass: 'btn-red',
                            action: function(){
                            }
                        },
                        Bağla: function () {
                        }
                    }
                });
                status = true;
            }
            return status;
        }

        order();

        function checkFileSize(element,message) {
            var status = false;
            if ($(element).get(0).files[0]) {
                if($(element).get(0).files[0].size > 8388608){
                    $.confirm({
                        title: message + ' fayl həcmi çox böyükdür',
                        content: 'Faylın həcmi maksimum 8mb olmalıdır!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-red',
                                action: function(){
                                }
                            },
                            Bağla: function () {
                            }
                        }
                    });
                    status = true;
                }
            }

            return status;
        }

        $(".lesson_start_btn").on('click',function(e){
            if (checkFileSize($("input[name='file_sound']"),"Başlama səs")) {
                e.preventDefault();
            }
        });

        function loader(status){
            if (status){
                $("#page_loading").css({"background-color":"black","opacity":"0.2"});
                $("#page_loading").show();
            }
            else{
                $("#page_loading").hide();
            }
        }
    </script>
@endsection