@extends('base')

@section('container')
    <div class="col-lg-6 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_room_type',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif
        <table class="table table-hover" id="basicTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Adı</th>
                <th style="width: 185px;">Əməliyyat</th>
            </tr>
            </thead>
            <tbody>
            @foreach($room_types as $room_type)
                <tr tr_id="{{ $room_type->id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $room_type->name }}</td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_room_type',\App\Library\Standarts::PRIV_CAN_EDIT))

                            <div class="btn-group-sm">
                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                @php $checkRoomType = \App\Models\Room::realData()->where('type',$room_type->id)->first(); @endphp
                                @if(!isset($checkRoomType))
                                    <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>


    <script>

        $('[data-toggle="save"]').tooltip();

        $(".add_new").click(function () {
            let tbody = $("tbody");

            tbody.prepend('<tr tr_id="0">' +
                '<td></td>' +
                '<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();
        });

        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                id = tr.attr("tr_id");

            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['_token'] = _token;

                $.post( "{{ route('msk_room_type_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="30" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post( "{{ route('msk_room_type_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });

        function isValid(tr) {
            let name = tr.find("[name='name']"),
                status = true;

            name.css("border", "");

            if(name.val().trim().length > 60 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                '<div class="btn-group-sm">'+ data['buttons'] +'</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            order();
        }

        function order() {
            $("#basicTable>tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }
    </script>
@endsection