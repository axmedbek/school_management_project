@extends('base')

@section('container')
    <style>
        .activeTr {
            color: white;
            background-color: rgba(62, 112, 170, 0.59) !important;
        }

        .loader{
            position: absolute;
            height: 100%;
            width: 100%;
            z-index: 9999;
        }

        table>tbody>tr> {
            cursor: pointer;
        }
    </style>
    <div class="col-lg-6 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_cities_and_regions',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new_city"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif
        <table class="table" id="city_table">
            <thead>
            <tr>
                <th>#</th>
                <th>Şəhərlər</th>
                <th style="width: 185px;">Əməliyyat</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cities as $city)
                <tr tr_id="{{ $city->id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $city->name }}</td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_cities_and_regions',\App\Library\Standarts::PRIV_CAN_EDIT))

                            <div class="btn-group-sm">
                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                @php
                                    $checkLivedCity = \App\User::realData()->where('lived_city',$city->id)->first();
                                    $checkCurrentCity = \App\User::realData()->where('current_city',$city->id)->first();
                                @endphp
                                @if(!isset($checkLivedCity) && !isset($checkCurrentCity))
                                    <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-lg-6 col-md-12" style="display: none;">
        <div>
            <img src="{{ asset('images/loader.gif') }}" alt="" style="width: 60px;height: 60px;margin-left: 280px;margin-top: 160px;">
        </div>
        <div>
            <div id="errors" class="row">

            </div>
            @if(\App\Library\Helper::has_priv('msk_cities_and_regions',\App\Library\Standarts::PRIV_CAN_EDIT))
                <button type="button" class="btn btn-success pull-right add_new_region"><i class="pg-plus_circle"> </i> Yeni</button>
            @endif
            <table class="table table-hover" id="region_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Rayonlar</th>
                    <th style="width: 185px;">Əməliyyat</th>
                </tr>
                </thead>
                <tbody>
                @foreach($regions as $region)
                    <tr tr_id="{{ $region->id }}">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $region->name }}</td>
                        <td>
                            @if(\App\Library\Helper::has_priv('msk_cities_and_regions',\App\Library\Standarts::PRIV_CAN_EDIT))

                                <div class="btn-group-sm">
                                    <a type="button" class="btn btn-primary col-lg-4 col-md-12 editRegion"><i class="fa fa-pencil"></i></a>
                                    @php
                                        $checkLivedRegion = \App\User::realData()->where('lived_region',$region->id)->first();
                                        $checkCurrentRegion = \App\User::realData()->where('current_region',$region->id)->first();
                                    @endphp
                                    @if(!isset($checkLivedRegion) && !isset($checkCurrentRegion))
                                        <a type="button" class="btn btn-danger col-lg-4 col-md-12 deleteRegion"><i class="fa fa-trash-o"></i></a>
                                    @endif
                                </div>
                            @else
                                <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js" type="text/javascript"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>


    <script>

        $('[data-toggle="save"]').tooltip();

        $(".add_new_city").click(function () {
            let tbody = $("#city_table>tbody");

            tbody.prepend('<tr tr_id="0">' +
                '<td></td>' +
                '<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order("#city_table");
            });
            order("#city_table");
        });

        $("#city_table>tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                id = tr.attr("tr_id");

            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['_token'] = _token;

                $.post( "{{ route('msk_cities_and_regions_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("#city_table>tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="30" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            tr.find(".cancel").click(function () {
                tr.html(html);
                order("#city_table");
            });
            order("#city_table");
        });

        $("#city_table>tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsiniz?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit : {
                        text : "Sil",
                        btnClass : 'btn-green',
                        action:function(){
                            $.post( "{{ route('msk_cities_and_regions_delete') }}", { id: id, _token: _token }, function( response ) {
                                if(response['status']=='error') $("#errors").html(response['errors']);
                                else {
                                    tr.remove();
                                    $("#city_table>tbody").find("tr:first").trigger('click');
                                    order('#city_table');
                                }
                            });
                        }

                    },
                    formCancel:{
                        text : "İmtina et",
                        btnClass : 'btn-red',
                        action:function() {

                        }

                    }
                }
            });
        });

        function isValid(tr) {
            let name = tr.find("[name='name']"),
                status = true;

            name.css("border", "");

            if(name.val().trim().length > 60 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                '<div class="btn-group-sm">' + data['buttons'] + '</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            order("#city_table");
        }

        // regions


        $(".add_new_region").click(function () {
            let tbody = $("#region_table>tbody"),
            data_city_id = $("#city_table").find('.activeTr').attr('tr_id');

            tbody.prepend('<tr tr-id="0" data-city-id="'+data_city_id+'">' +
                '<td></td>' +
                '<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 saveRegion" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancelRegion"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancelRegion").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order("#region_table");
            });
            order("#region_table");
        });



        $("#city_table>tbody").on('click','tr[tr_id!=0]',function () {
            $("#region_table").parent('div').parent('div').show();
            $("#city_table").find('.activeTr').removeClass('activeTr');
            $(this).addClass('activeTr');
            let city_id = $(this).attr('tr_id');
            loaderDisplay(1);
            $.post("{{ route('msk_cities_and_regions_get_regions') }}",{'city_id':city_id,'_token':_token},function (response) {
                if(response.status == 'ok'){
                   loaderDisplay (0);
                    $("#region_table>tbody").html("");
                    _.forEach(response.data,function (val,key) {
                        $("#region_table>tbody").append(
                            '<tr tr-id="'+val.id+'" data-city-id="'+val.msk_cities_id+'">' +
                            '<td></td>'+
                            '<td>'+val.name+'</td>' +
                            '<td>' +
                            '<div class="btn-group-sm">' + val.buttons + '</div>' +
                            '</td>'+
                            '</tr>');
                    });
                }
                order('#region_table');
            });
        });

        $("#city_table>tbody").find("tr:first").trigger('click');

        $("#region_table>tbody").on("click", ".saveRegion", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                id = tr.attr("tr-id"),
                city_id = tr.attr("data-city-id");


            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['city_id'] = city_id;
                data['_token'] = _token;

                $.post( "{{ route('msk_cities_and_regions_region_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveRegionTr(tr, response['data']);
                });
            }
        });

        function saveRegionTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                '<div class="btn-group-sm">' + data['buttons'] + '</div>' +
                '</td>'
            );
            tr.attr("tr-id", data['id']);
            tr.attr("data-city-id", data['msk_cities_id']);
            order("#region_table");
        }

        $("#region_table>tbody").on("click", ".editRegion", function () {
            let tr = $(this).parents("tr:eq(0)"),
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="30" required name="name" class="form-control" value="'+tr.find("td:eq(1)").text().trim()+'"></td>' +
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 saveRegion" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancelRegion"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            tr.find(".cancelRegion").click(function () {
                tr.html(html);
                order("#region_table");
            });
            order("#region_table");
        });

        $("#region_table>tbody").on("click", ".deleteRegion", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr-id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsiniz?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit : {
                        text : "Sil",
                        btnClass : 'btn-green',
                        action:function(){
                            $.post( "{{ route('msk_cities_and_regions_delete_region') }}", { id: id, _token: _token }, function( response ) {
                                if(response['status']=='error') $("#errors").html(response['errors']);
                                else {
                                    tr.remove();
                                    order('#region_table');
                                }
                            });
                        }
                    },
                    formCancel:{
                        text : "İmtina et",
                        btnClass : 'btn-red',
                        action:function() {

                        }

                    }
                }
            });
        });


        function order(tableName) {
            $(tableName).find("tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }


        function loaderDisplay(val = 0){
            if(val){
                $("#region_table").parent('div').parent('div').find('div:eq(0)').find('img').show();
                $("#region_table").parent('div').parent('div').find('div:eq(0)').addClass('loader');
                $("#region_table").parent('div').css('opacity','0.5');
            }
            else{
               $("#region_table").parent('div').parent('div').find('div:eq(0)').removeClass('loader');
                $("#region_table").parent('div').css('opacity','1');
                $("#region_table").parent('div').parent('div').find('div:eq(0)').find('img').hide();
            }
        }
    </script>
@endsection
