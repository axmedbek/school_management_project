@extends('base')

@section('container')
    <div class="col-lg-12">
        <div id="errors" class="row">

        </div>
        <div class="panel panel-transparent">
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                <h4 style="padding-left:15px;margin:5px;border-bottom:2px solid #d9d9d9;">Tədris ili</h4>
                @foreach($years as $year)
                    <li year_id="{{ $year->id }}">
                        <a data-toggle="tab" href="#tab{{ $year->id }}" style="font-size: 15px;">
                            {{ $year->start_date }}
                            <br>
                            {{ $year->end_date }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content bg-white">
                @foreach($years as $year)
                    <div class="tab-pane" id="tab{{ $year->id }}">
                        @if(\App\Library\Helper::has_priv('msk_holidays',\App\Library\Standarts::PRIV_CAN_EDIT))
                            @if($year->active == 1)
                                <button type="button" class="btn btn-success pull-right add_new"><i
                                            class="pg-plus_circle"> </i> Yeni
                                </button>
                            @endif
                        @endif
                        <div class="alert alert-danger col-md-11 error" style="display: none;font-size: 15px;">

                        </div>
                        <table class="table table-hover" id="basicTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Adı</th>
                                <th>Başlama tarİxİ</th>
                                <th>Bİtmə tarİxİ</th>
                                <th>Tİpİ</th>
                                <th>Sİnİf</th>
                                <th style="width: 185px;">Əməlİyyat</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($year->holidays as $holiday)
                                <tr tr_id="{{ $holiday->id }}" data-classes="{{ $holiday->holiday_classes->pluck('id')}}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $holiday->name }}</td>
                                    <td>{{ date('d-m-Y',strtotime($holiday->start_date)) }}</td>
                                    <td>{{ date('d-m-Y',strtotime($holiday->end_date)) }}</td>
                                    @php
                                    $holidayTypeName = "";
                                    switch ($holiday->type){
                                        case 1 : $holidayTypeName = 'Qeyri iş günü';
                                        break;
                                        case 2 : $holidayTypeName = 'Bayram günü';
                                        break;
                                        case 3 : $holidayTypeName = 'Matəm(hüzn) günü';
                                        break;
                                        case 4 : $holidayTypeName = 'Seçki günü';
                                        break;
                                        case 5 : $holidayTypeName = 'Tətil';
                                        break;
                                    }
                                    @endphp
                                    <td selected_id="{{ $holiday->type }}">{{ $holidayTypeName }}</td>
                                    <td>{{ implode(",",$holiday->holiday_classes->pluck('name')->toArray()) }}</td>
                                    <td>
                                        @if(\App\Library\Helper::has_priv('msk_holidays',\App\Library\Standarts::PRIV_CAN_EDIT))
                                            @if($year->active == 1)
                                            <div class="btn-group-sm">
                                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i
                                                            class="fa fa-pencil"></i></a>
                                                <a type="button"
                                                   class="btn btn-danger col-lg-4 col-md-12 data_delete"><i
                                                            class="fa fa-trash-o"></i></a>
                                            </div>
                                            @else
                                                <span class="badge badge-danger">İl aktiv deyil</span>
                                            @endif
                                        @else
                                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <script type="text/html" id="row">
        <td></td>
        <td>
            <div class="form-group">
                <input type="text" name="name" class="form-control">
            </div>
        </td>
        <td>
            <div class='form-group'>
                <input type='text' class="form-control datepicker" name="start_date">
            </div>
        </td>
        <td>
            <div class='form-group '>
                <input type='text' class="form-control datepicker" name="end_date">
            </div>
        </td>
        <td>
            <div class="form-group">
                <select name="type" id="type" class="form-control">
                    <option value="" style="color: darkgrey;" disabled selected>Tip seçin</option>
                    <option value="1">Qeyri iş günü</option>
                    <option value="2">Bayram günü</option>
                    <option value="3">Matəm(hüzn) günü</option>
                    <option value="4">Seçki günü</option>
                    <option value="5">Tətil</option>
                </select>
            </div>
        </td>
        <td>
            <select name="classNames[]" multiple class="full-width" style="width: 200px;">
                <option value="0">Hamisi</option>
                @foreach(\App\Models\MskClass::realData()->get() as $class)
                    <option value="{{ $class->id }}">{{ $class->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <div class="btn-group-sm">
                <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>
                <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>
            </div>
        </td>
        <br>
    </script>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
    <style>
        .ui-datepicker-week-end a {
            color: red !important;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
    <script>

        $(".add_new").click(function () {
            let tbody = $(this).parents(".tab-pane:eq(0)").find("tbody");
            if($('table>tbody>tr').is('[tr_id="0"]')) return;
            tbody.parents(".tab-pane:eq(0)").find("tbody").prepend(
                '<tr tr_id="0" data-classes="[]">' + $("#row").html() + '</tr>'
            );

            $('input[name="start_date"]').datepicker({
                format:'dd-mm-yyyy',
                weekStart : 1,
                todayHighlight: true,
                autoclose:true
            });

            $('input[name="end_date"]').datepicker({
                format:'dd-mm-yyyy',
                weekStart : 1,
                todayHighlight: true,
                autoclose:true
            });


            tbody.find("tr:first").hide().show('fast');
            tbody.find("tr:first select[name='classNames[]']").select2();
            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();

            $('select[name="classNames[]"]').on('change',function(){
               if($(this).val()){
                   for (var i = 0 ; i < $(this).val().length ; i ++ ){
                       if($(this).val()[i] == 0){
                           $(this).select2('val','').select2('val',0);
                       }
                   }
               }
            });
        });




        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                year_id = $("#tab-3 li.active").attr("year_id"),
                name = tr.find('input[name="name"]').val(),
                start_date = tr.find('input[name="start_date"]').val(),
                end_date = tr.find('input[name="end_date"]').val(),
                type = tr.find('select[name="type"]').val(),
                classes = tr.find('select[name="classNames[]"]').val(),
                id = tr.attr("tr_id");

            if (isValid(tr)) {

                data['id'] = id;
                data['start_date'] = start_date;
                data['end_date'] = end_date;
                data['name'] = name;
                data['year_id'] = year_id;
                data['type'] = type;
                data['classes'] = classes;
                data['_token'] = _token;

                //console.log(data);

                $.post("{{ route('msk_holidays_add_edit_action') }}", data, function (response) {
                    if (response['status'] == 'error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {

            let tr = $(this).parents("tr:eq(0)"),
                name = tr.find("td:eq(1)").text().trim(),
                start_date = tr.find("td:eq(2)").text().trim(),
                end_date = tr.find("td:eq(3)").text().trim(),
                type=tr.find("td:eq(4)").attr('selected_id'),
                classes = JSON.parse(tr.attr("data-classes")),
                html = tr.html();

            tr.html($("#row").html());
            tr.find("select[name='classNames[]']").select2();

            tr.find('input[name="name"]').val(name);
            tr.find('input[name="start_date"]').val(start_date).datepicker({
                format:'dd-mm-yyyy',
                weekStart : 1,
                todayHighlight: true,
                autoclose:true
            });

            tr.find('select[name="type"]').val(type);
            tr.find("[name='classNames[]']").val(classes).trigger('change');
            $('select[name="classNames[]"]').on('change',function(){
                if($(this).val()){
                    for (var i = 0 ; i < $(this).val().length ; i ++ ){
                        if($(this).val()[i] == 0){
                            $(this).select2('val','').select2('val',0);
                        }
                    }
                }
            });

            tr.find('input[name="end_date"]').val(end_date).datepicker({
                format:'dd-mm-yyyy',
                weekStart : 1,
                todayHighlight: true,
                autoclose:true
            });

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit: {
                        text: 'Bəli',
                        btnClass: 'btn-green',
                        action: function () {
                            $.post("{{ route('msk_holidays_delete') }}", {
                                id: id,
                                _token: _token
                            }, function (response) {
                                if (response['status'] == 'error') $("#errors").html(response['errors']);
                                else {
                                    tr.remove();
                                    order();
                                }
                            });
                        }
                    },
                    formCancel: {

                        text: 'Xeyr',
                        btnClass: 'btn-red',
                        action: function () {

                        },
                    }
                }
            });
        });

        function saveTr(tr, data) {
            var type = data['type'] == 1 ?
                "Qeyri iş günü" : data['type'] == 2 ?
                    "Bayram günü" : data['type'] == 3 ?
                        "Matəm(hüzn) günü" : data['type'] == 3 ?
                            "Seçki günü" : "Tətil";

            tr.html('<td> </td>' +
                '<td>' + data['name'] + '</td>' +
                '<td>' + data['start_date'].split("-").reverse().join("-") + '</td>' +
                '<td>' + data['end_date'].split("-").reverse().join("-") + '</td>' +
                '<td>' + type + '</td>' +
                '<td>' +  data['classNames'] + '</td>' +
                '<td>' +
                '<div class="btn-group-sm">' +
                '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>' +
                '<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            tr.attr('data-classes',"["+data['classId']+"]");
            tr.find("td:eq(4)").attr('selected_id',data['type']);
            order();
        }

        function isValid(tr) {
            let name = tr.find("input[name='name']"),
                start_date = tr.find("input[name='start_date']"),
                end_date = tr.find('input[name="end_date"]'),
                type = tr.find('select[name="type"]'),
                status = true;

            start_date.css("border", "");
            end_date.css("border", "");
            type.css("border","");
            name.css("border","");


            if (start_date.val().length <= 0) {
                start_date.css("border", "1px dotted tomato");
                status = false;
            }
            if (end_date.val().length <= 0) {
                end_date.css("border", "1px dotted tomato");
                status = false;
            }
            if(name.val().length <= 0 ){
                name.css("border","1px dotted tomato");
                status=false;
            }
            if(type.val().length <= 0){
                type.css("border","1px dotted tomato");
                status=false;
            }





            if(start_date.datepicker('getDate').getTime() > end_date.datepicker('getDate').getTime())
            {
                end_date.css("border", "1px dotted tomato");
                start_date.css("border", "1px dotted tomato");
                $('.error').text('Bitmə tarixi başlama tarixindən kiçik ola bilməz');
                $('.error').css("display","");
                setTimeout(function () {
                    $('.error').slideUp(500);
                },5000);
                status=false;
            }



            return status;
        }

        function order() {
            $("#basicTable>tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }

        $(function () {
            $("#tab-3 li:eq(0) a").click();
        });

    </script>
@endsection