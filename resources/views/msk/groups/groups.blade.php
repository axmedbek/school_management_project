@extends('base')

@section('container')
    <div class="col-lg-8 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_group',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new" onclick="openModal('{{ route('msk_groups_add_edit',['group' => 0]) }}')"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif

        <table class="table table-hover" id="basicTable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Group adi</th>
                    <th style="width: 185px;">Əməliyyat</th>
                </tr>
            </thead>
            <tbody>
            @foreach($groups as $group)
                <tr tr_id="{{ $group->id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $group->group_name }}</td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_group',\App\Library\Standarts::PRIV_CAN_EDIT))
                            <div class="btn-group-sm">
                                <a type="button" onclick="openModal('{{ route('msk_groups_add_edit',['group' => $group->id]) }}')" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                @php $checkGroup = \App\User::realData()->where('group_id',$group->id)->first();  @endphp
                                @if(!isset($checkGroup))
                                    <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>


    <script>
        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post( "{{ route('msk_groups_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });

        function isValid(tr) {
            let name = tr.find("[name='name']"),
                status = true;

            name.css("border", "");

            if(name.val().trim().length > 60 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>'+data['name']+'</td>' +
                '<td>' +
                '<div class="btn-group-sm">' +
                '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>' +
                '<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            order();
        }

        function order() {
            $("#basicTable>tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }
    </script>
@endsection