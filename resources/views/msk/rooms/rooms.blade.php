@extends('base')

@section('container')
    <div class="col-lg-12">
        <div id="errors" class="row">

        </div>
        <div class="panel panel-transparent">
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                @foreach($corpuses as $corpus)
                    <li corpus_id="{{ $corpus->id }}" style="float: left;">
                        <a data-toggle="tab" href="#tab{{ $corpus->id }}">{{ $corpus->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="tab-content bg-white">
            @foreach($corpuses as $corpus)
                <div class="tab-pane" id="tab{{ $corpus->id }}">
                    @if(\App\Library\Helper::has_priv('msk_rooms',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>
                    @endif
                    <table class="table table-hover" id="basicTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Otaq</th>
                            <th>Tutumu</th>
                            <th>Tİpİ</th>
                            <th>Qeyd</th>
                            <th>Fənnlər</th>
                            <th style="width: 185px;">Əməliyyat</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($corpus->rooms as $room)
                            <tr tr_id="{{ $room->id }}" subjects="{{ $room->subjects->pluck('id') }}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $room->name }}</td>
                                <td>{{ $room->capacity }}</td>
                                <td data-type-id="{{ $room->type }}">
                                    {{ $room->room_type->name }}
                                </td>
                                <td>{{ $room->info }}</td>
                                <td>{{ implode(",", $room->subjects->pluck('name')->toArray()) }}</td>
                                <td>
                                    @if(\App\Library\Helper::has_priv('msk_rooms',\App\Library\Standarts::PRIV_CAN_EDIT))
                                        <div class="btn-group-sm">
                                            <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                            @php
                                                $hasRoomInLessons = \App\Models\Lesson::realData()->where('room_id',$room->id)->get();
                                            @endphp
                                            @if(!isset($hasRoomInLessons[0]))
                                                <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                            @endif
                                        </div>
                                    @else
                                        <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>

    <script type="text/html" id="row">
            <td></td>
            <td style="min-width: 200px;"><input type="text" maxlength="30" required name="name" class="form-control" style="margin-right: -80px;width: 200px;"></td>
            <td><input type="number" required name="capacity" class="form-control" style="margin-right: -100px;"></td>
            <td>
                <input type="text" name="type" style="width: 200px;">
            </td>
            <td><input type="text" name="info" class="form-control" style="margin-right: 10px;"></td>
            <td>
                <select name="subjects[]" multiple class="full-width" style="width: 200px;">
                    @foreach(\App\Models\Subject::realData()->get() as $subject)
                        <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
               <div class="btn-group-sm">
                       <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>
                       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>
                   </div>
            </td>
    </script>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        $(".add_new").click(function () {
            let tbody = $(this).parents(".tab-pane:eq(0)").find("tbody");

            tbody.parents(".tab-pane:eq(0)").find("tbody").prepend('<tr tr_id="0" subjects="[]">' + $("#row").html() + '</tr>');

            $('input[name="type"]').select2({
                minimumResultsForSearch: -1,
                ajax: {
                    url: '{{ route('msk_rooms_get_data') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'msk_room_types',
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            });


            tbody.find("tr:first").hide().show('fast');
            tbody.find("tr:first select[name='subjects[]']").select2();

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();
        });

        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                corpus_id = $("#tab-3 li.active").attr("corpus_id"),
                type = tr.find('input[name="type"]').val(),
                id = tr.attr("tr_id");

            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['corpus_id'] = corpus_id;
                data['type'] = type;
                data['_token'] = _token;

                $.post( "{{ route('msk_rooms_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                name = tr.find("td:eq(1)").text().trim(),
                capacity = tr.find("td:eq(2)").text().trim(),
                type_id = tr.find("td:eq(3)").attr('data-type-id'),
                type = tr.find("td:eq(3)").text(),
                info = tr.find("td:eq(4)").text().trim(),
                subjects = JSON.parse(tr.attr("subjects")),
                html = tr.html();

           // console.log(type);

            tr.html($("#row").html());
            tr.find("select[name='subjects[]']").select2();

            //vals
            tr.find("[name='name']").val(name);
            tr.find("[name='capacity']").val(capacity);
            $('input[name="type"]').select2({
                minimumResultsForSearch: -1,
                ajax: {
                    url: '{{ route('msk_rooms_get_data') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'msk_room_types',
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            }).select2('data',{'id' : type_id,'text' : type});
            tr.find("[name='info']").val(info);
            tr.find("[name='subjects[]']").val(subjects).trigger('change');

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post( "{{ route('msk_rooms_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });
        
        function isValid(tr) {
            let name = tr.find("[name='name']"),
                capacity = tr.find("[name='capacity']"),
                type = tr.find("input[name='type']"),
                info = tr.find("[name='info']"),
                status = true;

            name.css("border", "");
            type.prev('div').css("border","");
            capacity.css("border", "");

            if(name.val().trim().length > 30 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }
            if(capacity.val().trim() == "" || (capacity.val() > 0)==false ){
                capacity.css("border", "1px dotted red");
                status = false;
            }
            if((type.val() > 0) == false ){
                type.prev('div').css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {



            tr.html('<td> </td>' +
                    '<td>'+data['name']+'</td>' +
                    '<td>'+data['capacity']+'</td>' +
                    '<td>'+data['room_type']+'</td>' +
                    '<td>'+(data['info']==null?'':data['info'])+'</td>' +
                    '<td>'+data['subjects']+'</td>' +
                    '<td>' +
                        '<div class="btn-group-sm">' +
                            '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>' +
                            '<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>' +
                        '</div>' +
                    '</td>'
                );
            tr.attr("tr_id", data['id']);
            tr.find("td:eq(3)").attr('data-type-id',data['type']);
            tr.attr("subjects", JSON.stringify(data['subjects_id']));
            order();
        }

        function order() {
            $("table").each(function () {
                $(this).find('tbody>tr').each(function (i) {
                    $(this).find('td:eq(0)').text(i+1);
                });
            });
        }
        $(function () {
           $("#tab-3 li:eq(0) a").click();
        });
    </script>
@endsection