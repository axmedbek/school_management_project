@extends('base')

@section('container')
    <div class="col-lg-8 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_heyetler',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif
        <table class="table table-hover" id="basicTable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Adı</th>
                    <th>Başlama vaxtı</th>
                    <th>Bitmə vaxtı</th>
                    <th style="width: 185px;">Əməliyyat</th>
                </tr>
            </thead>
            <tbody>
            @foreach($heyetler as $heyet)
                <tr tr_id="{{ $heyet->id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $heyet->name }}</td>
                    <td>{{ date('H:i',strtotime($heyet->start_time)) }}</td>
                    <td>{{ date('H:i',strtotime($heyet->end_time)) }}</td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_heyetler',\App\Library\Standarts::PRIV_CAN_EDIT))

                            <div class="btn-group-sm">
                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                @php $checkHeyetler = \App\User::realData()->where('personal_heyeti',$heyet->id)->first(); @endphp
                                @if(!isset($checkHeyetler))
                                    <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets\plugins\bt_timepicker\bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css" media="screen">
@endsection

@section('script')
    <script src="{{ asset('assets\plugins\bt_timepicker\bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
    <script>

        $('[data-toggle="save"]').tooltip();

        $(".add_new").click(function () {
            let tbody = $("tbody");

            tbody.prepend('<tr tr_id="0">' +
                '<td></td>' +
                '<td><input type="text" maxlength="60" required name="name" class="form-control"></td>' +
                '<td>'+
                    '<div class="form-group">'+
                        '<input type="text" name="start_time" class="form-control" required="">'+
                    '</div>'+
                '</td>'+
                '<td>'+
                    '<div class="form-group" >'+
                        '<input type="text" name="end_time" class="form-control"  required="">'+
                    '</div>'+
                '</td>'+
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>' +
                '</tr>');

            tbody.find("[name='start_time'],[name='end_time']").timepicker({
                defaultTime: 'current',
                minuteStep: 1,
                disableFocus: true,
                explicitMode : false,
                showMeridian: false
            });
            tbody.find("tr:first").hide().show('fast');

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();
        });

        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                start_time = tr.find('[name="start_time"]').val(),
                end_time = tr.find('[name="end_time"]').val(),
                data = {},
                id = tr.attr("tr_id");

            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['start_time'] = start_time;
                data['end_time'] = end_time;
                data['_token'] = _token;

                $.post( "{{ route('msk_heyetler_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                name = tr.find("td:eq(1)").text().trim(),
                start_time = tr.find("td:eq(2)").text().trim(),
                end_time = tr.find("td:eq(3)").text().trim(),
                html = tr.html();

            tr.html('<td></td>' +
                '<td><input type="text" maxlength="30" required name="name" class="form-control"></td>' +
                '<td>'+
                    '<div class="form-group">'+
                        '<input type="text" name="start_time" class="form-control datepicker" required="">'+
                    '</div>'+
                '</td>'+
                '<td>'+
                    '<div class="form-group" >'+
                        '<input type="text" name="end_time" class="form-control datepicker" required="">'+
                    '</div>'+
                '</td>'+
                '<td>' +
                '   <div class="btn-group-sm">' +
                '       <a type="button" class="btn btn-success col-lg-4 col-md-12 save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>' +
                '       <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>' +
                '   </div>' +
                '</td>');

            //tr.find(".datepicker").timepicker({showMeridian: false});
            //vals
            tr.find("[name='name']").val(name);
            tr.find("[name='start_time']").timepicker({
                defaultTime : start_time,
                minuteStep: 1,
                disableFocus: true,
                explicitMode : false,
                showMeridian: false
            });
            //tr.find("[name='start_time']").val(start_time);
            tr.find("[name='end_time']").timepicker({
                defaultTime : end_time,
                minuteStep: 1,
                disableFocus: true,
                explicitMode : false,
                showMeridian: false
            });
            //tr.find("[name='end_time']").val(end_time);

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post( "{{ route('msk_heyetler_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });

        function isValid(tr) {
            let name = tr.find("[name='name']"),
                start_time = tr.find("[name='start_time']"),
                end_time = tr.find("[name='end_time']"),
                status = true;

            name.css("border", "");
            start_time.css("border", "");
            end_time.css("border", "");

            if(name.val().trim().length > 60 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }
            if(start_time.val().trim().length > 60 || start_time.val().trim().length == 0){
                start_time.css("border", "1px dotted red");
                status = false;
            }
            if(end_time.val().trim().length > 60 || end_time.val().trim().length == 0){
                end_time.css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {
            tr.html('<td> </td>' +
                '<td>' + data['name']+'</td>' +
                '<td>' + data['start_time'] + '</td>' +
                '<td>' + data['end_time'] + '</td>' +
                '<td>' +
                    '<div class="btn-group-sm">'+data['buttons']+'</div>' +
                '</td>'
            );
            tr.attr("tr_id", data['id']);
            order();
        }

        function order() {
          $("#basicTable>tbody>tr").each(function(i){
              $(this).find("td:eq(0)").text(i+1);
          });
        }
    </script>
@endsection