@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-2 col-lg-1" style="float: right">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('msk_equipments',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('msk_equipments_add_edit_modal',['eqp' => 0]) }}')"
                           type="button" class="btn btn-success">
                                  <span class="p-t-5 p-b-5">
                                      <i class="pg-plus_circle fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">
                                {{ __('pages/devices.new_device') }}
                            </span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <form action="" method="get">
            <table class="table table-hover" id="basicTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('pages/devices.device_name') }}</th>
                    {{--<th>Modeli</th>--}}
                    {{--<th>Firmware versiyası</th>--}}
                    <th>{{ __('pages/devices.ip_address') }}</th>
                    {{--<th>Nömrəsi</th>--}}
                    <th>{{ __('pages/devices.type') }}</th>
                    <th>{{ __('pages/devices.device_type') }}</th>
                    <th>{{ __('pages/devices.corpus') }}</th>
                    {{--<th>Mərtəbə</th>--}}
                    <th>{{ __('pages/devices.room') }}</th>
                    {{--<th>Avadanlıq təyinatı</th>--}}
                    <th style="min-width: 180px;">{{ __('pages/devices.operation') }}</th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th></th>
                    <th>
                        <input type="text" name="name" placeholder="{{ __('pages/devices.device_name') }}" class="form-control form_find"
                               value="{{ request('name') }}">
                    </th>
                    {{--<th>--}}
                        {{--<input type="text" name="model" placeholder="Model adı" class="form-control form_find"--}}
                               {{--value="{{ request('model') }}">--}}
                    {{--</th>--}}
                    {{--<th>--}}
                        {{--<input type="text" name="version" placeholder="Versiya adı" class="form-control form_find"--}}
                               {{--value="{{ request('version') }}">--}}
                    {{--</th>--}}
                    <th>
                        <input type="text" name="ip" placeholder="{{ __('pages/devices.ip_address') }}" class="form-control form_find"
                               value="{{ request('ip') }}">
                    </th>
                    {{--<th>--}}
                        {{--<input type="text" name="number" placeholder="Nömrə adı" class="form-control form_find"--}}
                               {{--value="{{ request('number') }}">--}}
                    {{--</th>--}}
                    <th style="min-width: 180px;">
                        <select name="type" id="type" class="full-width" data-placement="{{ __('pages/devices.type') }}">
                            <option value="" selected></option>
                            @foreach(\App\Models\EquipmentTypes::realData()->get() as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th style="min-width: 180px;">
                        <select name="eqp_type" id="eqp_type" class="full-width" data-placement="{{ __('pages/devices.device_type') }}">
                            <option value="" selected></option>
                            @foreach(\App\Models\EqpInnerType::realData()->get() as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th>
                        <select name="corpus" id="corpus" class="full-width" data-placement="{{ __('pages/devices.corpus') }}">
                            <option value="" selected></option>
                            @foreach(\App\Models\Corpus::realData()->get() as $corpus)
                                <option value="{{ $corpus->id }}">{{ $corpus->name }}</option>
                            @endforeach
                        </select>
                    </th>
                    {{--<th>--}}
                        {{--<input type="text" name="floor" placeholder="Mərtəbə adı" class="form-control form_find"--}}
                               {{--value="{{ request('floor') }}">--}}
                    {{--</th>--}}
                    <th>
                        @php
                            $rooms = \App\Models\Room::realData()
                            ->join('equipment','equipment.room_id','rooms.id')
                            ->select('rooms.id','rooms.name')
                            ->get()
                        @endphp
                        <select name="rooms" id="rooms" class="full-width" data-placement="{{ __('pages/devices.room') }}">
                            <option value="" selected></option>
                            @foreach($rooms as $room)
                                <option value="{{ $room->id }}">{{ $room->name }}</option>
                            @endforeach
                        </select>
                    </th>
                    {{--<th>--}}
                        {{--<input type="text" name="destination" placeholder="Təyinat adı" class="form-control form_find"--}}
                               {{--value="{{ request('destination') }}">--}}
                    {{--</th>--}}
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($eqps as $eqp)
                    <tr data-tr-id="{{ $eqp->id }}">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $eqp->name }}</td>
                        {{--<td>{{ $eqp->model }}</td>--}}
                        {{--<td>{{ $eqp->version }}</td>--}}
                        <td>{{ $eqp->ip }}</td>
                        {{--<td>{{ $eqp->number }}</td>--}}
                        <td data-type="{{ $eqp->type }}">
                            {{ \App\Models\EquipmentTypes::realData()->find($eqp->type)->name }}
                        </td>
                        <td data-eqp-type="{{ $eqp->eqp_type }}">
                            @php
                                switch ($eqp->eqp_type){
                                    case '1' : echo('Giriş');
                                    break;
                                    case '2' : echo('Çıxış');
                                    break;
                                    case '3' : echo('Access Kontrol');
                                    break;
                                    case '4' : echo('Enroll');
                                    break;
                                }
                            @endphp
                        </td>
                        <td data-corpus="{{ $eqp->corpus->id }}">{{ $eqp->corpus->name }}</td>
                        {{--<td>{{ $eqp->floor }}</td>--}}
                        <td data-room="{{ $eqp['room_id']}}">{{ $eqp['room']['name'] }}</td>
                        {{--<td>{{ $eqp->destination }}</td>--}}
                        <td>
                            @if(\App\Library\Helper::has_priv('msk_equipments',\App\Library\Standarts::PRIV_CAN_EDIT))

                                <div class="btn-group-sm">
                                    <a type="button"
                                       href="javascript:openModal('{{ route('msk_equipments_add_edit_modal',['eqp' => $eqp->id]) }}')"
                                       class="btn btn-primary col-lg-4 col-md-12"><i class="fa fa-pencil"></i></a>

                                    <a type="button" url="{{ route('msk_equipments_delete',['eqp' => $eqp->id]) }}"
                                       class="btn btn-danger deleteAction col-lg-4 col-md-12"><i
                                                class="fa fa-trash-o"></i></a>
                                    @if($eqp->type == 1)
                                        <a type="button" href="{{ route('msk_equipments_refresh_data',['eqp' => $eqp->id]) }}" class="btn btn-info col-lg-4 col-md-12"><i class="fa fa-refresh"></i></a>
                                    @endif
                                </div>
                            @else
                                <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $eqps->appends($request->except('page')) }}
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        $('[name="corpus"]').select2({
            allowClear: true,
            placeholder: '{{ __('pages/devices.corpus') }}'
        }).select2('val',{{ request('corpus') }});

        $('[name="rooms"]').select2({
            allowClear: true,
            placeholder: '{{ __('pages/devices.room') }}',
        }).select2('val',{{ request('rooms') }});

        $('[name="type"]').select2({
            allowClear: true,
            placeholder: '{{ __('pages/devices.type') }}',
        }).select2('val',{{ request('type') }});

        $('[name="eqp_type"]').select2({
            allowClear:true,
            placeholder:'{{ __('pages/devices.device_type') }}'
        }).select2('val',{{ request('eqp_type') }});

        $('[name="corpus"],[name="rooms"],[name="type"],[name="eqp_type"]').on('change', function () {
            $(this).parents("form").submit();
        });
    </script>
@endsection
