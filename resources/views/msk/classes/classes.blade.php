@extends('base')

@section('container')
    <div class="col-lg-12 col-md-12">
        <div id="errors" class="row">

        </div>
        @if(\App\Library\Helper::has_priv('msk_classes',\App\Library\Standarts::PRIV_CAN_EDIT))
            <button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>
        @endif
        <table class="table table-hover" id="basicTable">
            <thead>
            <tr>
                <th>Sira</th>
                <th>Sinif</th>
                <th>Fənnlər</th>
                <th style="width: 185px;">Əməliyyat</th>
            </tr>
            </thead>
            <tbody>
            @foreach($classes as $class)
                <tr tr_id="{{ $class->id }}" subjects="{{ $class->subjects->pluck('id') }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $class->name }}</td>
                    <td>{{ implode(",", $class->subjects->pluck('name')->toArray()) }}</td>
                    <td>
                        @if(\App\Library\Helper::has_priv('msk_classes',\App\Library\Standarts::PRIV_CAN_EDIT))
                            <div class="btn-group-sm">
                                <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                                @php
                                    $hasClassInLetter = \App\Models\ClassLetter::realData()
                                    ->where('msk_class_id',$class->id)
                                    ->get();
                                @endphp
                                @if(!isset($hasClassInLetter[0]))
                                 <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </div>
                        @else
                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script type="text/html" id="row">
        <td></td>
        <td><input type="text" maxlength="60" required name="name" class="form-control"></td>
        <td>
            <select name="subjects[]" multiple class="full-width">
                @foreach(\App\Models\Subject::realData()->get() as $subject)
                    <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <div class="btn-group-sm">
                <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-pencil"></i></a>
                <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>
            </div>
        </td>
    </script>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>


    <script>
        $(".add_new").click(function () {
            let tbody = $("tbody");

            tbody.prepend('<tr tr_id="0" subjects="[]">' + $("#row").html() + '</tr>');
            tbody.find("tr:first").hide().show('fast');
            tbody.find("tr:first select").select2();

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });
            order();
        });

        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents("tr:eq(0)"),
                data = {},
                id = tr.attr("tr_id");

            if(isValid(tr)){
                tr.find("[name]").each(function () {
                    data[$(this).attr("name")] = $(this).val();
                });
                data['id'] = id;
                data['_token'] = _token;

                $.post( "{{ route('msk_classes_add_edit_action') }}", data, function( response ) {
                    if(response['status']=='error') $("#errors").html(response['errors']);
                    else saveTr(tr, response['data']);
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                name = tr.find("td:eq(1)").text().trim(),
                subjects = JSON.parse(tr.attr("subjects")),
                html = tr.html();

            tr.html($("#row").html());
            tr.find("select").select2();

            //vals
            tr.find("[name='name']").val(name);
            tr.find("[name='subjects[]']").val(subjects).trigger('change');

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".data_delete", function () {
            let tr = $(this).parents("tr:eq(0)"),
                id = tr.attr("tr_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsizin?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post( "{{ route('msk_classes_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order();
                                saveOrder();
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        });
        
        function isValid(tr) {
            let name = tr.find("[name='name']"),
                subjects = tr.find("[name='subjects[]']"),
                status = true;

            name.css("border", "");
            subjects.prev().css("border", "");

            if(name.val().trim().length > 60 || name.val().trim().length == 0){
                name.css("border", "1px dotted red");
                status = false;
            }
            if(subjects.val() == null){
                subjects.prev().css("border", "1px dotted red");
                status = false;
            }

            return status;
        }

        function saveTr(tr, data) {
            console.log(data['buttons']);
            tr.html('<td> </td>' +
                    '<td>'+data['name']+'</td>' +
                    '<td>'+data['subjects']+'</td>' +
                    '<td>' +
                        '<div class="btn-group-sm">' + data['buttons'] +
                        '</div>' +
                    '</td>'
                );
            tr.attr("tr_id", data['id']);
            tr.attr("subjects", JSON.stringify(data['subjects_id']));
            order();
            saveOrder();
        }

        function order() {
            $("#basicTable>tbody>tr").each(function(i){
                $(this).find("td:eq(0)").text(i+1);
            });
        }
        
        function saveOrder() {
            let i = 0, data = {};
            $("tbody tr").each(function () {
                i++;
                data[$(this).attr("tr_id")] = i;
            });

            $.post( "{{ route('msk_classes_order') }}", { data: data, _token: _token }, function( response ) {
                if(response['status']=='error') $("#errors").html(response['errors']);
            });
        }

        $(function () {
           $("#basicTable tbody").sortable({ helper: fixWidthHelper,
               cursor: 'pointer',
               revert: true,
               containment: 'parent',
               tolerance: 'pointer',
               update: function(event, ui) {
                   saveOrder();
               }
           });

            function fixWidthHelper(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            }
        });
    </script>
@endsection