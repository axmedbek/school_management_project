@extends('base')

@section('container')
    <div class="row">
        <div class="col-md-2 col-lg-1" style="float: right">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    @if(\App\Library\Helper::has_priv('str_students',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <a href="javascript:openModal('{{ route('str_students_add_edit',['user' => 0]) }}')" type="button" class="btn btn-success">
                                  <span class="p-t-5 p-b-5">
                                      <i class="pg-plus_circle fs-15"></i>
                                  </span>
                            <br>
                            <span class="fs-11 font-montserrat text-uppercase">Yeni</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
            <li class="{{ $request->get('type', 'active') == 'active' ? 'active':'' }}"><a href="?type=active" role="tab" aria-expanded="true" style='font-family:Segoe UI,sans-serif;font-weight: bold;'>Aktİv</a></li>
            <li class="{{ $request->get('type', 'active') == 'deactive' ? 'active':'' }}"><a href="?type=deactive" role="tab" aria-expanded="false" style='font-family:Segoe UI,sans-serif;font-weight: bold;'>Deaktİv</a></li>
        </ul>
        <form action="" method="get">
            <input type="hidden" name="type" value="{{ $request->get('type', 'active') }}">
            <table class="table table-hover" id="basicTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Ad Soyad Ata adı</th>
                        {{--<th>Doğum tarixi</th>--}}
                        {{--<th>Qeydiyyat tarixi</th>--}}
                        <th>Ev tel</th>
                        <th>Mobil tel</th>
                        <th>Email</th>
                        <th style="width: 185px;">Əməliyyat</th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <td></td>
                        <td><input class="form-control form_find" name="name" placeholder="Ad Soyad" value="{{ $request->get('name') }}" ></td>
                        {{--<td><input class="form-control form_find datepicker" name="birthday" placeholder="Doğum tarixi" value="{{ $request->get('birthday') }}" ></td>--}}
                        {{--<td><input class="form-control form_find datepicker" name="enroll_date" placeholder="Qeydiyyat tarixi" value="{{ $request->get('enroll_date') }}" ></td>--}}
                        <td><input class="form-control form_find" name="home_tel" placeholder="Ev tel" value="{{ $request->get('home_tel') }}" ></td>
                        <td><input class="form-control form_find" name="mobil_tel" placeholder="Mobil tel" value="{{ $request->get('mobil_tel') }}" ></td>
                        <td><input class="form-control form_find" name="email" placeholder="Email" value="{{ $request->get('email') }}" ></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr>
                        <td>{{ $students->perPage() * ($students->currentPage() - 1) + $loop->iteration }}</td>
                        <td>{{ $student->fullname() }}</td>
                        {{--<td>{{ empty($student->birthday) ? '' : date("d-m-Y", strtotime($student->birthday)) }}</td>--}}
                        {{--<td>{{ empty($student->enroll_date) ? '' : date("d-m-Y", strtotime($student->enroll_date)) }}</td>--}}
                        <td>{{ $student->home_tel }}</td>
                        <td>{{ $student->mobil_tel }}</td>
                        <td>{{ $student->email }}</td>
                        <td>
                            <div class="btn-group-sm">
                                <a type="button" href="javascript:openModal('{{ route('str_students_info',['student' => $student->id]) }}')" class="btn btn-info col-lg-4 col-md-12"><i class="fa fa-info-circle"></i></a>

                                @if(\App\Library\Helper::has_priv('str_students',\App\Library\Standarts::PRIV_CAN_EDIT))
                                    <a type="button" href="javascript:openModal('{{ route('str_students_add_edit',['student' => $student->id]) }}')" class="btn btn-primary col-lg-4 col-md-12"><i class="fa fa-pencil"></i></a>
                                    @if($student->id != \Illuminate\Support\Facades\Auth::user()->id)
                                        @if($request->get('type', 'active') == 'active')
                                            <a type="button" url="{{ route('str_students_delete',['student' => $student->id]) }}" class="btn btn-danger deleteAction col-lg-4 col-md-12" data-toggle="tooltip" title="İstifadəçini deaktiv edin"><i class="fa fa-close"></i></a>
                                        @else
                                            <a type="button" href="{{ route('str_students_delete',['student' => $student->id]) }}" class="btn btn-success col-lg-4 col-md-12"><i class="fa fa-check-circle"></i></a>
                                        @endif
                                     @endif
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $students->appends($request->except('page')) }}
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        #basicTable thead tr th{
            font-family: Segoe UI, sans-serif !important;
            font-weight: bold;
            color: #075177;
            text-align: center;
            /*border: 1px #373636 solid;*/
        }
        #basicTable thead tr td{
            font-family: Segoe UI, sans-serif !important;
            font-weight: bold;
            text-align: center;
           /* border: 1px #373636 solid;*/
        }
        #basicTable tbody tr td{
            font-family: Segoe UI, sans-serif !important;
            text-align: center;
            /*font-weight: bold;*/
            /*border: 1px #373636 solid;*/
        }
        /*.breadcrumb li a{*/
            /*font-family:Segoe UI,sans-serif;*/
            /*font-weight: bold !important;*/
            /*text-transform: capitalize;*/
            /*font-size: 17px !important;*/
            /*color: #075177 !important;*/
        /*}*/
        /*.breadcrumb li p{*/
            /*font-family:Segoe UI,sans-serif;*/
            /*font-weight: bold !important;*/
            /*text-transform: capitalize;*/
            /*font-size: 17px !important;*/
        /*}*/

        .select2-drop.select2-display-none.select2-with-searchbox.select2-drop-active {
            z-index:999999999 !important;
        }

        .select2-container--open{
            z-index:9999999 !important;
        }

        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
            z-index:9999999 !important;
        }

    </style>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        $(".datepicker").datepicker({format: 'dd-mm-yyyy'});
        $("#basicTable>tbody>tr").find('a[data-toggle="tooltip"]').tooltip();
    </script>
@endsection