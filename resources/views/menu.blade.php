<style>
    .sidebar-menu ul li:first-child{
        margin-top: 10px;
    }
</style>
<div class="sidebar-menu">
    <!-- BEGIN SIDEBAR MENU ITEMS-->
    <ul class="menu-items">
        {!! \App\Library\Helper::makeMenu(\App\Library\Standarts::$modules) !!}
    </ul>
    <div class="clearfix"></div>
</div>