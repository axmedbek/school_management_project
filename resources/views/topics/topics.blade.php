@extends('base')

@section('container')
    <div class="row">
            <form class="col-md-12" role="form">
                <div class="col-md-3 form-group form-group-default form-group-default-select2">
                    <label class="">Sinif</label>
                    <select class="full-width" data-placeholder="Sinif" id="classes">
                        @foreach(\App\Models\MskClass::realData()->orderBy('order')->get() as $class)
                            <option value="{{ $class->id }}">{{ $class->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group form-group-default form-group-default-select2">
                    <label class="">Fənn</label>
                    <input class="full-width" data-placeholder="Fənn" id="select2Subjects">
                </div>
            </form>
    </div>
    <div id="page-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>
        $("#classes").select2().on('change',function () {
            $("#select2Subjects").select2("val", null);
        });

        $("#select2Subjects").select2({
            placeholder: "Fənn",
            allowClear : true,
            ajax: {
                url: '{{ route('get_class_subject') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        class_id: $("#classes").val(),
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on('change', function () {
            openPage('#page-inside', '{{ route("topics_page") }}', {class_id: $("#classes").val(), subject_id: $("#select2Subjects").val()});
        });
    </script>
@endsection