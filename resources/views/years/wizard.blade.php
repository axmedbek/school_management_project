@extends('base')

@section('container')
    <style>
        #tab_li li a span{
           font-weight: bold;
            color: #303333;
        }
    </style>
    <div id="errors" class="row">

    </div>
    <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" id="tab_li">
        <li class="active">
            <a data-toggle="tab" href="#year"><i class="pg-calender tab-icon"></i> <span>Tədris ili</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
        <li>
            <a data-toggle="tab" href="#teachers"><i class="fa fa-male tab-icon"></i> <span>Müəllimlər</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#students"><i class="fa fa-graduation-cap tab-icon"></i> <span>Şagirdlər</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#groups"><i class="fa fa-sort-alpha-asc tab-icon"></i> <span>Siniflər/Qruplar</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#program"><i class="pg-note tab-icon"></i> <span>Dərs Programı</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#class_times"><i class="pg-note tab-icon"></i> <span>Dərs Saatları</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#non_working_days"><i class="pg-note tab-icon"></i> <span>Qeyri iş günləri</span> <i class="fa fa-check-circle edited-icon"></i></a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane padding-20 slide-left active" id="year">
            <div class="padding-30">
                <form role="form">
                    <div class="form-group-attached">
                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <div class="form-group form-group-default required">
                                    <label>Başlama tarixi</label>
                                    <input type="text" class="form-control datepicker" name="start_date" value="{{ $saved ? $saved['start_date'] : ( $lastYear ? date("d-m-Y", strtotime("+1 years", strtotime($lastYear['start_date']))) : '') }}" required="">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-group-default required">
                                    <label>Bitmə tarixi</label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="{{ $saved ? $saved['end_date'] : ( $lastYear ? date("d-m-Y", strtotime("+1 years", strtotime($lastYear['end_date']))) : '') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover" id="table_period">
                            <thead>
                                <tr>
                                    <th>Period</th>
                                    <th>Başlama tarixi</th>
                                    <th>Bitmə tarixi</th>
                                    <th><button type="button" class="btn btn-success pull-right btn-xs add_new_period"><i class="pg-plus_circle"> </i></button></th>
                                </tr>
                            </thead>
                            <tbody>
                                <script type="text/html" id="table_period_row">
                                    <tr>
                                        <td><input type="text" maxlength="255" name="period[name][]" class="form-control"></td>
                                        <td><input type="text" name="period[start_date][]" class="form-control datepicker"></td>
                                        <td><input type="text" maxlength="60" name="period[end_date][]" class="form-control datepicker"></td>
                                        <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                    </tr>
                                </script>
                                @if($saved)
                                    @if(isset($saved['period']))
                                        @foreach($saved['period'] as $period)
                                            <tr>
                                                <td><input type="text" maxlength="255" name="period[name][]" class="form-control" value="{{ $period['name'] }}"></td>
                                                <td><input type="text" name="period[start_date][]" class="form-control datepicker" value="{{ $period['start_date'] }}"></td>
                                                <td><input type="text" maxlength="60" name="period[end_date][]" class="form-control datepicker" value="{{ $period['end_date'] }}"></td>
                                                <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @else
                                    @if($lastYear)
                                        @foreach($lastYear->periods as $period)
                                            <tr>
                                                <td><input type="text" maxlength="255" name="period[name][]" class="form-control" value="{{ $period['name']}}"></td>
                                                <td><input type="text" name="period[start_date][]" class="form-control datepicker" value="{{ date("d-m-Y", strtotime("+1 years", strtotime($period['start_date']))) }}"></td>
                                                <td><input type="text" maxlength="60" name="period[end_date][]" class="form-control datepicker" value="{{ date("d-m-Y", strtotime("+1 years", strtotime($period['end_date']))) }}"></td>
                                                <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane padding-20 slide-left" id="teachers">
            <select multiple="multiple" id="teachers_select" name="teachers[]">
                @foreach(\App\User::realData('teacher')->get() as $teacher)
                    <option value='{{ $teacher->id }}' {{ $saved && isset($saved['deleted_teachers']) && in_array($teacher->id, $saved['deleted_teachers'])?'selected':'' }}>{{ $teacher->fullname() }}</option>
                @endforeach
            </select>
        </div>
        <div class="tab-pane padding-20 slide-left" id="students">
            <select multiple="multiple" id="students_select" name="students[]">
                @php
                    $studentsHaveGroup = [];
                @endphp

                @foreach($corpusLetters as $key => $corpusLetter)
                    <optgroup label="{{ $key }}">
                        @foreach($corpusLetter->groups as $group)
                            @foreach($group->students as $student)
                                @php $studentsHaveGroup[] = $student->id; @endphp
                                <option value='{{ $student->id }}'>{{ $student->fullname() }}</option>
                            @endforeach
                        @endforeach
                    </optgroup>
                @endforeach

                <optgroup label="Bağlı olmayanlar">
                    @foreach(\App\User::realData('student')->whereNotIn('id', $studentsHaveGroup)->get() as $student)
                        <option value='{{ $student->id }}'>{{ $student->fullname() }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
        <div class="tab-pane padding-20 slide-left" id="groups">
            <div class="col-md-12">
                <div class="col-md-2 form-group form-group-default form-group-default-select2" style="text-align: center;">
                    <label class="" style="margin-left: 35%">Korpus</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="corpus">
                        @foreach($corpuses as $corpus)
                            <option value="{{ $corpus->id }}">{{ $corpus->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 form-group form-group-default form-group-default-select2" style="text-align: center;">
                    <label class="" style="margin-left: 35%">Sinif</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="class">
                        @foreach($classes as $class)
                            <option value="{{ $class->id }}">{{ $class->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-12" style="padding: 0px;">
                    <div class="col-md-6 b-r b-dashed b-grey">
                        <div class="panel panel-transparent">
                            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="ul_groups_letter">
                                <h5 style="padding-left:15px;margin:5px;border-bottom:2px solid #d9d9d9;">Hərflər <i class="pg-plus_circle" id="add_new_letter" style="cursor: pointer;color: #10cfbd;"> </i></h5>
                                @php
                                    $savedClasses = $saved ? json_decode($saved['classes'],true) : false;
                                    $letterId = 0;
                                @endphp

                                @foreach($corpuses as $corpus)
                                    @foreach($classes as $class)
                                        @if(!$savedClasses && isset($allLetterGroups[$corpus->id]) && isset($allLetterGroups[$corpus->id][$class->id]))
                                            @foreach($allLetterGroups[$corpus->id][$class->id] as $letter)
                                                <li corpus_id="{{ $corpus->id }}" class_id="{{ $class->id }}" letter_id="{{ $letter->id }}">
                                                    <a data-toggle="tab" href="#tab_letter{{ $letter->id }}" style="width:120px">
                                                        <input type="text" class="form-control" value="{{ $letter->name }}" style="display: inline-block;width: 70%;">
                                                        <i style="font-size: 15px;cursor: pointer;color: red;" class="fa fa-trash letter_delete"></i>
                                                    </a>
                                                </li>
                                            @endforeach
                                        @elseif(isset($savedClasses[$corpus->id]) && isset($savedClasses[$corpus->id][$class->id]))
                                            @foreach($savedClasses[$corpus->id][$class->id] as $letter => $classData)
                                                <li corpus_id="{{ $corpus->id }}" class_id="{{ $class->id }}" letter_id="{{ ++$letterId }}">
                                                    <a data-toggle="tab" href="#tab_letter{{ $letterId }}" style="width:120px">
                                                        <input type="text" class="form-control" value="{{ $letter }}" style="display: inline-block;width: 70%;">
                                                        <i style="font-size: 15px;cursor: pointer;color: red;" class="fa fa-trash letter_delete"></i>
                                                    </a>
                                                </li>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endforeach

                                <script type="text/html" id="group_tab_li">
                                    <li corpus_id="" class_id="" letter_id="0">
                                        <a data-toggle="tab" href="#tab_letter" style="width:120px">
                                            <input type="text" class="form-control" value="" style="display: inline-block;width: 70%;">
                                            <i style="font-size: 15px;cursor: pointer;color: red;" class="fa fa-trash letter_delete"></i>
                                        </a>
                                    </li>
                                </script>
                                <script type="text/html" id="group_tab_pane">
                                    <div class="tab-pane" id="" letter_id="0">
                                        <button type="button" class="btn btn-success pull-right add_new_group"><i class="pg-plus_circle"> </i> Yeni</button>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Group Adi</th>
                                                <th style="width: 140px;">Tipi</th>
                                                <th>Əməliyyat</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </script>
                            </ul>
                            <div class="tab-content bg-white" id="group_tabs">
                                    @php
                                        $letterId = 0;
                                        $groupId = 0;
                                    @endphp
                                    @foreach($corpuses as $corpus)
                                        @foreach($classes as $class)
                                            @if(!$savedClasses && isset($allLetterGroups[$corpus->id]) && isset($allLetterGroups[$corpus->id][$class->id]))
                                                @foreach($allLetterGroups[$corpus->id][$class->id] as $letter)
                                                    <div class="tab-pane" id="tab_letter{{ $letter->id }}" letter_id="{{ $letter->id }}">
                                                        <button type="button" class="btn btn-success pull-right add_new_group"><i class="pg-plus_circle"> </i> Yeni</button>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Group Adi</th>
                                                                <th style="width: 140px;">Tipi</th>
                                                                <th>Əməliyyat</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($letter->groups as $group)
                                                                <tr group_id="{{ $group->id }}" group_type="{{ $group->group_type_id }}">
                                                                    <td><input type="text" class="form-control" value="{{ $group->name }}" name="name"></td>
                                                                    <td>
                                                                        <select class="form-control" name="group_type">
                                                                            @foreach($groupTypes as $groupType)
                                                                                <option value="{{ $groupType->id }}" {{ $group->group_type_id == $groupType->id ? 'selected':'' }}>{{ $groupType->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                    <td style="min-width: 130px">
                                                                        <label class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" name="isMainGroup" value="{{ empty($group['isMainGroup']) ? '0' : $group['isMainGroup']}}" {{ $group['isMainGroup'] == 1 ? 'checked' : '' }}  style="display: none;" onclick="controlIsMainGroup(this)">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                        <div class="btn-group">
                                                                            <a type="button" class="btn btn-danger btn-sm data_delete"><i class="fa fa-trash-o"></i></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @endforeach
                                            @elseif(isset($savedClasses[$corpus->id]) && isset($savedClasses[$corpus->id][$class->id]))
                                                    @foreach($savedClasses[$corpus->id][$class->id] as $letter => $classData)
                                                        <div class="tab-pane" id="tab_letter{{ ++$letterId }}" letter_id="{{ $letterId }}">
                                                            <button type="button" class="btn btn-success pull-right add_new_group"><i class="pg-plus_circle"> </i> Yeni</button>
                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Group Adi</th>
                                                                    <th style="width: 140px;">Tipi</th>
                                                                    <th>Əməliyyat</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($classData as $group)
                                                                    <tr group_id="{{ ++$groupId }}">
                                                                        <td><input type="text" class="form-control" value="{{ $group['group_name'] }}" name="name"></td>
                                                                        <td>
                                                                            <select class="form-control" name="group_type">
                                                                                @foreach($groupTypes as $groupType)
                                                                                    <option value="{{ $groupType->id }}" {{ $group['group_type'] == $groupType->id ? 'selected':'' }}>{{ $groupType->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </td>
                                                                        <td style="min-width: 130px">
                                                                            <label class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" name="isMainGroup" value="{{ empty($group['isMainGroup']) ? '0' : $group['isMainGroup']}}" {{ $group['isMainGroup'] == 1 ? 'checked' : '' }} style="display: none;" onclick="controlIsMainGroup(this)">
                                                                                <span class="custom-control-indicator"></span>
                                                                            </label>
                                                                            <div class="btn-group">
                                                                                <a type="button" class="btn btn-danger btn-sm data_delete"><i class="fa fa-trash-o"></i></a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    @endforeach
                                                @endif
                                        @endforeach
                                    @endforeach
                                     <script type="text/html" id="group_group_row">
                                         <tr group_id="">
                                             <td><input type="text" class="form-control" value="" name="name"></td>
                                             <td>
                                                 <select class="form-control" name="group_type">
                                                     @foreach($groupTypes as $groupType)
                                                         <option value="{{ $groupType->id }}">{{ $groupType->name }}</option>
                                                     @endforeach
                                                 </select>
                                             </td>
                                             <td style="min-width: 130px">
                                                 <label class="custom-control custom-checkbox">
                                                     <input type="checkbox" class="custom-control-input" name="isMainGroup" value="0" style="display: none;" onclick="controlIsMainGroup(this)">
                                                     <span class="custom-control-indicator"></span>
                                                 </label>
                                                 <div class="btn-group">
                                                     <a type="button" class="btn btn-danger btn-sm data_delete"><i class="fa fa-trash-o"></i></a>
                                                 </div>
                                             </td>
                                         </tr>
                                     </script>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6" id="students_of_group">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr role="row"><th class="sorting_disabled">Grupun şagirdləri</th></tr>
                                </thead>
                                <thead>
                                    <tr><th><input type="text" class="form-control find-here2"></th></tr>
                                </thead>
                                <tbody>
                                    @php
                                        $selectedStudents = [];
                                        $groupId = 0;
                                    @endphp

                                    @foreach($corpuses as $corpus)
                                        @foreach($classes as $class)
                                            @if(!$savedClasses && isset($allLetterGroups[$corpus->id]) && isset($allLetterGroups[$corpus->id][$class->id]))
                                                @foreach($allLetterGroups[$corpus->id][$class->id] as $letter)
                                                    @foreach($letter->groups as $group)
                                                        @foreach($group->students as $student)
                                                            @php $selectedStudents[] = $student->id @endphp
                                                            <tr group_id="{{ $group->id }}" student_id="{{ $student->id }}" style="display: none">
                                                                <td>{{ $student->fullname() }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @elseif(isset($savedClasses[$corpus->id]) && isset($savedClasses[$corpus->id][$class->id]))
                                                @foreach($savedClasses[$corpus->id][$class->id] as $letter)
                                                    @foreach($letter as $group)
                                                        @php $groupId++; @endphp
                                                        @foreach($group['students'] as $student)
                                                            @php $selectedStudents[] = $student @endphp
                                                            <tr group_id="{{ $groupId }}" student_id="{{ $student }}" style="display: none">
                                                                <td>{{ \App\User::realData('student')->where('id', $student)->first()->fullname() }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6" id="free_students">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr role="row"><th class="sorting_disabled">Sinifi olmayan şagirdlər</th></tr>
                                </thead>
                                <thead>
                                    <tr><th><input type="text" class="form-control find-here"></th></tr>
                                </thead>
                                <tbody>
                                    @foreach(\App\User::realData('student')->whereNotIn('id', $selectedStudents)->get() as $student)
                                        <tr group_id="" student_id="{{ $student->id }}">
                                            <td>{{ $student->fullname() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane padding-20 slide-left" id="program">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <label class="custom-control custom-checkbox col-md-4">
                        Bu hissə dəyişəcək
                        <input type="checkbox" class="custom-control-input" name="notfication" style="display: none;" onchange="chk_chnage($(this))" {{ $saved && $saved['program_chk']==1 ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                    </label>
                    <div class="col-md-4" style="display: none;"><input class="form-control col-md-4" name="notf_text" value="{{ $saved ? $saved['program_text'] : '' }}" placeholder="Mətn"></div>
                </div>
                <form class="col-md-12" role="form">
                    <div class="col-md-3 form-group form-group-default form-group-default-select2">
                        <label class="">Sinif</label>
                        <select class="full-width" data-placeholder="Sinif" id="classes">
                            @foreach(\App\Models\MskClass::realData()->orderBy('order')->get() as $class)
                                <option value="{{ $class->id }}">{{ $class->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 form-group form-group-default form-group-default-select2">
                        <label class="">Fənn</label>
                        <input class="full-width" data-placeholder="Fənn" id="select2Subjects">
                    </div>
                </form>
            </div>
            <div id="page-inside">

            </div>
        </div>
        <div class="tab-pane padding-20 slide-left" id="class_times">
            <div class="col-md-12" style="margin-bottom: 10px;">
                <label class="custom-control custom-checkbox col-md-4">
                    Bu hissə dəyişəcək
                    <input type="checkbox" class="custom-control-input" name="notfication" style="display: none;" onchange="chk_chnage($(this))" {{ $saved && $saved['class_times_chk']==1 ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                </label>
                <div class="col-md-4" style="display: none;"><input class="form-control col-md-4" name="notf_text" value="{{ $saved ? $saved['class_times_text'] : '' }}" placeholder="Mətn"></div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-transparent">
                    @php $corpuses = \App\Models\Corpus::realData()->get() @endphp
                    <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                        <h4 style="padding-left:15px;margin:5px;border-bottom:2px solid #d9d9d9;">Korpuslar</h4>
                        @foreach($corpuses as $corpus)
                            <li corpus_id="{{ $corpus->id }}" class="{{ $loop->iteration == 1 ? 'active':'' }}">
                                <a data-toggle="tab" href="#tab{{ $corpus->id }}">{{ $corpus->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content bg-white">
                        @foreach($corpuses as $corpus)
                            <div class="tab-pane {{ $loop->iteration == 1 ? 'active':'' }}" id="tab{{ $corpus->id }}">
                                <table class="table table-hover" id="basicTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Başlama vaxtı</th>
                                        <th>Bitmə vaxtı</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($corpus->class_times as $class_time)
                                        <tr tr_id="{{ $class_time->id }}">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ date('h:i',strtotime($class_time->start_time)) }}</td>
                                            <td>{{ date('h:i',strtotime($class_time->end_time)) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane padding-20 slide-left" id="non_working_days">
            <div class="col-md-12" style="margin-bottom: 10px;">
                <label class="custom-control custom-checkbox col-md-4">
                    Bu hissə dəyişəcək
                    <input type="checkbox" class="custom-control-input" name="notfication" style="display: none;" onchange="chk_chnage($(this))" {{ $saved && $saved['non_working_days_chk']==1 ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                </label>
                <div class="col-md-4" style="display: none;"><input class="form-control col-md-4" name="notf_text" value="{{ $saved ? $saved['non_working_days_text'] : '' }}" placeholder="Mətn"></div>
            </div>
            <div class="col-lg-12">

                <table class="table table-hover" id="basicTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Adı</th>
                        <th>Başlama tarİxİ</th>
                        <th>Bİtmə tarİxİ</th>
                        <th>Tİpİ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($lastYear)
                        @foreach($lastYear->holidays as $holiday)
                            <tr tr_id="{{ $holiday->id }}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $holiday->name }}</td>
                                <td>{{ date('d-m-Y',strtotime($holiday->end_date)) }}</td>
                                <td>{{ date('d-m-Y',strtotime($holiday->start_date)) }}</td>
                                <td selected_id="{{ $holiday->type }}">{{ $holiday->type==1 ? 'Tətil' : 'Qeyri iş günü' }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>

        <div class="padding-20 bg-white" id="action_buttons">
            <ul class="pager wizard">
                <li class="next">
                    <button class="btn btn-primary btn-cons btn-animated from-left fa fa-arrow-right pull-right" type="button">
                        <span>İrəli</span>
                    </button>
                </li>
                <li class="next finish">
                    <button class="btn btn-complete btn-cons btn-animated from-left fa fa-save pull-right" type="button">
                        <span>Yadda saxla</span>
                    </button>
                </li>
                <li class="next finish">
                    <button class="btn btn-success btn-cons btn-animated from-left fa fa-check pull-right" type="button" style="display: none">
                        <span>Tətbiq et</span>
                    </button>
                </li>
                <li class="previous">
                    <button class="btn btn-default disabled btn-cons btn-animated from-left fa fa-arrow-left pull-right" type="button">
                        <span>Geri</span>
                    </button>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/plugins/multi-select/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        .edited-icon{
            font-size: 15px;
            color: red;
            margin-left: 3px;
        }
        .nav-tabs > li > a{
            padding: 15px 5px !important;
        }
        #students_of_group tbody tr,#free_students tbody tr{
            cursor: pointer;
        }
        #students_of_group,#free_students{
            max-height: 500px;
            overflow-y: auto;
        }
        .custom-header{
            font-size: 20px;
            background-color: #949494;
            padding: 5px;
            color: white;
            text-align: center;
        }
        .ms-container{
            width: 100% !important;
            padding-top: 10px;
        }
        .ms-list{
            height: 400px !important;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('assets/plugins/multi-select/jquery.quicksearch.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/multi-select/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
    <script>

        function controlIsMainGroup(element) {
            if ($(element).val() == '0') {
                $(element).val('1');
                $(element).attr('checked', 'checked');
            }
            else {
                $(element).val('0');
            }
            var tr = $(element).parents('tr'),
                table = $(element).parents('table'),
                group_id = tr.attr('group_id');

            table.find('tbody>tr').not('[group_id="' + group_id + '"]').each(function () {
                $(this).find('input[name="isMainGroup"]').removeAttr('checked');
                $(this).find('input[name="isMainGroup"]').val("0");
            });
        }

        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
            return function( elem ) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        $('.datepicker').datepicker({format: 'dd-mm-yyyy'});
        $('.select2').select2();

        //chk
        function chk_chnage(t) {
            if(t.is(":checked")){
                t.parents("label:eq(0)").next().show();
            }
            else{
                t.parents("label:eq(0)").next().hide();
            }
        }
        $("[name='notfication']").trigger("change");

        //year
        $("#year").on("click", ".delete_row", function () {
            $(this).parents("tr:eq(0)").remove();
        });

        $("#year .add_new_period").click(function () {
            $("#year #table_period tbody").append($("#year #table_period_row").html());
            $("#year #table_period tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
        });

        //teacher
        $('#teachers_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
            selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
            selectableFooter: "<div class='custom-header'>Aktiv müəllimlər</div>",
            selectionFooter: "<div class='custom-header'>Deaktiv müəllimlər</div>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
        //student
        $('#students_select').multiSelect({
            selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
            selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
            selectableFooter: "<div class='custom-header'>Aktiv şagirdlər</div>",
            selectionFooter: "<div class='custom-header'>Deaktiv şagirdlər</div>",
            selectableOptgroup: true,
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(values){
                for(var n in values){
                    $("#free_students tbody tr[student_id='"+values[n]+"'],#students_of_group tbody tr[student_id='"+values[n]+"']").remove();
                }
            },
            afterDeselect: function(values){
                for(var n in values){
                    $("#free_students tbody").append("<tr student_id='"+values[n]+"'>" +
                        "<td>"+$("#students_select option[value='"+values[n]+"']").text()+"</td>" +
                        "</tr>");
                }
            }
        });
        $('#students_select').multiSelect('select', JSON.parse('{!! $saved && isset($saved['deleted_students']) ? json_encode($saved['deleted_students']) :'[]' !!}'));
        //program
        $("#classes").select2().on('change',function () {
            $("#select2Subjects").select2("val", null);
        });

        $("#select2Subjects").select2({
            placeholder: "Fənn",
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('get_class_subject') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        class_id: $("#classes").val(),
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on('change', function () {
            openPage('#page-inside', '{{ route("topics_page") }}', {class_id: $("#classes").val(), subject_id: $("#select2Subjects").val(), 'change': false});
        });
        //groups
        $("#corpus").on("change", function () {
            groupCorpusClassChange();
        });
        $("#class").on("change", function () {
            groupCorpusClassChange();
        }).trigger("change");
        function groupCorpusClassChange() {
            let corpus_id = $("#corpus").val(),
                class_id = $("#class").val();

            $("#groups li[corpus_id][class_id]").hide();
            $("#groups li[corpus_id='"+corpus_id+"'][class_id='"+class_id+"']").show();

            //tabs
            let clk = $("#groups li[corpus_id='"+corpus_id+"'][class_id='"+class_id+"']:eq(0) a");
            if(clk.length) $("#groups li[corpus_id='"+corpus_id+"'][class_id='"+class_id+"']:eq(0) a").trigger("click");
            else {
                $("#group_tabs .tab-pane.active").removeClass("active");
                $("#groups li.active").removeClass("active");
            }

            //users
            $("#students_of_group tbody tr").hide();
            $("#group_tabs .tab-pane tr.active").removeClass("active");
        }

        $("#groups").on("click", ".letter_delete", function () {
            let letter_id = $(this).parents("li:eq(0)").attr("letter_id");

            $(this).parents("li:eq(0)").remove();
            $("#groups .tab-pane[letter_id='"+letter_id+"']").remove();
        });
        $("#groups").on("click", ".data_delete", function () {
            $(this).parents("tr:eq(0)").remove();
        });

        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 10; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

        $("#add_new_letter").click(function () {
           let id = makeid(),
               li = $("#group_tab_li").html(),
               pane = $("#group_tab_pane").html();

            $("#ul_groups_letter").append(li);
            $("#ul_groups_letter li:last").attr("corpus_id", $("#corpus").val());
            $("#ul_groups_letter li:last").attr("class_id", $("#class").val());
            $("#ul_groups_letter li:last").attr("letter_id", id);
            $("#ul_groups_letter li:last a:eq(0)").attr("href", "#tab_letter" + id);

            $("#group_tabs").append(pane);
            $("#group_tabs .tab-pane:last").attr("letter_id", id);
            $("#group_tabs .tab-pane:last").attr("id", "tab_letter" + id);
        });

        $("#groups").on("click", ".add_new_group", function () {
            let row = $("#group_group_row").html(),
                id = makeid(),
                table = $(this).next().find("tbody");

            table.append(row);
            table.find("tr:last").attr("group_id", id);
        });

        $("#group_tabs").on("click", "table tr[group_id]", function () {
            let group_id = $(this).attr("group_id");

            $("#groups table tr[group_id].active").removeClass("active");
            $(this).addClass("active");

            $("#students_of_group tbody tr").hide();
            $("#students_of_group tbody tr[group_id='"+group_id+"']").show();
        });

        $("#free_students tbody").on("click", "tr", function () {
           let group_id = $("#group_tabs .tab-pane.active tr[group_id].active").attr("group_id"),
               tr = $(this).clone();

           if(group_id){
               $(this).remove();

               $("#students_of_group tbody").append(tr);
               $("#students_of_group tbody tr:last").attr("group_id", group_id);
           }

        });

        $("#students_of_group tbody").on("click", "tr", function () {
            let tr = $(this).clone();

            $(this).remove();

            $("#free_students tbody").append(tr);
            $("#free_students tbody tr:last").attr("group_id", "");
        });

        $("#ul_groups_letter").on("click", "li", function () {
            $("#students_of_group tbody tr").hide();
        });

        //finder
        let time = 300, action = false, table = false, txt = "";
        $(".find-here").keyup(function() {
            table = $(this).parents("table:eq(0)");
            txt = $(this).val();

            if(action) clearTimeout(action);
            action = setTimeout(find, time);
        });
        $(".find-here2").keyup(function() {
            table = $(this).parents("table:eq(0)");
            txt = $(this).val();

            if(action) clearTimeout(action);
            action = setTimeout(find2, time);
        });

        function find() {
            let tr = table.find("tbody:eq(0)").find("tr:contains('"+txt+"')");

            table.find("tbody:eq(0)").find("tr:visible").hide();
            tr.show();
        }
        function find2() {
            let group_id = $("#group_tabs .tab-pane.active tr[group_id].active").attr("group_id"),
                tr = table.find("tbody:eq(0)").find("tr[group_id='"+group_id+"']:contains('"+txt+"')");

            table.find("tbody:eq(0)").find("tr:visible").hide();
            tr.show();
        }

        //save
        function getFormData(check) {
            let data = {},
                ii = 0,
                error = false;

            data['_token'] = _token;

            //year periods
            let start_date = $("#year input[name='start_date']"),
                end_date = $("#year input[name='end_date']");

            if(check){
                start_date.css("border", "");
                end_date.css("border", "");

                if(start_date.val().trim() == ""){
                    start_date.css("border", "1px dotted red");
                    error = true;
                }
                if(end_date.val().trim() == ""){
                    end_date.css("border", "1px dotted red");
                    error = true;
                }
            }
            data['start_date'] = start_date.val();
            data['end_date'] = end_date.val();

            data['period'] = [];
            $("#table_period tbody tr").each(function () {
                let name = $(this).find('[name="period[name][]"]'),
                    start_date = $(this).find('[name="period[start_date][]"]'),
                    end_date = $(this).find('[name="period[end_date][]"]');

                if(check){
                    name.css("border", "");
                    start_date.css("border", "");
                    end_date.css("border", "");

                    if(name.val().trim() == ""){
                        name.css("border", "1px dotted red");
                        error = true;
                    }
                    if(start_date.val().trim() == ""){
                        start_date.css("border", "1px dotted red");
                        error = true;
                    }
                    if(end_date.val().trim() == ""){
                        end_date.css("border", "1px dotted red");
                        error = true;
                    }
                }

                data['period'].push({name: name.val(), start_date: start_date.val(), end_date: end_date.val()});
            });
            if(error){ $("a[href='#year']").trigger("click"); return; }

            data['deleted_teachers'] = [];
            let tt = $("#teachers [name='teachers[]']").val();
            if(tt != null){
                for(var n in tt)
                    data['deleted_teachers'].push(tt[n]);
            }

            data['deleted_students'] = [];
            tt = $("#students [name='students[]']").val();
            if(tt != null){
                for(var n in tt)
                    data['deleted_students'].push(tt[n]);
            }

            let classes = {};
            $("#ul_groups_letter li").each(function () {
                let corpus_id = $(this).attr("corpus_id"),
                    class_id = $(this).attr("class_id"),
                    letter_id = $(this).attr("letter_id"),
                    letter = $(this).find("input");

                if(check){
                    letter.css("border", "");
                    if(letter.val().trim() == ""){
                        letter.css("border", "1px dotted red");
                        error = true;
                    }
                }

                if(classes[corpus_id] == undefined) classes[corpus_id] = {};
                if(classes[corpus_id][class_id] == undefined) classes[corpus_id][class_id] = {};
                classes[corpus_id][class_id][letter.val()] = [];

                $("#group_tabs .tab-pane[letter_id='"+letter_id+"'] table tbody tr").each(function () {
                    let group = $(this).find("[name='name']"),
                        group_type = $(this).find("[name='group_type']"),
                        group_id = $(this).attr("group_id"),
                        isMainGroup = $(this).find("input[name='isMainGroup']"),
                        students = [];

                    if(check){
                        group.css("border", "");
                        group_type.css("border", "");
                        if(group.val().trim() == ""){
                            group.css("border", "1px dotted red");
                            error = true;
                        }
                        if((group_type.val() > 0) == false){
                            group_type.css("border", "1px dotted red");
                            error = true;
                        }
                    }

                    $("#students_of_group tbody tr[group_id='"+group_id+"']").each(function () {
                        students.push($(this).attr("student_id"));
                    });

                    classes[corpus_id][class_id][letter.val()].push({group_name: group.val(), group_type: group_type.val(),isMainGroup:isMainGroup.val(), students: students});
                });

            });
            data['classes'] = JSON.stringify(classes);

            //console.log(data['classes'] );
            
            if(error){ $("a[href='#groups']").trigger("click"); return; }

            //msks
            let program = $("#program [name='notf_text']"),
                class_times = $("#class_times [name='notf_text']"),
                non_working_days = $("#non_working_days [name='notf_text']");

            if(check){
                program.css("border", "");
                class_times.css("border", "");
                non_working_days.css("border", "");

                if($("#program [name='notfication']").is(":checked") && program.val().trim() == ""){
                    program.css("border", "1px dotted red");
                    $("a[href='#program']").trigger("click");
                    error = true;
                }
                if($("#class_times [name='notfication']").is(":checked") && class_times.val().trim() == ""){
                    class_times.css("border", "1px dotted red");
                    $("a[href='#class_times']").trigger("click");
                    error = true;
                }
                if($("#non_working_days [name='notfication']").is(":checked") && non_working_days.val().trim() == ""){
                    non_working_days.css("border", "1px dotted red");
                    $("a[href='#non_working_days']").trigger("click");
                    error = true;
                }
            }

            data['program_chk'] = ($("#program [name='notfication']").is(":checked")?1:0);
            data['program_text'] = program.val();

            data['class_times_chk'] = ($("#class_times [name='notfication']").is(":checked")?1:0);
            data['class_times_text'] = class_times.val();

            data['non_working_days_chk'] = ($("#non_working_days [name='notfication']").is(":checked")?1:0);
            data['non_working_days_text'] = non_working_days.val();

            data['error'] = error;

            return data;
        }

        $("#action_buttons .fa-check").click(function () {
            let data = getFormData(true);
            //console.log(data);

            if(data['error']) return;

            $.post( "{{ route('wizard_years_add_action') }}", data, function( response ) {
                if(response['status'] == 'error') $("#errors").html(response['errors']);
                else {
                    location.href = "{{ route('years') }}";
                }
            });
        });

        $("#action_buttons .fa-save").click(function () {
           let data = getFormData(false);

           //console.log(data);

            $.post( "{{ route('wizard_years_save_action') }}", data, function( response ) {
                if(response['status'] == 'error') {
                    $('body').pgNotification({
                        style: 'flip',
                        message: response['errors'],
                        position: "top",
                        timeout: 0,
                        type: "error"
                    }).show();
                }
                else {
                    $('body').pgNotification({
                        style: 'flip',
                        message: "Yadda saxlandı.",
                        position: "top",
                        timeout: 0,
                        type: "success"
                    }).show();
                }
            });
        });

        //tabs buttons
        let last_tab = "#year";
        $("#tab_li li a").click(function () {
           let tab_name = $(this).attr("href");

           $("#tab_li li a[href='"+last_tab+"'] .fa-check-circle").css("color", "#10cfbd");
           last_tab = tab_name;

           $("#action_buttons .fa-arrow-left").removeClass("disabled");
           $("#action_buttons .fa-arrow-right").removeClass("disabled");
           $("#action_buttons .fa-check").hide();

           switch(tab_name){
               case "#year":
                   $("#action_buttons .fa-arrow-left").addClass("disabled");
                   break;
               case "#non_working_days":
                   $("#action_buttons .fa-arrow-right").addClass("disabled");
                   $("#action_buttons .fa-check").show();
                   break;
           }
        });

        $("#action_buttons .fa-arrow-left").click(function () {
            $("#tab_li li.active").prev().find("a").click();
        });
        $("#action_buttons .fa-arrow-right").click(function () {
            $("#tab_li li.active").next().find("a").click();
        });
    </script>
@endsection