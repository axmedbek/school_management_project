@extends('base')

@section('container')
    <div class="row">
        @if(\Illuminate\Support\Facades\Session::has('errorMessage'))
            <div class="col-md-6 col-lg-6" style="float: left">
                <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('errorMessage') }}</div>
            </div>
        @endif

        <div class="col-md-4 col-lg-2" style="float: right">
            <div class="btn-group btn-group-justified">
                @if(\App\Library\Helper::has_priv('years',\App\Library\Standarts::PRIV_CAN_EDIT))
                <div class="btn-group">
                    <a href="javascript:openModal('{{ route('years_add_edit',['year' => 0]) }}')" type="button" class="btn btn-success">
                              <span class="p-t-5 p-b-5">
                                  <i class="pg-plus_circle fs-15"></i>
                              </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Yeni</span>
                    </a>
                </div>
                <div class="btn-group">
                    <a href="{{ route('years_wizard') }}" type="button" class="btn">
                              <span class="p-t-5 p-b-5">
                                  <i class="pg-plus_circle fs-15"></i>
                              </span>
                        <br>
                        <span class="fs-11 font-montserrat text-uppercase">Wizard</span>
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($years as $year)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading{{ $year->id }}" data-year-id="{{ $year->id }}">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $year->id }}" aria-expanded="false" aria-controls="collapse{{ $year->id }}" class="collapsed" style="font-size: 15px;font-family: 'Segoe UI'">
                                    Tədrİs İlİ ( {{ date("d-m-Y", strtotime($year->start_date)) }} - {{ date("d-m-Y", strtotime($year->end_date)) }} )
                                    @if(\App\Library\Helper::has_priv('years',\App\Library\Standarts::PRIV_CAN_EDIT))
                                        <i url="{{ route('years_delete',['year' => $year->id]) }}" class="fa fa-trash deleteAction" style="color: red;margin-left: 20px;margin-right: 5px;"></i>

                                        <i class="fa fa-edit edit-year" style="color: #6d5cae" onclick="event.stopPropagation();openModal('{{ route('years_add_edit',['year' => $year->id]) }}');"></i>
                                    @endif
                                </a>
                                <div class="inline">
                                    <span>{{ $year->active == 1 ? 'Aktiv' : 'Deaktiv' }}</span>
                                    <input name="isActiveYear" id="rtl-switch" type="checkbox" {{ $year->active == 1 ? 'checked' : '' }}  data-size="small" data-init-plugin="switchery" data-switchery="true" style="display: none;">
                                </div>
                            </h4>
                        </div>
                        <div id="collapse{{ $year->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $year->id }}" aria-expanded="false">
                            <div class="panel-body">
                                <table class="table table-hover" id="basicTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Rüb</th>
                                        <th>Başlama tarix</th>
                                        <th>Bitmə tarix</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($year->periods as $period)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $period->name }}</td>
                                                <td>{{ date("d-m-Y", strtotime($period->start_date)) }}</td>
                                                <td>{{ date("d-m-Y", strtotime($period->end_date)) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .gallery-item{
            padding: 0;
            margin: 10px;
        }
    </style>
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>

    <script>

        $("input[name='isActiveYear']").next().on('click',function(){
            let isActiveYearInput = $(this).prev('input'),
                active = isActiveYearInput.is(":checked") ? 1 : 0,
                year_id = $(this).parents('div:eq(1)').attr('data-year-id'),
                formData = new FormData();

            formData.append('id',year_id);
            formData.append('active',active);
            formData.append('_token',_token);

            //console.log(active);

            $.ajax({
                url: "{{ route('years_do_deactive') }}",
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') {
                        location.reload();
                    }
                    else {
                        var isActive = response.active == 0 ? 'Deaktiv' : 'Aktiv';
                        isActiveYearInput.prev('span').text(isActive);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        setTimeout(function () {
            $('.alert').slideUp();
        },3000);
    </script>
@endsection