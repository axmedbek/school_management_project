<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>İstifadəçi qrupları <span class="semi-bold">{{ $id > 0 ? 'Düzəliş et' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default required">
                                <label>Grup adi</label>
                                <input type="text" class="form-control" value="{{ $group['group_name'] }}" name="name" maxlength="50" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Modullar</label>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            @foreach (\App\Library\Standarts::$modules as $typeK => $type)
                                @if( isset($type['child']) )
                                    <div class="col-md-12">
                                        <label class="col-md-7"><i class="{{ $type['icon'] }}"></i> {{ $type['name'] }}:</label>
                                        @foreach ($type['child'] as $K => $t)
                                            <div class="col-md-12" style="padding-left: 50px">
                                                <label class="col-md-5">{{ $t['name'] }}:</label>
                                                <div class="col-md-7 radio radio-success">
                                                    <input type="radio" id="{{ $t['route'] }}1" name="available_modules[{{ $t['route'] }}]" id="{{ $t['route'] }}" value="1" checked /> <label for="{{ $t['route'] }}1">Görmür</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" id="{{ $t['route'] }}2" name="available_modules[{{ $t['route'] }}]" id="{{ $t['route'] }}" value="2" {{ $id >0 && $group->getModulePriv($t['route']) == 2 ? 'checked' : '' }} /> <label for="{{ $t['route'] }}2">Görür</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" id="{{ $t['route'] }}3" name="available_modules[{{ $t['route'] }}]" id="{{ $t['route'] }}" value="3" {{ $id >0 && $group->getModulePriv($t['route']) == 3 ? 'checked' : '' }} /> <label for="{{ $t['route'] }}3">Əlavə edir</label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="col-md-12">
                                        <label class="col-md-5"><i class="{{ $type['icon'] }}"></i> {{ $type['name'] }}:</label>
                                        <div class="col-md-7 radio radio-success">
                                            <input type="radio" id="{{ $type['route'] }}1" name="available_modules[{{ $type['route'] }}]" id="{{ $type['route'] }}" value="1" checked /> <label for="{{ $type['route'] }}1">Görmür</label>
                                            &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" id="{{ $type['route'] }}2" name="available_modules[{{ $type['route'] }}]" id="{{ $type['route'] }}" value="2" {{ $id >0 && $group->getModulePriv($type['route']) == 2 ? 'checked' : '' }} /> <label for="{{ $type['route'] }}2">Görür</label>
                                            &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" id="{{ $type['route'] }}3" name="available_modules[{{ $type['route'] }}]" id="{{ $type['route'] }}" value="3" {{ $id >0 && $group->getModulePriv($type['route']) == 3 ? 'checked' : '' }} /> <label for="{{ $type['route'] }}3">Əlavə edir</label>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            $.ajax({
                url: "{{ route('msk_groups_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>