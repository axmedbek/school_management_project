<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>{{ __('pages/devices.device') }} <span class="semi-bold">{{ $id > 0
                    ? __('pages/devices.update_modal')
                     : __('pages/devices.add_modal') }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="eqpForm" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">{{ __('pages/devices.device_name') }}</label>
                            <input type="text" name="name" class="form-control" value="{{ $eqp['name'] }}">
                        </div>
                        <div class="col-md-6">
                            <label for="model">{{ __('pages/devices.model') }}</label>
                            <input type="text" name="model" class="form-control" value="{{ $eqp['model'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="version">{{ __('pages/devices.version') }}</label>
                            <input type="text" name="version" class="form-control" value="{{ $eqp['version'] }}">
                        </div>
                        <div class="col-md-6">
                            <label for="ip">{{ __('pages/devices.ip_address') }}</label>
                            <input type="text" name="ip" class="form-control ip" value="{{ $eqp['ip'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="number">{{ __('pages/devices.number') }}</label>
                            <input type="number" name="number" class="form-control" value="{{ $eqp['number'] }}">
                        </div>
                        <div class="col-md-6">
                            <label for="type">{{ __('pages/devices.type') }}</label>
                            <input type="text" name="type" style="width: 100%;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="eqp_type">{{ __('pages/devices.device_type') }}</label>
                            <select name="eqp_type" id="eqp_type"
                                    data-placement="{{ __('pages/devices.device_type') }}"
                                    style="width: 100%;">
                                <option value="" selected></option>
                                @foreach(\App\Models\EqpInnerType::realData()->get() as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="corpus">{{ __('pages/devices.corpus') }}</label><br>
                            <input type="text" name="corpus" style="width: 100%;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="floor">{{ __('pages/devices.floor') }}</label>
                            <input type="number" name="floor" class="form-control" value="{{ $eqp['floor'] }}">
                        </div>
                        <div class="col-md-6">
                            <label for="room">{{ __('pages/devices.room') }}</label><br>
                            <input type="text" name="room" style="width: 100%;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <span class="error-message-equipment" style="color:tomato"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="destination">{{ __('pages/devices.destination') }}</label>
                            <input type="text" name="destination" class="form-control" value="{{ $eqp['destination'] }}"
                                   placeholder="{{ __('pages/devices.choose_destination') }}">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-success " typeof="submit"><i class="fa fa-save"></i>
                                {{ __('pages/devices.save') }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                {{ __('pages/devices.close') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<script>

    //$('.ip').inputmask('999.999.999.999');

    $("#eqpForm").find('input[name="corpus"]').select2('val', null);
    $("#eqpForm").find('input[name="room"]').select2('val', null);


    $("#eqpForm").find('input[name="type"]').select2({
        allowClear: true,
        placeholder: '{{ __('pages/devices.type') }}',
        ajax: {
            url: '{{ route('msk_equipments_get_data') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'getEquipmentType',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        }
    });
    if ({{ $type_id }} !=
    0
    )
    {
        $("#eqpForm").find('input[name="type"]').select2('data', {'id': {{ $type_id }} , 'text': '{{ $type_name }}'});
    }

    $("#eqpForm").find('select[name="eqp_type"]').select2({
        allowClear: true,
        placeholder: '{{ __('pages/devices.device_type') }}'
    });
    if ({{ $eqpTypeId }} !=
    0
    )
    {
        $("#eqpForm").find('input[name="eqp_type"]').select2(
            'data',
            {
                'id': {{ $eqpTypeId }} ,
                'text': '{{ $eqpTypeName }}'
            }
        );
    }


    $("#eqpForm").find('input[name="corpus"]').select2({
        minimumResultsForSearch: -1,
        placeholder: '{{ __('pages/devices.corpus') }}',
        ajax: {
            url: '{{ route('msk_equipments_get_data') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'getCorpuses',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change', function () {
        $("#eqpForm").find('input[name="room"]').select2('val', null);
        $(this).prev('div').css('border', '');
    }).select2('data', {id:{{ $corpus_id }}, text: '{{ $corpus_name }}'});

    $("#eqpForm").find('input[name="room"]').select2({
        allowClear: true,
        placeholder: '{{ __('pages/devices.room') }}',
        ajax: {
            url: '{{ route('msk_equipments_get_data') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'getRooms',
                    corpus_id: $('input[name="corpus"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });
    if ({{ $room_id }} !=
    0
    )
    {
        $("#eqpForm").find('input[name="room"]').select2('data', {id:{{ $room_id }}, text: '{{ $room_name }}'})
            .on('change', function () {
                $(this).prev('div').css('border', '');
            });
    }


    $("#eqpForm").on("submit", function (e) {
        e.preventDefault();

        var formData = new FormData(this);
        formData.append('_token', _token);

        //console.log(checkValidation());

        if (checkValidation()) {
            $.ajax({
                type: 'POST',
                url: '{{ route('msk_equipments_add_edit_action',['eqp' => $id]) }}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    location.reload();
                },
                error: function (response) {
                    console.log("error");
                    console.log(response);
                }
            });
        }
    });


    //check form validation

    function checkValidation() {
        var name = $("#eqpForm").find('input[name="name"]'),
            type = $("#eqpForm").find('input[name="type"]'),
            room = $("#eqpForm").find('input[name="room"]'),
            ip = $("#eqpForm").find('input[name="ip"]'),
            eqp_type = $("#eqpForm").find('input[name="eqp_type"]'),
            corpus = $("#eqpForm").find('input[name="corpus"]'),
            errorStatus = true;

        name.css('border', '');
        type.prev('div').css('border', '');
        corpus.css('border', '');
        ip.css('border', '');
        eqp_type.prev('div').css('border', '');
        // room.css('border','');


        if (name.val().length == 0) {
            name.css('border', '1px dotted red');
            errorStatus = false;
        }
        if (type.val() == 0) {
            type.prev('div').css('border', '1px dotted red');
            errorStatus = false;
        }
        if (corpus.val() == 0) {
            corpus.prev('div').css('border', '1px dotted red');
            errorStatus = false;
        }
        // if (room.val() == 0) {
        //     room.prev('div').css('border', '1px dotted red');
        //     errorStatus = false;
        // }

        if (type.val() == 1 || type.val() == 2) {

            //console.log(eqp_type.val());

            if (ip.val() == '') {
                ip.css('border', '1px dotted red');
                errorStatus = false;
            }
            if (type.val() == 1) {
                if (eqp_type.val() == '') {
                    eqp_type.prev('div').css('border', '1px dotted red');
                    errorStatus = false;
                }
            }
        }

        return errorStatus;

    }

</script>

