<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>İstifadəçi <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <input type="file" class="form-control" name="thumb">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $user['name'] }}" name="name" maxlength="30" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                <input type="text" class="form-control" value="{{ $user['surname'] }}" name="surname" maxlength="30">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                <input type="text" class="form-control" value="{{ $user['middle_name'] }}" name="middle_name" maxlength="30">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default required">
                                <label>Login</label>
                                <input type="text" class="form-control" value="{{ $user['username'] }}" name="username"  maxlength="20" autocomplete="false"
                                       onfocus="this.removeAttribute('readonly');" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default ">
                                <label>Parol</label>
                                <input type="password" class="form-control" name="password" minlength="5" maxlength="20" autocomplete="false" {{ $user['id'] > 0 ? '':'required' }}>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default ">
                                <label>Email</label>
                                <input type="text" class="form-control" value="{{ $user['email'] }}" name="email" >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                <input type="text" class="form-control" value="{{ $user['mobil_tel'] }}" name="mobil_tel" maxlength="20">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                <input type="text" class="form-control" value="{{ $user['home_tel'] }}" name="home_tel" maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $user['gender'] == 'm' || $id == 0?'checked':'' }} value="m" name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $user['gender'] == 'f'?'checked':'' }} value="f" name="gender" id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" style="display:{{ empty($user['enroll_date']) ? 'none' : ''}};">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ date("d-m-Y",strtotime($user['enroll_date'])) }}
                                {{--<input type="text" class="form-control datepicker" value="{{ empty($teacher['enroll_date']) ?'':date("d-m-Y",strtotime($teacher['enroll_date'])) }}" name="enroll_date">--}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                <input type="text" class="form-control datepicker" value="{{ empty($user['birthday']) ?'':date("d-m-Y",strtotime($user['birthday'])) }}" name="birthday">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                <input type="text" class="form-control" value="{{ $user['birth_place'] }}" name="birth_place">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Şəhər</label>
                                <input type="text" class="full-width" name="lived_city"
                                       data-placeholder="Şəhər">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Rayon</label>
                                <input type="text" class="full-width" name="lived_region"
                                       data-placeholder="Rayon">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Ünvan</label>
                                <input type="text" class="form-control" value="{{ $user['current_address'] }}" name="current_address" maxlength="255">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>İstifadəçi qrupu</label>
                                <select name="group_id" class="full-width" required style="border: none;">
                                    @foreach (\App\Models\Group::all() as $type)
                                        <option value="{{ $type['id'] }}" {{ $type['id'] == $user['group_id'] ? 'selected' : '' }}> {{ $type['group_name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Yadda Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            error = checkOnlyEnglishCharUsing([
                'input[name="username"]',
                'input[name="password"]'
            ]);

            if (error) return;
            $.ajax({
                url: "{{ route('str_users_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});

    $("#myModal").find('input[name="lived_city"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_cities',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change',function () {
        $("#myModal").find('input[name="lived_region"]').select2('val',null);
    });

    $("#myModal").find('input[name="lived_region"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_regions',
                    city_id: $("#myModal").find('input[name="lived_city"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });

    function checkOnlyEnglishCharUsing(elements){
        for (var i = 0 ; i < elements.length ; i ++ ) {
            if($(elements[i]).val().match(/[^\u0000-\u007F]+/)){
                $.confirm({
                    title: 'Şrift xətası',
                    content: 'Zəhmət olmasa ingilis şriftlərindən istifadə edin',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Təkrar',
                            btnClass: 'btn-red',
                            action: function(){
                            }
                        },
                        Bağla: function () {
                        }
                    }
                });
                return true;
            }
        }
        return false;
    }

    @if($user['lived_city'] > 0)
        $("#myModal").find('input[name="lived_city"]').select2('data', {id: "{{ $user->lived_city }}", text: "{{ $user->livedCity->name }}" });
    @endif
    @if($user['lived_region'] > 0)
        $("#myModal").find('input[name="lived_region"]').select2('data', {id: "{{ $user->lived_region }}", text: "{{ $user->livedRegion->name }}" });
    @endif
</script>