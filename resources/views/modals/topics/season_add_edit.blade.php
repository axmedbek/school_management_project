<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Fəsil <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <input type="hidden" name="class_id" value="{{ $class_id }}">
                    <input type="hidden" name="subject_id" value="{{ $subject_id }}">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $season['name'] }}" name="name" maxlength="255" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Grup tipleri</label>
                                <select class="full-width" data-placeholder="Grup tipleri" multiple name="group_types[]" required>
                                    @php
                                        $groupTypeIds = $id > 0 ? $season->group_types->pluck('id')->toArray() : [];
                                    @endphp
                                    @foreach(\App\Models\GroupType::realData()->get() as $type)
                                        <option  {{ ($id == 0 && $type->default == 1) || in_array($type->id, $groupTypeIds) ? 'selected':'' }} value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>

    let isCopyBtn = $
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);
            data.append('isCopy',{{ $isCopy }});
            data.append('copySeasonId',{{ $copySeasonId }});

            $.ajax({
                url: "{{ route('topic_season_add_edit_action',['season' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else {
                        $('#myModal').modal('hide');
                        openPage('#page-inside', '{{ route("topics_page") }}', {class_id: {{ $class_id }}, subject_id: {{ $subject_id }} });
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $('#myModal select').select2();
</script>