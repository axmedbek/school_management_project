<div class="modal-dialog">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Tədris ili <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default required">
                                <label>Başlama tarixi</label>
                                <input type="text" class="form-control datepicker" value="{{ empty($year['start_date']) ? '' : date("d-m-Y", strtotime($year['start_date'])) }}" name="start_date" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default required">
                                <label>Bitmə tarixi</label>
                                <input type="text" class="form-control datepicker" value="{{ empty($year['end_date']) ? '' : date("d-m-Y", strtotime($year['end_date'])) }}" name="end_date" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover" id="table_period">
                                <thead>
                                <tr>
                                    <th>Period</th>
                                    <th>Başlama tarixi</th>
                                    <th>Bitmə tarixi</th>
                                    <th><button type="button" class="btn btn-success pull-right btn-xs add_new_period"><i class="pg-plus_circle"> </i></button></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <script type="text/html" id="table_period_row">
                                        <tr>
                                            <td><input type="text" maxlength="255" name="period[name][]" class="form-control"></td>
                                            <td><input type="text" name="period[start_date][]" class="form-control datepicker"></td>
                                            <td><input type="text" maxlength="60" name="period[end_date][]" class="form-control datepicker"></td>
                                            <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                        </tr>
                                    </script>

                                @if($year != null)
                                    @foreach($year->periods as $period)
                                        <tr>
                                            <td><input type="text" maxlength="255" name="period[name][]" class="form-control" value="{{ $period->name }}"></td>
                                            <td><input type="text" name="period[start_date][]" class="form-control datepicker" value="{{ empty($period['start_date']) ? '' : date("d-m-Y", strtotime($period['start_date'])) }}"></td>
                                            <td><input type="text" maxlength="60" name="period[end_date][]" class="form-control datepicker" value="{{ empty($period['end_date']) ? '' : date("d-m-Y", strtotime($period['end_date'])) }}"></td>
                                            <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]),
                error = false;;
            data.append('_token',_token);

            $("#myModal #table_period tbody tr").each(function () {
                let tr = $(this),
                    name = tr.find("[name='period[name][]']"),
                    start_date = tr.find("[name='period[start_date][]']"),
                    end_date = tr.find("[name='period[end_date][]']");

                name.css("border","");
                start_date.css("border","");
                end_date.css("border","");

                if(name.val().trim() == ""){ name.css("border","1px solid red"); error = true; }
                if(start_date.val().trim() == ""){ start_date.css("border","1px solid red"); error = true; }
                if(end_date.val().trim() == ""){ end_date.css("border","1px solid red"); error = true; }
            });

            if(error) return false;

            $.ajax({
                url: "{{ route('years_add_edit_action',['year' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});

    //period
    $("#myModal").on("click", ".delete_row", function () {
        $(this).parents("tr:eq(0)").remove();
    });

    $("#myModal .add_new_period").click(function () {
        $("#myModal #table_period tbody").append($("#myModal #table_period_row").html());
        $("#myModal #table_period tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });
</script>