<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Personal <span class="semi-bold">Ətraflı</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <img src="{{ asset(\App\Library\Standarts::$userImageDir . $staff['thumb']) }}" height="180px" width="100%">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Adi</label>
                                {{ empty($staff['name']) ? '-' : $staff['name'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                {{ empty($staff['surname']) ? '-' : $staff['surname'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                {{ empty($staff['middle_name']) ? '-' : $staff['middle_name'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                {{ empty($staff['email']) ? '-' : $staff['email'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                {{ empty($staff['mobil_tel']) ? '-' : $staff['mobil_tel'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                {{ empty($staff['home_tel']) ? '-' : $staff['home_tel'] }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-1 control-label" style="margin-left: 10px;margin-top: 10px;">Cinsi</label>
                        <div class="col-sm-3">
                            <div class="radio radio-success">
                                <input type="radio" {{ $staff['gender'] == 'm' ?'checked':'' }} disabled value="m" name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $staff['gender'] == 'f'?'checked':'' }} disabled value="f" name="gender" id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ empty($staff['enroll_date']) ?'-':date("d-m-Y",strtotime($staff['enroll_date'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                {{ empty($staff['birthday']) ?'-':date("d-m-Y",strtotime($staff['birthday'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                {{ empty($staff['birth_place']) ? '-' : $staff['birth_place'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Vəzifə</label>
                                {{ empty($staff['position']) ? '-' : $staff['position'] }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Personal heyəti</label>
                                @php $personal_heyeti = \App\Models\MskHeyetler::find($staff['personal_heyeti']); @endphp
                                <span>{{ empty($personal_heyeti['name']) ? '-' : $personal_heyeti['name'] }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı şəhər</label>
                                {{ empty($staff['lived_city']) ? '-' : \App\Models\MskCities::realData()->find($staff['lived_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı rayon</label>
                                {{ empty($staff['lived_region']) ? '-' : \App\Models\MskRegions::realData()->find($staff['lived_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı ünvan</label>
                                {{ empty($staff['lived_address']) ? '-' : $staff['lived_address'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                {{ empty($staff['current_city']) ? '-' : \App\Models\MskCities::realData()->find($staff['current_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                {{ empty($staff['current_region']) ? '-' : \App\Models\MskRegions::realData()->find($staff['current_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                {{ empty($staff['current_address']) ? '-' : $staff['current_address'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-study"><span>Təhsil</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-languages" style="display: none;"><span>Dillər</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-labot-activity"><span>Əmək fəaliyyəti</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-family" style="display: none;"><span>Ailə</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-study">
                                    <table class="table table-hover" id="table_study">
                                        <thead>
                                        <tr>
                                            <th>Müəssisə</th>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>Sənəd</th>
                                            <th>Sənədin nömrəsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($staff->person_studies as $study)
                                                <tr>
                                                    <td>{{ $study->place }}</td>
                                                    <td style="min-width: 120px;">{{ date("d-m-Y", strtotime($study->start_date)) }}</td>
                                                    <td style="min-width: 120px;">{{ date("d-m-Y", strtotime($study->end_date)) }}</td>
                                                    <td>{{ $study->document }}</td>
                                                    <td>{{ $study->document_number }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-languages">
                                    <table class="table table-hover" id="table_language">
                                        <thead>
                                        <tr>
                                            <th>Dil</th>
                                            <th>Dərəcə</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($staff->person_languages as $language)
                                                <tr>
                                                    <td>{{ $language->name }}</td>
                                                    <td>{{ $language->mark_type->name }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-labot-activity">
                                    <table class="table table-hover" id="table_labor_activity">
                                        <thead>
                                        <tr>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>İş yeri</th>
                                            <th>Vəzifə</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($staff->person_labot_activities as $laborActivity)
                                                <tr>
                                                    <td>{{ date("d-m-Y", strtotime($laborActivity->start_date)) }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($laborActivity->end_date)) }}</td>
                                                    <td>{{ $laborActivity->workplace }}</td>
                                                    <td>{{ $laborActivity->position }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-family">
                                    <table class="table table-hover" id="table_family">
                                        <thead>
                                        <tr>
                                            <th>Qohumluq</th>
                                            <th>S.A.A</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($staff->person_families as $family)
                                                <tr>
                                                    <td>{{ $family->msk_relation->name }}</td>

                                                    @php $parentUser = \App\User::where('id',$family['parent_id'])->first(); @endphp

                                                    <td>{{ $parentUser->fullname() }} (Mob:{{ $parentUser->mobil_tel }} - {{ $parentUser->home_tel }})</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            $.ajax({
                url: "{{ route('str_users_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>