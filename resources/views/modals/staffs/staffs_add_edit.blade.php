<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Personal <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <input type="file" class="form-control" name="thumb">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $staff['name'] }}" name="name" maxlength="30" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                <input type="text" class="form-control" value="{{ $staff['surname'] }}" name="surname" maxlength="30">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                <input type="text" class="form-control" value="{{ $staff['middle_name'] }}" name="middle_name" maxlength="30">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="email" class="form-control" value="{{ $staff['email'] }}" name="email">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                <input type="text" class="form-control" value="{{ $staff['mobil_tel'] }}" name="mobil_tel" maxlength="20">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                <input type="text" class="form-control" value="{{ $staff['home_tel'] }}" name="home_tel" maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-1 control-label" style="margin-left: 10px;margin-top: 10px;">Cinsi</label>
                        <div class="col-sm-3">
                            <div class="radio radio-success">
                                <input type="radio" {{ $staff['gender'] == 'm' || $id == 0?'checked':'' }} value="m" name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $staff['gender'] == 'f'?'checked':'' }} value="f" name="gender" id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group form-group-default">
                                <label>Vəzifə</label>
                                <input type="text" class="form-control" value="{{ $staff['position'] }}" name="position" maxlength="60">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" style="display:{{ empty($staff['enroll_date']) ? 'none' : ''}};">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ date("d-m-Y",strtotime($staff['enroll_date'])) }}
                                {{--<input type="text" class="form-control datepicker" value="{{ empty($teacher['enroll_date']) ?'':date("d-m-Y",strtotime($teacher['enroll_date'])) }}" name="enroll_date">--}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                <input type="text" class="form-control datepicker" value="{{ empty($staff['birthday']) ?'':date("d-m-Y",strtotime($staff['birthday'])) }}" name="birthday">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                <input type="text" class="form-control" value="{{ $staff['birth_place'] }}" name="birth_place">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı şəhər</label>
                                <input type="text" class="full-width" name="lived_city"
                                       data-placeholder="Yaşadığı şəhər">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı rayon</label>
                                <input type="text" class="full-width" name="lived_region"
                                       data-placeholder="Yaşadığı rayon">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı ünvan</label>
                                <input type="text" class="form-control" value="{{ $staff['lived_address'] }}"
                                       name="lived_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                <input type="text" class="full-width" name="current_city"
                                       data-placeholder="Faktiki şəhər">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                <input type="text" class="full-width" name="current_region"
                                       data-placeholder="Faktiki rayon">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                <input type="text" class="form-control" value="{{ $staff['current_address'] }}"
                                       name="current_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Personal heyəti</label>
                                <select name="personal_heyeti" id="personal_heyeti" class="form-control">
                                    <option value="" disabled selected>Personal heyətini seçin</option>
                                    @foreach(\App\Models\MskHeyetler::realData()->get() as $heyet)
                                        <option value="{{ $heyet['id'] }}">{{ $heyet['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-study"><span>Təhsil</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-languages" style="display: none;"><span>Dillər</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-labot-activity"><span>Əmək fəaliyyəti</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-family" style="display: none;"><span>Ailə</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div class="tab-pane active" id="tab-study">
                                    <table class="table table-hover" id="table_study">
                                        <thead>
                                            <tr>
                                                <th>Təhsil Müəssisə</th>
                                                <th>Başlama tarixi</th>
                                                <th>Bitmə tarixi</th>
                                                <th>Sənəd</th>
                                                <th>Sənədin nömrəsi</th>
                                                <th><button type="button" class="btn btn-success pull-right btn-xs add_new_study"><i class="pg-plus_circle"> </i></button></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/html" id="table_study_row">
                                                <tr>
                                                    <td><input type="text" name="study[place][]" class="form-control"></td>
                                                    <td><input type="text" name="study[start_date][]" class="form-control datepicker"></td>
                                                    <td><input type="text" name="study[end_date][]" class="form-control datepicker"></td>
                                                    <td><input type="text" maxlength="60" name="study[document][]" class="form-control"></td>
                                                    <td><input type="text" maxlength="60" name="study[document_number][]" class="form-control"></td>
                                                    <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                                </tr>
                                            </script>

                                            @if($staff != null)
                                                @foreach($staff->person_studies as $study)
                                                    <tr>
                                                        <td><input type="text" name="study[place][]" class="form-control" value="{{ $study->place }}"></td>
                                                        <td><input type="text" name="study[start_date][]" class="form-control datepicker" value="{{ date("d-m-Y", strtotime($study->start_date)) }}"></td>
                                                        <td><input type="text" name="study[end_date][]" class="form-control datepicker" value="{{ date("d-m-Y", strtotime($study->end_date)) }}"></td>
                                                        <td><input type="text" maxlength="60" name="study[document][]" class="form-control" value="{{ $study->document }}"></td>
                                                        <td><input type="text" maxlength="60" name="study[document_number][]" class="form-control" value="{{ $study->document_number }}"></td>
                                                        <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-languages">
                                    <table class="table table-hover" id="table_language">
                                        <thead>
                                        <tr>
                                            <th>Dil</th>
                                            <th>Dərəcə</th>
                                            <th><button type="button" class="btn btn-success pull-right btn-xs add_new_language"><i class="pg-plus_circle"> </i></button></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/html" id="table_language_row">
                                                <tr>
                                                    <td><input type="text" name="language[name][]" class="form-control"></td>
                                                    <td>
                                                        <select class="form-control" name="language[mark][]">
                                                            @foreach(\App\Models\MarkType::realData()->get() as $mark)
                                                                <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                                </tr>
                                            </script>

                                        @if($staff != null)
                                            @foreach($staff->person_languages as $language)
                                                <tr>
                                                    <td><input type="text" name="language[name][]" class="form-control" value="{{ $language->name }}"></td>
                                                    <td>
                                                        <select class="form-control" name="language[mark][]">
                                                            @foreach(\App\Models\MarkType::realData()->get() as $mark)
                                                                <option value="{{ $mark->id }}" {{ $mark->id == $language->mark_type_id ? 'selected':'' }}>{{ $mark->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-labot-activity">
                                    <table class="table table-hover" id="table_labor_activity">
                                        <thead>
                                        <tr>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>İş yeri</th>
                                            <th>Vəzifə</th>
                                            <th><button type="button" class="btn btn-success pull-right btn-xs add_new_labor_activity"><i class="pg-plus_circle"> </i></button></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_labor_activity_row">
                                            <tr>
                                                <td><input type="text" name="labor_activity[start_date][]" class="form-control datepicker"></td>
                                                <td><input type="text" name="labor_activity[end_date][]" class="form-control datepicker"></td>
                                                <td><input type="text" maxlength="60" name="labor_activity[workplace][]" class="form-control"></td>
                                                <td><input type="text" maxlength="60" name="labor_activity[position][]" class="form-control"></td>
                                                <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                            </tr>
                                        </script>

                                        @if($staff != null)
                                            @foreach($staff->person_labot_activities as $laborActivity)
                                                <tr>
                                                    <td><input type="text" name="labor_activity[start_date][]" class="form-control datepicker" value="{{ date("d-m-Y", strtotime($laborActivity->start_date)) }}"></td>
                                                    <td><input type="text" name="labor_activity[end_date][]" class="form-control datepicker" value="{{ date("d-m-Y", strtotime($laborActivity->end_date)) }}"></td>
                                                    <td><input type="text" maxlength="60" name="labor_activity[workplace][]" class="form-control" value="{{ $laborActivity->workplace }}"></td>
                                                    <td><input type="text" maxlength="60" name="labor_activity[position][]" class="form-control" value="{{ $laborActivity->position }}"></td>
                                                    <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-family">
                                    <table class="table table-hover" id="table_family">
                                        <thead>
                                        <tr>
                                            <th>Qohumluq</th>
                                            <th>S.A.A</th>
                                            <th><button type="button" class="btn btn-success pull-right btn-xs add_new_family"><i class="pg-plus_circle"> </i></button></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_family_row">
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="family[relation][]">
                                                        @foreach(\App\Models\MskRelation::realData()->get() as $mark)
                                                            <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name="family[parents][]" style="width: 500px;"></td>
                                                <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                            </tr>
                                        </script>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>

   // console.log({{ $staff['personal_heyeti'] }});
    $("#modalUserAdd").find('select[name="personal_heyeti"]').val({{ $staff['personal_heyeti'] }});

    var personFamily = <?php echo json_encode($personFamily) ?>;
    var addressData = <?php echo json_encode($addressData) ?>;


   $("#myModal").find('input[name="lived_city"]').select2({
       allowClear: true,
       ajax: {
           url: '{{ route('search_for_select') }}',
           dataType: 'json',
           data: function (word, page) {
               return {
                   ne: 'get_cities',
                   q: word,
               };
           },
           results: function (data, page) {
               return data;
           },
           cache: true
       },
   }).on('change',function () {
       $("#myModal").find('input[name="lived_region"]').select2('val',null);
   });

   if(typeof addressData['lived_city'] != 'undefined'){
       $("#myModal").find('input[name="lived_city"]').select2('data',addressData['lived_city']);
   }

   $("#myModal").find('input[name="current_city"]').select2({
       allowClear: true,
       ajax: {
           url: '{{ route('search_for_select') }}',
           dataType: 'json',
           data: function (word, page) {
               return {
                   ne: 'get_cities',
                   q: word,
               };
           },
           results: function (data, page) {
               return data;
           },
           cache: true
       },
   }).on('change',function () {
       $("#myModal").find('input[name="current_region"]').select2('val',null);
   });

   if(typeof addressData['current_city'] != 'undefined'){
       $("#myModal").find('input[name="current_city"]').select2('data',addressData['current_city']);
   }

   $("#myModal").find('input[name="lived_region"]').select2({
       allowClear: true,
       ajax: {
           url: '{{ route('search_for_select') }}',
           dataType: 'json',
           data: function (word, page) {
               return {
                   ne: 'get_regions',
                   city_id: $("#myModal").find('input[name="lived_city"]').val(),
                   q: word,
               };
           },
           results: function (data, page) {
               return data;
           },
           cache: true
       },
   });

   if(typeof addressData['lived_region'] != 'undefined'){
       $("#myModal").find('input[name="lived_region"]').select2('data',addressData['lived_region']);
   }

   $("#myModal").find('input[name="current_region"]').select2({
       allowClear: true,
       ajax: {
           url: '{{ route('search_for_select') }}',
           dataType: 'json',
           data: function (word, page) {
               return {
                   ne: 'get_regions',
                   city_id: $("#myModal").find('input[name="current_city"]').val(),
                   q: word,
               };
           },
           results: function (data, page) {
               return data;
           },
           cache: true
       },
   });

   if(typeof addressData['current_region'] != 'undefined'){
       $("#myModal").find('input[name="current_region"]').select2('data',addressData['current_region']);
   }


   if(personFamily){
        _.forEach(personFamily,function(family){
            $("#myModal #table_family tbody").append($("#myModal #table_family_row").html());
            $("#myModal #table_family tbody>tr:last").find('select[name="family[relation][]"]').val(family.relation);
            $("#myModal #table_family tbody>tr:last").find('input[name="family[parents][]"]').select2({
                allowClear:true,
                ajax: {
                    url: '{{ route('str_students_get_data') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'get_parents',
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            }).select2('data',{'id' : family.id , 'text' : family.text});
        });
    }

    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]),
                personal_heyeti = $("#modalUserAdd").find('select[name="personal_heyeti"]'),
                error = false;
            data.append('_token',_token);

            personal_heyeti.parent('div').css("border","");

            if(personal_heyeti.val() <= 0){
                personal_heyeti.parent('div').css('border','1px dotted red');
                error = true;
            }
            $("#myModal #table_study tbody tr").each(function () {
                let tr = $(this),
                    place = tr.find("[name='study[place][]']"),
                    start_date = tr.find("[name='study[start_date][]']"),
                    end_date = tr.find("[name='study[end_date][]']");

                place.css("border","");
                start_date.css("border","");
                end_date.css("border","");

                if(place.val().trim() == ""){ place.css("border","1px solid red"); error = true;findErrorTab(place); }
                if(start_date.val().trim() == ""){ start_date.css("border","1px solid red"); error = true;findErrorTab(start_date); }
                if(end_date.val().trim() == ""){ end_date.css("border","1px solid red"); error = true;findErrorTab(end_date); }
            });

            $("#myModal #table_language tbody tr").each(function () {
                let tr = $(this),
                    name = tr.find("[name='language[name][]']"),
                    mark = tr.find("[name='language[mark][]']");

                name.css("border","");
                mark.css("border","");

                if(name.val().trim() == ""){ name.css("border","1px solid red"); error = true; findErrorTab(name);}
                if((mark.val().trim() > 0) == false ){ mark.css("border","1px solid red"); error = true;findErrorTab(mark); }
            });

            $("#myModal #table_labor_activity tbody tr").each(function () {
                let tr = $(this),
                    workplace = tr.find("[name='labor_activity[workplace][]']"),
                    start_date = tr.find("[name='labor_activity[start_date][]']"),
                    end_date = tr.find("[name='labor_activity[end_date][]']"),
                    position = tr.find("[name='labor_activity[position][]']");

                workplace.css("border","");
                start_date.css("border","");
                end_date.css("border","");
                position.css("border","");

                if(workplace.val().trim() == ""){ workplace.css("border","1px solid red"); error = true;findErrorTab(workplace); }
                if(start_date.val().trim() == ""){ start_date.css("border","1px solid red"); error = true;findErrorTab(start_date); }
                if(end_date.val().trim() == ""){ end_date.css("border","1px solid red"); error = true;findErrorTab(end_date); }
                if(position.val().trim() == ""){ position.css("border","1px solid red"); error = true;findErrorTab(position); }
            });

            $("#myModal #table_family tbody tr").each(function () {
                let tr = $(this),
                    relation = tr.find("[name='family[relation][]']"),
                    parents = tr.find("[name='family[parents][]']");

                relation.css("border","");
                parents.prev('div').css("border","");

                if((relation.val() > 0) == false ){ relation.css("border","1px solid red"); error = true;findErrorTab(relation); }
                if(parents.val().trim() == ""){ parents.prev('div').css("border","1px solid red"); error = true;findErrorTab(parents); }
            });

            if(error) return false;

            $.ajax({
                url: "{{ route('str_staffs_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});

    //study
    $("#myModal").on("click", ".delete_row", function () {
       $(this).parents("tr:eq(0)").remove();
    });

    $("#myModal .add_new_study").click(function () {
       $("#myModal #table_study tbody").append($("#myModal #table_study_row").html());
       $("#myModal #table_study tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });

    //language
    $("#myModal .add_new_language").click(function () {
        $("#myModal #table_language tbody").append($("#myModal #table_language_row").html());
    });
    //labor_activity
    $("#myModal .add_new_labor_activity").click(function () {
        $("#myModal #table_labor_activity tbody").append($("#myModal #table_labor_activity_row").html());
        $("#myModal #table_labor_activity tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });
    //family
    $("#myModal .add_new_family").click(function () {
        $("#myModal #table_family tbody").append($("#myModal #table_family_row").html());
        $("#myModal #table_family tbody>tr:last").find('input[name="family[parents][]"]').select2({
            allowClear:true,
            ajax: {
                url: '{{ route('str_students_get_data') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_parents',
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        });

    });



    function findErrorTab(element){
        var tab_id = element.parents('div:eq(0)').attr('id');
        $('ul>li>a[href="#'+tab_id+'"]').trigger('click');
    }

</script>