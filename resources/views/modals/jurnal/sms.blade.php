<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Mesaj</h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="smsForm" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Şagird</label>
                                <input class="form-control" placeholder="Şagird" value="{{ $student->fullname() }}" type="text" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Sms</label>
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="1" id="checkboxAS" name="sms">
                                    <label for="checkboxAS"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""> Mətn</label>
                                <textarea class="form-control" placeholder="Mətn" name="text" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Əlavə et</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<script>
    $("#smsForm").validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {

        } else {
            e.preventDefault();
            saveModalData();
        }
    });
    
    function saveModalData()
    {
        let error = false,
            data = new FormData($("#smsForm")[0]);

        if (error) return;

        data.append('_token', _token);
        data.append('subject_id', {{ $subjectId }});
        data.append('class_letter_id', {{ $classLetterId }});
        data.append('year', {{ $year }});
        data.append('month', {{ $month }});
        data.append('student_id', {{ $studentId }});

        $.ajax({
            url: "{{ route('ajax_save_sms_action') }}",
            type: "POST",
            data: data,
            async: false,
            success: function (response) {
                if(response['status'] == 'error') $("#smsForm").find(".errors").html(response['errors']);
                else {
                    $("#smsForm").modal('hide');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>