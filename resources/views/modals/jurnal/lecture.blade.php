<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Yeni dərs</h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="smsForm" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Müəllim(ə)</label>
                                <input class="form-control" placeholder="Müəllim(ə)" value="{{ Auth::user()->fullname() }}" type="text" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Fənn</label>
                                <input class="form-control" placeholder="Fənn" value="{{ \App\Models\Subject::find($subjectId)['name'] }}" type="text" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Sinif</label>
                                <input class="form-control" placeholder="Sinif" value="{{ $classLetter->msk_class['name'].$classLetter['name'] }}" type="text" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Tarix</label>
                                <input class="form-control date" placeholder="Tarix" type="text" value="{{ $date->format('d-m-Y') }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Fəsil</label>
                                <input class="form-control seasons" placeholder="Fəsil" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Mövzu</label>
                                <input class="form-control paragraphs" placeholder="Mövzu" name="paragraph_id" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""> Fayl</label>
                                <input class="form-control files" multiple placeholder="Fayl" type="file" name="files[]" accept="images/*,.doc,.ppt,.txt,.mp4,.mp3,.pdf">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Əlavə et</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<script>
    $("#modal{{ $modalId }} .seasons").select2({
        placeholder: "Fəsil",
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('ajax_seasons') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    subject_id: {{ $subjectId }},
                    class_letter_id: {{ $classLetterId }}
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    }).on("change", function () {
        $("#modal{{ $modalId }} .paragraphs").select2("val", null);
    });

    $("#modal{{ $modalId }} .paragraphs").select2({
        placeholder: "Mövzu",
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('ajax_season_paragraphs') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    season_id: $("#modal{{ $modalId }} .seasons").select2('val')
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    });

    $("#modal{{ $modalId }} .date").datepicker({
        format: 'dd-mm-yyyy',
        allowClear: false,
        autoclose: true,
    });

    $("#modal{{ $modalId }} form").validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {

        } else {
            e.preventDefault();
            saveModalData();
        }
    });
    
    function saveModalData()
    {
        let error = false,
            data = new FormData($("#modal{{ $modalId }} form")[0]);

        if (error) return;

        data.append('_token', _token);
        data.append('subject_id', {{ $subjectId }});
        data.append('class_letter_id', {{ $classLetterId }});
        data.append('year', {{ $year }});
        data.append('month', {{ $month }});
        data.append('day', {{ $day }});
        data.append('hour_id', {{ $hourId }});

        $.ajax({
            url: "{{ route('ajax_save_lecture_action') }}",
            type: "POST",
            data: data,
            async: false,
            success: function (response) {
                if(response['status'] == 'error') $("#modal{{ $modalId }}").find(".errors").html(response['errors']);
                else {
                    $("#modal{{ $modalId }}").modal('hide');
                    getPage();
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>