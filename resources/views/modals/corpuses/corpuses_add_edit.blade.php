<div class="modal-dialog">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Korpus <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <input type="file" class="form-control" name="thumb" accept="image/*">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $corpus['name'] }}" name="name" maxlength="30" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            $.ajax({
                url: "{{ route('corpuses_add_edit_action',['corpus' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>