<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Əvəzetmə <span class="semi-bold">Ətraflı</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Əvəzetmə tarİxİ</label>
                            {{ $sub->date }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Əvəz edİləcək müəllİm</label>
                            {{ $sub->user->name }}
                        </div>
                    </div>
                </div>

                @foreach($sub->substitutionLessons as $sublessons)
                <hr>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Əvəz edİləcək dərs</label>
                            {{ $sublessons->lesson->subject->name }}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Əvəz edən müəllİm</label>
                            {{ $sublessons->user->name }}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Korpus</label>
                            {{ $sublessons->lesson->room->corpus->name }}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Sinif</label>
                            @php $class = \App\Models\LetterGroup::realData()->find($sublessons->lesson->letter_group_id)->class_letter @endphp
                            {{ $class->msk_class->name }}{{ $class->name }}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Dərs saatı</label>
                            {{ (new DateTime($sublessons->lesson->class_time->start_time))->format('H:i') }}
                            -
                            {{ (new DateTime($sublessons->lesson->class_time->end_time))->format('H:i') }}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Otaq</label>
                            {{ $sublessons->lesson->room->name }}
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
                {{--<div class="row">--}}
                    {{--<div class="col-md-12 col-sm-12">--}}
                        {{--<div class="form-group form-group-default">--}}
                            {{--<label>Dərs saatı</label>--}}
                            {{--{{ (new DateTime($sub->lesson->class_time->start_time))->format('H:i') }}--}}
                            {{-----}}
                            {{--{{ (new DateTime($sub->lesson->class_time->end_time))->format('H:i') }}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="row">--}}
                    {{--<div class="col-md-12 col-sm-12">--}}
                        {{--<div class="form-group form-group-default">--}}
                            {{--<label>Otaq</label>--}}
                            {{--{{ $sub->lesson->room->name }}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="row">
                    <div class="col-md-12 col-sm-12 m-t-10 sm-m-t-10">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                            Bağla
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
