<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Əvəzetmə <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalSubAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Əvəzetmə tarixi</label>
                                <input type="text" name="sub_date" class="form-control datepicker"
                                       value="{{ empty($sub->date) ? '' : date('d-m-Y',strtotime($sub->date)) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Əvəz ediləcək müəllim</label>
                                <input class="form-control" id="select2Evezedilen">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group form-group-default required">
                                <label>Əvəz ediləcək dərs</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <span></span>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-success " typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<script>





    $('#myModal .datepicker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true
    }).on('changeDate', function () {
         $('#select2Evezedilen').select2("enable",true).select2('val',null);
         $('#myModal #modalSubAdd').find("div.row:eq(3)").html('');
        //$('#select2Evezeden').select2("val",null);
        //isValid($("#modalSubAdd"));
    });


    $('#select2Evezedilen').select2({
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('get_substitution_data') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_teachers',
                    q: word,
                    sub_date: $('input[name="sub_date"]').val(),
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change', function () {
        getSubjects();
        //isValid($("#modalSubAdd"));
    }).select2('enable',false);

    if (Sub.data) {
        $('#select2Evezedilen').select2('data', {id: Sub.data.teacherId, text: Sub.data.teacherName});
        $('#select2Evezedilen').trigger('change');
    }

    function getSubjects() {
        $.get('{{ route('get_substitution_data') }}', {
            'ne': 'get_subjects',
            'sub_date': $('input[name="sub_date"]').val(),
            'teacher': $('#select2Evezedilen').val()
        }, function (response) {
            $('#myModal #modalSubAdd').find("div.row:eq(3)").html('<span></span>');
            _.forEach(response.results, function (r, key) {
                $('#myModal #modalSubAdd').find("div.row:eq(3)").children('span')
                    .append('<div data-area-subject="' + r.subjectId + '">' +
                        '        <div class="col-md-6 col-sm-12">' +
                        '              <div class="form-group form-group-default">' +
                        '                     <label style="padding: 11px;">' + r.subjectName + '</label>' +
                        '               </div>' +
                        '        </div>' +
                        '        <div class="col-md-6 col-sm-12">' +
                        '               <div class="form-group form-group-default required">' +
                        '                      <label>Əvəz edəcək müəllim</label>' +
                        '                      <input class="form-control" name="evezEden">' +
                        '               </div>' +
                        '        </div>' +
                        '</div>');


                var evezEden = $('#myModal #modalSubAdd div[data-area-subject]:last').find('input[name="evezEden"]');
                evezEden.select2({
                    minimumResultsForSearch: -1,
                    ajax: {
                        url: '{{ route('get_substitution_data') }}',
                        dataType: 'json',
                        data: function (word, page) {
                            return {
                                ne: 'get_sub_teachers',
                                q: word,
                                sub_date: $('input[name="sub_date"]').val(),
                                lesson_id: evezEden.closest('div[data-area-subject]').attr('data-area-subject'),
                                teacher: $('#select2Evezedilen').val()
                            }
                        },
                        results: function (data, page) {
                            return data;
                        },
                        cache: true
                    },

                });

                if (Sub.data && Sub.data.teacherId == $('#select2Evezedilen').val()) {
                    evezEden.select2('data', {
                        id: Sub.data.dataEvezedenAndLessons[key].evezEdenTeacherId,
                        text: Sub.data.dataEvezedenAndLessons[key].evezEdenTeacherName
                    });
                }


                // console.log(Sub.data.dataEvezedenAndLessons[0].evezEdenTeacherName);


            });

        });
    }

    $("#modalSubAdd").on('submit', function (e) {
        e.preventDefault();

        var subs = [],
            sub_date = $(this).find('[name="sub_date"]').val(),
            teacher = $(this).find('#select2Evezedilen').val(),
            data = {};


        $('#myModal #modalSubAdd div[data-area-subject]').each(function (i) {

            var sub = [];
            sub.push(i);
            sub.push(($(this).attr('data-area-subject')));
            sub.push(($(this).find('input[name="evezEden"]').val()));
            subs.push(sub);
        });

        data['teacher'] = teacher;
        data['sub_date'] = sub_date;
        data['subs'] = subs;
        data['_token'] = _token;


        //console.log(data);

        //if(isValid($('#myModal #modalSubAdd'))){

        $.post("{{ route('substitution_add_edit_action',["sub" => $id]) }}", data, function (response) {
            if (response['status'] == 'error') $("#myModal #errors").html(response['errors']);
            else location.reload();
        })
        // }

    });


    function isValid(tr) {
        let sub_date = tr.find('input[name="sub_date"]'),
            teacher = tr.find("#select2Evezedilen"),
            sub_teacher = tr.find("#select2Evezeden"),
            subject = tr.find("#select2Subject"),
            status = true;

        sub_date.parent('div').css("border", "");
        teacher.parent('div').css("border", "");
        sub_teacher.parent('div').css("border", "");
        subject.parent('div').css("border", "");


        if (!sub_date.val()) {
            sub_date.parent('div').css("border", "1px dotted red");
            status = false;
        }
        if (!teacher.val()) {
            teacher.parent('div').css("border", "1px dotted red");
            status = false;
        }

        if (!subject.val()) {
            subject.parent().css("border", "1px dotted red");
            status = false;
        }
        if (!sub_teacher.val()) {
            sub_teacher.parent().css("border", "1px dotted red");
            status = false;
        }

        return status;
    }

</script>

