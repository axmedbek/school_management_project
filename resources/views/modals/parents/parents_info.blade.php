<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Personal <span class="semi-bold">Ətraflı</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <img src="{{ asset(\App\Library\Standarts::$userImageDir . $parent['thumb']) }}"
                                     height="180px" width="100%">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Adi</label>
                                {{ empty($parent['name']) ? '-' : $parent['name'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                {{ empty($parent['surname']) ? '-' : $parent['surname'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                {{ empty($parent['middle_name']) ? '-' : $parent['middle_name'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                {{ empty($parent['email']) ? '-' : $parent['email'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                {{ empty($parent['mobil_tel']) ? '-' : $parent['mobil_tel'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                {{ empty($parent['home_tel']) ? '-' : $parent['home_tel'] }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $parent['gender'] == 'm' ?'checked':'' }} disabled value="m"
                                       name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $parent['gender'] == 'f'?'checked':'' }} disabled value="f"
                                       name="gender" id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ empty($parent['enroll_date']) ?'-':date("d-m-Y",strtotime($parent['enroll_date'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                {{ empty($parent['birthday']) ?'-':date("d-m-Y",strtotime($parent['birthday'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                {{ empty($parent['birth_place']) ? '-' : $parent['birth_place'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>İş yeri</label>
                                {{ empty($parent['position']) ? '-' : $parent['position'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                {{ empty($parent['current_city']) ? '-' : \App\Models\MskCities::realData()->find($parent['current_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                {{ empty($parent['current_region']) ? '-' : \App\Models\MskRegions::realData()->find($parent['current_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                {{ empty($parent['current_address']) ? '-' : $parent['current_address'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-children"><span>Övlad(lar)</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-children">
                                    <table class="table table-hover" id="table_children">
                                        <thead>
                                        <tr>
                                            <th>A.S.A.</th>
                                            <th>Sinif</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(\App\Models\PersonFamily::realData()->where('parent_id',$parent['id'])->get() as $student)
                                            <tr>
                                                <td>{{ $student->user->fullname() }}</td>
                                                <td>
                                                    @foreach($student->user->letter_groups as $key => $letter_group)
                                                        @if ($letter_group->class_letter->year_id == \App\Library\YearDays::getCurrentYear()->id)
                                                            {{ $letter_group->class_letter->msk_class->name."/".$letter_group->class_letter->name }}
                                                            {{ count($student->user->letter_groups) > $key+1 ? ',' : '' }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function (form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token', _token);

            $.ajax({
                url: "{{ route('str_parents_add_edit_action',['parent' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if (response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>