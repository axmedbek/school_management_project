<style>
    .field-icon {
        float: right;
        margin-left: -15px;
        margin-top: -20px;
        position: relative;
        z-index: 2;
    }
</style>

<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Valideyn <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalParentAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <input autocomplete="false" name="hidden" type="text" style="display:none;">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <input type="file" class="form-control" name="thumb">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $parent['name'] }}" name="name" maxlength="30" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                <input type="text" class="form-control" value="{{ $parent['surname'] }}" name="surname" maxlength="30">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                <input type="text" class="form-control" value="{{ $parent['middle_name'] }}" name="middle_name" maxlength="30">
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default ">
                                <label for="login">Login</label>
                                <input type="text" class="form-control" name="username" value="{{$parent['username']}}" maxlength="30"
                                       onfocus="this.removeAttribute('readonly');" readonly autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label for="password">Parol</label>
                                <input type="password" class="form-control" name="password"  id="password" minlength="5" maxlength="30" autocomplete="off">
                                <span toggle="#password" class="fa fa-lock field-icon toggle-password"></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default ">
                                <label>Email</label>
                                <input type="email" class="form-control" value="{{ $parent['email'] }}" name="email" >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                <input type="text" class="form-control" value="{{ $parent['mobil_tel'] }}" name="mobil_tel" maxlength="20">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                <input type="text" class="form-control" value="{{ $parent['home_tel'] }}" name="home_tel" maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $parent['gender'] == 'm' || $id == 0?'checked':'' }} value="m" name="gender" id="man">
                                <label for="man">Kişi</label>
                                <input type="radio" {{ $parent['gender'] == 'f'?'checked':'' }} value="f" name="gender" id="woman">
                                <label for="woman">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" style="display:{{ empty($parent['enroll_date']) ? 'none' : ''}};">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ date("d-m-Y",strtotime($parent['enroll_date'])) }}
                                {{--<input type="text" class="form-control datepicker" value="{{ empty($teacher['enroll_date']) ?'':date("d-m-Y",strtotime($teacher['enroll_date'])) }}" name="enroll_date">--}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                <input type="text" class="form-control datepicker" value="{{ empty($parent['birthday']) ?'':date("d-m-Y",strtotime($parent['birthday'])) }}" name="birthday">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                <input type="text" class="form-control" value="{{ $parent['birth_place'] }}" name="birth_place">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>İş yeri</label>
                                <input type="text" class="form-control" value="{{ $parent['position'] }}" name="position" maxlength="60">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                <input type="text" class="full-width" name="parent_current_city"
                                       data-placeholder="Faktiki şəhər">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                <input type="text" class="full-width" name="current_region"
                                       data-placeholder="Faktiki rayon">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                <input type="text" class="form-control" value="{{ $parent['current_address'] }}"
                                       name="current_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-children"><span>Övlad(lar)</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-children">
                                    <table class="table table-hover" id="table_children">
                                        <thead>
                                        <tr>
                                            <th>A.S.A.</th>
                                            <th>Sinif</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(\App\Models\PersonFamily::realData()->where('parent_id',$parent['id'])->get() as $student)
                                            <tr>
                                                <td>{{ $student->user->fullname() }}</td>
                                                <td>
                                                    @foreach($student->user->letter_groups as $key => $letter_group)
                                                        @if ($letter_group->class_letter->year_id == \App\Library\YearDays::getCurrentYear()->id)
                                                            {{ $letter_group->class_letter->msk_class->name."/".$letter_group->class_letter->name }}
                                                            {{ count($student->user->letter_groups) > $key+1 ? ',' : '' }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>

    var addressData = <?php echo json_encode($addressData) ?>;

    $("#myModal2 #modalParentAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal2 #modalParentAdd")[0]),
                error = false;
            data.append('_token',_token);

            error = checkOnlyEnglishCharUsing([
                'input[name="username"]',
                'input[name="password"]'
            ]);

            $("#myModal2 #table_study tbody tr").each(function () {
                let tr = $(this),
                    place = tr.find("[name='study[place][]']"),
                    start_date = tr.find("[name='study[start_date][]']"),
                    end_date = tr.find("[name='study[end_date][]']");

                place.css("border","");
                start_date.css("border","");
                end_date.css("border","");

                if(place.val().trim() == ""){ place.css("border","1px solid red"); error = true; }
                if(start_date.val().trim() == ""){ start_date.css("border","1px solid red"); error = true; }
                if(end_date.val().trim() == ""){ end_date.css("border","1px solid red"); error = true; }
            });

            $("#myModal2 #table_language tbody tr").each(function () {
                let tr = $(this),
                    name = tr.find("[name='language[name][]']"),
                    mark = tr.find("[name='language[mark][]']");

                name.css("border","");
                mark.css("border","");

                if(name.val().trim() == ""){ name.css("border","1px solid red"); error = true; }
                if((mark.val().trim() > 0) == false ){ mark.css("border","1px solid red"); error = true; }
            });

            $("#myModal2 #table_labor_activity tbody tr").each(function () {
                let tr = $(this),
                    workplace = tr.find("[name='labor_activity[workplace][]']"),
                    start_date = tr.find("[name='labor_activity[start_date][]']"),
                    end_date = tr.find("[name='labor_activity[end_date][]']"),
                    position = tr.find("[name='labor_activity[position][]']");

                workplace.css("border","");
                start_date.css("border","");
                end_date.css("border","");
                position.css("border","");

                if(workplace.val().trim() == ""){ workplace.css("border","1px solid red"); error = true; }
                if(start_date.val().trim() == ""){ start_date.css("border","1px solid red"); error = true; }
                if(end_date.val().trim() == ""){ end_date.css("border","1px solid red"); error = true; }
                if(position.val().trim() == ""){ position.css("border","1px solid red"); error = true; }
            });

            $("#myModal2 #table_family tbody tr").each(function () {
                let tr = $(this),
                    relation = tr.find("[name='family[relation][]']"),
                    birthday = tr.find("[name='family[birthday][]']"),
                    name = tr.find("[name='family[name][]']"),
                    position = tr.find("[name='family[position][]']"),
                    address = tr.find("[name='family[address][]']");

                relation.css("border","");
                birthday.css("border","");
                name.css("border","");
                position.css("border","");
                address.css("border","");

                if((relation.val() > 0) == false ){ relation.css("border","1px solid red"); error = true; }
                if(birthday.val().trim() == ""){ birthday.css("border","1px solid red"); error = true; }
                if(relation.val().trim() == ""){ relation.css("border","1px solid red"); error = true; }
                if(name.val().trim() == ""){ name.css("border","1px solid red"); error = true; }
                if(position.val().trim() == ""){ position.css("border","1px solid red"); error = true; }
            });

            if(error) return false;

            $.ajax({
                url: "{{ route('str_parents_add_edit_action',['parent' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal2 #errors").html(response['errors']);
                    else {
                        if ($("#myModal2").attr("modalcount") == 2) {
                            $("#myModal2").modal('hide');
                        }else{
                            location.reload();
                        }
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $("#myModal2 [name='mobil_tel'],#myModal2 [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});

    console.log($('#myModal2 .datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose : true
    }));
    $('#myModal2 .datepicker').datepicker({format: 'dd-mm-yyyy',autoclose : true });
    console.log("end");

    //study
    $("#myModal2").on("click", ".delete_row", function () {
        $(this).parents("tr:eq(0)").remove();
    });

    $("#myModal2 .add_new_study").click(function () {
        $("#myModal2 #table_study tbody").append($("#myModal2 #table_study_row").html());
        $("#myModal2 #table_study tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy', autoclose : true});
    });

    //language
    $("#myModal2 .add_new_language").click(function () {
        $("#myModal2 #table_language tbody").append($("#myModal2 #table_language_row").html());
    });
    //labor_activity
    $("#myModal2 .add_new_labor_activity").click(function () {
        $("#myModal2 #table_labor_activity tbody").append($("#myModal2 #table_labor_activity_row").html());
        $("#myModal2 #table_labor_activity tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy', autoclose : true});
    });
    //family
    $("#myModal2 .add_new_family").click(function () {
        $("#myModal2 #table_family tbody").append($("#myModal2 #table_family_row").html());
        $("#myModal2 #table_family tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy', autoclose : true});
    });

    $("#myModal2").find('input[name="parent_current_city"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_cities',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change',function () {
         $("#myModal2").find('input[name="current_region"]').select2('val',null);
     });

     if(typeof addressData['parent_current_city'] != 'undefined'){
        $("#myModal2").find('input[name="parent_current_city"]').select2('data',addressData['parent_current_city']);
     }

    $("#myModal2").find('input[name="current_region"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_regions',
                    city_id: $("#myModal2").find('input[name="parent_current_city"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });

    if(typeof addressData['current_region'] != 'undefined'){
        $("#myModal2").find('input[name="current_region"]').select2('data',addressData['current_region']);
    }



    //show/hide password
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-lock fa-unlock");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    function checkOnlyEnglishCharUsing(elements){
        for (var i = 0 ; i < elements.length ; i ++ ) {
            if($(elements[i]).val().match(/[^\u0000-\u007F]+/)){
                $.confirm({
                    title: 'Şrift xətası',
                    content: 'Zəhmət olmasa ingilis şriftlərindən istifadə edin',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Təkrar',
                            btnClass: 'btn-red',
                            action: function(){
                            }
                        },
                        Bağla: function () {
                        }
                    }
                });
                return true;
            }
        }
        return false;
    }

</script>