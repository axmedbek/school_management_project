<style>
    .copyText{
        text-align: center;
    }
</style>
<div class="modal-dialog">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Düzəliş et</h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalSmsTemplate" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>{{ $smsTemplate->name }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <textarea name="" id="" cols="30" rows="10" style="margin: 0px; width: 522px; height: 110px;">{{ $smsTemplate->text }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default" style="margin-right: 40px;">
                                <div class="row">
                                    <div class="col-sm-3 copyText"><a href="javascript:void(0)" class="copyTextName"><i class="fa fa-copy"></i> <span> sagird</span></a></div>
                                    <div class="col-sm-2 copyText"><a href="javascript:void(0)" class="copyTextName"><i class="fa fa-copy"></i> <span> gun</span></a></div>
                                    <div class="col-sm-2 copyText"><a href="javascript:void(0)" class="copyTextName"><i class="fa fa-copy"></i> <span> saat</span></a></div>
                                    <div class="col-sm-3 copyText"><a href="javascript:void(0)" class="copyTextName"><i class="fa fa-copy"></i> <span> qiymet</span></a></div>
                                    <div class="col-sm-2 copyText"><a href="javascript:void(0)" class="copyTextName"><i class="fa fa-copy"></i> <span> fenn</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label style="text-transform:none;"><span style="color:red">*</span> Açar sözləri kopyalamaq üçün üzərilərinə klikləyin</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i>Yadda Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
    <input id="myCopyInput" style="opacity: 0">
</div>

<script>
    $('.copyTextName').on('click',function(){
        var copyText = document.getElementById("myCopyInput");
        var copiedText = $(this).find('span').text().trim();
        copyText.value = '$'+copiedText+'$';
        copyText.select();
        document.execCommand("Copy");
    });

    $('#modalSmsTemplate').on('submit',function(){

        var formData = new FormData(),
            smsText = $(this).find('textarea').val();
        formData.append('text',smsText);
        formData.append('_token',_token);

        $.ajax({
            url: "{{ route('sms_templates_edit_action',['smsTemplateId' => $id]) }}",
            type: "POST",
            data: formData,
            async: false,
            success: function (response) {
                if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                else location.reload();
            },
            cache: false,
            contentType: false,
            processData: false
        });


    });

</script>