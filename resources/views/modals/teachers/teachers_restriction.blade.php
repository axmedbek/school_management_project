<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Nizamlamalar <span class="semi-bold"></span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalTeacherRestriction" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group form-group-default">
                                <input type="text" class="full-width" name="teachers[]">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <div class="restrictionClass" style="float: right;margin-right: 20px;">
                                <span>Jurnalda düzəliş icazəsi </span>
                                <input type="checkbox" class="js-switch"  name="restriction"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                Tətbiq et
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    var elem = document.querySelector('.js-switch');
    var init = new Switchery(elem,{
        color : '#40D9CA',
        size : 'small'
    });


    $("#modalTeacherRestriction").find('input[name="teachers[]"]').select2({
        allowClear: true,
        multiple: true,
        ajax: {
            url: "{{ route('search_for_select') }}",
            dataType: 'json',
            data: function (soz) {
                return {
                    'a': soz,
                    'ne': 'getTeachers',
                };
            },
            results: function (data, a) {
                return data
            },
            cache: true
        }
    }).select2('data',{!! json_encode($teachersForSelect) !!});


    if(_.isArray({!! json_encode($teachersForSelect) !!}) && !_.isEmpty({!! json_encode($teachersForSelect) !!})){
        $("#modalTeacherRestriction").find('input[name="restriction"]').attr('checked','checked');
    }


    $("#modalTeacherRestriction").on('submit',function(){

        var formData = new FormData(this);

        formData.append('_token',_token);

        if(checkValidation()){
            $.ajax({
                url: "{{ route('str_teachers_journal_editing_priv_action') }}",
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });


    function checkValidation(){
        var form = $("#modalTeacherRestriction"),
            teachersInput = form.find('input[name="teachers[]"]'),
            error = true;

        teachersInput.prev('div').css('border','');

        if (!teachersInput.val()){
            teachersInput.prev('div').css('border','1px dotted red');
            error = false;
        }

        return error;
    }

</script>