<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Müəllim <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <input type="file" class="form-control" name="thumb">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <label class="custom-control custom-checkbox" style="margin-left: 270px;
    margin-top: 12px;">
                                Jurnalda düzəliş məhdudiyyəti
                                <input type="checkbox" class="custom-control-input" name="restriction"
                                       {{ $teacher['restrict_edit'] == 1 ? 'checked' : ''}} style="display: none;">
                                <span class="custom-control-indicator"></span>
                            </label>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $teacher['name'] }}" name="name"
                                       maxlength="30" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                <input type="text" class="form-control" value="{{ $teacher['surname'] }}" name="surname"
                                       maxlength="30">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                <input type="text" class="form-control" value="{{ $teacher['middle_name'] }}"
                                       name="middle_name" maxlength="30">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Login</label>
                                <input type="text" class="form-control" value="{{ $teacher['username'] }}"
                                       name="username" maxlength="20"
                                       onfocus="this.removeAttribute('readonly');" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Parol</label>
                                <input type="password" class="form-control" name="password" minlength="5"
                                       maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default ">
                                <label>Email</label>
                                <input type="email" class="form-control" value="{{ $teacher['email'] }}" name="email">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                <input type="text" class="form-control" value="{{ $teacher['mobil_tel'] }}"
                                       name="mobil_tel" maxlength="20">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                <input type="text" class="form-control" value="{{ $teacher['home_tel'] }}"
                                       name="home_tel" maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $teacher['gender'] == 'm' || $id == 0?'checked':'' }} value="m"
                                       name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $teacher['gender'] == 'f'?'checked':'' }} value="f" name="gender"
                                       id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" style="display:{{ empty($teacher['enroll_date']) ? 'none' : ''}};">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ date("d-m-Y",strtotime($teacher['enroll_date'])) }}
                                {{--<input type="text" class="form-control datepicker" value="{{ empty($teacher['enroll_date']) ?'':date("d-m-Y",strtotime($teacher['enroll_date'])) }}" name="enroll_date">--}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                <input type="text" class="form-control datepicker"
                                       value="{{ empty($teacher['birthday']) ?'':date("d-m-Y",strtotime($teacher['birthday'])) }}"
                                       name="birthday">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                <input type="text" class="form-control" value="{{ $teacher['birth_place'] }}"
                                       name="birth_place">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı şəhər</label>
                                <input type="text" class="full-width" name="lived_city"
                                       data-placeholder="Yaşadığı şəhər">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı rayon</label>
                                <input type="text" class="full-width" name="lived_region"
                                       data-placeholder="Yaşadığı rayon">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı ünvan</label>
                                <input type="text" class="form-control" value="{{ $teacher['lived_address'] }}"
                                       name="lived_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                <input type="text" class="full-width" name="current_city"
                                       data-placeholder="Faktiki şəhər">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                <input type="text" class="full-width" name="current_region"
                                       data-placeholder="Faktiki rayon">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                <input type="text" class="form-control" value="{{ $teacher['current_address'] }}"
                                       name="current_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Müəllim statusu</label>
                                <select name="msk_teacher_status_id" class="full-width" required style="border: none;"
                                        requred="">
                                    @foreach (\App\Models\MskTeacherStatus::all() as $type)
                                        <option value="{{ $type['id'] }}" {{ $type['id'] == $teacher['msk_teacher_status_id'] ? 'selected' : '' }}> {{ $type['name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Təhsil səviyyəsi</label>
                                <select name="msk_study_levels_id" class="full-width" required style="border: none;">
                                    @foreach (\App\Models\MskStudyLevels::all() as $type)
                                        <option value="{{ $type['id'] }}" {{ $type['id'] == $teacher['msk_study_levels_id'] ? 'selected' : '' }}> {{ $type['name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-study"><span>Təhsil</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-languages"
                                       style="display: none;"><span>Dillər</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-scientific_degree"><span>Elmi dərəcə</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-labor-activity"><span>Əmək fəaliyyəti</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-family" style="display: none;"><span>Ailə</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-subject"><span>Fənnlər</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-study">
                                    <table class="table table-hover" id="table_study">
                                        <thead>
                                        <tr>
                                            <th>Müəssisə</th>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>Sənəd</th>
                                            <th>Sənədin nömrəsi</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_study"><i
                                                            class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_study_row">
                                            <tr>
                                                <td><input type="text" name="study[place][]" class="form-control"></td>
                                                <td><input type="text" name="study[start_date][]"
                                                           class="form-control datepicker"></td>
                                                <td><input type="text" name="study[end_date][]"
                                                           class="form-control datepicker"></td>
                                                <td><input type="text" maxlength="60" name="study[document][]"
                                                           class="form-control"></td>
                                                <td><input type="text" maxlength="60" name="study[document_number][]"
                                                           class="form-control"></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($teacher != null)
                                            @foreach($teacher->person_studies as $study)
                                                <tr>
                                                    <td><input type="text" name="study[place][]" class="form-control"
                                                               value="{{ $study->place }}"></td>
                                                    <td><input type="text" name="study[start_date][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($study->start_date)) }}">
                                                    </td>
                                                    <td><input type="text" name="study[end_date][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($study->end_date)) }}">
                                                    </td>
                                                    <td><input type="text" maxlength="60" name="study[document][]"
                                                               class="form-control" value="{{ $study->document }}"></td>
                                                    <td><input type="text" maxlength="60"
                                                               name="study[document_number][]" class="form-control"
                                                               value="{{ $study->document_number }}"></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-languages">
                                    <table class="table table-hover" id="table_language">
                                        <thead>
                                        <tr>
                                            <th>Dil</th>
                                            <th>Dərəcə tipi</th>
                                            <th>Dərəcə</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_language"><i
                                                            class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_language_row">
                                            <tr>
                                                <td><input type="text" name="language[name][]" class="form-control">
                                                </td>
                                                <td>
                                                    <select class="form-control" name="language[mark_type][]"
                                                            onchange="langMarkReset(this);">
                                                        @foreach(\App\Models\MarkType::realData()->get() as $mark)
                                                            <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="full-width" name="language[mark_name][]">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($teacher != null)
                                            @foreach($teacher->person_languages as $language)
                                                <tr>
                                                    <td><input type="text" name="language[name][]" class="form-control"
                                                               value="{{ $language->name }}"></td>
                                                    <td>
                                                        <select class="form-control" name="language[mark_type][]"
                                                                onchange="langMarkReset(this);">
                                                            @foreach(\App\Models\MarkType::realData()->get() as $mark)
                                                                <option value="{{ $mark->id }}" {{ $mark->id == $language->mark_types_id ? 'selected':'' }}>{{ $mark->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="full-width"
                                                               name="language[mark_name][]">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-scientific_degree">
                                    <table class="table table-hover" id="table_scientific_degree">
                                        <thead>
                                        <tr>
                                            <th>Elmi dərəcə</th>
                                            <th>Verilmə tarixi</th>
                                            <th>Kim tərəfindən</th>
                                            <th>Sənədin nömrəsi</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_scientific_degree">
                                                    <i class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_scientific_degree_row">
                                            <tr>
                                                <td><input type="text" maxlength="60" name="scientific_degree[degree][]"
                                                           class="form-control"></td>
                                                <td><input type="text" name="scientific_degree[issue_date][]"
                                                           class="form-control datepicker"></td>
                                                <td><input type="text" maxlength="60"
                                                           name="scientific_degree[issued_by][]" class="form-control">
                                                </td>
                                                <td><input type="text" maxlength="60"
                                                           name="scientific_degree[document_number][]"
                                                           class="form-control"></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($teacher != null)
                                            @foreach($teacher->person_scientific_degrees as $scientificDegree)
                                                <tr>
                                                    <td><input type="text" maxlength="60"
                                                               name="scientific_degree[degree][]" class="form-control"
                                                               value="{{ $scientificDegree->degree }}"></td>
                                                    <td><input type="text" name="scientific_degree[issue_date][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($scientificDegree->issue_date)) }}">
                                                    </td>
                                                    <td><input type="text" maxlength="60"
                                                               name="scientific_degree[issued_by][]"
                                                               class="form-control"
                                                               value="{{ $scientificDegree->issued_by }}"></td>
                                                    <td><input type="text" maxlength="60"
                                                               name="scientific_degree[document_number][]"
                                                               class="form-control"
                                                               value="{{ $scientificDegree->document_number }}"></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-labor-activity">
                                    <table class="table table-hover" id="table_labor_activity">
                                        <thead>
                                        <tr>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>İş yeri</th>
                                            <th>Vəzifə</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_labor_activity">
                                                    <i class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_labor_activity_row">
                                            <tr>
                                                <td><input type="text" name="labor_activity[start_date][]"
                                                           class="form-control datepicker"></td>
                                                <td><input type="text" name="labor_activity[end_date][]"
                                                           class="form-control datepicker"></td>
                                                <td><input type="text" maxlength="60" name="labor_activity[workplace][]"
                                                           class="form-control"></td>
                                                <td><input type="text" maxlength="60" name="labor_activity[position][]"
                                                           class="form-control"></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($teacher != null)
                                            @foreach($teacher->person_labot_activities as $laborActivity)
                                                <tr>
                                                    <td><input type="text" name="labor_activity[start_date][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($laborActivity->start_date)) }}">
                                                    </td>
                                                    <td><input type="text" name="labor_activity[end_date][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($laborActivity->end_date)) }}">
                                                    </td>
                                                    <td><input type="text" maxlength="60"
                                                               name="labor_activity[workplace][]" class="form-control"
                                                               value="{{ $laborActivity->workplace }}"></td>
                                                    <td><input type="text" maxlength="60"
                                                               name="labor_activity[position][]" class="form-control"
                                                               value="{{ $laborActivity->position }}"></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-family">
                                    <table class="table table-hover" id="table_family">
                                        <thead>
                                        <tr>
                                            <th>Qohumluq</th>
                                            <th>S.A.A</th>
                                            <th>Doğum tarixi</th>
                                            <th>İş yeri vəzifə</th>
                                            <th>Yaşadığı ünvan</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_family"><i
                                                            class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_family_row">
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="family[relation][]">
                                                        @foreach(\App\Models\MskRelation::realData()->get() as $mark)
                                                            <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" maxlength="60" name="family[name][]"
                                                           class="form-control"></td>
                                                <td><input type="text" name="family[birthday][]"
                                                           class="form-control datepicker"></td>
                                                <td><input type="text" maxlength="60" name="family[position][]"
                                                           class="form-control"></td>
                                                <td><input type="text" name="family[address][]" class="form-control">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($teacher != null)
                                            @foreach($teacher->person_families as $family)
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="family[relation][]">
                                                            @foreach(\App\Models\MskRelation::realData()->get() as $mark)
                                                                <option value="{{ $mark->id }}" {{ $mark->id == $family->msk_relation_id ? 'selected':'' }}>{{ $mark->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input type="text" maxlength="60" name="family[name][]"
                                                               class="form-control" value="{{ $family->name }}"></td>
                                                    <td><input type="text" name="family[birthday][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($family->birthday)) }}">
                                                    </td>
                                                    <td><input type="text" maxlength="60" name="family[position][]"
                                                               class="form-control" value="{{ $family->position }}">
                                                    </td>
                                                    <td><input type="text" name="family[address][]" class="form-control"
                                                               value="{{ $family->address }}"></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-subject">
                                    <table class="table table-hover" id="table_subject">
                                        <thead>
                                        <tr>
                                            <th>Fənn</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_subject"><i
                                                            class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_subject_row">
                                            <tr>
                                                <td>
                                                    <input type="text" name="subject[subject_id][]" class="full-width">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>

    var addressData = {!! json_encode($addressData) !!};
    var languageData = {!! json_encode($languageData) !!};
    var subjectData = {!! json_encode($subjectData) !!};

    if (!_.isEmpty(languageData)) {
        $("#myModal #table_language tbody>tr").find('input[name="language[mark_name][]"]').each(function (i) {
            var tr = $(this).parents('tr:eq(0)');
            //console.log(tr);
            $(this).select2({
                allowClear: true,
                ajax: {
                    url: '{{ route('search_for_select') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'get_marks',
                            mark_type: tr.find('select[name="language[mark_type][]"]').val(),
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            }).select2('data', languageData[i]['mark_name']);
        });
    }

    if (!_.isEmpty(subjectData)) {
        _.forEach(subjectData, function (subject) {
            $("#myModal #tab-subject tbody").append($("#myModal #table_subject_row").html());
            $("#myModal #tab-subject tbody>tr:last").find('input[name="subject[subject_id][]"]').select2({
                allowClear: true,
                ajax: {
                    url: '{{ route('search_for_select') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'getSubjects',
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            }).select2('data', {'id': subject.id, 'text': subject.text});
        });
    }

    function langMarkReset(element) {
        $(element).parents('tr:eq(0)').find('input[name="language[mark_name][]"]').select2('val', null);
    }


    $("#myModal").find('input[name="lived_city"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_cities',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change', function () {
        $("#myModal").find('input[name="lived_region"]').select2('val', null);
    });

    if (typeof addressData['lived_city'] != 'undefined') {
        $("#myModal").find('input[name="lived_city"]').select2('data', addressData['lived_city']);
    }

    $("#myModal").find('input[name="current_city"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_cities',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change', function () {
        $("#myModal").find('input[name="current_region"]').select2('val', null);
    });

    if (typeof addressData['current_city'] != 'undefined') {
        $("#myModal").find('input[name="current_city"]').select2('data', addressData['current_city']);
    }

    $("#myModal").find('input[name="lived_region"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_regions',
                    city_id: $("#myModal").find('input[name="lived_city"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });

    if (typeof addressData['lived_region'] != 'undefined') {
        $("#myModal").find('input[name="lived_region"]').select2('data', addressData['lived_region']);
    }

    $("#myModal").find('input[name="current_region"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_regions',
                    city_id: $("#myModal").find('input[name="current_city"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });

    if (typeof addressData['current_region'] != 'undefined') {
        $("#myModal").find('input[name="current_region"]').select2('data', addressData['current_region']);
    }

    $("#myModal #modalUserAdd").validate({
        submitHandler: function (form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]),
                error = false;
            data.append('_token', _token);

            error = checkOnlyEnglishCharUsing([
                'input[name="username"]',
                'input[name="password"]'
            ]);

            $("#myModal #table_study tbody tr").each(function () {
                let tr = $(this),
                    place = tr.find("[name='study[place][]']"),
                    start_date = tr.find("[name='study[start_date][]']"),
                    end_date = tr.find("[name='study[end_date][]']");

                place.css("border", "");
                start_date.css("border", "");
                end_date.css("border", "");

                if (place.val().trim() == "") {
                    place.css("border", "1px solid red");
                    error = true;
                    findErrorTab(place);
                }
                if (start_date.val().trim() == "") {
                    start_date.css("border", "1px solid red");
                    error = true;
                    findErrorTab(start_date);
                }
                if (end_date.val().trim() == "") {
                    end_date.css("border", "1px solid red");
                    error = true;
                    findErrorTab(end_date);
                }
            });

            $("#myModal #table_language tbody tr").each(function () {
                let tr = $(this),
                    name = tr.find("[name='language[name][]']"),
                    mark_type = tr.find("select[name='language[mark_type][]']"),
                    mark_name = tr.find("input[name='language[mark_name][]']");

                name.css("border", "");
                mark_type.prev('div').css("border", "");
                mark_name.prev('div').css("border", "");

                if (name.val().trim() == "") {
                    name.css("border", "1px solid red");
                    error = true;
                    findErrorTab(name);
                }
                if ((mark_type.val().trim() > 0) == false) {
                    mark_type.prev('div').css("border", "1px solid red");
                    error = true;
                    findErrorTab(mark_type);
                }
                if ((mark_name.val().trim() > 0) == false) {
                    mark_name.prev('div').css("border", "1px solid red");
                    error = true;
                    findErrorTab(mark_type);
                }

            });

            $("#myModal #table_scientific_degree tbody tr").each(function () {
                let tr = $(this),
                    issue_date = tr.find("[name='scientific_degree[issue_date][]']"),
                    degree = tr.find("[name='scientific_degree[degree][]']"),
                    issued_by = tr.find("[name='scientific_degree[issued_by][]']"),
                    document_number = tr.find("[name='scientific_degree[document_number][]']");

                issue_date.css("border", "");
                degree.css("border", "");
                issued_by.css("border", "");
                document_number.css("border", "");

                if (issue_date.val().trim() == "") {
                    issue_date.css("border", "1px solid red");
                    error = true;
                    findErrorTab(issue_date);
                }
                if (degree.val().trim() == "") {
                    degree.css("border", "1px solid red");
                    error = true;
                    findErrorTab(degree);
                }
                if (issued_by.val().trim() == "") {
                    issued_by.css("border", "1px solid red");
                    error = true;
                    findErrorTab(issued_by);
                }
                if (document_number.val().trim() == "") {
                    document_number.css("border", "1px solid red");
                    error = true;
                    findErrorTab(document_number);
                }
            });

            $("#myModal #table_labor_activity tbody tr").each(function () {
                let tr = $(this),
                    workplace = tr.find("[name='labor_activity[workplace][]']"),
                    start_date = tr.find("[name='labor_activity[start_date][]']"),
                    end_date = tr.find("[name='labor_activity[end_date][]']"),
                    position = tr.find("[name='labor_activity[position][]']");

                workplace.css("border", "");
                start_date.css("border", "");
                end_date.css("border", "");
                position.css("border", "");

                if (workplace.val().trim() == "") {
                    workplace.css("border", "1px solid red");
                    error = true;
                    findErrorTab(workplace);
                }
                if (start_date.val().trim() == "") {
                    start_date.css("border", "1px solid red");
                    error = true;
                    findErrorTab(start_date);
                }
                if (end_date.val().trim() == "") {
                    end_date.css("border", "1px solid red");
                    error = true;
                    findErrorTab(end_date);
                }
                if (position.val().trim() == "") {
                    position.css("border", "1px solid red");
                    error = true;
                    findErrorTab(position);
                }
            });

            $("#myModal #table_family tbody tr").each(function () {
                let tr = $(this),
                    relation = tr.find("[name='family[relation][]']"),
                    birthday = tr.find("[name='family[birthday][]']"),
                    name = tr.find("[name='family[name][]']"),
                    position = tr.find("[name='family[position][]']"),
                    address = tr.find("[name='family[address][]']");

                relation.css("border", "");
                birthday.css("border", "");
                name.css("border", "");
                position.css("border", "");
                address.css("border", "");

                if ((relation.val() > 0) == false) {
                    relation.css("border", "1px solid red");
                    error = true;
                    findErrorTab(relation);
                }
                if (birthday.val().trim() == "") {
                    birthday.css("border", "1px solid red");
                    error = true;
                    findErrorTab(birthday);
                }
                if (relation.val().trim() == "") {
                    relation.css("border", "1px solid red");
                    error = true;
                    findErrorTab(relation);
                }
                if (name.val().trim() == "") {
                    name.css("border", "1px solid red");
                    error = true;
                    findErrorTab(name);
                }
                if (position.val().trim() == "") {
                    position.css("border", "1px solid red");
                    error = true;
                    findErrorTab(position);
                }
            });

            $("#myModal #table_subject tbody tr").each(function () {
                let tr = $(this),
                    subject_id = tr.find("[name='subject[subject_id][]']");

                subject_id.css("border", "");

                if ((subject_id.val() > 0) == false) {
                    subject_id.css("border", "1px solid red");
                    error = true;
                }
            });

            if (error) return false;

            $.ajax({
                url: "{{ route('str_teachers_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if (response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});

    //study
    $("#myModal").on("click", ".delete_row", function () {
        $(this).parents("tr:eq(0)").remove();
    });

    $("#myModal .add_new_study").click(function () {
        $("#myModal #table_study tbody").append($("#myModal #table_study_row").html());
        $("#myModal #table_study tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });

    //language
    $("#myModal .add_new_language").click(function () {
        $("#myModal #table_language tbody").append($("#myModal #table_language_row").html());
        var tbody = $(this).parents('table:eq(0)').find('tbody>tr:last');
        tbody.find('input[name="language[mark_name][]"]').select2({
            allowClear: true,
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_marks',
                        mark_type: tbody.find('select[name="language[mark_type][]"]').val(),
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        });
    });
    //scientific_degree
    $("#myModal .add_new_scientific_degree").click(function () {
        $("#myModal #table_scientific_degree tbody").append($("#myModal #table_scientific_degree_row").html());
        $("#myModal #table_scientific_degree tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });

    //labor_activity
    $("#myModal .add_new_labor_activity").click(function () {
        $("#myModal #table_labor_activity tbody").append($("#myModal #table_labor_activity_row").html());
        $("#myModal #table_labor_activity tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });
    //family
    $("#myModal .add_new_family").click(function () {
        $("#myModal #table_family tbody").append($("#myModal #table_family_row").html());
        $("#myModal #table_family tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy'});
    });
    //family
    $("#myModal .add_new_subject").click(function () {
        $("#myModal #table_subject tbody").append($("#myModal #table_subject_row").html());
        var tbody = $(this).parents('table:eq(0)').find('tbody>tr:last');
        tbody.find('input[name="subject[subject_id][]"]').select2({
            allowClear: true,
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'getSubjects',
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        });
    });

    function findErrorTab(element) {
        var tab_id = element.parents('div:eq(0)').attr('id');
        $('ul>li>a[href="#' + tab_id + '"]').trigger('click');
    }


    function checkOnlyEnglishCharUsing(elements) {
        for (var i = 0; i < elements.length; i++) {
            if ($(elements[i]).val().match(/[^\u0000-\u007F]+/)) {
                $.confirm({
                    title: 'Şrift xətası',
                    content: 'Zəhmət olmasa ingilis şriftlərindən istifadə edin',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Təkrar',
                            btnClass: 'btn-red',
                            action: function () {
                            }
                        },
                        Bağla: function () {
                        }
                    }
                });
                return true;
            }
        }
        return false;
    }

</script>

@section('css')

@endsection