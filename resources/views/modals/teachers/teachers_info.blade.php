<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Müəllim <span class="semi-bold">Ətraflı</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <img src="{{ asset(\App\Library\Standarts::$userImageDir . $teacher['thumb']) }}" height="180px" width="100%">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <label class="custom-control custom-checkbox" style="margin-left: 270px;
    margin-top: 12px;">
                                Jurnalda düzəliş məhdudiyyəti
                                <input type="checkbox" class="custom-control-input" name="restriction" disabled {{ $teacher['restrict_edit'] == 1 ? 'checked' : ''}} style="display: none;">
                                <span class="custom-control-indicator" disabled></span>
                            </label>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Adi</label>
                                {{ empty($teacher['name']) ? '-' : $teacher['name'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                {{ empty($teacher['surname']) ? '-' : $teacher['surname'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                {{ empty($teacher['middle_name']) ? '-' : $teacher['middle_name'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Login</label>
                                {{ empty($user['username']) ? '-' : $user['username'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                {{ empty($teacher['email']) ? '-' : $teacher['email'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                {{ empty($teacher['mobil_tel']) ? '-' : $teacher['mobil_tel'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                {{ empty($teacher['home_tel']) ? '-' : $teacher['home_tel'] }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $teacher['gender'] == 'm' ?'checked':'' }} disabled value="m" name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $teacher['gender'] == 'f'?'checked':'' }} disabled value="f" name="gender" id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ empty($teacher['enroll_date']) ?'-':date("d-m-Y",strtotime($teacher['enroll_date'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                {{ empty($teacher['birthday']) ?'-':date("d-m-Y",strtotime($teacher['birthday'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                {{ empty($teacher['birth_place']) ? '-' : $teacher['birth_place'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>İş stajı</label>
                                {{ empty($teacher['internship']) ? '-' : $teacher['internship'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı şəhər</label>
                                {{ empty($teacher['lived_city']) ? '-' : \App\Models\MskCities::realData()->find($teacher['lived_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı rayon</label>
                                {{ empty($teacher['lived_region']) ? '-' : \App\Models\MskRegions::realData()->find($teacher['lived_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı ünvan</label>
                                {{ empty($teacher['lived_address']) ? '-' : $teacher['lived_address'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                {{ empty($teacher['current_city']) ? '-' : \App\Models\MskCities::realData()->find($teacher['current_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                {{ empty($teacher['current_region']) ? '-' : \App\Models\MskRegions::realData()->find($teacher['current_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                {{ empty($teacher['current_address']) ? '-' : $teacher['current_address'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Müəllim statusu</label>
                                {{ $teacher->msk_teacher_status->name }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Təhsil səviyyəsi</label>
                                {{ \App\Models\MskStudyLevels::realData()->find($teacher['msk_study_levels_id'])['name']}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-study"><span>Təhsil</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-languages" style="display: none;"><span>Dillər</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-scientific_degree"><span>Elmi dərəcə</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-labor-activity"><span>Əmək fəaliyyəti</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-family" style="display: none;"><span>Ailə</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-subject"><span>Fənnlər</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-study">
                                    <table class="table table-hover" id="table_study">
                                        <thead>
                                        <tr>
                                            <th>Müəssisə</th>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>Sənəd</th>
                                            <th>Sənədin nömrəsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($teacher->person_studies as $study)
                                                <tr>
                                                    <td>{{ $study->place }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($study->start_date)) }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($study->end_date)) }}</td>
                                                    <td>{{ $study->document }}</td>
                                                    <td>{{ $study->document_number }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-languages">
                                    <table class="table table-hover" id="table_language">
                                        <thead>
                                        <tr>
                                            <th>Dil</th>
                                            <th>Dərəcə</th>
                                            <th>Dərəcə tipi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($teacher->person_languages as $language)
                                                <tr>
                                                    <td>{{ $language->name }}</td>
                                                    <td>{{ \App\Models\MarkType::realData()->find($language->mark_types_id)->name }}</td>
                                                    <td>{{ \App\Models\MskMarks::realData()->find($language->msk_marks_id)->name }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-scientific_degree">
                                    <table class="table table-hover" id="table_labor_activity">
                                        <thead>
                                        <tr>
                                            <th>Elmi dərəcə</th>
                                            <th>Verilmə tarixi</th>
                                            <th>Kim tərəfindən</th>
                                            <th>Sənədin nömrəsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($teacher->person_scientific_degrees as $scientificDegree)
                                                <tr>
                                                    <td>{{ $scientificDegree->degree }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($scientificDegree->issue_date)) }}</td>
                                                    <td>{{ $scientificDegree->issued_by }}</td>
                                                    <td>{{ $scientificDegree->document_number }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-labor-activity">
                                    <table class="table table-hover" id="table_labor_activity">
                                        <thead>
                                        <tr>
                                            <th>Başlama tarixi</th>
                                            <th>Bitmə tarixi</th>
                                            <th>İş yeri</th>
                                            <th>Vəzifə</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($teacher->person_labot_activities as $laborActivity)
                                            <tr>
                                                <td>{{ date("d-m-Y", strtotime($laborActivity->start_date)) }}</td>
                                                <td>{{ date("d-m-Y", strtotime($laborActivity->end_date)) }}</td>
                                                <td>{{ $laborActivity->workplace }}</td>
                                                <td>{{ $laborActivity->position }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-family">
                                    <table class="table table-hover" id="table_family">
                                        <thead>
                                        <tr>
                                            <th>Qohumluq</th>
                                            <th>S.A.A</th>
                                            <th>Doğum tarixi</th>
                                            <th>İş yeri vəzifə</th>
                                            <th>Yaşadığı ünvan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($teacher->person_families as $family)
                                            <tr>
                                                <td>{{ $family->msk_relation->name }}</td>
                                                <td>]{{ $family->name }}</td>
                                                <td>{{ date("d-m-Y", strtotime($family->birthday)) }}</td>
                                                <td>{{ $family->position }}</td>
                                                <td>{{ $family->address }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-subject">
                                    <table class="table table-hover" id="table_subject">
                                        <thead>
                                        <tr>
                                            <th>Fənn</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if($teacher != null)
                                                @foreach($teacher->teacher_subjects as $teacherSubject)
                                                    @if ($teacherSubject != null)
                                                        <tr>
                                                            <td>
                                                                {{ $teacherSubject['name'] }}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            $.ajax({
                url: "{{ route('str_users_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>