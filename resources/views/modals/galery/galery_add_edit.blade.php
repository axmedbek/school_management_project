<style>
    .ms-optgroup-label span {
        color: #3e3b3b;
        font-weight: bold;
        border-bottom: 2px solid #08c;
    }

</style>
<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Qovluq <span class="semi-bold">{{ $folder_id > 0 ? 'Düzəliş et' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="folderAddForm" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="folder_name">Qovluq adı</label>
                            <input type="text" class="form-control" name="folder_name" value="{{ $folderObj['name'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <label for="content">Alıcılar</label>
                        <div class="col-md-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                    <li class="active"><a href="#tab_teachers" data-toggle="tab" role="tab">Müəllimlər</a></li>
                                    <li class=""><a href="#tab_parents" data-toggle="tab" role="tab">Valideynlər</a></li>
                                    <li class=""><a href="#tab_students" data-toggle="tab" role="tab">Şagirdlər</a></li>
                                </ul>
                                <div class="tab-content hidden-sm">
                                    <div class="tab-pane active" id="tab_teachers">
                                        <select multiple="multiple" id="teachers_select" name="teachers[]">
                                            @foreach(\App\User::realData('teacher')->get() as $teacher)
                                                <option value='{{ $teacher->id }}' {{ isset($permitedUsersId[$teacher->id])?'selected':'' }}>{{ $teacher->fullname() }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="tab-pane" id="tab_parents">
                                        <select multiple="multiple" id="parents_select" name="parents[]">
                                            @foreach($parentsGroups as $class => $parents)
                                                <optgroup label="{{ $class }}">
                                                    @foreach($parents as $parent)
                                                        <option value='{{ $parent->id }}' {{ isset($permitedUsersId[$parent->id])?'selected':'' }}>{{ $class }} {{ $parent->fullname() }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="tab-pane" id="tab_students">
                                        <select multiple="multiple" id="students_select" name="students[]">
                                            @foreach($studentGroups as $class => $students)
                                                <optgroup label="{{ $class }}">
                                                    @foreach($students as $student)
                                                        <option value='{{ $student->id }}' {{ isset($permitedUsersId[$student->id])?'selected':'' }}>{{ $class }} {{ $student->fullname() }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-success " typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>

    $('#folderAddForm').on('submit',function(){
        pageLoading('show');
        var formData = new FormData($(this)[0]);
        if (checkValidation($(this))){
            $.ajax({
                type: 'POST',
                url: '{{ route('folder.save',$folder_id) }}',
                data: formData,
                cache: false,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == "ok"){
                        pageLoading();
                        location.reload();
                    }
                    else{
                        pageLoading();
                        location.reload();
                        console.log(response);
                    }
                },
                error: function (response) {
                    pageLoading();
                    console.log("error");
                    console.log(response);
                }
            });
        }
    });

    function checkValidation(form){
        var folder_name = form.find('input[name="folder_name"]'),
            status = true;

        if (folder_name.val().length < 1){
            confirmDialog('Boş saxlamaq olmaz!','Qovluq adı boş ola bilməz!');
            status = false;
        }
        if (folder_name.val().length > 60){
            confirmDialog('Maksimal söz sayı!','Girilən qovluq adı 60 hərfdən kiçik olmalıdır');
            status = false;
        }

        return status;
    }

    function confirmDialog(title,content){
        $.confirm({
            title:title,
            content: content,
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'OK',
                    btnClass: 'btn-red',
                    action: function(){
                    }
                },
                Bağla: function () {
                }
            }
        });
    }

    $('#teachers_select').multiSelect({
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiSelectAll(\"#teachers_select\")'>Hamısını seç >></a></div>",
        selectionFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiDeSelectAll(\"#teachers_select\")'><< Hamısını sil</a></div>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });
    $('#parents_select').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiSelectAll(\"#parents_select\")'>Hamısını seç >></a></div>",
        selectionFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiDeSelectAll(\"#parents_select\")'><< Hamısını sil</a></div>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });
    $('#students_select').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiSelectAll(\"#students_select\")'>Hamısını seç >></a></div>",
        selectionFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiDeSelectAll(\"#students_select\")'><< Hamısını sil</a></div>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    function multiSelectAll(sel) {
        $(sel).multiSelect('select_all');
    }
    function multiDeSelectAll(sel) {
        $(sel).multiSelect('deselect_all');
    }

</script>


