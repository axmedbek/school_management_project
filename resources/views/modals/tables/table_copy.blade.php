<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Cədvəl kopyalama </h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="tableCopyModal" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label class="">Tədris ili</label>
                                <select name="year_id" class="full-width" data-placeholder="Tədris ili" data-init-plugin="select2">
                                    @foreach($years as $year)
                                        <option value="{{ $year->id }}" {{ $request->get('year_id')==$year->id?'selected':'' }}>{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label class="">Korpus</label>
                                <select name="corpus_id" class="full-width" data-placeholder="Korpus" data-init-plugin="select2">
                                    @foreach($corpuses as $corpuse)
                                        <option value="{{ $corpuse->id }}" {{ $request->get('corpus_id')==$corpuse->id?'selected':'' }}>{{ $corpuse->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label class="">Sinif</label>
                                <select name="class_id" class="full-width" data-placeholder="Sinif" data-init-plugin="select2">
                                    @foreach($classes as $class)
                                        <option value="{{ $class->id }}" {{ $request->get('class_id')==$class->id?'selected':'' }}>{{ $class->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label class="">Sinif hərfi</label>
                                <input type="text" class="form-control" name="letter_id">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default required">
                                <label class="">Qrup</label>
                                <input type="text" class="form-control" name="group_id">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>


<script>
    $("#tableCopyModal").find('select[name="year_id"]').select2();

    $("#tableCopyModal").find('select[name="corpus_id"]').select2();

    $("#tableCopyModal").find('select[name="class_id"]').select2();

    $('select[name="year_id"], select[name="corpus_id"], select[name="class_id"]').on('change',function () {
        $('input[name="letter_id"]').select2("val", null);
        $('input[name="group_id"]').select2("val", null);
    });

   $("#tableCopyModal").find('input[name="letter_id"]').select2({
        placeholder: "Sinif hərfi",
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('get_class_letters') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    year_id: $('select[name="year_id"]').val(),
                    corpus_id: $('select[name="corpus_id"]').val(),
                    class_id: $('select[name="class_id"]').val(),
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    });

    $("#tableCopyModal").find('input[name="group_id"]').select2({
        placeholder: "Qrup",
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('get_letters_group') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    letter_id: $('input[name="letter_id"]').val()
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    });

    $("#tableCopyModal").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #tableCopyModal")[0]),
                error = false;
            data.append('_token',_token);

            if(error) return false;

            pageLoading('show');
            $.ajax({
                url: "{{ route('copy_table_action') }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else {
                        $("#lesson_table").html(response['table']);
                        $("#myModal").modal('hide');
                    }

                    pageLoading('hide');
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
</script>




