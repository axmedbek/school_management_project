<div class="modal-dialog">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Şagird <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <input type="hidden" name="group_id" value="{{ $group_id }}">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group form-group-default form-group-default-select2 required">
                                <label>Şagirdlər</label>
                                <input class="full-width" name="student" data-placeholder="Şagirdlər" id="select2Students">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            datas  = $("#select2Students").select2("data"),
            data.append('_token',_token);

            if(datas != null && datas.id > 0){
                $("#students_table tbody").append('<tr student_id="' + datas['id'] + '">' +
                    '<td> </td>' +
                    '<td> <img src="' + datas['pic'] + '" width="60px" height="60px"> </td>' +
                    '<td> ' + datas['text'] +  ' </td>' +
                    '<td> <a type="button" class="btn btn-danger col-lg-4 col-md-12 delete_student"><i class="fa fa-trash-o"></i></a></td>' +
                    '</tr>');

                $("#myModal").modal('hide');
                order($("#students_table"));
            }
            return false;
        }
    });

    $("#select2Students").select2({
        placeholder: "Şagirdlər",
        ajax: {
            url: '{{ route('get_table_students') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    group_id: {{ $group_id }},
                    students: getStudents()
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    });

    function getStudents() {
        let students = [];

        $("#students_table tr[student_id]").each(function () {
            students.push($(this).attr("student_id"));
        });

        return students;
    }
</script>