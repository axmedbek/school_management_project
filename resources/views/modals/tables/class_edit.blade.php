<div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Dərs </h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group form-group-default form-group-default-select2 required">
                                <label>Fənn</label>
                                <input class="full-width" name="class_id" data-placeholder="Fənn" id="select2Subjects">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group form-group-default form-group-default-select2 required">
                                <label>Müəllim</label>
                                <input class="full-width" name="teacher_id" data-placeholder="Müəllim" id="select2Teachers">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group form-group-default form-group-default-select2 required">
                                <label>Otaq</label>
                                <input class="full-width" name="room_id" data-placeholder="Otaq" id="select2Rooms">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]),
                subject_data  = $("#select2Subjects").select2("data"),
                teacher_data  = $("#select2Teachers").select2("data"),
                room_data  = $("#select2Rooms").select2("data"),
                error = false;

            data.append('_token',_token);

            $("#select2Subjects").prev().css("border", "");
            $("#select2Teachers").prev().css("border", "");
            $("#select2Rooms").prev().css("border", "");

            if( subject_data == null ){
                $("#select2Subjects").prev().css("border", "1px dotted red");
                error = true;
            }
            if( teacher_data == null ){
                $("#select2Teachers").prev().css("border", "1px dotted red");
                error = true;
            }
            if( room_data == null ){
                $("#select2Rooms").prev().css("border", "1px dotted red");
                error = true;
            }

            if(!error){
                lessonAddEdit('{{ $week_day }}', '{{ $class_time_id }}', subject_data.name, room_data.text, teacher_data.text, teacher_data.id, room_data.id, subject_data.id);

                $("#myModal").modal('hide');
            }
            return false;
        }
    });

    $("#select2Subjects").select2({
        placeholder: "Fənn",
        ajax: {
            url: '{{ route('get_table_group_subjects') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    group_id: {{ $group_id }},
                    subjects: '{!! json_encode($subjects) !!}'
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    }).on("change", function () {
        $("#select2Teachers").select2("val", null);
        $("#select2Rooms").select2("val", null);
    });

    $("#select2Teachers").select2({
        placeholder: "Müəllim",
        ajax: {
            url: '{{ route('get_table_group_teachers') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    group_id: {{ $group_id }},
                    week_day: {{ $week_day }},
                    class_time_id: {{ $class_time_id }},
                    subject_id: $("#select2Subjects").val()
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    }).on("change", function () {
        $("#select2Rooms").select2("val", null);
    });

    $("#select2Rooms").select2({
        placeholder: "Otaq",
        ajax: {
            url: '{{ route('get_table_group_rooms') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    group_id: {{ $group_id }},
                    week_day: {{ $week_day }},
                    class_time_id: {{ $class_time_id }},
                    subject_id: $("#select2Subjects").val(),
                    teacher_id: $("#select2Teachers").val()
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    });

    @if($subject != null)
        $("#select2Subjects").select2("data", {id: {{ $subject->id }}, text: '{{ $subject->name }}' });
    @endif
    @if($room != null)
        $("#select2Rooms").select2("data", {id: {{ $room->id }}, text: '{{ $room->name }}' });
    @endif
    @if($teacher != null)
        $("#select2Teachers").select2("data", {id: {{ $teacher->id }}, text: '{{ $teacher->fullname() }}' });
    @endif
</script>