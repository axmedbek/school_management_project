<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Şagird <span class="semi-bold">Ətraflı</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <img src="{{ asset(\App\Library\Standarts::$userImageDir . $student['thumb']) }}" height="180px" width="100%">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Adi</label>
                                {{ empty($student['name']) ? '-' : $student['name'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                {{ empty($student['surname']) ? '-' : $student['surname'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                {{ empty($student['middle_name']) ? '-' : $student['middle_name'] }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                {{ empty($student['email']) ? '-' : $student['email'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                {{ empty($student['mobil_tel']) ? '-' : $student['mobil_tel'] }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                {{ empty($student['home_tel']) ? '-' : $student['home_tel'] }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $student['gender'] == 'm' ? 'checked':'' }} disabled value="m" name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $student['gender'] == 'f'?'checked':'' }} disabled value="f" name="gender" id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ empty($student['enroll_date']) ?'-':date("d-m-Y",strtotime($student['enroll_date'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                {{ empty($student['birthday']) ?'-':date("d-m-Y",strtotime($student['birthday'])) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                {{ empty($student['birth_place']) ? '-' : $student['birth_place'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı şəhər</label>
                                {{ empty($student['lived_city']) ? '-' : \App\Models\MskCities::realData()->find($student['lived_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı rayon</label>
                                {{ empty($student['lived_region']) ? '-' : \App\Models\MskRegions::realData()->find($student['lived_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı ünvan</label>
                                {{ empty($student['lived_address']) ? '-' : $student['lived_address'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                {{ empty($student['current_city']) ? '-' : \App\Models\MskCities::realData()->find($student['current_city'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                {{ empty($student['current_region']) ? '-' : \App\Models\MskRegions::realData()->find($student['current_region'])->name }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                {{ empty($student['current_address']) ? '-' : $student['current_address'] }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-family"><span>Ailə</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-comment"><span>Rəylər</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-sms-number"><span>Sms nömrələri</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-family">
                                    <table class="table table-hover" id="table_family">
                                        <thead>
                                        <tr>
                                            <th>Qohumluq</th>
                                            <th>S.A.A</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($student->person_families as $family)
                                            <tr>
                                                <td>{{ $family->msk_relation->name }}</td>
                                                @php $parentUser = \App\User::where('id',$family['parent_id'])->first(); @endphp
                                                @if ($parentUser)
                                                    <td>{{ $parentUser->fullname() }} (Mob:{{ $parentUser->mobil_tel }} - {{ $parentUser->home_tel }})</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-comment">
                                    <table class="table table-hover" id="table_comment">
                                        <thead>
                                        <tr>
                                            <th>Tarix</th>
                                            <th>Rəy</th>
                                            <th>Müəllif</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($student->student_comments as $comment)
                                            <tr>
                                                <td>{{date("d-m-Y",strtotime($comment->date))}}</td>
                                                <td>{{$comment->comment}}</td>
                                                <td>{{$comment->author}}</td>
                                                <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                            </tr>
                                         @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-sms-number">
                                    <table class="table table-hover" id="table_sms_number">
                                        <thead>
                                        <tr>
                                            <th>Nömrə</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($student->sms_numbers as $number)
                                            <tr>
                                                <td>{{$number->number}}</td>
                                                <td><button type="button" class="btn btn-danger btn-xs delete_row"><i class="fa fa-trash"> </i></button></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            $.ajax({
                url: "{{ route('str_users_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $("#myModal [name='mobil_tel'],#myModal [name='home_tel']").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>