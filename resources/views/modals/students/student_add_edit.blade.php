<div class="modal-dialog modal-lg">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Şagird <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Şəkil</label>
                                <input type="file" class="form-control" name="thumb">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $student['name'] }}" name="name"
                                       maxlength="30" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Soyadı</label>
                                <input type="text" class="form-control" value="{{ $student['surname'] }}" name="surname"
                                       maxlength="30">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ata adı</label>
                                <input type="text" class="form-control" value="{{ $student['middle_name'] }}"
                                       name="middle_name" maxlength="30">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Login</label>
                                <input type="text" class="form-control" value="{{ $student['username'] }}"
                                       name="username" maxlength="20"
                                       onfocus="this.removeAttribute('readonly');" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Parol</label>
                                <input type="password" class="form-control" name="password" minlength="5"
                                       maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="email" class="form-control" value="{{ $student['email'] }}" name="email">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Mobil tel</label>
                                <input type="text" class="form-control mask" value="{{ $student['mobil_tel'] }}"
                                       name="mobil_tel" maxlength="20">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Ev tel</label>
                                <input type="text" class="form-control mask" value="{{ $student['home_tel'] }}"
                                       name="home_tel" maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Cinsi</label>
                        <div class="col-sm-9">
                            <div class="radio radio-success">
                                <input type="radio" {{ $student['gender'] == 'm' || $id == 0?'checked':'' }} value="m"
                                       name="gender" id="male">
                                <label for="male">Kişi</label>
                                <input type="radio" {{ $student['gender'] == 'f'?'checked':'' }} value="f" name="gender"
                                       id="female">
                                <label for="female">Qadın</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" style="display:{{ empty($student['enroll_date']) ? 'none' : ''}};">
                            <div class="form-group form-group-default">
                                <label>Qeydiyyat tarixi</label>
                                {{ date("d-m-Y",strtotime($student['enroll_date'])) }}
                                {{--<input type="text" class="form-control datepicker" value="{{ empty($teacher['enroll_date']) ?'':date("d-m-Y",strtotime($teacher['enroll_date'])) }}" name="enroll_date">--}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum tarixi</label>
                                <input type="text" class="form-control datepicker"
                                       value="{{ empty($student['birthday']) ?'':date("d-m-Y",strtotime($student['birthday'])) }}"
                                       name="birthday">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Doğum yeri</label>
                                <input type="text" class="form-control" value="{{ $student['birth_place'] }}"
                                       name="birth_place">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı şəhər</label>
                                <input type="text" class="full-width" name="lived_city"
                                       data-placeholder="Yaşadığı şəhər">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı rayon</label>
                                <input type="text" class="full-width" name="lived_region"
                                       data-placeholder="Yaşadığı rayon">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Yaşadığı ünvan</label>
                                <input type="text" class="form-control" value="{{ $student['lived_address'] }}"
                                       name="lived_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki şəhər</label>
                                <input type="text" class="full-width" name="current_city"
                                       data-placeholder="Faktiki şəhər">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group form-group-default">
                                <label>Faktiki rayon</label>
                                <input type="text" class="full-width" name="current_region"
                                       data-placeholder="Faktiki rayon">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Faktiki ünvan</label>
                                <input type="text" class="form-control" value="{{ $student['current_address'] }}"
                                       name="current_address" maxlength="255" style="margin-bottom: 15px;">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-transparent ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab-family"><span>Ailə</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-comment"><span>Rəylər</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-sms-number"><span>Sms nömrələri</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-family">
                                    <table class="table table-hover" id="table_family">
                                        <thead>
                                        <tr>
                                            <th>Qohumluq əlaqəsi</th>
                                            <th style="width: 100%">S.A.A</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_family"
                                                        style="height: 32px;margin-right: 25%"><i
                                                            class="pg-plus_circle"> </i></button>
                                                <a href="javascript:openModal('{{ route('str_parents_add_edit',['parent' => 0]) }}',null,undefined,2)" type="button" class="btn btn-success btn-sm" style="margin-right: 40px;float: left;margin-left: -25%;">Yeni valideyn əlavə et</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_family_row">
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="family[relation][]">
                                                        @foreach(\App\Models\MskRelation::realData()->get() as $mark)
                                                            <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="family[parents][]" style="width: 500px;">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-comment">
                                    <table class="table table-hover" id="table_comment">
                                        <thead>
                                        <tr>
                                            <th>Tarix</th>
                                            <th>Rəy</th>
                                            <th>Müəllif</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_comment"><i
                                                            class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_comment_row">
                                            <tr>
                                                <td><input type="text" name="comment[date][]"
                                                           class="form-control datepicker"></td>
                                                <td><input maxlength="255" type="text" name="comment[comment][]"
                                                           class="form-control"></td>
                                                <td><input maxlength="60" type="text" name="comment[author][]"
                                                           class="form-control"></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($student != null)
                                            @foreach($student->student_comments as $comment)
                                                <tr>
                                                    <td><input type="text" name="comment[date][]"
                                                               class="form-control datepicker"
                                                               value="{{ date("d-m-Y", strtotime($comment->date)) }}">
                                                    </td>
                                                    <td><input maxlength="255" type="text" name="comment[comment][]"
                                                               class="form-control" value="{{ $comment->comment }}">
                                                    </td>
                                                    <td><input maxlength="60" type="text" name="comment[author][]"
                                                               class="form-control" value="{{ $comment->author }}"></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab-sms-number">
                                    <table class="table table-hover" id="table_sms_number">
                                        <thead>
                                        <tr>
                                            <th>Nömrə</th>
                                            <th>
                                                <button type="button"
                                                        class="btn btn-success pull-right btn-xs add_new_sms_number"><i
                                                            class="pg-plus_circle"> </i></button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/html" id="table_sms_number_row">
                                            <tr>
                                                <td><input type="text" name="sms_number[number][]"
                                                           class="form-control mask"></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs delete_row"><i
                                                                class="fa fa-trash"> </i></button>
                                                </td>
                                            </tr>
                                        </script>

                                        @if($student != null)
                                            @foreach($student->sms_numbers as $number)
                                                <tr>
                                                    <td><input type="text" name="sms_number[number][]"
                                                               class="form-control mask" value="{{ $number->number }}">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-xs delete_row">
                                                            <i class="fa fa-trash"> </i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>


    var personFamily = <?php echo json_encode($personFamily) ?>;
    var addressData = <?php echo json_encode($addressData) ?>;

   //console.log(addressData['current_city']);

    if (personFamily) {
        _.forEach(personFamily, function (family) {
            $("#myModal #table_family tbody").append($("#myModal #table_family_row").html());
            $("#myModal #table_family tbody>tr:last").find('select[name="family[relation][]"]').val(family.relation);
            $("#myModal #table_family tbody>tr:last").find('input[name="family[parents][]"]').select2({
                allowClear: true,
                ajax: {
                    url: '{{ route('search_for_select') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'get_parents',
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            }).select2('data', {'id': family.id, 'text': family.text});
        });
    }

    $("#myModal #modalUserAdd").validate({
        submitHandler: function (form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]),
                error = false;
            data.append('_token', _token);

            $("#myModal #table_family tbody tr").each(function () {
                let tr = $(this),
                    relation = tr.find("[name='family[relation][]']"),
                    relationName = tr.find("[name='family[parents][]']");

                relation.css("border", "");
                relationName.prev('div').css("border", "");

                if ((relation.val() > 0) == false) {
                    relation.css("border", "1px solid red");
                    error = true;
                    findErrorTab(relation);
                }
                if (relationName.val().trim() == "") {
                    relationName.prev('div').css("border", "1px solid red");
                    error = true;
                    findErrorTab(relationName.prev('div'));
                }
            });

            $("#myModal #table_comment tbody tr").each(function () {
                let tr = $(this),
                    date = tr.find("[name='comment[date][]']"),
                    comment = tr.find("[name='comment[comment][]']"),
                    author = tr.find("[name='comment[author][]']");

                date.css("border", "");
                comment.css("border", "");
                author.css("border", "");

                if (date.val().trim() == "") {
                    date.css("border", "1px solid red");
                    error = true;
                    findErrorTab(date);
                }
                if (comment.val().trim() == "") {
                    comment.css("border", "1px solid red");
                    error = true;
                    findErrorTab(comment);
                }
                if (author.val().trim() == "") {
                    author.css("border", "1px solid red");
                    error = true;
                    findErrorTab(author);
                }
            });

            $("#myModal #table_sms_number tbody tr").each(function () {
                let tr = $(this),
                    number = tr.find("[name='sms_number[number][]']");

                number.css("border", "");

                if (number.val().trim() == "") {
                    number.css("border", "1px solid red");
                    error = true;
                    findErrorTab(number);
                }

                //if(findInArray(data))
            });

            if (error) return false;


            $.ajax({
                url: "{{ route('str_students_add_edit_action',['user' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if (response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else location.reload();
                },
                error: function (response) {
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $(".mask").inputmask("mask", {"mask": "(999) 999-9999"});
    $('#myModal .datepicker').datepicker({format: 'dd-mm-yyyy', autoclose : true});

    //comment
    $("#myModal").on("click", ".delete_row", function () {
        $(this).parents("tr:eq(0)").remove();
    });

    $("#myModal .add_new_comment").click(function () {
        $("#myModal #table_comment tbody").append($("#myModal #table_comment_row").html());
        $("#myModal #table_comment tbody tr:last .datepicker").datepicker({format: 'dd-mm-yyyy', autoclose : true});
    });
    //family
    $("#myModal .add_new_family").click(function () {
        $("#myModal #table_family tbody").append($("#myModal #table_family_row").html());
        $("#myModal #table_family tbody>tr:last").find('input[name="family[parents][]"]').select2({
            allowClear: true,
            ajax: {
                url: '{{ route('search_for_select') }}',
                dataType: 'json',
                data: function (word, page) {
                    return {
                        ne: 'get_parents',
                        q: word,
                    };
                },
                results: function (data, page) {
                    return data;
                },
                cache: true
            },
        });

    });

    $("#myModal").find('input[name="lived_city"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_cities',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change',function () {
        $("#myModal").find('input[name="lived_region"]').select2('val',null);
    });

    if(typeof addressData['lived_city'] != 'undefined'){
        $("#myModal").find('input[name="lived_city"]').select2('data',addressData['lived_city']);
    }

    $("#myModal").find('input[name="current_city"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_cities',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on('change',function () {
        $("#myModal").find('input[name="current_region"]').select2('val',null);
    });

    if(typeof addressData['current_city'] != 'undefined'){
        $("#myModal").find('input[name="current_city"]').select2('data',addressData['current_city']);
    }

    $("#myModal").find('input[name="lived_region"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_regions',
                    city_id: $("#myModal").find('input[name="lived_city"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });

    if(typeof addressData['lived_region'] != 'undefined'){
        $("#myModal").find('input[name="lived_region"]').select2('data',addressData['lived_region']);
    }

    $("#myModal").find('input[name="current_region"]').select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_regions',
                    city_id: $("#myModal").find('input[name="current_city"]').val(),
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    });

    if(typeof addressData['current_region'] != 'undefined'){
        $("#myModal").find('input[name="current_region"]').select2('data',addressData['current_region']);
    }


    //family
    $("#myModal .add_new_sms_number").click(function () {
        $("#myModal #table_sms_number tbody").append($("#myModal #table_sms_number_row").html());
        $("#myModal #table_sms_number tbody tr:last .mask").inputmask("mask", {"mask": "(999) 999-9999"});
    });


    function findInArray(arr, val) {
        for (var i = 0, len = arr.length; i < len; i++) {
            if (arr[i] === val) {
                return i;
            }
        }
        return -1;
    }

    function findErrorTab(element) {
        var tab_id = element.parents('div:eq(0)').attr('id');
        $('ul>li>a[href="#' + tab_id + '"]').trigger('click');
    }

</script>