<style>
    .ms-container {
        width: 100%;
    }

    .datepicker table tr td.today, .datepicker table tr td.today:hover {
        background-color: #3054d2;
    }

    .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
        color: #dedada;
    }
    .nav-tabs > li > a {
        padding: 8px;
    }
</style>
<div class="modal-dialog modal-md">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                </button>
                <h5>Elan & Tapşırıq <span class="semi-bold">{{ $task_id > 0 ? 'Düzəliş et' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="advertTaskForm" role="form" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="date_of_show">Əks olunma tarixi</label>
                            <input type="text" name="date_of_show" class="form-control datepicker"
                                   value="{{ $task_id == 0 ? '' : date('d-m-Y',strtotime($task['date_of_show'])) }}">
                        </div>
                        <div class="col-md-6">
                            <label for="date_of_event">Keçirilmə tarixi</label>
                            <input type="text" class="form-control datepicker" name="date_of_event"
                                   value="{{ $task_id == 0 ? '' : $task['date_of_event'] ? date('d-m-Y',strtotime($task['date_of_event'])) : '' }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">Adı</label>
                            <input type="text" name="name" class="form-control" value="{{ $task['name'] }}">
                        </div>
                        <div class="col-md-6">
                            <label for="range">Əhatəsi</label>
                            <input type="text" name="range" class="form-control" value="{{ $task['range'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="file">Fayl</label>
                            <input type="file" class="form-control" name="file">
                        </div>
                        <div class="col-md-6">
                            <label for="link">Link</label>
                            <input type="text" class="form-control" name="link" value="{{ $task['link'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="content">Məzmun</label>
                            <textarea name="content" id="content" cols="30" rows="10"
                                      style="resize: none;margin: 0px; width: 546px; height: 143px;">{{ $task['content'] }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <label for="content">Alıcılar</label>
                        <div class="col-md-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist"
                                    data-init-reponsive-tabs="collapse">
                                    <li class="active"><a href="#tab_teachers" data-toggle="tab"
                                                          role="tab">Müəllimlər</a></li>
                                    <li class=""><a href="#tab_parents" data-toggle="tab" role="tab">Valideynlər</a>
                                    </li>
                                    <li class=""><a href="#tab_students" data-toggle="tab" role="tab">Şagirdlər</a></li>
                                </ul>
                                <div class="tab-content hidden-xs">
                                    <div class="tab-pane active" id="tab_teachers">
                                        <select multiple="multiple" id="teachers_select" name="teachers[]">
                                            @foreach(\App\User::realData('teacher')->get() as $teacher)
                                                <option value='{{ $teacher->id }}' {{ isset($permitedUsersId[$teacher->id])?'selected':'' }}>{{ $teacher->fullname() }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="tab-pane" id="tab_parents">
                                        <select multiple="multiple" id="parents_select" name="parents[]">
                                            @foreach($parentsGroups as $class => $parents)
                                                <optgroup label="{{ $class }}">
                                                    @foreach($parents as $parent)
                                                        <option value='{{ $parent->id }}' {{ isset($permitedUsersId[$parent->id])?'selected':'' }}>{{ $class }} {{ $parent->fullname() }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="tab-pane" id="tab_students">
                                        <select multiple="multiple" id="students_select" name="students[]">
                                            @foreach($studentGroups as $class => $students)
                                                <optgroup label="{{ $class }}">
                                                    @foreach($students as $student)
                                                        <option value='{{ $student->id }}' {{ isset($permitedUsersId[$student->id])?'selected':'' }}>{{ $class }} {{ $student->fullname() }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-success " typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i>
                                Bağla
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<script>
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        startDate: new Date(),
        autoclose: true
    });


    $("#advertTaskForm").on("submit", function (e) {
        e.preventDefault();

        let formData = new FormData($(this)[0]),
            date_of_event = $(this).find("[name='date_of_event']");
        formData.append('_token', _token);

        if (checkValidation($("#advertTaskForm"))) {
            if (date_of_event.val().trim().length == 0) {
                $.confirm({
                    title: 'Xəbərdarlıq!',
                    content: 'Keçirilmə tarixi qeyd etmədən davam etmək istədiyinizə əminsiniz ? ' +
                    '. Davam etdiyiniz təqdirdə tədbirdən bir gün öncə məlumat ana səhifədə əks olunmayacaq.',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        positiveButtons: {
                            text: 'Davam et',
                            btnClass: 'btn-green',
                            action: function () {
                                saveTaskAjax();
                            }
                        },
                        negativeButtons: {
                            text: 'Xeyr',
                            btnClass: 'btn-red',
                            action: function () {

                            }
                        },
                    }
                });
            }
            else{
                saveTaskAjax();
            }
        }

        function saveTaskAjax(){
            $.ajax({
                type: 'POST',
                url: '{{ route('advert_task.save',$task_id) }}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == "ok") {
                        location.reload();
                    }
                    else {
                        $("#errors").html(response.errors);
                    }
                },
                error: function (response) {
                    console.log("error");
                    console.log(response);
                }
            });
        }

        function checkValidation(form) {
            let name = form.find("[name='name']"),
                date_of_show = form.find("[name='date_of_show']"),
                range = form.find("[name='range']"),
                content = form.find("#content"),
                status = true;

            name.css("border", "");
            date_of_show.css("border", "");
            range.css("border", "");
            content.css("border", "");

            if (name.val().trim().length == 0) {
                name.css("border", "1px dotted red");
                status = false;
            }
            if (date_of_show.val().trim().length == 0) {
                date_of_show.css("border", "1px dotted red");
                status = false;
            }
            if (range.val().length < 1) {
                range.css("border", "1px dotted red");
                status = false;
            }
            if (content.val().trim().length == 0) {
                content.css("border", "1px dotted red");
                status = false;
            }

            return status;
        }
    });

    $('#teachers_select').multiSelect({
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiSelectAll(\"#teachers_select\")'>Hamısını seç >></a></div>",
        selectionFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiDeSelectAll(\"#teachers_select\")'><< Hamısını sil</a></div>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });
    $('#parents_select').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiSelectAll(\"#parents_select\")'>Hamısını seç >></a></div>",
        selectionFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiDeSelectAll(\"#parents_select\")'><< Hamısını sil</a></div>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });
    $('#students_select').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiSelectAll(\"#students_select\")'>Hamısını seç >></a></div>",
        selectionFooter: "<div class='custom-header'><a class='btn btn-default' style='width: 100%' onclick='multiDeSelectAll(\"#students_select\")'><< Hamısını sil</a></div>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    function multiSelectAll(sel) {
        $(sel).multiSelect('select_all');
    }

    function multiDeSelectAll(sel) {
        $(sel).multiSelect('deselect_all');
    }
</script>


