<div class="modal-dialog">
    <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Hərf <span class="semi-bold">{{ $id > 0 ? 'Düzəliş' : 'Əlavə et' }}</span></h5>
            </div>
            <div class="modal-body">
                <div class="row" id="errors">

                </div>
                <form id="modalUserAdd" role="form" onsubmit="return false;" autocomplete="off">
                    <input type="hidden" name="class_id" value="{{ $class_id }}">
                    <input type="hidden" name="year_id" value="{{ $year_id }}">
                    <input type="hidden" name="corpus_id" value="{{ $corpus_id }}">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default required">
                                <label>Adi</label>
                                <input type="text" class="form-control" value="{{ $letter['name'] }}" name="name" maxlength="60" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-t-10 sm-m-t-10">
                            <button class="btn btn-success" typeof="submit"><i class="fa fa-save"></i> Saxla</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="pg-close"></i> Bağla</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<script>
    $("#myModal #modalUserAdd").validate({
        submitHandler: function(form) {
            let data = new FormData($("#myModal #modalUserAdd")[0]);
            data.append('_token',_token);

            $.ajax({
                url: "{{ route('class_group_letter_add_edit_action',['letter' => $id]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error') $("#myModal #errors").html(response['errors']);
                    else {
                        $('#myModal').modal('hide');
                        openPage('#page-inside', '{{ route("class_groups_page") }}', {year_id: {{ $year_id }}, corpus_id: {{ $corpus_id }}, class_id: {{ $class_id }} });
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $('#myModal select').select2();
</script>