@extends('base')

@section('container')
    <form>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Tədrİs İlİ</label>
                    <select class="full-width select2" data-placeholder="Sinif" id="year">
                        @foreach(\App\Models\Year::realData()->where('active', 1)->orderBy('id')->get() as $year)
                            <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Sİnİf</label>
                    <input class="full-width" data-placeholder="Sinif" id="class">
                </div>
                <div class="col-md-3 form-group form-group-default form-group-default-select2 text-center">
                    <label style="width: 100%">Fənn</label>
                    <input class="full-width"  id="subject">
                </div>
                <div class="col-md-3 form-group form-group-default text-center" id="date-sel">
                    <label style="width: 100%">Tarix</label>
                    <input class="form-control text-center" id="date">
                </div>
            </div>
        </div>
    </form>
    <div id="data-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <style>
        #date-sel{
            display: none;
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>

    <script>

        $("#year").select2().on("change", function () {
            $("#class").select2("val", null).trigger("change");
        });

        $("#class").select2({
            allowClear : true,
            ajax: {
                url: '{{ route('report_jurnal_year_classes') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        year_id: $("#year").val()
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on("change", function () {
            $("#subject").select2("val", null);
        });


        $("#subject").select2({
            allowClear:true,
            placeholder:'Fənn seçin',
            ajax: {
                url: '{{ route('report_jurnal_class_subjects') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        letter_group_id: $("#class").select2("data")['id'],
                        class_id: $("#class").select2("data")['class_id']
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on("change", function () {
            getPage();
        });

        function getPage() {
            if($("#subject").val() > 0){
                let data = {
                    year_id: $("#year").val(),
                    letter_id: $("#class").select2("data")['letter_id'],
                    subject_id: $("#subject").val(),
                    month: $("#date").datepicker("getDate").getMonth()+1,
                    year: $("#date").datepicker("getDate").getFullYear()
                };

                openPage('#data-inside', '{{ route('report_jurnal_students_page') }}', data);
            }
        }

        $.fn.datepicker.dates.en.months = {!! json_encode(\App\Library\Standarts::$months) !!};

        $('#date').datepicker({
            format: 'MM / yyyy',
            viewMode: "months",
            minViewMode: "months",
            allowClear: false,
            autoclose: true,
        });

        $('#date').datepicker('update', new Date());

    </script>
@endsection