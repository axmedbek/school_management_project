@extends('base')

@section('container')
    <div class="row">
        <div class="btn-group" style="float: left;display: none;width: 50%;">
            <button class="btn btn-primary btn-sm back-folder-btn" style="border-radius: 5px;"><i class="fa fa-arrow-left"></i> Geri</button>
            <div style="margin-left: 100px;padding: 5px;border:1px solid #B2B2B2;border-radius: 4px;font-weight: bold;">
                Qovluq adı : <span class="folder-name" style="font-style: italic;font-weight: 400;"></span>
            </div>
        </div>

        <div class="btn-group-small" style="float: right;">
            @if(\App\Library\Helper::has_priv('galery',\App\Library\Standarts::PRIV_CAN_EDIT))
                <a href="javascript:openModal('{{ route('folder.add.modal',['folder_id' => 0]) }}')"
                   type="button" class="btn btn-success btn-sm add-folder" style="border-radius: 5px;">
                    <i class="pg-plus_circle"></i> Yeni qovluq
                </a>
                <a href="javascript:void(0)" style="display: none;border-radius: 5px;width: 140px;"
                   type="button" class="btn btn-primary btn-sm edit-folder">
                    <i class="fa fa-pencil"></i> Qovluğu dəyişdir
                </a>
                <a href="javascript:void(0)" style="display: none;border-radius: 5px;width: 140px;"
                   type="button" class="btn btn-danger btn-sm delete-folder">
                    <i class="fa fa-trash"></i> Qovluğu sil
                </a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @foreach($folders as $folder)
                <div class="col-md-3 col-sm-4 folders" data-folder-id="{{ $folder['id'] }}">
                    <img src="{{ asset('assets/img/folder2.png') }}" alt="{{ $folder['name'] }}" width="230"
                         height="180">
                    <p style="position: absolute;
                                top: 30%;
                                left:20%;
                                color: white;
                                word-wrap: break-word;
                                margin-right: 20%;
                                font-size: 16px;
                                width: 150px;" class="text-center text-justify">
                        {{ $folder['name'] }}
                    </p>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row" style="display: none;">
        <form action="{{ route('galery.save.images') }}" enctype="multipart/form-data" class="dropzone"
              id="folder-image-upload" style="margin-top: 15px;" onsubmit="return false;" accept="images/*">
            <input type="hidden" name="folder_id">
        </form>
        <div class="text-center" style="margin-top: 15px;">
            <button class="btn btn-success add-images-btn">Şəkilləri qovluğa əlavə edin</button>
        </div>
        <div class="col-md-12 images" style="margin-top: 50px;">
        </div>
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-confirm.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dropzone.css') }}">
    <link href="{{ asset('assets/plugins/multi-select/multi-select.css') }}" media="screen" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/portal/bower_components/photo_swipe/dist/photoswipe.css') }}" rel="stylesheet">
    <link href="{{ asset('/portal/bower_components/photo_swipe/dist/default-skin/default-skin.css') }}" rel="stylesheet">
    <style>
        body {
            font-family: Verdana, sans-serif;
            margin: 0;
        }

        .fs-11 {
            font-size: 9px !important;
        }

        * {
            box-sizing: border-box;
        }

        .row > .column {
            padding: 0 8px;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .column {
            float: left;
            width: 25%;
        }

        .imageModal {
            display: none;
            position: fixed;
            z-index: 1;
            padding-top: 12%;
            left: 40px;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(45, 44, 50, 0.85);
            z-index: 99;
        }

        /* Modal Content */
        .image-modal-content {
            position: relative;
            background-color: #8a8c89;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 1200px;
        }

        /* The Close Button */
        .closeBtn {
            position: absolute;
            top: 2px;
            color: #8cc4ea;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
        }

        .image-icons {
            position: absolute;
            margin-left: 25%;
            top: 48%;
        }

        .image-icons-hover {
            border: 2px solid grey;
            padding: 5px;
        }

        /*.download {*/
            /*position: absolute;*/
            /*color: #48b0f7;*/
            /*top: 10px;*/
            /*right: 60px;*/
            /*font-size: 30px;*/
            /*font-weight: bold;*/
        /*}*/

        .closeBtn:hover,
        .closeBtn:focus {
            color: #48b0f7;
            text-decoration: none;
            cursor: pointer;
        }

        .mySlides {
            display: none;
        }

        .cursor {
            cursor: pointer;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        img {
            margin-bottom: -4px;
        }

        .caption-container {
            text-align: center;
            background-color: #8a8c89;
            padding: 2px 16px;
            color: white;
        }

        .demo {
            opacity: 0.6;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }

        img.hover-shadow {
            transition: 0.3s;
        }

        .hover-shadow:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .images img:hover{
            cursor: pointer;
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dropzone.js') }}"></script>
    <script src="{{ asset('assets/plugins/multi-select/jquery.quicksearch.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/multi-select/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/portal/bower_components/photo_swipe/dist/photoswipe.min.js') }}"></script>
    <script src="{{ asset('/portal/bower_components/photo_swipe/dist/photoswipe-ui-default.min.js') }}"></script>
    <script>

        Dropzone.autoDiscover = false;

        $(document).ready(function () {
            $('.folders').hover(function () {
                $(this).css('cursor', 'pointer');
            });
            $('.delete-folder').click(function () {
                var folder_id = $('input[name="folder_id"]').val();
                $.confirm({
                    title: 'Təsdiq',
                    content: 'Silmək istədiyinizə əminsiniz?',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        formSubmit: {
                            text: "Sil",
                            btnClass: 'btn-green',
                            action: function () {
                                $.post("{{ route('galery.delete.folder') }}", {
                                    id: folder_id,
                                    _token: _token
                                }, function (response) {
                                    if (response['status'] == 'error') $("#errors").html(response['errors']);
                                    else {
                                        location.reload();
                                    }
                                });
                            }

                        },
                        formCancel: {
                            text: "İmtina et",
                            btnClass: 'btn-red',
                            action: function () {

                            }

                        }
                    }
                });
            });

        });
        var dz = new Dropzone("#folder-image-upload", {
            url: $('#folder-image-upload').attr('action'),
            maxFilesize: 500, // MB
            maxFiles: 20,
            addRemoveLinks: true,
            dictDefaultMessage: 'Yükləmək istədiyiniz faylları buraya sürükləyin',
            dictRemoveFile: 'Faylı silin',
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            autoProcessQueue: false
        });

        $('.folders').on('click', function () {
            var folder_id = $(this).attr('data-folder-id'),
                folder_name = $(this).find('p').text();
            url = '{{ route("folder.add.modal", ":id") }}';
            url = url.replace(':id', folder_id);
            $('input[name="folder_id"]').val(folder_id);
            $('.folder-name').text(folder_name);
            openPage('.images', '{{ route('galery.load.images') }}', {'id': folder_id});
            $('.edit-folder').attr('href','javascript:openModal("'+url+'")');
            $('.images').parent('div').show();
            $('.back-folder-btn').parent('div').show();
            $('.delete-folder').show();
            $('.edit-folder').show();
            $('.add-folder').hide();
            $(this).parents('div:eq(1)').hide();
        });

        $('.back-folder-btn').on('click', function () {
            $('.images').html('');
            $('.folders').parents('div:eq(1)').show();
            $('.add-folder').show();
            $('.delete-folder').hide();
            $('.edit-folder').hide();
            $('.images').parent('div').hide();
            $(this).parent('div').hide();
        });

        $('.add-images-btn').on('click', function () {
            var form = $('#folder-image-upload'),
                files = form.get(0).dropzone.getAcceptedFiles(),
                url = form.attr('action'),
                folder_id = form.find('input[name="folder_id"]').val(),
                formData = new FormData();

            formData.append('_token', _token);
            formData.append('folder_id', folder_id);
            Array.prototype.forEach.call(files, function (file) {
                formData.append('files[]', file);
            });

            if (true) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        dz.removeAllFiles();
                        $('.images').html(response);
                    },
                    error: function (response) {
                        console.log("error");
                        console.log(response);
                    }
                });
            }
        });

        function openImageModal() {
            document.getElementById('imageModal').style.display = "block";
        }

        function closeModal() {
            document.getElementById('imageModal').style.display = "none";
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
            captionText.innerHTML = dots[slideIndex - 1].alt;
        }
    </script>
@endsection
