@extends('base')

@section('container')
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div style="float: right;">
            <ul class="nav nav-tabs upper">

            </ul>
            <ul class="nav nav-pills smaller d-lg-flex">
                <li class="nav-item"><a class="nav-link active show" view_type="view_type_1" data-toggle="tab" href="#">
                        <i class="fa fa-square" style="color : #047bf8"></i></a></li>
                <li class="nav-item"><a class="nav-link" view_type="view_type_2" data-toggle="tab" href="#"> <i
                                class="fa fa-columns"></i></a></li>
                <li class="nav-item"><a class="nav-link" view_type="view_type_4" data-toggle="tab" href="#"> <i
                                class="fa fa-th-large"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12" id="all_cameras_content">
        <iframe src="" id="frame" style="display:none;" frameborder="0"></iframe>
    </div>

    <script type="text/html" id="view_type_1">
        <div class="col-lg-12">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-1">
                                <h6 class="element-header camera_name_text">Kamera 1</h6>
                            </div>
                            <div class="col-md-4">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 417px;">

                    </div>
                </div>
            </div>
        </div>
    </script>
    <script type="text/html" id="view_type_2">
        <div class="col-md-6 col-lg-6">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="element-header camera_name_text">Kamera 1</h6>
                            </div>
                            <div class="col-md-10">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 329px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="element-header camera_name_text">Kamera 2</h6>
                            </div>
                            <div class="col-md-10">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 329px;">

                    </div>
                </div>
            </div>
        </div>
    </script>
    <script type="text/html" id="view_type_4">
        <div class="col-lg-6">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="element-header camera_name_text">Kamera 1</h6>
                            </div>
                            <div class="col-md-6">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 329px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 b-l-lg">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="element-header camera_name_text">Kamera 2</h6>
                            </div>
                            <div class="col-md-6">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 329px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="element-header camera_name_text">Kamera 3</h6>
                            </div>
                            <div class="col-md-6">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 329px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 b-l-lg">
            <div class="padded-lg">
                <div class="element-wrapper camera-element">
                    <div class="element-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="element-header camera_name_text">Kamera 4</h6>
                            </div>
                            <div class="col-md-6">
                                <form class="form-inline justify-content-sm-end">
                                    {{--<select class="form-control form-control-sm rounded" name="camera">--}}
                                    {{--<option></option>--}}
                                    {{--{!! $camerasHtml !!}--}}
                                    {{--</select>--}}
                                    <input type="text" name="camera_rooms" class="full-width"
                                           style="width: 80% !important;">
                                    <a class="btn btn-outline-info ml-2 btn-rounded"
                                       onclick="reconnectCam($(this))"><i class="fa fa-refresh"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="element-box" style="min-height: 329px;">

                    </div>
                </div>
            </div>
        </div>
    </script>
@endsection

@section('css')
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
    <style>
        .nav-link i {
            color: #047bf8;
        }

        .nav-link.active i {
            color: white;
        }

        .nav.smaller.nav-pills .nav-link {
            padding: 0.3em 1.1em;
            padding-top: 0.8em;
        }

        #all_cameras_content .element-box {
            background-image: url("/portal/img/camera.png");
            background-position: center;
            background-size: 155px;
            background-repeat: no-repeat;
        }

        .camera_name_text {
            font-weight: 500;
        }

        .camera-element {
            padding: 5px;
            border: 1px solid gray;
            border-radius: 5px;
            margin-top: 10px;
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}"></script>
    <script>

        $("a.nav-link[view_type]").click(function () {
            let type = $(this).attr("view_type");

            $("#all_cameras_content").html($("#" + type).html());

            $('input[name="camera_rooms"]').select2({
                allowClear: true,
                placeholder: 'Otaq seçin',
                ajax: {
                    url: '{{ route('camera_get_rooms') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            ne: 'get_all_rooms',
                            q: word,
                        };
                    },
                    results: function (data, page) {
                        return data;
                    },
                    cache: true
                },
            });

            camChange();
            //$("#all_cameras_content select").select2();
        });

        $("a.nav-link[view_type]:eq(0)").click();

        function camChange() {
            $('input[name="camera_rooms"]').change(function () {
                var camera = $(this).val();
                if (camera == "") {
                    $(this).parents('.padded-lg:eq(0)').find('.element-box').html('');
                    return;
                }
                $(this).parents('.padded-lg:eq(0)').find('.element-box').html('<img src="http://'+camera+'/goform/stream?cmd=get&channel=4" style="width:100%;" />');

                // $('#frame').attr('src', 'http://admin:j272304j@' + camera + '');
                // $('#frame').attr('src', 'http://admin:j272304j@' + camera + '');

               let cam1 = window.open('http://admin:j272304j@' + camera + '', '', "width=1,height=1,menubar=no,location=no,resizable=no,scrollbars=no,status=no,adresbar=no");
                setTimeout(function () {
                    cam1.close();
                    // th.prev('input[name="camera_rooms"]').trigger('change');
                }, 1000);
                let cam2 = window.open('http://admin:j272304j@' + camera + '', '', "width=1,height=1,menubar=no,location=no,resizable=no,scrollbars=no,status=no,adresbar=no");
                setTimeout(function () {
                    cam2.close();
                    // th.prev('input[name="camera_rooms"]').trigger('change');
                }, 1000);
            });
        }

        //camChange();

        function reconnectCam(th) {
            let camera = th.parents(".form-inline:eq(0)").find('input[name="camera_rooms"]').val();
            let cam = window.open('http://admin:j272304j@' + camera + '', '', "width=10,height=10,menubar=no,location=no,resizable=no,scrollbars=no,status=no,adresbar=no");
            setTimeout(function () {
                cam.close();
                th.prev('input[name="camera_rooms"]').trigger('change');
            }, 1500);
        }

    </script>
@endsection