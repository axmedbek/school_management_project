<div class="col-lg-12" style="padding: 0px;">
    <div id="errors" class="row">

    </div>
    <div class="panel panel-transparent">
        <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
            <h4 style="padding-left:15px;margin:5px;border-bottom:2px solid #d9d9d9;">Fəsillər
                @if($change === true && \App\Library\Helper::has_priv('topics',\App\Library\Standarts::PRIV_CAN_EDIT))
                    <i class="pg-plus_circle"
                       onclick="openModal('{{ route('topic_season_add_edit',['season'=> 0]) }}',{class_id: {{ $class_id }}, subject_id: {{ $subject_id }} } )"
                       style="cursor: pointer;color: #10cfbd;"> </i>
                @endif
            </h4>
            <input title="Axtarış etmək istədiyiniz sözü daxil edikdən sonra kənara klikləyin" type="text" name="season"
                   class="form-control" placeholder="Axtar" value="{{ $searchedKey }}">
            @foreach($seasons as $season)
                <li season_id="{{ $season->id }}">
                    <a data-toggle="tab" href="#tab{{ $season->id }}"
                       style="max-width: 500px;word-wrap: break-word;font-family: Ubuntu;letter-spacing: 0.4px;">{{ $season->name }}
                        <span style="
                            font-size: 14px;
                            text-transform: initial;
                            font-family: Ubuntu;
                            font-weight: 400;
                            letter-spacing: 0.4px;">
                            ({{ implode(",", $season->group_types->pluck('name')->toArray()) }})
                        </span>
                        @if($change === true && \App\Library\Helper::has_priv('topics',\App\Library\Standarts::PRIV_CAN_EDIT))
                            <i style="font-size: 15px;cursor: pointer;color:blueviolet;" class="fa fa-edit"
                               onclick="openModal('{{ route('topic_season_add_edit',['season'=> $season->id]) }}',{class_id: {{ $class_id }}, subject_id: {{ $subject_id }} } )"></i>
                            <i style="font-size: 15px;cursor: pointer;color: red;"
                               class="fa fa-trash sesson_delete" onclick="deleteSession(this)"></i>
                        @endif
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content bg-white">
            @foreach($seasons as $season)
                <div class="tab-pane" id="tab{{ $season->id }}">
                    @if($change === true && \App\Library\Helper::has_priv('topics',\App\Library\Standarts::PRIV_CAN_EDIT))
                        <button type="button" class="btn btn-success pull-right add_new "><i
                                    class="pg-plus_circle"> </i> Yeni
                        </button>
                        <button type="button"
                                onclick="openModal('{{ route('topic_season_add_edit',['season'=> 0]) }}',{class_id: {{ $class_id }}, subject_id: {{ $subject_id }},isCopy:1,copySeasonId:{{ $season->id }} } )"
                                class="btn btn-danger pull-right copy_new " style="margin-right: 5px;"><i
                                    class="fa fa-copy"></i> Kopyala
                        </button>

                    @endif
                    <table class="table table-hover paragraph_table" id="basicTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Paragraf</th>
                            <th>Standart</th>
                            <th>Saat</th>
                            <th style="width: 185px;">Əməliyyat</th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th></th>
                            <th><input type="text" class="form-control" name="paragrahp_search" placeholder="Paraqraf axtar"></th>
                            <th></th>
                            <th></th>
                            <th style="width: 185px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($season->paragraphs as $paragraph)
                            <tr tr_id="{{ $paragraph->id }}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $paragraph->name }}</td>
                                <td>{{ $paragraph->standard }}</td>
                                <td>{{ $paragraph->hour }}</td>
                                <td>
                                    @if($change === true && \App\Library\Helper::has_priv('topics',\App\Library\Standarts::PRIV_CAN_EDIT))
                                        <div class="btn-group-sm">
                                            <a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i
                                                        class="fa fa-pencil"></i></a>
                                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i
                                                        class="fa fa-trash-o"></i></a>
                                        </div>
                                    @else
                                        <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script type="text/html" id="row">
    <td></td>
    <td><input type="text" style="color: #626262;" onfocus="this.removeAttribute('readonly');" readonly maxlength="255" required name="name" class="form-control"></td>
    <td><input type="text" style="color: #626262;" onfocus="this.removeAttribute('readonly');" readonly name="standard" class="form-control"></td>
    <td><input type="number" style="color: #626262;" onfocus="this.removeAttribute('readonly');" readonly step="0.01" name="hour" required class="form-control"></td>
    <td>
        <div class="btn-group-sm">
            <a type="button" class="btn btn-success col-lg-4 col-md-12 save"><i class="fa fa-save"></i></a>
            <a type="button" class="btn btn-warning col-lg-4 col-md-12 cancel"><i class="fa fa-remove"></i></a>
        </div>
    </td>
</script>

<script>

    $('[name="season"]').on('keyup', function () {
        let searchedKey = $(this).val();
        $.post('{{ route("topics_page") }}',{
            searchedKey: searchedKey,
            class_id: $("#classes").val(),
            subject_id: $("#select2Subjects").val(),
            _token : _token
        },function(response){
            $('[name="season"]').nextAll().remove();
            $('[name="season"]').after(response.data);
            $('[name="season"]').nextAll().eq(0).find('a').click();
        });
    });

    $('[name="paragrahp_search"]').on('keyup',function(){
        let searchedKey = $(this).val(),
            season_id = $('#tab-3').find('.active').attr('season_id');
        $.post('{{ route('topics_get_pragraph') }}',{searchedKey : searchedKey , season_id : season_id ,_token : _token},function(response){
            $('.paragraph_table tbody').html('');
            $('.paragraph_table tbody').html(response.data);
        });
    });

    // $("i").click('.sesson_delete',function () {
        function deleteSession(element){
            let tr = $(element).parents("li:eq(0)"),
                id = tr.attr("season_id");

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsiniz?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    "Sil": function () {
                        $.post("{{ route('topic_season_delete') }}", {id: id, _token: _token}, function (response) {
                            if (response['status'] == 'error') $("#errors").html(response['errors']);
                            else {
                                $("#select2Subjects").trigger("change");
                            }
                        });
                    },
                    "İmtina": function () {

                    },
                }
            });
        }
    // });
    //
    $(".add_new").click(function () {
        let tbody = $(this).parents(".tab-pane:eq(0)").find("tbody");

        tbody.parents(".tab-pane:eq(0)").find("tbody").append('<tr tr_id="0">' + $("#row").html() + '</tr>');
        tbody.find("tr:last").hide().show('fast');

        tbody.find("tr:last .cancel").click(function () {
            $(this).parents("tr:eq(0)").remove();
            order(tbody.parents("table:eq(0)"));
        });
        order(tbody.parents("table:eq(0)"));
    });

    $("tbody").on("click", ".save", function () {
        let tr = $(this).parents("tr:eq(0)"),
            data = {},
            season_id = $("#tab-3 li.active").attr("season_id"),
            id = tr.attr("tr_id");

        if (isValid(tr)) {
            tr.find("[name]").each(function () {
                data[$(this).attr("name")] = $(this).val();
            });
            data['id'] = id;
            data['season_id'] = season_id;
            data['_token'] = _token;

            $.post("{{ route('topic_paragraph_add_edit_action') }}", data, function (response) {
                if (response['status'] == 'error') $("#errors").html(response['errors']);
                else saveTr(tr, response['data']);
            });
        }
    });

    $("tbody").on("click", ".edit_data", function () {
        let tr = $(this).parents("tr:eq(0)"),
            name = tr.find("td:eq(1)").text().trim(),
            standard = tr.find("td:eq(2)").text().trim(),
            hour = tr.find("td:eq(3)").text().trim(),
            html = tr.html();

        tr.html($("#row").html());

        //vals
        tr.find("[name='name']").val(name);
        tr.find("[name='standard']").val(standard);
        tr.find("[name='hour']").val(hour);

        tr.find(".cancel").click(function () {
            tr.html(html);
            order(tr.parents("table:eq(0)"));
        });
        order(tr.parents("table:eq(0)"));
    });

    $("tbody").on("click", ".data_delete", function () {
        let tr = $(this).parents("tr:eq(0)"),
            id = tr.attr("tr_id");

        $.confirm({
            title: 'Təsdiq',
            content: 'Silmək istədiyinizə əminsizin?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                "Sil": function () {
                    $.post("{{ route('topic_paragraph_delete') }}", {id: id, _token: _token}, function (response) {
                        if (response['status'] == 'error') $("#errors").html(response['errors']);
                        else {
                            tr.remove();
                            order($("#basicTable"));
                        }
                    });
                },
                "İmtina": function () {

                },
            }
        });
    });

    function isValid(tr) {
        let name = tr.find("[name='name']"),
            standard = tr.find("[name='standard']"),
            hour = tr.find("[name='hour']"),
            status = true;

        name.css("border", "");
        hour.css("border", "");

        if (name.val().trim().length > 100 || name.val().trim().length == 0) {
            name.css("border", "1px dotted red");
            status = false;
        }
        if ((hour.val() > 0) == false) {
            hour.css("border", "1px dotted red");
            status = false;
        }

        return status;
    }

    function saveTr(tr, data) {
        tr.html('<td> </td>' +
            '<td>' + data['name'] + '</td>' +
            '<td>' + (data['standard'] == null ? '' : data['standard']) + '</td>' +
            '<td>' + data['hour'] + '</td>' +
            '<td>' +
            '<div class="btn-group-sm">' +
            '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>' +
            '<a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>' +
            '</div>' +
            '</td>'
        );
        tr.attr("tr_id", data['id']);
        order(tr.parents("table:eq(0)"));
    }

    function order(table) {
        let i = 0;
        table.find("tbody tr").each(function () {
            i++;
            $(this).find("td:eq(0)").text(i);
        });
    }

    $(function () {
        $("#tab-3 li:eq(0) a").click();
    });
</script>