<style>
    #student_report_table thead tr th {
        font-family: DejaVu Sans, sans-serif !important;
        text-align: center;
        color: #111111;
    }

    #student_report_table tbody tr td {
        font-family: DejaVu Sans, sans-serif !important;
        text-align: center;
    }

    #student_report_table thead tr th {
        border: 1px solid #36393a;
        color: #36393a;
        font-weight: bold;
    }

    #student_report_table tbody tr td {
        border: 1px solid #959b9c;
        text-align: center;
    }
</style>
{{--<input type="hidden" name="page_count" value="{{ $pageCount }}">--}}
<h5 class="titleOne" style="font-family: 'DejaVu Sans';text-align: center;display: none;">{{ $tableTitle }}</h5>
<h6 class="titleTwo" style="text-align: center;display: none;">{{ 'Tarix : '.$tableDate }}</h6>
<table class="table" id="student_report_table">
    <thead>
    <tr>
        <th>No</th>
        <th>ADI</th>
        <th>SOYADI</th>
        <th>ATA ADI</th>
        <th style="min-width: 130px;">TƏVƏLLÜDÜ</th>
        <th>Yaşı</th>
        <th>CİNSİ</th>
        <th>DOĞULDUĞU YER</th>
        <th>SİNİFİ</th>
        <th>TƏMAYÜL</th>
        <th>QRUPU</th>
        <th>FAKTİKİ ÜNVAN ŞƏHƏR</th>
        <th>FAKTİKİ ÜNVAN RAYON</th>
        <th>FAKTİKİ ÜNVAN</th>
        <th>QEYDİYYAT ÜNVAN ŞƏHƏR</th>
        <th>QEYDİYYAT ÜNVAN RAYON</th>
        <th>QEYDİYYAT ÜNVANI</th>
        <th style="min-width: 130px;">SİSTEMƏ QEYDİYYAT TARİXİ</th>
        <th>STATUSU</th>
        <th style="min-width: 130px;">DEAKTIV Tarixi</th>
        <th style="min-width: 170px;">EV TELEFONU</th>
        <th style="min-width: 170px;">MOBİL TELEFONU</th>
        <th style="min-width: 50px;">EMAIL</th>
        @for ($i = 0; $i < $maxFamilyCount; $i++)
            <th>QOHUMU</th>
            <th style="min-width: 100px;">ASA</th>
            <th style="min-width: 170px;">TEL</th>
        @endfor
    </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $student->username }}</td>
            <td>{{ $student['surname'] }}</td>
            <td>{{ $student->middle_name }}</td>
            <td>{{ $student->birthday == '' ? '-' : date('d-m-Y', strtotime($student->birthday)) }}</td>
            <td>{{ isset($student->birthday) ? date('Y') -  date('Y',strtotime($student->birthday)) : 'Təyin edilməyib'}}</td>
            <td>{{ $student->getGenger() }}</td>
            <td>{{ $student->birth_place }}</td>
            <td>
                @if(isset($student['class_name'])){{ $student['class_name']."/" }}{{ $student['class_letter_name'] }}@endif
            </td>
            <td>{{ $student['temayul'] }}</td>
            <td>
                @if(isset($student['group_name'])){{ $student['group_name'] }}{{ "(".$student['temayul'].")" }}@endif
            </td>
            <td>{{ $student->currentCity['name'] }}</td>
            <td>{{ $student->currentRegion['name'] }}</td>
            <td>{{ $student->current_address }}</td>
            <td>{{ $student->livedCity['name'] }}</td>
            <td>{{ $student->livedRegion['name'] }}</td>
            <td>{{ $student->lived_address }}</td>
            <td>{{ $student->created_at == '' ? '-' : date('d-m-Y', strtotime($student->created_at)) }}</td>
            <td>{{ $student->getStatus() }}</td>
            <td>{{ $student->deleted_at == '' ? '-' : date('d-m-Y', strtotime($student->deleted_at)) }}</td>
            <td>{{ $student->home_tel }}</td>
            <td>{{ $student->mobil_tel }}</td>
            <td>{{ $student->email }}</td>
            @for ($i = 0; $i < $maxFamilyCount; $i++)
                @if (isset($student->person_families[$i]))
                    @php $familyObj = \App\User::find($student->person_families[$i]->parent_id) @endphp
                    <td>{{ $student->person_families[$i]->msk_relation->name }}</td>
                    <td>{{ $familyObj['surname']." ". $familyObj['name']." ".$familyObj['middle_name'] }}</td>
                    <td>{{ $familyObj['mobil_tel'] }}</td>
                @else
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
            @endfor
        </tr>
    @endforeach
    </tbody>
</table>


{{--<script>--}}
    {{--let pageCount = $('[name="page_count"]').val();--}}
    {{--for (let i = 0; i < pageCount ; i++){--}}
        {{--setTimeout(function(){--}}
            {{--$.post('{{ route('') }}',data,function (response) {--}}
                {{--$('#student_report_table').--}}
            {{--});--}}
        {{--},3000);--}}
    {{--}--}}
{{--</script>--}}