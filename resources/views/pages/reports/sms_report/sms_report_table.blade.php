<style>
    #sms_report_table thead tr th {
        font-family: DejaVu Sans, sans-serif !important;
        border: 1px solid #a5a9ac;
    }

    #sms_report_table tbody tr td {
        font-family: DejaVu Sans, sans-serif !important;
        border: 1px solid #a5a9ac;
    }
    .title {
        font-family: DejaVu Sans, sans-serif !important;
    }
</style>

<div class="text-center title" style="margin-top: 10px;
    margin-bottom: 30px;
    font-weight: bold;
    display : {{ $title ? 'block' : 'none' }}">
    Bakı Modern Təhsil Kompleksinin {{ $date }} tarixinə SMS Hesabatı
</div>
<table class="table table-bordered" id="sms_report_table">
    <thead>
    <tr>
        <th>No</th>
        <th>Ad Soyad Ata Adı</th>
        <th>Daxil olub</th>
        <th>SMS Status</th>
        <th>Xaric olub</th>
        <th>SMS Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($smses as $sms)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $sms->name." ".$sms->surname." ".$sms->middle_name }}</td>
            <td>{{ $sms->in_out_type == 'In' ? \Carbon\Carbon::parse($sms->date)->format('Y-m-d , H:i') : '' }}</td>
            <td>{{ $sms->in_out_type == 'In' ? $sms->sms_status : ''}}</td>
            <td>{{ $sms->in_out_type == 'Out' ? \Carbon\Carbon::parse($sms->date)->format('Y-m-d , H:i') : '' }}</td>
            <td>{{ $sms->in_out_type == 'Out' ? $sms->sms_status : ''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $('#sms_report_table').find('tbody>tr').each(function (i) {
        $(this).find('td:eq(0)').text(i + 1);
    });
</script>