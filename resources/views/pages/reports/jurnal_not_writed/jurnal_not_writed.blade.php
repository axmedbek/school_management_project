<style>
    #jurnal_not_writed_table thead tr th {
        font-family: DejaVu Sans, sans-serif !important;
        border: 1px solid #a5a9ac;
        /*font-size: 10px;*/
        /*padding: 0;*/
    }

    #jurnal_not_writed_table tbody tr td {
        font-family: DejaVu Sans, sans-serif !important;
        border: 1px solid #a5a9ac;
        /*font-size: 10px;*/
        /*padding: 0;*/
    }
    .title {
        font-family: DejaVu Sans, sans-serif !important;
    }
    .select2-search-choice-close{
        margin-top: 0;
    }
</style>

<input type="hidden" data-select2="teachers" value='@json($teacherSelect2)'>
<input type="hidden" data-select2="letter_groups" value='@json($letterGroupSelect2)'>
<input type="hidden" data-select2="subjects" value='@json($subjectSelect2)'>


<div id="jurnal_title" class="text-center title" style="
    margin-top: 10px;
    margin-bottom: 30px;
    font-weight: bold;
    display: none;">
    Bakı Modern Təhsil Kompleksinin {{ $selectedDate }} tarixində Jurnal yazılmaması üzrə hesabatı
</div>

<table class="table table-bordered" id="jurnal_not_writed_table">
    <thead>
    <tr>
        <th>No</th>
        <th>Tarix</th>
        <th>Müəllim</th>
        <th>Sinif</th>
        <th>Fənn</th>
    </tr>
    </thead>
    <thead style="display: {{ $filter ? 'contents' : 'none' }}" id="jurnal_filter_head">
    <tr>
        <th>#</th>
        <th><input type="text" class="form-control" name="filter_date" value="{{ request('filter_date') }}"></th>
        <th><input class="full-width" data-placeholder="Müəllim" id="teachers"></th>
        <th><input class="full-width" data-placeholder="Sinif" id="class"></th>
        <th><input class="full-width" data-placeholder="Fənn" id="subject"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key =>  $userData)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $userData['date'] }}</td>
            <td>{{ $userData['full_name'] }}</td>
            <td>{{ $userData['letter_group_name'] }}</td>
            <td>{{ $userData['subject_name'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    // $('#jurnal_not_writed_table').find('tbody>tr').each(function (i) {
    //     $(this).find('td:eq(0)').text(i + 1);
    // });

    $('[name="filter_date"]').datepicker({
       autoclose : true,
       format : 'yyyy-mm-dd',
        clearBtn : true
    }).on('changeDate',function(){
        if ($(this).val().length > 0) {
            if (Object.prototype.toString.call(new Date($(this).val())) === "[object Date]") {
                if (isNaN((new Date($(this).val())).getTime())) {
                    // $('.table-errors').show();
                } else {
                    if ($(this).val().match(/^\d{4}[./-]\d{2}[./-]\d{2}$/)) {
                        $('.table-errors').hide();
                        getTable();
                    }
                    else {
                        $('.table-errors').show();
                    }

                }
            } else {
                $('.table-errors').show();
            }
        }
        else {
            getTable();
        }
    });

    $("#teachers").select2({
        allowClear: true,
        ajax: {
            url: '{{ route('search_for_select') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    ne: 'getTeachers',
                    filter_date : $('[name="filter_date"]').val()
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on("change", function () {
        $("#class").select2("val", null);
        getTable();
    });

    $("#class").select2({
        allowClear: true,
        ajax: {
            url: '{{ route('reports_jurnal_not_writed_year_classes') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    teacher_id: $("#teachers").val()
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on("change", function () {
        $("#subject").select2("val", null);
        getTable();
    });


    $("#subject").select2({
        allowClear: true,
        placeholder: 'Fənn seçin',
        ajax: {
            url: '{{ route('reports_jurnal_not_writed_class_subjects') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    letter_group_id: $("#class").val(),
                    teacher_id: $("#teachers").val()
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        },
    }).on("change", function () {
        getTable();
    });


    if ($('[data-select2="teachers"]').val() != "[]"){
        $("#teachers").select2('data',{!! json_encode($teacherSelect2) !!});
    }

    if ($('[data-select2="letter_groups"]').val() != "[]"){
        $("#class").select2('data',{!! json_encode($letterGroupSelect2) !!});
    }

    if ($('[data-select2="subjects"]').val() != "[]"){
        $("#subject").select2('data',{!! json_encode($subjectSelect2) !!});
    }

</script>