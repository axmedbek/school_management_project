<style>
    #staff_report_table thead tr th{
        font-family: DejaVu Sans, sans-serif !important;
        border: 2px solid #36393a;
        color: #36393a;
        font-weight: bold;
    }
    #staff_report_table tbody tr td{
        font-family: DejaVu Sans, sans-serif !important;
        border: 2px solid #959b9c;
        text-align: center;
    }
</style>


<h2 style="font-family: 'DejaVu Sans';text-align: center;">{{ isset($tableTitle) ? $tableTitle : '' }}</h2>
<h3 style="text-align: center;">{{ isset($tableDate) ? 'Tarix : '.$tableDate : '' }}</h3>
<table class="table" id="staff_report_table">
    <thead>
    <tr>
        <th>S/S</th>
        <th>ADI</th>
        <th>SOYADI</th>
        <th>ATA ADI</th>
        <th>Vəzifə</th>
        <th>Heyyət</th>
        <th>TƏVƏLLÜDÜ</th>
        <th>Yaşı</th>
        <th>CİNSİ</th>
        <th>DOĞULDUĞU YER</th>
        <th>FAKTİKİ ÜNVAN ŞƏHƏR</th>
        <th>FAKTİKİ ÜNVAN RAYON</th>
        <th>FAKTİKİ ÜNVAN</th>
        <th>QEYDİYYAT ÜNVAN ŞƏHƏR</th>
        <th>QEYDİYYAT ÜNVAN RAYON</th>
        <th>QEYDİYYAT ÜNVANI</th>
        <th>SİSTEMƏ QEYDİYYAT TARİXİ</th>
        <th>STATUSU</th>
        <th>DEAKTIV Tarixi</th>
        <th>EV TELEFONU</th>
        <th>MOBİL TELEFONU</th>
        <th>EMAIL</th>
    </tr>
    </thead>
    <tbody>
        @foreach($staffs as $staff)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $staff->name }}</td>
                <td>{{ $staff->surname }}</td>
                <td>{{ $staff->middle_name }}</td>
                <td>{{ $staff->position }}</td>
                <td>{{ $staff->heyet['name'] }}</td>
                <td>{{ $staff->birthday == '' ? '-' : date('d-m-Y', strtotime($staff->birthday)) }}</td>
                <td>{{ $staff->age }}</td>
                <td>{{ $staff->getGenger() }}</td>
                <td>{{ $staff->birth_place }}</td>
                <td>{{ $staff->currentCity['name'] }}</td>
                <td>{{ $staff->currentRegion['name'] }}</td>
                <td>{{ $staff->current_address }}</td>
                <td>{{ $staff->livedCity['name'] }}</td>
                <td>{{ $staff->livedRegion['name'] }}</td>
                <td>{{ $staff->lived_address }}</td>
                <td>{{ $staff->enroll_date == '' ? '-' : date('d-m-Y', strtotime($staff->enroll_date)) }}</td>
                <td>{{ $staff->getStatus() }}</td>
                <td>{{ $staff->deleted_at == '' ? '-' : date('d-m-Y', strtotime($staff->deleted_at)) }}</td>
                <td>{{ $staff->home_tel }}</td>
                <td>{{ $staff->mobil_tel }}</td>
                <td>{{ $staff->email }}</td>
            </tr>
        @endforeach
    </tbody>
</table>