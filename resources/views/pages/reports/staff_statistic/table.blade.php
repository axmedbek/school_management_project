<style>
    #staff_report_table thead tr th{
        font-family: DejaVu Sans, sans-serif !important;
    }
    #staff_report_table tbody tr td{
        font-family: DejaVu Sans, sans-serif !important;
    }
    #staff_report_table thead tr th{
        border: 2px solid #959b9c;
    }
    #staff_report_table tbody tr td{
        border: 2px solid #959b9c;
    }
</style>

<h2 style="font-family: 'DejaVu Sans';text-align: center;">{{ isset($tableTitle) ? $tableTitle : '' }}</h2>
<h3 style="text-align: center;">{{ isset($tableDate) ? 'Tarix : '.$tableDate : '' }}</h3>
<table class="table" id="staff_report_table" align="center">
    <thead>
    <tr>
        <th>No</th>
        <th>{{ $groupName }}</th>
        <th>Dövrün əvvəlinə personal sayı</th>
        <th>Dövrün sonuna personal sayı</th>
    </tr>
    </thead>
    <tbody>
        @php $sumFirst = 0; $sumLast = 0; @endphp
        @foreach($mergedData as $data)
            @php $sumFirst += $data['countFirst']; $sumLast += $data['countLast']; @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $data['name'] == '' ? 'Təyin edilməyib' : $data['name'] }}</td>
                <td>{{ $data['countFirst'] }}</td>
                <td>{{ $data['countLast'] }}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="2" align="center">Ümumi</td>
            <td>{{ $sumFirst }}</td>
            <td>{{ $sumLast }}</td>
        </tr>
    </tbody>
</table>