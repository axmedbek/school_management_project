<script>
    var groups = {!! json_encode(Illuminate\Support\Facades\Cache::get('ders_cedveli')['groups']) !!};
    if (groups[0]){
        $('.excel-export-btn').css('display','');
    }
    else{
        $('.excel-export-btn').css('display','none');
    }
</script>

@foreach($groups as $group)
    <div class="panel-group" id="accordion{{ $group->id }}" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{ $group->id }}">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion{{ $group->id }}" href="#collapse{{ $group->id }}" aria-expanded="false" aria-controls="collapse{{ $group->id }}" class="collapsed">
                        SİNİF {{ $group->class_name }}{{ $group->letter_name }} Qrup {{ $group->name }}
                    </a>
                </h4>
            </div>
            <div id="collapse{{ $group->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $group->id }}" aria-expanded="false" style="height: 0px;">
                <div class="row">
                    <div class="panel-body col-md-12">
                        @php
                            $lessonData = [];
                            $lessons = $group->lessons;
                            if($lessons != null)
                            foreach ($lessons as $lesson){
                                if( !isset($lessonData[$lesson->week_day]) ) $lessonData[$lesson->week_day] = [];
                                $lessonData[$lesson->week_day][$lesson->class_time_id] = $lesson;
                            }
                        @endphp


                        @foreach(\App\Library\Standarts::$weekDayNames as $key => $weekDay)
                            <div class="col-md-4">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th colspan="3" class="week-day-th">{{ $weekDay }}</th>
                                    </tr>
                                    <tr>
                                        <th>Saat</th>
                                        <th>Fənn</th>
                                        <th>Otaq</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($classTimes as $classTime)
                                            @php $chk = isset($lessonData[$key + 1]) && isset($lessonData[$key + 1][$classTime->id]) ? $lessonData[$key + 1][$classTime->id] : false; @endphp

                                            @if($chk)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $chk->subject->name }}</td>
                                                    <td>{{ $chk->room->name }}</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

