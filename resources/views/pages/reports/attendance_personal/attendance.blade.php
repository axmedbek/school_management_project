<style>
    #tabel_table thead tr th{
        font-family: DejaVu Sans, sans-serif !important;
    }
    #tabel_table tbody tr td{
        font-family: DejaVu Sans, sans-serif !important;
    }
</style>

<table class="table table-bordered" id="tabel_table">
    <thead>
        <tr>
            <th>No</th>
            <th>Ad Soyad Ata Adı</th>
            <th>Vəzifə</th>
            @for($date = \Illuminate\Support\Carbon::parse($data['firstDay']); $date->lte(\Illuminate\Support\Carbon::parse($data['lastDay'])); $date->addDay())
                <th colspan="2" style="text-align: center">{{ $date->format('Y-m-d') }}</th>
            @endfor
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            @for($date = \Illuminate\Support\Carbon::parse($data['firstDay']); $date->lte(\Illuminate\Support\Carbon::parse($data['lastDay'])); $date->addDay())
                <th>Gecikmə</th>
                <th>Tez tərk etmə</th>
            @endfor
        </tr>
    </thead>
    <tbody>
        @foreach($data['userData'] as $user)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $user['fullname'] }}</td>
                <td>{{ $user['position'] }}</td>
                @for($date = \Illuminate\Support\Carbon::parse($data['firstDay']); $date->lte(\Illuminate\Support\Carbon::parse($data['lastDay'])); $date->addDay())
                    <td>{{ isset($user[$date->format('Y-m-d')])? $user[$date->format('Y-m-d')]['In'] : '-' }}</td>
                    <td>{{ isset($user[$date->format('Y-m-d')])? $user[$date->format('Y-m-d')]['Out'] : '-' }}</td>
                @endfor
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    $('#tabel_table').find('tbody>tr').each(function(i){
        $(this).find('td:eq(0)').text(i + 1);
    });
</script>