<style>
    .table {
        border: 1px solid #69ca9d;
        text-align: center;
        width: 100%;
    }

    .table tbody tr td {
        border: 1px solid #69ca9d;
        background-color: #efeff0;
        font-family: DejaVu Sans, sans-serif !important;
    }

    .table thead tr th {
        border: 1px solid #69ca9d;
        color: #63666d;
        background-color: #efeff0;
        font-family: DejaVu Sans, sans-serif !important;
    }

    .table_title {
        font-family: DejaVu Sans, sans-serif !important;
        text-align: center;
        width: 35%;
    }
</style>


<div class="col-md-4 text-center table_title" style="
            margin-top: 8%;
            margin-bottom: 4%;
    border: 2px solid #69ca9d;
    margin-left: 38%;
    padding-top: 10px;">
    <p class="text-center">Bakı Modern Təhsil
        Kompleksinin {{ Carbon\Carbon::parse($year->start_date)->format('Y')." - ".Carbon\Carbon::parse($year->end_date)->format('Y') }}
        tədris ilinin yekun qiymətləndirmə cədvəli.</p>
</div>
@foreach($classLetters as $classLetter)
    <table class="table table-bordered" id="table{{ $classLetter->id }}">
        <thead>
        <tr>
            <th colspan="2">
                Sinif : {{ $classLetter->class_name }}/{{ $classLetter->name }}
            </th>
        </tr>
        <tr>
            <th rowspan="3">No</th>
            <th rowspan="3">Ad Soyad Ata Adı</th>
        </tr>
        <tr>
            @foreach($subjects as $subject)
                @if ($subject['letter_group_id'] == $classLetter->id)
                    <th colspan="3">{{ $subject->name }}</th>
                @endif
            @endforeach
        </tr>
        <tr>
            @foreach($subjects as $subject)
                @if ($subject['letter_group_id'] == $classLetter->id)
                    @foreach($evals as $key => $eval)
                        @if ($eval->type == "yi")
                            <th>Y{{$key+1}}</th>
                        @else
                            <th>İ</th>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </tr>
        </thead>
        <tbody>
        @php $sira = 0 ; @endphp
        @foreach ($users as $user)
            @if ($user['letter_group_id'] == $classLetter['id'])
                @php ++$sira; @endphp
                <tr>
                    <td>{{ $sira }}</td>
                    <td>{{ $user['name']." ".$user['surname']." ".$user['middle_name'] }}</td>
                    @foreach($subjects as $subject)
                        @if ($subject['letter_group_id'] == $classLetter->id)
                            @foreach($evalValues[$classLetter->id][$user->id][$subject->id] as $mark)
                                <td>{{ $mark }}</td>
                            @endforeach
                        @endif
                    @endforeach
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    <div style="margin-top: 5%;"></div>
@endforeach