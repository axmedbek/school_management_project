
    <style>
        #teacher_report_table thead tr th{
            font-family: DejaVu Sans, sans-serif !important;
            border: 2px solid #36393a;
            color: #36393a;
            font-weight: bold;
        }
        #teacher_report_table tbody tr td{
            font-family: DejaVu Sans, sans-serif !important;
            border: 2px solid #959b9c;
            text-align: center;
        }
    </style>

<h2 style="font-family: 'DejaVu Sans';text-align: center;">{{ isset($tableTitle) ? $tableTitle : '' }}</h2>
<h3 style="text-align: center;">{{ isset($tableDate) ? 'Tarix : '.$tableDate : '' }}</h3>
<table class="table" id="teacher_report_table">
    <thead>
    <tr>
        <th>S/S</th>
        <th>ADI</th>
        <th>SOYADI</th>
        <th>ATA ADI</th>
        <th>MÜƏLLİM STATUSU</th>
        <th>TƏHSİLİ</th>
        <th>TƏVƏLLÜDÜ</th>
        <th>YAŞI</th>
        <th>CİNSİ</th>
        <th>DOĞULDUĞU YER</th>
        <th>FAKTİKİ ÜNVAN ŞƏHƏR</th>
        <th>FAKTİKİ ÜNVAN RAYON</th>
        <th>FAKTİKİ ÜNVAN</th>
        <th>QEYDİYYAT ÜNVAN ŞƏHƏR</th>
        <th>QEYDİYYAT ÜNVAN RAYON</th>
        <th>QEYDİYYAT ÜNVANI</th>
        <th>SİSTEMƏ QEYDİYYAT TARİXİ</th>
        <th>STATUSU</th>
        <th>DEAKTIV Tarixi</th>
        <th>EV TELEFONU</th>
        <th>MOBİL TELEFONU</th>
        <th>EMAIL</th>
    </tr>
    </thead>
    <tbody>
    @foreach($teachers as $teacher)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $teacher->name }}</td>
            <td>{{ $teacher->surname }}</td>
            <td>{{ $teacher->middle_name }}</td>
            <td>{{ $teacher->msk_teacher_status->name }}</td>
            <td>
                @foreach($teacher->person_studies as  $study)
                    {{ $study->place }} <br>
                    <hr>
                @endforeach
            </td>
            <td>{{ $teacher->birthday == '' ? '-' : date('d-m-Y', strtotime($teacher->birthday)) }}</td>
            <td>{{ $teacher->age }}</td>
            <td>{{ $teacher->getGenger() }}</td>
            <td>{{ $teacher->birth_place }}</td>
            <td>{{ $teacher->currentCity['name'] }}</td>
            <td>{{ $teacher->currentRegion['name'] }}</td>
            <td>{{ $teacher->current_address }}</td>
            <td>{{ $teacher->livedCity['name'] }}</td>
            <td>{{ $teacher->livedRegion['name'] }}</td>
            <td>{{ $teacher->lived_address }}</td>
            <td>{{ $teacher->created_at == '' ? '-' : date('d-m-Y', strtotime($teacher->created_at)) }}</td>
            <td>{{ $teacher->getStatus() }}</td>
            <td>{{ $teacher->deleted_at == '' ? '-' : date('d-m-Y', strtotime($teacher->deleted_at)) }}</td>
            <td>{{ $teacher->home_tel }}</td>
            <td>{{ $teacher->mobil_tel }}</td>
            <td>{{ $teacher->email }}</td>
        </tr>
    @endforeach
    </tbody>
</table>