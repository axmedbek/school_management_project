<style>
    #tabel_table thead tr th,p{
        font-family: DejaVu Sans, sans-serif !important;
        color: #565858;
        font-weight: bold;
    }
    #tabel_table tbody tr td{
        font-family: DejaVu Sans, sans-serif !important;
    }
    #tabel_table thead tr th{
        border: 2px solid #959b9c;
    }
    #tabel_table tbody tr td{
        border: 2px solid #959b9c;
    }
</style>


<h2 style="font-family: 'DejaVu Sans';text-align: center;">{{ isset($tableTitle) ? $tableTitle : '' }}</h2>
<h3 style="font-family: 'DejaVu Sans';text-align: center;">{{ isset($tableDate) ? 'Tarix : '.$tableDate : '' }}</h3>
<table class="table" id="tabel_table">
    <thead>
    <tr>
        <th>No</th>
        <th>Ad Soyad Ata Adı</th>
        <th>Vəzifə</th>
        @foreach(array_keys($daysOfMonth) as $item)
            <th>{{ $item }}</th>
        @endforeach
        <th><p style="writing-mode: vertical-rl">İş saatı</p></th>
        <th><p style="writing-mode: vertical-rl">İş günləri</p></th>
        <th><p style="writing-mode: vertical-rl">Qeyd</p></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $key =>  $user)
        <tr>
            <td>{{ $key + 1  }}</td>
            <td>{{ $user->fullname() }}</td>
            <td>{{ $user->position }}</td>
            @foreach($user['dayOfMonth'] as $key => $item)
                <td {{ $item == 'Bz' || $item == 'Şn' ? 'style = background-color:bisque;' : ''}} {{ $item == 'x' ? 'style = background-color:darkseagreen;' : ''}} > {{ $item == 'Bz' || $item == 'Şn' ? '' : $item }}</td>
            @endforeach
            <td>{{ $user['workDays']*8 }}</td>
            <td>{{ $user['workDays'] }}</td>
            <td>-</td>
        </tr>
    @endforeach
    </tbody>
</table>