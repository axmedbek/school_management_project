<div class="col-lg-12" style="padding: 0px;">
    <div id="errors" class="row">

    </div>
    <div class="panel">
        <ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
            <li class="active"><a href="#table_tab" data-toggle="tab" role="tab">Cədvəl</a>
            </li>
            <li><a href="#students_tab" data-toggle="tab" role="tab">Şagİrdlər</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="table_tab">
                <div class="col-md-3 col-lg-3" style="float: right">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="javascript:openModal('{{ route('table_copy_modal') }}', {year_id: '{{ $classLetter->year_id }}', corpus_id: '{{ $classLetter->corpus_id }}', class_id: '{{ $classLetter->msk_class_id }}' })" type="button" class="btn btn-danger" style="padding-bottom: 10px;padding-top: 10px;">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-copy fs-15"></i>
                              </span>
                                <span class="fs-11 font-montserrat text-uppercase" style="font-weight: bold;"> Kopyala</span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="javascript:save()" type="button" class="btn btn-default" style="background-color: #179228;color: white;padding-bottom: 10px;padding-top: 10px;">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-save fs-15"></i>
                              </span>
                                <span class="fs-11 font-montserrat text-uppercase" style="font-weight: bold;"> Yadda saxla</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4" style="float: right;">
                    <div class="row">
                        <div class="col-md-6" style="float: right;margin-top: 5px;display: none;">
                            <input type="text" class="form-control datepicker" name="choose_date" style="width : 150px">
                        </div>
                        <div class="col-md-6" style="float: right;">
                            <div class="form-check" style="margin-top: 12px;">
                                <label class="form-check-label" style="font-size: 15px">Tətbiqetmə tarixi</label>
                                <input type="checkbox" class="form-check-input" name="choose_date_check" style="width: 17px;height: 17px;margin-left: 4px;">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table start  -->
                <div id="lesson_table">
                    @include('pages.tables.lesson_table')
                </div>
                <!-- table end  -->
            </div>
            <div class="tab-pane " id="students_tab">
                <div class="col-md-8 col-lg-5" style="float: right">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="javascript:save()" type="button" class="btn btn-default">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-save fs-15"></i>
                              </span>
                                <br>
                                <span class="fs-11 font-montserrat text-uppercase">Saxla</span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="javascript:$('#students').multiSelect('select_all');" type="button" class="btn btn-default">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-check fs-15"></i>
                              </span>
                                <br>
                                <span class="fs-11 font-montserrat text-uppercase">Hamısını seç</span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="javascript:$('#students').multiSelect('deselect_all');" type="button" class="btn btn-default">
                              <span class="p-t-5 p-b-5">
                                  <i class="fa fa-remove fs-15"></i>
                              </span>
                                <br>
                                <span class="fs-11 font-montserrat text-uppercase">Heçbirini seçmə</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <select multiple="multiple" id="students" name="students[]">
                        @foreach($group->students as $student)
                            <option value='{{ $student->id }}' selected>{{ $student->fullname() }}</option>
                        @endforeach
                        @foreach($freeStudents as $student)
                            <option value='{{ $student->id }}'>{{ $student->fullname() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .wcs-timetable__container{position:relative;min-height:12.5vh;clear:both;width:100%;padding:5vh 0;display:flex;flex-wrap:wrap;}
    .wcs-timetable__container::after{content:'';display:block;clear:both;}
    .wcs-timetable__container *{box-sizing:border-box;}
    .wcs-timetable__container .wcs-timetable{flex-basis:100%;}
    .wcs-timetable__container h3{font-size:160%;line-height:1.25;}
    .wcs-timetable__container .wcs-class__title:not(td){margin:0;padding:0;}
    .wcs-timetable{text-align:center;position:relative;width:100%;}
    .wcs-class__title{word-break:break-word;}
    .wcs-timetable__week{padding:0;margin:0;text-align:left;}
    @media (min-width: 992px){
        .wcs-timetable__week{margin:0 -0.2vh;display:flex;width:100%;align-content:stretch;align-items:stretch;justify-content:space-between;}
    }
    .wcs-timetable__week .wcs-day{margin:0;padding:0;min-height:15vh;}
    @media (min-width: 992px){
        .wcs-timetable__week .wcs-day{display:flex;align-items:stretch;align-content:stretch;flex-direction:column;width:100%;margin:0 0.2vh;}
    }
    .wcs-timetable__week .wcs-day__title{font-size:110%;position:relative;padding:1vh;margin:0 0 0.4vh 0!important;}
    @media (min-width: 992px){
        .wcs-timetable__week .wcs-day__title{flex-shrink:0;flex-grow:0;text-align:center;padding:1vh 0;}
    }
    .wcs-timetable__week .wcs-day__title::before{content:'';display:block;position:absolute;top:0;right:0;bottom:0;left:0;background-color:currentcolor;opacity:0.125;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes{padding:0;margin:0 0 0.4vh 0;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class{padding:10px 15px;position:relative;flex-shrink:0;font-size:90%;box-sizing:border-box;display:flex;flex-wrap:wrap;align-content:stretch;align-items:stretch;width:100%;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class .wcs-class__title{flex-grow:3;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class .wcs-class__time{flex-grow:1;text-align:right;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class div{flex-grow:4;align-self:flex-end;width:100%;}
    @media (min-width: 992px){
        .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class{flex-direction:column;}
        .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class .wcs-class__title{flex-grow:0;width:100%;display:block;}
        .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class .wcs-class__time{flex-grow:1;text-align:left;width:100%;}
        .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class div{flex-grow:0;align-self:flex-end;}
    }
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class > div{font-size:80%;position:relative;width:100%;opacity:0.6;transition:opacity 100ms ease-in;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class:hover > div{opacity:0.95;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class::before{content:'';display:block;position:absolute;top:0;right:0;bottom:0;left:0;opacity:0.06;background-color:currentcolor;transition:opacity, background-color, border 100ms ease-in;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class:hover::before{opacity:0.05;background-color:transparent;border:0.4vh solid;}
    .wcs-timetable__week .wcs-day .wcs-timetable__classes .wcs-class small{text-overflow:ellipsis;display:block;font-size:inherit;position:relative;cursor:pointer;}
    .wcs-timetable__week .wcs-class__time{font-size:inherit;}
    .wcs-timetable__week time{display:block;font-size:inherit;opacity:0.75;margin-bottom:1.5vh;}
    .wcs-timetable__week time span{font-size:inherit;white-space:normal;display:inline-block;}
    @media (min-width: 992px){
        .wcs-timetable--style-3:not(.wcs-timetable--grouped-by-hours) .wcs-class--slots-6{min-height:7.5vh;}
    }
    .wcs-class--slots-6{margin: 0.4vh 0 0; cursor: pointer;}
    .wcs-addons--blink{animation:blink 1s steps(5, start) infinite;-webkit-animation:blink 1s steps(5, start) infinite;display:inline-block;padding:0 1px;}
    .wcs-modal-call{cursor:pointer;}
    /*! CSS Used from: http://demo.curlythemes.com/timetable-wordpress-plugin/wp-content/themes/leisure/css/bootstrap.min.css */
    small{font-size:80%;}
    @media print{
        *{color:#000!important;text-shadow:none!important;background:transparent!important;-webkit-box-shadow:none!important;box-shadow:none!important;}
        h3{orphans:3;widows:3;}
        h3{page-break-after:avoid;}
    }
    *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
    :before,:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
    h3{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
    h3{margin-top:20px;margin-bottom:10px;}
    h3{font-size:24px;}
    small{font-size:85%;}
    /*! CSS Used from: http://demo.curlythemes.com/timetable-wordpress-plugin/wp-content/themes/leisure/style.css?ver=2.0 */
    ::-moz-selection{border-radius:2px;}
    ::selection{border-radius:2px;}
    ::-moz-selection{border-radius:2px;}
    h3{line-height:1.2;}
    h3{margin:2.8rem 0 1.4rem;}
    small{color:inherit;}
    h3{position:relative;}
    #site #content div > :first-child{margin-top:0;}
    #site #content div > :last-child{margin-bottom:0;}
    /*! CSS Used from: Embedded */
    h3{font-family:'Arimo';font-weight:normal;font-style:normal;text-transform:capitalize;font-size:2.4rem;}
    ::selection{background:rgba(213,0,126,0.9);}
    ::-moz-selection{background:rgba(213,0,126,0.9);}
    ::selection{color:rgba(255,255,255,1);}
    ::-moz-selection{color:rgba(255,255,255,1);}
    h3{color:rgba(54,61,64,1);}
    /*! CSS Used from: Embedded */
    .wcs-timetable--4.wcs-timetable__container{color:#282828;}
    .wcs-timetable--4 .wcs-timetable{border-color:#282828;}
    .wcs-timetable--4 .wcs-timetable__week .wcs-day__title{background-color:#0f4836;color:rgba( 255,255,255,1);}
    /*! CSS Used keyframes */
    @keyframes blink{to{visibility:hidden;}}
    @-webkit-keyframes blink{to{visibility:hidden;}}
    .wcs-class[teacher_id=''] .clear-lesson{ display: none; font-size: 15px; color: red;}
    .wcs-class .clear-lesson{ font-size: 15px; color: red;}
    .class-text-style{
        font-weight: 900;
        color: #2c5e4e;
        font-size: 15px !important;
    }
</style>
<style>
    #students_table td{
        vertical-align: middle;
    }
    .ms-container{
        width: 100% !important;
        padding-top: 10px;
    }
    .ms-list{
        height: 400px !important;
    }
    .custom-header{
        font-size: 20px;
        background-color: #949494;
        padding: 5px;
        color: white;
        text-align: center;
    }
</style>
<link href="{{ asset('assets/plugins/multi-select/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css" />

<script src="{{ asset('assets/plugins/multi-select/jquery.quicksearch.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/multi-select/jquery.multi-select.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/notifications.js') }}" type="text/javascript"></script>
<script>

    $('[name="choose_date"]').datepicker({
        format : 'dd-mm-yyyy',
        autoclose : true
    }).on('changeDate',function(){
        let selectedDate =$(this).data('datepicker')['dates'][0],
            currentDate = new Date();
        if (selectedDate.getTime() > currentDate.getTime()) {
            $(this).val("");
            $('body').pgNotification({
                style: 'flip',
                message: "Bu gündən irəli tarix seçə bilməzsiniz!",
                position: "top",
                timeout: 5000,
                type: "error"
            }).show();
        }
    });

    $('[name="choose_date_check"]').on('change',function(){
       if ($(this).is(':checked')){
           $('[name="choose_date"]').parents('div:eq(0)').show();
       }
       else{
           $('[name="choose_date"]').parents('div:eq(0)').hide();
           $('[name="choose_date"]').val('');
       }
    });

    $('#students').multiSelect({
        selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Axtar'>",
        selectableFooter: "<div class='custom-header'>Bağlı olmayan şagirdlər</div>",
        selectionFooter: "<div class='custom-header'>Qrupun şagirdləri</div>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    $("#lesson_table").on('click', '.wcs-class[week_day]', function () {
        let d = $(this),
            subjects = {},
            group_id = {{ $group->id }},
            week_day = $(this).attr("week_day"),
            class_time_id = $(this).attr("class_time_id"),
            teacher_id = $(this).attr("teacher_id"),
            subject_id = $(this).attr("subject_id"),
            room_id = $(this).attr("room_id");

        $(".wcs-class[week_day]").each(function () {
            let s_id = $(this).attr("subject_id"); console.log(week_day,$(this).attr("week_day"),class_time_id,$(this).attr("class_time_id"));
            if(s_id > 0 && (week_day==$(this).attr("week_day") && class_time_id==$(this).attr("class_time_id"))==false ){
                if(subjects[s_id] == undefined) subjects[s_id] = 0;
                subjects[s_id]++;
            }
        });

       openModal('{{ route("table_class_edit") }}', {group_id: group_id, week_day: week_day, class_time_id: class_time_id, teacher_id: teacher_id, subject_id: subject_id, room_id: room_id, subjects: subjects}, 'slide-right');
    });

    $("#lesson_table").on('click', '.clear-lesson', function (event) {
        event.stopPropagation();

        let div = $(this).parents('.wcs-class:eq(0)');

        lessonAddEdit(div.attr('week_day'), div.attr('class_time_id'), '-', '-', '-', '', '', '');
    });
    
    function lessonAddEdit(week_day, class_time_id, subject, room, teacher, teacher_id, room_id, subject_id) {
        let div = $("#table_tab .wcs-class[week_day='"+week_day+"'][class_time_id='"+class_time_id+"']");
        div.find('.wcs-class__title').text(subject);
        div.find('.wcs-class__location span').text(room);
        div.find('.wcs-class__instructor span').text(teacher);

        div.attr("teacher_id", teacher_id).attr("room_id", room_id).attr("subject_id", subject_id);
    }

    function save() {
        let data = new FormData(),
            ii = 0,
            choosedDate = $('[name="choose_date"]').val(),
            error = false;
        data.append('_token',_token);

        $("#table_tab .wcs-class[week_day][class_time_id]").css('border', '');

        for(let n in $("#students").val()) {
            data.append('students[]', $("#students").val()[n]);
        }

        ii = 0;
        $("#table_tab .wcs-class[week_day]").each(function () {
            let div = $(this),
                week_day = div.attr("week_day"),
                class_time_id = div.attr("class_time_id"),
                teacher_id = div.attr("teacher_id"),
                subject_id = div.attr("subject_id"),
                room_id = div.attr("room_id");

            data.append('lessons[' + ii + '][week_day]', week_day);
            data.append('lessons[' + ii + '][class_time_id]', class_time_id);
            data.append('lessons[' + ii + '][teacher_id]', teacher_id);
            data.append('lessons[' + ii + '][subject_id]', subject_id);
            data.append('lessons[' + ii + '][room_id]', room_id);
            ii++;
        });

        if(error) return false;

        if (choosedDate.length > 0){
            data.append('startDate',choosedDate);
            $.confirm({
                title: 'Təsdiq',
                content: '\n' +
                    'Hörmətli istifadəçi ! \n' +
                    'Nəzərə alın ki, dərs cədvəli cari tarixdən önə çəkildikdə , sonuncu daxil edilmiş dərs günləri tarixləri üst üstə düşməzsə , müəllimlərin daxil etdiyi mövzu və qiymətlər itirilmiş olacaq. ',
                type: 'red',
                typeAnimated: true,
                buttons: {

                    formSubmit: {
                        text: 'Bəli',
                        btnClass: 'btn-green',
                        action: function () {
                            pageLoading('show');
                            $.ajax({
                                url: "{{ route('table_save_data', ['group'=> $group_id ]) }}",
                                type: "POST",
                                data: data,
                                async: false,
                                success: function (response) {
                                    if(response['status'] == 'error'){
                                        $("#errors").html(response['errors']);

                                        if(response['er_lessons'] != undefined){
                                            for(let n in response['er_lessons']){
                                                $("#table_tab .wcs-class[week_day='"+response['er_lessons'][n]['weekDay']+"'][class_time_id='"+response['er_lessons'][n]['classTimeId']+"']").css('border', '1px dotted red');
                                            }
                                        }
                                    }
                                    else {
                                        $("#errors").html("");

                                        $('body').pgNotification({
                                            style: 'flip',
                                            message: "Yadda saxlandı.",
                                            position: "top",
                                            timeout: 5000,
                                            type: "success"
                                        }).show();
                                    }

                                    pageLoading('hide');
                                },
                                cache: false,
                                contentType: false,
                                processData: false
                            });
                        }
                    },
                    formCancel: {
                        text: 'Xeyr',
                        btnClass: 'btn-red',
                        action: function () {
                            checkStatus = false;
                        }
                    }
                }
            });
        }
        else{
            pageLoading('show');
            $.ajax({
                url: "{{ route('table_save_data', ['group'=> $group_id ]) }}",
                type: "POST",
                data: data,
                async: false,
                success: function (response) {
                    if(response['status'] == 'error'){
                        $("#errors").html(response['errors']);

                        if(response['er_lessons'] != undefined){
                            for(let n in response['er_lessons']){
                                $("#table_tab .wcs-class[week_day='"+response['er_lessons'][n]['weekDay']+"'][class_time_id='"+response['er_lessons'][n]['classTimeId']+"']").css('border', '1px dotted red');
                            }
                        }
                    }
                    else {
                        $("#errors").html("");

                        $('body').pgNotification({
                            style: 'flip',
                            message: "Yadda saxlandı.",
                            position: "top",
                            timeout: 5000,
                            type: "success"
                        }).show();
                    }

                    pageLoading('hide');
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        {{----}}
       {{--if (checkStatus){--}}
           {{--$.ajax({--}}
               {{--url: "{{ route('table_save_data', ['group'=> $group_id ]) }}",--}}
               {{--type: "POST",--}}
               {{--data: data,--}}
               {{--async: false,--}}
               {{--success: function (response) {--}}
                   {{--if(response['status'] == 'error'){--}}
                       {{--$("#errors").html(response['errors']);--}}

                       {{--if(response['er_lessons'] != undefined){--}}
                           {{--for(let n in response['er_lessons']){--}}
                               {{--$("#table_tab .wcs-class[week_day='"+response['er_lessons'][n]['weekDay']+"'][class_time_id='"+response['er_lessons'][n]['classTimeId']+"']").css('border', '1px dotted red');--}}
                           {{--}--}}
                       {{--}--}}
                   {{--}--}}
                   {{--else {--}}
                       {{--$("#errors").html("");--}}

                       {{--$('body').pgNotification({--}}
                           {{--style: 'flip',--}}
                           {{--message: "Yadda saxlandı.",--}}
                           {{--position: "top",--}}
                           {{--timeout: 5000,--}}
                           {{--type: "success"--}}
                       {{--}).show();--}}
                   {{--}--}}

                   {{--pageLoading('hide');--}}
               {{--},--}}
               {{--cache: false,--}}
               {{--contentType: false,--}}
               {{--processData: false--}}
           {{--});--}}
       {{--}--}}
    }

</script>