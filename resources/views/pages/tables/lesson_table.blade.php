<div class="wcs-timetable__container wcs-timetable--4 wcs-timetable--style-3">
    <div class="wcs-timetable wcs-timetable--week">
        <div class="wcs-timetable__week wcs-timetable__parent">
            @foreach(\App\Library\Standarts::$weekDayNames as $key => $weekDay)
                <div class="wcs-day wcs-day--1">
                    <h3 class="wcs-day__title" style="font-size: 17px;font-family: 'Segoe UI';font-weight: bold;">{{ $weekDay }}</h3>
                    <div class="wcs-timetable__classes" style="font-size: 20px;">
                        @foreach($group->class_letter->corpus->class_times as $keyy => $classTime)
                            @php $chk = isset($lessonData[$key + 1]) && isset($lessonData[$key + 1][$classTime->id]) ? $lessonData[$key + 1][$classTime->id] : false; @endphp
                            <div class="wcs-class wcs-class--slots-6" week_day="{{ $key + 1 }}" class_time_id="{{ $classTime->id }}" teacher_id="{{ $chk ? $chk['user_id'] : '' }}" room_id="{{ $chk ? $chk['room_id'] : '' }}" subject_id="{{ $chk ? $chk['subject_id'] : '' }}">
                                <div class="class-text-style">{{ $keyy + 1 }}. dərs</div>
                                {{--<div><i class="fa fa-trash clear-lesson"></i> </div>--}}
                                <small class="wcs-class__title wcs-modal-call">
                                    @if($chk)
                                        {{ $chk->subject['name'] }}
                                        <i class="fa fa-trash clear-lesson"></i>
                                    @else
                                        -
                                    @endif
                                    {{--{{ $chk ? $chk->subject['name'] : "-" }}  --}}
                                </small>
                                <time class="wcs-class__time">
                                    @php
                                        $strartTime = explode(":", date("H:i", strtotime($classTime->start_time)));
                                        $endTime = explode(":", date("H:i", strtotime($classTime->end_time)));
                                    @endphp
                                    {{ $strartTime[0] }}<span class="wcs-addons--blink">:</span>{{ $strartTime[1] }} - {{ $endTime[0] }}<span class="wcs-addons--blink">:</span>{{ $endTime[1] }}
                                </time>
                                <div class="wcs-class__location">
                                    Otaq: <span> {{ $chk ? $chk->room['name'] : '-' }} </span>
                                </div>
                                <div class="wcs-class__instructor">Tədrisçi: <span> {{ $chk ? $chk->user['name']." ".$chk->user['surname'] : '-' }} </span></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>