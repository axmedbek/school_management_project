<style>
    a:focus{outline:thin dotted;}
    a:active,a:hover{outline:0;}
    table{border-collapse:collapse;border-spacing:0;}
    *,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
    a{color:#428bca;text-decoration:none;}
    a:hover,a:focus{color:#2a6496;text-decoration:underline;}
    a:focus{outline:thin dotted #333;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
    @media print{
        *{text-shadow:none!important;color:#000!important;background:transparent!important;box-shadow:none!important;}
        a,a:visited{text-decoration:underline;}
        thead{display:table-header-group;}
        tr{page-break-inside:avoid;}
        .table td,.table th{background-color:#fff!important;}
        .table{border-collapse:collapse!important;}
    }
    table{max-width:100%;background-color:transparent;}
    th{text-align:left;}
    .table{width:100%;margin-bottom:20px;}
    .table thead > tr > th,.table tbody > tr > td{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #dddddd;}
    .table thead > tr > th{vertical-align:bottom;border-bottom:2px solid #dddddd;}
    .table thead:first-child tr:first-child th{border-top:0;}
    .table-striped > tbody > tr:nth-child(odd) > td{background-color:#f9f9f9;}
    a{color:#575757;text-decoration:none;}
    a:hover,a:focus{color:#7d7d7d;text-decoration:none;}
    .panel .table td,.panel .table th{padding:6px 5px;border-top:1px solid #eaedef;border-right: 1px solid #e0e4e8;}
    .panel .table thead > tr > th{border-bottom:1px solid #e0e4e8;}
    .panel .table-striped > tbody > tr:nth-child(odd) > td{background-color:#fcfdfe;}
    .panel .table-striped > thead th{background:#fafbfc;border-right:1px solid #e0e4e8;}
    .panel .table-striped > thead th:last-child{border-right:none;}
    .text-sm{font-size:11px;}
    .m-b-none{margin-bottom:0;}

    /*! CSS Used from: file:///C:/Users/tabdullayev/Desktop/pages-213/demo/html/assets/plugins/boostrapv3/css/bootstrap.min.css */
    input{margin:0;font:inherit;color:inherit;}
    input::-moz-focus-inner{padding:0;border:0;}
    input{line-height:normal;}
    input[type=checkbox]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0;}
    @media print{
        *,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important;}
    }
    *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
    :after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
    input{font-family:inherit;font-size:inherit;line-height:inherit;}
    label{display:inline-block;max-width:100%;margin-bottom:5px;font-weight:700;}
    input[type=checkbox]{margin:4px 0 0;margin-top:1px \9;line-height:normal;}
    input[type=checkbox]:focus{outline:thin dotted;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
    .checkbox{position:relative;display:block;margin-top:10px;margin-bottom:10px;}
    .checkbox label{min-height:20px;padding-left:20px;margin-bottom:0;font-weight:400;cursor:pointer;}
    .checkbox input[type=checkbox]{position:absolute;margin-top:4px \9;margin-left:-20px;}
    .checkbox+.checkbox{margin-top:-5px;}
    /*! CSS Used from: file:///C:/Users/tabdullayev/Desktop/pages-213/demo/html/pages/css/pages.css */
    label,input{font-size:14px;font-weight:normal;line-height:20px;}
    input[type="checkbox"]{margin-top:1px 0 0;line-height:normal;cursor:pointer;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
    input:focus,input[type="checkbox"]:focus{outline:none;-webkit-box-shadow:none;box-shadow:none;}
    .checkbox{margin-bottom:10px;margin-top:10px;padding-left:0px;}
    .checkbox label{display:inline-block;cursor:pointer;position:relative;padding-left:25px!important;font-size:13px;}
    .checkbox label:before{content:"";display:inline-block;width:17px;height:17px;margin-right:10px;position:absolute;left:0px;background-color:#ffffff;border:1px solid #d0d0d0;}
    .checkbox label{transition:border 0.2s linear 0s, color 0.2s linear 0s;white-space:nowrap;}
    .checkbox label:before{top:1.4px;border-radius:3px;transition:border 0.2s linear 0s, color 0.2s linear 0s;}
    .checkbox label::after{display:inline-block;width:16px;height:16px;position:absolute;left:3%;top:2%;font-size:11px;transition:border 0.2s linear 0s, color 0.2s linear 0s;}
    .checkbox label:after{border-radius:3px;}
    .checkbox input[type=checkbox]{opacity:0;width:0;height:0;}
    .checkbox input[type=checkbox]:checked + label:before{border-width:8.5px;}
    .checkbox input[type=checkbox]:checked + label::after{font-family:'FontAwesome';content:"\F00C";color:#fff;}
    .checkbox input[type="checkbox"]:focus + label{color:#2c2c2c;}
    .checkbox input[type="checkbox"]:focus + label:before{background-color:#e6e6e6;}
    .checkbox.check-success input[type=checkbox]:checked + label:before{border-color:#10cfbd;}
    .checkbox.check-success input[type=checkbox]:checked + label::after{color:#ffffff;}
    input,input:focus{-webkit-transition:none!important;}
    .checkbox{ margin: 0px;}

    .fmc{
        cursor: pointer;
        font-size: 17px;
    }
    .fmc.fa-book{
        margin-right: 20%;
    }
    .fmc.fa-home{
        margin-left: 20%;
    }
    .table-striped thead th{
        text-align: center;
        vertical-align: middle !important;
    }
    .table-striped tbody td[user_id]{
        cursor: pointer;
        vertical-align: middle;
        text-align: center;
        font-size: 15px;
    }
    .no-wrap{
        white-space: nowrap;
    }
    .table thead tr th{
        color: #2c2c2c;
    }
    #date-sel{
        display: block;
    }
</style>

<div class="col-12 panel" style="padding: 0;overflow-y: auto">
    <table class="table table-striped m-b-none" id="all-students">
        <thead>
        <tr>
            <th style="width: 1%;position: sticky;left: 0;" rowspan="3">S/S</th>
            <th rowspan="3" style="position: sticky;left: 45px;">Ad Soyad Ata adı</th>
            <th style="text-align: center" colspan="{{ count($classDays) }}"> {{ \App\Library\Standarts::$months[(int)$month-1] }} / {{ $year }} </th>
        </tr>
        <tr>
            @foreach($classDays as $classDay)
                <?php
                $classDay->date = \Carbon\Carbon::parse($classDay->date);
                ?>
                @if($classDay->type === 'day')
                    <th class="no-wrap">{{ $classDay->date->format("Y-m-d") }} <br/> ({{ date("H:i", strtotime($classDay->start_time)) . '-' . date("H:i", strtotime($classDay->end_time)) }})</th>
                @else
                    <th class="no-wrap">{{ \App\Library\Standarts::$dayTypes[$classDay->type] }}</th>
                @endif
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            <tr user_id="{{ $student->user_id }}">
                <td style="position: sticky;left: 0;">{{ $loop->iteration }}</td>
                <td class="no-wrap" style="position: sticky;left: 45px;">{{ $student->f_name }} </td>
                @foreach($classDays as $classDay)
                    @php
                        $markDayType = $classDay->type == 'day' ? $classDay->ct_id : $classDay->type;
                        $userMark = isset($userMarksGroup[$student->user_id])
                                    && isset($userMarksGroup[$student->user_id][(int)$classDay->date->format("d")])
                                    && isset($userMarksGroup[$student->user_id][(int)$classDay->date->format("d")][$markDayType]) ? $userMarksGroup[$student->user_id][(int)$classDay->date->format("d")][$markDayType] : false;

                        $dayMark = $userMark ? ( in_array($classDay->type, ['yi', 'y']) ? $userMark->value : ( $userMark->qb==1 ? 'q' : $userMark->mskMark['name'] ) ) : '';
                    @endphp
                    <td user_id="{{ $student->user_id }}" day="{{ (int)$classDay->date->format("d") }}" hour="{{ $classDay->ct_id === null ? $classDay->type : $classDay->ct_id  }}"> {{ $dayMark }} </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="col-12" style="margin-top: 20px;">
    <div class="os-tabs-w">
        <div class="os-tabs-controls">
            <ul class="nav nav-tabs smaller">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_classes">Dərslər</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_homework">Ev tapşırığı</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_classes">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Tarix</th>
                        <th>Mövzu</th>
                        <th>Elektron vəsait</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lectures as $lecture)
                        <tr>
                            <td class="text-center">{{ date('d-m-Y', strtotime($lecture->date)) }}</td>
                            <td class="text-center">{{ $lecture->seasonParagraph['name'] }}</td>
                            <td class="text-center">
                                @foreach($lecture->files as $file)
                                    <a class="tooltips" href="{{ asset(\App\Library\Standarts::$portalLectureFilesDir . $file->file_name) }}" download data-original-title="{{ $file->file_name }}"><i class="fa fa-file"></i> </a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_homework">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Tarix</th>
                        <th>Tapşırıq</th>
                        <th>Elektron vəsait</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($homeworks as $homework)
                        <tr>
                            <td class="text-center">{{ date('d-m-Y', strtotime($homework->date)) }}</td>
                            <td class="text-center">{{ $homework->task }}</td>
                            <td class="text-center">
                                @foreach($homework->files as $file)
                                    <a class="tooltips" href="{{ asset(\App\Library\Standarts::$portalHomeworkFilesDir . $file->file_name) }}" download data-original-title="{{ $file->file_name }}"><i class="fa fa-file"></i> </a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $('#date').unbind("changeDate");
    //$('#date').datepicker("setStartDate", new Date("{{ $yearStartDate->format("Y-m-d") }}"));
    //$('#date').datepicker("setEndDate", new Date("{{ $yearEndDate->format("Y-m-d") }}"));
    $('#date').datepicker("setDate", new Date("{{ $selectedMonth->format("Y-m-d") }}"));
    $('#date').on('changeDate', function(e) {
        getPage();
    });
</script>