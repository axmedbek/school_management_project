<div class="col-lg-12" style="padding: 0px;">
    <div id="errors" class="row">

    </div>
    <div class="panel panel-transparent">
        <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">

            <h4 style="padding-left:15px;margin:5px;border-bottom:2px solid #d9d9d9;">
                Hərflər
                @if(\App\Models\Year::realData()->find($year_id)->active == 1)
                 <i class="pg-plus_circle" onclick="openModal('{{ route('class_group_letter_add_edit',['letter'=> 0]) }}',{class_id: {{ $class_id }}, year_id: {{ $year_id }}, corpus_id: {{ $corpus_id }} } )" style="cursor: pointer;color: #10cfbd;"> </i>
                @endif
            </h4>
            @foreach($letters as $letter)
                <li letter_id="{{ $letter->id }}">

                    <a data-toggle="tab" href="#tab{{ $letter->id }}">{{ $letter->name }}
                        @if($letter->year->active == 1)
                        <i style="font-size: 15px;cursor: pointer;" class="fa fa-edit" onclick="openModal('{{ route('class_group_letter_add_edit',['letter'=> $letter->id]) }}',{class_id: {{ $class_id }}, year_id: {{ $year_id }}, corpus_id: {{ $corpus_id }} } )"></i>
                        <i style="font-size: 15px;cursor: pointer;color: red;" class="fa fa-trash letter_delete"></i>
                        @endif
                    </a>


                </li>
            @endforeach
        </ul>
        <div class="tab-content bg-white">
            @foreach($letters as $letter)
                <div class="tab-pane" id="tab{{ $letter->id }}">
                    @if($letter->year->active == 1)
                        <button type="button" class="btn btn-success pull-right add_new"><i class="pg-plus_circle"> </i> Yeni</button>
                    @endif
                    <table class="table table-hover" id="basicTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Group Adi</th>
                                <th>Tipi</th>
                                <th style="width: 175px;text-align: center;">Əməliyyat</th>
                                <th style="width: 75px;">Əsas Qrup</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($letter->groups as $group)
                                <tr tr_id="{{ $group->id }}" group_type="{{ $group->group_type_id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $group['name'] }}</td>
                                    <td>{{ $group->group_type['name'] }}</td>
                                    <td>
                                        @if(\App\Library\Helper::has_priv('class_groups',\App\Library\Standarts::PRIV_CAN_EDIT))
                                            @if($letter->year->active == 1)
                                            <div class="btn-group-sm">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <a type="button" class="btn btn-primary edit_data"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                    @php $letterGroups = \App\Models\Lesson::realData()->where('letter_group_id',$group->id)->first(); @endphp
                                                    @if(!isset($letterGroups))
                                                    <div class="col-md-4">
                                                            <a type="button" class="btn btn-danger data_delete"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                        @else
                                            <span class="badge badge-danger">Əməliyyat apara bilməzsiniz</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="col-md-4">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="isMainGroup" value="{{ $group['isMainGroup'] }}" {{ $group['isMainGroup'] == 1 ? 'checked' : ''}}  style="display: none;" onclick="controlIsMainGroup(this)">
                                                <span class="custom-control-indicator"></span>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script type="text/html" id="row">
    <td></td>
    <td><input type="text" maxlength="60" required name="name" class="form-control"></td>
    <td>
        <select class="form-control" name="group_type">
            @foreach(\App\Models\GroupType::realData()->get() as $groupType)
                <option value="{{ $groupType->id }}" {{ $groupType->default==1?'selected':'' }}> {{ $groupType->name }} </option>
            @endforeach
        </select>
    </td>
    <td>
        <div class="btn-group-sm">
            <div class="row">
                <div class="col-md-4">
                    <a type="button" class="btn btn-success save" data-toggle="save" title="Yadda saxla"><i class="fa fa-save"></i></a>
                    </div>
                <div class="col-md-4">
                    <a type="button" class="btn btn-warning cancel"><i class="fa fa-remove"></i></a>
                    </div>
                {{--<div class="col-md-4">--}}
                    {{--<label class="custom-control custom-checkbox">--}}
                        {{--<input type="checkbox" class="custom-control-input" name="isMainGroup" value="0"  style="display: none;" onclick="controlIsMainGroup(this)">--}}
                        {{--<span class="custom-control-indicator"></span>--}}
                    {{--</label>--}}
                {{--</div>--}}
            </div>
        </div>
    </td>
</script>

<script>

    function controlIsMainGroup(element){
        if($(element).val() == '0'){
            $(element).val('1');
            $(element).attr('checked','checked');
        }
        else{
            $(element).val('0');
            //$(element).removeAttr('checked');
        }
        var tr = $(element).parents('tr'),
            table = $(element).parents('table'),
            tr_id = tr.attr('tr_id'),
            isMainGroup = $(element).val(),
            letter_id = table.parent('div').parent('div').prev('ul').find('li.active').attr('letter_id');

        table.find('tbody>tr').not('[tr_id="'+tr.attr('tr_id')+'"]').each(function() {
            $(this).find('input[name="isMainGroup"]').removeAttr('checked');
        });

        $.post( "{{ route('class_group_letter_is_main_group') }}", { letter_id: letter_id,id:tr_id,isMainGroup:isMainGroup, _token: _token }, function( response ) {
            if(response['status']=='error') $("#errors").html(response['errors']);
            else {
            }
        });

    }

    $(".letter_delete").click(function () {
        let tr = $(this).parents("li:eq(0)"),
            id = tr.attr("letter_id");

        $.confirm({
            title: 'Təsdiq',
            content: 'Silmək istədiyinizə əminsinizmi?',
            buttons: {
                formSubmit : {
                    text :'Sil',
                    btnClass : 'btn-red',
                    action : function () {
                        $.post( "{{ route('class_group_letter_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                $("select#class").trigger("change");
                            }
                        });
                    }
                },
                formCancel : {
                    text:"İmtina",
                    btnClass : 'btn-green',
                    action : function () {

                    }
                },
            }
        });
    });
    //
    $(".add_new").click(function () {
        let tbody = $(this).parents(".tab-pane:eq(0)").find("tbody");

        tbody.parents(".tab-pane:eq(0)").find("tbody").append('<tr tr_id="0">' + $("#row").html() + '</tr>');
        tbody.find("tr:last").hide().show('fast');

        tbody.find("tr:last .cancel").click(function () {
            $(this).parents("tr:eq(0)").remove();
            order(tbody.parents("table:eq(0)"));
        });
        order(tbody.parents("table:eq(0)"));
    });

    $("tbody").on("click", ".save", function () {
        let tr = $(this).parents("tr:eq(0)"),
            data = {},
            letter_id = $("#tab-3 li.active").attr("letter_id"),
            id = tr.attr("tr_id");

        if(isValid(tr)){
            tr.find("[name]").each(function () {
                data[$(this).attr("name")] = $(this).val();
            });
            data['id'] = id;
            data['letter_id'] = letter_id;
            data['_token'] = _token;

            $.post( "{{ route('class_group_letter_group_add_edit_action') }}", data, function( response ) {
                if(response['status']=='error') $("#errors").html(response['errors']);
                else saveTr(tr, response['data']);
            });
        }
    });

    $("tbody").on("click", ".edit_data", function () {
        let tr = $(this).parents("tr:eq(0)"),
            name = tr.find("td:eq(1)").text().trim(),
            group_type = tr.attr("group_type"),
            html = tr.html();

        tr.html($("#row").html());

        //vals
        tr.find("[name='name']").val(name);
        tr.find("[name='group_type']").val(group_type);

        tr.find(".cancel").click(function () {
            tr.html(html);
            order(tr.parents("table:eq(0)"));
        });
        order(tr.parents("table:eq(0)"));
    });

    $("tbody").on("click", ".data_delete", function () {
        let tr = $(this).parents("tr:eq(0)"),
            id = tr.attr("tr_id");

        $.confirm({
            title: 'Təsdiq',
            content: 'Silmək istədiyinizə əminsinizmi?',
            buttons: {
                formSubmit : {
                    text :'Sil',
                    btnClass : 'btn-red',
                    action : function () {
                        $.post( "{{ route('group_letter_delete') }}", { id: id, _token: _token }, function( response ) {
                            if(response['status']=='error') $("#errors").html(response['errors']);
                            else {
                                tr.remove();
                                order(tr.parents("table:eq(0)"));
                            }
                        });
                    }
                },
                formCancel : {
                    text: "İmtina",
                    btnClass : 'btn-green',
                    action : function () {

                    }
                },
            }
        });
    });

    function isValid(tr) {
        let name = tr.find("[name='name']"),
            status = true;

        name.css("border", "");

        if(name.val().trim().length > 30 || name.val().trim().length == 0){
            name.css("border", "1px dotted red");
            status = false;
        }

        return status;
    }

    function saveTr(tr, data) {
        tr.html('<td> </td>' +
            '<td>'+data['name']+'</td>' +
            '<td>'+data['group_type']+'</td>' +
            '<td colspan="2">' +
                '<div class="btn-group-sm">' +
                    '<div class="row">'+
                        '<div class="col-md-4">'+
                            '<a type="button" class="btn btn-primary edit_data"><i class="fa fa-pencil"></i></a>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<a type="button" class="btn btn-danger data_delete"><i class="fa fa-trash-o"></i></a>'+
                        '</div>'+
                       '<div class="col-md-4">'+
                            '<label class="custom-control custom-checkbox">'+
                                '<input type="checkbox" class="custom-control-input" name="isMainGroup" value="'+data['isMainGroup']+'"  style="display: none;" onclick="controlIsMainGroup(this)">'+
                                '<span class="custom-control-indicator"></span>'+
                            '</label>'+
                        '</div>'+
                    '</div>'+
                '</div>' +
            '</td>'
        );
        if(data['isMainGroup'] == 1){
            tr.find('input[name="isMainGroup"]').attr('checked','checked');
        }
        tr.attr("tr_id", data['id']);
        tr.attr("group_type", data['group_type_id']);
        order(tr.parents("table:eq(0)"));
    }

    function order(table) {
        let i = 0;
        table.find("tbody tr").each(function () {
            i++;
            $(this).find("td:eq(0)").text(i);
        });
    }

    $(function () {
        $("#tab-3 li:eq(0) a").click();
    });
</script>