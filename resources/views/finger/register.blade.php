@php
    $regFingers = [];
    if($user->card){
        foreach ($user->card->fingers as $finger){
            $regFingers[$finger->fing_id + 1] = $finger;
        }
    }
@endphp
<div class="row card" style="width: 100%">
    <div class="col-sm-12 padding-20 col-top">
        <p class="font-montserrat bold">Barmaq izi qeydiyyatı</p>
        <div class="fingers_map">
            <img src="{{ asset('images/biometric_icons/el.png') }}" style="width: 100%;" usemap="#fingers_map41">
            <img src="{{ asset('images/biometric_icons/sol_1.png') }}" fing="fing_1" class="fingers_map_img {{ isset($regFingers[1]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sol_2.png') }}" fing="fing_2" class="fingers_map_img {{ isset($regFingers[2]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sol_3.png') }}" fing="fing_3" class="fingers_map_img {{ isset($regFingers[3]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sol_4.png') }}" fing="fing_4" class="fingers_map_img {{ isset($regFingers[4]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sol_5.png') }}" fing="fing_5" class="fingers_map_img {{ isset($regFingers[5]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sag_1.png') }}" fing="fing_10" class="fingers_map_img {{ isset($regFingers[10]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sag_2.png') }}" fing="fing_9" class="fingers_map_img {{ isset($regFingers[9]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sag_3.png') }}" fing="fing_8" class="fingers_map_img {{ isset($regFingers[8]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sag_4.png') }}" fing="fing_7" class="fingers_map_img {{ isset($regFingers[7]) ? 'active':'' }}">
            <img src="{{ asset('images/biometric_icons/sag_5.png') }}" fing="fing_6" class="fingers_map_img {{ isset($regFingers[6]) ? 'active':'' }}">
            <div used="{{ isset($regFingers[1]) ? '1':'0' }}" idsi="{{ isset($regFingers[1]) ? $regFingers[1]->id:'' }}" fing="fing_1" class="fingers_map_area" style="top: 25%; left: 1%; width: 6%; height: 16%;"></div>
            <div used="{{ isset($regFingers[2]) ? '1':'0' }}" idsi="{{ isset($regFingers[2]) ? $regFingers[2]->id:'' }}" fing="fing_2" class="fingers_map_area" style="top: 10%; left: 7%; width: 6%; height: 20%;"></div>
            <div used="{{ isset($regFingers[3]) ? '1':'0' }}" idsi="{{ isset($regFingers[3]) ? $regFingers[3]->id:'' }}" fing="fing_3" class="fingers_map_area" style="top: 2%; width: 8%; height: 22%; left: 13.7%;"></div>
            <div used="{{ isset($regFingers[4]) ? '1':'0' }}" idsi="{{ isset($regFingers[4]) ? $regFingers[4]->id:'' }}" fing="fing_4" class="fingers_map_area" style="top: 5%; left: 25%; width: 7%; height: 24%;"></div>
            <div used="{{ isset($regFingers[5]) ? '1':'0' }}" idsi="{{ isset($regFingers[5]) ? $regFingers[5]->id:'' }}" fing="fing_5" class="fingers_map_area" style="top: 37%; left: 38%; width: 10%; height: 17%;"></div>
            <div used="{{ isset($regFingers[6]) ? '1':'0' }}" idsi="{{ isset($regFingers[6]) ? $regFingers[6]->id:'' }}" fing="fing_6" class="fingers_map_area" style="top: 37%; width: 10%; height: 16%; left: 51.5%;"></div>
            <div used="{{ isset($regFingers[7]) ? '1':'0' }}" idsi="{{ isset($regFingers[7]) ? $regFingers[7]->id:'' }}" fing="fing_7" class="fingers_map_area" style="top: 5%; left: 68%; width: 7%; height: 23%;"></div>
            <div used="{{ isset($regFingers[8]) ? '1':'0' }}" idsi="{{ isset($regFingers[8]) ? $regFingers[8]->id:'' }}" fing="fing_8" class="fingers_map_area" style="top: 2%; width: 7%; height: 22%; left: 79%;"></div>
            <div used="{{ isset($regFingers[9]) ? '1':'0' }}" idsi="{{ isset($regFingers[9]) ? $regFingers[9]->id:'' }}" fing="fing_9" class="fingers_map_area" style="top: 11%; width: 7%; height: 18%; left: 86.5%;"></div>
            <div used="{{ isset($regFingers[10]) ? '1':'0' }}" idsi="{{ isset($regFingers[10]) ? $regFingers[10]->id:'' }}" fing="fing_10" class="fingers_map_area" style="top: 25%; left: 93%; width: 6%; height: 16%;"></div>
        </div>
    </div>

    <div class="col-sm-12 padding-20 col-top">
        <p class="font-montserrat bold">İD Kart</p>
        <div class="input-group">
            <input type="text" class="form-control card-number" value="{{ $user->card ? $user->card->card_number : '' }}">
            <span class="input-group-addon success save-card" style="cursor: pointer">
                 <i class="fa fa-save"></i> Yadda saxla
            </span>
        </div>
    </div>
</div>
