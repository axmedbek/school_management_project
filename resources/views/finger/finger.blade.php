@extends('base')

@section('container')
    <style>
        @media only screen and (max-width: 1365px) {
            #kart-icon {
                position: unset;
                margin-top: 10px;
                margin-left: 20px;
            }
            #finger-icon{
                position: unset;
                margin-top: 10px;
            }
        }
    </style>
    <div class="row" id="errors">
        @include('errors')
    </div>
    <div class="row">
        <div class="col-md-3" id="user_view">



        </div>
        <div class="col-md-5 col-sm-6">
            <div class="card" style="width: 100%;">
                <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    {{--<li class="active" type="user"><a href="#tabUsers" data-toggle="tab" role="tab" aria-expanded="true">İstifadəçilər</a>--}}
                    {{--</li>--}}
                    <li class="active" type="student"><a href="#tabStudents" data-toggle="tab" role="tab" aria-expanded="false">Şagirdlər</a>
                    </li>
                    <li class="" type="parent"><a href="#tabParents" data-toggle="tab" role="tab" aria-expanded="false">Valideynlər</a>
                    </li>
                    <li class="" type="staff"><a href="#tabPersonel" data-toggle="tab" role="tab" aria-expanded="false">Personal</a>
                    </li>
                    <li class="" type="teacher"><a href="#tabTeachers" data-toggle="tab" role="tab" aria-expanded="false">Müəllimlər</a>
                    </li>
                </ul>
                <div class="tab-content hidden-xs" style="height: 500px;overflow-y: auto;">
                    {{--<div class="tab-pane active" id="tabUsers">--}}
                        {{--<div class="find">--}}
                            {{--<input type="text" class="form-control find-here" placeholder="Axtar">--}}
                        {{--</div>--}}
                        {{--@foreach(\App\User::realData('user')->with('card')->get() as $user)--}}
                            {{--<div class="card share" data_id="{{ $user->id }}">--}}
                                {{--<div class="card-header clearfix">--}}
                                    {{--<div class="user-pic">--}}
                                        {{--<img alt="Profile Image" width="33" height="33" data-src-retina="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}" src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}">--}}
                                    {{--</div>--}}
                                    {{--<h5>{{ $user->fullname() }}</h5>--}}
                                    {{--<h6>{{ 'İstifadəçi' }}</h6>--}}
                                    {{--<i id="kart-icon" class="fa fa-credit-card {{ $user->card && $user->card->card_number > 0? 'active':'' }}"></i>--}}
                                    {{--<i id="finger-icon" class="fa fa-hand-o-up {{ $user->card && $user->card->fingers->count() ? 'active':'' }}"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                    <div class="tab-pane active" id="tabStudents">
                        <div class="find">
                            <input type="text" class="form-control find-here" placeholder="Axtar">
                        </div>
                        @foreach($user = \App\User::realData('student')->with('card')
                            ->leftJoin('letter_group_user', function ($join) use($lastYearId) {
                                $join->on('users.id', '=', 'letter_group_user.user_id')->on('letter_group_user.year_id', DB::raw($lastYearId));
                            })
                            ->leftJoin('letter_groups', 'letter_groups.id', '=', 'letter_group_user.letter_group_id')
                            ->leftJoin('class_letters', 'class_letters.id', '=', 'letter_groups.class_letter_id')
                            ->leftJoin('msk_classes', 'msk_classes.id', '=', 'class_letters.msk_class_id')
                            ->select('users.*', DB::raw("CONCAT(msk_classes.name, class_letters.name) as class_name"))
                            ->get() as $user)
                            <div class="card share" data_id="{{ $user->id }}">
                                <div class="card-header clearfix">
                                    <div class="user-pic">
                                        <img alt="Profile Image" width="33" height="33" data-src-retina="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}" src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}">
                                    </div>
                                    <h5>{{ $user->fullname() }}</h5>
                                    <h6>{{ $user->class_name == '' ? '-' : $user->class_name }}</h6>
                                    <i class="fa fa-credit-card {{ $user->card && $user->card->card_number > 0? 'active':'' }}"></i>
                                    <i class="fa fa-hand-o-up {{ $user->card && $user->card->fingers->count() ? 'active':'' }}"></i>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="tabParents">
                        <div class="find">
                            <input type="text" class="form-control find-here" placeholder="Axtar">
                        </div>
                        @foreach(\App\User::realData('parent')->with('card')->get() as $user)
                            <div class="card share" data_id="{{ $user->id }}">
                                <div class="card-header clearfix">
                                    <div class="user-pic">
                                        <img alt="Profile Image" width="33" height="33" data-src-retina="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}" src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}">
                                    </div>
                                    <h5>{{ $user->fullname() }}</h5>
                                    <h6>{{ 'Valideynlər' }}</h6>
                                    <i class="fa fa-credit-card {{ $user->card && $user->card->card_number > 0? 'active':'' }}"></i>
                                    <i class="fa fa-hand-o-up {{ $user->card && $user->card->fingers->count() ? 'active':'' }}"></i>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="tabPersonel">
                        <div class="find">
                            <input type="text" class="form-control find-here" placeholder="Axtar">
                        </div>
                        @foreach(\App\User::realData('staff')->with('card')->get() as $user)
                            <div class="card share" data_id="{{ $user->id }}">
                                <div class="card-header clearfix">
                                    <div class="user-pic">
                                        <img alt="Profile Image" width="33" height="33" data-src-retina="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}" src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}">
                                    </div>
                                    <h5>{{ $user->fullname() }}</h5>
                                    <h6>{{ 'Personal' }}</h6>
                                    <i class="fa fa-credit-card {{ $user->card && $user->card->card_number > 0? 'active':'' }}"></i>
                                    <i class="fa fa-hand-o-up {{ $user->card && $user->card->fingers->count() ? 'active':'' }}"></i>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="tabTeachers">
                        <div class="find">
                            <input type="text" class="form-control find-here" placeholder="Axtar">
                        </div>
                        @foreach(\App\User::realData('teacher')->with('card')->get() as $user)
                            <div class="card share" data_id="{{ $user->id }}">
                                <div class="card-header clearfix">
                                    <div class="user-pic">
                                        <img alt="Profile Image" width="33" height="33" data-src-retina="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}" src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}">
                                    </div>
                                    <h5>{{ $user->fullname() }}</h5>
                                    <h6>{{ 'Müəllimlər' }}</h6>
                                    <i class="fa fa-credit-card {{ $user->card && $user->card->card_number > 0? 'active':'' }}"></i>
                                    <i class="fa fa-hand-o-up {{ $user->card && $user->card->fingers->count() ? 'active':'' }}"></i>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" id="finger_view">

        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .card .fa.active{
            color: #0eb2a3;
        }
        .card{
            margin-bottom: 1px !important;
            width: 100% !important;
        }
        .card.active{
            background: #daeffd;
        }
        .card .fa-hand-o-up{
             position: absolute;
             right: 5px;
             top: 11px;
             font-size: 25px;
         }
        .card .fa-credit-card{
            position: absolute;
            right: 30px;
            top: 11px;
             font-size: 25px;
        }
        .find{
            margin-bottom: 10px;
            border-bottom: 1px dotted #e6e6e6;
            padding-bottom: 10px;
        }
        .fingers_map{position:relative;}
        .fingers_map_img{display:none;width:100%;position:absolute;top:0;left:0;opacity:0.2;}
        .fingers_map_area{position:absolute;cursor:pointer;}
        .fingers_map_img.active{
            display: inline;
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script>
        //find
        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
            return function( elem ) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        let time = 300, action = false, tab = false, txt = "";
        $(".find-here").keyup(function() {
            tab = $(this).parents(".tab-pane:eq(0)");
            txt = $(this).val();

            if(action) clearTimeout(action);
            action = setTimeout(find, time);
        });

        function find() {
            let card = tab.find(".card:contains('"+txt+"')");

            tab.find(".card:visible").hide();
            card.show('fast');
        }

        //card
        $(".card[data_id]").click(function () {
            let data_id = $(this).attr("data_id");

            pageLoading('show');
            $(".card.active").removeClass("active");
            $(this).addClass("active");

            $.post("{{ route('finger_user_info') }}", {
                id: data_id,
                _token: _token
            }, function (response) {
                if (response['status'] == 'error') $("#errors").html(response['errors']);
                else {
                    $("#finger_view").html(response['fingesView']);
                    $("#user_view").html(response['userView']);
                }
                pageLoading('hide');
            });
        });

        //register
        $("#finger_view").on("click", ".save-card", function ()
        {
            let type = $('.card .nav-tabs li.active').attr('type'),
                uid = $(".card.active").attr("data_id"),
                cardN = $("input.card-number").val();

            if(uid>0)
            {
                pageLoading('show');
                $.post("{{ asset('fpp/fp_register_remove.php') }}", {card:uid,cardN:cardN,'type':type}).done(function(rRes)
                {
                    if(rRes=='Success')
                    {
                        $('body').pgNotification({
                            style: 'flip',
                            message: "Yadda saxlandı.",
                            position: "top",
                            timeout: 0,
                            type: "success"
                        }).show();

                        if(cardN <= 0)
                            $(".card.active .fa-credit-card").removeClass('active');
                        else
                            $(".card.active .fa-credit-card").addClass('active');
                    }
                    else if(rRes=="cardError")
                    {
                        $('body').pgNotification({
                            style: 'flip',
                            message: "Bu kart artıq başqa istifadəçi üçün qeydiyyatdan keçirilib.",
                            position: "top",
                            timeout: 0,
                            type: "error"
                        }).show();
                    }

                    pageLoading('hide');
                });
            }
        });

        $('#finger_view').on('click', 'div[fing]', function () {
            let used = $(this).attr('used'),
                idsi = $(this).attr('idsi'),
                type = $('.card .nav-tabs li.active').attr('type'),
                uid =  $(".card.active").attr("data_id"),
                fingNum = parseInt($(this).attr('fing').substring(5))-1;

            if(used<=0)
            {
                pageLoading('show');
                $.post("{{ asset('fpp/fp_register_add.php') }}", {'type':type, 'uid': uid}, function(result)
                {
                    try
                    {
                        pageLoading('hide');

                        result = JSON.parse(result);
                        if(result['status']=="ok")
                        {
                            let regId = result['reg_id'];
                            location.href = "proschool:u"+regId+":"+fingNum;
                        }
                        else
                        {
                            $('body').pgNotification({
                                style: 'flip',
                                message: "Səhv var!",
                                position: "top",
                                timeout: 0,
                                type: "error"
                            }).show();
                        }
                    }
                    catch( e )
                    {

                    }
                });
            }
            else
            {
                $.confirm({
                    title: 'Təsdiq',
                    content: 'Silmək istədiyinizə əminsiniz?',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        formSubmit : {
                            text:'Bəli',
                            btnClass:'btn-green',
                            action:function(){

                                $.post("{{ asset('fpp/fp_register_remove.php') }}", {uid:uid, fingNum:fingNum, idsi:idsi}).done(function(rRes)
                                {
                                    if(rRes=='Success')
                                    {
                                        $(".card.active").trigger("click");
                                    }
                                });

                            }
                        },
                        formCancel : {
                            text:'Xeyr',
                            btnClass:'btn-red',
                            action:function(){

                            }
                        }
                    }
                });
            }
        });
    </script>
@endsection