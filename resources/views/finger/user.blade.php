<style>
    .card div:nth-child(2) h5{
        color: #075177;
        font-weight: bold;
        font-family: "Segoe UI";
        font-size: 14px;
    }
</style>
<div class="card" style="width: 100%;">
    <div class="row-xs-height">
        <div class="social-user-profile col-xs-height text-center col-top">
            <div class="thumbnail-wrapper d48 circular bordered b-white">
                <img alt="Avatar" width="55" height="55" src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}">
            </div>
        </div>
        <div class="col-xs-height p-l-20">
            <h3 class="no-margin">{{ $user->fullname() }}</h3>
            <p class="hint-text m-t-5 small">{{ $user->user_type == 'student' ? ($user->class_name==''?'-':$user->class_name) : \App\Library\Standarts::$userTypes[$user->user_type] }}</p>
        </div>
    </div>
    <div class="com-md-12" style="padding: 10px">
        <h5 class="hint-text no-margin">DOĞUM TARIXI:</h5>
        <h5 class="no-margin">{{ $user->birthday != '' ? date("d-m-Y", strtotime($user->birthday)) : '' }}</h5>
        <h5 class="hint-text no-margin">E-MAIL:</h5>
        <h5 class="no-margin">{{ $user->email }}</h5>
        <h5 class="hint-text no-margin">MOBILTELEFON:</h5>
        <h5 class="no-margin">{{ $user->mobil_tel }}</h5>
        <h5 class="hint-text no-margin">EVTELEFONU:</h5>
        <h5 class="no-margin">{{ $user->home_tel }}</h5>
        <h5 class="hint-text no-margin">ÜNVAN:</h5>
        <h5 class="no-margin">{{ $user->address }}</h5>
    </div>
</div>