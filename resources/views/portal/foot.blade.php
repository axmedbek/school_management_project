<script>
    var _token = "{{ csrf_token() }}";
</script>
<script src="{{ asset('/portal/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/dropzone/dist/dropzone.js') }}"></script>
<script src="{{ asset('/portal/bower_components/editable-table/mindmup-editabletable.js') }}"></script>
<script src="{{ asset('/portal/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/slick-carousel/slick/slick.min.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/util.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/alert.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/button.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/carousel.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/collapse.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/dropdown.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/modal.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/tab.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/tooltip.js') }}"></script>
<script src="{{ asset('/portal/bower_components/bootstrap/js/dist/popover.js') }}"></script>
<script src="{{ asset('/portal/js/maince5a.js?version=4.6') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>--}}
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/socket.io.js') }}" type="text/javascript"></script>
<script>
    {{--var socket = io(location.hostname + ':2222', {--}}
    {{--    query: 'token={{ $_COOKIE["school_management_system_session"] }}&authType=portal',--}}
    {{--    transports: ['websocket'],--}}
    {{--    reconnection: false,--}}
    {{--    secure: true--}}
    {{--});--}}

    {{--socket.on('connect', function () {--}}
    {{--});--}}

    {{--socket.on('disconnect', function () {--}}
    {{--});--}}
</script>
@yield('script')