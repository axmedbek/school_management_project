<!--------------------
           START - Mobile Menu
           -------------------->
<div class="menu-mobile menu-activated-on-click color-scheme-dark">
    <div class="mm-logo-buttons-w">
        <a class="mm-logo" href="{{ route('portal_dashboard') }}">
            <img src="{{ asset('assets/img/school.png') }}"><span>Schoolary Mobile</span>
        </a>
        <div class="mm-buttons">
            <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
            </div>
        </div>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="avatar-w"><img alt=""
                                       src="{{ asset(\App\Library\Standarts::$userThumbir . (auth()->guard('portal')->user()->thumb != null ? auth('portal')->user()->thumb : "user.png")) }}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">{{ Auth::user()->fullname() }}</div>
                <div class="logged-user-role">{{ \App\Library\Standarts::$userTypesSingle[Auth::user()->user_type] }}</div>
            </div>
        </div>
        <!--------------------
           START - Mobile Menu List
           -------------------->
        <ul class="main-menu">
            <ul class="main-menu">
                <li class="sub-header"><span>{{ trans('system_messages.modules.name') }}</span></li>
                @foreach(\App\Library\Standarts::$portalModules as $key => $module)
                    @if(in_array(\Illuminate\Support\Facades\Auth::user()->user_type, $module['priv']) && $module['is_menu'])
                        <li class="{{ $module['route']==\Request::route()->getName()?'selected':'' }}">
                            <a @if($module['route']) href="{{ route($module['route']) }}" @endif route="{{ $module['route'] }}">
                                <div class="icon-w">
                                    <div class="{{ $module['icon'] }}"></div>
                                </div>
                                <span>{{ trans('system_messages.modules.'.$key) }}</span>
                                @if(!$module['route'])
                                    <strong class="badge badge-danger">Upgrade</strong>
                                @endif
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </ul>
        <!--------------------
           END - Mobile Menu List
           -------------------->
    </div>
</div>
<!--------------------
   END - Mobile Menu
   --------------------><!--------------------
               START - Main Menu
               -------------------->
<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-dark color-style-default sub-menu-color-dark menu-position-side menu-side-left menu-layout-compact sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="{{ route('portal_dashboard') }}" style="margin-right: 30px;">
            <img src="{{ asset('assets/img/school.png') }}" style="width:130%">
        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w" style="width: 50px;"><img alt=""
                                                            src="{{ asset(\App\Library\Standarts::$userThumbir . (auth()->guard('portal')->user()->thumb != null ? auth('portal')->user()->thumb : "user.png")) }}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">{{ Auth::user()->fullname() }}</div>
                <div class="logged-user-role">{{ \App\Library\Standarts::$userTypesSingle[Auth::user()->user_type] }}</div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w" style="width: 50px;"><img alt=""
                                                                    src="{{ asset(\App\Library\Standarts::$userThumbir . (auth()->guard('portal')->user()->thumb != null ? auth('portal')->user()->thumb : "user.png")) }}">
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">{{ Auth::user()->fullname() }}</div>
                        <div class="logged-user-role">{{ \App\Library\Standarts::$userTypesSingle[Auth::user()->user_type] }}</div>
                    </div>
                </div>
                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                <ul>
                    <li><a href="{{ route('portal_profile') }}"><i class="os-icon os-icon-user-male-circle2"></i><span>Profil</span></a>
                    </li>
                    <li><a href="{{ route('logout') }}"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    {{--<div class="menu-actions">--}}
        {{--<!----------------------}}
           {{--START - Messages Link in secondary top menu--}}
           {{---------------------->--}}
        {{--<div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">--}}
            {{--<i class="os-icon os-icon-mail-07"></i>--}}
            {{--<div class="new-messages-count" style="margin-top: -10px;">Upgrade</div>--}}
            {{--<div class="os-dropdown light message-list">--}}
            {{--<ul>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<div class="user-avatar-w"><img alt="" src="img/avatar1.jpg"></div>--}}
            {{--<div class="message-content">--}}
            {{--<h6 class="message-from">John Mayers</h6>--}}
            {{--<h6 class="message-title">Account Update</h6>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<div class="user-avatar-w"><img alt="" src="img/avatar2.jpg"></div>--}}
            {{--<div class="message-content">--}}
            {{--<h6 class="message-from">Phil Jones</h6>--}}
            {{--<h6 class="message-title">Secutiry Updates</h6>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<div class="user-avatar-w"><img alt="" src="img/avatar3.jpg"></div>--}}
            {{--<div class="message-content">--}}
            {{--<h6 class="message-from">Bekky Simpson</h6>--}}
            {{--<h6 class="message-title">Vacation Rentals</h6>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="#">--}}
            {{--<div class="user-avatar-w"><img alt="" src="img/avatar4.jpg"></div>--}}
            {{--<div class="message-content">--}}
            {{--<h6 class="message-from">Alice Priskon</h6>--}}
            {{--<h6 class="message-title">Payment Confirmation</h6>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!----------------------}}
           {{--END - Messages Link in secondary top menu--}}
           {{----------------------><!----------------------}}
                     {{--START - Settings Link in secondary top menu--}}
                     {{---------------------->--}}
        {{--<div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-right">--}}
            {{--<i class="fa fa-language"></i>--}}
            {{--<div class="os-dropdown">--}}
                {{--<div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div>--}}
                {{--<ul>--}}
                    {{--<li><a href="{{ route('portal_change_lang',['locale'=> 'az']) }}"><img--}}
                                    {{--src="{{ asset('portal/img/az.png') }}"> <span>Azərbaycan</span></a></li>--}}
                    {{--<li><a href="{{ route('portal_change_lang',['locale'=> 'en']) }}"><img--}}
                                    {{--src="{{ asset('portal/img/en.png') }}"> <span>English</span></a></li>--}}
                    {{--<li><a href="{{ route('portal_change_lang',['locale'=> 'ru']) }}"><img--}}
                                    {{--src="{{ asset('portal/img/ru.png') }}"> <span>Русский</span></a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!----------------------}}
           {{--END - Settings Link in secondary top menu--}}
           {{----------------------><!----------------------}}
                     {{--START - Messages Link in secondary top menu--}}
                     {{---------------------->--}}
        {{--<div class="messages-notifications os-dropdown-trigger os-dropdown-position-right"><i--}}
                    {{--class="fa fa-bell-o"></i>--}}
            {{--@php--}}
                {{--$notifications = \App\Models\Notification::where('user_id',auth()->guard('portal')->user()->id)--}}
                {{--->where('seen',0)--}}
                {{--->orderByDesc('id')->get();--}}
            {{--@endphp--}}
            {{--<div class="new-messages-count specific_notify_count">{{ count($notifications) }}</div>--}}
            {{--<div class="os-dropdown light message-list" style="right: 10px;">--}}
                {{--<div class="icon-w"><i class="os-icon os-icon-zap"></i></div>--}}
                {{--<div style="width: 250px;">--}}
                    {{--<h6 class="text-center"--}}
                        {{--style="color: #777373;">{{ trans('system_messages.teacher_request.teacher_request_notify') }} </h6>--}}
                    {{--@if ($smses == "[]") <h6  class="text-center" style="color: #777373;font-size: 14px;margin-top: 20px">{{ trans('system_messages.teacher_request.teacher_request_notify_info') }}</h6> @endif--}}
                    {{--<p style="margin-bottom: 30px;"></p>--}}
                {{--</div>--}}
                {{--@if ($notifications == "[]")--}}
                    {{--<ul>--}}
                        {{--<li>--}}
                            {{--<h6 class="text-center" style="color: #777373;font-size: 14px;margin-top: 20px">--}}
                                {{--{{ trans('system_messages.teacher_request.teacher_request_notify_info') }}--}}
                            {{--</h6>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--@else--}}
                    {{--<ul style="height: 300px;--}}
                               {{--overflow-y: auto !important;--}}
                               {{--overflow-x: hidden;--}}
                               {{--width: 450px;">--}}
                        {{--@foreach($notifications as $notfication)--}}
                            {{--<li notify_id="{{ $notfication->id }}"><a href="{{ $notfication->route }}">--}}
                                    {{--<div class="message-content">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-2" style="margin-top: 5px;">--}}
                                                {{--<div class="message-from">--}}
                                                    {{--<i class="{{ $notfication->icon }}"--}}
                                                       {{--style="color:{{ $notfication->color }}; /*#047bf8;*/"></i>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-10">--}}
                                                {{--<h6 class="message-from"--}}
                                                    {{--style="font-size: 13px;">{{ $notfication->content }}</h6>--}}
                                                {{--<h6 class="message-title"--}}
                                                    {{--style="font-size: 11px;color: #323b58;">{{ Carbon\Carbon::parse($notfication->date)->format('Y-m-d H:i:s') }}</h6>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<hr>--}}
                                    {{--</div>--}}
                                {{--</a></li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!----------------------}}
               {{--END - Messages Link in secondary top menu--}}
               {{---------------------->--}}
    {{--</div>--}}
    {{--<div class="element-search autosuggest-search-activator"><input placeholder="Start typing to search..." type="text"></div>--}}
    <h1 class="menu-page-header">Page Header</h1>
    <ul class="main-menu">
        <li class="sub-header"><span>{{ trans('system_messages.modules.name') }}</span></li>
        @foreach(\App\Library\Standarts::$portalModules as $key => $module)
            @if(in_array(\Illuminate\Support\Facades\Auth::user()->user_type, $module['priv']) && $module['is_menu'])
                <li class="{{ $module['route']==\Request::route()->getName()?'selected':'' }}">
                    <a @if($module['route']) href="{{ route($module['route']) }}" @endif route="{{ $module['route'] }}">
                        <div class="icon-w">
                            <div class="{{ $module['icon'] }}"></div>
                        </div>
                        <span>{{ trans('system_messages.modules.'.$key) }}</span>
                        @if(!$module['route'])
                            <strong class="badge badge-danger">Upgrade</strong>
                        @endif
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
    <style>
        .footer {
            position: fixed;
            bottom: 0;
            text-align: left;
            margin-left: 20px;
        }
    </style>
    <div class="footer">
        <span class="hint-text"
              style="color: #ffffff;font-weight: bold;"> &copy; Schoolary {{ \Carbon\Carbon::now()->year }} </span><br>
        <a href="https://schoolary.com/" class="font-montserrat" target="_blank" style="color: #a4caf3">www.schoolary.com</a>
    </div>
</div>
<!--------------------
   END - Main Menu
   -------------------->