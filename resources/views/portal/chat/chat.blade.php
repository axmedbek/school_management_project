@extends('portal.base')
@section('content')

    <div class="full-chat-w">
        <div class="full-chat-i">
            <div class="full-chat-left">
                <div class="os-tabs-w">
                    <ul class="nav nav-tabs upper centered">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat"><i
                                        class="os-icon os-icon-mail-14"></i><span>Chats</span></a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#groups"><i
                                        class="os-icon os-icon-ui-93"></i><span>Groups</span></a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane" id="chat">
                        <div class="chat-search">
                            <div class="element-search"><input placeholder="Axtar..." type="text" name="search_by_name">
                            </div>
                        </div>
                        <div class="user-list" style="overflow: auto;height: 400px;">
                            @foreach($users as $user)
                                @php
                                    if ($user->user_type == 'user'){
                                        if ($user->group->getModulePriv('admin_chat') < 2){
                                            continue;
                                        }
                                    }
                                        $dateDiff = Carbon\Carbon::now()->getTimestamp() - Carbon\Carbon::parse($user->last_active_date)->getTimestamp();
                                        $newMsgCount = \App\Models\Chat::realData()
                                        ->where('sender_id',$user->id)
                                        ->where('reciever_id',auth('portal')->user()->id)
                                        ->where('read',0)
                                        ->get();
                                @endphp
                                <div class="user-w" data-user-id="{{ $user->id }}">
                                    <div class="avatar with-status status-green">
                                        @if(file_exists(public_path().'/images/user/thumb/'.$user->thumb))
                                            <img src="{{ asset(\App\Library\Standarts::$userThumbir . $user->thumb) }}" alt="">
                                        @else
                                            <img src="{{ asset(\App\Library\Standarts::$userThumbir) }}" alt="">
                                        @endif
                                    </div>
                                    {{--($dateDiff == 0 || $dateDiff > 3000) ? 'Offline' :--}}
                                    {{--gmdate("H:i:s",$dateDiff).' san.'--}}
                                    <div class="user-info">
                                        <div class="user-date onlineStatus"
                                             style="font-weight: bold;color:{{ $user->is_online ? '#5BC89E' : 'tomato' }}">{{ $user->is_online ? 'Online' :
                                        'Offline' }}</div>
                                        <div class="user-name">{{ $user->fullname() }}</div>
                                        <div class="user-date msgCount"
                                             style="display: {{ count($newMsgCount) > 0 ? 'block' : 'none' }};
                                                     width: 60px;text-align: center;
                                                     font-size: 12px;font-weight: bold;
                                                     color: white;background-color: #69ca9d;">
                                            <span>{{ count($newMsgCount) }}</span> yeni
                                        </div>
                                        <div class="last-message">
                                            @switch($user->user_type)
                                                @case('teacher')
                                                {{ $user->teacher_subjects()->first() != null ? $user->teacher_subjects()->first()->name.' müəllimi' : ' Müəllim' }}
                                                @break
                                                @case('student')
                                                {{ 'Sagird' }}
                                                @break
                                                @case('parent')
                                                Valideyn
                                                @break
                                                @case('user')
                                                Admin
                                                @break
                                            @endswitch
                                            {{--{{ $user->user_type == 'teacher' ?--}}
                                               {{--(isset($user->lesson) ? $user->lesson->subject->name.' müəllimi' : ' Müəllim')--}}
                                               {{--: ($user->letter_groups->first() ?--}}
                                                    {{--'Sinif : '.\App\Models\ClassLetter::realData()->find($user->letter_groups->first()['class_letter_id'])['msk_class']['name'].--}}
                                                   {{--\App\Models\ClassLetter::realData()->find($user->letter_groups->first()['class_letter_id'])['name']--}}
                                                   {{--: $user->user_type == 'student' ? 'Şagird' : $user->user_type == 'user' ? 'Admin' : 'Valideyn')--}}
                                               {{--}}--}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane" id="groups">
                        <div class="chat-search">
                            {{--<div class="element-search"><input placeholder="Axtar..." type="text" name="search_group"></div>--}}
                        </div>
                        <div class="user-list" style="overflow: auto;height: 400px;">

                            @foreach($letterGroups as $letter_group)
                                @php $letterGroup = \App\Models\LetterGroup::realData()->find($letter_group->letter_group_id) @endphp
                                <div class="user-w" data-group-id="{{ $letterGroup->id }}">
                                    <div class="user-info">
                                        <div class="user-date"></div>
                                        <div class="user-name">
                                            {{
                                                $letterGroup->class_letter->msk_class->name.' '.
                                                $letterGroup->class_letter->name.' ( '.$letterGroup->name.' ) sinif qrupu'
                                            }}
                                        </div>
                                        <div class="last-message">

                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            <div class="full-chat-middle">
                <div class="chat-head" style="height: 77px;">
                    <div class="col-md-8">
                        <div class="user-info">
                        </div>
                        <span class="user_definer">
                        </span>
                    </div>
                    <div class="col-md-4" style="float: right;">
                        <a onclick="openModal('{{ route('portal_chat_group_user_info') }}', {'letter_group_id' : $('.activeGroupTab').attr('data-group-id')}, $('.chat-head .user-info').text()+' istifadəçiləri', 'modal-md')"
                           class="btn btn-danger btn-sm btnGroupUserInfo"
                           style="float: right;">
                            <i class="fa fa-info-circle" style="font-size: 20px;color: white;"></i>
                        </a>
                        <span class="userStatus" style="float: right;
                        margin-right: 12px;
                        color: white;
                        border-radius: 4px;
                        padding: 4px;
                        width: 80px;
                        text-align: center;"></span>
                    </div>
                </div>
                <div class="chat-content-w">
                    <div class="chat-content">

                    </div>
                </div>
                <div class="chat-controls">
                    <div class="chat-input"><input placeholder="Mesajınızı buraya daxil edin..." type="text"></div>
                    <div class="chat-input-extra">
                        <div class="chat-btn-custom"><a class="btn btn-primary btn-sm" href="javascript:void(0)">Mesaj
                                yaz</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('css')
    <style>
        body {
            overflow: auto !important;
        }

        .full-chat-w .chat-content-w {
            height: 450px;
            overflow-y: scroll;
            position: relative;
        }

        .activeUserTab, .activeGroupTab {
            background-color: #047bf8;
            cursor: pointer;
            color: white !important;
        }

        .full-chat-w .chat-content-w .chat-content {
            padding: 50px;
            min-height: 450px;
        }

        .content-box {
            min-height: 0;
        }

        .full-chat-w .full-chat-left .user-list .user-w:hover .user-date {
            background-color: tomato;
            color: white !important;
        }

    </style>
@endsection
@section('script')
    <script>
        $('.btnGroupUserInfo').hide();
        socket.on('new message', function (response) {

            if (response.type == 'private') {
                if (response.connection_id == $('.activeUserTab').attr('data-user-id')) {
                    $('.chat-content').append('<div class="chat-message">' +
                        '<div class="chat-message-content-w">' +
                        '<div class="chat-message-content" style="color: #885050;background-color: #f1dcbe;">' + response.name + ' ' + (response.surname != null ? response.surname : '') + ' ' + (response.middle_name != null ? response.middle_name : '') + '</div></div>' +
                        '<div class="chat-message-avatar"><img alt="" src="{{ asset('images/user/thumb/') }}/' + (response.thumb ? response.thumb : 'user.png') + '">' +
                        '</div>' +
                        '<div class="chat-message-content" style="margin-left: 0;margin-bottom: 0;padding: 0;padding-left: 33px;">' + response.content + '</div><br>' +
                        '<div class="chat-message-date" style="margin-left: 77px;">{{ Carbon\Carbon::now()->format('Y-m-d H:i') }}</div>' +
                        '</div>');
                    var $messages_w = $('.chat-content-w');
                    $messages_w.scrollTop($messages_w[0].scrollHeight);
                }
            }
            else if (response.type == 'group') {
                if (response.connection_id == $('.activeGroupTab').attr('data-group-id')) {
                    $('.chat-content').append('<div class="chat-message">' +
                        '<div class="chat-message-content-w">' +
                        '<div class="chat-message-content" style="color: #885050;background-color: #f1dcbe;">' + response.name + ' ' + (response.surname != null ? response.surname : '') + ' ' + (response.middle_name != null ? response.middle_name : '') + '</div></div>' +
                        '<div class="chat-message-avatar"><img alt="" src="{{ asset('images/user/thumb/') }}/' + (response.thumb ? response.thumb : 'user.png') + '">' +
                        '</div>' +
                        '<div class="chat-message-content" style="margin-left: 0;margin-bottom: 0;padding: 0;padding-left: 33px;">' + response.content + '</div><br>' +
                        '<div class="chat-message-date" style="margin-left: 77px;">{{ Carbon\Carbon::now()->format('Y-m-d H:i') }}</div>' +
                        '</div>');
                    var $messages_w = $('.chat-content-w');
                    $messages_w.scrollTop($messages_w[0].scrollHeight);
                }
            }
        });

        //offline user
        socket.on('user offline', function (response) {
            console.log(response);
            $('#chat .user-list .user-w').each(function () {
                if ($(this).attr('data-user-id') == response.user_id) {
                    $(this).find('.user-date').text('Offline');
                    $(this).find('.user-date').css('color', 'red');
                }
            });
        });

        //online user
        socket.on('user online', function (response) {
            $('#chat .user-list .user-w').each(function () {
                if ($(this).attr('data-user-id') == response.user_id) {
                    $(this).find('.user-date').text('Online');
                    $(this).find('.user-date').css('color', '#5BC89E');
                }
            });
        });


        // CHAT USER SEARCH START HERE
        $('[name="search_by_name"]').on('keyup', function () {
            let val = $(this).val();
            delay(function () {
                pageLoading('show');
                $.get('{{ route('portal_chat') }}', {
                    'searchedVal': val,
                    'type': 'ajax'
                }, function (response) {
                    pageLoading();
                    if (response.status == "ok") {
                        $('#chat .user-list').html('');
                        $('#chat .user-list').html(response.users);
                        listingChatOrGroup('#chat', 'activeUserTab', 'data-user-id', 'private');
                        $('#chat .user-list .user-w').eq(0).click();
                    }
                });
            }, 1000);
        });

        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        // CHAT USER SEARCH END HERE

        // chat list listing and clicking
        listingChatOrGroup('#chat', 'activeUserTab', 'data-user-id', 'private');

        // $('#chat .user-list .user-w').eq(0).click();

        //group tab click process
        listingChatOrGroup('#groups', 'activeGroupTab', 'data-group-id', 'group');

        let searchParams = new URLSearchParams(window.location.search);

        if (searchParams.has('tab')) {
            if (searchParams.get('tab') == "chat") {
                if ($('#chat').find('[data-user-id="' + searchParams.get('rec_id') + '"]').length == 0) {
                    $('[href="#chat"]').click();
                    $('#chat .user-list .user-w').eq(0).click();
                }
                else {
                    $('[href="#chat"]').click();
                    $('#chat').find('[data-user-id="' + searchParams.get('rec_id') + '"]').click();
                    $('#groups').find('.activeGroupTab').removeClass('activeGroupTab');
                }

            }
            else if (searchParams.get('tab') == "groups") {
                if ($('#groups').find('[data-group-id="' + searchParams.get('rec_id') + '"]').length == 0) {
                    $('[href="#chat"]').click();
                    $('#chat .user-list .user-w').eq(0).click();
                }
                else {
                    $('[href="#groups"]').click();
                    $('#groups').find('[data-group-id="' + searchParams.get('rec_id') + '"]').click();
                    $('#chat').find('.activeUserTab').removeClass('activeUserTab');
                }
            }
            else {
                $('[href="#chat"]').click();
                $('#chat .user-list .user-w').eq(0).click();
                $('#groups').find('.activeGroupTab').removeClass('activeGroupTab');
            }
        }
        else {
            $('[href="#chat"]').click();
            $('#chat .user-list .user-w').eq(0).click();
            $('#groups').find('.activeGroupTab').removeClass('activeGroupTab');
        }

        $('[href="#chat"]').click(function () {
            $('#chat .user-list .user-w').eq(0).click();
            $('#groups .user-list .user-w').eq(0).removeClass('activeGroupTab');
            $('.btnGroupUserInfo').hide();
        });

        // GROUPS AUTO CLICK
        $('[href="#groups"]').click(function () {
            $('#groups .user-list .user-w').eq(0).click();
            $('#chat .user-list .user-w').eq(0).removeClass('activeUserTab');
            $('.btnGroupUserInfo').show();
        });


        // reply button click
        $('.chat-btn-custom a').on('click', function () {
            addMessage();
        });

        // enter key press
        $('.chat-input input').on('keypress', function (e) {
            if (e.which == 13) {
                addMessage();
            }
        });


        // ADD NEW MESSAGE TO MEXXAGE BOX START HERE
        function addMessage() {
            let message = $('.chat-input input').val();
            let sender_id = {{ auth('portal')->user()->id }},
                recivier_id = $('.activeUserTab').attr('data-user-id');

            if (message.length < 1) return;

            $('.chat-content').append('<div class="chat-message self">' +
                '                                <div class="chat-message-content-w">' +
                '                                <div class="chat-message-content">' + message + '</div>' +
                '                                </div>' +
                '                                <div class="chat-message-date">{{ Carbon\Carbon::now()->format('Y-m-d H:i') }}</div>' +
                '                                <div class="chat-message-avatar"><img alt="" src="{{ asset('images/user/thumb/'.auth()->user()->thumb) }}"></div>' +
                '                                </div>');
            if ($('a[href="#groups"]').hasClass('active')) {
                socket.emit('new message', {
                    'user_id': $('.activeGroupTab').attr('data-group-id'),
                    'sender_id': sender_id,
                    'message': message,
                    'laravel_session': '{{ $_COOKIE["school_management_system_session"] }}',
                    'type': 'group'
                });
            }
            else if ($('a[href="#chat"]').hasClass('active')) {
                socket.emit('new message', {
                    'user_id': recivier_id,
                    'sender_id': sender_id,
                    'message': message,
                    'school_management_system_session': '{{ $_COOKIE["school_management_system_session"] }}',
                    'type': 'private'
                });
            }
            $('.chat-input input').val("");
            var $messages_w = $('.chat-content-w');
            $messages_w.scrollTop($messages_w[0].scrollHeight);
        }
        // ADD NEW MESSAGE TO MESSAGE BOX END HERE

        // CHAT AND GROUP TAB FOREACH START HERE
        function listingChatOrGroup(tab, activeTabClassName, recieverAttribute, type) {
            $(tab + ' .user-list .user-w').each(function () {
                $(this).on('click', function () {
                    if ($(this).hasClass(activeTabClassName)) return;
                    pageLoading('show');
                    let sender_id = {{ auth('portal')->user()->id }},
                        recivier_id = $(this).attr(recieverAttribute);
                    $('.chat-head .user-info').text($(this).find('.user-name').text());
                    $('.chat-head .user_definer').text($(this).find('.last-message').text());
                    $('.chat-head .userStatus').text($(this).find('.onlineStatus').text());
                    if ($(this).find('.onlineStatus').text().length > 0) {
                        $('.chat-head .userStatus').css('background-color', $(this).find('.onlineStatus').text() == 'Online' ? '#5BC89E' : '#e84022');
                        $('.user-w').hover(function () {
                            $(this).find('.onlineStatus').css('background-color', $(this).find('.onlineStatus').text() == 'Online' ? '#5BC89E' : '#e84022');
                        }, function () {
                            $(this).find('.onlineStatus').css('background-color', 'white');
                        });
                    }
                    else {
                        $('.chat-head .userStatus').css('background-color', 'white');
                    }
                    $('.' + activeTabClassName).find('.last-message').css('color', 'rgba(0,0,0,0.4)');
                    $('.' + activeTabClassName).removeClass(activeTabClassName);

                    $(this).addClass(activeTabClassName);
                    $(this).find('.last-message').css('color', 'white');
                    pageLoading('show');
                    $.post('{{ route('chat.message') }}', {
                        'sender_id': sender_id,
                        'recivier_id': recivier_id,
                        'type': type,
                        _token: _token
                    }, function (response) {
                        $('.chat-content').html(response.messages);
                        $(this).find('.msgCount span').text(response.newMessagesCount > 0 ? response.newMessagesCount : '');
                        pageLoading();
                        var $messages_w = $('.chat-content-w');
                        $messages_w.scrollTop($messages_w[0].scrollHeight);
                    });
                });
            });
        }
        // CHAT AND GROUP TAB FOREACH END HERE

    </script>
@endsection