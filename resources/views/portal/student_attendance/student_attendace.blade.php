@extends('portal.base')
@section('content')
    <div class="col-md-12" style="margin-top: 20px;">
        <div class="row">
            <div class="col-sm-3 col-md-3 text-center" style="background-color: #ffffff;border-radius: 4px;border: 2px solid #dde2ec;">
                <label style="margin-top: 4px;">{{ trans('system_messages.student_attendance.interval') }}</label>
            </div>
            <div class="col-sm-3 col-md-3">
                <input type="text" class="form-control datepicker" name="start_interval" style="text-align: center">
            </div>
            <div class="col-sm-3 col-md-3">
                <input type="text" class="form-control datepicker" name="end_interval" style="text-align: center">
            </div>
            <div class="col-sm-2 col-md-2">
                <button class="btn btn-primary show_attendance"><i class="fa fa-search"></i> {{ trans('system_messages.showBtn') }}</button>
            </div>
        </div>
    </div>
    <div class="col-md-12 attendance_table" style="margin-top: 5%;">

    </div>
@endsection
@section('css')
    <link href="{{ asset('portal/bower_components/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">
@endsection
@section('script')
    <script src="{{ asset('/portal/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script>

        $.fn.datepicker.dates['en'] = {
            days: '{!! json_encode(trans('system_messages.weeks')) !!}',
            daysShort: {!! json_encode(trans('messages.daysMin')) !!},
            daysMin: {!! json_encode(trans('messages.daysMin')) !!},
            months: {!! json_encode(array_values(trans('messages.months'))) !!},
            monthsShort: {!! json_encode(trans('messages.monthsShort')) !!},
            today: "{{ trans('system_messages.dashboard.in_out.today') }}",
            clear: "{{ trans('system_messages.clear') }}",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 1
        };

        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoclose : true,
            clearBtn: true,
        });

        openPage('.attendance_table', '{{ route('portal_attendance_get_table') }}');

        $('.show_attendance').click(function(){
            var start_interval = $('[name="start_interval"]').val(),
                end_interval = $('[name="end_interval"]').val();

            openPage('.attendance_table', '{{ route('portal_attendance_get_table') }}',{
                start_interval : start_interval,
                end_interval : end_interval
            });
        });

    </script>
@endsection