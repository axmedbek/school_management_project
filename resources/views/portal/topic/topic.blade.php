@extends('portal.base')

@section('content')
    <!-- Temayul start -->
    <div class="os-tabs-w">
        <div class="os-tabs-controls">
            <ul class="nav nav-tabs upper group_types">
                @foreach($groupTypes as $groupTye)
                    <li class="nav-item "><a class="nav-link {{ $loop->iteration==1?'active clicked':'' }}" group_type_id="{{ $groupTye->id }}" data-toggle="tab" href="#tab_group_type{{ $groupTye->id }}">{{ $groupTye->name }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="tab-content">
            <!-- Class start -->
            <div class="row">
                <!-- Class start -->
                <div class="col-md-1 ver-col classes">
                    <div class="ae-side-menu">
                        <div class="aem-head">
                            <a class="ae-side-menu-toggler" href="javascript:void(0)">
                                {{ trans('system_messages.topics.classes') }}
                            </a>
                        </div>
                        <ul class="ae-main-menu">
                            @foreach($teacherClasses as $teacherClass)
                                <li class_id="{{ $teacherClass->id }}"><a><span>{{ $teacherClass->name }}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- Class end -->

                <!-- Subject start -->
                @foreach($teacherClasses as $teacherClass)
                    <div class="col-md-2 ver-col subjects" class_id="{{ $teacherClass->id }}" style="display: none">
                        <div class="ae-side-menu">
                            <div class="aem-head">
                                <a class="ae-side-menu-toggler" href="#">
                                    {{ trans('system_messages.topics.subjects') }}
                                </a>
                            </div>
                            <ul class="ae-main-menu">
                                @foreach($classGroup[$teacherClass->id] as $subject)
                                    <li class="" subject_id="{{ $subject->id }}"><a><span>{{ $subject->name }}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
                <!-- Subject end -->

                <!-- Season start -->
                @foreach($teacherClasses as $teacherClass)
                    @foreach($classGroup[$teacherClass->id] as $subject)
                        @foreach($groupTypes as $groupType)
                            <div class="col-md-3 ver-col seasons" group_type_id="{{ $groupType->id }}" class_id="{{ $teacherClass->id }}" subject_id="{{ $subject->id }}" style="display: none">
                                <div class="ae-side-menu">
                                    <div class="aem-head">
                                        <a class="ae-side-menu-toggler" href="#">
                                            {{ trans('system_messages.topics.season') }}
                                        </a>
                                    </div>
                                    <ul class="ae-main-menu">
                                        @foreach($seasonGroup[$teacherClass->id][$subject->id][$groupType->id] as $season)
                                            <li class="" season_id="{{ $season->id }}"><a><span>{{ $season->name }}</span></a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                @endforeach
                <!-- Season end -->

                <!-- Paragraphs start -->
                @foreach($teacherClasses as $teacherClass)
                    @foreach($classGroup[$teacherClass->id] as $subject)
                        @foreach($groupTypes as $groupType)
                            @foreach($seasonGroup[$teacherClass->id][$subject->id][$groupType->id] as $season)
                                <div class="col-md-6 paragraph" season_id="{{ $season->id }}" group_type_id="{{ $groupType->id }}" style="display: none">
                                    <table class="table table-bordered table-lg table-v2 table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('system_messages.topics.paraqraf') }}</th>
                                            <th>{{ trans('system_messages.topics.standart') }}</th>
                                            <th>{{ trans('system_messages.topics.time') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($season->paragraphs as $paragraph)
                                            <tr>
                                                <td>{{ $paragraph->name }}</td>
                                                <td>{{ $paragraph->standard }}</td>
                                                <td>{{ $paragraph->hour }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach
            <!-- Paragraphs end -->
            </div>
        </div>
    </div>
    <!-- Temayul end -->
@endsection

@section('css')
    <style>
        .ae-main-menu a{
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            width: 100%;
            cursor: pointer;
        }
        .ae-side-menu .aem-head{
            padding: 5px 5px;
        }
        .ae-side-menu-toggler{
            font-size: 15px;
            font-weight: bold;
            text-align: center;
            width: 100%;
        }
        .ver-col{
            padding-right: 0;
            padding-left: 0;
        }
    </style>
@endsection

@section('script')
    <script>
        $(".ver-col.classes li[class_id]").click(function () {
           let class_id = $(this).attr('class_id');

            $(".ver-col.classes li[class_id].active").removeClass('active');
            $(this).addClass('active');

            $(".ver-col.subjects[class_id]").hide();
            $(".ver-col.subjects[class_id='"+class_id+"']").show();
            //trigger
            $(".ver-col.subjects[class_id='"+class_id+"'] li[subject_id]:eq(0)").click();
        });

        $(".ver-col.subjects li[subject_id]").click(function () {
            let subject_id = $(this).attr('subject_id'),
                class_id = $(".ver-col.classes li[class_id].active").attr('class_id'),
                group_type_id = $(".group_types li a[group_type_id].clicked").attr('group_type_id');

            $(".ver-col.subjects li[subject_id].active").removeClass('active');
            $(this).addClass('active');

            $(".ver-col.seasons[class_id][subject_id]").hide();
            $(".ver-col.seasons[class_id='"+class_id+"'][subject_id='"+subject_id+"'][group_type_id='"+group_type_id+"']").show();
            //trigger
            $(".ver-col.seasons[class_id='"+class_id+"'][subject_id='"+subject_id+"'] li[season_id]:eq(0)").click();
        });

        $(".ver-col.seasons li[season_id]").click(function () {
            let season_id = $(this).attr('season_id'),
                group_type_id = $(".group_types li a[group_type_id].clicked").attr('group_type_id');

            $(".ver-col.seasons li[season_id].active").removeClass('active');
            $(this).addClass('active');

            $(".paragraph[season_id][group_type_id]").hide();
            $(".paragraph[season_id='"+season_id+"'][group_type_id='"+group_type_id+"']").show();
        });

        $(".group_types li a[group_type_id]").click(function () {
            $(".group_types li a[group_type_id].clicked").removeClass("clicked");
            $(this).addClass("clicked");

            $(".ver-col.subjects li[subject_id].active").click();
        });

        $(function () {
            $(".ver-col.classes li[class_id]:eq(0)").click();
        });
    </script>
@endsection