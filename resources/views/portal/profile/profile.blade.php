@extends('portal.base')
@section('content')
    <div class="element-wrapper">
        <div class="user-profile">
            <div class="up-head-w" style="background-image:url(img/profile_bg1.jpg)">
                <div class="up-social"><a href="#"><i class="os-icon os-icon-twitter"></i></a><a href="#"><i
                                class="os-icon os-icon-facebook"></i></a></div>
                <div class="up-main-info">
                    <div class="user-avatar-w">
                        <div class="user-avatar"><img alt=""
                                                      src="{{ asset('images/user/thumb/'.auth()->user()->thumb) }}">
                        </div>
                    </div>
                    @php $user_type ='messages.standarts_'.auth()->guard('portal')->user()->user_type.'_single'; @endphp
                    <h1 class="up-header">{{ auth()->user()->name." ".auth()->user()->surname}}</h1><h5
                            class="up-sub-header">{{ trans($user_type) }}</h5>
                </div>
                <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219"
                     preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF">
                        <path class="decor-path"
                              d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
                    </g>
                </svg>
            </div>
            <div class="up-controls">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="value-pair">
                            <div class="label">{{ trans('system_messages.profile.status') }}:</div>
                            <div class="value badge badge-pill badge-success">Online</div>
                        </div>
                        <div class="value-pair">
                            <div class="label">{{ trans('system_messages.profile.adding_date') }}:</div>
                            <div class="value badge badge-pill badge-success">{{ Carbon\Carbon::parse(auth()->guard('portal')->user()->enroll_date)->format('d-m-Y') }}</div>
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">

                        <a class="btn btn-primary btn-sm"
                           href="javascript:openModal('{{ route('portal_profile_change_password') }}',{},'{{ trans('system_messages.profile.changePasswordInfo') }}')"><i
                                    class="fa fa-unlock-alt">
                            </i><span>  {{ trans('system_messages.profile.changePasswordBtn') }}</span>
                        </a>

                        {{--<a class="btn btn-secondary btn-sm" href="">--}}
                        {{--<i class="os-icon os-icon-email-forward"></i>--}}
                        {{--<span>Send Message</span>--}}
                        {{--</a>--}}

                    </div>
                </div>
            </div>

            @if (auth()->guard('portal')->user()->user_type == "parent")
            <div class="up-contents">
                <div class="row" id="errors"></div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="element-wrapper" id="element_table">
                            <div class="element-actions d-none d-sm-block">
                                <a style="min-width: 100px;margin-right: 80px;" class="btn btn-primary btn-sm addSmsNumber" href="javascript:void(0)"><i
                                            class="fa fa-paper-plane"></i> <span>{{ trans('system_messages.profile.newBtn') }}</span></a>
                            </div>
                            <h6 class="element-header">{{ trans('system_messages.profile.sms_numbers') }}</h6>
                            <div class="">
                                <table class="table table-padded sms_number_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('system_messages.profile.number') }}</th>
                                        <th>{{ trans('system_messages.profile.operation') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($numbers as $number)
                                        <tr data-sms-number-id="{{ $number->id }}">
                                            <td></td>
                                            <td>{{ $number->number }}</td>
                                            <td>
                                                <a class="btn btn-primary btn-sm edit_data" href="javascript:void(0)"><i
                                                            class="fa fa-pencil"></i></a>
                                                <a class="btn btn-danger btn-sm delete_data" href="javascript:void(0)"><i
                                                            class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection
@section('css')
    <link href="{{ asset('assets/css/jquery-confirm.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.min.js') }}"></script>
    <script>

        $('.addSmsNumber').on('click',function(){
            let tbody = $('.sms_number_table tbody');
            if (tbody.find('tr[data-sms-number-id="0"]').length > 0) return;
            tbody.prepend('<tr data-sms-number-id="0">' +
                '<td></td>'+
                '<td><input type="text" name="sms_number" class="form-control"></td>'+
                '<td>' +
                '<a class="btn btn-success btn-sm save" href="javascript:void(0)"' +
                '><i' +
                '                                                    class="fa fa-save"></i></a>' +
                '<a class="btn btn-warning btn-sm cancel" href="javascript:void(0)"' +
                '><i' +
                '                                                    class="fa fa-times"></i></a>' +
                '</td>'+
                '</tr>');

            tbody.find('input[name="sms_number"]').inputmask("(999)-999-99-99");

            tbody.find("tr:first .cancel").click(function () {
                $(this).parents("tr:eq(0)").remove();
                order();
            });

            order();
        });


        $("tbody").on("click", ".save", function () {
            let tr = $(this).parents('tr:eq(0)'),
                sms_number_id = tr.attr('data-sms-number-id'),
                number = tr.find('input[name="sms_number"]').val(),
                _token = '{{ csrf_token() }}';

            if (true){
                $.post('{{ route('portal_profile_sms_number_add_edit') }}',{ id:sms_number_id , number : number , _token : _token},function(response){
                    if (response.status == "ok"){
                        saveTr(tr, response['data']);
                        order();
                    }
                    else{
                        $('#errors').html(response.errors);
                        setTimeout(function(){
                            $('#errors').html('');
                        },3000);
                    }
                });
            }
        });

        $("tbody").on("click", ".edit_data", function () {
            let tr = $(this).parents("tr:eq(0)"),
                number = tr.find('td:eq(1)').text(),
                html = tr.html();

            tr.html('<td></td>'+
                '<td><input type="text" name="sms_number" class="form-control" value="'+number+'"></td>'+
                '<td>' +
                '<a class="btn btn-success btn-sm save" href="javascript:void(0)"' +
                '><i' +
                '                                                    class="fa fa-save"></i></a>\n' +
                '<a class="btn btn-warning btn-sm cancel" href="javascript:void(0)"' +
                '><i' +
                '                                                    class="fa fa-times"></i></a>' +
                '</td>');

            tr.find('input[name="sms_number"]').inputmask("(999)-999-99-99");

            tr.find(".cancel").click(function () {
                tr.html(html);
                order();
            });
            order();
        });

        $("tbody").on("click", ".delete_data", function () {
            let tr = $(this).parents('tr:eq(0)'),
                id = tr.attr('data-sms-number-id'),
                _token = '{{ csrf_token() }}';

            $.confirm({
                title: 'Təsdiq',
                content: 'Silmək istədiyinizə əminsiniz?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    formSubmit : {
                        text : "Sİl",
                        btnClass : 'btn-green',
                        action:function(){
                            $.post('{{ route('portal_profile_sms_number_delete') }}',{ id:id , _token : _token},function(response){
                                if (response.status == "ok"){
                                    tr.remove();
                                    order();
                                }
                                else{
                                    console.log(response);
                                }
                            });
                        }
                    },
                    formCancel:{
                        text : "İmtİna et",
                        btnClass : 'btn-red',
                        action:function() {

                        }

                    }
                }
            });
        });

        function saveTr(tr, data) {

            tr.html('<td> </td>' +
                '<td>' + data['number'] + '</td>' +
                '<td><a class="btn btn-primary btn-sm edit_data" href="javascript:void(0)"><i\n' +
                '                                                            class="fa fa-pencil"></i></a>\n' +
                '                                                <a class="btn btn-danger btn-sm delete_data" href="javascript:void(0)"><i\n' +
                '                                                            class="fa fa-trash"></i></a></td>'
            );
            tr.attr("data-sms-number-id", data['id']);
            order();
        }


        function isValid(tr) {
            let name = tr.find("input[name='name']"),
                start_date = tr.find("input[name='start_date']"),
                status = true;

            start_date.css("border", "");

            if (start_date.val().length <= 0) {
                start_date.css("border", "1px dotted tomato");
                status = false;
            }
            return status;
        }

        function order() {
            $('.sms_number_table>tbody>tr').each(function(i){
                $(this).find('td:eq(0)').text(i+1);
            });
        }

        order();
    </script>
@endsection