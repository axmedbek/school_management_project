@extends('portal.base')
@section('content')
    <div class="row">
        @if ($nLesson == null && $cLesson == null)
            <div class="col-lg-7 col-md-5" id="lesson_monitor">
                <div class="padded-lg">
                    <div class="element-wrapper camera-element">
                        <div class="element-actions">
                            <div class="row">
                                <div class="col-md-2">
                                    <h6 class="element-header camera_name_text">{{ trans('system_messages.video_camera.title') }}</h6>
                                </div>
                                <div class="col-md-10">
                                </div>
                            </div>
                        </div>
                        <div class="element-box ring_time">
                            <h6 style="text-align: center;color: white;font-size: 25px;margin-top: 30%;">{{ trans('system_messages.video_camera.lesson_over') }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if ($cLesson == null)
                <div class="col-lg-7 col-md-5" id="lesson_monitor">
                    <div class="padded-lg">
                        <div class="element-wrapper camera-element">
                            <div class="element-actions">
                                <div class="row">
                                    <div class="col-md-2">
                                        <h6 class="element-header camera_name_text">{{ trans('system_messages.video_camera.title') }}</h6>
                                    </div>
                                    <div class="col-md-10">
                                    </div>
                                </div>
                            </div>
                            <div class="element-box ring_time">
                                <h6 style="text-align: center;color: white;font-size: 25px;margin-top: 30%;">Hazırda fasilədir</h6>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                @foreach($cLesson->class_time->corpus->class_times as $key => $class_time)
                    @php
                        $start_time = Carbon\Carbon::parse($class_time->start_time)->getTimestamp();
                        $end_time = Carbon\Carbon::parse($class_time->end_time)->getTimestamp();
                        $current_start_time = Carbon\Carbon::parse($cLesson->class_time->start_time)->getTimestamp();
                        $current_end_time = Carbon\Carbon::parse($cLesson->class_time->end_time)->getTimestamp();
                    @endphp
                    @if ($start_time == $current_start_time && $end_time == $current_end_time)
                        @php $currentLessonNumber = $key + 1; @endphp
                    @endif
                @endforeach
                @if($currentLessonNumber > 6)
                    <div class="col-lg-7 col-md-5" id="lesson_monitor">
                        <div class="padded-lg">
                            <div class="element-wrapper camera-element">
                                <div class="element-actions">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <h6 class="element-header camera_name_text">{{ trans('system_messages.video_camera.title') }}</h6>
                                        </div>
                                        <div class="col-md-10">
                                        </div>
                                    </div>
                                </div>
                                <div class="element-box ring_time">
                                    <h6 style="text-align: center;color: white;font-size: 25px;margin-top: 30%;">{{ trans('system_messages.video_camera.lesson_over') }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-7 col-md-5" id="lesson_monitor">
                        <div class="padded-lg">
                        @if ($cLesson)
                            <!--Camera-->
                                <div class="element-wrapper camera-element">
                                    <div class="element-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <h6 class="element-header camera_name_text">{{ trans('system_messages.video_camera.title') }}</h6>
                                            </div>
                                            <div class="col-md-10">
                                                <a class="btn btn-outline-info ml-2 btn-rounded re-connect-cam"
                                                   style="float: right;"
                                                   data-toggle="tooltip" data-placement="top" title=""
                                                   data-original-title="{{ trans('system_messages.video_camera.refresh_cam_info') }}">
                                                    <i class="fa fa-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="camera-shower" class="element-box" style="min-height: 394px;">
                                        <img src="http://{{ $camera['ip'] }}/goform/stream?cmd=get&channel=4"
                                             style="width:100%;height: 100%"/>
                                    </div>
                                </div>
                                <!--END - Camera -->
                            @else
                                <div class="element-wrapper camera-element">
                                    <div class="element-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <h6 class="element-header camera_name_text">{{ trans('system_messages.video_camera.title') }}</h6>
                                            </div>
                                            <div class="col-md-10">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="element-box ring_time">
                                        <h6 id="lesson_countdown"></h6>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-7" id="lesson_info">
                        @php $cLesson = !$cLesson ? $nLesson : $cLesson @endphp
                        <div class="padded-lg">
                            <!--START - Lesson info-->
                            <div class="element-wrapper">
                                <h6 class="element-header">{{ trans('system_messages.video_camera.lesson_info') }}</h6>
                                <div class="element-box" style="background-color: #323b58;">
                                    <div class="row" style="margin-bottom: -20px;">
                                        <div class="col-md-4">
                                            <div class="logged-user-w">
                                                <div class="logged-user-i" style="margin-top: -7px;margin-left: -30px;">
                                                    <div class="avatar-w" style="border: 1px solid #ffffff;">
                                                        <img alt=""
                                                             src="{{ asset('images/user/thumb/'.$cLesson->user->thumb) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8" style="margin-top: 14px;">
                                            <p style="font-size: 16px;color: white;">{{ $cLesson->user->fullname() }}</p>
                                            <p style="font-size: 12px;color: white;">{{ $nLesson ? trans('system_messages.video_camera.next_lesson') : trans('system_messages.video_camera.current_lesson') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="element-box" style="margin-top: -12px;border: 2px solid #8690b1;">
                                    <div class="row" style="margin-left: 2%;">
                                        <div class="col-md-12" id="lesson_info_param">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <ul class="nav nav-tabs smaller">
                                                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)">
                                                    <span class="lesson_params">
                                                        {{ trans('system_messages.video_camera.subject') }}
                                                    </span>
                                                                <br>
                                                                <span class="lesson_values badge">
                                                        {{ $cLesson->subject->name }}
                                                    </span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="margin-top: 29px;"><a class="nav-link"
                                                                                                          href="javascript:void(0)">
                                                    <span class="lesson_params">
                                                        {{ trans('system_messages.video_camera.room') }}
                                                    </span>
                                                                <br>
                                                                <span class="lesson_values badge" style="margin-top: 10px;">
                                                        {{ $cLesson->room->name }}
                                                    </span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    <ul class="nav nav-tabs smaller">
                                                        <li class="nav-item"><a class="nav-link"
                                                                                href="javascript:void(0)">
                                                    <span class="lesson_params">
                                                        {{ trans('system_messages.video_camera.lesson') }}
                                                    </span>
                                                                <br>
                                                                <span class="lesson_values badge">
                                                            {{ $currentLessonNumber }}
                                                        </span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="margin-top: 18px;">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a class="nav-link"
                                                                       href="javascript:void(0)" style="margin-top: 10px;">
                                                            <span class="lesson_params">
                                                                    {{ trans('system_messages.video_camera.time') }}
                                                            </span>
                                                                    </a>
                                                                </div>
                                                                <div class="col-md-12" style="margin-top: 6px;">
                                                                    <div class="btn-group-vertical">
                                                            <span style="background-color: #323b58;color: white;
                                                        border-radius: 5px;"
                                                                  class="btn btn-sm btn-white"
                                                                  data-toggle="tooltip" data-placement="top" title=""
                                                                  data-original-title="{{ trans('system_messages.video_camera.start_date') }}">
                                                                {{ trans('system_messages.video_camera.start') }} : {{ Carbon\Carbon::parse($cLesson->class_time->start_time)
                                                            ->format('H-i') }}
                                                            </span>
                                                                        <span style="
                                                            margin-left: 0px;
                                                            background-color: #323b58;
                                                            color:white;
                                                            border-radius: 5px;
                                                            text-align: left;"
                                                                              class="btn btn-sm btn-white"
                                                                              data-toggle="tooltip" data-placement="top"
                                                                              title=""
                                                                              data-original-title="{{ trans('system_messages.video_camera.end_date') }}">
                                                                <span style="margin-right: 20px;">{{ trans('system_messages.video_camera.end') }}
                                                                    :</span>
                                                                            {{ Carbon\Carbon::parse($cLesson->class_time->end_time)
                                                                        ->format('H-i') }}
                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END - Lesson info-->
                        </div>
                    </div>
                @endif
            @endif
        @endif
    </div>
@endsection
@section('css')
    <style>
        .element-wrapper .element-actions {
            float: none;
        }

        #lesson_info_param .nav-link {
            color: #3E4B5B;
            font-size: 12px;
        }

        #lesson_countdown {
            text-align: center;
            margin-top: 22%;
            padding: 12px;
            color: white;
            font-size: 24px;
            font-weight: bold;
        }

        .logged-user-w .avatar-w img {
            width: 68px;
            height: auto;
            border-radius: 50%;
        }

        .element-wrapper .element-header:after {
            background-color: #8690b1;
        }

        .nav-tabs .nav-link:after, .nav-tabs .nav-item .nav-link:after {
            background-color: #8690b1;
        }

        .lesson_params {
            font-size: 15px !important;
        }

        .lesson_values {
            font-style: italic;
            background-color: #323b58;
            color: white;
            padding: 6px;
        }

        .ring_time {
            background-image: url({{ asset('portal/img/ze.jpg') }});
            min-height: 394px;
            background-size: cover;
            opacity: 0.8;
        }

        #lesson_monitor .padded-lg{
            padding: 0;
        }
        #camera-shower {
            padding: 0;
        }
    </style>
@endsection
@section('script')
    <script>
{{--        console.log({{ (Carbon\Carbon::parse($cLesson->class_time->end_time)->getTimestamp() - Carbon\Carbon::now()->getTimestamp())*1000  }});--}}
        {{--setTimeout(function () {--}}
           {{--location.reload();--}}
         {{--},{{ (Carbon\Carbon::parse($cLesson->class_time->end_time)->getTimestamp() - Carbon\Carbon::now()->getTimestamp())*1000  }});--}}

        $('.re-connect-cam').click();
        $('.re-connect-cam').click();

        $('.re-connect-cam').on('click', function () {
            let camera = '{{ $camera['ip'] }}';
            let cam = window.open('http://admin:j272304j@'+ camera +'', '', "width=10,height=10,menubar=no,location=no,resizable=no,scrollbars=no,status=no,adresbar=no");
            setTimeout(function () {
                cam.close();
                $('#camera-shower').find('img').attr('src','http://'+camera+'/goform/stream?cmd=get&channel=4');
            }, 1500);
        });

        if ("{{ $nLesson }}" != 0) {
            // Set the date we're counting down to
            var countDownDate = Date.parse("{{ $timeUntilStartLesson }}");

// Update the count down every 1 second
            var x = setInterval(function () {

                // Get todays date and time
                var now = new Date().getTime();

                //console.log(countDownDate,now);
                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                //Time calculations for days, hours, minutes and seconds

                var days = Math.floor(distance / (1000 * 60 * 60 * 24)) != 0 ?
                    Math.floor(distance / (1000 * 60 * 60 * 24)) + " gün " :
                    "";
                //var hours =Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + " saat ";
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="demo"
                // document.getElementById("lesson_countdown").innerHTML = hours + " saat " + minutes + " dəqiqə " + seconds + " saniyə sonra dərs başlayacaq";

                // If the count down is over, write some text
                if (distance < 0) {
                    clearInterval(x);
                    location.reload();
                }
            }, 1000);
        }
        else{
            let time;
            if(parseInt('{{ $timeForEndingLesson }}') === 1){
                clearTimeout()
            }
            else{
                time = setTimeout(function () {
                    location.reload();
                },{{ $timeForEndingLesson }});
            }
        }

    </script>
@endsection
