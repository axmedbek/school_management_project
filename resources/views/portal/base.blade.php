<!DOCTYPE html>
<html>
<head>
    @include('portal.head')
    <style>
        /*.message-list ul li {*/
            /*width: 200px !important;*/
            /*padding: 0 !important;*/
        /*}*/
    </style>
</head>
<body class="with-content-panel full-screen menu-position-side menu-side-left">
<!-- BEGIN LOADING-->
<div class="text-center"
     style="position: fixed;z-index: 999999999;width: 100%;height: 100%;background-color: #c8c4c485;display: none"
     id="page_loading">
    <div class="sk-circle">
        <span>Gözləyin...</span>
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!-- END LOADING-->
<div class="all-wrapper with-side-panel solid-bg-all">
    <div class="layout-w">
        @include('portal.menu')
        <div class="content-w">
            <!--------------------
                  START - Top Bar
                  -------------------->
            <div class="top-bar color-scheme-dark">
                @if(auth()->guard('portal')->user()->user_type == "parent")
                @php
                    $students = \App\User::realData()->join('person_families','users.id','person_families.user_id')
                    ->where('parent_id',auth()->guard('portal')->user()->id)
                    ->select('users.*')
                    ->get();
                    $currentStudent = \App\User::find(\App\Library\Helper::getSelectedStudentForParent());
                    $letterGroupObj = $currentStudent->letter_groups()->where('year_id',\App\Library\YearDays::getCurrentYear()->id)->first();
                    $classLetterObjForCurrentStudent = $letterGroupObj ? $letterGroupObj->class_letter : "";
                @endphp
                <div class="fancy-selector-w">
                    <div class="fancy-selector-current" data-active-student="{{ \App\Library\Helper::getSelectedStudentForParent() }}">
                        @if($currentStudent)
                            <div class="fs-img"><img alt="" src="{{ asset('images/user/thumb/'.$currentStudent->thumb) }}"></div>
                            <div class="fs-main-info">
                                <div class="fs-name"><span>{{ $currentStudent->name." ".$currentStudent->surname }}</span></div>
                                <div class="fs-sub"><span>{{ trans('system_messages.jurnal.class') }} : {{ $classLetterObjForCurrentStudent ? ($classLetterObjForCurrentStudent->msk_class->name."/".$classLetterObjForCurrentStudent->name) : "" }}</span></div>
                            </div>
                        @else
                            <div class="fs-img"></div>
                            <div class="fs-main-info">
                                <div class="fs-name"><span>Heçbir şagird seçilməyib</span></div>
                                <div class="fs-sub"><span>-</span></div>
                            </div>
                        @endif
                        <div class="fs-selector-trigger"><i class="os-icon os-icon-arrow-down4"></i></div>
                    </div>
                    <div class="fancy-selector-options">
                        @foreach($students as $student)
                            @php
                                $letterGroupObj = $student->letter_groups()->where('year_id',\App\Library\YearDays::getCurrentYear()->id)->first();
                                $classLetterObj = $letterGroupObj ? $letterGroupObj->class_letter : "";
                            @endphp
                            <a href="{{ route('portal_change_student',$student->id) }}" style="text-decoration: none;">
                                <div class="fancy-selector-option" data-active-student="{{ $student->id }}">
                                    <div class="fs-img"><img alt="" src="{{ asset('images/user/thumb/'.$student->thumb) }}"></div>
                                    <div class="fs-main-info">
                                        <div class="fs-name"><span>{{ $student->name." ".$student->surname }}</span></div>
                                        <div class="fs-sub"><span>{{ trans('system_messages.jurnal.class') }} : {{ $classLetterObj ? ($classLetterObj->msk_class->name."/".$classLetterObj->name) : "" }}</span></div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
                @endif
                <!--------------------
                   START - Top Menu Controls
                   -------------------->
                <div class="top-menu-controls">
                {{--<div class="element-search autosuggest-search-activator"><input placeholder="Start typing to search..." type="text"></div>--}}
                <!--------------------
                       START - Messages Link in secondary top menu
                       -------------------->

                    <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right"><i
                                class="fa fa-bell-o"></i>
                        @php
                            $notifications = \App\Models\Notification::where('user_id',auth()->guard('portal')->user()->id)
                            ->where('seen',0)
                            ->orderByDesc('id')->get();
                        @endphp
                        <div class="new-messages-count specific_notify_count">{{ count($notifications) }}</div>
                        <div class="os-dropdown light message-list" style="right: 190px;">
                            <div class="icon-w"><i class="os-icon os-icon-zap"></i></div>
                            <div style="width: 250px;">
                                <h6 class="text-center"
                                    style="color: #777373;">{{ trans('system_messages.teacher_request.teacher_request_notify') }} </h6>
                                {{--@if ($smses == "[]") <h6  class="text-center" style="color: #777373;font-size: 14px;margin-top: 20px">{{ trans('system_messages.teacher_request.teacher_request_notify_info') }}</h6> @endif--}}
                                <p style="margin-bottom: 30px;"></p>
                            </div>
                            @if ($notifications == "[]")
                                <ul>
                                    <li>
                                        <h6  class="text-center" style="color: #777373;font-size: 14px;margin-top: 20px">
                                            {{ trans('system_messages.teacher_request.teacher_request_notify_info') }}
                                        </h6>
                                    </li>
                                </ul>
                            @else
                                <ul style="overflow-y: auto !important;
                                           overflow-x: hidden;width: 320px;height:300px;">
                                    @foreach($notifications as $notfication)
                                            <li notify_id="{{ $notfication->id }}"><a href="{{ $notfication->route }}">
                                                <div class="message-content">
                                                    <div class="row">
                                                        <div class="col-md-2" style="margin-top: 5px;">
                                                            <div class="message-from">
                                                                <i class="{{ $notfication->icon }}" style="color:{{ $notfication->color }}; /*#047bf8;*/"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10">
                                                            @php
                                                                if ($notfication->icon == "fa fa-picture-o"){
                                                                    $notificationContent = __('system_messages.notification.galery.image_added',['folder' => $notfication->content]);
                                                                }
                                                                else{
                                                                    $notificationContent = $notfication->content;
                                                                }
                                                            @endphp
                                                            <h6 class="message-from" style="font-size: 12px;width: 220px;white-space: pre-line;">{{ $notificationContent }}</h6>
                                                            <h6 class="message-title" style="font-size: 11px;color: #323b58;">{{ Carbon\Carbon::parse($notfication->date)->format('Y-m-d H:i:s') }}</h6>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    {{--<div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">--}}
                        {{--<i class="os-icon os-icon-mail-14"></i>--}}
                        {{--<div class="new-messages-count" style="margin-top: -4px">Upgrade</div>--}}
                    {{--</div>--}}
                    <!--------------------
                       END - Messages Link in secondary top menu
                       --------------------><!--------------------
                        START - Settings Link in secondary top menu
                        -------------------->
                    <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-left">
                        <i class="fa fa-language"></i>
                        <div class="os-dropdown">
                            <div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div>
                            <ul>
                                <li><a href="{{ route('portal_change_lang',['locale'=> 'az']) }}"><img
                                                src="{{ asset('portal/img/az.png') }}"> <span>Azərbaycan</span></a></li>
                                <li><a href="{{ route('portal_change_lang',['locale'=> 'en']) }}"><img
                                                src="{{ asset('portal/img/en.png') }}"> <span>English</span></a></li>
                                <li><a href="{{ route('portal_change_lang',['locale'=> 'ru']) }}"><img
                                                src="{{ asset('portal/img/ru.png') }}"> <span>Русский</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!--------------------
                       END - Settings Link in secondary top menu
                       --------------------><!--------------------
                        START - User avatar and menu in secondary top menu
                        -------------------->
                    <div class="logged-user-w">
                        <div class="logged-user-i">
                            <div class="avatar-w"><img alt=""
                                                       src="{{ asset(\App\Library\Standarts::$userThumbir . Auth::user()->thumb) }}">
                            </div>
                            <div class="logged-user-menu">
                                <div class="logged-user-avatar-info">
                                    <div class="avatar-w"><img alt=""
                                                               src="{{ asset(\App\Library\Standarts::$userThumbir . Auth::user()->thumb) }}">
                                    </div>
                                    <div class="logged-user-info-w">
                                        <div class="logged-user-name">{{ Auth::user()->fullname() }}</div>
                                        <div class="logged-user-role">{{ \App\Library\Standarts::$userTypesSingle[Auth::user()->user_type] }}</div>
                                    </div>
                                </div>
                                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                                <ul>
                                    <li><a href="{{ route('portal_profile') }}"><i
                                                    class="os-icon os-icon-user-male-circle2"></i><span>Profil</span></a>
                                    </li>
                                    <li><a href="{{ route('logout') }}"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--------------------
                       END - User avatar and menu in secondary top menu
                       -------------------->
                </div>
                <!--------------------
                   END - Top Menu Controls
                   -------------------->
            </div>
            <!--------------------
               END - Top Bar
               -------------------->

            <!--------------------
               START - Breadcrumbs
               -------------------->
            <ul class="breadcrumb">
                {{--<li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                <li class="breadcrumb-item"><a href="index-2.html">Products</a></li>--}}
                @if (\App\Library\Standarts::$portalModules[\Request::route()->getName()]['is_menu'])
                    <li class="breadcrumb-item">
                        <span>{{ trans('system_messages.modules.'.\App\Library\Standarts::$portalModules[\Request::route()->getName()]['route']) }}</span>
                    </li>
                @endif
            </ul>
            <!--------------------
               END - Breadcrumbs
               -------------------->
            <div class="content-i">
                <div class="content-box">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <div class="display-type"></div>
</div>
@include('portal.foot')
<script>

    $('.message-list ul li[notify_id]').each(function(){
       $(this).on('click',function(){
           let id = $(this).attr('notify_id');
           if(id > 0){
               $.post('{{ route('notify_seen') }}',{ id : id , _token : _token } , function(response){
                   if(response.status == 'ok'){

                   }
                   else{
                       console.error(response);
                   }
               });
           }
           else{
               console.error('Notification id is incorret')
           }
       });
    });
    socket.on('new message', function (response) {
        if (response.type == 'private') {
            toastr.success('<p style="color:white;">'+response.content+'</p><hr><span style="font-size:12px;">{{ Carbon\Carbon::now()->format('d-m-Y H:i')}}</span>','<h6 style="color:white;">'+response.name + ' ' + (response.surname != null ? response.surname : '') + ' ' + (response.middle_name != null ? response.middle_name : '') +'</h6><hr>',{
                timeOut : 3000,
                escapeHtml : false
            });
            // $('#toast-container').css('background-color','white');
            $('#toast-container .toast-success').css('cssText',"background-image : url('{{ asset('images/user/thumb/user.png') }}') !important;background-size: 40px 40px");
            $('#toast-container .toast-title').css('margin-left','20px');
            $('#toast-container .toast-message').css('margin-left','20px');

            var countElement = $('[data-user-id="'+response.user_id+'"]').find('.msgCount span');
            countElement.text(parseInt(countElement.text()) + 1);
        }
        else if(response.type == 'group'){
            toastr.success('<p style="color:white;">'+
                response.content+'</p>' +
                '<hr>' +
                '<span style="font-size:12px;">{{ Carbon\Carbon::now()->format('d-m-Y H:i')}}</span>',
                '<h6 style="color:white;">Qrup mesaji </h6><hr>',{
                timeOut : 3000,
                escapeHtml : false
            });
            // $('#toast-container').css('background-color','white');
            $('#toast-container .toast-success').css('cssText',"background-image : url('{{ asset('images/user/thumb/user.png') }}') !important;background-size: 40px 40px");
            $('#toast-container .toast-title').css('margin-left','20px');
            $('#toast-container .toast-message').css('margin-left','20px');
        }
    });

    socket.on('new notify',function (response) {
        $('.message-list ul').prepend('<li notify_id="'+response.id+'"><a href="'+response.route+'">' +
            '                                                <div class="message-content">' +
            '                                                    <div class="row">' +
            '                                                        <div class="col-md-2" style="margin-top: 5px;">' +
            '                                                            <div class="message-from">' +
            '                                                                <i class="'+response.icon+'" style="color:'+response.color+';"></i>' +
            '                                                            </div>' +
            '                                                        </div>' +
            '                                                        <div class="col-md-10">' +
            '                                                            <h6 class="message-from" style="font-size: 13px;">'+response.content+'</h6>' +
            '                                                            <h6 class="message-title" style="font-size: 11px;color: #323b58;">{{ Carbon\Carbon::now()->format('Y-m-d H:i')}}</h6>' +
            '                                                        </div>' +
            '                                                    </div>' +
            '                                                    <hr>' +
            '                                                </div>' +
            '                                            </a></li>');
        $('.specific_notify_count').text(parseInt($('.specific_notify_count').text()) + 1);
    });

    // set student active tab
    let active_student_tab = $('.fancy-selector-current').attr('data-active-student');
    $('div[data-active-student="'+active_student_tab+'"]').addClass('active');

</script>
</body>
</html>