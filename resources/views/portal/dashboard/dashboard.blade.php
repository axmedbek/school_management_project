@extends('portal.base')

@section('content')
    <div class="row">
        <div class="col-lg-7">
            <div class="padded-lg">
                <!--Dərs cədvəli-->
                <div class="element-wrapper" id="element_table">
                    <div class="element-actions d-none d-sm-block">
                        <a class="btn btn-primary btn-sm" href="#element_table"
                           onclick="openModal('{{ route('dashboard_table_modal') }}', {}, '{{ trans('system_messages.dashboard.lesson.lesson_table') }}', 'modal-lg')"><i
                                    class="fa fa-calendar"></i> <span>{{ trans('system_messages.dashboard.lesson.weekly') }}</span></a>
                    </div>
                    <h6 class="element-header">{{ trans('system_messages.dashboard.lesson.lesson_table') }} ( {{ \Illuminate\Support\Carbon::now()->format("d-m-Y") }}
                        )</h6>
                    <div class="">
                        <table class="table table-padded">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('system_messages.dashboard.lesson.subject') }}</th>
                                <th>{{ trans('system_messages.dashboard.lesson.class_room') }}</th>
                                <th>{{ trans('system_messages.dashboard.lesson.time') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lessons as $lesson)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $lesson['subject_name'] }}</td>
                                    <td>{{ $lesson['class_name'] . $lesson['letter_name'] }} <span
                                                class="smaller lighter">{{ $lesson['room_name'] }}</span></td>
                                    @php
                                        $hour = \Illuminate\Support\Carbon::parse($lesson['start_time']);
                                        $now = \Illuminate\Support\Carbon::now();
                                        $class = $hour->greaterThan($now) ? 'text-danger' : 'text-success';
                                    @endphp
                                    <td><span class="{{ $class }}">{{ $hour->format("H:i") }}</span>
                                        @if($lesson->substitution_lesson_id > 0)
                                            <i class="fa fa-random my-tooltips" style="color: #f8bc34"><span
                                                        class="my-tooltip-span">Əvəz edilib</span></i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--END - Dərs cədvəli-->

                <!--Elan & Tapşırıq cədvəli-->
                <div class="element-wrapper">
                    <div class="element-actions d-none d-sm-block" id="element_adert">
                        <a class="btn btn-primary btn-sm" href="#element_adert"
                           onclick="openModal('{{ route('dashboard_task_modal') }}', {}, '{{ trans('system_messages.dashboard.advert_task.advert_task_table') }}', 'modal-lg')"><i
                                    class="fa fa-file-text-o"></i><span> {{ trans('system_messages.dashboard.advert_task.show_all') }}</span></a>
                    </div>
                    <h6 class="element-header">{{ trans('system_messages.dashboard.advert_task.advert_task_table') }} </h6>
                    <div class="">
                        <table class="table table-padded" style="word-wrap:break-word;
              table-layout: fixed;">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('system_messages.dashboard.advert_task.date_of_event') }}</th>
                                <th>{{ trans('system_messages.dashboard.advert_task.content') }}</th>
                                <th>{{ trans('system_messages.dashboard.advert_task.status.name') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tasks as $task)
                                @php
                                    $today = \Carbon\Carbon::parse(\Carbon\Carbon::today()->format('Y-m-d'));
                                    $date_of_show = \Carbon\Carbon::parse($task['date_of_show']);

                                 $status = strtotime($task->date_of_event) > strtotime(date('d-m-Y')) ?
                                        2: (strtotime($task->date_of_event) == strtotime(date('d-m-Y')) ?
                                         0 : 1);
                                @endphp
                                @if($today->gte($date_of_show))
                                    <tr tr_id="{{ $task->id }}">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $task['date_of_event'] }}</td>
                                        <td>
                                            <div class="text-center content-btn">
                                                <i class="fa fa-file-text-o my-tooltips"><span
                                                            class="my-tooltip-span">{{ $task['content'] }}</span></i>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            @if($status == 2)
                                                <div class="status-pill green my-tooltips"><span class="my-tooltip-span">{{ trans('system_messages.dashboard.advert_task.status.waiting') }}</span>
                                                </div>
                                            @elseif($status == 1)
                                                <div class="status-pill red my-tooltips"><span
                                                            class="my-tooltip-span">{{ trans('system_messages.dashboard.advert_task.status.ending') }}</span></div>
                                            @else
                                                <div class="status-pill yellow my-tooltips"><span class="my-tooltip-span">{{ trans('system_messages.dashboard.advert_task.status.last_day') }}</span>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--END - Elan & Tapşırıq  cədvəli-->

                <!--Qeyri iş günləri  cədvəli-->
                <div class="element-wrapper">
                    <h6 class="element-header">{{ trans('system_messages.dashboard.holiday.holiday_table') }} </h6>
                    <div class="">
                        <table class="table table-padded" style="word-wrap:break-word;
              table-layout: fixed;">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('system_messages.dashboard.holiday.name') }}</th>
                                <th style="width: 200px;">{{ trans('system_messages.dashboard.holiday.date') }}</th>
                                <th>{{ trans('system_messages.dashboard.holiday.type') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($holidays as $holiday)
                                @php
                                    $holidayTypeName = "";
                                    switch ($holiday->type){
                                        case 1 : $holidayTypeName = 'Qeyri iş günü';
                                        break;
                                        case 2 : $holidayTypeName = 'Bayram günü';
                                        break;
                                        case 3 : $holidayTypeName = 'Matəm(hüzn) günü';
                                        break;
                                        case 4 : $holidayTypeName = 'Seçki günü';
                                        break;
                                        case 5 : $holidayTypeName = 'Tətil';
                                        break;
                                    }
                                @endphp
                                <tr data-id="{{ $holiday->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $holiday->name }}</td>
                                    <td>{{ $holiday->start_date }} / {{ $holiday->end_date }}</td>
                                    <td>{{ $holidayTypeName }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--END - Qeyri iş günləri cədvəli-->
            </div>
        </div>

        <div class="col-lg-5 b-l-lg">
            <div class="padded-lg">
                <!--START - Giriş/Çıxış-->
                <div class="element-wrapper">
                    <div class="element-actions d-none d-sm-block">
                        <a class="btn btn-primary btn-sm"
                           href="javascript:openModal('{{ route('dashboard_in_out_log_modal') }}', {}, '{{ trans('system_messages.dashboard.in_out.title') }}', 'modal-sm')"><i
                                    class="fa fa-clock-o"></i> <span>{{ trans('system_messages.dashboard.in_out.today') }}</span></a>
                    </div>
                    <h6 class="element-header">{{ trans('system_messages.dashboard.in_out.title') }}</h6>
                    <div class="element-box">
                        <table class="table table-stripeds">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><i class="fa fa-arrow-down text-success"></i> {{ trans('system_messages.dashboard.in_out.in') }}</th>
                                <th><i class="fa fa-arrow-up text-danger"></i> {{ trans('system_messages.dashboard.in_out.out') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($groupInOuts as $log)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ isset($log['In']) ? $log['In']->format('H:i') : '-' }}</td>
                                    <td>{{ isset($log['Out']) ? $log['Out']->format('H:i') : '-' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--END - Giriş/Çıxış-->
                <!--START - Zəng cədvəli-->
                <div class="element-wrapper">
                    <h6 class="element-header">{{ trans('system_messages.dashboard.ring_table.title') }}</h6>
                    <div class="element-box">
                        <!--START - tabs start-->
                        <div class="os-tabs-w" id="class_times_tabs">
                            <div class="os-tabs-controls">
                                <ul class="nav nav-tabs smaller">
                                    @foreach(\App\Models\Corpus::realData()->get() as $corpus)
                                        <li class="nav-item"><a class="nav-link {{ $loop->iteration==1?'active':'' }}"
                                                                data-toggle="tab"
                                                                href="#tab_corpus{{ $corpus->id }}">{{ $corpus->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="tab-content">
                                @foreach(\App\Models\Corpus::realData()->get() as $corpus)
                                    <div class="tab-pane {{ $loop->iteration==1?'active':'' }}"
                                         id="tab_corpus{{ $corpus->id }}">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{ trans('system_messages.dashboard.ring_table.start') }}</th>
                                                <th>{{ trans('system_messages.dashboard.ring_table.end') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($corpus->class_times as $classTime)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ \Illuminate\Support\Carbon::parse($classTime->start_time)->format("H:i") }}</td>
                                                    <td>{{ \Illuminate\Support\Carbon::parse($classTime->end_time)->format("H:i") }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!--end - tabs start-->
                    </div>
                </div>
                <!--END - Zəng cədvəli-->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        #class_times_tabs .nav-item {
            width: 100%;
            text-align: center;
        }

        .tooltip-inner {
            max-width: 350px;
            /* If max-width does not work, try using width instead */
            width: 350px;
        }
    </style>
@endsection

@section('script')
    <script>
        $('.content-btn').tooltip();
    </script>
@endsection