@extends('portal.base')
@section('content')
    <div class="row">
        <div class="col-sm-5 col-md-6 col-lg-7 folder-inside">

        </div>
        <div class="col-sm-7 col-md-6 col-lg-5 folders">
            <div class="padded-lg"><!--START - Projects list-->
                <div class="element-wrapper">
                    <h6 class="element-header">{{ trans('system_messages.galery.search') }}</h6>
                    <div class="input-group mb-3">
                        <input type="text" name="folder_search" class="form-control" placeholder="{{ trans('system_messages.galery.search_folder') }}"
                               aria-label="{{ trans('system_messages.galery.search_folder') }}" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary search_btn"><i class="fa fa-search"></i></button>
                        </div>
                        <div class="input-group-append">
                            <button class="btn btn-default clear_btn" style="background-color: #a0a0a0;">
                                <i class="fa fa-times" style="color: white;"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="projects-list">

                </div>
                <!--END - Projects list-->
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link href="{{ asset('/portal/bower_components/photo_swipe/dist/photoswipe.css') }}" rel="stylesheet">
    <link href="{{ asset('/portal/bower_components/photo_swipe/dist/default-skin/default-skin.css') }}"
          rel="stylesheet">
    <style>

        .activeFolder {
            border: 2px solid #047bf8
        }
        .element-wrapper{
            padding-bottom: 0px;
         }
    </style>
@endsection
@section('script')
    <script src="{{ asset('/portal/bower_components/photo_swipe/dist/photoswipe.min.js') }}"></script>
    <script src="{{ asset('/portal/bower_components/photo_swipe/dist/photoswipe-ui-default.min.js') }}"></script>
    <script>

        openPage('.folders .projects-list', '{{ route('portal_galery_folder_list') }}');

        $('.search_btn').on('click', function () {
            let search_input = $('[name="folder_search"]');
            if (search_input.val() != "") {
                openPage('.folders .projects-list', '{{ route('portal_galery_folder_list') }}', {'s': search_input.val()});
            }
        });

        $('.clear_btn').on('click',function(){
            let search_input = $('[name="folder_search"]');
            if (search_input.val() != "") {
                console.log(search_input.val());
                search_input.val("");
                openPage('.folders .projects-list', '{{ route('portal_galery_folder_list') }}');
            }
        });

    </script>
@endsection