@extends('portal.base')
@section('content')
    <div class="col-12" style="margin-top: 20px;">
        <div class="os-tabs-w">
            <div class="os-tabs-controls">
                <ul class="nav nav-tabs smaller">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_classes">{{ trans('system_messages.lesson_materials.lessons') }}</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_homework">{{ trans('system_messages.lesson_materials.homeworks') }}</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_classes">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{ trans('system_messages.lesson_materials.subject') }}</th>
                            <th>{{ trans('system_messages.lesson_materials.date') }}</th>
                            <th>{{ trans('system_messages.lesson_materials.text') }}</th>
                            <th>{{ trans('system_messages.lesson_materials.files') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lectures as $lecture)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $lecture->subject->name }}</td>
                                <td>{{ date('d-m-Y', strtotime($lecture->date)) }}</td>
                                <td>{{ $lecture->seasonParagraph['name'] }}</td>
                                <td class="text-center lesson_material_tooltip">
                                    @foreach($lecture->files as $file)
                                        <a class="tooltips"
                                           href="{{ asset(\App\Library\Standarts::$portalLectureFilesDir . $file->file_name) }}"
                                           download data-original-title="{{ $file->file_name }}"><i
                                                    class="fa fa-file"></i> </a>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="lecture_pagination text-center">
                        {{ $lectures ? $lectures->appends([
                            'active_tab' => 'tab_classes',
                            'lecture_page' => $lectures->currentPage(),
                            'homework_page' => $homeworks->currentPage(),
                        ])->links() : ""}}
                    </div>
                </div>
                <div class="tab-pane" id="tab_homework">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{ trans('system_messages.lesson_materials.subject') }}</th>
                            <th>{{ trans('system_messages.lesson_materials.date') }}</th>
                            <th>{{ trans('system_messages.lesson_materials.task') }}</th>
                            <th>{{ trans('system_messages.lesson_materials.files') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($homeworks as $homework)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $homework->subject->name }}</td>
                                <td>{{ date('d-m-Y', strtotime($homework->date)) }}</td>
                                <td>{{ $homework->task }}</td>
                                <td class="text-center lesson_material_tooltip">
                                    @foreach($homework->files as $file)
                                        <a class="tooltips"
                                           href="{{ asset(\App\Library\Standarts::$portalHomeworkFilesDir . $file->file_name) }}"
                                           download data-original-title="{{ $file->file_name }}"><i
                                                    class="fa fa-file"></i> </a>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="homework_pagination text-center">
                        {{ $homeworks ? $homeworks->appends([
                            'active_tab' => 'tab_homework',
                            'lecture_page' => $lectures->currentPage(),
                            'homework_page' => $homeworks->currentPage(),
                        ])->links() : "" }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .pagination {
            display: flex;
            padding-left: 0px;
            list-style: none;
            border-radius: 0.25rem;
            justify-content: center;
        }
    </style>
@endsection
@section('script')
    <script>
        $('[data-original-title]').tooltip();

        $('[href="#{{ $activeTab }}"]').click();
    </script>
@endsection