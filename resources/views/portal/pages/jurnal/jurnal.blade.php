<link rel="stylesheet" href="{{ asset('assets/css/jquery-confirm.css') }}">
<style>
    a:focus {
        outline: thin dotted;
    }

    a:active, a:hover {
        outline: 0;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    *, *:before, *:after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    a {
        color: #428bca;
        text-decoration: none;
    }

    a:hover, a:focus {
        color: #2a6496;
        text-decoration: underline;
    }

    a:focus {
        outline: thin dotted #333;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
    }

    @media print {
        * {
            text-shadow: none !important;
            color: #000 !important;
            background: transparent !important;
            box-shadow: none !important;
        }

        a, a:visited {
            text-decoration: underline;
        }

        thead {
            display: table-header-group;
        }

        tr {
            page-break-inside: avoid;
        }

        .table td, .table th {
            background-color: #fff !important;
        }

        .table {
            border-collapse: collapse !important;
        }
    }

    table {
        max-width: 100%;
        background-color: transparent;
    }

    th {
        text-align: left;
    }

    .table {
        width: 100%;
        margin-bottom: 20px;
    }

    .table thead > tr > th, .table tbody > tr > td {
        padding: 8px;
        line-height: 1.428571429;
        vertical-align: top;
        border-top: 1px solid #dddddd;
    }

    .table thead > tr > th {
        vertical-align: bottom;
        border-bottom: 2px solid #dddddd;
    }

    .table thead:first-child tr:first-child th {
        border-top: 0;
    }

    .table-striped > tbody > tr:nth-child(odd) > td {
        background-color: #f9f9f9;
    }

    a {
        color: #575757;
        text-decoration: none;
    }

    a:hover, a:focus {
        color: #7d7d7d;
        text-decoration: none;
    }

    .panel .table td, .panel .table th {
        padding: 6px 5px;
        border-top: 1px solid #eaedef;
        border-right: 1px solid #e0e4e8;
    }

    .panel .table thead > tr > th {
        border-bottom: 1px solid #e0e4e8;
    }

    .panel .table-striped > tbody > tr:nth-child(odd) > td {
        background-color: #fcfdfe;
    }

    .panel .table-striped > thead th {
        background: #fafbfc;
        border-right: 1px solid #e0e4e8;
    }

    .panel .table-striped > thead th:last-child {
        border-right: none;
    }

    .text-sm {
        font-size: 11px;
    }

    .m-b-none {
        margin-bottom: 0;
    }

    /*! CSS Used from: file:///C:/Users/tabdullayev/Desktop/pages-213/demo/html/assets/plugins/boostrapv3/css/bootstrap.min.css */
    input {
        margin: 0;
        font: inherit;
        color: inherit;
    }

    input::-moz-focus-inner {
        padding: 0;
        border: 0;
    }

    input {
        line-height: normal;
    }

    input[type=checkbox] {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 0;
    }

    @media print {
        *, :after, :before {
            color: #000 !important;
            text-shadow: none !important;
            background: 0 0 !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }
    }

    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    :after, :before {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    input {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }

    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
    }

    input[type=checkbox] {
        margin: 4px 0 0;
        margin-top: 1px \9;
        line-height: normal;
    }

    input[type=checkbox]:focus {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
    }

    .checkbox {
        position: relative;
        display: block;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .checkbox label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 0;
        font-weight: 400;
        cursor: pointer;
    }

    .checkbox input[type=checkbox] {
        position: absolute;
        margin-top: 4px \9;
        margin-left: -20px;
    }

    .checkbox + .checkbox {
        margin-top: -5px;
    }

    /*! CSS Used from: file:///C:/Users/tabdullayev/Desktop/pages-213/demo/html/pages/css/pages.css */
    label, input {
        font-size: 14px;
        font-weight: normal;
        line-height: 20px;
    }

    input[type="checkbox"] {
        margin-top: 1px 0 0;
        line-height: normal;
        cursor: pointer;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    input:focus, input[type="checkbox"]:focus {
        outline: none;
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .checkbox {
        margin-bottom: 10px;
        margin-top: 10px;
        padding-left: 0px;
    }

    .checkbox label {
        display: inline-block;
        cursor: pointer;
        position: relative;
        padding-left: 25px !important;
        font-size: 13px;
    }

    .checkbox label:before {
        content: "";
        display: inline-block;
        width: 17px;
        height: 17px;
        margin-right: 10px;
        position: absolute;
        left: 0px;
        background-color: #ffffff;
        border: 1px solid #d0d0d0;
    }

    .checkbox label {
        transition: border 0.2s linear 0s, color 0.2s linear 0s;
        white-space: nowrap;
    }

    .checkbox label:before {
        top: 1.4px;
        border-radius: 3px;
        transition: border 0.2s linear 0s, color 0.2s linear 0s;
    }

    .checkbox label::after {
        display: inline-block;
        width: 16px;
        height: 16px;
        position: absolute;
        left: 3%;
        top: 2%;
        font-size: 11px;
        transition: border 0.2s linear 0s, color 0.2s linear 0s;
    }

    .checkbox label:after {
        border-radius: 3px;
    }

    .checkbox input[type=checkbox] {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .checkbox input[type=checkbox]:checked + label:before {
        border-width: 8.5px;
    }

    .checkbox input[type=checkbox]:checked + label::after {
        font-family: 'FontAwesome';
        content: "\F00C";
        color: #fff;
    }

    .checkbox input[type="checkbox"]:focus + label {
        color: #2c2c2c;
    }

    .checkbox input[type="checkbox"]:focus + label:before {
        background-color: #e6e6e6;
    }

    .checkbox.check-success input[type=checkbox]:checked + label:before {
        border-color: #10cfbd;
    }

    .checkbox.check-success input[type=checkbox]:checked + label::after {
        color: #ffffff;
    }

    input, input:focus {
        -webkit-transition: none !important;
    }

    .checkbox {
        margin: 0px;
    }

    .fmc {
        cursor: pointer;
        font-size: 17px;
    }

    .fmc.fa-book {
        margin-right: 20%;
    }

    .fmc.fa-home {
        margin-left: 20%;
    }

    .table-striped thead th {
        text-align: center;
        vertical-align: middle !important;
    }

    .table-striped tbody td[user_id] {
        cursor: pointer;
        vertical-align: middle;
        text-align: center;
        font-size: 15px;
    }

    .no-wrap {
        white-space: nowrap;
    }
</style>

<div class="col-12 panel" style="padding: 0;overflow-y: auto">
    <table class="table table-striped m-b-none" id="all-students">
        <thead>
        <tr>
            <th style="width: 1%;position: sticky;left: 0;" rowspan="3">S/S</th>
            <th style="width: 1%;padding: 5px;text-align: center;position: sticky;left: 30px;" rowspan="3">
                <div class="checkbox check-success">
                    <input type="checkbox" value="1" id="checkbox2" onchange="chkAllOnOff($(this))">
                    <label for="checkbox2"></label>
                </div>
            </th>
            <th rowspan="3" style="position: sticky;left: 66px;">{{ trans('system_messages.jurnal.fullname') }}</th>
            <th style="text-align: center"
                colspan="{{ count($classDays) }}"> {{ \App\Library\Standarts::$months[(int)$month-1] }}
                / {{ $year }} </th>
        </tr>
        <tr>
            @foreach($classDays as $key => $classDay)
                <?php
                $classDay->date = \Carbon\Carbon::parse($classDay->date);
                ?>
                @if($classDay->type === 'day')
                    @if ($key != 0 && \Carbon\Carbon::parse($classDays[$key-1]->date) == \Carbon\Carbon::parse($classDays[$key]->date) &&
                    (strtotime($classDays[$key-1]->start_time) == strtotime($classDays[$key]->start_time) &&
                    strtotime($classDays[$key-1]->end_time) == strtotime($classDays[$key]->end_time)))
                    @else
                        <th>{{ $classDay->date->format("Y-m-d") }} <br/>
                            ({{ date("H:i", strtotime($classDay->start_time)) . '-' . date("H:i", strtotime($classDay->end_time)) }}
                            )
                        </th>
                    @endif
                @else
                    <th>{{ \App\Library\Standarts::$dayTypes[$classDay->type] }}</th>
                @endif
            @endforeach
        </tr>
        <tr>
            @foreach($classDays as $key => $classDay)
                @if ($key != 0 && $classDay->type === 'day' && \Carbon\Carbon::parse($classDays[$key-1]->date) == \Carbon\Carbon::parse($classDays[$key]->date) &&
                    (strtotime($classDays[$key-1]->start_time) == strtotime($classDays[$key]->start_time) &&
                    strtotime($classDays[$key-1]->end_time) == strtotime($classDays[$key]->end_time)))
                @else
                    <th style="min-width: 120px;" day="{{ (int)$classDay->date->format("d") }}"
                        hour="{{ $classDay->ct_id === null ? $classDay->type : $classDay->ct_id }}"><i
                                class="fa fa-book fmc tooltips" data-original-title="Mövzu əlavə et"></i> | <i
                                class="fa fa-home fmc tooltips" data-original-title="Ev tapşırığı əlavə et"></i></th>
                @endif
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            <tr user_id="{{ $student->user_id }}">
                <td style="position: sticky;left: 0;">{{ $loop->iteration }}</td>
                <td style="padding: 5px;text-align: center;position: sticky;left: 30px;">
                    <div class="checkbox check-success">
                        <input type="checkbox" value="1" id="checkbox{{ $student->user_id }}">
                        <label for="checkbox{{ $student->user_id }}"></label>
                    </div>
                </td>
                <td class="no-wrap" style="position: sticky;left: 66px;min-width: 210px;">{{ $student->f_name }} <i
                            class="fa fa-envelope-o fmc float-right tooltips" data-original-title="Mesaj göndər"></i>
                </td>
                @foreach($classDays as $key => $classDay)
                    @if ($key != 0 && $classDay->type === 'day' && \Carbon\Carbon::parse($classDays[$key-1]->date) == \Carbon\Carbon::parse($classDays[$key]->date) &&
                  (strtotime($classDays[$key-1]->start_time) == strtotime($classDays[$key]->start_time) &&
                  strtotime($classDays[$key-1]->end_time) == strtotime($classDays[$key]->end_time)))
                    @else
                        @php
                            $markDayType = $classDay->type == 'day' ? $classDay->ct_id : $classDay->type;
                            $userMark = isset($userMarksGroup[$student->user_id])
                                        && isset($userMarksGroup[$student->user_id][(int)$classDay->date->format("d")])
                                        && isset($userMarksGroup[$student->user_id][(int)$classDay->date->format("d")][$markDayType]) ? $userMarksGroup[$student->user_id][(int)$classDay->date->format("d")][$markDayType] : false;

                            $dayMark = $userMark ? ( in_array($classDay->type, ['yi', 'y']) ? $userMark->value : ( $userMark->qb==1 ? 'q' : $userMark->mskMark['name'] ) ) : '';
                        @endphp
                        <td user_id="{{ $student->user_id }}" day="{{ (int)$classDay->date->format("d") }}"
                            hour="{{ $classDay->ct_id === null ? $classDay->type : $classDay->ct_id  }}"> {{ $dayMark }} </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="col-12" style="margin-top: 20px;">
    <div class="os-tabs-w">
        <div class="os-tabs-controls">
            <ul class="nav nav-tabs smaller">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab"
                                        href="#tab_classes">{{ trans('system_messages.lesson_materials.lessons') }}</a>
                </li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab"
                                        href="#tab_homework">{{ trans('system_messages.lesson_materials.homeworks') }}</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_classes">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('system_messages.lesson_materials.date') }}</th>
                        <th>{{ trans('system_messages.lesson_materials.text') }}</th>
                        <th>{{ trans('system_messages.lesson_materials.files') }}</th>
                        <th>{{ trans('system_messages.jurnal.mark.delete') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lectures as $lecture)
                        <tr lecture_id="{{ $lecture->id }}">
                            <td class="text-center">{{ date('d-m-Y', strtotime($lecture->date)) }}</td>
                            <td class="text-center">{{ $lecture->seasonParagraph['name'] }}</td>
                            <td class="text-center">
                                @foreach($lecture->files as $file)
                                    <a class="tooltips"
                                       href="{{ asset(\App\Library\Standarts::$portalLectureFilesDir . $file->file_name) }}"
                                       download data-original-title="{{ $file->file_name }}"><i class="fa fa-file"></i>
                                    </a>
                                @endforeach
                            </td>
                            <td style="text-align: center;">
                                <a href="javascript:void(0)" class="btn btn-danger lecture_delete"
                                   style="text-align: center;"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_homework">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('system_messages.lesson_materials.date') }}</th>
                        <th>{{ trans('system_messages.lesson_materials.task') }}</th>
                        <th>{{ trans('system_messages.lesson_materials.files') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($homeworks as $homework)
                        <tr>
                            <td class="text-center">{{ date('d-m-Y', strtotime($homework->date)) }}</td>
                            <td class="text-center">{{ $homework->task }}</td>
                            <td class="text-center">
                                @foreach($homework->files as $file)
                                    <a class="tooltips"
                                       href="{{ asset(\App\Library\Standarts::$portalHomeworkFilesDir . $file->file_name) }}"
                                       download data-original-title="{{ $file->file_name }}"><i class="fa fa-file"></i>
                                    </a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/js/jquery-confirm.js') }}"></script>
<script>

    $('.lecture_delete').on('click', function () {
        let tr = $(this).parents('tr:eq(0)'),
            lecture_id = tr.attr('lecture_id');

        $.confirm({
            title: 'Təsdiq',
            content: 'Silmək istədiyinizə əminsiniz?',
            type: 'red',
            typeAnimated: true,
            buttons: {

                formSubmit: {
                    text: 'Bəli',
                    btnClass: 'btn-green',
                    action: function () {
                        $.post('{{ route('jurnal_lecture_delete') }}', {
                            lecture_id: lecture_id,
                            _token: _token
                        }, function (response) {
                            if (response.status == 'ok') {
                                tr.remove();
                            }
                            else {
                                console.error(response);
                            }
                        });
                    }
                },
                formCancel: {
                    text: 'Xeyr',
                    btnClass: 'btn-red',
                    action: function () {

                    }
                }
            }
        });
    });

    function chkAllOnOff(c) {
        if (c.is(':checked')) {
            $("#all-students tbody .checkbox input:not(:checked)").click();
        }
        else {
            $("#all-students tbody .checkbox input:checked").click();
        }
    }

    $("#all-students td[user_id]:not([hour='yi'],[hour='y'])").click(function () {
        let checkedUsers = [];
        $("#all-students tbody .checkbox input:checked").each(function () {
            checkedUsers.push($(this).parents('tr:eq(0)').attr('user_id'));
        });

        let data = {
            user_id: $(this).attr('user_id'),
            day: $(this).attr('day'),
            hour_id: $(this).attr('hour'),
            year: '{{ $year }}',
            month: '{{ (int)$month }}',
            checked_users: checkedUsers,
            class_letter_id: '{{ $classLetterId }}',
            subject_id: '{{ $subjectId }}'
        };

        openModal('{{ route('jurnal_student_marks_modal') }}', data, '{{ trans('system_messages.jurnal.mark.title') }}');
    });

    $("#all-students thead .fa-book").click(function () {
        let data = {
            day: $(this).parents('th:eq(0)').attr('day'),
            hour_id: $(this).parents('th:eq(0)').attr('hour'),
            year: '{{ $year }}',
            month: '{{ (int)$month }}',
            class_letter_id: '{{ $classLetterId }}',
            subject_id: '{{ $subjectId }}'
        };

        openModal('{{ route('jurnal_lecture_modal') }}', data, '{{ trans('system_messages.jurnal.new_lesson.title') }}', '');
    });

    $("#all-students thead .fa-home").click(function () {
        let data = {
            day: $(this).parents('th:eq(0)').attr('day'),
            hour_id: $(this).parents('th:eq(0)').attr('hour'),
            year: '{{ $year }}',
            month: '{{ (int)$month }}',
            class_letter_id: '{{ $classLetterId }}',
            subject_id: '{{ $subjectId }}'
        };

        openModal('{{ route('jurnal_homework_modal') }}', data, '{{ trans('system_messages.lesson_materials.homeworks') }}');
    });

    $("#all-students tbody .fa-envelope-o").click(function () {
        let data = {
            year: '{{ $year }}',
            month: '{{ (int)$month }}',
            class_letter_id: '{{ $classLetterId }}',
            subject_id: '{{ $subjectId }}',
            student_id: $(this).parents('tr:eq(0)').attr('user_id')
        };

        openModal('{{ route('jurnal_sms_modal') }}', data, '{{ trans('system_messages.jurnal.sms.title') }}');
    });

    $(".tooltips").tooltip();
</script>