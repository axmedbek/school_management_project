@foreach($folders as $folder)
    <div class="project-box" data-folder-id="{{ $folder['id'] }}" style="cursor: pointer;">
        <div class="project-head">
            <div class="project-title"><h5 style="font-size: 14px;">{{ $folder['name'] }}</h5></div>
        </div>
        <div class="project-info">
            <div class="row align-items-center">

                <div class="col-12 table" style="margin: 0;">
                    <div class="cell-image-list">
                        @foreach($folder->images->take(10) as $image)
                            @if(file_exists(public_path('option_files/'.$image['image'])))
                                <div class="cell-img" style="background-image: url('{{ route('portal_galery_image_169', ['url' => urlencode(public_path('option_files/'.$image['image'])) ]) }}')"></div>
                            @endif
                        @endforeach
                        @if($imageCount[$folder['name']] > 10)
                            <div class="cell-img-more" style="margin-right: 30px;">+ {{ $imageCount[$folder['name']] - 10 }} daha</div>
                        @else
                        @endif
                    </div>
                </div>

                {{--<div class="col-md-4 col-sm-4" style="margin-top: 20px;">
                    <button class="btn btn-primary btn-sm show_folder"
                            data-folder-id="{{ $folder['id'] }}" type="button"> Qovluğa bax
                    </button>
                </div>--}}
            </div>
        </div>
    </div>
@endforeach
<script>
    $('.project-box[data-folder-id]').click(function () {
        $('.activeFolder').removeClass('activeFolder');
        let folder_id = $(this).attr('data-folder-id');
        $(this).addClass('activeFolder');

        openPage('.folder-inside', '{{ route('portal_galery.get.folder') }}', {'folder_id': folder_id});
    });

    $('.project-box[data-folder-id]').eq(0).trigger('click');
</script>
<style>
    .project-head .project-users{
        flex: none !important;
    }
    .table .cell-image-list .cell-img-more{
        right: -35%;
    }
</style>
