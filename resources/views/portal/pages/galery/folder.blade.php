<div class="padded-lg">
    <div class="element-wrapper">
        <h6 class="element-header">{{ $folder['name'] }}</h6>
        <div class="element-box" id="images_tab_inside">
            <div class="">
                <div class="centered-header"><h6>{{ trans('system_messages.galery.images') }}</h6></div>
                <div class="row">
                    @php $i = 0;@endphp
                    @foreach($folder->images as $key => $image)
                        @php
                            if (file_exists(public_path('option_files/'.$image['image']))){
                                $imgUrl = asset('option_files/'.$image['image']);
                                $sizes = getimagesize(public_path('option_files/'.$image['image']));
                                $images[] = [
                                    'src' => $imgUrl,
                                    'w' => $sizes[0],
                                    'h' => $sizes[1],
                                    'url' => asset('option_files/'.$image['image'])
                                ];
                            }
                        @endphp
                        @if(file_exists(public_path('option_files/'.$image['image'])))
                            <div class="col-sm-6 col-xxxl-3">
                                <a class="element-box el-tablo">
                                    <img src="{{ $imgUrl }}" onclick="openImage({{ $i }})">
                                </a>
                            </div>
                            @php ++$i;@endphp
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<script>
    // build items array
    items = {!! json_encode($images) !!};

    function openImage(index) {
        let pswpElement = document.querySelectorAll('.pswp')[0];

        // define options (if needed)
        let options = {
            // optionName: 'option value'
            // for example:
            shareEl: true,
            index: index,
            shareButtons: [
                {
                    id: 'facebook',
                    label: 'Facebook\'da paylaş',
                    url: 'https://www.facebook.com/sharer/sharer.php?u="' + items[index].url + '"'
                },
                {id: 'download', label: 'Şəkili yüklə', url: items[index].url, download: true}
            ],
        };

        // Initializes and opens PhotoSwipe
        let gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    }

</script>

<style>
    #images_tab_inside .element-box {
        padding: 0;
    }

    #images_tab_inside .element-box img {
        width: 100%;
        height: 160px;
        cursor: pointer;
    }
</style>