<table class="table table-striped">
    <thead>
    <tr>
        <th>{{ trans('system_messages.student_attendance.date') }}</th>
        <th>{{ trans('system_messages.student_attendance.enter') }}</th>
        <th>{{ trans('system_messages.student_attendance.enter_status') }}</th>
        <th>{{ trans('system_messages.student_attendance.exit') }}</th>
        <th>{{ trans('system_messages.student_attendance.exit_status') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($inOutLogs as $inOutLog)
        <tr>
            <td>{{ \Carbon\Carbon::parse($inOutLog->date)->format('d-m-Y') }}</td>
            <td>{{ $inOutLog->in_out_type == 'In' ? \Carbon\Carbon::parse($inOutLog->date)->format('H:i') : '-' }}</td>
            <td>{{ $inOutLog->in_out_type == 'In' ? $inOutLog->sms_status : ''}}</td>
            <td>{{ $inOutLog->in_out_type == 'Out' ? \Carbon\Carbon::parse($inOutLog->date)->format('H:i') : '-' }}</td>
            <td>{{ $inOutLog->in_out_type == 'Out' ? $inOutLog->sms_status : ''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="custom-pagination">
    {!! $pagination !!}
</div>

<script>
    $('.custom-pagination a').click(function (e) {
        e.preventDefault();
        var page = $(this).text(),
            start_interval = $('[name="start_interval"]').val(),
            end_interval = $('[name="end_interval"]').val();
        $.get('{{ route('portal_attendance_get_table') }}',{
            start_interval: start_interval,
            end_interval: end_interval,
            page: page
        },function(response){
            $('.attendance_table').html(response);
        });
    });
</script>
