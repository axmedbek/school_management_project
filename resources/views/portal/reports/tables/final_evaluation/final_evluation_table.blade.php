<style>
    #final_eval_table thead tr th {
        border: 1px solid gray;
    }

    #final_eval_table tbody tr td {
        border: 1px solid gray;
    }

    #final_eval_table thead tr th, #final_eval_table tbody tr td {
        text-align: center;
    }

    #final_eval_table {
        overflow: auto;
    }
</style>
<table class="table table-striped table-responsive" id="final_eval_table">
    <thead>
    <tr>
        <th rowspan="3" style="position: sticky;left: 0;background-color: #c5d3ff;">No</th>
        <th rowspan="3" style="position: sticky;left: 30px;background-color: #c5d3ff;">{{ trans('system_messages.final_evaluation.subject') }}</th>
    </tr>
    <tr>
        @php $finalYearFounder=0; @endphp
        @foreach($evals as $key => $eval)
            @if ($eval->type == "yi")
                @php ++$finalYearFounder; @endphp;
                <th rowspan="2" style="min-width: 100px;">{{ $finalYearFounder > 1 ? $finalYearFounder > 2 ? "III" : "II" : "I" }} {{ trans('system_messages.final_evaluation.half_yearly') }}</th>
            @elseif($eval->type == "y")
                <th rowspan="2">{{ trans('system_messages.final_evaluation.yearly') }}</th>
            @else
                @if (Carbon\Carbon::parse($evals[$key+1]->date)->format('F') == Carbon\Carbon::parse($eval->date)->format('F'))
                    @if ( $eval->type == "bsq")
                        <th colspan="2"
                            style="width: 140px;">
                            {{ trans('messages.months.'.Carbon\Carbon::parse($eval->date)->format('F')) }}
                        </th>
                    @endif
                @else
                    <th>{{ trans('messages.months.'.Carbon\Carbon::parse($eval->date)->format('F')) }}</th>
                @endif
            @endif
        @endforeach
    </tr>
    <tr>
        @foreach($evals as $key => $eval)
            @if ($eval->type == "yi")
            @elseif($eval->type == "y")
            @else
                @if (Carbon\Carbon::parse($evals[$key+1]->date)->format('F') == Carbon\Carbon::parse($eval->date)->format('F'))
                    @if ( $eval->type == "bsq")
                        <th>BSQ </th>
                    @else
                        <th> KSQ </th>
                    @endif
                @else
                    <th> KSQ </th>
                @endif
            @endif
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($subjects as $keySubject => $subject)
        <tr>
            <td style="position: sticky;left:0;background-color: #c5d3ff;">{{ $keySubject + 1  }}</td>
            <td style="position: sticky;left: 30px;background-color: #c5d3ff;">{{ $subject->name }}</td>
            @foreach($evals as $keyEval => $eval)
                <td>{{ $subjectEvals[$subject->id][$eval->id] }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>