@extends('portal.base')
@section('content')
    <div class="col-md-8" style="margin-top: 20px;">
        <div class="row">
            <div class="col-sm-2 col-md-2 text-center" style="min-width: 100px;background-color: #ffffff;border-radius: 4px;border: 2px solid #dde2ec;">
                <label style="margin-top: 6px;">{{ trans('system_messages.final_evaluation.education_year') }} </label>
            </div>
            <div class="col-sm-6 col-md-6">
                <select class="form-control" name="years" id="years">
                    @foreach(\App\Models\Year::realData()->where('active', 1)->orderBy('id')->get() as $year)
                        <option value="{{ $year->id }}">{{ date("d-m-Y",strtotime($year->start_date)) . ' / ' . date("d-m-Y",strtotime($year->end_date)) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2 col-md-2">
                <button class="btn btn-primary show_result" style="margin-top: 2px;"><i class="fa fa-search"></i> {{ trans('system_messages.showBtn') }}</button>
            </div>
        </div>
    </div>
    <div class="col-md-12 final_evaluation_table" style="margin-top: 5%;">

    </div>
@endsection
@section('css')
    <link href="{{ asset('portal/bower_components/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">
@endsection
@section('script')
    <script src="{{ asset('/portal/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script>
        $('.show_result').on('click',function(){
            openPage('.final_evaluation_table', '{{ route('portal_final_evaluation_get_table') }}',{ 'year_id' : $('#years').val() });
        });
    </script>
@endsection