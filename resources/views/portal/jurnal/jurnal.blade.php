@extends('portal.base')

@section('content')
    <form>
        <div class="row">
            <div class="col-md-4" id="month-select-input">
                <div class="form-group">
                    <label for="">{{ trans('system_messages.jurnal.month') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.jurnal.month') }}" type="text" id="date" data-date-format="YYYY-MM-DD">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">{{ trans('system_messages.jurnal.subject') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.jurnal.select_month') }}" type="text" id="subject">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">{{ trans('system_messages.jurnal.class') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.jurnal.select_class') }}" type="text" id="class">
                </div>
            </div>
        </div>
    </form>
    <div id="data-inside">

    </div>
@endsection

@section('css')
    <link href="{{ asset('portal/bower_components/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">
@endsection

@section('script')
    <script src="{{ asset('/portal/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script>

        $.fn.datepicker.dates['en'] = {
            days: '{!! json_encode(trans('system_messages.weeks')) !!}',
            daysShort: {!! json_encode(trans('messages.daysMin')) !!},
            daysMin: {!! json_encode(trans('messages.daysMin')) !!},
            months: {!! json_encode(array_values(trans('messages.months'))) !!},
            monthsShort: {!! json_encode(trans('messages.monthsShort')) !!},
            today: "{{ trans('system_messages.dashboard.in_out.today') }}",
            clear: "{{ trans('system_messages.clear') }}",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 1
        };

        $("#subject").select2({
            placeholder: "Fənni seçin",
            allowClear:true,
            ajax: {
                url: '{{ route('ajax_subjects') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on("change", function () {
            $("#class").select2("val", null);
        });

        $("#class").select2({
            placeholder: "Sinifi seçin",
            minimumResultsForSearch: -1,
            ajax: {
                url: '{{ route('ajax_subject_classes') }}',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term,
                        subject_id: $("#subject").val(),
                        month: ($("#date").datepicker("getDate").getMonth()+1),
                        year: $("#date").datepicker("getDate").getFullYear()
                    };
                },
                results: function (data, page) {
                    return  data;
                },
                cache: true
            },
        }).on("change", function () {
            getPage();
        });

        $('#date').datepicker({
            format: 'MM / yyyy',
            viewMode: "months",
            minViewMode: "months",
            allowClear: false,
            autoclose: true,
        }).on('changeDate', function(e) {
            $("#class").select2("val", null);
        });
        $('#date').datepicker("setDate", new Date());

        function getPage() {
            if($("#class").val() > 0){
                let data = {
                    class_id: $("#class").val(),
                    letter_group_id : $('#class').select2('data')['letter_group'],
                    subject_id: $("#subject").val(),
                    month: $("#date").datepicker("getDate").getMonth()+1,
                    year: $("#date").datepicker("getDate").getFullYear()
                };

                openPage('#data-inside', '{{ route('portal_jurnal_students_page') }}', data);
            }
        }
    </script>
@endsection