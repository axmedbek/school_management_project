<link href="{{ asset('portal/bower_components/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">
<style>
    #modal{{ $modalId }} .project-box {
        background-color: #f2f4f8;
        margin-bottom: 10px;
    }

    #modal{{ $modalId }} .projects-list .project-info {
        padding: 20px 10px;
    }
</style>
<div class="col-md-12 errors"></div>

<div class="projects-list">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-padded task_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('system_messages.dashboard.advert_task.date_of_event') }}</th>
                    <th>{{ trans('system_messages.dashboard.advert_task.content') }}</th>
                    <th>{{ trans('system_messages.dashboard.advert_task.inserting_user') }}</th>
                    <th>{{ trans('system_messages.dashboard.advert_task.status.name') }}</th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th></th>
                    <th>
                        <input type="text" class="form-control" name="date_of_event" style="width: 100px;">
                    </th>
                    <th>
                        <div class="btn-group" style="width: 250px;">
                            <input type="text" class="form-control" name="content" style="width: 280px;">
                            <button id="clear-content-input" class="btn btn-default" title="{{ trans('system_messages.clear') }}"
                                    onclick="$(this).prev('input').val('');filterProcess();"
                            style="background-color: #bcbdbf;color: white;"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        {{--<input type="text" class="form-control" name="content">--}}
                    </th>
                    <th>
                        <input type="text" name="adding_user" class="form-control">
                    </th>
                    <th>
                        <input type="text" class="full-width" name="status">
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    @php
                        $today = \Carbon\Carbon::parse(\Carbon\Carbon::today()->format('Y-m-d'));
                        $date_of_show = \Carbon\Carbon::parse($task['date_of_show']);

                     $status = strtotime($task->date_of_event) > strtotime(date('d-m-Y')) ?
                            2: (strtotime($task->date_of_event) == strtotime(date('d-m-Y')) ?
                             0 : 1);
                    @endphp
                    @if($today->gte($date_of_show))
                        <tr tr_id="{{ $task->id }}">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $task['date_of_event'] }}</td>
                            <td style="word-break: break-all;">{{ $task['content'] }}</td>
                            <td>{{ $task->user->name." ".$task->user->surname }}</td>

                            <td class="text-center">
                                @if($status == 2)
                                    <div class="status-pill green my-tooltips"><span
                                                class="my-tooltip-span">{{ trans('system_messages.dashboard.advert_task.status.waiting') }}</span></div>
                                @elseif($status == 1)
                                    <div class="status-pill red my-tooltips"><span class="my-tooltip-span">{{ trans('system_messages.dashboard.advert_task.status.ending') }}</span>
                                    </div>
                                @else
                                    <div class="status-pill yellow my-tooltips"><span
                                                class="my-tooltip-span">{{ trans('system_messages.dashboard.advert_task.status.last_day') }}</span></div>
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="page-custom-div">
        {{ $tasks->links() }}
    </div>

</div>
<script src="{{ asset('/portal/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

<script>

    $.fn.datepicker.dates['en'] = {
        days: '{!! json_encode(trans('system_messages.weeks')) !!}',
        daysShort: {!! json_encode(trans('messages.daysMin')) !!},
        daysMin: {!! json_encode(trans('messages.daysMin')) !!},
        months: {!! json_encode(array_values(trans('messages.months'))) !!},
        monthsShort: {!! json_encode(trans('messages.monthsShort')) !!},
        today: "{{ trans('system_messages.dashboard.in_out.today') }}",
        clear: "{{ trans('system_messages.clear') }}",
        format: "mm/dd/yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 1
    };

    doPagination();

    $('.task_table').find('input[name="date_of_event"]').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        clearBtn: true,
    }).on('changeDate', function () {
        filterProcess();
    });

    $('input[name="content"]').on('keyup', function () {
        filterProcess();
    });


    $('.task_table').find('input[name="status"]').select2({
        allowClear: true,
        placeholder: '{{ trans('system_messages.dashboard.advert_task.select_status') }}',
        data: [
            {id: 0, text: '{{ trans('system_messages.dashboard.advert_task.status.all') }}'},
            {id: 1, text: '{{ trans('system_messages.dashboard.advert_task.status.waiting') }}'},
            {id: 2, text: '{{ trans('system_messages.dashboard.advert_task.status.last_day') }}'},
            {id: 3, text: '{{ trans('system_messages.dashboard.advert_task.status.ending') }}'},
        ]
    }).on('change', function () {
        filterProcess();
    });

    $('.task_table').find('input[name="adding_user"]').select2({
        allowClear: true,
        placeholder: '{{ trans('system_messages.dashboard.advert_task.inserting_user') }}',
        ajax: {
            url: '{{ route('dashboard.task.all.users') }}',
            dataType: 'json',
            data: function (word, page) {
                return {
                    ne: 'get_all_users',
                    q: word,
                };
            },
            results: function (data, page) {
                return data;
            },
            cache: true
        }
    }).on('change', function () {
        filterProcess();
    });

    orderTable();


    function doPagination() {
        $('.page-custom-div a').on('click', function (e) {
            e.preventDefault();
            var page = $(this).text();
            filterProcess(page);
        });
    }

    function filterProcess(page, startNum = 0) {
        var adding_user = $('input[name="adding_user"]').val(),
            date_of_event = $('input[name="date_of_event"]').val(),
            content = $('input[name="content"]').val(),
            status = $('input[name="status"]').val();

        $.get('{{ route('dashboard_task_filter') }}', {
            'status': status,
            'date_of_event': date_of_event,
            'adding_user': adding_user,
            'content': content,
            'page': page,
            '_token': _token
        }, function (response) {
            $('.task_table>tbody').html();
            $('.task_table>tbody').html(response.data);
            $('.page-custom-div').html(response.pagination);
            doPagination();
            console.log(response.pageCount);
            var startNum = response.pageCount == 1 ? 0 : {{ count($tasks) }} *
            $('.active').find('span').text() - {{ count($tasks) }};
            orderTable(startNum);
        });
    }

    function orderTable(startNum = 0) {
        $('.task_table>tbody>tr').each(function (i) {
            $(this).find('td:eq(0)').text(parseInt(i) + parseInt(startNum) + 1);
        });
    }

</script>