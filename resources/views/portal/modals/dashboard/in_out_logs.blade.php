<style>

</style>
<div class="col-md-12 errors"></div>

<div class="projects-list">
    <div class="row">
        <table class="table table-stripeds">
            <thead>
            <tr>
                <th>#</th>
                <th><i class="fa fa-arrow-down text-success"></i> {{ trans('system_messages.dashboard.in_out.in') }}</th>
                <th><i class="fa fa-arrow-up text-danger"></i> {{ trans('system_messages.dashboard.in_out.out') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($groupInOuts as $log)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ isset($log['In']) ? $log['In']->format('H:i') : '-' }}</td>
                    <td>{{ isset($log['Out']) ? $log['Out']->format('H:i') : '-' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>

</script>