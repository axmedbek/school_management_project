<style>
    #modal{{ $modalId }} .project-box{
        background-color: #f2f4f8;
        margin-bottom: 10px;
    }
    #modal{{ $modalId }} .projects-list .project-info{
        padding: 20px 10px;
    }
</style>
<div class="col-md-12 errors"></div>

<div class="projects-list">
    <div class="row">
        @foreach(\App\Library\Standarts::$weekDayNames as $key => $weekDayName)
            <div class="col-lg-6 col-md-12">
                <div class="project-box">
                    <div class="project-head">
                        <div class="project-title">
                            <h6>{{ trans('system_messages.weeks')[$key] }}</h6>
                        </div>
                    </div>
                    <div class="project-info">
                        <table class="table table-padded">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('system_messages.dashboard.lesson.subject') }}</th>
                                <th>{{ trans('system_messages.dashboard.lesson.class_room') }}</th>
                                <th>{{ trans('system_messages.dashboard.lesson.time') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(isset($lessonsGroupForWeekDay[$key+1]))
                                    @foreach($lessonsGroupForWeekDay[$key+1] as $lesson)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $lesson['subject_name'] }}</td>
                                            <td>{{ $lesson['class_name'] . $lesson['letter_name'] }} <span class="smaller lighter">{{ $lesson['room_name'] }}</span></td>
                                            @php
                                                $hour = \Illuminate\Support\Carbon::parse($lesson['start_time']);
                                                $now = \Illuminate\Support\Carbon::now();
                                                $class = $hour->greaterThan($now) ? 'text-danger' : 'text-success';
                                            @endphp
                                            <td>
                                                <span class="{{ $class }}">{{ $hour->format("H:i") }}</span>
                                                @if($lesson->substitution_lesson_id > 0)
                                                    <i class="fa fa-random my-tooltips" style="color: #f8bc34"><span class="my-tooltip-span">Əvəz edilib</span></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<script>

</script>