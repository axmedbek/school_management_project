<div class="col-md-12 errors"></div>
<form novalidate="novalidate" id="change_password_form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.profile.insert_current_password') }}</label>
                <input class="form-control" placeholder="{{ trans('system_messages.profile.insert_current_password') }}" type="password"
                       name="current_password" autocomplete="off">
                <div class="help-block form-text with-errors form-control-feedback">
                    <ul class="list-unstyled">
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.profile.insert_new_password') }}</label>
                <input class="form-control" placeholder="{{ trans('system_messages.profile.insert_new_password') }}" type="password" name="new_password"
                       autocomplete="off">
                <div class="help-block form-text with-errors form-control-feedback">
                    <ul class="list-unstyled">
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.profile.insert_new_password_confirm') }} </label>
                <input class="form-control" placeholder="{{ trans('system_messages.profile.insert_new_password_confirm') }}" type="password"
                       name="new_password_confirmation" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> {{ trans('system_messages.closeBtn') }}</button>
        <button class="btn btn-primary" type="submit"> {{ trans('system_messages.refreshBtn') }}</button>
    </div>
</form>

<script>
    $('#change_password_form').on('submit', function (e) {
        e.preventDefault();
        let formData = new FormData(this),
            current_password = $('input[name="current_password"]'),
            new_password = $('input[name="new_password"]');

        $(this).find('.has-danger').removeClass('has-danger');
        $(this).find('li').text("");
        formData.append('_token', _token);
        $.ajax({
            url: "{{ route('portal_profile_change_password_process') }}",
            type: "POST",
            data: formData,
            async: false,
            success: function (response) {
                if (response['status'] == 'error') {
                     if (response['errors'].current_password){
                         current_password.parent('div').addClass('has-danger');
                         current_password.parent('div').find('li').html(response['errors'].current_password);
                     }
                    if (response['errors'].new_password){
                        new_password.parent('div').addClass('has-danger');
                        new_password.parent('div').find('li').multiline(response['errors'].new_password);
                    }
                }
                else {
                    $("#change_password_form").closest('.modal').modal('hide');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $.fn.multiline = function(text){
        this.text(text);
        this.html(this.html().replace(',','<br/>'));
        return this;
    }

</script>