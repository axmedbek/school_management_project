<style>
    #marks-table tbody tr td{
        vertical-align: middle;
    }
</style>
<div class="col-md-12 errors"></div>
<form novalidate="novalidate" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.homework.teacher') }}</label>
                <input class="form-control" placeholder="{{ trans('system_messages.jurnal.homework.teacher') }}" value="{{ Auth::user()->fullname() }}" type="text" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.subject') }}</label>
                <input class="form-control" placeholder="{{ trans('system_messages.jurnal.subject') }}" value="{{ \App\Models\Subject::find($subjectId)['name'] }}" type="text" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.class') }}</label>
                <input class="form-control" placeholder="{{ trans('system_messages.jurnal.class') }}" value="{{ $classLetter->msk_class['name'].$classLetter['name'] }}" type="text" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.lesson_materials.date') }}</label>
                <input class="form-control date" placeholder="{{ trans('system_messages.lesson_materials.date') }}" type="text" value="{{ $date->format('d-m-Y') }}" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.new_lesson.season') }}</label>
                <input class="form-control seasons" placeholder="{{ trans('system_messages.jurnal.new_lesson.season') }}" type="text" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.new_lesson.topic') }}</label>
                <input class="form-control paragraphs" placeholder="{{ trans('system_messages.jurnal.new_lesson.topic') }}" name="paragraph_id" type="text" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.homework.file') }}</label>
                <input class="form-control files" multiple placeholder="{{ trans('system_messages.jurnal.homework.file') }}" type="file" name="files[]" accept="image/*,.doc,.docx,.xlsx,.xls,.ppt,.txt,.mp4,.mp3,.pdf">
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> {{ trans('system_messages.closeBtn') }}</button>
        <button class="btn btn-primary" type="submit"> {{ trans('system_messages.addBtn') }}</button>
    </div>
</form>

<script>
    $("#modal{{ $modalId }} .seasons").select2({
        placeholder: "Fəsil",
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('ajax_seasons') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    subject_id: {{ $subjectId }},
                    class_letter_id: {{ $classLetterId }}
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    }).on("change", function () {
        $("#modal{{ $modalId }} .paragraphs").select2("val", null);
    });

    $("#modal{{ $modalId }} .paragraphs").select2({
        placeholder: "Mövzu",
        minimumResultsForSearch: -1,
        ajax: {
            url: '{{ route('ajax_season_paragraphs') }}',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term,
                    season_id: $("#modal{{ $modalId }} .seasons").select2('val')
                };
            },
            results: function (data, page) {
                return  data;
            },
            cache: true
        },
    });

    $("#modal{{ $modalId }} .date").datepicker({
        format: 'dd-mm-yyyy',
        allowClear: false,
        autoclose: true,
    });

    $("#modal{{ $modalId }} form").validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {

        } else {
            e.preventDefault();
            saveModalData();
        }
    });
    
    function saveModalData()
    {
        let error = false,
            data = new FormData($("#modal{{ $modalId }} form")[0]);

        if (error) return;

        data.append('_token', _token);
        data.append('subject_id', {{ $subjectId }});
        data.append('class_letter_id', {{ $classLetterId }});
        data.append('year', {{ $year }});
        data.append('month', {{ $month }});
        data.append('day', {{ $day }});
        data.append('hour_id', {{ $hourId }});

        $.ajax({
            url: "{{ route('ajax_save_lecture_action') }}",
            type: "POST",
            data: data,
            async: false,
            success: function (response) {
                if(response['status'] == 'error') $("#modal{{ $modalId }}").find(".errors").html(response['errors']);
                else {
                    $("#modal{{ $modalId }}").modal('hide');
                    getPage();
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>