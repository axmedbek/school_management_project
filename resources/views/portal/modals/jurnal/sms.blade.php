<style>
    #marks-table tbody tr td{
        vertical-align: middle;
    }
    #modal{{ $modalId }} .checkbox label::after{
        left: 11%;
    }
</style>
<div class="col-md-12 errors"></div>
<form novalidate="novalidate">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.teacher_request.student') }}</label>
                <input class="form-control" placeholder="Şagird" value="{{ $student->fullname() }}" type="text" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.sms.sms') }}</label>
                <div class="checkbox check-success">
                    <input type="checkbox" value="1" id="checkboxAS" name="sms">
                    <label for="checkboxAS"></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.lesson_materials.text') }}</label>
                <textarea class="form-control" placeholder="{{ trans('system_messages.lesson_materials.text') }}" name="text" required></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for=""> {{ trans('system_messages.jurnal.homework.file') }}</label>
                <input class="form-control files" multiple placeholder="{{ trans('system_messages.jurnal.homework.file') }}" type="file" name="files[]" accept="image/*,.doc,.docx,.xlsx,.xls,.ppt,.txt,.mp4,.mp3,.pdf">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> {{ trans('system_messages.closeBtn') }}</button>
        <button class="btn btn-primary" type="submit"> {{ trans('system_messages.addBtn') }}</button>
    </div>
</form>

<script>
    $("#modal{{ $modalId }} form").validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {

        } else {
            e.preventDefault();
            saveModalData();
        }
    });
    
    function saveModalData()
    {
        let error = false,
            data = new FormData($("#modal{{ $modalId }} form")[0]);

        if (error) return;

        data.append('_token', _token);
        data.append('subject_id', {{ $subjectId }});
        data.append('class_letter_id', {{ $classLetterId }});
        data.append('year', {{ $year }});
        data.append('month', {{ $month }});
        data.append('student_id', {{ $studentId }});

        $.ajax({
            url: "{{ route('ajax_save_sms_action') }}",
            type: "POST",
            data: data,
            async: false,
            success: function (response) {
                if(response['status'] == 'error') $("#modal{{ $modalId }}").find(".errors").html(response['errors']);
                else {
                    $("#modal{{ $modalId }}").modal('hide');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>