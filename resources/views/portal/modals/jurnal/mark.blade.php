<style>
    #marks-table tbody tr td{
        vertical-align: middle;
    }
    #marks-table .mark{
        margin-bottom: 5px;
    }
</style>
<div class="col-md-12 errors"></div>
<table class="table table-lightborder" id="marks-table">
    <thead>
        <tr>
            <th>{{ trans('system_messages.jurnal.mark.mark_type') }}</th>
            <th>{{ trans('system_messages.jurnal.mark.marks') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($markTypes as $markType)
            <tr>
                <td class="nowrap">{{ $markType->name }}</td>
                <td>
                    <div class="">
                        @foreach($markType->marks as $mark)
                            <a class="mr-2 btn btn-outline-success mark" mark="{{ $mark->id }}"><span>{{ $mark->name }}</span></a>
                        @endforeach
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="col-md-12">
    <a class="mr-2 btn btn-outline-warning mark" mark="qb"><span>{{ trans('system_messages.jurnal.mark.qb') }}</span></a>
    <a class="mr-2 btn btn-outline-danger mark" mark="del"><span>{{ trans('system_messages.jurnal.mark.delete') }}</span></a>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary" data-dismiss="modal" type="button"> {{ trans('system_messages.closeBtn') }}</button>
    <button class="btn btn-primary" type="button" onclick="saveModalData()"> {{ trans('system_messages.makeBtn') }}</button>
</div>

<script>
    $("#modal{{ $modalId }}").find(".mark").click(function () {
        $(".mark.active").removeClass("active");
        $(this).addClass("active");
    });
    
    function saveModalData()
    {
        let error = false,
            data = new FormData(),
            mark = $("#modal{{ $modalId }}").find(".mark.active");

        /*if(mark.length == 0){
            error = true;
        }*/

        if (error) return;

        data.append('_token', _token);
        data.append('mark', (typeof mark.attr("mark")==='undefined'?'':mark.attr("mark")) );
        data.append('subject_id', {{ $subjectId }});
        data.append('class_letter_id', {{ $classLetterId }});
        data.append('year', {{ $year }});
        data.append('month', {{ $month }});
        data.append('day', {{ $day }});
        data.append('user_id', {{ $userId }});
        data.append('hour_id', '{{ $hourId }}');

        let cc = {!! json_encode($checkedUsers) !!};
        for(var n in cc)
            data.append('checked_users[]', cc[n]);

        $.ajax({
            url: "{{ route('ajax_save_mark_action') }}",
            type: "POST",
            data: data,
            async: false,
            success: function (response) {
                if(response['status'] == 'error') $("#modal{{ $modalId }}").find(".errors").html(response['errors']);
                else {
                    $("#modal{{ $modalId }}").modal('hide');
                    getPage();
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>