<style>
    #modal{{ $modalId }} .project-box {
        background-color: #f2f4f8;
        margin-bottom: 10px;
    }

    #modal{{ $modalId }} .projects-list .project-info {
        padding: 20px 10px;
    }

</style>
<div class="col-md-12 errors"></div>

<div class="projects-list">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-padded task_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>A.S.A</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key => $user)
                    @if ($user->user_type != "user")
                        <tr>
                            <td>{{ $key+1  }}</td>
                            <td>{{ $user->fullname() }}</td>
                            <td>
                                @switch($user->user_type)
                                    @case('teacher')
                                    {{ $user->teacher_subjects() ? $user->teacher_subjects()->first()->name." müəllimi" : ' Müəllim' }}
                                    @break
                                    @case('student')
                                    Şagird
                                    @break;
                                    @case('parent')
                                    Valideyn
                                    @break;
                                    @case('user')
                                    @break;
                                @endswitch
                            </td>
                        </tr>
                    @else
                        @php $adminUser = \App\User::realData()->find($user->id) @endphp
                        @if ($adminUser->group->getModulePriv('admin_chat') >= 2)
                            <tr>
                                <td>{{ $key+1  }}</td>
                                <td>{{ $adminUser->fullname() }}</td>
                                <td></td>
                            </tr>
                        @endif
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
