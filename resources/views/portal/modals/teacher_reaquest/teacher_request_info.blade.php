<link href="{{ asset('portal/bower_components/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet">
<style>
    #modal{{ $modalId }} .project-box {
        background-color: #f2f4f8;
        margin-bottom: 10px;
    }

    #modal{{ $modalId }} .projects-list .project-info {
        padding: 20px 10px;
    }
</style>
<div class="col-md-12 errors"></div>
@if ($sms)
    <div class="projects-list">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for=""> {{ trans('system_messages.teacher_request.student') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.teacher_request.student') }}" value="{{ $sms->student->name." ".$sms->student->surname." ".$sms->student->middle_name }}" type="text" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for=""> {{ trans('system_messages.teacher_request.teacher') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.teacher_request.teacher') }}" value="{{ $sms->teacher->name." ".$sms->teacher->surname." ".$sms->teacher->middle_name }}" type="text" readonly>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for=""> {{ trans('system_messages.dashboard.lesson.subject') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.dashboard.lesson.subject') }}" value="{{ $sms->subject->name }}" type="text" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for=""> {{ trans('system_messages.lesson_materials.date') }}</label>
                    <input class="form-control" placeholder="{{ trans('system_messages.lesson_materials.date') }}" value="{{ Carbon\Carbon::parse($sms->created_at)->format('d-m-Y') }}" type="text" readonly>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for=""> {{ trans('system_messages.teacher_request.text') }}</label>
                    <textarea class="form-control" placeholder="{{ trans('system_messages.teacher_request.text') }}" name="text" style="height: 250px;" required disabled>{{ $sms->text }}</textarea>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-12 text-right">
                <button class="btn btn-secondary" data-dismiss="modal" type="button"> {{ trans('system_messages.closeBtn') }}</button>
            </div>
        </div>
    </div>
@endif
<script src="{{ asset('/portal/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>