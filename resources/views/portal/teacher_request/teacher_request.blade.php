@extends('portal.base')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="padded-lg">
                <!--Dərs cədvəli-->
                <div class="element-wrapper" id="element_table">
                    <h6 class="element-header">{{ trans('system_messages.teacher_request.title') }}</h6>
                    <div class="">
                        <table class="table table-padded" id="teacher_request_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('system_messages.teacher_request.request_date') }}</th>
                                <th>{{ trans('system_messages.teacher_request.teacher') }}</th>
                                <th>{{ trans('system_messages.teacher_request.student') }}</th>
                                <th>{{ trans('system_messages.jurnal.homework.file') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($smses as $sms)
                                <tr data-toggle="tooltip" title="{{ trans('system_messages.teacher_request.info') }}"
                                    data-sms-id="{{ $sms->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon\Carbon::parse($sms->created_at)->format('d-m-Y') }}</td>
                                    <td>{{ $sms->teacher->name." ".$sms->teacher->surname." ".$sms->teacher->middle_name }}</td>
                                    <td>{{ $sms->student->name." ".$sms->student->surname." ".$sms->student->middle_name }}</td>
                                    <td>
                                        @foreach($sms->files as $file)
                                            <a href="{{ asset(\App\Library\Standarts::$optionFilesDir.$file->file_name) }}" download><i class="fa fa-download"></i></a>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $smses->links() }}
                </div>
                <!--END - Dərs cədvəli-->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>

        #teacher_request_table tbody tr td {
            border: 1px solid #b5b8bb;
        }

        .tooltip-inner {
            background-color: #3b7bf8;
            max-width: 100%;
            /* If max-width does not work, try using width instead */
            width: 100%;
        }

        .bs-tooltip-top .arrow::before, .bs-tooltip-auto[x-placement^="top"] .arrow::before {
            border-top-color: #3b7bf8;
        }
    </style>
@endsection

@section('script')
    <script>

        if ({{ $sms_id }}) {
            let sms_id = parseInt({{ $sms_id }});
            openModal('{{ route('portal_teacher_request_info') }}', {'sms_id': sms_id}, '{{ trans('system_messages.teacher_request.teacher_custom_request') }}', 'modal-lg');
        }

        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="tooltip"]').css('cursor', 'pointer');
        $('[data-toggle="tooltip"]').on('click', function () {
            let sms_id = $(this).attr('data-sms-id');
            openModal('{{ route('portal_teacher_request_info') }}', {'sms_id': sms_id}, '{{ trans('system_messages.teacher_request.teacher_custom_request') }}', 'modal-lg');
        });
    </script>
@endsection