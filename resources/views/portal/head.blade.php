<title>Portal</title>
<meta charset="utf-8">
<meta content="ie=edge" http-equiv="x-ua-compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">

<link href="{{ asset('portal/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('portal/bower_components/dropzone/dist/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('portal/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('portal/bower_components/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ asset('portal/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" rel="stylesheet">
<link href="{{ asset('portal/bower_components/slick-carousel/slick/slick.css') }}" rel="stylesheet">
<link href="{{ asset('portal/icon_fonts_assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('portal/css/maince5a.css?version=6') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@yield('css')