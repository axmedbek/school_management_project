<?php
/**
 * Created by PhpStorm.
 * User: tabdullayev
 * Date: 2018-07-06
 * Time: 10:05
 */

namespace App\Library;


use App\Models\Lesson;
use App\Models\LessonDay;
use App\Models\Mark;
use App\Models\SubstitutionLesson;
use App\Models\Year;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LessonDaysFixer
{
    private $tenant_id = false;
    private $yearId = false;
    private $years = false;

    public function __construct($yearId = false)
    {
        $this->tenant_id = Auth::user()->tenant_id;
        $this->yearId = $yearId;
    }

    public function fixYearDays()
    {
        foreach($this->getYears() as $year){
            $this->clearYearData($year->id);
            $periods = $year->periods;
            $periodCount = count($periods);
            $currentPeriod = 0;

            foreach($periods as $period){
                $currentPeriod++;
                $isLastPeriod = $currentPeriod === $periodCount ? true : false;
                $startDate = Carbon::parse($period->start_date)->firstOfMonth();
                $realEndDate = Carbon::parse($period->end_date);
                $endDate = (clone $realEndDate)->firstOfMonth();


                for($date = $startDate; $date->lte($endDate); $date->addMonth()){
                    $endOfPeriod = $date->eq($endDate);
                    $sDate = $endOfPeriod ? $realEndDate : (clone $date)->lastOfMonth();
                    $this->insertToDays($sDate, $year->id, 'ksq');
                    if($endOfPeriod){
                        $this->insertToDays($sDate, $year->id, 'bsq');
                        $this->insertToDays($sDate, $year->id, 'yi');
                    }
                    if($endOfPeriod && $isLastPeriod) $this->insertToDays($sDate, $year->id, 'y');
                }
            }
        }
    }

    private function clearYearData($yearId)
    {
        LessonDay::realData()->where('year_id', $yearId)
            ->whereIn('type', ['ksq', 'bsq', 'yi', 'y'])
            ->delete();
    }

    private function insertToDays($date = null, $yearId = null, $type = null, $lessonId = null, $letterGroupId = null, $userId = null, $substitutionLessonId = null)
    {
        $lessonDay = new LessonDay();
        $lessonDay->tenant_id = $this->tenant_id;
        $lessonDay->order = ($type=='ksq'?1:($type=='bsq'?2:($type=='yi'?3:($type=='y'?4:0))));
        $lessonDay->date = $date;
        $lessonDay->year_id = $yearId;
        $lessonDay->type = $type;
        $lessonDay->lesson_id = $lessonId;
        $lessonDay->letter_group_id = $letterGroupId;
        $lessonDay->user_id = $userId;
        $lessonDay->substitution_lesson_id = $substitutionLessonId;
        $lessonDay->save();
    }

    // get years for letter group
    private function getYears()
    {
        if($this->years) return $this->years;

        $this->years = Year::realData();
        if($this->yearId) $this->years->where('id', $this->yearId);

        $this->years = $this->years->get();

        return ($this->years);
    }


    // delete lesson days
    private function clearLessonDays($yearId, $groupId = null, Carbon $startDate = null, Carbon $endDate = null){
        // find lesson days for year id
        $lessonDays = LessonDay::realData()
            ->where('type', 'day')
            ->where('year_id', $yearId);

        // if there is a $groupId then find lessons for $groupId
        if($groupId) $lessonDays->where('letter_group_id', $groupId);
        // if there is a $startDate then find lessons for $startDate
        if($startDate) $lessonDays->where('date', '>=', $startDate->format("Y-m-d"));
        // if there is a $endDate then find lessons for $endDate
        if($endDate) $lessonDays->where('date', '<=', $endDate->format("Y-m-d"));

        // delete this lesson days
        $lessonDays->delete();

    }

    public function fixLessonDays($groupId = null, Carbon $startDate = null, Carbon $endDate = null)
    {
//        dd($this->getYears());

        foreach ($this->getYears() as $year){
            // find year periods
            $periods = $year->periods;

            // delete all lesson days
            $this->clearLessonDays($year->id, $groupId, $startDate, $endDate);

            foreach($periods as $period){
                // period 1 start date - period 2 start date - ......
                $pStartDate = Carbon::parse($period->start_date);
                // period 1 end date - period 2 end date - ......
                $pEndDate = Carbon::parse($period->end_date);

                // if $startDate is less than periodEndDate
                if($startDate && !$startDate->lte($pEndDate)){ continue; }
                // if $endDate is great than $pStartDate
                if($endDate && !$endDate->gte($pStartDate)){ continue; }

                $realStart = $startDate ? max($startDate, $pStartDate) : $pStartDate;
                $realEnd = $endDate ? min($endDate, $pEndDate) : $pEndDate;

                // get substitutions that lessons for date
                $substitutions = $this->getSubstitutions($groupId, $realStart, $realEnd);

                $lessonDays = Lesson::realData()
                    ->join('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                    ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                    ->where('lessons.year_id', $year->id)
//                    ->whereRaw("( (CAST(lessons.created_at as DATE) <= '{$realEnd->format("Y-m-d")}'
//                    AND (lessons.deleted_at >= '{$realStart->format("Y-m-d")}' OR lessons.deleted_at is null))
//                    OR (lessons.created_at >= '{$realEnd->format("Y-m-d")}'
//                    AND (lessons.deleted_at <= '{$realStart->format("Y-m-d")}' OR lessons.deleted_at is null)))")
                    ->select('lessons.*', 'class_times.start_time', 'class_times.end_time', 'class_times.id as ct_id')
                    ->orderBy('class_times.start_time');

                if($groupId) $lessonDays->where('letter_groups.id', $groupId);

                foreach($lessonDays->get() as $lessonDay){

                    $forStartDate =Carbon::parse($realStart);
                    $forEndDate = Carbon::parse($realEnd);

                    for($date = $forStartDate; $date->lte($forEndDate); $date->addDay()) {
//                        if ($lessonDay['subject_id'] == 2) dd($date);
                        if(Helper::getDayOfWeekIso($date->dayOfWeek) == $lessonDay->week_day)
                        {
                            //substituted
                            if(isset($substitutions[$lessonDay->id]) && isset($substitutions[$lessonDay->id][$date->format("Y-m-d")])){
                                $subs = $substitutions[$lessonDay->id][$date->format("Y-m-d")];
                                $this->insertToDays($date, $year->id, 'day', $lessonDay->id, $lessonDay->letter_group_id, $subs->user_id, $subs->id);
                            }
                            else{
                                $this->insertToDays($date, $year->id, 'day', $lessonDay->id, $lessonDay->letter_group_id, $lessonDay->user_id, null);
                            }
                            $date->addDays(6);
                        }
                    }

                }

//                dd($subjectsTets);
            }

        }
    }

    public function chechDayExists($year, $month, $day, $classLetterId = null, $subjectId = null, $dayType)
    {
        $chk = LessonDay::realData()
                ->leftJoin('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->where('lesson_days.type', $dayType)
                ->where('date', Carbon::create($year, $month, $day));

        if($classLetterId) $chk->where('letter_groups.class_letter_id', $classLetterId);
        if($subjectId) $chk->where('lessons.subject_id', $subjectId);

        return $chk->first();
    }

    public function fixStudentYIandY($studentId, $subjectId, $classLetterId)
    {
        foreach($this->getYears() as $year){
            $yiSum = 0;
            $yiCount = 0;
            $periods = $year->periods;
            $periodCount = count($periods);
            $currentPeriod = 0;
            $filledPeriods = 0;

            foreach($periods as $period){
                $currentPeriod++;
                $isLastPeriod = $currentPeriod === $periodCount ? true : false;

                $allKsqs = LessonDay::realData()
                    ->join('marks', function ($join) use($studentId, $subjectId){
                        $join->on('marks.date', 'lesson_days.date')
                            ->on('marks.student_id', DB::raw($studentId))
                            ->on('marks.type', 'lesson_days.type')
                            ->on('marks.subject_id', DB::raw($subjectId));
                    })
                    ->leftJoin('msk_marks', 'msk_marks.id', 'marks.msk_mark_id')
                    ->where('lesson_days.year_id', $year->id)
                    ->where('lesson_days.type', 'ksq')
                    ->where('lesson_days.date', '<=', $period->end_date)
                    ->where('lesson_days.date', '>=', $period->start_date)
                    ->select('marks.*', 'msk_marks.name as mark_val')->get();

                $countKsq = 0; $countKsqFilled = 0; $sumKsq = 0; $ksqVal = 0;
                foreach ($allKsqs as $ksqMark){
                    $countKsq++;
                    if($ksqMark['mark_val'] > 0) $countKsqFilled++;
                    $sumKsq += (int)$ksqMark['mark_val'];
                }
                $ksqVal = $countKsq === 0 ? 0 : ($sumKsq / $countKsq) * 0.4;

                $bsqValReal = LessonDay::realData()
                    ->join('marks', function ($join) use($studentId, $subjectId){
                        $join->on('marks.date', 'lesson_days.date')
                            ->on('marks.student_id', DB::raw($studentId))
                            ->on('marks.type', 'lesson_days.type')
                            ->on('marks.subject_id', DB::raw($subjectId));
                    })
                    ->leftJoin('msk_marks', 'msk_marks.id', 'marks.msk_mark_id')
                    ->where('lesson_days.year_id', $year->id)
                    ->where('lesson_days.type', 'bsq')
                    ->where('lesson_days.date', '<=', $period->end_date)
                    ->where('lesson_days.date', '>=', $period->start_date)
                    ->select('marks.*', 'msk_marks.name as mark_val')->first()['mark_val'];
                $bsqVal = (int)$bsqValReal * 0.6;

                $yi = round($ksqVal + $bsqVal);
                $yiSum += $yi;
                $yiCount++;

                //check if all ksq was filled
                if($countKsqFilled !== $countKsq || $bsqValReal === null) continue;
                $this->insertYiandY($studentId, $subjectId, $period->end_date, 'yi', $classLetterId, $yi);

                $filledPeriods++;
                if($isLastPeriod && $filledPeriods === $periodCount){
                    $y = round($yiSum / $yiCount);
                    $this->insertYiandY($studentId, $subjectId, $period->end_date, 'y', $classLetterId, $y);
                }
            }
        }
    }

    public function insertYiandY($studentId, $subjectId, $date, $dayType, $classLetterId, $value)
    {
        $markData = Mark::realData()->where('date', $date)
            ->where('student_id', $studentId)
            ->where('subject_id', $subjectId)
            ->where('type', $dayType)
            ->first();

        if(!$markData){
            $markData = new Mark();
        }

        $markData->date = $date;
        $markData->tenant_id = 1;
        $markData->teacher_id = null;
        $markData->student_id = $studentId;
        $markData->subject_id = $subjectId;
        $markData->class_letter_id = $classLetterId;
        $markData->class_time_id = null;
        $markData->msk_mark_id = null;
        $markData->qb = 0;
        $markData->type = $dayType;
        $markData->value = $value;
        $markData->save();
    }

    public function getSubstitutions($groupId = null, Carbon $startDate = null, Carbon $endDate = null)
    {
        $substutionsGroup = [];
        $substutions = SubstitutionLesson::join('substitutions', 'substitutions.id', 'substitution_lessons.substitution_id')
            ->join('lessons', 'lessons.id', 'substitution_lessons.lesson_id');

        if($groupId) $substutions->where('lessons.letter_group_id', $groupId);
        if($startDate) $substutions->whereRaw("substitutions.date >= '{$startDate->format('Y-m-d')}'");
        if($endDate) $substutions->whereRaw("substitutions.date <= '{$endDate->format('Y-m-d')}'");

        $substutions = $substutions
            ->select('substitution_lessons.*', 'substitutions.date')
            ->get();

        foreach($substutions as $substution){
            if(!isset($substutionsGroup[$substution->lesson_id])) $substutionsGroup[$substution->lesson_id] = [];
            $substutionsGroup[$substution->lesson_id][$substution->date] = $substution;
        }

        return $substutionsGroup;
    }
}