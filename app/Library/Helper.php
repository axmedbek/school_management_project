<?php
/**
 * Created by PhpStorm.
 * User: tabdullayev
 * Date: 2018-02-26
 * Time: 14:43
 */

namespace App\Library;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Response;

class Helper
{
    private static $pathOrder = false;

    private static function makeProperRouteName($route)
    {
        switch ($route) {
            case 'years_wizard':
                return 'years';
            default:
                return $route;
        }
    }

    private static function findPlace($modules, $routes, $dept = 0)
    {
        if (self::$pathOrder !== false) return self::$pathOrder;

        $currentRoute = self::makeProperRouteName(\Request::route()->getName());
        foreach ($modules as $key => $module) {
            if (isset($module['route']) && $module['route'] == $currentRoute) {
                $routes[$dept] = $key;
                return (self::$pathOrder = $routes);
            } else if (isset($module['child'])) {
                $cc = $routes;
                $cc[$dept] = $key;
                self::findPlace($module['child'], $cc, $dept + 1);
            }
        }
    }

    public static function makeMenu($menuItems, $dept = 0)
    {
        self::findPlace(Standarts::$modules, [], 0);
        $ii = 0;

        foreach ($menuItems as $key => $item) {
            $ii++;

            if (!isset($item['child'])) {
                if (!self::has_priv($item['route'], $item['priv']))
                    continue;

                print '<li class="' . ($ii == 1 ? 'm-t-30' : '') . ' ' . (isset(self::$pathOrder[$dept]) && self::$pathOrder[$dept] == $key ? 'active' : '') . '">
                            <a href="' . route($item['route']) . '">
                                <span class="title">' . Standarts::getTrans('modules.'.$key) . '</span>
                            </a>
                            <span class="icon-thumbnail"><i class="' . $item['icon'] . '">' . (isset($item['icon_name']) ? $item['icon_name'] : '') . '</i></span>
                        </li>';
            } else {

                $checkHasSubMenuIgnoreL1 = '';
                $checkHasSubMenuIgnoreL2 = '';
                foreach ($item['child'] as $innerKey => $it) {
                    if (self::has_priv($it['route'], $it['priv'])) {
                        $checkHasSubMenuIgnoreL1 = '<li class="' . ($ii == 1 ? 'm-t-30' : '') . ' ' . (isset(self::$pathOrder[$dept]) && self::$pathOrder[$dept] == $innerKey ? 'open active' : '') . '">
                        <a href="javascript:void(0);"><span class="title">' . Standarts::getTrans('modules.'.$key) . '</span>
                            <span class="arrow ' . (isset(self::$pathOrder[$dept]) && self::$pathOrder[$dept] == $innerKey ? 'open active' : '') . '"></span>
                        </a>
                        <span class="icon-thumbnail"><i class="' . $item['icon'] . '">' . (isset($item['icon_name']) ? $item['icon_name'] : '') . '</i></span>
                        <ul class="sub-menu">';
                        $checkHasSubMenuIgnoreL2 = '</ul></li>';
                    }
                }
                print $checkHasSubMenuIgnoreL1;
                self::makeMenu($item['child'], $dept + 1);
                print $checkHasSubMenuIgnoreL2;
            }
        }
    }

    public static function makeLinker()
    {
        self::findPlace(Standarts::$modules, []);

        $last = count(self::$pathOrder) - 1;
        $modules = Standarts::$modules;
        foreach (self::$pathOrder as $key => $path) {
            if ($key > 0) $modules = $modules['child'];
            $modules = $modules[$path];
            if ($key != $last) print '<li><p>' . self::strToUpperCase(Standarts::getTrans('modules.'. $path)) . '</p></li>';
            else print '<li><a href="#" class="active">' . self::strToUpperCase(Standarts::getTrans('modules.'. $path)) . '</a></li>';
        }
    }

    public static function has_priv($module, $priv)
    {
        return Auth::user()->group->getModulePriv($module) >= $priv ? true : false;
    }

    public static function getCell($col, $row)
    {
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD',
            'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AP', 'AQ',
            'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE',
            'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL','BM'.'BN','BO','BP', 'BQ',
            'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',];

        return $cols[$col - 1] . $row;
    }

    public static function strToUpperCase($str)
    {
        return str_replace(['I', 'İ̇'], ['İ', 'İ'], mb_strtoupper($str));
    }

    public static function filePermision($path, $set = false)
    {
        if (!file_exists($path))
            mkdir($path, 0700, true);

        $permision = substr(sprintf('%o', fileperms($path)), -3);

        if ($set) {
            chmod($path, 0777);
        }

        return $permision;
    }

    public static function locale()
    {
        return request()->cookie('my_locale',config('app.locale'));
    }

    public static function getDayOfWeekIso($weekDay)
    {
        return $weekDay == 0 ? 7 : $weekDay;
    }

    public static function getSelectedStudentForParent()
    {
        $student = User::realData()->join('person_families', 'users.id', 'person_families.user_id')
            ->where('parent_id', auth()->guard('portal')->user()->id)
            ->select('users.*')
            ->first();

        if ($student) {
            return session('selected_student_for_parent', $student->id);
        } else 0;
    }

    public static function makeOnlineUserOrOffline($guard,$date,$status = 0){
        $user = auth($guard)->user();
        if ($status != 0){
            $user->is_online = 1;
            $user->last_active_date = null;
        }
        else{
            $user->is_online = 0;
            $user->last_active_date = $date;
        }

        $user->save();
    }
}