<?php
namespace App\Library;

use App\Models\ClassTime;
use App\Models\Lesson;
use App\Models\Room;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LessonState
{
    private $groupSubjects = false;
    private $group = false;
    private $classLetter = false;
    private $msk_class = false;
    private $year = false;
    private $weeks = 0;

    public function __construct($group)
    {
        $this->group = $group;
        $this->classLetter = $group->class_letter;
        $this->msk_class = $this->classLetter->msk_class;
        $this->year = $this->classLetter->year;
        $this->weeks = (new YearDays($this->year))->getClassWeeks($this->classLetter->msk_class_id)['weeks'];

        $this->getGroupSubjectHours();
    }

    private function getGroupSubjectHours()
    {
        $subjects = $this->msk_class->subjects()
            ->select('id',DB::raw("( SELECT SUM([hour]) FROM seasons
                            INNER JOIN group_type_season ON group_type_season.season_id = seasons.id AND group_type_season.group_type_id = {$this->group->group_type_id}
                            INNER JOIN season_paragraphs ON season_paragraphs.season_id = seasons.id WHERE seasons.subject_id = subjects.id AND seasons.msk_class_id='{$this->classLetter->msk_class_id}') as hours"))

            ->get();

        foreach ($subjects as $subject){
            $hours = $this->weeks == 0 ? 0 : (int)ceil($subject->hours / $this->weeks);
            $this->groupSubjects[$subject->id] = $hours;
        }
    }

    private function checkTeacher($subject_id, $week_day, $class_time, $teacherId)
    {
        $teachers = User::realData('teacher')
            ->join('subject_user', function ($join) use($subject_id){ $join->on('subject_user.user_id', '=', 'users.id')->on('subject_user.subject_id', '=', DB::raw($subject_id)); })
            ->whereNotIn('subject_user.user_id', function ($q) use($week_day, $class_time)
            {
                return $q->select('user_id')->from('lessons')
                    ->leftJoin('class_times', 'class_times.id', '=', 'lessons.class_time_id')
                    ->leftJoin('letter_groups', 'letter_groups.id', '=', 'lessons.letter_group_id')
                    ->whereRaw("( (class_times.start_time <= '{$class_time->end_time}' AND class_times.end_time >= '{$class_time->start_time}') OR (class_times.start_time >= '{$class_time->end_time}' AND class_times.end_time <= '{$class_time->start_time}'))")
                    ->where('year_id', $this->year->id)->where('week_day', $week_day)
                    ->where('letter_groups.class_letter_id', '!=', $this->classLetter->id)
                    ->whereNull('lessons.deleted_at');
            })
            ->where('users.id', $teacherId)
            ->first();

        return !!$teachers;
    }

    private function checkRoom($week_day, $teacherId, $class_time, $roomId, $subject_id)
    {
        $classMerge = Lesson::realData()
            ->leftJoin('class_times', 'class_times.id', '=', 'lessons.class_time_id')
            ->where("class_times.id", $class_time->id)
            ->where('year_id', $this->year->id)->where('week_day', $week_day)
            ->where('lessons.letter_group_id', '!=', DB::raw($this->group->id))
            ->where('lessons.user_id', $teacherId)
            ->where('lessons.room_id', $roomId)->first();

        if(!!$classMerge) return true;

        $rooms = Room::realData()
            ->leftJoin('room_subject', 'room_subject.room_id', '=', 'rooms.id')
            ->whereNotIn('rooms.id', function ($q) use($week_day, $class_time)
            {
                return $q->select('room_id')->from('lessons')
                    ->leftJoin('class_times', 'class_times.id', '=', 'lessons.class_time_id')
                    ->leftJoin('letter_groups', 'letter_groups.id', '=', 'lessons.letter_group_id')
                    ->whereRaw("( (class_times.start_time <= '{$class_time->end_time}' AND class_times.end_time >= '{$class_time->start_time}') OR (class_times.start_time >= '{$class_time->end_time}' AND class_times.end_time <= '{$class_time->start_time}'))")
                    ->where('lessons.letter_group_id', '!=', DB::raw($this->group->id))
                    ->where('year_id', $this->year->id)->where('week_day', $week_day)
                    ->whereNull('lessons.deleted_at');
            })
            ->where('rooms.corpus_id', $this->classLetter->corpus_id)
            ->where(function ($f)use($subject_id){ $f->where('room_subject.subject_id', $subject_id)->orWhereNull('room_subject.subject_id'); })
            ->where('rooms.id', $roomId)
            ->first();

        return !!$rooms;
    }

    public function checkLesson($weekDay, $classTimeId, $subjectId, $teacherId, $roomId )
    {
        if(!isset($this->groupSubjects[$subjectId])) return ['weekDay'=> $weekDay, 'classTimeId'=> $classTimeId, 'error_str'=> 'Bu fənni seçmək mümkün deyil!'];
        else if(--$this->groupSubjects[$subjectId] < 0) return ['weekDay'=> $weekDay, 'classTimeId'=> $classTimeId, 'error_str'=> 'Bu fənni seçmək mümkün deyil!'];

        $class_time = ClassTime::realData()->find($classTimeId);
        if(!$this->checkTeacher($subjectId, $weekDay, $class_time, $teacherId)) return ['weekDay'=> $weekDay, 'classTimeId'=> $classTimeId, 'error_str'=> 'Bu müəllimi seçmək mümkün deyil!'];
        else if(!$this->checkRoom($weekDay, $teacherId, $class_time, $roomId, $subjectId)) return ['weekDay'=> $weekDay, 'classTimeId'=> $classTimeId, 'error_str'=> 'Bu otağı seçmək mümkün deyil!'];

        return false;
    }
}