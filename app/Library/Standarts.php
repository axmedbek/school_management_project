<?php
/**
 * Created by PhpStorm.
 * User: tabdullayev
 * Date: 2018-02-26
 * Time: 15:01
 */

namespace App\Library;

class Standarts
{
    public static $modules =
    [
        'dashboard' => ['name'=> 'Dashboard', 'icon'=> 'fa fa-dashboard',  'route'=> 'dashboard', 'priv'=> self::PRIV_CAN_SEE ],

        'msk_equipments' => ['name'=> 'Devices', 'icon'=> 'fa pg-layouts4', 'icon_name' => '','route'=> 'msk_equipments', 'priv'=> self::PRIV_CAN_SEE ],
        'msk_add' => ['name' => 'Lesson Settings', 'icon'=> 'fa fa-gears', 'child'=> [
                'msk_class_times' => ['name'=> 'Lesson Times', 'icon'=> '', 'icon_name' => 'CT','route'=> 'msk_class_times', 'priv'=> self::PRIV_CAN_SEE ],
                'msk_rooms' => ['name'=> 'Rooms', 'icon'=> '', 'icon_name' => 'R','route'=> 'msk_rooms', 'priv'=> self::PRIV_CAN_SEE ],
                'msk_classes' => ['name'=> 'Classes and Subjects', 'icon'=> '', 'icon_name' => 'C','route'=> 'msk_classes', 'priv'=> self::PRIV_CAN_SEE ],
                'class_groups' => ['name'=> 'Class Groups', 'icon'=> 'fa fa-sort-alpha-asc',  'route'=> 'class_groups', 'priv'=> self::PRIV_CAN_SEE ],
            ]
        ],
        'topics' => ['name'=> 'Lesson Program', 'icon'=> 'pg-note',  'route'=> 'topics', 'priv'=> self::PRIV_CAN_SEE ],
        'users' => ['name' => 'Users', 'icon'=> 'fa fa-group', 'child'=> [
                'str_users' => ['name'=> 'Admin Users', 'icon'=> 'fa fa-user',  'route'=> 'str_users', 'priv'=> self::PRIV_CAN_SEE ],
                'str_students' => ['name'=>'Students','icon'=>'fa fa-graduation-cap', 'route' => 'str_students','priv'=>self::PRIV_CAN_SEE],
                'str_teacher' => ['name'=> 'Teachers', 'icon'=> '', 'icon_name' => 'T',  'route'=> 'str_teacher', 'priv'=> self::PRIV_CAN_SEE ],
                'str_staff' => ['name'=> 'Personals', 'icon'=> 'fa fa-male',  'route'=> 'str_staff', 'priv'=> self::PRIV_CAN_SEE ],
                'str_parents' => ['name' => 'Parents','icon' => 'fa fa-users','route' => 'str_parents' , 'priv' => self::PRIV_CAN_SEE],
            ]
        ],
        'cameras' => ['name'=> 'Cameras', 'icon'=> 'fa fa-video-camera',  'route'=> 'cameras', 'priv'=> self::PRIV_CAN_SEE ],
        'tables' => ['name'=> 'Journals', 'icon'=> 'fa fa-table',  'route'=> 'tables', 'priv'=> self::PRIV_CAN_SEE ],
        'substitution' => ['name'=> 'Lesson Substitution', 'icon'=> 'fa fa-arrows-h',  'route'=> 'substitution', 'priv'=> self::PRIV_CAN_SEE ],
        'advert_task' => ['name'=> 'Events & Tasks', 'icon'=> 'fa fa-tasks',  'route'=> 'advert_task', 'priv'=> self::PRIV_CAN_SEE ],
        'galery' => ['name'=> 'Gallery', 'icon'=> 'pg-movie',  'route'=> 'galery', 'priv'=> self::PRIV_CAN_SEE ],
        'msk' => ['name' => 'Reports', 'icon'=> 'fa fa-wrench', 'child'=> [
            'years' => ['name'=> 'Tədris İli', 'icon'=> 'pg-calender',  'route'=> 'years', 'priv'=> self::PRIV_CAN_SEE ],
            'corpuses' => ['name'=> 'Korpuslar', 'icon'=> 'fa fa-building',  'route'=> 'corpuses', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_subjects' => ['name'=> 'Fənnlər', 'icon'=> '', 'icon_name' => 'S','route'=> 'msk_subjects', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_room_type' => ['name'=> 'Otaq tipləri', 'icon'=> '', 'icon_name' => 'RT','route'=> 'msk_room_type', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_group_types' => ['name'=> 'Təmayül tipləri', 'icon'=> '', 'icon_name' => 'GT','route'=> 'msk_group_types', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_mark_types' => ['name'=> 'Qiymət tipləri', 'icon'=> '', 'icon_name' => 'MT','route'=> 'msk_mark_types', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_group' => ['name'=> 'İstifadəçi qrupları', 'icon'=> '', 'icon_name' => 'UG','route'=> 'msk_group', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_teacher_status' => ['name'=> 'Müəllim Statusları', 'icon'=> '', 'icon_name' => 'TS','route'=> 'msk_teacher_status', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_heyetler' => ['name'=> 'Personal heyət tipləri', 'icon'=> '', 'icon_name' => 'H','route'=> 'msk_heyetler', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_relations' => ['name'=> 'Qohumliq əlaqəsi tipləri', 'icon'=> '', 'icon_name' => 'R','route'=> 'msk_relations', 'priv'=> self::PRIV_CAN_SEE ],
            'sms_templates' => ['name'=> 'Sms şablonları', 'icon'=> 'fa fa-send-o',  'route'=> 'sms_templates', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_holidays' => ['name'=> 'Qeyri iş günləri', 'icon'=> '', 'icon_name' => 'HD','route'=> 'msk_holidays', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_equipments_types' => ['name'=> 'Avadanlıq tipi', 'icon'=> '', 'icon_name' => 'ET','route'=> 'msk_equipments_types', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_cities_and_regions' => ['name'=> 'Şəhərlər və Rayonlar', 'icon'=> 'pg-cupboard', 'icon_name' => '','route'=> 'msk_cities_and_regions', 'priv'=> self::PRIV_CAN_SEE ],
            'msk_study_levels' => ['name'=> 'Təhsil səviyyəsi', 'icon'=> 'fa fa-university', 'icon_name' => '','route'=> 'msk_study_levels', 'priv'=> self::PRIV_CAN_SEE ],
            'translations' => ['name'=> 'Translations', 'icon'=> 'fa fa-language', 'icon_name' => '','route'=> 'msk_translations', 'priv'=> self::PRIV_CAN_SEE ],
            ]
        ],
        'reports' => ['name' => 'Hesabatlar' , 'icon' => 'fa fa-stack-exchange','child' =>[
            'report_table' => ['name' => 'Cədvəl','icon' => 'fa fa-table','route' => 'report_table','priv' => self::PRIV_CAN_SEE],
            'report_staff' => ['name' => 'Personal hesabatı','icon' => 'fa fa-male','route' => 'report_staff','priv' => self::PRIV_CAN_SEE],
            'report_teacher' => ['name' => 'Müəllim hesabatı','icon' => 'fa fa-user-md','route' => 'report_teacher','priv' => self::PRIV_CAN_SEE],
            'report_student' => ['name' => 'Şagird hesabatı','icon' => 'fa fa-user','route' => 'report_student','priv' => self::PRIV_CAN_SEE],
            'report_staff_statistic' => ['name' => 'Personal statistikası','icon' => 'fa fa-pie-chart','route' => 'report_staff_statistic','priv' => self::PRIV_CAN_SEE],
            'report_teacher_statistic' => ['name' => 'Müəllim statistikası','icon' => 'fa fa-area-chart','route' => 'report_teacher_statistic','priv' => self::PRIV_CAN_SEE],
            'report_student_statistic' => ['name' => 'Şagird statistikası','icon' => 'fa fa-line-chart','route' => 'report_student_statistic','priv' => self::PRIV_CAN_SEE],
            'report_tabel' => ['name' => 'Tabel hesabatı','icon' => '','icon_name' => 'T','route' => 'report_tabel','priv' => self::PRIV_CAN_SEE],
            'report_attendance' => ['name' => 'Davamiyyət cədvəli','icon' => '','icon_name' => 'DT','route' => 'report_attendance','priv' => self::PRIV_CAN_SEE],
            'report_attendance_personal' => ['name' => 'Davamiyyət personal gecikmə\tez getmə','icon' => '','icon_name' => 'DP','route' => 'report_attendance_personal','priv' => self::PRIV_CAN_SEE],
            'reports_jurnal' => ['name'=> 'Jurnal', 'icon'=> '','icon_name' => 'J',  'route'=> 'reports_jurnal', 'priv'=> self::PRIV_CAN_SEE ],
            'reports_final_evaluation_table' => ['name'=> 'Tədris ilinin yekun qiymətləndirmə cədvəli', 'icon'=> '','icon_name' => 'YQ',  'route'=> 'reports_final_evaluation_table', 'priv'=> self::PRIV_CAN_SEE ],
            'reports_sms_table' => ['name'=> 'SMS Report', 'icon'=> '','icon_name' => 'SR',  'route'=> 'reports_sms_table', 'priv'=> self::PRIV_CAN_SEE ],
            'reports_jurnal_not_writed' => ['name'=> 'Jurnal yazılmaması üzrə hesabat', 'icon'=> '','icon_name' => 'JNR',  'route'=> 'reports_jurnal_not_writed', 'priv'=> self::PRIV_CAN_SEE ],
            ]
        ],
    ];

    public static $portalModules =
    [
        'portal_dashboard' => ['name'=> 'Ana səhifə', 'icon'=> 'os-icon os-icon-layout', 'is_menu' =>true , 'route'=> 'portal_dashboard', 'priv'=> ['teacher', 'student', 'parent'] ],
        'portal_lesson_materials' => ['name'=> 'Tapşırıqlar', 'icon'=> 'fa fa-address-book', 'is_menu' =>true, 'route'=> 'portal_lesson_materials', 'priv'=> ['student', 'parent'] ],
        'portal_jurnal' => ['name'=> 'Jurnal', 'icon'=> 'fa fa-wpforms',  'route'=> 'portal_jurnal','is_menu' =>true, 'priv'=> ['teacher'] ],
        'portal_attendance' => ['name'=> 'Davamiyyət', 'icon'=> 'fa fa-check-square-o','is_menu' =>true,  'route'=> 'portal_attendance', 'priv'=> ['student', 'parent'] ],
        'portal_topics' => ['name'=> 'Tədri̇s proqrami', 'icon'=> 'fa fa-file-text-o', 'is_menu' =>true, 'route'=> 'portal_topics', 'priv'=> ['teacher'] ],
        'portal_galery' => ['name'=> 'Galereya', 'icon'=> 'fa fa-picture-o', 'is_menu' =>true , 'route'=> 'portal_galery', 'priv'=> ['teacher', 'student', 'parent'] ],
        'portal_profile' => ['is_menu' =>false , 'route'=> 'portal_profile', 'priv'=> ['teacher', 'student', 'parent'] ],
        'portal_change_student' => ['is_menu' =>false , 'route'=> 'portal_change_student', 'priv'=> ['parent'] ],
        'portal_teacher_request' => ['name' => 'Müəllim müraciəti', 'icon' => 'fa fa-paper-plane' , 'is_menu' => true , 'route' => 'portal_teacher_request', 'priv' => ['parent']],

        //chat
        'portal_chat' => ['name' => 'Chat', 'icon' => 'fa fa-comment' , 'is_menu' => true , 'route' => 'portal_chat', 'priv' => ['parent','student','teacher']],

        //video_camera
        'portal_camera' => ['name' => 'Video müşahidə', 'icon' => 'fa fa-video-camera' , 'is_menu' => true , 'route' => 'portal_camera', 'priv' => ['parent']],

        //reports
        'portal_final_evaluation' => ['name' => 'Tədris ilinin yekun qiymətləndirmə cədvəli', 'icon' => 'fa fa-wpforms' , 'is_menu' => true , 'route' => 'portal_final_evaluation', 'priv' => ['parent']],

        //upgrade
        'portal_library' => ['name'=> 'Kitabxana', 'icon'=> 'fa fa-book', 'is_menu' =>true , 'route'=> false, 'priv'=> ['student'] ],
        'portal_test' => ['name'=> 'İmtahan', 'icon'=> 'fa fa-clipboard', 'is_menu' =>true , 'route'=> false, 'priv'=> ['student','parent'] ],
        'portal_online_payment' => ['name'=> 'Online ödəniş', 'icon'=> 'fa fa-cc-visa', 'is_menu' =>true , 'route'=> false, 'priv'=> ['parent'] ],
    ];

    public static $userTypes = [
        'user' => 'İstifadəçilər',
        'student' => 'Şagirdlər',//
        'staff' => 'Personal',
        'teacher' => 'Müəllimlər',//
        'parent' => 'Valideynlər',
    ];

    public static $userTypesSingle = false;
    public static $dayTypes = [
        'ksq' => 'KSQ',
        'bsq' => 'BSQ',
        'y' => 'İ',
        'yi' => 'Yİ',
    ];

    public static $rowCount = 20;

    public static $userImageDir = "/images/user/";
    public static $userThumbir = "/images/user/thumb/";
    public static $corpusImageDir = "/images/corpus/";
    public static $optionFilesDir = "/option_files/";
    public static $eventTaskFileDir = "/option_files/";
    public static $portalLectureFilesDir = "/option_files/";
    public static $portalHomeworkFilesDir = "/option_files/";
    public static $portalSmsFilesDir = "/option_files/";


    /**
     * Privs
     */

    const PRIV_CAN_SEE = 2;
    const PRIV_CAN_EDIT = 3;

    /**
     * Week day names
     */

    public static $weekDayNames = ['Bazar ertəsi','Çərşənbə axşamı','Çərşənbə','Cümə axşamı','Cümə','Şənbə','Bazar'];
    public static $months = ["Yanvar", "Fevral", "Mart", "Aprel", "May", "İyun", "İyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"];

    public static function init()
    {
        self::$userTypesSingle = [
            'user' => __('messages.standarts_user_single'),
            'student' => __('messages.standarts_student_single'),
            'staff' => __('messages.standarts_staff_single'),
            'teacher' => __('messages.standarts_teacher_single'),
            'parent' => __('messages.standarts_parent_single'),
        ];

        self::$dayTypes = [
            'ksq' => 'KSQ',
            'bsq' => 'BSQ',
            'y' => 'İ',
            'yi' => 'Yİ',
        ];
    }

    public static function getTrans($key){
        return trans($key);
    }
}