<?php
/**
 * Created by PhpStorm.
 * User: tabdullayev
 * Date: 2018-03-30
 * Time: 12:10
 */

namespace App\Library;


use App\Models\MskClass;
use App\Models\Year;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class YearDays
{

    private $year;
    const FILE_NAME = 'year_days';

    public function __construct($year)
    {
        $this->year = is_object($year) ? $year : Year::realData()->find($year);
    }

    public function calculateDays()
    {
        // year object
        $year = $this->year;
        // start_date with time
        $startY = strtotime($year->start_date);
        // end_date with time
        $endY = strtotime($year->end_date);

        // find count of all days in  selected year
        $yearDays = (int)(($endY - $startY) / (60 * 60 * 24));

        // all of classes
        $classes = MskClass::realData($year->tenant_id)->get()->toArray();

        $holidays = [];
        foreach ($classes as $class) {
            // holidays for per class
            $holidays[$class['id']] = [];
        }
        // foreach all holidays of year
        foreach ($year->holidays as $holiday) {
            // check holiday has in lesson day
            for ($date = Carbon::parse($holiday->start_date); $date->lte(Carbon::parse($holiday->end_date)); $date->addDay()) {
                $time = strtotime($date->format('Y-m-d'));

                if ($startY <= $time && $endY >= $time) {
                    if (count($holiday->holiday_classes) > 0) {
                        foreach ($holiday->holiday_classes as $class)
                            $holidays[$class->id][$date->format('Y-m-d')] = 1;
                    } else {
                        foreach ($holidays as $classId => $val)
                            $holidays[$classId][$date->format('Y-m-d')] = 1;
                    }
                }
            }
        }

        $holidayDays = [];
        foreach ($classes as $class){
            $hDays = count($holidays[$class['id']]);
            $days = $yearDays - $hDays;
            $holidayDays[$class['id']] = ['days'=> $days, 'holidays'=> $hDays, 'weeks'=> (int)($days / 7)];
        }

        $this->saveData($holidayDays);

        return true;
    }

    public function saveData($dataThisYear)
    {
        $dataAll = $this->getData();

        $dataAll[$this->year->id] = $dataThisYear;

        Storage::put(self::FILE_NAME, json_encode($dataAll));
    }

    public function getClassWeeks($classId)
    {
        $dataAll = $this->getData();

        return isset($dataAll[$this->year->id]) && isset($dataAll[$this->year->id][$classId]) ? $dataAll[$this->year->id][$classId] : ["days"=> 0, "holidays"=> 0, "weeks"=> 0];
    }

    private function getData()
    {
        $dataAll = Storage::exists(self::FILE_NAME) ? Storage::get(self::FILE_NAME) : "[]";
//        if(Storage::exists(self::FILE_NAME)){
//            $dataAll = Storage::get(self::FILE_NAME);
//        }else{
//            (new YearDays(YearDays::getCurrentYear()))->calculateDays();
//            $dataAll = Storage::get(self::FILE_NAME);
//        }

        $dataAll = is_array(json_decode($dataAll, true)) ? json_decode($dataAll, true) : [];

        return $dataAll;
    }

    public static function getCurrentYear($month = false, $year = false, $date = false)
    {
            $currentYear = Year::realData()
                ->orderBy('id', 'DESC');

            if($date){
                $currentYear->whereRaw("start_date<='$date' AND end_date>='$date'");
            }
            elseif ($month && $year){
                $offest = $month + $year * 12;
                $currentYear->whereRaw(" YEAR(start_date)*12+MONTH(start_date)<='$offest' AND YEAR(end_date)*12+MONTH(end_date)>='$offest'");
            }

        return $currentYear->first();
    }
}