<?php

namespace App;

use App\Library\Standarts;
use App\Models\Finger;
use App\Models\Group;
use App\Models\Lesson;
use App\Models\LetterGroup;
use App\Models\MskCities;
use App\Models\MskHeyetler;
use App\Models\MskRegions;
use App\Models\MskStudyLevels;
use App\Models\MskTeacherStatus;
use App\Models\PersonFamily;
use App\Models\PersonLaborActivity;
use App\Models\PersonLanguage;
use App\Models\PersonScientificDegree;
use App\Models\PersonStudy;
use App\Models\SmsNumber;
use App\Models\StudentComment;
use App\Models\Subject;
use App\Models\Substitution;
use App\Models\UserHistory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    protected  $table = 'users';

    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function realData($type = null)
    {
        $data = self::where("users.tenant_id", Auth::user()->tenant_id);
        if( $type !== null ) $data = $data->where('user_type', $type);

        return $data;
    }

    public function save(array $options = [])
    {
        parent::save($options);

        //user repaired
        if($this->deleted_at == null && $this->histories->count() == 0){
            $history = new UserHistory();
            $this->histories()->save($history);
        }
    }

    public function fullname()
    {
        return $this->name . " " . $this->surname . " " .$this->middle_name;
    }

    public function delete()
    {
        //$this->clearPic();
        $this->histories()->delete();

        return parent::delete();
    }

    public function getGenger()
    {
        return $this->gender == 'm' ? 'Kişi':'Qadın';
    }

    public function getStatus()
    {
        return $this->deleted_at == null ? 'Aktiv':'Deaktiv';
    }

    public function clearPic()
    {
        if( !empty($this->thumb) && is_file(public_path(Standarts::$userImageDir.$this->thumb))) unlink( public_path(Standarts::$userImageDir.$this->thumb) );
        if( !empty($this->thumb) && is_file(public_path(Standarts::$userThumbir.$this->thumb))) unlink( public_path(Standarts::$userThumbir.$this->thumb) );
    }

    public function histories()
    {
        return $this->hasMany(UserHistory::class,'user_id','id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function person_studies()
    {
        return $this->hasMany(PersonStudy::class);
    }

    public function person_languages()
    {
        return $this->hasMany(PersonLanguage::class);
    }

    public function person_labot_activities()
    {
        return $this->hasMany(PersonLaborActivity::class);
    }

    public function person_families()
    {
        return $this->hasMany(PersonFamily::class,'user_id');
    }

    public function parent_families()
    {
        return $this->hasMany(PersonFamily::class,'parent_id');
    }

    public function person_scientific_degrees()
    {
        return $this->hasMany(PersonScientificDegree::class);
    }

    public function student_comments()
    {
        return $this->hasMany(StudentComment::class);
    }

    public function msk_teacher_status()
    {
        return $this->belongsTo(MskTeacherStatus::class);
    }

    public function sms_numbers()
    {
        return $this->hasMany(SmsNumber::class);
    }

    public function teacher_subjects()
    {
        return $this->belongsToMany(Subject::class,'subject_user','user_id','subject_id');
    }

    public function substitution(){
        return $this->hasMany(Substitution::class);
    }

    public function lesson(){
        return $this->hasOne(Lesson::class);
    }

    public function fingers()
    {
        return $this->hasMany(Finger::class);
    }

    public function heyet()
    {
        return $this->belongsTo(MskHeyetler::class, 'personal_heyeti', 'id');
    }

    //address
    public function currentCity()
    {
        return $this->belongsTo(MskCities::class, 'current_city', 'id');
    }

    public function currentRegion()
    {
        return $this->belongsTo(MskRegions::class, 'current_region', 'id');
    }

    public function livedCity()
    {
        return $this->belongsTo(MskCities::class, 'lived_city', 'id');
    }

    public function livedRegion()
    {
        return $this->belongsTo(MskRegions::class, 'lived_region', 'id');
    }

    public function msk_study_levels(){
        return $this->belongsTo(MskStudyLevels::class);
    }

    public function letter_groups(){
        return $this->belongsToMany(LetterGroup::class,'letter_group_user');
    }

}
