<?php

namespace App\Http\Controllers;

use App\Library\LessonDaysFixer;
use App\Library\SmsTemplate;
use App\Library\YearDays;
use App\Models\Chat;
use App\Models\Duty;
use App\Models\LetterGroup;
use App\Models\Notification;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'attendedStudents' => 0,
            'totalEmployee' => User::realData('staff')->count(),
            'totalTeacher' => User::realData('teacher')->count(),
            'totalStudent' => User::realData('student')->count(),
        ];

        return view('dashboard', $data);
    }

    public function dutyDelete($duty)
    {
        $duty = Duty::realData()->find($duty);

        if ($duty == null) return response()->view("errors.403", [], 403);

        $duty->delete();

        return redirect()->back();
    }

    public function test()
    {
        $equality = Carbon::parse('19:00')->getTimestamp() > Carbon::now()->getTimestamp();
        dd($equality);
//        (new LessonDaysFixer())->fixYearDays();
//        (new LessonDaysFixer())->fixLessonDays();

//        date_default_timezone_set('Asia/Baku');
//        $equality = Carbon::parse('17:12')->getTimestamp() > Carbon::now()->getTimestamp();
//        return ['type' => $equality];
    }

    public function getMyInfo()
    {
       $authType = request('authType');

       if (!request()->filled('authType') || request('authType') == ""){
            return response()->json(['status' => 'error','message' => 'Parameters are incorrect']);
       }

        $user = auth($authType)->user();

        if ($user == null){
            return response()->json(['status' => 'error','message' => 'Not found user']);
        }

        $selectedUser = null;
        if ($user->user_type == "teacher") {
            $selectedUser = User::where('users.tenant_id',$user->tenant_id)
                ->join('lessons', 'lessons.user_id', 'users.id')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name','users.user_type')
                ->first();
            $userGroups = User::where('users.tenant_id',$user->tenant_id)
                ->join('lessons','lessons.user_id','users.id')
                ->where('users.id',$user->id)
                ->select('lessons.letter_group_id as group_id')
                ->groupBy('lessons.letter_group_id')
                ->get();
        } else if ($user->user_type == "student") {
            $selectedUser = User::where('users.tenant_id',$user->tenant_id)
                ->join('letter_group_user', 'letter_group_user.user_id', 'users.id')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name','users.user_type')
                ->first();
            $userGroups = User::where('users.tenant_id',$user->tenant_id)
                ->join('letter_group_user','letter_group_user.user_id','users.id')
                ->where('users.id',$user->id)
                ->select('letter_group_user.letter_group_id as group_id')
                ->groupBy('letter_group_user.letter_group_id')
                ->get();

        } else if ($user->user_type == "parent") {
            $selectedUser = User::where('users.tenant_id',$user->tenant_id)
                ->join('person_families', 'person_families.parent_id', 'users.id')
                ->join('letter_group_user', 'letter_group_user.user_id', 'person_families.user_id')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name','users.user_type')
                ->first();

            $userGroups = User::where('users.tenant_id',$user->tenant_id)
                ->join('person_families', 'person_families.parent_id', 'users.id')
                ->join('letter_group_user','letter_group_user.user_id','person_families.user_id')
                ->where('users.id',$user->id)
                ->select('letter_group_user.letter_group_id as group_id')
                ->groupBy('letter_group_user.letter_group_id')
                ->get();

        } else if ($user->user_type == "user") {
            $selectedUser = User::where('users.tenant_id',$user->tenant_id)
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name','users.user_type')
                ->first();
            $userGroups = [];
        }

        $selectedUser['school_management_system_session'] = $_COOKIE['school_management_system_session'];
        $selectedUser['groups'] = $userGroups;

        return response()->json(['status' => 'success','user' => $selectedUser]);
    }

    public function saveMessage()
    {
        $validator = validator(request()->all(), [
           'sender_id' => 'required|integer|exists:users,id',
            //'recivier_id' => 'nullable|integer|exists:users,id',
           'message' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error']);
        } else {

            $type = request('type');
            $sender_id = request('sender_id');
            $recivier_id = request('recivier_id');
            $senderUser = User::find($sender_id);
            $sendingMessage = request('message');

            if ($senderUser == null){
                return response()->json(['status' => 'error','error' => 'Parameters is incorrect [user not found]']);
            }

            if ($type == 'private') {
                $message = new Chat();
                $message->sender_id = $sender_id;
                $message->reciever_id = $recivier_id;
                $message->message = $sendingMessage;
                $message->tenant_id = $senderUser->tenant_id;
                $message->date = Carbon::now()->format('Y-m-d H:i:s');
                $message->save();

                //add notification
                $notifyObj = new Notification();
                $notifyObj->user_id = $recivier_id;
                $notifyObj->content = $senderUser->fullname() . " sizə mesaj göndərdi";
                $notifyObj->icon = "fa fa-comment";
                $notifyObj->color = "#333F57";
                $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                $notifyObj->route = route('portal_chat', ['tab' => 'chat', 'rec_id' => $sender_id]);
                $notifyObj->save();
            } else if ($type == 'group') {
                $message = new Chat();
                $message->sender_id = $sender_id;
                $message->letter_group_id = $recivier_id;
                $message->message = $sendingMessage;
                $message->tenant_id = $senderUser->tenant_id;
                $message->date = Carbon::now()->format('Y-m-d H:i:s');
                $message->save();


                //add notification all group users
               foreach (HomeController::getUsersForLetterGroup($recivier_id) as $user) {
                    $letterGroup = LetterGroup::find($recivier_id);
                    $notifyObj = new Notification();
                    $notifyObj->user_id = $user->id;
                    $notifyObj->content = $letterGroup->class_letter->msk_class->name . "/" . $letterGroup->class_letter->name . " qrupundan mesaj göndərildi";
                    $notifyObj->icon = "fa fa-comment";
                    $notifyObj->color = "#333F57";
                    $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                    $notifyObj->route = route('portal_chat', ['tab' => 'groups', 'rec_id' => $recivier_id]);
                    $notifyObj->save();
                }
            }
        }
        return response()->json(['status' => 'success', 'notifyObj' => $notifyObj]);
    }


    public function getUsersForLetterGroup($letter_group_id)
    {
        // group users listing
        $groupUsers = User::where('users.user_type','teacher')
            ->join('lessons', 'lessons.user_id', 'users.id')
            ->where('lessons.letter_group_id', $letter_group_id)
            ->select('users.id as id',
                'users.name as name',
                'users.surname as surname',
                'users.middle_name as middle_name',
                'users.user_type as user_type',
                'users.thumb as thumb')
            ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            ->unionAll(
                User::where('users.user_type','student')
                    ->join('letter_group_user', 'letter_group_user.user_id', 'users.id')
                    ->where('letter_group_user.letter_group_id', $letter_group_id)
                    ->select('users.id as id',
                        'users.name as name',
                        'users.surname as surname',
                        'users.middle_name as middle_name',
                        'users.user_type as user_type',
                        'users.thumb as thumb')
                    ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            )->unionAll(
                User::where('users.user_type','parent')
                    ->join('person_families', 'person_families.parent_id', 'users.id')
                    ->join('letter_group_user', 'letter_group_user.user_id', 'person_families.user_id')
                    ->where('letter_group_user.letter_group_id', $letter_group_id)
                    ->select('users.id as id',
                        'users.name as name',
                        'users.surname as surname',
                        'users.middle_name as middle_name',
                        'users.user_type as user_type',
                        'users.thumb as thumb')
                    ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            )->unionAll(
                User::where('users.user_type','user')
                    ->select('users.id as id',
                        'users.name as name',
                        'users.surname as surname',
                        'users.middle_name as middle_name',
                        'users.user_type as user_type',
                        'users.thumb as thumb')
                    ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            )->get();

        return $groupUsers;
    }
}
