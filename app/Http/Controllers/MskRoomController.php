<?php

namespace App\Http\Controllers;

use App\Models\Corpus;
use App\Models\MskRoomType;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskRoomController extends Controller
{
    public function index(Request $request)
    {
        $corpuses = Corpus::realData()->get();

        $data = [
            'corpuses' => $corpuses,
            'request' => $request
        ];

        return view('msk.rooms.rooms', $data);
    }

    public function roomAddEditAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'capacity' => 'required|integer|max:300|min:1',
            'type' => 'required|integer',
            'info' => 'nullable|string',
            'id' => 'required|integer',
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,' . Auth::user()->tenant_id,
            'subjects' => 'array|nullable',
            'subjects.*' => 'nullable|integer|exists:subjects,id,tenant_id,'. Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($request->get('id') == 0)
            {
                $newRoom = new Room();
            }
            else
            {
                $newRoom = Room::realData()->find($request->get('id'));
            }

            $newRoom->name = $request->get('name');
            $newRoom->capacity = $request->get('capacity');
            $newRoom->type = $request->get('type');
            $newRoom->info = $request->get('info');
            $newRoom->tenant_id = Auth::user()->tenant_id;
            $newRoom->corpus_id = $request->get('corpus_id');
            $newRoom->save();

            //subjects
            $newRoom->subjects()->detach();
            foreach ($request->get('subjects', []) as $subject)
                $newRoom->subjects()->attach($subject);

            return response()->json(['status'=>'ok', "data"=> array_merge($newRoom->toArray(),['room_type' => MskRoomType::realData()->find($newRoom->type)->name], ['subjects'=> implode(",", $newRoom->subjects->pluck('name')->toArray()), 'subjects_id'=> $newRoom->subjects->pluck('id')->toArray()] ) ]);
        }
    }

    public function roomDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:rooms,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $room = Room::realData()->find($request->get('id'));
            $room->delete();

            return response()->json(['status'=>'ok']);
        }
    }

    public function roomGetDataForSelect(Request $request){

        $results = [];

        if($request->has('ne') && $request->get('ne') == "msk_room_types"){

            $roomTypes = MskRoomType::realData()->get();

            foreach ($roomTypes as $roomType){
                $results[] = ['id' => $roomType->id,'text' => $roomType->name];
            }

            return response()->json(['results' => $results]);
        }
    }
}
