<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Library\YearDays;
use App\Models\Folder;
use App\Models\FolderImages;
use App\Library\Standarts;
use App\Models\FolderUsers;
use App\Models\Notification;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class GaleryController extends Controller
{
    public function index()
    {
        $folders = Folder::realData()->get();
        $data = [
            'folders' => $folders
        ];
        return view('galery.galery', $data);
    }

    public function addFolderModal($folder_id)
    {
        if ($folder_id == 0) {
            $folderObj = new Folder();
        } else {
            $folderObj = Folder::realData()->find($folder_id);
        }

        $currentYear = YearDays::getCurrentYear();

        $students = User::realData()
            ->leftJoin('letter_group_user','letter_group_user.user_id','users.id')
//            ->leftJoin('letter_group_user', function ($join) use($currentYear){
//                $join->on('letter_group_user.user_id', 'users.id')->on('letter_group_user.year_id', DB::raw((int)$currentYear['id']));
//            })
            ->leftJoin('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
            ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->where('user_type', 'student')
            ->select('users.*', 'class_letters.name as letter_name', 'msk_classes.name as class_name')
            ->get();

//        dd(User::realData()
//            ->leftJoin('letter_group_user', function ($join) use($currentYear){
//                $join->on('letter_group_user.user_id', 'users.id')->on('letter_group_user.year_id', DB::raw((int)$currentYear['id']));
//            })
//            ->leftJoin('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
//            ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
//            ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
//            ->where('user_type', 'student')
//            ->select('users.*', 'class_letters.name as letter_name', 'msk_classes.name as class_name')->toSql());

        $studentGroups = [];
        foreach ($students as $student){
            $cName = $student['class_name'] . $student['letter_name'];
            if(trim($cName) === "") $cName = "Qrupsuz";

            if(!isset($studentGroups[$cName])) $studentGroups[$cName] = [];

            $studentGroups[$cName][] = $student;
        }



        $parents = User::realData()
            ->leftJoin('person_families', 'person_families.parent_id', 'users.id')
            ->leftJoin('letter_group_user','letter_group_user.user_id','person_families.user_id')
//            ->leftJoin('letter_group_user', function ($join) use($currentYear){
//                $join->on('letter_group_user.user_id', 'person_families.user_id')->on('letter_group_user.year_id', DB::raw((int)$currentYear['id']));
//            })
            ->leftJoin('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
            ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->where('user_type', 'parent')
            ->select('users.*', 'class_letters.name as letter_name', 'msk_classes.name as class_name')
            ->get();

        $parentsGroups = [];
        foreach ($parents as $parent){
            if ($parent->name != "" || $parent->surname != ""){
                $cName = $parent['class_name'] . $parent['letter_name'];
                if(trim($cName) === "") $cName = "Qrupsuz";
                if(!isset($parentsGroups[$cName])) $parentsGroups[$cName] = [];
                if(!isset($parentsGroups[$cName][$parent->id])) $parentsGroups[$cName][$parent->id] = [];
                $parentsGroups[$cName][$parent->id] = $parent;
            }
        }

        $permitedUsersId = [];
        if($folderObj !== null){
            $permitedUsers = $folderObj->permitedUsers;
            foreach ($permitedUsers as $permitedUser){
                $permitedUsersId[$permitedUser->user_id] = true;
            }
        }

        $data = [
            'folder_id' => $folder_id,
            'folderObj' => $folderObj,
            'studentGroups' => $studentGroups,
            'parentsGroups' => $parentsGroups,
            'permitedUsersId' => $permitedUsersId
        ];

        return view('modals.galery.galery_add_edit', $data);
    }

    public function saveFolderAction(Request $request, $folder_id)
    {
        $validator = Validator::make($request->all(), [
            'folder_name' => 'required|string',
            'teachers' => 'nullable|array',
            'teachers.*' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id,
            'parents' => 'nullable|array',
            'parents.*' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id,
            'students' => 'nullable|array',
            'students.*' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();
            return response()->json(['status'=>'error', 'errors' => $errors]);
        } else {
            if ($folder_id == 0) {
                $folderObj = new Folder();
            } else {
                $folderObj = Folder::realData()->find($folder_id);
            }

            $folderObj->name = $request->get('folder_name');
            $folderObj->tenant_id = auth()->user()->tenant_id;
            $folderObj->save();

            $folderObj->permitedUsers()->delete();
            if($request->has('teachers'))
                foreach (array_unique($request->get('teachers')) as $teacherId){
                    $permitedUser = new FolderUsers();
                    $permitedUser->tenant_id = auth()->user()->tenant_id;
                    $permitedUser->user_id = $teacherId;

                    $folderObj->permitedUsers()->save($permitedUser);

                    $notifyObj = new Notification();
                    $notifyObj->user_id = $teacherId;
                    $notifyObj->content = $request->get('folder_name');
                    $notifyObj->icon = "fa fa-picture-o";
                    $notifyObj->color = "#e65152";
                    $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                    $notifyObj->route = route('portal_galery');
                    $notifyObj->save();

//                    event(new NotificationEvent($notifyObj));
                }

            if($request->has('parents'))
                foreach (array_unique($request->get('parents')) as $teacherId){
                    $permitedUser = new FolderUsers();
                    $permitedUser->tenant_id = auth()->user()->tenant_id;
                    $permitedUser->user_id = $teacherId;

                    $folderObj->permitedUsers()->save($permitedUser);

                    $notifyObj = new Notification();
                    $notifyObj->user_id = $teacherId;
                    $notifyObj->content = $request->get('folder_name');
                    $notifyObj->icon = "fa fa-picture-o";
                    $notifyObj->color = "#e65152";
                    $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                    $notifyObj->route = route('portal_galery');
                    $notifyObj->save();

//                    event(new NotificationEvent($notifyObj));
                }
            if($request->has('students'))
                foreach (array_unique($request->get('students')) as $teacherId){
                    $permitedUser = new FolderUsers();
                    $permitedUser->tenant_id = auth()->user()->tenant_id;
                    $permitedUser->user_id = $teacherId;

                    $folderObj->permitedUsers()->save($permitedUser);

                    $notifyObj = new Notification();
                    $notifyObj->user_id = $teacherId;
                    $notifyObj->content = $request->get('folder_name');
                    $notifyObj->icon = "fa fa-picture-o";
                    $notifyObj->color = "#e65152";
                    $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                    $notifyObj->route = route('portal_galery');
                    $notifyObj->save();

//                    event(new NotificationEvent($notifyObj));
                }

            return response()->json(['status' => 'ok']);
        }
    }

    public function loadImagesPage(Request $request)
    {
        $folder_id = $request->get('id');
        $images = Folder::realData()->find($folder_id)->images;
        $data = [
            'folder_id' => $folder_id,
            'images' => $images,
            'showedImages' => []
        ];

        return view('pages.galery.galery_images', $data);
    }

    public function saveImages(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);

        if ($validator->fails()) {

        } else {
            $files = $request->all()['files'];
            $folderObj = Folder::realData()->find($request->get('folder_id'));
            foreach ($files as $file) {
                $filePath = time().$file->getClientOriginalName();
                $file->move(public_path(Standarts::$optionFilesDir),$filePath);
                $imageFolderObj = new FolderImages();
                $imageFolderObj->image = $filePath;
                $imageFolderObj->folder_id = $request->get('folder_id');
                $imageFolderObj->save();
            }

            $data = [
                'images' => $folderObj->images
            ];

            return view('pages.galery.galery_images', $data);
        }
    }

    public function deleteFolder(Request $request){
        $validator = Validator::make($request->all(),[

        ]);

        if ($validator->fails()){

        }
        else{
            $folderObj = Folder::find($request->get('id'));
            $folderObj->delete();

            return response()->json(['status' => 'ok']);
        }
    }

    public function deleteImages(Request $request){
        $validator = Validator::make($request->all(),[

        ]);

        if ($validator->fails()){

        }
        else{
            $imageFolderObj = FolderImages::find($request->get('id'));
            @unlink(public_path(Standarts::$optionFilesDir.$imageFolderObj['image']));
            $imageFolderObj->delete();
        }
    }
}
