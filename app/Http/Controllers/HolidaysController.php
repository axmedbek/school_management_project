<?php

namespace App\Http\Controllers;

use App\Library\YearDays;
use App\Models\Holiday;
use App\Models\MskClass;
use App\Models\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HolidaysController extends Controller
{
    public function index(Request $request){

        $years = Year::realData()->orderByDesc('id')->get();

        $data = [
          'years' => $years,
          'request' => $request
        ];

        return view('msk.holidays.holidays',$data);
    }

    public function AddEditAction(Request $request){

        $validators = Validator::make($request->all(),[
            'name' => 'required|string|max:60',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'type' => 'required',
            'id' => 'required|integer',
            'year_id' => 'required|integer|exists:years,id,tenant_id,'. Auth::user()->tenant_id
        ]);


        if ($validators->fails()){
            $errors = View::make('modals.modal_errors',['errors' => $validators->errors() ])->render();

            return response()->json(['status'=> 'ok','errors' => $errors]);
        }

        else{
            if ($request->get('id') == 0){
                $holiday = new Holiday();
            }
            else{
                $holiday = Holiday::realData()->find($request->get('id'));
            }

            $start_date = strtotime($request->get('start_date'));
            $end_date = strtotime($request->get('end_date'));



            if ($start_date > $end_date){
                return redirect()->back();
            }
            $holiday->name = $request->get('name');
            $holiday->start_date = date('Y-m-d',$start_date);
            $holiday->end_date = date('Y-m-d',$end_date);
            $holiday->type = $request->get('type');
            $holiday->year_id = $request->get('year_id');
            $holiday->tenant_id = Auth::user()->tenant_id;
            $holiday->save();

            $holiday->holiday_classes()->detach();
            if ($request->has('classes') && is_array($request->get('classes'))){
                if(array_search("0",$request->get('classes')) === false){
                    foreach ($request->get('classes') as $class){
                        $holiday->holiday_classes()->attach($class);
                    }

                }else{
                    foreach (MskClass::realData()->get() as $class){
                        $holiday->holiday_classes()->attach($class);
                    }
                }
            }
            //weeks calc
            $year = $holiday->year;
            $year->save();
            $data = $holiday->toArray();
            $data['classNames'] = implode(",", $holiday->holiday_classes->pluck('name')->toArray());
            $data['classId'] = $holiday->holiday_classes->pluck('id');

            (new YearDays($year))->calculateDays();

            return response()->json(['status' => 'ok','data' => $data]);
        }

    }

    public function Delete(Request $request){


            $validators = Validator::make($request->all(),[
                'id' => 'required|integer|exists:holidays,id,tenant_id,'.Auth::user()->tenant_id
            ]);


            if ($validators->fails()){

                $errors = View::make('modals.modal_errors',['errors' => $validators->errors() ])->render();

                return response()->json(['status' => 'error', 'errors' => $errors]);
            }

            else{

                $holiday = Holiday::realData()->find($request->get('id'));
                $year = $holiday->year;
                $holiday->delete();

                (new YearDays($year))->calculateDays();

                return response()->json(['status' => 'ok']);
            }
        }


}
