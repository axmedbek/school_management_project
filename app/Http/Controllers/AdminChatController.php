<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\LetterGroup;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class AdminChatController extends Controller
{
    public function index()
    {
        $users = User::realData()
            ->where('id', '!=', auth()->user()->id)
            ->where('user_type', '!=', 'staff');

        $groups = LetterGroup::realData()
            ->join('class_letters', 'letter_groups.class_letter_id', 'class_letters.id')
            ->join('msk_classes', 'class_letters.msk_class_id', 'msk_classes.id')
            ->select('letter_groups.id as id','letter_groups.name as name','class_letters.name as class_letter','msk_classes.name as msk_class')
            ->groupBy('letter_groups.id','letter_groups.name','class_letters.name','msk_classes.name');

        if (request('type') == 'ajax') {
            if (request('searchType') == 'private') {
                $datas = $users->where(DB::raw("concat(name,' ',surname,' ',middle_name)"), 'like', '%' . request('searchedVal') . '%')->get();
            } else if (request('searchType') == 'group') {
                $datas = $groups->where(DB::raw("concat(msk_classes.name,'/',class_letters.name,' ( ',letter_groups.name,' ) ')"), 'like', '%' . request('searchedVal') . '%')->get();
            }
            $htmlContent = self::htmlListCreator($datas,request('searchType'));
            return response()->json(['status' => 'ok', 'data' => $htmlContent]);
        }

        $data = [
            'users' => $users->get(),
            'groups' => $groups->get()
        ];
        return view('chat.admin_chat', $data);
    }

    public function getMessages()
    {
        $sender_id = request('sender_id');
        $recivier_id = request('recivier_id');
        $type = request('type');

        //$newMessageCount = ChatController::makeReadedSms($sender_id,$recivier_id,$type);

        if ($type == "private") {
            $messages = Chat::realData()
                ->where(function ($query) use ($sender_id, $recivier_id) {
                    $query->where('sender_id', $sender_id);
                    $query->where('reciever_id', $recivier_id);
                })
                ->orWhere(function ($query) use ($sender_id, $recivier_id) {
                    $query->where('sender_id', $recivier_id);
                    $query->where('reciever_id', $sender_id);
                })
                ->get();
        } else if ($type == "group") {
            $messages = Chat::realData()
                ->where('letter_group_id', $recivier_id)
                ->get();
        }

        $messageContent = "";
        foreach ($messages as $message) {
            if ($sender_id == $message->sender_id) {
                $messageContent .= "<div class=\"message clearfix\">
                        <div class=\"chat-bubble from-me\">
                           " . $message->message . "
                        </div><br>
                        <div style='float:right;font-size: 11px;'>" . Carbon::parse($message->date)->format('Y-m-d H:i') . "</div>
                    </div>";
            } else {
                $messageContent .= "<div class=\"message clearfix\" style='padding: 15px;'>
                        <div style='margin-left: 8px;background-color: antiquewhite;width: 230px;
    padding: 5px;padding-left: 15px;border-radius: 30px;'>" . $message->senderUser->fullname() . "</div>
                        <div class=\"profile-img-wrapper m-t-5 inline\">
                            <img class=\"col-top\" width=\"30\" height=\"30\"
                                 src=\"" . asset('images/user/thumb/' . $message->senderUser->thumb) . "\" alt=\"\"
                                 data-src=\"" . asset('images/user/thumb/' . $message->senderUser->thumb) . "\"
                                 data-src-retina=\"" . asset('images/user/thumb/' . $message->senderUser->thumb) . "\">
                        </div>
                        <div class=\"chat-bubble from-them\">
                            " . $message->message . "
                        </div>
                        <br>
                        <div style='font-size: 10px;margin-left: 40px;'>" . Carbon::parse($message->date)->format('Y-m-d H:i') . "</div>
                    </div>";
            }
        }
        return response()->json(['status' => 'ok', 'messages' => $messageContent]);
    }

    public static function htmlListCreator($datas,$type)
    {
        $tr = "";
        foreach ($datas as $data) {
            $statusTitle = "";
            $userImage = "";
            if ($type == 'private'){
                switch ($data->user_type) {
                    case('student') :
                        $statusTitle = "Şagird";
                        break;
                    case('teacher') :
                        $statusTitle = "Müəllim";
                        break;
                    case('user') :
                        $statusTitle = "Admin";
                        break;
                    case('parent') :
                        $statusTitle = "Valideyn";
                        break;
                }
                $userImage = "<span class=\"thumbnail-wrapper d32 circular bg-success\" style=\"margin-left: 10px;margin-top: 10px;\">
<img width=\"34\" height=\"34\" alt=\"\" data-src-retina=\"" . asset('images/user/thumb/' . $data->thumb) . "\"
     data-src=\"" . asset('images/user/thumb/' . $data->thumb) . "\" src=\"" . asset('images/user/thumb/' . $data->thumb) . "\"
     class=\"col-top\">
</span>";
            }

            $tr .= "<li data-id=\"" . $data->id . "\" class=\"chat-user-list clearfix\" style=\"margin-top: 10px;
                                            border-radius: 4px;
                                            border: 2px solid #d7deda;
                                            list-style-type: none;
                                            margin-left: -40px !important;
                                            cursor: pointer;
                                            background-color: #efeff0;
                                            ".($type == 'private' ? 'padding-bottom: 10px;padding-top: 5px;' : '')."\">
                                            <a>
                                            ".$userImage."
                                                <p class=\"p-l-10 \"
                                                   style=\"margin-top: 5px;".($type == 'private' ? 'padding-left: 60px !important;' : 'padding: 4px;')."\">
                                                    <span class=\"text-master name-title\">" . ($type == 'private' ? $data->fullname() : $data->msk_class.'/'.$data->class_letter.' ( '.$data->name.' )'). "</span>
                                                    <span class=\"block text-master hint-text fs-12 status-title\">
                                                      " . $statusTitle . "
                                                    </span>
                                                </p>
                                            </a>
                                        </li>";
        }

        return $tr;
    }

}
