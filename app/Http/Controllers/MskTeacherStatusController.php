<?php

namespace App\Http\Controllers;

use App\Models\MskTeacherStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Request;

class MskTeacherStatusController extends Controller
{

    public function index(Request $request){

        $data = [
            'teacher_statuses' => MskTeacherStatus::realData()->orderByDesc('id')->get(),
            'request' => $request

        ];
        return view('msk.teachers.teacher_statuses',$data);
    }

    public function msk_teacher_statuses_AddEdit_Action(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:60',
            'id'=>'required|integer',
        ]);

        if($validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{
            if($request->get('id')==0){
                $newTeacherStatus = new MskTeacherStatus();
            }
            else{
                $newTeacherStatus = MskTeacherStatus::realData()->find($request->get('id'));
            }

            $newTeacherStatus->name = $request->get('name');
            $newTeacherStatus->tenant_id = Auth::user()->tenant_id;
            $newTeacherStatus->save();

            $checkTeacherStatus = \App\User::realData()->where('msk_teacher_status_id',$newTeacherStatus->id)->first();
            if(!isset($checkTeacherStatus)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $newTeacherStatus["buttons"] = $buttons;


            return response()->json(['status'=>'ok','data'=>$newTeacherStatus->toArray()]);
        }


    }

    public function msk_teacher_statuses_Delete_Action(Request $request){

        $validator = Validator::make($request->all(),[
            'id'=>'required|integer|exists:msk_teacher_statuses,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'ok','errors'=>$errors]);
        }
        else{

            $teacher_status = MskTeacherStatus::realData()->find($request->get('id'));
            $teacher_status->delete();


            return response()->json(['status'=>'ok']);

        }

    }
}
