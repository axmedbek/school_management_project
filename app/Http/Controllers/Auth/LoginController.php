<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Library\Standarts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/";

    public function username(): string
    {
        return 'username';
    }

    public function login(Request $request): \Illuminate\Http\RedirectResponse
    {
        if (auth('web')->attempt($this->credentials($request))) {
            return redirect()->route('dashboard');
        }
        else {
            return redirect()->back()->withErrors(['login' => 'Username or password incorrect']);
        }
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    protected function credentials(Request $request): array
    {
        return array_merge($request->only($this->username(), 'password'), ['user_type' => 'user']);
    }

    public function logout(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->back();
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    protected function authenticated(Request $request, $user)
    {
        $continue = true;
        foreach (Standarts::$modules as $key => $item) {
            if ($continue) {
                if (!isset($item['child'])) {
                    if (!Helper::has_priv($item['route'], $item['priv']))
                        continue;
                    $this->redirectTo = route($item['route']);
                    $continue = false;
                } else {
                    foreach ($item['child'] as $ch) {
                        if (!Helper::has_priv($ch['route'], $ch['priv']))
                            continue;
                        $this->redirectTo = route($ch['route']);
                        $continue = false;
                    }
                }
            }
        }
        if ($continue) {
            $this->redirectTo = route('admin_logout');
        }
    }
}
