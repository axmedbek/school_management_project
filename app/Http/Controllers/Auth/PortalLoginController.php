<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PortalLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:portal')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.portal_login');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function username(): string
    {
        return 'username';
    }

    protected function guard()
    {
        return Auth::guard('portal');
    }

    protected function credentials(Request $request): array
    {
        return array_merge($request->only($this->username(), 'password'));
    }

    public function logout(Request $request): \Illuminate\Http\RedirectResponse
    {
//        Helper::makeOnlineUserOrOffline('portal',Carbon::now(),0);
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->back();
    }

    public function login(Request $request): \Illuminate\Http\RedirectResponse
    {
        app()->setLocale('az');

        if (auth()->attempt($this->credentials($request))) {
            if (Auth::guard('portal')->user() &&
                Auth::guard('portal')->user()->user_type == "user") {

                Auth::guard('portal')->logout();

                return redirect()->back()->withErrors(['login' => 'You are not right access']);
            }
            else {
                dd("okey");
            }
        } else {
            return redirect()->back()->withErrors(['login' => 'Username or password incorrect']);
        }
    }

//    protected function authenticated(Request $request, $user)
//    {
//        Helper::makeOnlineUserOrOffline('portal',Carbon::now(),1);
//    }

}
