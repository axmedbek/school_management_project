<?php

namespace App\Http\Controllers;

use App\Library\Standarts;
use App\Models\Corpus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class CorpusController extends Controller
{
    public function corpus(Request $request)
    {
        $corpuses = Corpus::realData()->orderBy('id', 'DESC')->get();

        $data = [
            'corpuses' => $corpuses,
            'request' => $request
        ];

        return view('corpuses.corpuses', $data);
    }

    public function corpusDelete($corpus)
    {
        $corpus = Corpus::realData()->find($corpus);

        if($corpus == null) return response()->view("errors.403",[],403);

        $corpus->delete();

        return redirect()->back();
    }

    public function corpusAddEditModal($corpus)
    {
        $data = [
            'corpus' => Corpus::realData()->find($corpus),
            'id' => $corpus
        ];

        return view('modals.corpuses.corpuses_add_edit', $data);
    }

    public function corpusAddEditAction(Request $request, $corpus)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'thumb'    => 'nullable|image|max:3000',
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($corpus == 0)
            {
                $newCorpus = new Corpus();
            }
            else
            {
                $newCorpus = Corpus::realData()->find($corpus);
            }

            $newCorpus->name = $request->get('name');
            $newCorpus->tenant_id = Auth::user()->tenant_id;

            if($request->hasFile('thumb'))
            {
                $newCorpus->clearPic();
                $newName = uniqid() . "." . $request->file('thumb')->extension();

                $request->file('thumb')->move(public_path(Standarts::$corpusImageDir), $newName);

                $newCorpus->thumb = $newName;
            }

            $newCorpus->save();

            return response()->json(['status'=>'ok']);
        }
    }

    public function infoModal($corpus)
    {
        $data = [
            'corpus' => Corpus::realData()->find($corpus),
            'id' => $corpus
        ];

        return view('modals.corpuses.corpuses_info', $data);
    }
}
