<?php

namespace App\Http\Controllers;

use App\Models\MskStudyLevels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskStudyLevelController extends Controller
{
    public function index(Request $request){

        $studyLevels = MskStudyLevels::realData()->orderByDesc('id')->get();

        $data = [
            'studyLevels' => $studyLevels,
            'request' => $request
        ];

        return view('msk.study_levels.study_levels',$data);
    }



    public function AddEdit_Action(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:30',
            'id' => 'required|integer',
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            if($request->get('id')==0){
                $studyLevels = new MskStudyLevels();
            }
            else{
                $studyLevels = MskStudyLevels::realData()->find($request->get('id'));
            }

            $studyLevels->name = $request->get('name');
            $studyLevels->tenant_id = Auth::user()->tenant_id;
            $studyLevels->save();

            $checkStudyLevels = \App\User::realData()->where('msk_study_levels_id',$studyLevels->id)->first();
            if(!isset($checkStudyLevels)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $studyLevels["buttons"] = $buttons;


            return response()->json(['status'=>'ok','data'=>$studyLevels->toArray()]);

        }
    }

    public function Delete_Action(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:msk_study_levels,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);

        }
        else{

            $studyLevels = MskStudyLevels::realData()->find($request->get('id'));
            $studyLevels->delete();


            return response()->json(['status'=>'ok']);


        }
    }
}
