<?php

namespace App\Http\Controllers;

use App\Exports\ReportTableExport;
use App\Library\Standarts;
use App\Models\ClassLetter;
use App\Models\ClassTime;
use App\Models\LetterGroup;
use App\Models\MskClass;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Excel;

class ReportTableController extends Controller
{
    public function index()
    {
        return view('reports.table.table');
    }

    public function getDataForSelect(Request $request)
    {

        $results = [];

        if ($request->has('ne') && $request->get('ne') == "getClasses") {

            $classes = MskClass::realData()->orderBy('order')->get();
            foreach ($classes as $class) {
                $results[] = ['id' => $class['id'], 'text' => $class['name']];
            }
        } else if ($request->has('ne') && $request->get('ne') == "getClassLetters") {

            $classes = explode(",", $request->get('classes'));

            $class_letters = ClassLetter::realData()
                ->where('corpus_id', $request->get('corpus_id'))
                ->where('year_id', $request->get('year_id'))
                ->whereIn('msk_class_id', $classes)->get();

            foreach ($class_letters as $class_letter) {
                $results[] = ['id' => $class_letter['id'], 'text' => $class_letter['name'] . " (Sinif : " . $class_letter->msk_class->name . " )"];
            }

        } else if ($request->has('ne') && $request->get('ne') == "getGroups") {

            $class_letters = explode(",", $request->get('letters'));

            $class_groups = LetterGroup::realData()
                ->whereIn('class_letter_id', $class_letters)->get();

            foreach ($class_groups as $class_group) {
                $results[] = ['id' => $class_group['id'], 'text' => $class_group['name'] . " ( Sinif : " . $class_group->class_letter->msk_class->name . "" . $class_group->class_letter->name . ")"];
            }

            //dd($class_letters);

        }

        return response()->json(['results' => $results]);
    }

    public function tablePage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . Auth::user()->tenant_id,
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,' . Auth::user()->tenant_id,
            'class_id' => 'nullable|array|',
            'class_id.*' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
            'letter_id' => 'nullable|array|',
            'letter_id.*' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::user()->tenant_id,
            'group_id' => 'nullable|array|',
            'group_id.*' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {
            $classTimes = ClassTime::realData()->where('corpus_id', $request->get('corpus_id'))->orderBy('id', 'asc')->get();

            $groupsAll = LetterGroup::realData()
                ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->where('year_id', $request->get('year_id'))
                ->where('corpus_id', $request->get('corpus_id'));

            if ($request->has('class_id')) $groupsAll->whereIn('class_letters.msk_class_id', $request->get('class_id'));
            if ($request->has('letter_id')) $groupsAll->whereIn('class_letters.id', $request->get('letter_id'));
            if ($request->has('group_id')) $groupsAll->whereIn('letter_groups.id', $request->get('group_id'));

            $groupsAll->orderBy('msk_classes.order')->orderBy('class_letters.id');

            $groupsAll = $groupsAll->select('letter_groups.*', 'msk_classes.name as class_name', 'class_letters.name as letter_name')->get();

            Cache::put('ders_cedveli', ['groups' => $groupsAll, 'classTimes' => $classTimes], 360000);

            return View::make('pages.reports.table.table', ['groups' => $groupsAll, 'classTimes' => $classTimes])->render();
        }
    }


    public function exportToExcel()
    {
        $data = Cache::get('ders_cedveli');
        $myFile =  Excel::create('ders_cedveli', function ($excel) use ($data) {
            $excel->sheet('ders_cedveli', function ($sheet) use ($data) {
                $sira = 4; // baslangic row
                foreach ($data['groups'] as $group_key => $group) {
                    $lessonData = [];
                    $lessons = $group->lessons;
                    if($lessons != null) {
                        foreach ($lessons as $lesson) {
                            if (!isset($lessonData[$lesson->week_day])) $lessonData[$lesson->week_day] = [];
                            $lessonData[$lesson->week_day][$lesson->class_time_id] = $lesson;
                        }
                    }
                    $sheet->mergeCells('F'. $sira.':K'. $sira);
                    $sheet->cell('F'.$sira,function($cell){
                        $cell->setBackground('#e0bc86');
                        $cell->setBorder('thin','thin','thin','thin');
                    });
                    $sheet->setHeight($sira,40);
                    $sheet->cell('F' .$sira,'                                 Korpus : ' . $group->class_letter->corpus->name .'                    
                                Sinif : ' . $group->class_name . '' . $group->letter_name . ' - Qrup : ' . $group->name);
                    $sheet->getStyle('F' .$sira)->getAlignment('center')->setWrapText(true);

                    //hefteler
                    for ($i = 0; $i < 7; $i++) {
                        $sheet->cell('F'. ($sira+1),Standarts::$weekDayNames[$i]);

                        $sheet->mergeCells('F'. ($sira+1).':K'.($sira+1));
                        $sheet->cell('F'. ($sira+1).':K'.($sira+1),function($cell){
                            $cell->setBackground('#e8dcdc');
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell('F' . ($sira+2),function($cell){
                            $cell->setValue('Saat');
                            $cell->setFontWeight('bold');
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->mergeCells('G' . ($sira+2).':J'.($sira+2));
                        $sheet->cell('G' . ($sira+2), function ($cell){
                            $cell->setValue('Fenn');
                            $cell->setFontWeight('bold');
                            $cell->setBorder('thin','thin','thin','thin');
                        });

                        $sheet->cell('K' . ($sira+2),function ($cell){
                            $cell->setValue('Otaq');
                            $cell->setFontWeight('bold');
                            $cell->setBorder('thin','thin','thin','thin');
                        });

                        $dersSira = 1 ;
                        //dersler
                        foreach ($data['classTimes'] as $class_key => $class){
                            $chk = isset($lessonData[$i + 1]) && isset($lessonData[$i + 1][$class->id]) ? $lessonData[$i + 1][$class->id] : false;
                            if ($chk){
                                $sheet->cell('F' . ($sira+3+$class_key),$class_key + 1);
                                $sheet->cell('F' . ($sira+3+$class_key),function ($cell){
                                    $cell->setBorder('thin','thin','thin','thin');
                                });
                                $sheet->mergeCells('G' . ($sira+3+$class_key).':J'.($sira+3+$class_key));
                                $sheet->cell('G' . ($sira+3+$class_key), $chk->subject->name );
                                $sheet->cell('G' . ($sira+3+$class_key).':J'.($sira+3+$class_key),function ($cell){
                                    $cell->setBorder('thin','thin','thin','thin');
                                });
                                $sheet->cell('K' . ($sira+3+$class_key), $chk->room->name );
                                $sheet->cell('K' . ($sira+3+$class_key),function ($cell){
                                    $cell->setBorder('thin','thin','thin','thin');
                                });
                            }
                            else{
                                $sheet->cell('F' . ($sira+3+$class_key),$class_key + 1);
                                $sheet->cell('F' . ($sira+3+$class_key),function ($cell){
                                    $cell->setBorder('thin','thin','thin','thin');
                                });
                                $sheet->mergeCells('G' . ($sira+3+$class_key).':J'.($sira+3+$class_key));
                                $sheet->cell('G' . ($sira+3+$class_key), '-');
                                $sheet->cell('G' . ($sira+3+$class_key).':J'.($sira+3+$class_key),function ($cell){
                                    $cell->setBorder('thin','thin','thin','thin');
                                });
                                $sheet->cell('K' . ($sira+3+$class_key), '-');
                                $sheet->cell('K' . ($sira+3+$class_key),function ($cell){
                                    $cell->setBorder('thin','thin','thin','thin');
                                });
                            }
                            $dersSira = $dersSira + 1;
                        }

                        $sira  = $sira + $dersSira + 1 ;
                    }
                    $sira = $sira + 1;
                }

            });
        });
        $myFile = $myFile->string('xlsx');

        return response()->json([
            'loading' => false,
            'filename' => 'ders_cedveli.xlsx',
            'path' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile)
        ]);
    }
}
