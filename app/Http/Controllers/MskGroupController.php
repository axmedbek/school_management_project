<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;

class MskGroupController extends Controller
{
    public function index(Request $request)
    {
        $groups = Group::realData()->get();

        $data = [
            'groups' => $groups,
            'request' => $request
        ];

        return view('msk.groups.groups', $data);
    }

    public function groupDelete(Request $request, MessageBag $message_bag)
    {
        $hasError = false;

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:groups,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        $group = Group::realData()->find($request->get('id'));

        if(count($group->users) > 0){
            $validator->errors()->add('error', 'Bu qrupa bağlı istifadəçilər var!');
            $hasError = true;
        }

        if ($hasError || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $group->delete();

            return response()->json(['status'=>'ok']);
        }
    }

    public function groupAddEditModal($group)
    {
        $data = [
            'group' => Group::realData()->find($group),
            'id' => $group
        ];

        return view('modals.msk.groups.groups_add_edit', $data);
    }

    public function groupAddEditAction(Request $request, $group)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'available_modules' => 'array|required',
            'available_modules.*' => 'required|integer|in:1,2,3',
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($group == 0)
            {
                $newGroup = new Group();
            }
            else
            {
                $newGroup = Group::realData()->find($group);
            }

            $newGroup->group_name = $request->get('name');
            $newGroup->available_modules = json_encode($request->get('available_modules'));
            $newGroup->tenant_id = Auth::user()->tenant_id;
            $newGroup->save();

            return response()->json(['status'=>'ok']);
        }
    }
}
