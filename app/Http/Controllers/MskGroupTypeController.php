<?php

namespace App\Http\Controllers;

use App\Models\GroupType;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskGroupTypeController extends Controller
{
    public function index(Request $request)
    {
        $group_types = GroupType::realData()->orderByDesc('id')->get();

        $data = [
            'group_types' => $group_types,
            'request' => $request
        ];

        return view('msk.group_types.group_types', $data);
    }

    public function group_typeAddEditAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'id' => 'required|integer',
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($request->get('id') == 0)
            {
                $newGroupType = new GroupType();
            }
            else
            {
                $newGroupType = GroupType::realData()->find($request->get('id'));
            }

            $newGroupType->name = $request->get('name');
            $newGroupType->tenant_id = Auth::user()->tenant_id;
            $newGroupType->save();

            $checkGroupType = \App\Models\LetterGroup::realData()->where('group_type_id',$newGroupType->id)->first();
            if(!isset($checkGroupType)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $newGroupType["buttons"] = $buttons;
            return response()->json(['status'=>'ok', "data"=> $newGroupType->toArray()]);
        }
    }

    public function group_typeDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:group_types,id,default,0,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $room = GroupType::realData()->find($request->get('id'));
            $room->delete();

            return response()->json(['status'=>'ok']);
        }
    }


}
