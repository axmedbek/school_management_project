<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

class MskSunjectController extends Controller
{
    public function index(Request $request)
    {
        $subjects = Subject::realData()->orderByDesc('id')->get();

        $data = [
            'subjects' => $subjects,
            'request' => $request
        ];

        return view('msk.subjects.subjects', $data);
    }

    public function subjectAddEditAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:subjects,name,'.$request->get('id').',id,deleted_at,NULL',
            'id' => 'required|integer',
        ],[
            'unique' => 'Bu fənn artıq daxil edilib'
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($request->get('id') == 0)
            {
                $newSubject = new Subject();
            }
            else
            {
                $newSubject = Subject::realData()->find($request->get('id'));
            }

            $newSubject->name = $request->get('name');
            $newSubject->tenant_id = Auth::user()->tenant_id;
            $newSubject->date_of_use = $request->get('checkDateOfUse') == 1 ?  date('Y-m-d',strtotime($request->get('dateOfUse'))) : null;
            $newSubject->save();

            $buttons = "";
            $subjectArr = $newSubject->toArray();
            $checkSubjectHasInLessons = \App\Models\Lesson::realData()->where('subject_id',$subjectArr['id'])->get();
            if(!isset($checkSubjectHasInLessons[0])){
                $buttons = "<a type=\"button\" class=\"btn btn-primary col-lg-4 col-md-12 edit_data\"><i class=\"fa fa-pencil\"></i></a>
                            <a type=\"button\" class=\"btn btn-danger col-lg-4 col-md-12 data_delete\"><i class=\"fa fa-trash-o\"></i></a>";
                $subjectArr["buttons"] = $buttons;
            }
            else{
                $buttons = "<a type=\"button\" class=\"btn btn-primary col-lg-4 col-md-12 edit_data\"><i class=\"fa fa-pencil\"></i></a>";
                $subjectArr["buttons"] = $buttons;

            }

            return response()->json(['status'=>'ok', "data"=> $subjectArr]);
        }
    }

    public function subjectDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $room = Subject::realData()->find($request->get('id'));
            $room->delete();

            return response()->json(['status'=>'ok']);
        }
    }
}
