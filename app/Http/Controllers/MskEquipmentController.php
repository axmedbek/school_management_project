<?php

namespace App\Http\Controllers;

use App\Library\Standarts;
use App\Models\Corpus;
use App\Models\Equipment;
use App\Models\EquipmentTypes;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskEquipmentController extends Controller
{
    public function index(Request $request)
    {

        $equipments = Equipment::realData()->orderByDesc('equipment.id');

        $this->filter($equipments,$request);

        $data = [
            'eqps' => $equipments->paginate(Standarts::$rowCount),
            'request' => $request
        ];

        return view('msk.equipments.equipments', $data);
    }

    public function filter($object, $request)
    {
        if ($request->get('name')) $object->where('name', 'like', '%' . $request->get('name') . '%');
//        if ($request->get('model')) $object->where('equipment.model','like','%'.$request->get('model').'%');
//        if ($request->get('version')) $object->where('equipment.version','like','%'.$request->get('version').'%');
        if ($request->get('ip')) $object->where('ip','like','%'.$request->get('ip').'%');
//        if ($request->get('number')) $object->where('number','like','%'.$request->get('number').'%');
        if ($request->get('type')) $object->where('type',$request->get('type'));
        if ($request->get('eqp_type')) $object->where('eqp_type',$request->get('eqp_type'));
        if ($request->get('corpus')) $object->where('corpus_id',$request->get('corpus'));
//        if ($request->get('floor')) $object->where('floor','like','%'.$request->get('floor').'%');
        if ($request->get('rooms')) $object->where('room_id',$request->get('rooms'));
//        if ($request->get('destination')) $object->where('destination','like','%'.$request->get('destination').'%');

    }

    public function addEditModal($eqp)
    {
        $equipment = Equipment::realData()->find($eqp);
        $data = [
            'eqp' => $equipment,
            'id' => $eqp
        ];

        $dataForSelect = [];

        if ($eqp > 0) {

            $eType = EquipmentTypes::realData()->find($equipment->type);
            $eqpTypeName = '';
            switch ($equipment['eqp_type']) {
                case 1 :
                    $eqpTypeName = 'Giriş';
                    break;
                case 2 :
                    $eqpTypeName = 'Çıxış';
                    break;
                case 3 :
                    $eqpTypeName = 'Access Kontrol';
                    break;
                case 4 :
                    $eqpTypeName = 'Enroll';
                    break;
            }

            if ($equipment['room']['id'] != null) {
                $dataForSelect['room_id'] = $equipment->room->id;
                $dataForSelect['room_name'] = $equipment->room->name;
            } else {
                $dataForSelect['room_id'] = 0;
                $dataForSelect['room_name'] = '';
            }
            $dataForSelect['corpus_id'] = $equipment->corpus->id;
            $dataForSelect['corpus_name'] = $equipment->corpus->name;
            $dataForSelect['type_id'] = $eType->id;
            $dataForSelect['type_name'] = $eType->name;
            $dataForSelect['eqpTypeId'] = isset($equipment->eqp_type) ? $equipment->eqp_type : 0;
            $dataForSelect['eqpTypeName'] = $eqpTypeName;
        } else {
            $dataForSelect['corpus_id'] = 0;
            $dataForSelect['corpus_name'] = '';
            $dataForSelect['room_id'] = 0;
            $dataForSelect['room_name'] = '';
            $dataForSelect['type_id'] = 0;
            $dataForSelect['type_name'] = '';
            $dataForSelect['eqpTypeId'] = 0;
            $dataForSelect['eqpTypeName'] = '';
        }

        $data = array_merge($data, $dataForSelect);

        return view('modals.msk.equipment.equipment_add_edit', $data);
    }

    public function addEditAction(Request $request, $eqp)
    {

        $hasError = false;

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:120',
            'type' => 'required|integer',
            'corpus' => 'required|integer|exists:corpuses,id',
            //'room' => 'required|integer|exists:rooms,id',
        ]);

        if ($request->get('type') == 1) {
            if ($request->get('ip') == null) {
                $validator->errors()->add('error', 'IP Address daxil edilməlidir');
                $hasError = true;
            }
            if ($request->get('eqp_type') == null) {
                $validator->errors()->add('error', 'Avadanlıq təyinatı seçilməlidir');
                $hasError = true;
            }
        }

        if ($hasError || $validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();
            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {

            if ($eqp == 0) {
                $equipment = new Equipment();
            } else {
                $equipment = Equipment::realData()->find($eqp);
            }

            $equipment->name = $request->get('name');
            $equipment->model = $request->get('model');
            $equipment->version = $request->get('version');
            $equipment->ip = $request->get('ip');
            $equipment->type = $request->get('type');
            $equipment->eqp_type = $request->get('eqp_type');
            $equipment->corpus_id = $request->get('corpus');
            $equipment->number = $request->get('number');
            $equipment->floor = $request->get('floor');
            $equipment->room_id = $request->get('room');
            $equipment->destination = $request->get('destination');
            $equipment->tenant_id = Auth::user()->tenant_id;
            $equipment->save();

            $device = TblFpDevice::where('equipment_id', $equipment->id)->first();

            if (!$device) {
                $device = new TblFpDevice();
            }

            if ($equipment->type == 1 && $equipment->eqp_type != 4) {
                $device->equipment_id = $equipment->id;
                $device->device_ip = trim(str_replace(['_'], '', $request->get('ip')), ".");
                $device->device_type = $equipment->eqp_type == 1 ? 'In' : ($equipment->eqp_type == 2 ? 'Out' : '');
                $device->save();
            }

            return response()->json(['status' => 'ok']);
        }
    }

    public function deleteAction($eqp)
    {

        $equipment = Equipment::realData()->find($eqp);
        $equipment->delete();

        return redirect()->back();
    }

    public function getEquipmentData(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'ne' => 'required',
        ]);

        if ($validator->fails()) {

            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);

        } else {
            if ($request->get('ne') == 'getCorpuses') {

                $corpuses = Corpus::realData()->get();

                $results = [];
                foreach ($corpuses as $corpus) {
                    $results[] = ['id' => $corpus->id, 'text' => $corpus->name];
                }
            } else if ($request->get('ne') == 'getRooms') {

                $rooms = Room::realData()
                    ->where('name','like','%'.$request->get('q').'%')
                    ->where('corpus_id', $request->get('corpus_id'))->get();

                $results = [];
                foreach ($rooms as $room) {
                    $results[] = ['id' => $room->id, 'text' => $room->name];
                }
            } else if ($request->get('ne') == 'getEquipmentType') {
                $eTypes = EquipmentTypes::realData()->get();

                $results = [];
                foreach ($eTypes as $et) {
                    $results[] = ['id' => $et->id, 'text' => $et->name];
                }
            }
        }

        return response()->json(["results" => $results]);
    }

    public function refreshData(Equipment $eqp)
    {
        return redirect()->back();
    }

}
