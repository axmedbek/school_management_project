<?php

namespace App\Http\Controllers;

use App\Models\SmsTemplates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SmsTemplatesController extends Controller
{
    public function index(Request $request)
    {

        $smsTemplates = SmsTemplates::realData()->get();

        $data = [
            'smsTemplates' => $smsTemplates,
            'request' => $request
        ];

        return view('sms_templates.sms_templates', $data);
    }

    public function smsTemplateEditModal($smsTemplateId)
    {

        $smsTemplate = SmsTemplates::realData()->find($smsTemplateId);

        if ($smsTemplate == null) return response()->view('errors.403', [], 403);

        $data = [
            'smsTemplate' => $smsTemplate,
            'id' => $smsTemplateId
        ];

        return view('modals.sms_templates.sms_template_edit', $data);
    }

    public function smsTemplateEditAction(Request $request, $smsTemplateId)
    {

        $smsTemplate = SmsTemplates::realData()->find($smsTemplateId);

        if ($smsTemplate == null) return response()->view('errors.403', [], 403);

        $smsTemplate->text = $request->get('text');
        $smsTemplate->save();

        return response()->json(['status' => 'ok']);

    }

    public function doSmsTemplateDeActive(Request $request){
        $validator  = Validator::make($request->all(),[
            'id' => 'required|integer|exists:sms_templates,id',
            'active' => 'required|boolean'
        ]);

        if ($validator->fails())
        {
            return response()->json(['status' => 'error','errors'=>$validator->errors()]);
        }

        $smsTemplate = SmsTemplates::realData()->find($request->get('id'));
        if($smsTemplate == null )  return response()->json(['status' => 'error']);
        $smsTemplate->active = $request->get('active');
        $smsTemplate->save();
        return response()->json(['status' => 'ok','active'=>$smsTemplate->active]);


    }
}
