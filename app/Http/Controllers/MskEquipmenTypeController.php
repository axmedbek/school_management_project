<?php

namespace App\Http\Controllers;

use App\Models\EquipmentTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskEquipmenTypeController extends Controller
{
    public function index(Request $request){
        $data = [
            'eTypes' => EquipmentTypes::realData()->orderByDesc('id')->get(),
            'request' => $request
        ];

        return view('msk.equipment_types.equipment_types',$data);
    }

    public function addEditAction(Request $request){
        $hasError = false;
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'name' => 'required|string'
        ]);

        $checkEquipmentNameUnique = EquipmentTypes::realData()->where('name',$request->get('name'))->first();

        if ($request->get('id') == 0 && isset($checkEquipmentNameUnique)){
            $validator->errors()->add('error','Bu ad ilə artıq avadanlıq tipi daxil edilib');
            $hasError = true;
        }

        if ($hasError || $validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{
            if($request->get('id') == 0){
                $objEquipmentType  = new EquipmentTypes();
            }
            else{
                $objEquipmentType = EquipmentTypes::realData()->find($request->get('id'));
            }

            $objEquipmentType->name = $request->get('name');
            $objEquipmentType->tenant_id = Auth::user()->tenant_id;
            $objEquipmentType->save();

            $checkEquipment = \App\Models\Equipment::realData()->where('eqp_type',$objEquipmentType->id)->first();
            if(!isset($checkEquipment)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $objEquipmentType["buttons"] = $buttons;

            return response()->json(['status' => 'ok' , 'data' => $objEquipmentType->toArray()]);
        }
    }

    public function deleteAction(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:equipment_types,id'
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'ok','errors'=>$errors]);
        }

        else{

            $objEquipmentType = EquipmentTypes::realData()->find($request->get('id'));
            $objEquipmentType->delete();
            return response()->json(['status' => 'ok']);
        }
    }
}
