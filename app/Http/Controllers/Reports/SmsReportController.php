<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\User;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SmsReportController extends Controller
{
    public function index()
    {
        return view('reports.sms_report.sms_report');
    }

    public function getTable()
    {

        $smses = User::realData()
            ->join('letter_group_user', 'users.id', 'letter_group_user.user_id')
            ->join('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
            ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->where('letter_groups.id', request('class_letter_id', 0))
            ->select('users.name as name', 'users.surname as surname', 'users.middle_name as middle_name', 'tblFpInOuts.*')
            ->orderByDesc('users.id')
            ->get();

        $users = [];
    
        $data = [
            'smses' => $smses,
            'img' => false,
            'title' => false,
            'date' => ''
        ];

        Cache::put('report_sms_report', ['smses' => $smses, 'date' => request('sms_date')], 360000);

        return view('pages.reports.sms_report.sms_report_table', $data);
    }

    public function tabelExport($type)
    {
        $smses = Cache::get('report_sms_report')['smses'];
        $date = Cache::get('report_sms_report')['date'];
        if ($type == "pdf") {
            $pdf = PDF::loadView('pages.reports.sms_report.sms_report_table', ['smses' => $smses, 'title' => true, 'date' => $date])->setPaper('a3', 'landscape');
            return $pdf->download('sms_reports.pdf');
        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('report_sms_report', function ($excel) use ($smses, $date) {
                $excel->sheet('report_sms_report', function ($sheet) use ($smses, $date) {
                    $currentRow = 8;
                    $currentCol = 1;
                    $columns = ["No", "ADI SOYADI ATA ADI", "Daxil olub", "SMS Status", "Xaric olub", "SMS Status"];


                    $sheet->mergeCells('B5:F5');
                    $sheet->cell('B5', function ($cell) use ($date) {
                        $cell->setValue("      Bakı Modern Təhsil Kompleksinin " . $date . " tarixinə SMS Hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    foreach ($columns as $column) {
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentCol++;
                    }
                    $currentRow = $currentRow + 1;
                    foreach ($smses as $key => $sms) {
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($sms, $key) {
                            $cell->setValue($key + 1);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($sms, $key) {
                            $cell->setValue($sms->name." ".$sms->surname." ".$sms->middle_name);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($sms, $key) {
                            $cell->setValue($sms->in_out_type == 'In' ? Carbon::parse($sms->date)->format('Y-m-d , H:i') : '');
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($sms, $key) {
                            $cell->setValue($sms->in_out_type == 'In' ? $sms->sms_status : '');
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(5, $currentRow), function ($cell) use ($sms, $key) {
                            $cell->setValue($sms->in_out_type == 'Out' ? Carbon::parse($sms->date)->format('Y-m-d , H:i') : '');
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(6, $currentRow), function ($cell) use ($sms, $key) {
                            $cell->setValue($sms->in_out_type == 'Out' ? $sms->sms_status : '');
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentRow++;
                    }
                });
            })->download('xls');
        }
    }
}
