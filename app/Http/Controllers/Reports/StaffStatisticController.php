<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Library\Standarts;
use App\Models\ClassLetter;
use App\Models\ClassTime;
use App\Models\LetterGroup;
use App\Models\MskClass;
use App\Models\Year;
use App\User;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Excel;

class StaffStatisticController extends Controller
{
    public function index()
    {
        return view('reports.staff_statistic.staff');
    }

    public function staffPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'interval_type' => 'required|string|in:interval,monthly',
            'start_date' => 'required|date_format:d-m-Y',
            'end_date' => 'required|date_format:d-m-Y',
            'month' => 'required|between:1,12',
            'group' => 'required|string|in:status,heyet,age,gender,current_region,lived_region',
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {

            $group = $request->get('group');

            $tarix = "";
            if ($request->get('interval_type') == 'interval') {
                $startDate = Carbon::parse($request->get('start_date'));
                $endDate = Carbon::parse($request->get('end_date'));
                $endDate = $endDate->addDay();
                $tarix = $startDate->format('d-m-Y') . "  -  " . $endDate->subDay()->format('d-m-Y');
            } else {
                $m = str_pad((int)$request->get('month'), 2, '0', STR_PAD_LEFT);
                $startDate = date("$m/01/Y");
                $endDate = date("m/t/Y", strtotime($startDate));
                $tarix = Standarts::$months[$request->get('month') - 1] . "-" . date("Y");
            }

            $groupsHelper = [
                'status' => ['name' => 'Status'],
                'heyet' => ['name' => 'Heyyət', 'select' => "msk_heyetlers.name as name, msk_heyetlers.id as id, COUNT(1) as count", 'group' => 'msk_heyetlers.name, msk_heyetlers.id'],
                'age' => ['name' => 'Yaş aralığı', 'select' => "(CASE
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 17 THEN N'17 yaşdan kiçik'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 18 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 25 THEN '18-25'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 26 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 36 THEN '26-35'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 37 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 50 THEN '36-50'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 51 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 65 THEN '51-65'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 66 THEN '66+'
                    ELSE N'Təyin edilməyib' END) as name, 
                    (CASE
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 17 THEN N'17 yaşdan kiçik'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 18 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 25 THEN '18-25'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 26 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 36 THEN '26-35'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 37 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 50 THEN '36-50'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 51 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 65 THEN '51-65'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 66 THEN '66+'
                    ELSE N'Təyin edilməyib' END) as id, COUNT(1) as count", 'group' => '(CASE
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 17 THEN N\'17 yaşdan kiçik\'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 18 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 25 THEN \'18-25\'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 26 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 36 THEN \'26-35\'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 37 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 50 THEN \'36-50\'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 51 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 65 THEN \'51-65\'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 66 THEN \'66+\'
                    ELSE N\'Təyin edilməyib\' END)'],
                'gender' => ['name' => 'Cinsi', 'select' => "(CASE WHEN users.gender='F' THEN N'Qadın' ELSE N'Kişi'  END) as name, (CASE WHEN users.gender='F' THEN 2 ELSE 1  END) as id, COUNT(1) as count", 'group' => "(CASE WHEN users.gender='F' THEN N'Qadın' ELSE N'Kişi'  END), users.gender"],
                'current_region' => ['name' => 'Faktiki rayon', 'select' => "current_regions.name as name,(SELECT name FROM msk_cities WHERE id = current_regions.msk_cities_id) as city, current_regions.id as id, COUNT(1) as count", 'group' => 'current_regions.name,current_regions.msk_cities_id,current_regions.id'],
                'lived_region' => ['name' => 'Qeydiyyat rayon', 'select' => "lived_regions.name as name,(SELECT name FROM msk_cities WHERE id = lived_regions.msk_cities_id) as city, lived_regions.id as id, COUNT(1) as count", 'group' => 'lived_regions.name,lived_regions.msk_cities_id,lived_regions.id']
            ];
            //                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 1 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=17 THEN N'18 yaşdan kiçik'
//                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 1 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=17 THEN N'18 yaşdan kiçik'
            //                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 1 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=17 THEN N'18 yaşdan kiçik'
            if ($group != 'status') {

                $staffsFirst = User::realData('staff')->withTrashed()
                    ->leftJoin('msk_heyetlers', 'msk_heyetlers.id', 'users.personal_heyeti')
                    ->leftJoin('msk_regions as current_regions', 'current_regions.id', 'users.current_region')
                    ->leftJoin('msk_regions as lived_regions', 'lived_regions.id', 'users.lived_region')
                    ->whereIn('users.id', function ($query) use ($startDate) {
                        $query->select('user_id')->from('user_histories')
                            ->whereRaw("(user_histories.created_at <= '$startDate' AND (user_histories.deleted_at >= '{$startDate}' OR user_histories.deleted_at is null))");
                    })->select(DB::raw($groupsHelper[$group]['select']))->groupBy(DB::raw($groupsHelper[$group]['group']))->get();

                $staffsLast = User::realData('staff')->withTrashed()
                    ->leftJoin('msk_heyetlers', 'msk_heyetlers.id', 'users.personal_heyeti')
                    ->leftJoin('msk_regions as current_regions', 'current_regions.id', 'users.current_region')
                    ->leftJoin('msk_regions as lived_regions', 'lived_regions.id', 'users.lived_region')
                    ->whereIn('users.id', function ($query) use ($endDate) {
                        $query->select('user_id')->from('user_histories')
                            ->whereRaw("(user_histories.created_at <= '$endDate' AND (user_histories.deleted_at >= '{$endDate}' OR user_histories.deleted_at is null))");
                    })->select(DB::raw($groupsHelper[$group]['select']))->groupBy(DB::raw($groupsHelper[$group]['group']))->get();

                $mergedData = [];
                foreach ($staffsFirst as $data) {
                    $data['name'] = isset($data['city']) ? $data['name']." ( ".$data['city']." )" : $data['name'];
                    $mergedData[(int)$data['id']] = ['name' => $data['name'], 'countFirst' => $data['count'], 'countLast' => 0];
                }
                foreach ($staffsLast as $data) {
                    $data['name'] = isset($data['city']) ? $data['name']." ( ".$data['city']." )" : $data['name'];
                    if (!isset($mergedData[(int)$data['id']])) $mergedData[(int)$data['id']] = ['name' => $data['name'], 'countFirst' => 0, 'countLast' => $data['count']];
                    $mergedData[(int)$data['id']]['countLast'] = $data['count'];
                }
            } else {
                $allFirst = User::realData('staff')->withTrashed()
                    ->whereRaw("CAST(created_at as DATE) <= '$startDate'")->count();

                $aktivFirst = User::realData('staff')->withTrashed()
                    ->whereIn('users.id', function ($query) use ($startDate) {
                        $query->select('user_id')->from('user_histories')
                            ->whereRaw("(CAST(user_histories.created_at as DATE) <= '$startDate' AND (CAST(user_histories.deleted_at as DATE) >= '{$startDate}' OR user_histories.deleted_at is null))");
                    })->count();

                $allLast = User::realData('staff')->withTrashed()
                    ->whereRaw("CAST(created_at as DATE) <= '$endDate'")->count();

                $aktivLast = User::realData('staff')->withTrashed()
                    ->whereIn('users.id', function ($query) use ($endDate) {
                        $query->select('user_id')->from('user_histories')
                            ->whereRaw("(CAST(user_histories.created_at as DATE) <= '$endDate' AND (CAST(user_histories.deleted_at as DATE) >= '{$endDate}' OR user_histories.deleted_at is null))");
                    })->count();

                $mergedData[] = ['name' => 'Aktiv', 'countFirst' => $aktivFirst, 'countLast' => $aktivLast];
                $mergedData[] = ['name' => 'Deaktiv', 'countFirst' => ($allFirst - $aktivFirst), 'countLast' => ($allLast - $aktivLast)];
            }


            Cache::put('report_staff_statistic', ['mergedData' => $mergedData, 'groupName' => $groupsHelper[$group]['name'], 'tarix' => $tarix], 360000);

            return View::make('pages.reports.staff_statistic.table', ['mergedData' => $mergedData, 'groupName' => $groupsHelper[$group]['name']])->render();
        }
    }


    public function exportAction($type)
    {
        $mergedData = Cache::get('report_staff_statistic')['mergedData'];
        $groupName = Cache::get('report_staff_statistic')['groupName'];
        $tarix = Cache::get('report_staff_statistic')['tarix'];
        if ($type == "pdf") {
            $pdf = PDF::loadView('pages.reports.staff_statistic.table', ['mergedData' => $mergedData, 'groupName' => $groupName, 'tableTitle' => 'Personal Statistik Hesabatı', 'tableDate' => $tarix])->setPaper('a4', 'landscape');
            return $pdf->download('personal_statistic_reports.pdf');
        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('personal_statistic_reports', function ($excel) use ($groupName, $mergedData, $tarix) {
                $excel->sheet('personal_statistic_reports', function ($sheet) use ($groupName, $mergedData, $tarix) {
                    $currentRow = 5;
                    $currentCol = 1;
                    $columns = ["NO", $groupName, "Dövrün əvvəlinə personal sayı", "Dövrün sonuna personal sayı"];

                    foreach ($columns as $column) {
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentCol++;
                    }
                    $sheet->mergeCells(Helper::getCell(1, 1) . ':' . Helper::getCell(3, 1));
                    $sheet->cell(Helper::getCell(1, 1), function ($cell) use ($column) {
                        $cell->setValue("      Personal Statistik Hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->mergeCells(Helper::getCell(1, 3) . ':' . Helper::getCell(3, 3));
                    $sheet->cell(Helper::getCell(1, 3), function ($cell) use ($tarix) {
                        $cell->setValue($tarix);
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sumFirst = 0;
                    $sumLast = 0;
                    foreach ($mergedData as $data) {
                        $sumFirst += $data['countFirst'];
                        $sumLast += $data['countLast'];
                        $currentRow++;
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow) {
                            $cell->setValue($currentRow - 1);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($data) {
                            $cell->setValue($data['name'] == '' ? 'Təyin edilməyib' : $data['name']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($data) {
                            $cell->setValue($data['countFirst']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($data) {
                            $cell->setValue($data['countLast']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                    }
                    $currentRow = $currentRow + 1;
                    $sheet->mergeCells(Helper::getCell(1, $currentRow) . ':' . Helper::getCell(2, $currentRow));
                    $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow) {
                        $cell->setValue('     Ümumi');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($sumFirst) {
                        $cell->setValue($sumFirst);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($sumLast) {
                        $cell->setValue($sumLast);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                });
            })->download('xls');
        }
    }
}
