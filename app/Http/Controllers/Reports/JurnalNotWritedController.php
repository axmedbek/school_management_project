<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\LessonDay;
use App\Models\LetterGroup;
use App\Models\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDF;

class JurnalNotWritedController extends Controller
{
    public function index()
    {
        return view('reports.jurnal_not_writed.jurnal_not_writed');
    }
    public function yearClasses(Request $request)
    {
//        $validator = validator($request->all(), [
//            'teacher_id' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id
//        ]);
//
//        if ($validator->fails()) {
//            $errors = view('modals.modal_errors', ['errors' => $validator->errors()])->render();
//
//            return $errors;
//        } else {

        $yearId = YearDays::getCurrentYear()->id;
        $teacherId = $request->get('teacher_id', 0);
        $results = [];

        $classes = LetterGroup::realData()
            ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->join('lessons', 'lessons.letter_group_id', 'letter_groups.id')
            ->join('users', 'users.id', 'lessons.user_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->join('corpuses', 'corpuses.id', 'class_letters.corpus_id')
            ->where('class_letters.year_id', $yearId)
//                ->where('lessons.user_id',$request->get('teacher_id'))
            ->where(DB::raw("concat(msk_classes.name,'',class_letters.name,'','(',corpuses.name,'',')')"), 'like', '%' . $request->get('q') . '%')
            ->select('letter_groups.id', 'letter_groups.name as letter_group_name', 'class_letters.id as letter_id', 'msk_classes.id as class_id', 'msk_classes.name as class_name', 'class_letters.name as letter', 'corpuses.name as corpus_name')
            ->orderBy('msk_classes.order', 'ASC')
            ->groupBy('letter_groups.id', 'letter_groups.name', 'class_letters.id', 'msk_classes.id', 'msk_classes.name', 'class_letters.name', 'corpuses.name', 'msk_classes.order');
//                ->get();

        if ($teacherId > 0) {
            $classes->where('lessons.user_id', $request->get('teacher_id'));
        }

        $classes = $classes->get();

        foreach ($classes as $class) {
            $results[] = ['id' => $class->id, 'letter_id' => $class->letter_id, 'class_id' => $class->class_id, 'text' => ($class->class_name . $class->letter)];
        }

        return response()->json(['status' => 'ok', "results" => $results]);
//        }
    }
    public function classSubjects(Request $request)
    {
//        $validator = validator($request->all(), [
//            'letter_group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . auth()->user()->tenant_id,
//            'teacher_id' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id
//        ]);

//        if ($validator->fails()) {
//            $errors = view('modals.modal_errors', ['errors' => $validator->errors()])->render();
//
//            return $errors;
//        } else {

        $yearId = YearDays::getCurrentYear()->id;
        $letter_group_id = $request->get('letter_group_id', 0);
        $teacher_id = $request->get('teacher_id', 0);
        $results = [];

        $subjects = Subject::realData()
            ->join('lessons', 'lessons.subject_id', 'subjects.id')
//                ->where('lessons.letter_group_id',$letter_group_id)
//                ->where('lessons.user_id',$teacher_id)
//                ->where('lessons.year_id',$yearId)
            ->where('subjects.name', 'like', '%' . $request->get('q') . '%');
//                ->groupBy('subjects.id','subjects.name','lessons.year_id','lessons.user_id');
//                ->get(['subjects.id','subjects.name']);

        if ($letter_group_id > 0) {
            $subjects->where('lessons.letter_group_id', $letter_group_id);
        }
        if ($teacher_id > 0) {
            $subjects->where('lessons.user_id', $teacher_id);
        }

        if ($letter_group_id == 0 && $teacher_id == 0) {
            $subjects->groupBy('subjects.id', 'subjects.name');
        } else {
            $subjects
                ->where('lessons.year_id', $yearId)
                ->groupBy('subjects.id', 'subjects.name', 'lessons.year_id', 'lessons.user_id');
        }

        $subjects = $subjects->get(['subjects.id', 'subjects.name']);

        foreach ($subjects as $subject) {
            $results[] = ['id' => $subject->id, 'text' => $subject->name];
        }

        return response()->json(['status' => 'ok', "results" => $results]);
//        }
    }
    public function getTable(Request $request)
    {
        $validator = validator($request->all(), [
            'teacher' => 'nullable|exists:users,id,tenant_id,'. auth()->user()->tenant_id,
            'letter_group_id' => 'nullable|integer|exists:letter_groups,id,tenant_id,' . auth()->user()->tenant_id,
            'subject_id' => 'nullable|integer|exists:subjects,id,tenant_id,' . auth()->user()->tenant_id,
            'filter_date' => 'nullable|date'
        ]);

        if ($validator->fails()) {
            $errors = view('modals.modal_errors', ['status' => 'error','errors' => $validator->errors()])->render();

            return $errors;
        } else {

            $startDate = Carbon::parse($request->get('date'))->startOfMonth()->format('Y-m-d');
            $endDate = Carbon::parse($request->get('date'))->endOfMonth()->format('Y-m-d');

            $yearId = YearDays::getCurrentYear()->id;
            $teacher_id = $request->get('teacher',0);
            $teacher_name = $request->get('teacher_name');
            $letter_group_id = $request->get('letter_group_id',0);
            $letter_group_name = $request->get('letter_group_name');
            $subject_id = $request->get('subject_id',0);
            $subject_name = $request->get('subject_name');
            $filter_date = $request->get('filter_date',0);

            $dataLessons = LessonDay::realData()
                ->leftJoin('lectures',function ($join){
                    $join->on('lectures.date','=','lesson_days.date');
                    $join->on('lectures.teacher_id','=','lesson_days.user_id');
                })
                ->join('users','users.id','lesson_days.user_id')
                ->join('lessons','lessons.id','lesson_days.lesson_id')
                ->join('subjects','subjects.id','lessons.subject_id')
                ->join('corpuses','corpuses.id','lessons.corpus_id')
                ->join('letter_groups','letter_groups.id','lessons.letter_group_id')
                ->join('class_letters','class_letters.id','letter_groups.class_letter_id')
                ->join('msk_classes','msk_classes.id','class_letters.msk_class_id')
                ->whereNull('lectures.id')
                ->where('lesson_days.type','=','day')
                ->where('lesson_days.year_id',$yearId)
                ->whereBetween('lesson_days.date',[$startDate,$endDate])
                ->oldest('lesson_days.date');

            $teacherSelect2 = [];
            $letterGroupSelect2 = [];
            $subjectSelect2 = [];

            if ($teacher_id > 0){
                $dataLessons->where('users.id',$teacher_id);
                $teacherSelect2 = ['id' => (int)$teacher_id , 'text' => $teacher_name];
            }

            if ($letter_group_id > 0){
                $dataLessons->where('letter_groups.id',$letter_group_id);
                $letterGroupSelect2 = ['id' => (int)$letter_group_id , 'text' => $letter_group_name];
            }

            if ($subject_id > 0){
                $dataLessons->where('subjects.id',$subject_id);
                $subjectSelect2 = ['id' => (int)$subject_id , 'text' => $subject_name];
            }

            if ($filter_date != 0){
                $dataLessons->where('lesson_days.date',Carbon::parse($filter_date)->format('Y-m-d'));
            }

            $dataLessons = $dataLessons->get([
                    'lesson_days.date',
                    DB::raw("CONCAT(users.name,' ',users.surname,' ',users.middle_name) as full_name"),
                    DB::raw("CONCAT(msk_classes.name,'',class_letters.name) as letter_group_name"),
                    'subjects.name as subject_name']);

            $data = [
                'data' => $dataLessons,
                'teacherSelect2' => $teacherSelect2,
                'letterGroupSelect2' => $letterGroupSelect2,
                'subjectSelect2'  => $subjectSelect2,
                'filter' => true,
                'title' => false,
                'selectedDate' => Standarts::$months[Carbon::parse($startDate)->format('m') - 1].'/'.Carbon::parse($startDate)->format('Y')
            ];

            Cache::put('reports_jurnal_not_writed', $data , 360000);

            return view('pages.reports.jurnal_not_writed.jurnal_not_writed',$data);
        }
    }
    public function tabelExport($type)
    {
//        ini_set('max_execution_time', 300);
//        ini_set("memory_limit","512M");

        $data = Cache::get('reports_jurnal_not_writed')['data'];
        $teacherSelect2 = Cache::get('reports_jurnal_not_writed')['teacherSelect2'];
        $letterGroupSelect2 = Cache::get('reports_jurnal_not_writed')['letterGroupSelect2'];
        $subjectSelect2 = Cache::get('reports_jurnal_not_writed')['subjectSelect2'];
        $selectedDate = Cache::get('reports_jurnal_not_writed')['selectedDate'];

        if ($type == "pdf") {


            $pdf = PDF::loadView('pages.reports.jurnal_not_writed.jurnal_not_writed', ['data' => $data,'teacherSelect2' => $teacherSelect2,
                'letterGroupSelect2' => $letterGroupSelect2,'subjectSelect2'  => $subjectSelect2,'title' => true , 'filter' => false,'selectedDate' => $selectedDate])->setPaper('a3', 'landscape');
            return $pdf->download('jurnal_not_writed.pdf');

        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('jurnal_not_writed_report', function ($excel) use ($data,$selectedDate) {
                $excel->sheet('jurnal_not_writed_report', function ($sheet) use ($data,$selectedDate) {
                    $currentRow = 8;
                    $currentCol = 1;
                    $columns = ["No", "Tarix", "Müəllim", "Sinif", "Fənn"];


                    $sheet->mergeCells('A5:F5');
                    $sheet->cell('A5', function ($cell) use($selectedDate){
                        $cell->setValue("      Bakı Modern Təhsil Kompleksinin".$selectedDate." tarixində Jurnal yazılmaması üzrə hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    foreach ($columns as $column) {
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentCol++;
                    }
                    $currentRow = $currentRow + 1;
                    foreach ($data as $key => $userInfo) {
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($userInfo, $key) {
                            $cell->setValue($key + 1);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($userInfo, $key) {
                            $cell->setValue($userInfo['date']);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($userInfo, $key) {
                            $cell->setValue($userInfo['full_name']);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($userInfo, $key) {
                            $cell->setValue($userInfo['letter_group_name']);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(5, $currentRow), function ($cell) use ($userInfo, $key) {
                            $cell->setValue($userInfo['subject_name']);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentRow++;
                    }
                });
            })->download('xls');
        }

    }
}
