<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Models\LessonDay;
use App\Models\LetterGroup;
use App\Models\Mark;
use App\Models\Subject;
use App\Models\Year;
use App\User;
use Carbon\Carbon;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class FinalEvaluationController extends Controller
{
    public function index()
    {
        return view('reports.final_evluation_table.final_evluation_table');
    }

    public function getTable()
    {

        $year_id = request('year_id');
        $class_id = request('class_id');
        $group_id = request('group_id');
        $selectedSubjects = request()->filled('subjects') ? explode(",", request('subjects')) : [];

//        dd($selectedSubjects);

        $classLetters = LetterGroup::realData()
            ->join('lessons', 'lessons.letter_group_id', 'letter_groups.id')
            ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->whereIn('letter_groups.isMainGroup', explode(",", $group_id))
            ->where('class_letters.year_id', $year_id)
            ->where('msk_classes.id', $class_id)
            ->whereNull('lessons.deleted_at')
            ->select('letter_groups.id as id', 'class_letters.name as name', 'msk_classes.name as class_name')
            ->groupBy('letter_groups.id', 'class_letters.name', 'msk_classes.name');

        if ($selectedSubjects) {
            $classLetters = $classLetters->whereIn('lessons.subject_id', $selectedSubjects)->get();
        } else {
            $classLetters = $classLetters->get();
        }


        $users = User::realData()
            ->join('letter_group_user', 'letter_group_user.user_id', 'users.id')
            ->join('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
            ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->whereIn('letter_groups.isMainGroup', explode(",", $group_id))
            ->where('class_letters.year_id', $year_id)
            ->where('msk_classes.id', $class_id)
            ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.middle_name as middle_name', 'letter_groups.id as letter_group_id')
            ->get();

        $subjects = LetterGroup::realData()
            ->join('lessons', 'lessons.letter_group_id', 'letter_groups.id')
            ->join('subjects', 'subjects.id', 'lessons.subject_id')
            ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
//            ->whereIn('letter_groups.isMainGroup', explode(",", $group_id))
//            ->where('class_letters.year_id',$year_id);
            ->whereNull('lessons.deleted_at')
            ->where('msk_classes.id', $class_id);

//        SELECT letter_groups.id as class_letter_id,s.id , s.name FROM letter_groups
//INNER JOIN lessons l on letter_groups.id = l.letter_group_id
//INNER JOIN subjects as s ON s.id = l.subject_id
//INNER JOIN class_letters cl on letter_groups.class_letter_id = cl.id
//INNER JOIN msk_classes m2 on cl.msk_class_id = m2.id


        if ($selectedSubjects) {
            $subjects->whereIn('subjects.id', $selectedSubjects);
        }
        $subjects = $subjects
            ->select('subjects.id as id', 'subjects.name as name', 'letter_groups.id as letter_group_id')
            ->groupBy('letter_groups.id', 'subjects.id', 'subjects.name')
            ->get();

//        dd($subjects->toSql());


        $evals = LessonDay::realData()
            ->where('year_id', $year_id)
            ->whereIn('type', ['yi', 'y'])->get();

        $marks = Mark::realData()
            ->join('class_letters', 'class_letters.id', 'marks.class_letter_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->join('subjects', 'subjects.id', 'marks.subject_id')
            ->where('msk_classes.id', $class_id)
            ->whereIn('marks.type', ['yi', 'y'])
            ->select('marks.*')
            ->get();

        $evalValues = [];
        foreach ($classLetters as $classLetter) {
            foreach ($users as $user) {
                foreach ($subjects as $subject) {
                    $y1 = "";
                    $y2 = "";
                    $i = "";
                    foreach ($evals as $key => $eval) {
                        if (count($marks) > 0) {
                            foreach ($marks as $mark) {
                                if ($mark['student_id'] == $user->id) {
                                    if ($mark['subject_id'] == $subject->id) {
                                        if ($mark->type == "y") {
                                            $i = $mark->value;
                                        } else {
                                            if ($key == 0 && $eval['date'] == $mark['date']) {
                                                $y1 = $mark->value;
                                            } else {
                                                $y2 = $mark->value;
                                            }
                                        }
                                    }
                                }
                                $evalValues[$classLetter->id][$user->id][$subject->id] = ['y1' => $y1, 'y2' => $y2, 'y' => $i];
                            }
                        } else {
                            $evalValues[$classLetter->id][$user->id][$subject->id] = ['y1' => $y1, 'y2' => $y2, 'y' => $i];
                        }
                    }
                }
            }
        }

        $data = [
            'classLetters' => $classLetters,
            'year' => Year::realData()->find($year_id),
            'users' => $users,
            'subjects' => $subjects,
            'evals' => $evals,
            'marks' => $marks,
            'evalValues' => $evalValues
        ];

        Cache::put('report_final_evaluation', ['data' => $data], 360000);

        return view('pages.reports.final_evaluation_table.final_evaluation_table', $data);
    }

    public function exportAction($type)
    {
        $data = Cache::get('report_final_evaluation')['data'];
        $year = $data['year'];
        $classLetters = $data['classLetters'];
        $users = $data['users'];
        $subjects = $data['subjects'];
        $evals = $data['evals'];
        $marks = $data['marks'];
        $evalValues = $data['evalValues'];

        if ($type == "pdf") {
            $pdf = PDF::loadView('pages.reports.final_evaluation_table.final_evaluation_table', [
                'classLetters' => $classLetters,
                'year' => $year,
                'users' => $users,
                'subjects' => $subjects,
                'evals' => $evals,
                'marks' => $marks,
                'evalValues' => $evalValues
            ])->setPaper('a2', 'landscape');
            return $pdf->download('report_final_evaluation.pdf');
        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('report_final_evaluation_excel', function ($excel) use ($year, $classLetters, $users, $subjects, $evals, $marks, $evalValues) {
                $excel->sheet('report_final_evaluation_excel', function ($sheet) use ($year, $classLetters, $users, $subjects, $evals, $marks, $evalValues) {
                    $currentRow = 5;
                    $currentCol = 1;
                    $currentColForSecondRow = 1;
                    $date = Carbon::parse($year->start_date)->format('Y') . " - " . Carbon::parse($year->end_date)->format('Y');
                    $secondColumns = ["", ""];

                    $sheet->mergeCells('B5:H5');
                    $sheet->cell('B5', function ($cell) {
                        $cell->setBackground('#86d1e0');
                        $cell->setBorder('#ffffff', '#ffffff', '#ffffff', '#ffffff');
                    });

                    $sheet->setHeight(5, 40);
                    $sheet->cell('B5', "                Bakı Modern Təhsil Kompleksinin " . $date . " 
                    tədris ilinin yekun qiymətləndirmə cədvəli.");
                    $sheet->getStyle('B5')->getAlignment('center')->setWrapText(true);


                    foreach ($classLetters as $classLetter) {
                        $columns = [];
                        $secondColumns = ["", ""];
                        $sheet->cell(Helper::getCell(1, $currentRow + 2), function ($cell) use ($classLetter) {
                            $cell->setValue("Sinif : " . $classLetter->class_name . "/" . $classLetter->name);
                            $cell->setBackground('#ededed');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });

                        $columns = ["No", "ADI SOYADI ATA ADI"];

                        foreach ($subjects as $subject) {
                            if ($subject['letter_group_id'] == $classLetter->id) {
                                array_push($columns, $subject->name);
                                foreach ($evals as $key => $eval) {
                                    if ($eval->type == "yi") {
                                        array_push($secondColumns, "Y" . ($key + 1));
                                    } else {
                                        array_push($secondColumns, "İ");
                                    }
                                }
                            }
                        }

                        $currentCol = 1;
                        foreach ($columns as $key => $column) {
                            if ($key > 1) {
                                $sheet->mergeCells(Helper::getCell($currentCol, $currentRow + 3) . ':' . Helper::getCell($currentCol + 2, $currentRow + 3));
                                $sheet->cell(Helper::getCell($currentCol, $currentRow + 3), function ($cell) use ($column) {
                                    $cell->setValue($column);
                                    $cell->setBackground('#ededed');
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                $currentCol = $currentCol + 2;
                            } else {
                                $sheet->cell(Helper::getCell($currentCol, $currentRow + 3), function ($cell) use ($column) {
                                    $cell->setValue($column);
                                    $cell->setBackground('#ededed');
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                            }

                            $currentCol++;
                        }

                        $currentCol = 1;
                        foreach ($secondColumns as $secondColumn) {
                            $sheet->cell(Helper::getCell($currentCol, $currentRow + 4), function ($cell) use ($secondColumn) {
                                $cell->setValue($secondColumn);
                                $cell->setBackground('#ededed');
                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            });
                            $currentCol++;
                        }

                        $currentRow = $currentRow + 5;
                        $currentCol = 1;
                        $sira = 0;
                        foreach ($users as $key => $user) {
                            if ($user['letter_group_id'] == $classLetter['id']) {
                                ++$sira;
                                $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow, $key, $sira) {
                                    $cell->setValue($sira);
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($user) {
                                    $cell->setValue($user->name . " " . $user->surname . " " . $user->middle_name);
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });

                                $currentCol = 3;
                                foreach ($subjects as $subject) {
                                    if ($subject['letter_group_id'] == $classLetter->id) {
                                        foreach ($evalValues[$classLetter->id][$user->id][$subject->id] as $mark) {
                                            $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($mark) {
                                                $cell->setValue($mark);
                                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                            });
                                            $currentCol++;
                                        }
                                    }
                                }
                                $currentRow++;
                            }
                        }
                    }
                });
            })->download('xls');
        }

    }
}
