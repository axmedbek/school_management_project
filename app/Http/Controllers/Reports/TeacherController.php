<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Models\Year;
use App\User;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TeacherController extends Controller
{
    public function index(){
        return view('reports.teacher.teacher');
    }

    public function teacherPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . Auth::user()->tenant_id,
            'teacher' => 'nullable|array',
            'teacher.*' => 'required|integer|exists:users,id,tenant_id,' . Auth::user()->tenant_id,
            'min_age' => 'nullable|integer|min:0',
            'max_age' => 'nullable|integer|min:0',
            'gender' => 'nullable|string|in:f,m',
            'teacher_status' => 'nullable|integer|min:1',
            'study_levels' => 'nullable|integer|min:1',
            'status' => 'nullable|integer|in:1,0',
            'current_city' => 'nullable|array',
            'current_city.*' => 'required|integer|exists:msk_cities,id,tenant_id,'. Auth::user()->tenant_id,
            'current_region' => 'nullable|array',
            'current_region.*' => 'required|integer|exists:msk_regions,id,tenant_id,'. Auth::user()->tenant_id,
            'enrolled_city' => 'nullable|array',
            'enrolled_city.*' => 'required|integer|exists:msk_cities,id,tenant_id,'. Auth::user()->tenant_id,
            'enrolled_region' => 'nullable|array',
            'enrolled_region.*' => 'required|integer|exists:msk_regions,id,tenant_id,'. Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {

            $year = Year::realData()->find($request->get('year_id'));
            $tarix = $year->start_date." / ".$year->end_date;

            $teachers = User::realData('teacher')->withTrashed()
                ->whereIn('id', function ($query)use($year){ $query->select('user_id')->from('user_histories')
                    ->whereRaw("( (user_histories.created_at <= '{$year->end_date}' AND (user_histories.deleted_at >= '{$year->start_date}' OR user_histories.deleted_at is null)) OR (user_histories.created_at >= '{$year->end_date}' AND (user_histories.deleted_at <= '{$year->start_date}' OR user_histories.deleted_at is null)))");
                });

            if($request->has('teacher')) $teachers->whereIn('id', $request->get('teacher'));
            if($request->has('min_age')) $teachers->where(DB::raw('CAST(DATEDIFF(d, birthday, GETDATE()) / 365.25 as INT)'), '>=', (int)$request->get('min_age'));
            if($request->has('max_age')) $teachers->where(DB::raw('CAST(DATEDIFF(d, birthday, GETDATE()) / 365.25 as INT)'), '<=', (int)$request->get('max_age'));
            if($request->has('gender')) $teachers->where('gender', $request->get('gender'));
            if ($request->has('teacher_status')) $teachers->where('msk_teacher_status_id',$request->get('teacher_status'));
            if ($request->has('study_level')) $teachers->where('msk_study_levels_id',$request->get('study_level'));
            if($request->has('status')){
                if($request->get('status') == 1) $teachers->whereNull('deleted_at');
                else $teachers->whereNotNull('deleted_at');
            }
            if($request->has('current_city')) $teachers->whereIn('current_city', $request->get('current_city'));
            if($request->has('current_region')) $teachers->whereIn('current_region', $request->get('current_region'));
            if($request->has('enrolled_city')) $teachers->whereIn('lived_city', $request->get('enrolled_city'));
            if($request->has('enrolled_region')) $teachers->whereIn('lived_region', $request->get('enrolled_region'));

            $teachers = $teachers->select('*', DB::raw('CAST(DATEDIFF(d, birthday, GETDATE()) / 365.25 as INT) as age'))->orderBy('id', 'desc')->get();

            Cache::put('report_teacher', ['teachers' => $teachers,'tarix' => $tarix], 360000);

            return View::make('pages.reports.teacher.teacher_table', ['teachers' => $teachers])->render();
        }
    }



    public function exportToExcel($type)
    {
        $tarix = Cache::get('report_teacher')['tarix'];

        if($type == "pdf"){
            $teachers = Cache::get('report_teacher')['teachers'];
            $pdf = PDF::loadView('pages.reports.teacher.teacher_table',['teachers' => $teachers,'tableTitle' => 'Müəllim  Hesabatı','tableDate' => $tarix])->setPaper('a1', 'landscape');
//            return $pdf->download('teacher_reports.pdf');

            return response()->json([
                'loading' => false,
                'filename' => 'teacher_reports.pdf',
                'path' => "data:application/pdf;base64,".base64_encode($pdf->download('teacher_reports.pdf'))
            ]);
        }
        else{
            $data = Cache::get('report_teacher')['teachers'];
            $myFile = \Maatwebsite\Excel\Facades\Excel::create('teacher_reports', function ($excel) use ($data,$tarix) {
                $excel->sheet('teacher_reports', function ($sheet) use ($data,$tarix) {
                    $currentRow = 5;
                    $currentCol = 1;
                    $columns = ["S/S","ADI","SOYADI","ATA ADI","MÜƏLLİM STATUSU","TƏHSİLİ","TƏVƏLLÜDÜ","Yaşı","CİNSİ","DOĞULDUĞU YER","FAKTİKİ ÜNVAN ŞƏHƏR","FAKTİKİ ÜNVAN RAYON","FAKTİKİ ÜNVAN","QEYDİYYAT ÜNVAN ŞƏHƏR","QEYDİYYAT ÜNVAN RAYON","QEYDİYYAT ÜNVANI","SİSTEMƏ QEYDİYYAT TARİXİ","STATUSU","DEAKTIV Tarixi","EV TELEFONU","MOBİL TELEFONU","EMAIL"];

                    foreach ($columns as $column){
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function($cell)use($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $currentCol++;
                    }
                    $sheet->mergeCells(Helper::getCell(1, 1) .':'.Helper::getCell(3, 1));
                    $sheet->cell(Helper::getCell(1, 1), function($cell)use($column) {
                        $cell->setValue("      Müəllim  Hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin','thin','thin','thin');
                    });
                    $sheet->mergeCells(Helper::getCell(1, 3) .':'.Helper::getCell(3, 3));
                    $sheet->cell(Helper::getCell(1, 3), function($cell)use($tarix) {
                        $cell->setValue($tarix);
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin','thin','thin','thin');
                    });

                    foreach($data as $key => $staff){
                        $currentRow++;
                        $sheet->cell(Helper::getCell(1, $currentRow), function($cell)use($currentRow,$key) {
                            $cell->setValue($key + 1);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->name);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->surname);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(4, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->middle_name);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(5, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->msk_teacher_status->name);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(6, $currentRow), function($cell)use($staff) {
                            $studyData = "";
                            foreach ($staff->person_studies as $study){
                                $studyData.="".$study->place;
                            }
                            $cell->setValue($studyData);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(7, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->birthday == '' ? '-' : date('d-m-Y', strtotime($staff->birthday)));
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(8, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->age);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(9, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->getGenger());
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(10, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->birth_place);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(11, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->currentCity['name']);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(12, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->currentRegion['name']);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(13, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->current_address);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(14, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->livedCity['name']);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(15, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->livedRegion['name']);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(16, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->lived_address);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(17, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->created_at == '' ? '-' : date('d-m-Y', strtotime($staff->created_at)));
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(18, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->getStatus());
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(19, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->deleted_at == '' ? '-' : date('d-m-Y', strtotime($staff->deleted_at)));
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(20, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->home_tel);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(21, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->mobil_tel);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                        $sheet->cell(Helper::getCell(22, $currentRow), function($cell)use($staff) {
                            $cell->setValue($staff->email);
                            $cell->setBorder('thin','thin','thin','thin');
                        });
                    }
                });
            });

            $myFile = $myFile->string('xlsx');
            return response()->json([
                'loading' => false,
                'filename' => 'teacher_reports.xlsx',
                'path' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile)
            ]);
        }

    }
}
