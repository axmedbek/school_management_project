<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Library\Standarts;
use App\Models\Year;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Http\Request;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class StudentController extends Controller
{
    public function index()
    {

        return view('reports.student.student');
    }

    public function studentPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . Auth::user()->tenant_id,
            'student' => 'nullable|array',
            'student.*' => 'required|integer|exists:users,id,tenant_id,' . Auth::user()->tenant_id,
            'min_age' => 'nullable|integer|min:0',
            'max_age' => 'nullable|integer|min:0',
            'heyyet' => 'nullable|integer|exists:msk_heyetlers,id,tenant_id,' . Auth::user()->tenant_id,
            'gender' => 'nullable|string|in:f,m',
            'status' => 'nullable|integer|in:1,0',
            'current_city' => 'nullable|array',
            'current_city.*' => 'required|integer|exists:msk_cities,id,tenant_id,' . Auth::user()->tenant_id,
            'current_region' => 'nullable|array',
            'current_region.*' => 'required|integer|exists:msk_regions,id,tenant_id,' . Auth::user()->tenant_id,
            'enrolled_city' => 'nullable|array',
            'enrolled_city.*' => 'required|integer|exists:msk_cities,id,tenant_id,' . Auth::user()->tenant_id,
            'enrolled_region' => 'nullable|array',
            'enrolled_region.*' => 'required|integer|exists:msk_regions,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {

            $year = Year::realData()->find($request->get('year_id'));
            $tarix = $year->start_date . " / " . $year->end_date;

            $students = User::realData('student')->withTrashed();

            $students
                ->leftJoin('letter_group_user', function ($join) use ($year) {
                    $join->on('letter_group_user.user_id', 'users.id')->on('letter_group_user.year_id', DB::raw($year->id));
                })
                ->leftJoin('letter_groups', 'letter_groups.id', '=', 'letter_group_user.letter_group_id')
                ->leftJoin('class_letters', 'class_letters.id', '=', 'letter_groups.class_letter_id')
                ->leftJoin('msk_classes', 'msk_classes.id', '=', 'class_letters.msk_class_id')
                ->leftJoin('group_types', 'group_types.id', '=', 'letter_groups.group_type_id');

            if ($request->has('student')) $students->whereIn('users.id', $request->get('student'));
            if ($request->has('min_age')) $students->where(DB::raw('CAST(DATEDIFF(d, birthday, GETDATE()) / 365.25 as INT)'), '>=', (int)$request->get('min_age'));
            if ($request->has('max_age')) $students->where(DB::raw('CAST(DATEDIFF(d, birthday, GETDATE()) / 365.25 as INT)'), '<', (int)$request->get('max_age'));
            if ($request->has('gender')) $students->where('gender', $request->get('gender'));
            if ($request->has('status')) {
                if ($request->get('status') == 1) $students->whereNull('users.deleted_at');
                else $students->whereNotNull('users.deleted_at');
            }
            if ($request->has('month_in_birth')) {
                $month = array_search($request->get('month_in_birth'), Standarts::$months) + 1;
                $students->where(DB::raw("MONTH(REPLACE(CONVERT(NVARCHAR,birthday,106),'-','/'))"), '=', $month);
            }
            if ($request->has('class_names')) {
                $classes = $request->get('class_names');
                $bagliOlmayanlarHas = array_search("0", $request->get('class_names'));
                if (!is_bool($bagliOlmayanlarHas)) {
                    unset($classes[$bagliOlmayanlarHas]);
                    $students->where(function ($where) use ($classes) {
                        $where->whereNull('letter_group_user.user_id')->orWhereIn('msk_classes.id', $classes);
                    });
                } else {
                    $students->whereIn('msk_classes.id', $classes);
                }
            }

            // dd(explode(",",$request->get('class_letter')));

            if ($request->has('class_letter')) $students->whereIn('class_letters.id', $request->get('class_letter'));

            if ($request->has('temayul')) $students->whereIn('group_types.id', $request->get('temayul'));

            if ($request->has('qrupu')) $students->whereIn('letter_groups.id', $request->get('qrupu'));

            if ($request->has('current_city')) $students->whereIn('current_city', $request->get('current_city'));
            if ($request->has('current_region')) $students->whereIn('current_region', $request->get('current_region'));
            if ($request->has('enrolled_city')) $students->whereIn('lived_city', $request->get('enrolled_city'));
            if ($request->has('enrolled_region')) $students->whereIn('lived_region', $request->get('enrolled_region'));

            $students = $students
                ->select('users.*',
                    'users.name as username',
                    'msk_classes.name as class_name',
                    'class_letters.name as class_letter_name',
                    'group_types.name as temayul',
                    'letter_groups.name as group_name',
                    'users.deleted_at as deleted_at',
                    DB::raw('CAST(DATEDIFF(d, birthday, GETDATE()) / 365.25 as INT) as age'))
                ->orderBy('users.id', 'desc')->get();

//            dd($students);

            $findMaxFamilyCount = (int)User::realData()
                ->join('person_families', 'person_families.user_id', 'users.id')
                ->where('users.user_type', 'student')
                ->groupBy('person_families.user_id')
                ->select(DB::raw('COUNT(person_families.user_id) as count'))
                ->orderByDesc('count')
                ->first()->count;

            $viewData = view('pages.reports.student.student_table', ['students' => $students, 'maxFamilyCount' => $findMaxFamilyCount, 'tableTitle' => 'Şagird  Hesabatı', 'tableDate' => $tarix]);

            Cache::put('report_student', ['students' => $students, 'maxFamilyCount' => $findMaxFamilyCount, 'tarix' => $tarix, 'viewData' => (string)$viewData], 360000);

            return $viewData;
        }
    }

    public function exportToExcel($type)
    {
        $tarix = Cache::get('report_student')['tarix'];
        $students = Cache::get('report_student')['students'];
        $maxFamilyCount = Cache::get('report_student')['maxFamilyCount'];
        $viewData = Cache::get('report_student')['viewData'];

        if ($type == "pdf") {
//            $pdf = PDF::loadView('pages.reports.student.student_table', ['students' => $students, 'maxFamilyCount' => $maxFamilyCount, 'tableTitle' => 'Şagird  Hesabatı', 'tableDate' => $tarix])->setPaper('a1', 'landscape');
//            return $pdf->download('student_reports.pdf');
            $pdf = app()->make('dompdf.wrapper');
            $pdf->loadHTML($viewData)->setPaper('a1', 'landscape');
            return $pdf->download('student_reports.pdf');

        } else {
            $myFile = \Maatwebsite\Excel\Facades\Excel::create('student_reports', function ($excel) use ($students, $tarix, $maxFamilyCount) {
                $excel->sheet('student_reports', function ($sheet) use ($students, $tarix, $maxFamilyCount) {
                    $currentRow = 5;
                    $currentCol = 1;
                    $columns = ["S/S", "ADI", "SOYADI", "ATA ADI", "TƏVƏLLÜDÜ", "Yaşı", "CİNSİ", "DOĞULDUĞU YER", "SİNİFİ/HƏRFİ", "TƏMAYÜL", "QRUPU", "FAKTİKİ ÜNVAN ŞƏHƏR", "FAKTİKİ ÜNVAN RAYON", "FAKTİKİ ÜNVAN", "QEYDİYYAT ÜNVAN ŞƏHƏR", "QEYDİYYAT ÜNVAN RAYON", "QEYDİYYAT ÜNVANI", "SİSTEMƏ QEYDİYYAT TARİXİ", "STATUSU", "DEAKTIV Tarixi", "EV TELEFONU", "MOBİL TELEFONU", "EMAIL"];

                    for ($i = 0; $i < $maxFamilyCount; $i++) {
                        array_push($columns, "QOHUMU");
                        array_push($columns, "ASA");
                        array_push($columns, "TEL");
                    }

                    foreach ($columns as $column) {
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            $cell->setAlignment('center');
                        })->getStyle(Helper::getCell($currentCol, $currentRow))->applyFromArray(array(
                                'font' => [
                                    'bold'  => true,
                                    'color' => ['rgb' => '36393a'],
                                    'size'  => 11,
                                ]
                            )
                        );
                        $currentCol++;
                    }

                    $sheet->mergeCells(Helper::getCell(1, 1) . ':' . Helper::getCell(3, 1));
                    $sheet->cell(Helper::getCell(1, 1), function ($cell) use ($column) {
                        $cell->setValue("      Şagird  Hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->mergeCells(Helper::getCell(1, 3) . ':' . Helper::getCell(3, 3));
                    $sheet->cell(Helper::getCell(1, 3), function ($cell) use ($tarix) {
                        $cell->setValue($tarix);
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    foreach ($students as $key => $student) {

                        $currentRow++;
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow, $key) {
                            $cell->setValue($key + 1);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->username);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->surname);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->middle_name);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(5, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->birthday == '' ? '-' : date('d-m-Y', strtotime($student->birthday)));
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(6, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->age);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(7, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->getGenger());
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(8, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->birth_place);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(9, $currentRow), function ($cell) use ($student) {
                            $cell->setValue(isset($student['class_name']) ? $student['class_name'] . "/" . $student['class_letter_name'] : '');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(10, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student['temayul']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(11, $currentRow), function ($cell) use ($student) {
                            $cell->setValue(isset($student['group_name']) ? $student['group_name'] . "(" . $student['temayul'] . ")" : '');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(12, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->currentCity['name']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(13, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->currentRegion['name']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(14, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->current_address);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(15, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->livedCity['name']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(16, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->livedRegion['name']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(17, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->lived_address);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(18, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->created_at == '' ? '-' : date('d-m-Y', strtotime($student->created_at)));
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(19, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->getStatus());
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(20, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->deleted_at == '' ? '-' : date('d-m-Y', strtotime($student->deleted_at)));
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(21, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->home_tel);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(22, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->mobil_tel);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(23, $currentRow), function ($cell) use ($student) {
                            $cell->setValue($student->email);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });

                        $familyRow = 0;
                        for ($i = 0; $i < $maxFamilyCount; $i++) {
                            if (isset($student->person_families[$i])) {
                                $familyObj = \App\User::find($student->person_families[$i]->parent_id);
                                $sheet->cell(Helper::getCell(24 + $i + $familyRow, $currentRow), function ($cell) use ($student, $i) {
                                    $cell->setValue($student->person_families[$i]->msk_relation->name);
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                $sheet->cell(Helper::getCell(25 + $i + $familyRow, $currentRow), function ($cell) use ($student, $familyObj) {
                                    $cell->setValue($familyObj['surname'] . " " . $familyObj['name'] . " " . $familyObj['middle_name']);
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                $sheet->cell(Helper::getCell(26 + $i + $familyRow, $currentRow), function ($cell) use ($student, $familyObj) {
                                    $cell->setValue($familyObj['mobil_tel']);
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                            } else {
                                $sheet->cell(Helper::getCell(24 + $i + $familyRow, $currentRow), function ($cell) use ($student) {
                                    $cell->setValue('');
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                $sheet->cell(Helper::getCell(25 + $i + $familyRow, $currentRow), function ($cell) use ($student) {
                                    $cell->setValue('');
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                $sheet->cell(Helper::getCell(26 + $i + $familyRow, $currentRow), function ($cell) use ($student) {
                                    $cell->setValue('');
                                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                            }
                            $familyRow = $familyRow + 2;
                        }
                    }
                });
            });

            $myFile = $myFile->string('xlsx');
            return response()->json([
                'loading' => false,
                'filename' => 'student_reports.xlsx',
                'path' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile)
            ]);
        }
    }
}
