<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Library\Standarts;
use App\Library\YearDays;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class StudentStatisticController extends Controller
{
    public function index()
    {
        return view('reports.student_statistic.student');
    }

    public function studentPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'interval_type' => 'required|string|in:interval,monthly',
            'start_date' => 'required|date_format:d-m-Y',
            'end_date' => 'required|date_format:d-m-Y',
            'month' => 'required|between:1,12',
            'group' => 'required|string|in:status,sinif,age,gender,current_region,lived_region',
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {

            $group = $request->get('group');
            $tarix = "";

            if ($request->get('interval_type') == 'interval') {
                $startDate = Carbon::parse($request->get('start_date'));
                $endDate = Carbon::parse($request->get('end_date'));
                $endDate = $endDate->addDay();
                $tarix = $startDate->format('d-m-Y') . "  -  " . $endDate->subDay()->format('d-m-Y');
            } else {
                $m = str_pad((int)$request->get('month'), 2, '0', STR_PAD_LEFT);
                $startDate = date("$m/01/Y");
                $endDate = date("m/t/Y", strtotime($startDate));
                $tarix = Standarts::$months[$request->get('month') - 1] . "-" . date("Y");
            }


            $groupsHelper = [
                'status' => ['name' => 'Status'],
                'sinif' => ['name' => 'Sinif', 'select' => "msk_classes.name as name, msk_classes.id as id, COUNT(1) as count", 'group' => 'msk_classes.name, msk_classes.id , msk_classes.[order]'],
                'age' => ['name' => 'Yaş aralığı', 'select' => "
                (CASE
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 5  THEN  N'6 yaşdan kiçik'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 6 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 9 THEN '6-9'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 10 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=12 THEN '10-12'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 13 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 15 THEN '13-15'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 16 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 18 THEN '16-18'
                    WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 19 THEN '18+'
                    ELSE N'Təyin edilməyib' END) as name,
                    (CASE
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 5  THEN 1
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 6 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 9 THEN 2
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 10 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=12 THEN 3
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 13 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 15 THEN 4
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 16 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 18 THEN 5
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 19 THEN 6
                    ELSE 7 END) as id, COUNT(1) as count", 'group' => '
                    (CASE
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 5  THEN N\'6 yaşdan kiçik\'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 6 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 9 THEN \'6-9\'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 10 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=12 THEN \'10-12\'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 13 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 15 THEN \'13-15\'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 16 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 18 THEN \'16-18\'
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 19 THEN \'18+\'
                    ELSE N\'Təyin edilməyib\' END),
                    (CASE
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 5  THEN 1
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 6 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 9 THEN 2
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 10 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <=12 THEN 3
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 13 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 15 THEN 4
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 16 AND CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) <= 18 THEN 5
                        WHEN CAST(DATEDIFF(d,birthday,GETDATE())/365.25 AS INT) >= 19 THEN 6
                    ELSE 7 END)'],
                'gender' => ['name' => 'Cinsi', 'select' => "(CASE WHEN users.gender='F' THEN N'Qadın' ELSE N'Kişi'  END) as name, (CASE WHEN users.gender='F' THEN 2 ELSE 1 END) as id, COUNT(1) as count", 'group' => "(CASE WHEN users.gender='F' THEN N'Qadın' ELSE N'Kişi'  END), users.gender"],
                'current_region' => ['name' => 'Faktiki rayon', 'select' => "current_regions.name as name,(SELECT name FROM msk_cities WHERE id = current_regions.msk_cities_id) as city, current_regions.id as id, COUNT(1) as count", 'group' => 'current_regions.name,current_regions.msk_cities_id,current_regions.id'],
                'lived_region' => ['name' => 'Qeydiyyat rayon', 'select' => "lived_regions.name as name,(SELECT name FROM msk_cities WHERE id = lived_regions.msk_cities_id) as city, lived_regions.id as id, COUNT(1) as count", 'group' => 'lived_regions.name,lived_regions.msk_cities_id,lived_regions.id']
            ];

            if ($group != 'status') {
                if ($group != 'sinif') {
                    $studentsFirst = User::realData('student')->withTrashed()
                        ->leftJoin('msk_regions as current_regions', 'current_regions.id', 'users.current_region')
                        ->leftJoin('msk_regions as lived_regions', 'lived_regions.id', 'users.lived_region')
                        ->whereIn('users.id', function ($query) use ($startDate) {
                            $query->select('user_id')->from('user_histories')
                                ->whereRaw("(user_histories.created_at <= '$startDate' AND (user_histories.deleted_at >= '{$startDate}' OR user_histories.deleted_at is null))");
                        })->select(DB::raw($groupsHelper[$group]['select']))->groupBy(DB::raw($groupsHelper[$group]['group']))->orderBy('id')->get();

                    $studentsLast = User::realData('student')
                        ->leftJoin('msk_regions as current_regions', 'current_regions.id', 'users.current_region')
                        ->leftJoin('msk_regions as lived_regions', 'lived_regions.id', 'users.lived_region')
                        ->select(DB::raw($groupsHelper[$group]['select']))
                        ->groupBy(DB::raw($groupsHelper[$group]['group']))->orderBy('id')->get();
                }
                else {

                    $notDefinedUsersFirst = User::realData('student')->withTrashed()
                        ->leftJoin('letter_group_user','users.id','letter_group_user.user_id')
                        ->whereNull('letter_group_user.user_id')
                        ->whereIn('users.id', function ($query) use ($startDate) {
                            $query->select('user_id')->from('user_histories')
                                ->whereRaw("(user_histories.created_at <= '$startDate' AND (user_histories.deleted_at >= '{$startDate}' OR user_histories.deleted_at is null))");
                        })
                        ->select(DB::raw("N'Təyin edilməyib' as name"),DB::raw("0 as id"),DB::raw("COUNT(*) as count"))
                        ->first();

                    $studentsFirst = User::realData('student')->withTrashed()
                        ->leftJoin('letter_group_user', 'letter_group_user.user_id', 'users.id')
                        ->leftJoin('letter_groups', 'letter_groups.id', '=', 'letter_group_user.letter_group_id')
                        ->leftJoin('class_letters', 'class_letters.id', '=', 'letter_groups.class_letter_id')
                        ->leftJoin('msk_classes', 'msk_classes.id', '=', 'class_letters.msk_class_id')
                        ->leftJoin('msk_regions as current_regions', 'current_regions.id', 'users.current_region')
                        ->leftJoin('msk_regions as lived_regions', 'lived_regions.id', 'users.lived_region')
                        ->where('letter_group_user.year_id',YearDays::getCurrentYear()->id)
                        ->whereIn('users.id', function ($query) use ($startDate) {
                            $query->select('user_id')->from('user_histories')
                                ->whereRaw("(user_histories.created_at <= '$startDate' AND (user_histories.deleted_at >= '{$startDate}' OR user_histories.deleted_at is null))");
                        })->select(DB::raw($groupsHelper[$group]['select']))
                        ->groupBy(DB::raw($groupsHelper[$group]['group']))
                        ->orderBy('msk_classes.order')
                        ->get();

                    $studentsFirst[] = $notDefinedUsersFirst;

                    $year = YearDays::getCurrentYear();

                    $notDefinedUsersLast =
                        User::realData('student')
                            ->leftJoin('letter_group_user', function ($join) use($year) {
                                $join->on('users.id', '=', 'letter_group_user.user_id')->on('letter_group_user.year_id', DB::raw($year->id));
                            })
                            ->whereNull('letter_group_user.user_id')
                            ->select(DB::raw("N'Təyin edilməyib' as name"),DB::raw("0 as id"),DB::raw("COUNT(*) as count"))
                            ->first();

//                        User::realData('student')->withTrashed()
//                        ->leftJoin('letter_group_user','users.id','letter_group_user.user_id')
//                        ->whereNull('letter_group_user.user_id')
//                        ->select(DB::raw("N'Təyin edilməyib' as name"),DB::raw("0 as id"),DB::raw("COUNT(*) as count"))
//                        ->first();

                    $studentsLast = User::realData('student')
                        ->leftJoin('letter_group_user', 'letter_group_user.user_id', 'users.id')
                        ->leftJoin('letter_groups', 'letter_groups.id', '=', 'letter_group_user.letter_group_id')
                        ->leftJoin('class_letters', 'class_letters.id', '=', 'letter_groups.class_letter_id')
                        ->leftJoin('msk_classes', 'msk_classes.id', '=', 'class_letters.msk_class_id')
                        ->leftJoin('msk_regions as current_regions', 'current_regions.id', 'users.current_region')
                        ->leftJoin('msk_regions as lived_regions', 'lived_regions.id', 'users.lived_region')
                        ->where('letter_group_user.year_id',YearDays::getCurrentYear()->id)
                        ->select(DB::raw($groupsHelper[$group]['select']))
                        ->groupBy(DB::raw($groupsHelper[$group]['group']))
                        ->orderBy('msk_classes.order')->get();

                   $studentsLast[] = $notDefinedUsersLast;

                }

                $mergedData = [];

                foreach ($studentsFirst as $data) {
                    $data['name'] = isset($data['city']) ? $data['name'] . " ( " . $data['city'] . " )" : $data['name'];
                    $mergedData[(int)$data['id']] = ['name' => $data['name'], 'countFirst' => $data['count'], 'countLast' => 0];
                }

                foreach ($studentsLast as $data) {
                    $data['name'] = isset($data['city']) ? $data['name'] . " ( " . $data['city'] . " )" : $data['name'];
                    if (!isset($mergedData[(int)$data['id']])) $mergedData[(int)$data['id']] = ['name' => $data['name'], 'countFirst' => 0, 'countLast' => $data['count']];
                    $mergedData[(int)$data['id']]['countLast'] = $data['count'];
                }

            } else {

                $allFirst = User::realData('student')->withTrashed()
                    ->whereRaw("CAST(created_at as DATE) <= '$startDate'")->count();

                $aktivFirst = User::realData('student')->withTrashed()
                    ->whereIn('users.id', function ($query) use ($startDate) {
                        $query->select('user_id')->from('user_histories')
                            ->whereRaw("(CAST(user_histories.created_at as DATE) <= '$startDate' AND (CAST(user_histories.deleted_at as DATE) >= '{$startDate}' OR user_histories.deleted_at is null))");
                    })->count();

                $allLast = User::realData('student')->withTrashed()
                    ->whereRaw("CAST(created_at as DATE) <= '$endDate'")->count();

                $aktivLast = User::realData('student')->count();

                $mergedData[] = ['name' => 'Aktiv', 'countFirst' => $aktivFirst, 'countLast' => $aktivLast];
                $mergedData[] = ['name' => 'Deaktiv', 'countFirst' => ($allFirst - $aktivFirst), 'countLast' => ($allLast - $aktivLast)];
            }

            //Cache::put('report_student', ['mergedData' => $mergedData], 360000);
            Cache::put('report_student_statistic', ['mergedData' => $mergedData, 'groupName' => $groupsHelper[$group]['name'], 'tarix' => $tarix], 360000);

            return View::make('pages.reports.student_statistic.table', ['mergedData' => $mergedData, 'groupName' => $groupsHelper[$group]['name']])->render();
        }
    }

    public function exportAction($type)
    {

        $mergedData = Cache::get('report_student_statistic')['mergedData'];
        $groupName = Cache::get('report_student_statistic')['groupName'];
        $tarix = Cache::get('report_student_statistic')['tarix'];

        if ($type == "pdf") {
            $pdf = PDF::loadView('pages.reports.student_statistic.table', ['mergedData' => $mergedData, 'groupName' => $groupName, 'tableTitle' => 'Şagird Statistik Hesabatı', 'tableDate' => $tarix])->setPaper('a4', 'landscape');
            return $pdf->download('student_statistic_reports.pdf');
        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('student_statistic_reports', function ($excel) use ($groupName, $mergedData, $tarix) {
                $excel->sheet('student_statistic_reports', function ($sheet) use ($groupName, $mergedData, $tarix) {
                    $currentRow = 5;
                    $currentCol = 1;
                    $columns = ["NO", $groupName, "Dövrün əvvəlinə şagird sayı", "Dövrün sonuna şagird sayı"];

                    foreach ($columns as $column) {
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentCol++;
                    }
                    $sheet->mergeCells(Helper::getCell(1, 1) . ':' . Helper::getCell(3, 1));
                    $sheet->cell(Helper::getCell(1, 1), function ($cell) use ($column) {
                        $cell->setValue("      Şagird Statistik Hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->mergeCells(Helper::getCell(1, 3) . ':' . Helper::getCell(3, 3));
                    $sheet->cell(Helper::getCell(1, 3), function ($cell) use ($tarix) {
                        $cell->setValue($tarix);
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sumFirst = 0;
                    $sumLast = 0;
                    $number = 1;
                    foreach ($mergedData as $key => $data) {
                        $sumFirst += $data['countFirst'];
                        $sumLast += $data['countLast'];
                        $currentRow++;
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow,$number) {
                            $cell->setValue($number);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($data) {
                            $cell->setValue($data['name'] == '' ? 'Təyin edilməyib' : $data['name']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($data) {
                            $cell->setValue($data['countFirst']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($data) {
                            $cell->setValue($data['countLast']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });

                        $number ++ ;
                    }

                    $currentRow = $currentRow + 1;
                    $sheet->mergeCells(Helper::getCell(1, $currentRow) . ':' . Helper::getCell(2, $currentRow));
                    $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow) {
                        $cell->setValue('     Ümumi');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($sumFirst) {
                        $cell->setValue($sumFirst);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->cell(Helper::getCell(4, $currentRow), function ($cell) use ($sumLast) {
                        $cell->setValue($sumLast);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                });
            })->download('xls');
        }
    }
}
