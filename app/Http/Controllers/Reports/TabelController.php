<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\Library\Standarts;
use App\User;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class TabelController extends Controller
{
    public function index()
    {
        return view('reports.tabel.tabel');
    }

    public function tabelPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'month' => 'required|string',
            'groups' => 'nullable|string|in:staff,teacher',
            'heyetler' => 'nullable|integer|min:0',
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {

            $month = date('m', strtotime($request->get('month')));
            $year = date('Y', strtotime($request->get('month')));
            $daysOfNumber = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $startMonth = date('Y-m-d', strtotime($year . '-' . $month . '-01'));
            $endMonth = date('Y-m-d', strtotime($year . '-' . $month . '-' . $daysOfNumber));
            $tarix = Standarts::$months[$month - 1] . "-" . $year;

            //  dd($request->get('month'));

            $daysOfMonth = [];
            $users = User::realData($request->get('groups'))->withTrashed()
                ->whereIn('users.id', function ($query) use ($startMonth, $endMonth) {
                    $query->distinct()->select('user_id')->from('user_histories')
                        ->whereRaw("( (user_histories.created_at <= '{$endMonth}' AND (user_histories.deleted_at >= '{$startMonth}' OR user_histories.deleted_at is null)) OR (user_histories.created_at >= '{$endMonth}' AND (user_histories.deleted_at <= '{$startMonth}' OR user_histories.deleted_at is null)))");
                });


            if ($request->get('heyetler') > 0 && $request->get('groups') == "staff") $users->where('personal_heyeti', $request->get('heyetler'));
            $users = $users->get();

            $usersInWork = DB::table('tblFpRegisters as tb1')
                ->leftJoin('tblFpInOuts as tb2', function ($join) {
                    $join->on('tb2.reg_id', '=', 'tb1.id')
                        ->where('tb2.in_out_type', '=', 'In');
                })
                ->whereBetween(DB::raw('CAST(tb2.[date] as DATE)'), [$startMonth, $endMonth])
                ->select('tb1.uid as user_id', DB::raw('DAY(tb2.[date]) as day'))->get();

            foreach ($users as $user) {
                $workDays = 0;
                for ($i = 1; $i <= $daysOfNumber; $i++) {
                    if ($i < 10) $i = '0' . $i;
                    $tableControl = "-";
                    if (date('D', strtotime($i . '-' . $month . '-' . $year)) == "Sun") {
                        $tableControl = "Bz";
                    } else if (date('D', strtotime($i . '-' . $month . '-' . $year)) == "Sat") {
                        $tableControl = "Şn";
                    } else {
                        foreach ($usersInWork as $userInWork) {
                            if ($i == $userInWork->day && $user->id == $userInWork->user_id) {
                                $tableControl = "x";
                                ++$workDays;
                                break;
                            }
                        }
                    }
                    $daysOfMonth[$i] = $tableControl;
                    $user['dayOfMonth'] = $daysOfMonth;
                    $user['workDays'] = $workDays;
                }
            }


            $data = ['users' => $users, 'daysOfMonth' => $daysOfMonth];

            Cache::put('report_tabel', ['data' => $data, 'tarix' => $tarix], 360000);

            return View::make('pages.reports.tabel.tabel', $data)->render();
        }

    }

    public function tabelExport($type)
    {

        $data = Cache::get('report_tabel')['data'];
        $tarix = Cache::get('report_tabel')['tarix'];

        if ($type == "pdf") {
            $pdf = PDF::loadView('pages.reports.tabel.tabel', ['users' => $data['users'], 'daysOfMonth' => $data['daysOfMonth'], 'tableTitle' => 'Tabel  Hesabatı', 'tableDate' => $tarix])->setPaper('a3', 'landscape');
            return $pdf->download('tabel_reports.pdf');
        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('tabel_reports', function ($excel) use ($data, $tarix) {
                $excel->sheet('tabel_reports', function ($sheet) use ($data, $tarix) {
                    $currentRow = 5;
                    $currentCol = 1;
                    $columns = ["No", "ADI SOYADI ATA ADI", "VƏZİFƏ"];
                    foreach (array_keys($data['daysOfMonth']) as $dayOfMonth) {
                        array_push($columns, $dayOfMonth);
                    }
                    array_push($columns, "İş saatı", "İş günləri", "Qeyd");

                    foreach ($columns as $column) {
                        $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                            $cell->setValue($column);
                            $cell->setBackground('#e7e6e6');
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $currentCol++;
                    }

                    $sheet->mergeCells(Helper::getCell(1, 1) . ':' . Helper::getCell(3, 1));
                    $sheet->cell(Helper::getCell(1, 1), function ($cell) use ($column) {
                        $cell->setValue("      Tabel  Hesabatı");
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    $sheet->mergeCells(Helper::getCell(1, 3) . ':' . Helper::getCell(3, 3));
                    $sheet->cell(Helper::getCell(1, 3), function ($cell) use ($tarix) {
                        $cell->setValue($tarix);
                        $cell->setBackground('#e7e6e6');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });


                    $siraNo = 1;
                    foreach ($data['users'] as $user) {
                        $columnNumber = 3;
                        $currentRow++;
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($siraNo) {
                            $cell->setValue($siraNo);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($user) {
                            $cell->setValue($user->fullname());
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($user) {
                            $cell->setValue($user->position);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        foreach ($user['dayOfMonth'] as $key => $item) {
                            $sheet->cell(Helper::getCell(3 + $key, $currentRow), function ($cell) use ($item) {
                                $cell->setValue($item);
                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            });
                            $columnNumber = $columnNumber + 1;
                        }
                        $sheet->cell(Helper::getCell($columnNumber + 1, $currentRow), function ($cell) use ($user) {
                            $cell->setValue($user['workDays'] * 8);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell($columnNumber + 2, $currentRow), function ($cell) use ($user) {
                            $cell->setValue($user['workDays']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell($columnNumber + 3, $currentRow), function ($cell) use ($user) {
                            $cell->setValue("-");
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });

                        $siraNo++;
                    }
                });
            })->download('xls');
        }
    }
}
