<?php

namespace  App\Http\Controllers\Reports;


use App\Http\Controllers\Controller;
use App\Library\LessonDaysFixer;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\ClassLetter;
use App\Models\ClassTime;
use App\Models\Homework;
use App\Models\HomeworkFile;
use App\Models\Lecture;
use App\Models\LectureFile;
use App\Models\LessonDay;
use App\Models\LetterGroup;
use App\Models\Mark;
use App\Models\MarkType;
use App\Models\MskClass;
use App\Models\MskMarks;
use App\Models\Sms;
use App\Models\Subject;
use App\Models\Year;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class JurnalController extends Controller
{
    public function index()
    {
        return view('jurnal.jurnal');
    }

    public function yearClasses(Request $request)
    {
        $validator = validator($request->all(), [
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . auth()->user()->tenant_id
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {
            $yearId = $request->get('year_id', 0);
            $results = [];

            $classes = LetterGroup::realData()
                ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->join('corpuses', 'corpuses.id', 'class_letters.corpus_id')
                ->where('class_letters.year_id', $yearId)
                ->where(DB::raw("concat(msk_classes.name,'',class_letters.name,'','(',corpuses.name,'',')')"),'like','%'.$request->get('q').'%')
                ->select('letter_groups.id','letter_groups.name as letter_group_name','class_letters.id as letter_id','msk_classes.id as class_id', 'msk_classes.name as class_name', 'class_letters.name as letter', 'corpuses.name as corpus_name')
                ->orderBy('msk_classes.order', 'ASC')
                ->get();

            foreach ($classes as $class)
            {
                $results[] = ['id' => $class->id,'letter_id' => $class->letter_id, 'class_id' => $class->class_id, 'text'=> ($class->class_name . $class->letter . "(".$class->corpus_name.")"."/"."(".$class->letter_group_name.")")];
            }

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function classSubjects(Request $request)
    {
        $validator = validator($request->all(), [
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . auth()->user()->tenant_id
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {
            $classId = $request->get('class_id', 0);
            $letter_group_id = $request->get('letter_group_id', 0);
            $results = [];

//            $classes = MskClass::realData()
//                ->where('id', $classId)
//                ->first();

            $subjects = Subject::realData()
                ->join('lessons','lessons.subject_id','subjects.id')
                ->where('lessons.letter_group_id',$letter_group_id)
                ->where('subjects.name','like','%'.$request->get('q').'%')
                ->groupBy('subjects.id','subjects.name')
                ->get(['subjects.id','subjects.name']);

            foreach ($subjects as $subject)
            {
                $results[] = ['id' => $subject->id, 'text'=> $subject->name ];
            }

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function studentsPage(Request $request)
    {
        $validator = validator($request->all(), [
            'letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . auth()->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . auth()->user()->tenant_id,
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . auth()->user()->tenant_id,
            //'year' => 'required|integer',
            //'month' => 'required|integer|digits_between:1,12',
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {
            $classLetterId = $request->get('letter_id');
            $yearId = $request->get('year_id');
            $yearData = Year::realData()->find($yearId);
            $subjectId = $request->get('subject_id', 0);

            $yearStartDate = Carbon::parse($yearData->start_date);
            $yearEndDate = Carbon::parse($yearData->end_date);
            $year = (int)$request->get('year', null);
            $month = (int)$request->get('month', null);
            if(!$year || !$month)
            {
                $yearStartDate = Carbon::parse($yearData->start_date);
                $year = $yearStartDate->year;
                $month = $yearStartDate->month;
            }
            $selectedMonth = Carbon::create($year, $month, 1);

            if(!($selectedMonth->gte($yearStartDate) && $selectedMonth->lte($yearEndDate)))
            {
                $yearStartDate = Carbon::parse($yearData->start_date);
                $year = $yearStartDate->year;
                $month = $yearStartDate->month;
            }
            $selectedMonth = Carbon::create($year, $month, 1);
            $offest = $month + $year * 12;

            $classAllCounts = 0;

            $students = LetterGroup::realData()
                ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
                ->leftJoin('users', 'users.id', 'letter_group_user.user_id')
                ->orderBy(DB::raw('CONCAT(users.name,users.surname,users.middle_name)'))
                ->select(DB::raw("CONCAT(users.name,' ',users.surname,' ',users.middle_name) as f_name"), 'users.id as user_id')
                ->where('letter_groups.class_letter_id', $classLetterId)->get();

            $userMarksGroup = [];
            $studentMarks = Mark::realData()->where('subject_id', $subjectId)
                ->where('class_letter_id', $classLetterId)
                ->whereRaw("YEAR(date)*12+MONTH(date)='$offest'")
                ->select('*', DB::raw('DAY(date) as day'))
                ->get();
            foreach ($studentMarks as $studentMark) {
                $markDayType = $studentMark->type == 'day' ? $studentMark->class_time_id : $studentMark->type;

                if (!isset($userMarksGroup[$studentMark->student_id])) $userMarksGroup[$studentMark->student_id] = [];
                if (!isset($userMarksGroup[$studentMark->student_id][$studentMark->day])) $userMarksGroup[$studentMark->student_id][$studentMark->day] = [];
                $userMarksGroup[$studentMark->student_id][$studentMark->day][$markDayType] = $studentMark;
            }

            $classDays = LessonDay::realData()
                ->leftJoin('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                ->whereRaw("lesson_days.date >= '{$selectedMonth->format("Y-m-d")}' AND lesson_days.date <= '{$selectedMonth->lastOfMonth()->format("Y-m-d")}'")
                ->where(function ($where) use ($classLetterId, $subjectId) {
                    $where->where(function ($where) use ($classLetterId, $subjectId) {
                        $where->where('letter_groups.class_letter_id', $classLetterId)
                            ->where('lessons.subject_id', $subjectId);
                    })->orWhereIn('lesson_days.type', ['ksq', 'bsq', 'yi', 'y']);
                })
                ->select('lesson_days.type', 'lesson_days.date', 'class_times.start_time', 'class_times.end_time', 'class_times.id as ct_id')
                ->orderBy('lesson_days.date')
                ->orderBy('lesson_days.order')
                ->orderBy('class_times.start_time')
                ->get();

            $lectures = Lecture::realData()
                ->whereRaw("YEAR(date)='$year' AND MONTH(date)='$month'")
                ->where('class_letter_id', $classLetterId)
                ->where('subject_id', $subjectId)->get();

            $homeworks = Homework::realData()
                ->whereRaw("YEAR(date)='$year' AND MONTH(date)='$month'")
                ->where('class_letter_id', $classLetterId)
                ->where('subject_id', $subjectId)->get();

            $data = [
                'students' => $students,
                'classDays' => $classDays,
                'year' => $year,
                'month' => $month,
                'classAllCounts' => $classAllCounts,
                'subjectId' => $subjectId,
                'classLetterId' => $classLetterId,
                'userMarksGroup' => $userMarksGroup,
                'lectures' => $lectures,
                'homeworks' => $homeworks,
                'selectedMonth' => $selectedMonth,
                'yearStartDate' => $yearStartDate->firstOfMonth(),
                'yearEndDate' => $yearEndDate->lastOfMonth(),
            ];
//dd($selectedMonth);
            return view('pages.jurnal.jurnal', $data);
        }
    }
}
