<?php

namespace App\Http\Controllers\Reports;

use App\Library\Helper;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class AttendancePersonalController extends Controller
{
    public function index()
    {
        return view('reports.attendance_personal.attendance');
    }

    public function attendancePage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'date_type' => 'required|string|in:date,month',
            'date' => 'required|date_format:d-m-Y',
            'month' => 'required|date_format:d-m-Y',
            'heyetler' => 'nullable|integer|exists:msk_heyetlers,id,tenant_id,'. Auth::user()->tenant_id
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return $errors;
        } else {

            if($request->get('date_type') == "date"){
                $firstDay = date("m/d/Y", strtotime($request->get('date')));
                $lastDay = date("m/d/Y", strtotime($request->get('date')));
            }
            else{
                $firstDay = date("m/01/Y", strtotime($request->get('month')));
                $lastDay = date("m/t/Y", strtotime($request->get('month')));
            }

            $users = User::realData('staff')->withTrashed()
                ->whereIn('users.id', function ($query)use($firstDay, $lastDay){ $query->distinct()->select('user_id')->from('user_histories')
                    ->whereRaw("( (user_histories.created_at <= '{$lastDay}' AND (user_histories.deleted_at >= '{$firstDay}' OR user_histories.deleted_at is null)) OR (user_histories.created_at >= '{$lastDay}' AND (user_histories.deleted_at <= '{$firstDay}' OR user_histories.deleted_at is null)))");
                })
                ->join('msk_heyetlers', 'msk_heyetlers.id', 'users.personal_heyeti')
                ->groupBy(DB::raw("users.name,users.surname,users.middle_name,users.position,CAST(tblFpInOuts.[date] as DATE),tblFpInOuts.in_out_type,users.id"))
                ->select(DB::raw("MAX(msk_heyetlers.start_time) as start_time,MAX(msk_heyetlers.end_time) as end_time,users.name,users.surname,users.middle_name,users.position,CAST(tblFpInOuts.[date] as DATE) as date,tblFpInOuts.in_out_type,users.id,CAST(MIN(tblFpInOuts.[date]) as TIME) as min_time,CAST(MAX(tblFpInOuts.[date]) as TIME) as max_time"));

            if ($request->has('heyetler')) $users->where('personal_heyeti', $request->get('heyetler'));
            $users = $users->get();

            //dd($users->toArray());

            $userData = [];
            foreach ($users as $user){
                if(!isset($userData[$user->id])) $userData[$user->id] = ['fullname'=> $user->name." ".$user->surname." ".$user->middle_name, 'position'=> $user->position ];
                if($user->date != ''){
                    if(!isset($userData[$user->id][$user->date])) $userData[$user->id][$user->date] = ['In' => '-', 'Out'=> '-'];

                    if($user->in_out_type == 'In') $userData[$user->id][$user->date]['In'] = date("H:i:s", strtotime($user->min_time) - strtotime($user->start_time) );
                    else $userData[$user->id][$user->date]['Out'] = date("H:i:s", strtotime($user->end_time)  - strtotime($user->max_time));
                }
            }
            $data = ["userData" => $userData,"firstDay" => $firstDay,"lastDay" => $lastDay];

            Cache::put('report_attendance', ['data' => $data], 360000);

            return View::make('pages.reports.attendance_personal.attendance',['data'=>$data])->render();
        }
    }

    public function attendanceExport($type)
    {
        $data = Cache::get('report_attendance')['data'];

        if ($type == "pdf") {
            $pdf = PDF::loadView('pages.reports.attendance.attendance',['data'=>$data])->setPaper('a1', 'landscape');
            return $pdf->download('attendance_reports.pdf');
        } else {
            return \Maatwebsite\Excel\Facades\Excel::create('attendance_reports', function ($excel) use ($data) {
                $excel->sheet('attendance_reports', function ($sheet) use ($data) {
                    $currentRow = 1;
                    $currentCol = 1;
                    $columns = ["No", "ADI SOYADI ATA ADI","VƏZİFƏ"];
                    for($date = \Illuminate\Support\Carbon::parse($data['firstDay']); $date->lte(\Illuminate\Support\Carbon::parse($data['lastDay'])); $date->addDay()){
                        array_push($columns, $date->format('Y-m-d'));
                    }

                    foreach ($columns as $key => $column) {
                        if ($key > 2) {
                            $sheet->mergeCells(Helper::getCell($currentCol, $currentRow).':'.Helper::getCell($currentCol+1, $currentRow));
                            $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                                $cell->setValue('    '.$column);
                                $cell->setBackground('#e7e6e6');
                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            });
                            $currentCol = $currentCol + 1;
                        }
                       else{
                           $sheet->cell(Helper::getCell($currentCol, $currentRow), function ($cell) use ($column) {
                               $cell->setValue($column);
                               $cell->setBackground('#e7e6e6');
                               $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           });
                       }
                        $currentCol = $currentCol + 1;
                    }

                    $currentRow = 2;
                    $colValueInOrOut = "ÇIXIŞ";
                    for ($i = 1;$i<$currentCol;$i++){
                        if ($i > 3){
                            $sheet->cell(Helper::getCell($i, $currentRow), function ($cell) use ($colValueInOrOut) {
                                $cell->setValue($colValueInOrOut);
                                $cell->setBackground('#e7e6e6');
                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            });
                        }
                        else{
                            $sheet->cell(Helper::getCell($i, $currentRow), function ($cell){
                                $cell->setValue("");
                                $cell->setBackground('#e7e6e6');
                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            });
                        }
                        $colValueInOrOut = $colValueInOrOut == "GİRİŞ" ? "ÇIXIŞ" : "GİRİŞ";
                    }

                    foreach ($data['userData'] as $user) {
                        $columnNumber = 4;
                        $currentRow++;
                        $sheet->cell(Helper::getCell(1, $currentRow), function ($cell) use ($currentRow) {
                            $cell->setValue($currentRow - 2);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(2, $currentRow), function ($cell) use ($user) {
                            $cell->setValue($user['fullname']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $sheet->cell(Helper::getCell(3, $currentRow), function ($cell) use ($user) {
                            $cell->setValue($user['position']);
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                        $inOrOut = 1;

                        $date = \Illuminate\Support\Carbon::parse($data['firstDay']);

                        for ($columnNumber;$columnNumber<$currentCol;$columnNumber++) {
                            $sheet->cell(Helper::getCell($columnNumber, $currentRow), function ($cell) use ($user,$date,$inOrOut,$data) {
                                if ($inOrOut == 1){
                                    $cell->setValue(isset($user[$date->format('Y-m-d')]) ? $user[$date->format('Y-m-d')]['In'] : '-');
                                }
                                else{
                                    $cell->setValue(isset($user[$date->format('Y-m-d')])? $user[$date->format('Y-m-d')]['Out'] : '-');
                                }
                                $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            });
                            $inOrOut = $inOrOut == 1 ? 2 : 1;
                            if($columnNumber%2 == 0) $date = $date->addDay();
                        }
                    }
                });
            })->download('xls');
        }
    }
}
