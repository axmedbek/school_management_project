<?php

namespace App\Http\Controllers;

use App\Models\MskHeyetler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MskHeyetlerController extends Controller
{
    public function index(Request $request){

        $data = [
            'heyetler' => MskHeyetler::realData()->orderByDesc('id')->get(),
            'request' => $request
        ];

        return view('msk.heyetler.heyetler',$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function AddEditAction(Request $request){
        $hasError = false;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:30',
            'id' => 'required|integer',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        if(strtotime($request->get('start_time')) > strtotime($request->get('end_time'))){
            $validator->errors()->add('error','Başlama vaxtı bitmə vaxtında böyük ola bilməz');
            $hasError = true;
        }

        if ($hasError || $validator->fails()){

            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            if($request->get('id')==0){
                $mskHeyetler = new MskHeyetler();
            }
            else{
                $mskHeyetler = MskHeyetler::realData()->find($request->get('id'));
            }

            $mskHeyetler->name = $request->get('name');
            $mskHeyetler->tenant_id = Auth::user()->tenant_id;
            $mskHeyetler->start_time = $request->get('start_time');
            $mskHeyetler->end_time = $request->get('end_time');
            $mskHeyetler->save();

            $checkHeyetler = \App\User::realData()->where('personal_heyeti',$mskHeyetler->id)->first();
            if(!isset($checkHeyetler)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $mskHeyetler["buttons"] = $buttons;

            return response()->json(['status'=>'ok','data'=>$mskHeyetler->toArray()]);

        }
    }

    public function DeleteAction(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:msk_heyetlers,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);

        }
        else{

            $mskHeyetler = MskHeyetler::realData()->find($request->get('id'));
            $mskHeyetler->delete();


            return response()->json(['status'=>'ok']);


        }
    }
}
