<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\MskClass;
use App\Models\Season;
use App\Models\SeasonParagraph;
use App\Models\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TopicController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'request' => $request,
        ];

        return view('topics.topics', $data);
    }

    public function classSubjects(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            $class = MskClass::realData()->find($request->get('class_id'));
            $subjects = $class->subjects()
                ->where('subjects.name', 'like', '%' . $request->get('q') . '%')
                ->select('subjects.id as id', 'subjects.name as name')
                ->get();

            $results = [];
            foreach ($subjects as $subject)
                $results[] = ['id' => $subject->id, 'text' => $subject->name];

            return response()->json(['status' => 'ok', "results" => $results]);
        }
    }

    public function topicPage(Request $request)
    {
        $searchedKey = "";

        $validator = Validator::make($request->all(), [
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            $seasons = Season::realData()->where('msk_class_id', $request->get('class_id'))
                ->where('subject_id', $request->get('subject_id'));
            if ($request->has('searchedKey')) {
                $searchedKey = $request->get('searchedKey');
                $seasons->where('seasons.name', 'like', '%' . $searchedKey . '%');
                $seasons = $seasons->get();

                $data = "";
                $icons = "";
                foreach ($seasons as $season) {
                    if(Helper::has_priv('topics',\App\Library\Standarts::PRIV_CAN_EDIT)) {
                       $icons = "<i style = \"font-size: 15px;cursor: pointer;color:blueviolet;\" class=\"fa fa-edit\"
                               onclick = \"openModal('".route('topic_season_add_edit',['season'=> $season->id])."',{class_id: ".$request->get('class_id').", subject_id:".$request->get('subject_id')." } )\" ></i >
                            <i style = \"font-size: 15px;cursor: pointer;color: red;\"
                               class=\"fa fa-trash sesson_delete\" onclick=\"deleteSession(this)\"></i >";
                    }
                    $data .= "<li season_id = " . $season->id . " >
                            <a data-toggle=\"tab\" href=\"#tab".$season->id."\"
                       style=\"max-width: 500px;word-wrap: break-word;font-family: Ubuntu;letter-spacing: 0.4px;\">" . $season->name . "
                        <span style=\"
                            font-size: 14px;
                            text-transform: initial;
                            font-family: Ubuntu;
                            font-weight: 400;
                            letter-spacing: 0.4px;\">
                            (" . implode(",", $season->group_types->pluck('name')->toArray()) . ")
                        </span>
                        ".$icons."
                        </a>
                        </li >";
                }
                return response(['status' => 'ok', 'data' => $data]);
            }
            $seasons = $seasons->get();

            return view('pages.topics.topics', ['searchedKey' => $searchedKey, 'seasons' => $seasons, 'change' => $request->get('change', true), 'class_id' => $request->get('class_id'), 'subject_id' => $request->get('subject_id')]);
        }
    }

    public function seasonAddEditModal(Request $request, $season)
    {
        $data = [
            'season' => Season::realData()->find($season),
            'class_id' => $request->get('class_id', 0),
            'subject_id' => $request->get('subject_id', 0),
            'isCopy' => $request->get('isCopy', 0),
            'copySeasonId' => $request->get('copySeasonId', 0),
            'id' => $season
        ];

        return view('modals.topics.season_add_edit', $data);
    }

    public function getParagraphs()
    {
        $searchedKey = request('searchedKey');
        $season_id = request('season_id');

        $paragraphs = SeasonParagraph::realData()->where('season_id', $season_id)
            ->where('name', 'like', '%' . $searchedKey . '%')
            ->get();

        $tr = "";
        $buttons = "";
        foreach ($paragraphs as $key => $paragraph) {
            if (Helper::has_priv('topics', Standarts::PRIV_CAN_EDIT)) {
                $buttons = "<div class=\"btn-group-sm\">
                                            <a type=\"button\" class=\"btn btn-primary col-lg-4 col-md-12 edit_data\"><i
                                                        class=\"fa fa-pencil\"></i></a>
                                            <a type=\"button\" class=\"btn btn-danger col-lg-4 col-md-12 data_delete\"><i
                                                        class=\"fa fa-trash-o\"></i></a>
                                        </div>";
            } else {
                $buttons = "<span class=\"badge badge-danger\">Əməliyyat apara bilməzsiniz</span>";
            }
            $tr .= "<tr tr_id=" . $paragraph->id . ">
                                <td>" . ($key + 1) . "</td>
                                <td>" . $paragraph->name . "</td>
                                <td>" . $paragraph->standard . "</td>
                                <td>" . $paragraph->hour . "</td>
                                <td>" . $buttons . "</td>
                            </tr>";
        }

        return response()->json(['status' => 'ok', 'data' => $tr]);
    }

    public function seasonAddEditAction(Request $request, $season)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'group_types' => 'array|required',
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::user()->tenant_id,
            'group_types.*' => 'required|integer|exists:group_types,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($season == 0) {
                $newSeason = new Season();
            } else {
                $newSeason = Season::realData()->find($season);
            }

            $newSeason->name = $request->get('name');
            $newSeason->msk_class_id = $request->get('class_id');
            $newSeason->subject_id = $request->get('subject_id');
            $newSeason->tenant_id = Auth::user()->tenant_id;
            $newSeason->save();

            //subjects
            $newSeason->group_types()->detach();
            foreach ($request->get('group_types', []) as $group_type)
                $newSeason->group_types()->attach($group_type);


            //check operation is to copy or not
            if ($request->get('isCopy') == 1 && $request->get('copySeasonId') != 0) {

                $copySeason = Season::realData()->find($request->get('copySeasonId'));
                foreach ($copySeason->paragraphs as $paragraph) {
                    $newParagraph = new SeasonParagraph();
                    $newParagraph->season_id = $newSeason->id;
                    $newParagraph->name = $paragraph->name;
                    $newParagraph->standard = $paragraph->standard;
                    $newParagraph->hour = $paragraph->hour;
                    $newParagraph->tenant_id = Auth::user()->tenant_id;
                    $newParagraph->save();
                }
            }

            return response()->json(['status' => 'ok']);


        }
    }

    public function seasonDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:seasons,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            $season = Season::realData()->find($request->get('id'));
            $season->delete();

            return response()->json(['status' => 'ok']);
        }
    }

    public function paragraphAddEditAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'standard' => 'string|nullable',
            'hour' => 'numeric|required|min:0',
            'id' => 'required|integer',
            'season_id' => 'required|integer|exists:seasons,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($request->get('id') == 0) {
                $newParagrahp = new SeasonParagraph();
            } else {
                $newParagrahp = SeasonParagraph::realData()->find($request->get('id'));
            }

            $newParagrahp->name = $request->get('name');
            $newParagrahp->standard = $request->get('standard');
            $newParagrahp->hour = $request->get('hour');
            $newParagrahp->season_id = $request->get('season_id');
            $newParagrahp->tenant_id = Auth::user()->tenant_id;
            $newParagrahp->save();

            foreach (Year::realData()->where('active', 1)->get() as $year)
                (new YearDays($year))->calculateDays();

            return response()->json(['status' => 'ok', 'data' => $newParagrahp->toArray()]);
        }
    }

    public function paragraphDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:season_paragraphs,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            $seasonParagrahp = SeasonParagraph::realData()->find($request->get('id'));
            $seasonParagrahp->delete();

            foreach (Year::realData()->where('active', 1)->get() as $year)
                (new YearDays($year))->calculateDays();

            return response()->json(['status' => 'ok']);
        }
    }
}
