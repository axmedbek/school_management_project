<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSearchRequest;
use App\Library\Standarts;
use App\Models\MarkType;
use App\Models\MskCities;
use App\Models\MskMarks;
use App\Models\MskRegions;
use App\Models\Option;
use App\Models\PersonFamily;
use App\Models\PersonLaborActivity;
use App\Models\PersonLanguage;
use App\Models\PersonScientificDegree;
use App\Models\PersonStudy;
use App\Models\Subject;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class TeacherController extends Controller
{
    public function teacher(UserSearchRequest $request)
    {
        $teachers = User::realData('teacher')->orderByDesc('id');

        if ($request->get('type', 'active') == 'deactive') $teachers->onlyTrashed();

        $this->filter($teachers, $request);

        $data = [
            'teachers' => $teachers->paginate(Standarts::$rowCount),
            'request' => $request
        ];

        return view('teachers.teachers', $data);
    }

    public function filter($object, $request)
    {
        if ($request->get('name')) $object->where(DB::raw("concat(name,' ',surname)"), 'like', '%' . $request->get('name') . '%');
        if ($request->get('birthday')) $object->where('birthday', date("Y-m-d", strtotime($request->get('birthday'))));
        if ($request->get('enroll_date')) $object->where('enroll_date', date("Y-m-d", strtotime($request->get('enroll_date'))));
        if ($request->get('home_tel')) $object->where('home_tel', 'like', '%' . $request->get('home_tel') . '%');
        if ($request->get('mobil_tel')) $object->where('mobil_tel', 'like', '%' . $request->get('mobil_tel') . '%');
        if ($request->get('email')) $object->where('email', 'like', '%' . $request->get('email') . '%');
    }

    public function teacherAddEditModal($staff)
    {
        $addressData = [];
        $languageData = [];
        $subjectData = [];

        $teacher = User::realData('teacher')->find($staff);

        //get address data
        $livedCity = MskCities::realData()->find((int)$teacher['lived_city']);
        $currentCity = MskCities::realData()->find((int)$teacher['current_city']);
        $livedRegion = MskRegions::realData()->find((int)$teacher['lived_region']);
        $currentRegion = MskRegions::realData()->find((int)$teacher['current_region']);
        $languages = PersonLanguage::realData()->where('user_id',$staff)->get();

        if (isset($teacher->teacher_subjects)){
            foreach ($teacher->teacher_subjects as $subject){
                $subjectData[] = ['id' => $subject->id , 'text' => $subject->name];
            }
        }

        foreach ($languages as $key => $language){
            $languageData[$key]['mark_name'] = ['id' => $language['msk_marks_id'],'text' => MskMarks::realData()->find($language['msk_marks_id'])->name];
        }


        if($livedCity){
            $addressData['lived_city'] = ['id' =>$livedCity['id'] , 'text' => $livedCity['name']];
        }

        if($currentCity){
            $addressData['current_city'] = ['id' =>$currentCity['id'] , 'text' => $currentCity['name'] ];
        }

        if($livedRegion){
            $addressData['lived_region'] = ['id' =>$livedRegion['id'] , 'text' => $livedRegion['name']];
        }

        if($currentRegion){
            $addressData['current_region'] = ['id' =>$currentRegion['id'] , 'text' => $currentRegion['name'] ];
        }

        $data = [
            'teacher' => $teacher,
            'addressData' => $addressData,
            'languageData' => $languageData,
            'subjectData' => $subjectData,
            'id' => $staff
        ];

        return view('modals.teachers.teachers_add_edit', $data);
    }

    public function teacherAddEditAction(Request $request, $user)
    {

        $validator = validator($request->all(), [
            'name' => 'required|string|max:30',
            'surname' => 'nullable|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'mobil_tel' => 'nullable|string|max:20',
            'home_tel' => 'nullable|string|max:20',
            'enroll_date' => 'nullable|date_format:d-m-Y',
            'birthday' => 'nullable|date_format:d-m-Y',
            'username' => 'nullable|string|max:30|unique:users,username,'.$user,
            'birth_place' => 'nullable|string',
            'email' => 'nullable|email|unique:users,email,' . $user,
            'gender' => 'required|string|in:m,f',
            'city' => 'nullable|string|max:20',
            'address' => 'nullable|string|max:255',
            'current_address' => 'nullable|string|max:255',
            'thumb' => 'nullable|image|max:3000',
            'internship' => 'nullable|numeric|min:0',
            'msk_study_levels_id' => 'integer|exists:msk_study_levels,id,tenant_id,' . Auth::user()->tenant_id,
            'msk_teacher_status_id' => 'integer|exists:msk_teacher_statuses,id,tenant_id,' . Auth::user()->tenant_id,
            //study
            'study' => 'nullable|array',
            'study.place' => 'array',
            'study.place.*' => 'required|string',
            'study.start_date' => 'array',
            'study.start_date.*' => 'required|date_format:"d-m-Y"',
            'study.end_date' => 'array',
            'study.end_date.*' => 'required|date_format:"d-m-Y"',
            'study.document' => 'array',
            'study.document.*' => 'nullable|string|max:60',
            'study.document_number' => 'array',
            'study.document_number.*' => 'nullable|string|max:60',
            //language
            'language' => 'nullable|array',
            'language.name' => 'array',
            'language.name.*' => 'required|string',
            'study.mark' => 'array',
            'study.mark.*' => 'required|integer|exists:mark_types,id,tenant_id,' . Auth::user()->tenant_id,
            //scientific_degree
            'scientific_degree' => 'nullable|array',
            'scientific_degree.degree' => 'array',
            'scientific_degree.degree.*' => 'required|string|max:60',
            'scientific_degree.issue_date' => 'array',
            'scientific_degree.issue_date.*' => 'required|date_format:"d-m-Y"',
            'scientific_degree.issued_by' => 'array',
            'scientific_degree.issued_by.*' => 'required|string|max:60',
            'scientific_degree.document_number' => 'array',
            'scientific_degree.document_number.*' => 'nullable|string|max:60',
            //labor_activity
            'labor_activity' => 'nullable|array',
            'labor_activity.workplace' => 'array',
            'labor_activity.workplace.*' => 'required|string|max:60',
            'labor_activity.start_date' => 'array',
            'labor_activity.start_date.*' => 'required|date_format:"d-m-Y"',
            'labor_activity.end_date' => 'array',
            'labor_activity.end_date.*' => 'required|date_format:"d-m-Y"',
            'labor_activity.position' => 'array',
            'labor_activity.position.*' => 'nullable|string|max:60',
            //family
            'family' => 'nullable|array',
            'family.relation' => 'array',
            'family.relation.*' => 'required|integer|exists:msk_relations,id,tenant_id,' . Auth::user()->tenant_id,
            'family.birthday' => 'array',
            'family.birthday.*' => 'required|date_format:"d-m-Y"',
            'family.name' => 'array',
            'family.name.*' => 'required|string|max:60',
            'family.position' => 'array',
            'family.position.*' => 'required|string|max:60',
            'family.address' => 'array',
            'family.address.*' => 'nullable|string',
            //subject
            'subject' => 'nullable|array',
            'subject.subject_id' => 'array',
            'subject.subject_id.*' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($user == 0) {
                $newUser = new User();
            } else {
                $newUser = User::realData('teacher')->find($user);
            }

            $newUser->name = $request->get('name');
            $newUser->surname = $request->get('surname');
            $newUser->middle_name = $request->get('middle_name');
            $newUser->mobil_tel = $request->get('mobil_tel');
            $newUser->home_tel = $request->get('home_tel');
//            $newUser->enroll_date = !empty($request->get('enroll_date')) ? date("Y-m-d", strtotime($request->get('enroll_date'))) : null;
            $newUser->enroll_date = Carbon::today();
            $newUser->birthday = !empty($request->get('birthday')) ? date("Y-m-d", strtotime($request->get('birthday'))) : null;
            $newUser->birth_place = $request->get('birth_place');
            $newUser->email = $request->get('email');
            $newUser->username = $request->get('username');
            $newUser->gender = $request->get('gender');

            // save address data
            $newUser->lived_city = $request->get('lived_city');
            $newUser->lived_region = $request->get('lived_region');
            $newUser->lived_address = $request->get('lived_address');
            $newUser->current_city = $request->get('current_city');
            $newUser->current_region = $request->get('current_region');
            $newUser->current_address = $request->get('current_address');

            $newUser->user_type = "teacher";
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->internship = $request->get('internship');
            $newUser->msk_teacher_status_id = $request->get('msk_teacher_status_id');
            $newUser->msk_study_levels_id = $request->get('msk_study_levels_id');

            if ($request->get('password')) {
                $newUser->password = bcrypt($request->get('password'));
            }
            if ($request->has('restriction')) {
                $newUser->restrict_edit = 1;
            } else {
                $newUser->restrict_edit = null;
            }


            if ($request->hasFile('thumb')) {
                $newUser->clearPic();
                $newName = uniqid() . "." . $request->file('thumb')->extension();

                Image::make($request->file('thumb')->getRealPath())->resize(150, 150)->save(public_path(Standarts::$userThumbir . $newName));
                $request->file('thumb')->move(public_path(Standarts::$userImageDir), $newName);

                $newUser->thumb = $newName;
            }

            $newUser->save();

            //study
            $newUser->person_studies()->delete();
            if ($request->has('study'))
                foreach ($request->get('study')['place'] as $key => $value) {
                    $newStudy = new PersonStudy();
                    $newStudy->place = $value;
                    $newStudy->start_date = date("Y-m-d", strtotime($request->get('study')['start_date'][$key]));
                    $newStudy->end_date = date("Y-m-d", strtotime($request->get('study')['end_date'][$key]));
                    $newStudy->document = $request->get('study')['document'][$key];
                    $newStudy->document_number = $request->get('study')['document_number'][$key];
                    $newStudy->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_studies()->save($newStudy);
                }
            //language
            $newUser->person_languages()->delete();
            if ($request->has('language'))
                foreach ($request->get('language')['name'] as $key => $value) {
                    $newLanguage = new PersonLanguage();
                    $newLanguage->name = $value;
                    $newLanguage->mark_types_id = $request->get('language')['mark_type'][$key];
                    $newLanguage->msk_marks_id = $request->get('language')['mark_name'][$key];
                    $newLanguage->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_languages()->save($newLanguage);
                }
            //scientific_degree
            $newUser->person_scientific_degrees()->delete();
            if ($request->has('scientific_degree'))
                foreach ($request->get('scientific_degree')['degree'] as $key => $value) {
                    $newScientificDegree = new PersonScientificDegree();
                    $newScientificDegree->degree = $value;
                    $newScientificDegree->issued_by = $request->get('scientific_degree')['issued_by'][$key];
                    $newScientificDegree->issue_date = date("Y-m-d", strtotime($request->get('scientific_degree')['issue_date'][$key]));
                    $newScientificDegree->document_number = $request->get('scientific_degree')['document_number'][$key];
                    $newScientificDegree->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_scientific_degrees()->save($newScientificDegree);
                }

            //labor_activity
            $newUser->person_labot_activities()->delete();
            if ($request->has('labor_activity'))
                foreach ($request->get('labor_activity')['workplace'] as $key => $value) {
                    $newLaborActivity = new PersonLaborActivity();
                    $newLaborActivity->workplace = $value;
                    $newLaborActivity->position = $request->get('labor_activity')['position'][$key];
                    $newLaborActivity->start_date = date("Y-m-d", strtotime($request->get('labor_activity')['start_date'][$key]));
                    $newLaborActivity->end_date = date("Y-m-d", strtotime($request->get('labor_activity')['end_date'][$key]));
                    $newLaborActivity->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_labot_activities()->save($newLaborActivity);
                }
            //family
            $newUser->person_families()->delete();
            if ($request->has('family'))
                foreach ($request->get('family')['relation'] as $key => $value) {
                    $newFamily = new PersonFamily();
                    $newFamily->msk_relation_id = $value;
                    $newFamily->name = $request->get('family')['name'][$key];
                    $newFamily->birthday = date("Y-m-d", strtotime($request->get('family')['birthday'][$key]));
                    $newFamily->position = $request->get('family')['position'][$key];
                    $newFamily->address = $request->get('family')['address'][$key];
                    $newFamily->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_families()->save($newFamily);
                }
            //subject
            $newUser->teacher_subjects()->detach();
            if ($request->has('subject'))
                foreach ($request->get('subject')['subject_id'] as $key => $value) {
                    $newUser->teacher_subjects()->attach($value);
                }

            return response()->json(['status' => 'ok']);
        }
    }

    public function infoModal($teacher)
    {
        $data = [
            'teacher' => User::realData('teacher')->find($teacher),
            'id' => $teacher
        ];

        return view('modals.teachers.teachers_info', $data);
    }

    public function teacherDelete($teacher)
    {
        $teacher = User::realData('teacher')->withTrashed()->find($teacher);

        if ($teacher == null) return response()->view("errors.403", [], 403);

        if ($teacher->deleted_at != null) {
            $teacher->deleted_at = null;
            $teacher->save();
        } else {
            $teacher->delete();
        }
        return redirect()->back();
    }

    public function teacherPrivModal()
    {
        $teachersForSelect = [];
          $allRestrictedTeachers = User::realData('teacher')->where('restrict_edit',1)->get();

          foreach ($allRestrictedTeachers as $teacher){
              $teachersForSelect[] = ['id' => $teacher->id , 'text' => $teacher->fullname()];
          }

        return view('modals.teachers.teachers_restriction',compact('teachersForSelect'));
    }

    public function teacherPrivAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'teachers' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($request->has('teachers')) {
                $teacherArray = explode(",", $request->get('teachers')[0]);
                User::realData('teacher')->where('restrict_edit',1)->whereNotIn('id',$teacherArray)->update(['restrict_edit'=>null]);
                User::realData('teacher')->whereIn('id',$teacherArray)->update(['restrict_edit'=> !empty($request->get('restriction')) ? 1 : null]);
            }
        }
    }
}
