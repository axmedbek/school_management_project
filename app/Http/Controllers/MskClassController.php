<?php

namespace App\Http\Controllers;

use App\Library\YearDays;
use App\Models\MskClass;
use App\Models\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskClassController extends Controller
{
    public function index(Request $request)
    {
        $classes = MskClass::realData()->orderBy('order','ASC')->get();

        $data = [
            'classes' => $classes,
            'request' => $request
        ];

        return view('msk.classes.classes', $data);
    }

    public function classAddEditAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:60|unique:msk_classes,name,'.$request->get('id').',id,deleted_at,NULL',
            'id' => 'required|integer',
            'subjects' => 'array|required',
            'subjects.*' => 'required|integer|exists:subjects,id,tenant_id,'. Auth::user()->tenant_id,
        ],[
            'unique' => 'Bu sinif artıq daxil edilib'
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($request->get('id') == 0)
            {
                $newClass = new MskClass();
            }
            else
            {
                $newClass = MskClass::realData()->find($request->get('id'));
            }

            $newClass->name = $request->get('name');
            $newClass->tenant_id = Auth::user()->tenant_id;
            $newClass->order = 0;
            $newClass->save();

            $lastYear = Year::realData()->orderBy('id', 'desc')->first();
            if($lastYear) (new YearDays($lastYear))->calculateDays();

            //subjects
            $newClass->subjects()->detach();
            foreach ($request->get('subjects', []) as $subject) {
                $newClass->subjects()->attach($subject);
            }

            $buttons = "";
            $classArr = $newClass->toArray();

            $hasClassInLetter = \App\Models\ClassLetter::realData()
                ->where('msk_class_id',$classArr['id'])
                ->get();
            if(!isset($hasClassInLetter[0])){
                $buttons = "<a type=\"button\" class=\"btn btn-primary col-lg-4 col-md-12 edit_data\"><i class=\"fa fa-pencil\"></i></a>
                            <a type=\"button\" class=\"btn btn-danger col-lg-4 col-md-12 data_delete\"><i class=\"fa fa-trash-o\"></i></a>";
                $classArr["buttons"] = $buttons;
            }
            else{
                $buttons = "<a type=\"button\" class=\"btn btn-primary col-lg-4 col-md-12 edit_data\"><i class=\"fa fa-pencil\"></i></a>";
                $classArr["buttons"] = $buttons;

            }

            return response()->json(['status'=>'ok', "data"=> array_merge($classArr, ['subjects'=> implode(",", $newClass->subjects->pluck('name')->toArray()), 'subjects_id'=> $newClass->subjects->pluck('id')->toArray()] ) ]);
        }
    }

    public function classDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $room = MskClass::realData()->find($request->get('id'));
            $room->delete();

            return response()->json(['status'=>'ok']);
        }
    }

    public function classOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data' => 'required|array'
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $data = $request->get('data');

            $classes = MskClass::realData()->get();
            foreach ($classes as $class){
                $class->order = isset($data[$class->id])?$data[$class->id]:0;
                $class->save();
            }

            return response()->json(['status'=>'ok']);
        }
    }
}
