<?php

namespace App\Http\Controllers\Search;

use App\Library\YearDays;
use App\Models\ClassLetter;
use App\Models\LetterGroup;
use App\Models\MskCities;
use App\Models\MskClass;
use App\Models\MskMarks;
use App\Models\MskRegions;
use App\Models\Subject;
use App\Models\Year;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SearchForSelectController extends Controller
{
    public function index(Request $request){

        $results = [];

        if ($request->has('ne') && $request->get('ne') == 'getTeachers') {


            $teachers = User::realData('teacher')
                ->whereNull('restrict_edit')
                ->where(DB::raw("concat(name,' ',surname,' ',middle_name)"), 'like', '%' . $request->get('q') . '%');

            if ($request->filled('filter_date'))
            {
                $teachers->join('lesson_days','lesson_days.user_id','users.id')
                    ->leftJoin('lectures',function($join){
                        $join->on('lectures.date','=','lesson_days.date');
                        $join->on('lectures.teacher_id','=','lesson_days.user_id');
                    })
                    ->whereDate('lesson_days.date',Carbon::parse($request->get('filter_date'))->format('Y-m-d'))
                    ->whereNull('lectures.id')
                ->groupBy('users.id','users.name','users.surname','users.middle_name');

            }

            $teachers = $teachers->get(['users.id','users.name','users.surname','users.middle_name']);

            foreach ($teachers as $teacher) {
                $results[] = ['id' => $teacher['id'], 'text' => $teacher->fullname()];
            }
        }

        if($request->has('ne') && $request->get('ne') == "get_cities"){

            $cities = MskCities::realData()->get();

            foreach ($cities as $city){
                $results[] = ['id' => $city->id , 'text' => $city->name];
            }
        }

        if($request->has('ne') && $request->get('ne') == "get_regions"){

            $regions = MskRegions::realData()->where('msk_cities_id',$request->get('city_id'))->get();

            foreach ($regions as $region){
                $results[] = ['id' => $region->id , 'text' => $region->name];
            }
        }

        if($request->has('ne') && $request->get('ne') == "get_parents"){

            $parents = User::realData('parent')
                ->where(DB::raw("concat(name,' ',surname,' ',middle_name,' ')"), 'like', '%' . $request->get('q') . '%')
                ->get();
            foreach ($parents as $parent){
                $results[] = ['id' => $parent->id,'text' => $parent->fullname()."(Mob:".$parent['mobil_tel']." - Ev:".$parent['home_tel'].")"];
            }

        }

        if($request->has('ne') && $request->get('ne') == "get_letter_for_student_report"){
//            $validator = Validator::make($request->all(), [
//                'year_id' => 'required|integer',
//                'class_name' => 'required|array',
//            ]);
//            if ($validator->fails()){
//                $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();
//                return $errors;
//            }
          //  else {
               if (!empty($request->get('class_name'))){
                   $class_letters = ClassLetter::realData()
                       ->where('year_id', $request->get('year_id'))
                       ->whereIn('msk_class_id', $request->get('class_name'))
                       ->get();

                   foreach ($class_letters as $class_letter){
                       $results[] = ['id' => $class_letter->id,'text' => $class_letter->msk_class->name.$class_letter->name." (".$class_letter->corpus->name.")"];
                   }
               }
           // }

        }

        if($request->has('ne') && $request->get('ne') == "get_groups_for_student_report"){
          /*  $validator = Validator::make($request->all(), [
                'class_letter' => 'required|array',
            ]);
            if ($validator->fails()){
                $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();
                return $errors;
            }
            else{*/
            if (!empty($request->get('class_letter'))) {
                $groups = DB::table('letter_groups')
                    ->whereIn('group_type_id', $request->get('temayul'))
                    ->whereIn('class_letter_id', $request->get('class_letter'))
                    ->get();

                foreach ($groups as $group) {
                    $results[] = [
                        'id' => $group->id,
                        'text' =>
                            $group->name . "(" .
                            ClassLetter::find($group->class_letter_id)->first()
                                ->msk_class->name . "/" . ClassLetter::find($group
                                ->class_letter_id)->first()->name . ")"];
                }
            }
            //}
        }

        if($request->has('ne') && $request->get('ne') == "get_marks"){

          // dd($request->all());

            $marks = MskMarks::realData()->where('mark_types_id',$request->get('mark_type'))->get();

            foreach ($marks as $mark){
                $results[] = ['id' => $mark->id,'text' => $mark->name];
            }

        }

        if($request->has('ne') && $request->get('ne') == "get_classes"){

            // dd($request->all());
            $classes = MskClass::realData()->where('name','like','%'.$request->get('q').'%')->orderBy('order','ASC')->get();

          //  $marks = MskMarks::realData()->where('mark_types_id',$request->get('mark_type'))->get();

            foreach ($classes as $class){
                $results[] = ['id' => $class->id,'text' => $class->name];
            }

        }

        if($request->has('ne') && $request->get('ne') == "get_class_letters"){

            $classes = LetterGroup::realData()
                ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->join('corpuses', 'corpuses.id', 'class_letters.corpus_id')
                ->where('class_letters.year_id', YearDays::getCurrentYear()->id)
                ->where('msk_classes.id',$request->get('class_id'))
                ->where(DB::raw("concat(msk_classes.name,'',class_letters.name,'','(',corpuses.name,'',')')"),'like','%'.$request->get('q').'%')
                ->select('letter_groups.id','class_letters.id as letter_id','msk_classes.id as class_id', 'msk_classes.name as class_name', 'class_letters.name as letter', 'corpuses.name as corpus_name')
                ->orderBy('msk_classes.order', 'ASC')
                ->get();

            foreach ($classes as $class)
            {
                $results[] = ['id' => $class->id,'text'=> ($class->class_name . $class->letter . "(".$class->corpus_name.")")];
            }
        }

        if ($request->has('ne') && $request->get('ne') == 'getSubjects') {

            $subjects = Subject::realData()
                ->where('name', 'like', '%' . $request->get('q') . '%')
                ->get();
            foreach ($subjects as $subject) {
                $results[] = ['id' => $subject['id'], 'text' => $subject->name];
            }
        }

        if($request->has('ne') && $request->get('ne') == "get_classes_for_final_evaluation_report"){

            $year_id = $request->get('year_id');

            $classes = MskClass::realData()
                ->join('class_letters','class_letters.msk_class_id','msk_classes.id')
                ->where('class_letters.year_id',$year_id)
                ->select('msk_classes.id as id','msk_classes.name as name')
                ->groupBy('msk_classes.id','msk_classes.name','msk_classes.order')
                ->orderBy('msk_classes.order','ASC')
                ->get();

            foreach ($classes as $class){
                $results[] = ['id' => $class->id,'text' => $class->name];
            }

        }
        if($request->has('ne') && $request->get('ne') == "get_subjects_for_final_evaluation_report"){

            $year_id = $request->get('year_id');
            $class_id = $request->get('class_id');
            $group_id = $request->get('group_id');
            $q = $request->get('q');

            $subjects = Subject::realData()
                ->join('lessons','lessons.subject_id','subjects.id')
                ->join('letter_groups','letter_groups.id','lessons.letter_group_id')
                ->join('class_letters','class_letters.id','letter_groups.class_letter_id')
                ->join('msk_classes','msk_classes.id','class_letters.msk_class_id')
                ->whereIn('letter_groups.isMainGroup', explode(",", $group_id))
                ->where('class_letters.year_id',$year_id)
                ->where('subjects.name','like','%'.$q.'%')
                ->where('msk_classes.id',$class_id)->select('subjects.id as id','subjects.name as name')
                ->groupBy('subjects.id','subjects.name')
                ->get();

            foreach ($subjects as $subject){
                $results[] = ['id' => $subject->id,'text' => $subject->name];
            }

        }

        return response()->json(['results' => $results]);
    }
}
