<?php

namespace App\Http\Controllers;

use App\Library\LessonDaysFixer;
use App\Library\LessonState;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\ClassLetter;
use App\Models\ClassTime;
use App\Models\Corpus;
use App\Models\Group;
use App\Models\Lesson;
use App\Models\LetterGroup;
use App\Models\MskClass;
use App\Models\Room;
use App\Models\Season;
use App\Models\SeasonParagraph;
use App\Models\Subject;
use App\Models\Year;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TableController extends Controller
{

    public function index(Request $request)
    {
        if (!Storage::exists(YearDays::FILE_NAME)){
            foreach (Year::realData()->where('active', 1)->get() as $year)
                (new YearDays($year))->calculateDays();
        }

        $data = [
            'request' => $request
        ];

        return view('tables.tables', $data);
    }

    public function getClassLetters(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . Auth::user()->tenant_id,
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,' . Auth::user()->tenant_id,
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $letters = ClassLetter::realData()
                ->where('year_id', $request->get('year_id'))
                ->where('corpus_id', $request->get('corpus_id'))
                ->where('msk_class_id', $request->get('class_id'))
                ->get();

            $results = [];
            foreach ($letters as $letter)
                $results[] = ['id'=> $letter->id, 'text'=> $letter->name];

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function getLettersGroup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $letters = LetterGroup::realData()
                ->where('class_letter_id', $request->get('letter_id'))
                ->get();

            $results = [];
            foreach ($letters as $letter)
                $results[] = ['id'=> $letter->id, 'text'=> $letter->name . " (" . $letter->group_type->name . ")" ];

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function tablePage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $group = LetterGroup::realData()->find($request->get('group_id'));
            $classLetter = $group->class_letter;
            $year = $classLetter->year;

            //lessons
            $lessonData = [];
            $lessons = $group->lessons;
            if($lessons != null)
            foreach ($lessons as $lesson){
                if( !isset($lessonData[$lesson->week_day]) ) $lessonData[$lesson->week_day] = [];
                $lessonData[$lesson->week_day][$lesson->class_time_id] = $lesson;
            }

            $freeStudents = User::realData('student')
                ->leftJoin('letter_group_user', function ($join) use($year) {
                    $join->on('users.id', '=', 'letter_group_user.user_id')->on('letter_group_user.year_id', DB::raw($year->id));
                })
                ->whereNull('letter_group_user.user_id')
                ->get();

            return View::make('pages.tables.tables', ['group' => $group, 'classLetter' => $classLetter, 'freeStudents' => $freeStudents, 'lessonData'=> $lessonData, 'group_id'=> $request->get('group_id')])->render();
        }
    }

    public function studentAddModal(Request $request)
    {
        $data = [
            'group_id' => $request->get('group_id', 0),
            'id' => 0
        ];

        return view('modals.tables.student_add', $data);
    }

    public function classEditModal(Request $request)
    {
        $data = [
            'group_id' => $request->get('group_id', 0),
            'week_day' => $request->get('week_day', 0),
            'class_time_id' => $request->get('class_time_id', 0),
            'room_id' => $request->get('room_id', 0),
            'subject_id' => $request->get('subject_id', 0),
            'teacher_id' => $request->get('teacher_id', 0),
            'subjects' => $request->get('subjects', []),

            'subject' => Subject::realData()->find($request->get('subject_id', 0)),
            'room' => Room::realData()->find($request->get('room_id', 0)),
            'teacher' => User::realData('teacher')->find($request->get('teacher_id', 0)),
            'id' => 0
        ];

        return view('modals.tables.class_edit', $data);
    }

    public function getStudents(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id,
            'students' => 'array|nullable',
            'students.*' => 'required|integer|exists:users,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $group = LetterGroup::realData()->find($request->get('group_id'));
            $year = $group->class_letter->year;

            $students = User::realData('student')
                ->where(DB::raw("concat(name,' ',surname)"), 'like', '%'.$request->get('q','').'%')
                ->whereNotIn('id', $request->get('students', []))
                ->whereNotIn('id', function($q) use($year) { $q->select('user_id')->from('letter_group_user')->where('year_id', $year->id)->where('tenant_id', $year->tenant_id); })
                ->take(50)->get();

            $results = [];
            foreach ($students as $student)
                $results[] = ['id'=> $student->id, 'text'=> $student->fullname(), 'pic'=> asset(Standarts::$userThumbir . $student->thumb) ];

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function getGroupSubjects(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $group = LetterGroup::realData()->find($request->get('group_id'));
            $classLetter = $group->class_letter;
            $searchKey = $request->get('q');
            $weeks =  (new YearDays($classLetter->year))->getClassWeeks($classLetter->msk_class_id)['weeks'];
            $subject_used = json_decode($request->get('subjects', '[]'), true);

            $subjects = $group->class_letter->msk_class->subjects()
                ->where('name','like','%'.$searchKey.'%')
                        ->select('id','name', DB::raw("( SELECT SUM([hour]) FROM seasons
                            INNER JOIN group_type_season ON group_type_season.season_id = seasons.id AND group_type_season.group_type_id = {$group->group_type_id}
                            INNER JOIN season_paragraphs ON season_paragraphs.season_id = seasons.id WHERE seasons.subject_id = subjects.id AND seasons.msk_class_id='{$classLetter->msk_class_id}') as hours"))
                ->get();

            $resultsDeactive = [];
            $resultsActive = [];
            foreach ($subjects as $subject){
                $hours = $weeks == 0 ? 0 : (int)ceil($subject->hours / $weeks) - (int)(isset($subject_used[$subject->id])?$subject_used[$subject->id]:0);
                $disable = $hours <= 0 ? true : false;

                if($disable) $resultsDeactive[] = ['id'=> $subject->id, 'text'=> $subject->name . "(Qalan saatlar: ".$hours.")", 'name'=> $subject->name, 'hours' => $hours, 'disabled'=> $disable ];
                else $resultsActive[] = ['id'=> $subject->id, 'text'=> $subject->name . "(Qalan saatlar: ".$hours.")", 'name'=> $subject->name, 'hours' => $hours, 'disabled'=> $disable ];
            }

            return response()->json(['status'=>'ok', "results"=> array_merge($resultsActive, $resultsDeactive) ]);
        }
    }

    public function getGroupTeachers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::user()->tenant_id,
            'class_time_id' => 'required|integer|exists:class_times,id,tenant_id,' . Auth::user()->tenant_id,
            'week_day' => 'required|integer|in:1,2,3,4,5,6,7'
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $class_time_id = (int)$request->get('class_time_id');
            $week_day = (int)$request->get('week_day');
            $group_id = $request->get('group_id');
            $subject_id = $request->get('subject_id');

            $letter = LetterGroup::realData()->find($group_id)->class_letter;
            $class_time = ClassTime::realData()->find($class_time_id);
            $year_id = $letter->year_id;

            $teachers = User::realData('teacher')
                ->join('subject_user', function ($join) use($subject_id){ $join->on('subject_user.user_id', '=', 'users.id')->on('subject_user.subject_id', '=', DB::raw($subject_id)); })
                ->whereNotIn('subject_user.user_id', function ($q) use($year_id, $week_day, $class_time, $letter)
                {
                    return $q->select('user_id')->from('lessons')
                        ->leftJoin('class_times', 'class_times.id', '=', 'lessons.class_time_id')
                        ->leftJoin('letter_groups', 'letter_groups.id', '=', 'lessons.letter_group_id')
                        ->whereRaw("( (class_times.start_time <= '{$class_time->end_time}' AND class_times.end_time >= '{$class_time->start_time}') OR (class_times.start_time >= '{$class_time->end_time}' AND class_times.end_time <= '{$class_time->start_time}'))")
                        ->where('year_id', $year_id)->where('week_day', $week_day)
                        ->where('letter_groups.class_letter_id', '!=', $letter->id)
                        ->whereNull('lessons.deleted_at');
                })
                ->where(DB::raw("concat(users.name,' ',users.surname)"), 'like', '%'.$request->get('q','').'%')
                ->get();


            $results = [];
            foreach ($teachers as $teacher)
                $results[] = ['id'=> $teacher->id, 'text'=> $teacher->fullname() ];

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function getGroupRooms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::user()->tenant_id,
            'class_time_id' => 'required|integer|exists:class_times,id,tenant_id,' . Auth::user()->tenant_id,
            'teacher_id' => 'required|integer|exists:users,id,tenant_id,' . Auth::user()->tenant_id,
            'week_day' => 'required|integer|in:1,2,3,4,5,6,7'
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $class_time_id = (int)$request->get('class_time_id');
            $week_day = (int)$request->get('week_day');
            $group_id = $request->get('group_id');
            $subject_id = $request->get('subject_id');
            $teacher_id = $request->get('teacher_id');

            $group = LetterGroup::realData()->find($group_id);
            $class_time = ClassTime::realData()->find($class_time_id);
            $class_letter = $group->class_letter;
            $year_id = $class_letter->year_id;
            //$class_id = $class_letter->msk_class_id;

            $classMerge = Lesson::realData()
                ->leftJoin('class_times', 'class_times.id', '=', 'lessons.class_time_id')
                ->where("class_times.id", $class_time->id)
                ->where('year_id', $year_id)->where('week_day', $week_day)
                ->where('lessons.letter_group_id', '!=', DB::raw($group_id))
                ->where('lessons.user_id', $teacher_id)
                ->first();

            if($classMerge){
                $results[] = ['id'=> $classMerge->room_id, 'text'=> $classMerge->room->name ];
            }
            else{
                $rooms = Room::realData()
                    ->leftJoin('room_subject', 'room_subject.room_id', '=', 'rooms.id')
                    ->whereNotIn('rooms.id', function ($q) use($year_id, $week_day, $class_time, $group_id)
                    {
                        return $q->select('room_id')->from('lessons')
                            ->leftJoin('class_times', 'class_times.id', '=', 'lessons.class_time_id')
                            ->leftJoin('letter_groups', 'letter_groups.id', '=', 'lessons.letter_group_id')
                            ->whereRaw("( (class_times.start_time <= '{$class_time->end_time}' AND class_times.end_time >= '{$class_time->start_time}') OR (class_times.start_time >= '{$class_time->end_time}' AND class_times.end_time <= '{$class_time->start_time}'))")
                            ->where('lessons.letter_group_id', '!=', DB::raw($group_id))
                            ->where('year_id', $year_id)->where('week_day', $week_day)
                            ->whereNull('lessons.deleted_at');
                    })
                    ->where('rooms.corpus_id', $class_letter->corpus_id)
                    ->where('rooms.name','like','%'.$request->get('q').'%')
                    ->where(function ($f)use($subject_id){ $f->where('room_subject.subject_id', $subject_id)->orWhereNull('room_subject.subject_id'); })
                    ->get();

                $results = [];
                foreach ($rooms as $room)
                    $results[] = ['id'=> $room->id, 'text'=> $room->name ];
            }

            return response()->json(['status'=>'ok', "results"=> $results ]);
        }
    }

    public function tableSaveData(Request $request, LetterGroup $group)
    {
        $hasError = false;
        $validator = Validator::make($request->all(), [
            'students' => 'required|array',
            'students.*' => 'required|integer|exists:users,id,tenant_id,'. Auth::user()->tenant_id,
            //lessons
            'lessons' => 'required|array',
            'lessons.*' => 'required|array',
            'lessons.*.week_day' => 'required|integer|in:1,2,3,4,5,6,7',
            'lessons.*.class_time_id' => 'required|integer|exists:class_times,id,tenant_id,'. Auth::user()->tenant_id,
            'lessons.*.teacher_id' => 'nullable|integer|exists:users,id,tenant_id,'. Auth::user()->tenant_id,
            'lessons.*.subject_id' => 'nullable|integer|exists:subjects,id,tenant_id,'. Auth::user()->tenant_id,
            'lessons.*.room_id' => 'nullable|integer|exists:rooms,id,tenant_id,'. Auth::user()->tenant_id,
        ],
        [
            'students.required' => 'Şagirdlər seçilməyib!',
            'lessons.required' => 'Dərs saatları doldurulmayıb!',
            'lessons.*.required' => 'Dərs saatları doldurulmayıb!',
        ]
        );

        // find class letter
        $letter = $group->class_letter;

        // check year is active or not
        if($letter->year->active == 0){
            $validator->errors()->add('error','Seçilmiş il deaktiv olduğu üçün əməliyyat aparıla bilməz!');
            $hasError = true;
        }

        //check lessons
        $errors = [];
        $lessonState = new LessonState($group);

        foreach ($request->get('lessons', []) as $lesson){
            if( $lesson['teacher_id'] > 0 && $lesson['subject_id'] > 0 && $lesson['room_id'] > 0 ){
                $error = $lessonState->checkLesson($lesson['week_day'], $lesson['class_time_id'], $lesson['subject_id'], $lesson['teacher_id'], $lesson['room_id']);
                if($error){
                    $errors[] = $error;
                }
            }
        }
        if(count($errors) > 0) return response()->json(['status'=>'error', 'errors' => '', 'er_lessons'=> $errors]);

        if ($hasError || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            //lessons

            // delete lessons for this group
            $group->lessons()->delete();

//            dd("--");

            foreach ($request->get('lessons', []) as $lesson){
                if( $lesson['teacher_id'] > 0 && $lesson['subject_id'] > 0 && $lesson['room_id'] > 0 ){
                    $newLesson = new Lesson();
                    $newLesson->tenant_id = Auth::user()->tenant_id;
                    $newLesson->year_id = $letter->year_id;
                    $newLesson->week_day = $lesson['week_day'];
                    $newLesson->class_time_id = $lesson['class_time_id'];
                    $newLesson->user_id = $lesson['teacher_id'];
                    $newLesson->subject_id = $lesson['subject_id'];
                    $newLesson->room_id = $lesson['room_id'];
                    $newLesson->corpus_id = $letter->corpus_id;

                    // insert new lesson for this group
                    $group->lessons()->save($newLesson);
                }
            }


            //subjects
            $startDate = $request->has('startDate') ?
                Carbon::parse($request->get('startDate')) :
                Carbon::parse(date("Y-m-d"));

            // delete all students for this group
            $group->students()->detach();
            foreach ($request->get('students', []) as $student_id)
                // insert new students for this group
                $group->students()->attach($student_id, ['year_id'=> $letter->year_id, 'tenant_id'=> $letter->tenant_id]);

            //fix lesson days
            $lessonDayFixer = new LessonDaysFixer($letter->year_id);

            $lessonDayFixer->fixLessonDays($group->id,$startDate);

            return response()->json(['status'=>'ok' ]);
        }
    }

    public function createTable()
    {
        $groupId = 1;

        $group = LetterGroup::realData()->where('id', $groupId)->first();
        $weeks = $group->class_letter->year->weeks;

        $subjects = $group->class_letter->msk_class->subjects()
            ->select('id','name', DB::raw("( SELECT SUM([hour]) FROM seasons
                            INNER JOIN group_type_season ON group_type_season.season_id = seasons.id AND group_type_season.group_type_id = {$group->group_type_id}
                            INNER JOIN season_paragraphs ON season_paragraphs.season_id = seasons.id WHERE seasons.subject_id = subjects.id) as hours"))->get();

        $subjectsHours = [];
        foreach ($subjects as $subject){
            $hours = (int)ceil($subject->hours / $weeks);

            if($hours > 0){
                $subjectsHours[$subject->id] = [$hours, $subject];
            }
        }



        return $subjectsHours;
    }

    public function tableCopyModalAction(Request $request){

        $years = Year::realData()->orderByDesc('id')->get();
        $corpuses = Corpus::realData()->orderByDesc('id')->get();
        $classes = MskClass::realData()->orderBy('id')->get();


        return view('modals.tables.table_copy', ['request'=> $request, 'years' => $years, 'corpuses'=> $corpuses, 'classes'=> $classes]);
    }

    public function copyAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . Auth::user()->tenant_id,
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,' . Auth::user()->tenant_id,
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
            'letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::user()->tenant_id,
            'group_id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $group = LetterGroup::realData()->find($request->get('group_id'));

            //lessons
            $lessonData = [];
            $lessons = $group->lessons;
            if($lessons != null)
                foreach ($lessons as $lesson){
                    if( !isset($lessonData[$lesson->week_day]) ) $lessonData[$lesson->week_day] = [];
                    $lessonData[$lesson->week_day][$lesson->class_time_id] = $lesson;
                }

            $table = View::make('pages.tables.lesson_table', ['group' => $group, 'lessonData' => $lessonData ])->render();

            return response()->json(['table'=> $table, 'status' => 'ok']);
        }
    }

}
