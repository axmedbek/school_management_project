<?php

namespace App\Http\Controllers;

use App\Library\Standarts;
use App\Models\ClassTime;
use App\Models\Corpus;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ClassTimesController extends Controller
{
    public function index(Request $request){

        $corpuses = Corpus::realData()->get();
        $startTime = Option::realData()->where('option_name', 'class_first_time')->first();
        $startSound = Option::realData()->where('option_name', 'class_first_sound')->first();

        if($request->isMethod('post') && $request->has('start_time')){

            $validator = Validator::make($request->all(), [
                'file_sound' => 'max:8388608',
            ],[
                'file_sound' => 'Başlama səsi max 8mb olmalıdır'
            ]);

            if ($validator->fails()){
                $errors = View::make('modals.modal_errors',['errors' => $validator->errors() ])->render();

                return redirect()->back()->with("errors" , $errors);
            }
            else{

                if ($request->hasFile('file_sound')){
                    $soundFile = Option::realData()->where('option_name', 'class_first_sound')->first();
                    $fileName = uniqid() . "." . $request->file('file_sound')->getClientOriginalExtension();
                    $request->file('file_sound')->move(public_path(Standarts::$optionFilesDir), $fileName);
                    $deletedFileUrl = public_path(Standarts::$optionFilesDir) . $soundFile['data_file'];
                    if($soundFile['data_file'] != "" && file_exists($deletedFileUrl)){
                        @unlink($deletedFileUrl);
                    }

                    if($startTime){
                        $soundFile->data_file = $fileName;
                        $soundFile->save();
                    }
                    else{
                        $soundFile = new Option();
                        $soundFile->tenant_id = Auth::user()->tenant_id;
                        $soundFile->option_name = "class_first_sound";
                        $soundFile->data_file = $fileName;
                        $soundFile->save();
                    }
                }
                if($startTime){
                    $startTime->data_time = $request->get('start_time');
                    $startTime->save();
                }
                else{
                    $startTime = new Option();
                    $startTime->tenant_id = Auth::user()->tenant_id;
                    $startTime->option_name = "class_first_time";
                    $startTime->data_time = $request->get('start_time');
                    $startTime->save();
                }

                return redirect()->back();
            }

        }

        $data = [
            'corpuses' => $corpuses,
            'request' => $request,
            'startTime' => $startTime['data_time'],
            'startSound' => $startSound['data_file'],
        ];
        return view('msk.class_times.class_times',$data);
    }

    public function AddEditAction(Request $request){
        $validators = Validator::make($request->all(),[
            'start_time' => 'required|unique:class_times,start_time,'.$request->get('id').',id,corpus_id,'.$request->get('corpus_id'),
            'end_time' => 'required|unique:class_times,end_time,'.$request->get('id').',id,corpus_id,'.$request->get('corpus_id'),
            'voiceOne' => 'nullable|max:8388608',
            'voiceTwo' => 'nullable|max:8388608',
            'id' => 'required|integer',
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,' . Auth::user()->tenant_id
        ], [
            'max' => 'Səs fayilının ölçüsü çox böyükdür (Maks: 8mb)!',
        ]);

        if ($validators->fails()){
            $errors = View::make('modals.modal_errors',['errors' => $validators->errors() ])->render();

            return response()->json(['status' => 'error' , 'errors' => $errors]);
        }
        else{

            if ($request->get('id') == 0){
                $newClassTime = new ClassTime();
            }
            else{
                $newClassTime = ClassTime::realData()->find($request->get('id'));
            }

           if ($request->hasFile('voiceOne')) {

               $voiceOne = $request->file('voiceOne');
               $randomString = str_random(30);
               //audio1 full name
               $voiceOneName = $randomString . '.' . $voiceOne->getClientOriginalExtension();

               //path where audio files is stored
               $destinationPath = public_path(Standarts::$optionFilesDir);

               if ($request->get('id') != 0 && isset($newClassTime['start_sound']) && file_exists($destinationPath . $newClassTime['start_sound'])) {
                   @unlink(public_path(Standarts::$optionFilesDir) . $newClassTime['start_sound']);
               }

               $voiceOne->move($destinationPath, $voiceOneName);
               //audio files save to db
               $newClassTime->start_sound = $voiceOneName;
           }

           if($request->hasFile('voiceTwo')) {

               $voiceTwo = $request->file('voiceTwo');
               $randomString = str_random(30);
               //audio2 full name
               $voiceTwoName = $randomString . '.' . $voiceTwo->getClientOriginalExtension();

               //path where audio files is stored
               $destinationPath = public_path(Standarts::$optionFilesDir);

               if($request->get('id') != 0 && isset($newClassTime['end_sound']) && file_exists($destinationPath . $newClassTime['end_sound'])){
                   @unlink(public_path(Standarts::$optionFilesDir) . $newClassTime['end_sound']);
               }

               $voiceTwo->move($destinationPath, $voiceTwoName);

               //audio files save to db
               $newClassTime->end_sound = $voiceTwoName;
           }

            $newClassTime->start_time = $request->get('start_time');
            $newClassTime->end_time = $request->get('end_time');
            $newClassTime->corpus_id = $request->get('corpus_id');
            $newClassTime->tenant_id = Auth::user()->tenant_id;
            $newClassTime->save();

            $checkClassTime = \App\Models\Lesson::realData()->where('class_time_id',$newClassTime->id)->first();
            if(!isset($checkClassTime)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $newClassTime["buttons"] = $buttons;

            return response()->json(['status' => 'ok' , 'data' => $newClassTime->toArray()]);
        }
    }

    public function Delete(Request $request){

        $validators = Validator::make($request->all(),[
            'id' => 'required|integer|exists:class_times,id,tenant_id,'.Auth::user()->tenant_id
        ]);


        if ($validators->fails()){

            $errors = View::make('modals.modal_errors',['errors' => $validators->errors() ])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        }

        else{

            $classTime = ClassTime::realData()->find($request->get('id'));
            $destinationPath = public_path(Standarts::$optionFilesDir);

            //delete audio files from db
            @unlink($destinationPath.$classTime['start_sound']);
            //unlink($destinationPath.'/'.$classTime['end_sound']);

            $classTime->delete();

            return response()->json(['status' => 'ok']);
        }
    }
}
