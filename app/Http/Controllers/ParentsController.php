<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSearchRequest;
use App\Library\Standarts;
use App\Models\MskCities;
use App\Models\MskRegions;
use App\Models\PersonFamily;
use App\Models\PersonLaborActivity;
use App\Models\PersonLanguage;
use App\Models\PersonStudy;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class ParentsController extends Controller
{
    //

    public function parent(UserSearchRequest $request){
        $parents = User::realData('parent')->orderByDesc('id');

        if( $request->get('type', 'active') == 'deactive' ) $parents->onlyTrashed();
        $this->filter($parents,$request);
        $data = [
            'parents' => $parents->paginate(Standarts::$rowCount),
            'request' => $request
        ];

        return view('parents.parents',$data);
    }

    public function filter($object,$request)
    {
        if($request->get('name')) $object->where(DB::raw("concat(name,' ',surname)"), 'like', '%'.$request->get('name').'%');
        if($request->get('birthday')) $object->where('birthday', date("Y-m-d", strtotime($request->get('birthday'))) );
        if($request->get('enroll_date')) $object->where('enroll_date', date("Y-m-d", strtotime($request->get('enroll_date'))) );
        if($request->get('home_tel')) $object->where('home_tel', 'like', '%'.$request->get('home_tel').'%');
        if($request->get('mobil_tel')) $object->where('mobil_tel', 'like', '%'.$request->get('mobil_tel').'%');
        if($request->get('email')) $object->where('email', 'like', '%'.$request->get('email').'%');
    }


    public function parent_AddEdit_Modal($parent){

        $parentData = User::realData('parent')->find($parent);
        $currentCity = MskCities::realData()->find((int)$parentData['current_city']);
        $currentRegion = MskRegions::realData()->find((int)$parentData['current_region']);

        $addressData = [];

        if($currentCity){
            $addressData['current_city'] = ['id' =>$currentCity['id'] , 'text' => $currentCity['name'] ];
        }

        if($currentRegion){
            $addressData['current_region'] = ['id' =>$currentRegion['id'] , 'text' => $currentRegion['name'] ];
        }

        $data = [
            'parent' => $parentData,
            'addressData' => $addressData,
            'id' => $parent

        ];

        return view('modals.parents.parents_add_edit',$data);

    }



    public function parent_Add_Edit_Action(Request $request, $parent)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'surname' => 'nullable|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'username' => 'nullable|string|max:30|unique:users,username,'.$parent,
            'password' => 'nullable|string|max:20',
            'mobil_tel' => 'nullable|string|max:20',
            'home_tel' => 'nullable|string|max:20',
            'enroll_date' => 'nullable|date_format:d-m-Y',
            'birthday' => 'nullable|date_format:d-m-Y',
            'birth_place' => 'nullable|string',
            'email' => 'nullable|email|unique:users,email,'.$parent,
            'gender' => 'required|string|in:m,f',
            'current_city' => 'nullable|integer',
            'current_region' => 'nullable|integer',
            'current_address' => 'nullable|string|max:255',
            'position' => 'nullable|string|max:60',
            'thumb'    => 'nullable|image|max:3000',

        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($parent == 0)
            {
                $newUser = new User();
            }
            else
            {
                $newUser = User::realData()->find($parent);
            }

            $newUser->name = $request->get('name');
            $newUser->surname = $request->get('surname');
            $newUser->middle_name = $request->get('middle_name');
            $newUser->mobil_tel = $request->get('mobil_tel');
            $newUser->home_tel = $request->get('home_tel');
           // $newUser->enroll_date = !empty($request->get('enroll_date')) ? date("Y-m-d", strtotime($request->get('enroll_date'))) : null;
            $newUser->enroll_date = Carbon::today();
            $newUser->birthday =  !empty($request->get('birthday')) ? date("Y-m-d", strtotime($request->get('birthday'))) : null;
            $newUser->birth_place = $request->get('birth_place');
            $newUser->email = $request->get('email');
            $newUser->username = $request->get('username');
            $newUser->gender = $request->get('gender');
            $newUser->user_type = "parent";
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->current_city = $request->get('current_city');
            $newUser->current_region = $request->get('current_region');
            $newUser->current_address = $request->get('current_address');
            $newUser->position = $request->get('position');
            if($request->has('password')) $newUser->password = bcrypt($request->get('password'));

            if($request->hasFile('thumb'))
            {
                $newUser->clearPic();
                $newName = uniqid() . "." . $request->file('thumb')->extension();

                Image::make($request->file('thumb')->getRealPath())->resize(150, 150)->save( public_path(Standarts::$userThumbir . $newName) );
                $request->file('thumb')->move(public_path(Standarts::$userImageDir), $newName);

                $newUser->thumb = $newName;
            }

            $newUser->save();

            return response()->json(['status'=>'ok']);
        }
    }


    public function parent_info_modal_Action($parent){

        $data = [
            'parent' => User::realData('parent')->find($parent),
            'id' => $parent
        ];

        return view('modals.parents.parents_info',$data);
    }


    public function parent_Delete_Action($parent){
        $parent = User::realData('parent')->withTrashed()->find($parent);

        if($parent==null)return response()->view('errors.403',[],403);

        if ($parent->deleted_at != null){
            $parent->deleted_at = null;
            $parent->save();
        }
        else $parent->delete();

        return redirect()->back();


    }

}
