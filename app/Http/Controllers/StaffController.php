<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSearchRequest;
use App\Library\Standarts;
use App\Models\MskCities;
use App\Models\MskRegions;
use App\Models\PersonFamily;
use App\Models\PersonLaborActivity;
use App\Models\PersonLanguage;
use App\Models\PersonStudy;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class StaffController extends Controller
{
    public function staff(UserSearchRequest $request)
    {
        $staffs = User::realData('staff')->orderByDesc('id');

        if ($request->get('type', 'active') == 'deactive') $staffs->onlyTrashed();


        $this->filter($staffs, $request);

        $data = [
            'staffs' => $staffs->paginate(Standarts::$rowCount),
            'request' => $request
        ];

        return view('staffs.staffs', $data);
    }

    public function filter($object, $request)
    {
        if ($request->get('name')) $object->where(DB::raw("concat(name,' ',surname)"), 'like', '%' . $request->get('name') . '%');
        if ($request->get('birthday')) $object->where('birthday', date("Y-m-d", strtotime($request->get('birthday'))));
        if ($request->get('enroll_date')) $object->where('enroll_date', date("Y-m-d", strtotime($request->get('enroll_date'))));
        if ($request->get('home_tel')) $object->where('home_tel', 'like', '%' . $request->get('home_tel') . '%');
        if ($request->get('mobil_tel')) $object->where('mobil_tel', 'like', '%' . $request->get('mobil_tel') . '%');
        if ($request->get('email')) $object->where('email', 'like', '%' . $request->get('email') . '%');
    }

    public function staffAddEditModal($staff)
    {
        $staffData = User::realData('staff')->find($staff);
        $personFamily = PersonFamily::where('user_id', $staff)->get();

        //get address data
        $livedCity = MskCities::realData()->find((int)$staffData['lived_city']);
        $currentCity = MskCities::realData()->find((int)$staffData['current_city']);
        $livedRegion = MskRegions::realData()->find((int)$staffData['lived_region']);
        $currentRegion = MskRegions::realData()->find((int)$staffData['current_region']);

        $addressData = [];

        if($livedCity){
            $addressData['lived_city'] = ['id' =>$livedCity['id'] , 'text' => $livedCity['name']];
        }

        if($currentCity){
            $addressData['current_city'] = ['id' =>$currentCity['id'] , 'text' => $currentCity['name'] ];
        }

        if($livedRegion){
            $addressData['lived_region'] = ['id' =>$livedRegion['id'] , 'text' => $livedRegion['name']];
        }

        if($currentRegion){
            $addressData['current_region'] = ['id' =>$currentRegion['id'] , 'text' => $currentRegion['name'] ];
        }

        $dataParent = [];
        foreach ($personFamily as $family) {
            $user = User::realData('parent')->find($family['parent_id']);
            $dataParent[] = ['id' => $family['parent_id'], 'text' => $user['name']."(Mob:".$user['mobil_tel']." - Ev:".$user['home_tel'].")", 'relation' => $family['msk_relation_id']];
        }

        $data = [
            'staff' => $staffData,
            'personFamily' => $dataParent,
            'addressData' => $addressData,
            'id' => $staff
        ];

        return view('modals.staffs.staffs_add_edit', $data);
    }

    public function staffAddEditAction(Request $request, $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'surname' => 'nullable|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'mobil_tel' => 'nullable|string|max:20',
            'home_tel' => 'nullable|string|max:20',
            'enroll_date' => 'nullable|date_format:d-m-Y',
            'birthday' => 'nullable|date_format:d-m-Y',
            'birth_place' => 'nullable|string',
            'personal_heyeti' => 'required|integer|exists:msk_heyetlers,id',
            'email' => 'nullable|email|unique:users,email,' . $user,
            'gender' => 'required|string|in:m,f',
            'city' => 'nullable|string|max:20',
            'address' => 'nullable|string|max:255',
            'current_address' => 'nullable|string|max:255',
            'position' => 'nullable|string|max:60',
            'thumb' => 'nullable|image|max:3000',
            //study
            'study' => 'nullable|array',
            'study.place' => 'array',
            'study.place.*' => 'required|string',
            'study.start_date' => 'array',
            'study.start_date.*' => 'required|date_format:"d-m-Y"',
            'study.end_date' => 'array',
            'study.end_date.*' => 'required|date_format:"d-m-Y"',
            'study.document' => 'array',
            'study.document.*' => 'nullable|string|max:60',
            'study.document_number' => 'array',
            'study.document_number.*' => 'nullable|string|max:60',
            //language
            'language' => 'nullable|array',
            'language.name' => 'array',
            'language.name.*' => 'required|string',
            'study.mark' => 'array',
            'study.mark.*' => 'required|integer|exists:mark_types,id,tenant_id,' . Auth::user()->tenant_id,
            //labor_activity
            'labor_activity' => 'nullable|array',
            'labor_activity.workplace' => 'array',
            'labor_activity.workplace.*' => 'required|string|max:60',
            'labor_activity.start_date' => 'array',
            'labor_activity.start_date.*' => 'required|date_format:"d-m-Y"',
            'labor_activity.end_date' => 'array',
            'labor_activity.end_date.*' => 'required|date_format:"d-m-Y"',
            'labor_activity.position' => 'array',
            'labor_activity.position.*' => 'nullable|string|max:60',
            //family
            'family' => 'nullable|array',
            'family.relation' => 'array',
            'family.parents' => 'array',
        ]);

        if ($validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($user == 0) {
                $newUser = new User();
            } else {
                $newUser = User::realData('staff')->find($user);
            }

            $newUser->name = $request->get('name');
            $newUser->surname = $request->get('surname');
            $newUser->middle_name = $request->get('middle_name');
            $newUser->mobil_tel = $request->get('mobil_tel');
            $newUser->home_tel = $request->get('home_tel');
            //$newUser->enroll_date = !empty($request->get('enroll_date')) ? date("Y-m-d", strtotime($request->get('enroll_date'))) : null;
            $newUser->enroll_date = Carbon::today();
            $newUser->birthday = !empty($request->get('birthday')) ? date("Y-m-d", strtotime($request->get('birthday'))) : null;
            $newUser->birth_place = $request->get('birth_place');
            $newUser->email = $request->get('email');
            $newUser->username = $request->get('username');
            $newUser->gender = $request->get('gender');

            // save address data
            $newUser->lived_city = $request->get('lived_city');
            $newUser->lived_region = $request->get('lived_region');
            $newUser->lived_address = $request->get('lived_address');
            $newUser->current_city = $request->get('current_city');
            $newUser->current_region = $request->get('current_region');
            $newUser->current_address = $request->get('current_address');

            $newUser->personal_heyeti = $request->get('personal_heyeti');
            $newUser->user_type = "staff";
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->position = $request->get('position');
            if ($request->has('password')) $newUser->password = bcrypt($request->get('password'));

            if ($request->hasFile('thumb')) {
                $newUser->clearPic();
                $newName = uniqid() . "." . $request->file('thumb')->extension();

                Image::make($request->file('thumb')->getRealPath())->resize(150, 150)->save(public_path(Standarts::$userThumbir . $newName));
                $request->file('thumb')->move(public_path(Standarts::$userImageDir), $newName);

                $newUser->thumb = $newName;
            }

            $newUser->save();

            //study
            $newUser->person_studies()->delete();
            if ($request->has('study'))
                foreach ($request->get('study')['place'] as $key => $value) {
                    $newStudy = new PersonStudy();
                    $newStudy->place = $value;
                    $newStudy->start_date = date("Y-m-d", strtotime($request->get('study')['start_date'][$key]));
                    $newStudy->end_date = date("Y-m-d", strtotime($request->get('study')['end_date'][$key]));
                    $newStudy->document = $request->get('study')['document'][$key];
                    $newStudy->document_number = $request->get('study')['document_number'][$key];
                    $newStudy->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_studies()->save($newStudy);
                }
            //language
            $newUser->person_languages()->delete();
            if ($request->has('language'))
                foreach ($request->get('language')['name'] as $key => $value) {
                    $newLanguage = new PersonLanguage();
                    $newLanguage->name = $value;
                    $newLanguage->mark_type_id = $request->get('language')['mark'][$key];
                    $newLanguage->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_languages()->save($newLanguage);
                }
            //labor_activity
            $newUser->person_labot_activities()->delete();
            if ($request->has('labor_activity'))
                foreach ($request->get('labor_activity')['workplace'] as $key => $value) {
                    $newLaborActivity = new PersonLaborActivity();
                    $newLaborActivity->workplace = $value;
                    $newLaborActivity->position = $request->get('labor_activity')['position'][$key];
                    $newLaborActivity->start_date = date("Y-m-d", strtotime($request->get('labor_activity')['start_date'][$key]));
                    $newLaborActivity->end_date = date("Y-m-d", strtotime($request->get('labor_activity')['end_date'][$key]));
                    $newLaborActivity->tenant_id = Auth::user()->tenant_id;

                    $newUser->person_labot_activities()->save($newLaborActivity);
                }
            //family
            if ($request->has('family')){
                $personFamily = PersonFamily::where('user_id', $newUser['id']);
                if ($personFamily != null) $personFamily->delete();
                foreach ($request->get('family')['relation'] as $key => $value) {
                    $newFamily = new PersonFamily();
                    $newFamily->msk_relation_id = $request->get('family')['relation'][$key];
                    $newFamily->user_id = $newUser->id;
                    $newFamily->parent_id = $request->get('family')['parents'][$key];
                    $newFamily->tenant_id = Auth::user()->tenant_id;
                    $newFamily->save();
                }
            }

            return response()->json(['status' => 'ok']);
        }
    }

    public function infoModal($staff)
    {
        $data = [
            'staff' => User::realData('staff')->find($staff),
            'id' => $staff
        ];

        return view('modals.staffs.staffs_info', $data);
    }

    public function staffDelete($staff)
    {
        $staff = User::realData('staff')->withTrashed()->find($staff);

        if ($staff == null) return response()->view("errors.403", [], 403);

        if ($staff->deleted_at != null) {
            $staff->deleted_at = null;
            $staff->save();
        } else {
            $staff->delete();
        }


        return redirect()->back();
    }
}
