<?php

namespace App\Http\Controllers\Portal;
use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\SmsNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index(){
        $numbers = SmsNumber::where('user_id',Helper::getSelectedStudentForParent())->orderByDesc('id')->get();
        $data = [
            'numbers' => $numbers
        ];
        return view('portal/profile/profile',$data);
    }

    public function changePasswordModal(){
        return view('portal.modals.profile.change_password');
    }

    public function changePasswordProcess(Request $request){
        //this variable for custom errors
        $hasError = false;

        $validator = Validator::make($request->all(),[
            'new_password' => 'min:6|confirmed'
        ],[
            'confirmed' => 'Təkrar daxil etdiyiniz şifrə yanlışdır'
        ]);

        //check current_password is equal to inserted password
        if (!Hash::check($request->get('current_password'),auth()->user()->password)){
            $hasError = true;
            $validator->errors()->add('current_password','Parolunuz yanlışdır');
        }

        if ($hasError || $validator->fails()){
            return response()->json(['status' => 'error' , 'errors' => $validator->errors()]);
        }
        else{
            $user = auth()->user();
            $user->password = bcrypt($request->get('new_password'));
            $user->save();
            return response()->json(['status' => 'success']);
        }
    }

    public function smsNumberAddEditAction(){
        $validator = validator(request()->all(),[
            'id' => 'required|integer',
            'number' => 'required'
        ]);

        if ($validator->fails()){
            $errors = view('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return ['status' => 'error', 'errors' => $errors];
        }
        else{
            if (request()->get('id') == 0){
                $smsNumber = new SmsNumber();
            }
            else{
                $smsNumber = SmsNumber::find(request()->get('id'));
            }

            $smsNumber->number = request('number');
            $smsNumber->user_id = Helper::getSelectedStudentForParent();
            $smsNumber->tenant_id = auth()->guard('portal')->user()->tenant_id;
            $smsNumber->save();
            return response()->json(['status' => 'ok' , 'data' => $smsNumber->toArray()]);
        }
    }

    public function smsNumberDeleteAction(){
        $validator = validator(request()->all(),[
            'id' => 'required|integer|exists:sms_numbers,id,tenant_id,'.auth()->guard('portal')->user()->tenant_id
        ]);

        if ($validator->fails()){
            $errors = view('modals.modal_errors', ['errors' => $validator->errors() ])->render();
            return ['status' => 'error', 'errors' => $errors];
        }
        else {
            $smsNumber = SmsNumber::find(request('id'));
            if ($smsNumber == null){

            }
            else{
                $smsNumber->delete();
                return response()->json(['status' => 'ok']);
            }
        }
    }
}
