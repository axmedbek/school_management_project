<?php

namespace App\Http\Controllers\Portal;

use App\Models\Folder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class GaleryController extends Controller
{
    public function index()
    {
        return view('portal.galery.galery');
    }

    public function getFolderList(Request $request)
    {
        $user = Auth::guard('portal')->user();

        $folders = Folder::realData()
            ->whereIn('folders.id', function ($select) use ($user) {
                $select->select('folder_id')->from('folder_users')->where('folder_users.user_id', $user->id);
            });

        if ($request->has('s')){
            $folders = $folders->where('name','like','%'.$request->get('s').'%');
        }

        $folders = $folders->get();

        $countImagesWithPath = [];
        foreach ($folders as $folder) {
            $imageCountExistsInPath = 0;
            foreach ($folder->images as $image) {
                if (file_exists(public_path('option_files/' . $image['image']))) {
                    ++$imageCountExistsInPath;
                }
            }
            $countImagesWithPath[$folder['name']] = $imageCountExistsInPath;
        }

        $data = [
            'folders' => $folders,
            'imageCount' => $countImagesWithPath
        ];
        return view('portal.pages.galery.folder_list', $data);
    }

    public function getFolder(Request $request)
    {
        $folder = Folder::realData()->find($request->get('folder_id'));

        $data = [
            'folder' => $folder,
            'images' => [],
        ];

        return view('portal.pages.galery.folder', $data);
    }

    public function image169(Request $request)
    {
        $url = urldecode($request->get('url'));

        $img = Image::make($url);
        $img->resize(50, 50);

        return $img->response();
    }
}
