<?php

namespace App\Http\Controllers\Portal;

use App\Library\Helper;
use App\Library\YearDays;
use App\Models\LessonDay;
use App\Models\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FinalEvaluationController extends Controller
{
    public function index()
    {
        return view("portal.reports.final_evaluation.final_evaluation");
    }

    public function getTable()
    {
        $year_id = request('year_id');
        $student_id = Helper::getSelectedStudentForParent();

        $evals = LessonDay::realData()
            ->where('year_id', $year_id)
            ->whereIn('type', ['ksq', 'bsq', 'yi', 'y'])->get();

        $subjectQuery = Subject::realData()
            ->join('lessons', 'lessons.subject_id', 'subjects.id')
            ->join('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
            ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
            ->where('letter_group_user.user_id',$student_id)
            ->whereNull('lessons.deleted_at')
            ->where('lessons.year_id', $year_id);

        $subjects = $subjectQuery
            ->select('subjects.id as id', 'subjects.name as name')
            ->distinct()->get();

        $evalValues = $subjectQuery
            ->leftJoin(DB::raw('(SELECT * from [marks] where [marks].[student_id] ='.$student_id.') as dd'), 'dd.subject_id', 'subjects.id')
            ->leftJoin('msk_marks', 'msk_marks.id', 'dd.msk_mark_id')
            ->select('dd.date as date','subjects.id as subject_id', 'dd.type as lesson_type', 'dd.value as mark_value', 'msk_marks.name as mark_name')
            ->get();


        $subjectEvals = [];
        foreach ($subjects as $subject) {
            foreach ($evals as $key => $eval) {
                foreach ($evalValues as $evalValue){
                    $insertedDate = Carbon::parse($evalValue['date']);
                    $date = Carbon::parse($eval['date']);
                    if ($date->eq($insertedDate) && $subject['id'] == $evalValue['subject_id']){
                        if ($eval->type == "yi") {
                            $subjectEvals[$subject->id][$eval->id] = $evalValue['mark_value'] ;
                        } elseif ($eval->type == "y") {
                            $subjectEvals[$subject->id][$eval->id] = $evalValue['mark_value'];
                        } else {
                            if (Carbon::parse($evals[$key + 1]->date)->format('F') == Carbon::parse($eval->date)->format('F') && $eval->type == "bsq") {
                                $subjectEvals[$subject->id][$eval->id] = $evalValue['mark_name'];
                            } else {
                                $subjectEvals[$subject->id][$eval->id] = $evalValue['mark_name'];
                            }
                        }
                        break;
                    }
                    else{
                        $subjectEvals[$subject->id][$eval->id] = "";
                    }
                }
            }
        }

        $data = [
            'evals' => $evals,
            'subjects' => $subjects,
            'evalValues' => $evalValues,
            'subjectEvals' => $subjectEvals
        ];

        return view('portal.reports.tables.final_evaluation.final_evluation_table', $data);
    }
}
