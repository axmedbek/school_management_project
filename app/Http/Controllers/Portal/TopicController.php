<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\GroupType;
use App\Models\Lesson;
use App\Models\MskClass;
use App\Models\Season;
use App\Models\Subject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacger = Auth::guard('portal')->user();

        $groupTypes = GroupType::realData()->get();

        $teacherClasses = Lesson::realData()
            ->join('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
            ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->where('lessons.user_id', $teacger->id)
            ->orderBy('msk_classes.order')
            ->distinct()->get(['msk_classes.id','msk_classes.name','msk_classes.order']);

        $classGroup = [];
        foreach ($teacherClasses as $teacherClass){
            $classGroup[$teacherClass->id] = Subject::realData()
                ->join('msk_class_subject', function ($join) use($teacherClass){
                    $join->on('msk_class_subject.subject_id', 'subjects.id')
                        ->on('msk_class_subject.msk_class_id', DB::raw($teacherClass->id));
                })
                ->join('subject_user', function ($join) use($teacger){
                    $join->on('subject_user.subject_id', 'subjects.id')
                        ->on('subject_user.user_id', DB::raw($teacger->id));
                })
                ->get();
        }

        $seasonGroup = [];
        foreach ($teacherClasses as $teacherClass){
            $seasonGroup[$teacherClass->id] = [];
            foreach ($classGroup[$teacherClass->id] as $subject){
                $seasonGroup[$teacherClass->id][$subject->id] = [];
                foreach ($groupTypes as $groupType){
                    $seasonGroup[$teacherClass->id][$subject->id][$groupType->id] = Season::realData()
                        ->join('group_type_season', 'group_type_season.season_id', 'seasons.id')
                        ->where('group_type_season.group_type_id', $groupType->id)
                        ->where('seasons.subject_id', $subject->id)
                        ->where('seasons.msk_class_id', $teacherClass->id)
                        ->get();
                }
            }
        }

        $data = [
            'teacherClasses' => $teacherClasses,
            'classGroup' => $classGroup,
            'seasonGroup' => $seasonGroup,
            'groupTypes' => $groupTypes,
        ];

        return view('portal.topic.topic', $data);
    }
}
