<?php

namespace App\Http\Controllers\Portal;

use App\Library\Helper;
use App\Library\YearDays;
use App\Models\Homework;
use App\Models\Lecture;
use App\Models\LetterGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LessonMaterialController extends Controller
{
    public function index()
    {
        $user = auth()->guard('portal')->user();
        $currentYear = YearDays::getCurrentYear();
        $activeTab = request()->has('active_tab') ? request()->get('active_tab') : 'tab_classes';
        $selectedStudentId = Helper::getSelectedStudentForParent();

        if($user->user_type == 'parent'){
            $letter_group_id = DB::table('letter_group_user')
                ->where('year_id', $currentYear->id)
                ->where('user_id', $selectedStudentId)
                ->value('letter_group_id');
        }
        else{
            $letter_group_id = DB::table('letter_group_user')
                ->where('year_id', $currentYear->id)
                ->where('user_id', $user->id)
                ->value('letter_group_id');
        }

        $letterGroup = LetterGroup::realData()->find($letter_group_id);

        $lectures = [];
        if ($letterGroup['class_letter']) {
            $lectures = Lecture::realData()
                ->whereBetween("date", [$currentYear['start_date'], $currentYear['end_date']])
                ->where('class_letter_id',$letterGroup['class_letter']->id)
                ->paginate(6,['*'],'lecture_page');
        }
        $homeworks = [];
        if ($letterGroup['class_letter']) {
            $homeworks = Homework::realData()
                ->whereBetween("date", [$currentYear['start_date'], $currentYear['end_date']])
                ->where('class_letter_id', $letterGroup['class_letter']->id)
                ->paginate(6,['*'],'homework_page');
        }

        $data = [
            'lectures' => $lectures,
            'homeworks' => $homeworks,
            'activeTab' => $activeTab
        ];

        return view('portal.lesson_materials.lesson_materials', $data);
    }
}
