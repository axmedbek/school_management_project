<?php

namespace App\Http\Controllers\Portal;

use App\Models\Sms;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherRequestForParent extends Controller
{
    public function index()
    {
        $student_id = session('selected_student_for_parent');
        $smses = Sms::realData()->where('student_id', $student_id)->paginate(6);
        $smsObj = Sms::realData()->where('id',request('sms_id'))->where('student_id',$student_id)->first();

        $data = [
            'smses' => $smses,
            'sms_id' => $smsObj ? $smsObj['id'] : 0
        ];
        return view('portal.teacher_request.teacher_request', $data);
    }

    public function infoModal()
    {

        $student_id = session('selected_student_for_parent');
        $sms_id = request()->get('sms_id');

        $sms = Sms::realData()
            ->where('student_id', $student_id)
            ->where('id', $sms_id)
            ->first();

        $data = [
            'modalId' => request('modalId', 1),
            'sms' => $sms,
            'is_null' => 1
        ];

        if ($sms != null){
            $sms->read = 1;
            $sms->save();
            $data['is_null'] = 0;
        }

        return view('portal.modals.teacher_reaquest.teacher_request_info', $data);

    }
}
