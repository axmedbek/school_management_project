<?php

namespace App\Http\Controllers\Portal;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Library\LessonDaysFixer;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\ClassLetter;
use App\Models\ClassTime;
use App\Models\Duty;
use App\Models\Homework;
use App\Models\HomeworkFile;
use App\Models\Lecture;
use App\Models\LectureFile;
use App\Models\Lesson;
use App\Models\LessonDay;
use App\Models\LetterGroup;
use App\Models\Mark;
use App\Models\MarkType;
use App\Models\MskMarks;
use App\Models\Notification;
use App\Models\PersonFamily;
use App\Models\Sms;
use App\Models\SmsFiles;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class JurnalController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            //'teacherId' => Auth::guard('portal')->user()->id,
        ];

        return view('portal.jurnal.jurnal', $data);
    }

    public function studentsPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $teacher = Auth::guard('portal')->user();
            $classLetterId = $request->get('class_id');
            $letterGroupId = $request->get('letter_group_id');
            $year = $request->get('year', 0);
            $month = str_pad($request->get('month', 0), 2, '0', STR_PAD_LEFT);
            $subjectId = $request->get('subject_id', 0);
            $selectedMonth = Carbon::create($year, $month, 1);
            $offest = $month + $year * 12;
            $curretYear = YearDays::getCurrentYear($month, $year);
            $calcYI = false;
            $calcY = false;

            $classAllCounts = 0;

            $students = LetterGroup::realData()
                ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
                ->leftJoin('users', 'users.id', 'letter_group_user.user_id')
                ->orderBy(DB::raw('CONCAT(users.name,users.surname,users.middle_name)'))
                ->select(DB::raw("CONCAT(users.name,' ',users.surname,' ',users.middle_name) as f_name"), 'users.id as user_id')
                ->where('letter_groups.class_letter_id', $classLetterId)->get();

            $userMarksGroup = [];
            $studentMarks = Mark::realData()->where('subject_id', $subjectId)
                ->where('class_letter_id', $classLetterId)
                ->whereRaw("YEAR(date)*12+MONTH(date)='$offest'")
                ->select('*', DB::raw('DAY(date) as day'))
                ->get();
            foreach($studentMarks as $studentMark)
            {
                $markDayType = $studentMark->type == 'day' ? $studentMark->class_time_id : $studentMark->type;

                if(!isset($userMarksGroup[$studentMark->student_id])) $userMarksGroup[$studentMark->student_id] = [];
                if(!isset($userMarksGroup[$studentMark->student_id][$studentMark->day])) $userMarksGroup[$studentMark->student_id][$studentMark->day] = [];
                $userMarksGroup[$studentMark->student_id][$studentMark->day][$markDayType] = $studentMark;
            }

            $classDays = LessonDay::realData()
                ->leftJoin('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                ->whereRaw("lesson_days.date >= '{$selectedMonth->format("Y-m-d")}' AND lesson_days.date <= '{$selectedMonth->lastOfMonth()->format("Y-m-d")}'")
                ->where(function ($where) use($letterGroupId, $subjectId)
                {
                    $where->where(function ($where) use ($letterGroupId, $subjectId)
                    {
                        $where->where('letter_groups.id', $letterGroupId)
                            ->where('lessons.subject_id', $subjectId);
                    })->orWhereIn('lesson_days.type', ['ksq', 'bsq', 'yi', 'y']);
                })
                ->where(function ($where) use($teacher){
                    $where->where('lesson_days.user_id', $teacher->id)->orWhereRaw('lesson_days.user_id is null');
                })
                ->select('lesson_days.type', 'lesson_days.date', 'class_times.start_time', 'class_times.end_time', 'class_times.id as ct_id')
                ->orderBy('lesson_days.date')
                ->orderBy('lesson_days.order')
                ->orderBy('class_times.start_time')
                ->get();

            $lectures = Lecture::realData()
                ->whereRaw("YEAR(date)='$year' AND MONTH(date)='$month'")
                ->where('class_letter_id', $classLetterId)
                ->where('subject_id', $subjectId)
                ->oldest('date')
                ->get();

            $homeworks = Homework::realData()
                ->whereRaw("YEAR(date)='$year' AND MONTH(date)='$month'")
                ->where('class_letter_id', $classLetterId)
                ->where('subject_id', $subjectId)->get();

            $data = [
                'students' => $students,
                'classDays' => $classDays,
                'year' => $year,
                'month' => $month,
                'classAllCounts' => $classAllCounts,
                'subjectId' => $subjectId,
                'classLetterId' => $classLetterId,
                'userMarksGroup' => $userMarksGroup,
                'lectures' => $lectures,
                'homeworks' => $homeworks
            ];

            return view('portal.pages.jurnal.jurnal', $data);
        }
    }

    public function studentsMarksModal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'user_id' => 'required|integer|exists:users,id,user_type,student,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'day' => 'required|integer',
            'checked_users' => 'nullable|array',
            'checked_users.*' => 'required|integer|exists:users,id,user_type,student,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
        ]);

        $hourId = $request->get('hour_id');

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $data = [
                'markTypes' => MarkType::realData()->get(),
                'modalId' => $request->get('modalId' ,1),
                'subjectId' => $request->get('subject_id' ,1),
                'classLetterId' => $request->get('class_letter_id' ,1),
                'year' => $request->get('year' ,1),
                'month' => $request->get('month' ,1),
                'day' => $request->get('day' ,1),
                'userId' => $request->get('user_id' ,1),
                'hourId' => $hourId,
                'checkedUsers' => $request->get('checked_users' , []),
            ];

            return view('portal.modals.jurnal.mark', $data);
        }
    }

    public function saveMarkAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'user_id' => 'required|integer|exists:users,id,user_type,student,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'day' => 'required|integer',
            'hour_id' => 'required|string',
            'checked_users' => 'nullable|array',
            'checked_users.*' => 'required|integer|exists:users,id,user_type,student,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'mark' => 'required|string',
        ],
        [
            'mark.required' => 'Qiymət seçilməyib',
        ]);

        $mark = $request->get('mark');
        $hourId = $request->get('hour_id');
        $year = $request->get('year', 0);
        $month = $request->get('month', 0);
        $day = $request->get('day', 0);
        $subjectId = $request->get('subject_id', 0);
        $classLetterId = $request->get('class_letter_id', 0);
        $dayType = $hourId > 0 ? 'day' : $hourId;
        $error = false;

        if(!$validator->fails() && (in_array($mark, ['qb', 'del']) ||
            (ctype_digit($mark) && MskMarks::realData()->where('id', $mark)->first()))==false
        ){
            $validator->getMessageBag()->add('mark', 'Qiymət düz seçilməyib.');
            $error = true;
        }
        else if(!$validator->fails() && (in_array($hourId, ['ksq', 'bsq']) ||
                (ctype_digit($hourId) && ClassTime::realData()->where('id', $hourId)->first()))==false
        ){
            $validator->getMessageBag()->add('mark', 'Qiymət düz seçilməyib.');
            $error = true;
        }
        else if(!$validator->fails() && (
            ($dayType === 'day' && (new LessonDaysFixer())->chechDayExists($year, $month, $day, $classLetterId, $subjectId, $dayType) )
            || ((new LessonDaysFixer())->chechDayExists($year, $month, $day, null, null, $dayType))
            ) === false
        ){
            $validator->getMessageBag()->add('mark', 'Bu günə qiymət yaza bilməzsiniz.');
            $error = true;
        }

        if ($error || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return ['status' => 'error', 'errors' => $errors];
        }
        else
        {
            $checkedUsers = $request->get('checked_users', []);
            $userId = $request->get('user_id', 0);
            $hourId = $request->get('hour_id', 0);
            $teacher = Auth::guard('portal')->user();
            $date = Carbon::create($year, $month, $day);
            //$currentYear = YearDays::getCurrentYear(false, false, $date);

            if(count($checkedUsers) == 0) $checkedUsers[] = $userId;

            foreach ($checkedUsers as $student){
                $markData = Mark::realData()->where('date', $date)
                    ->where('student_id', $student)
                    ->where('subject_id', $subjectId)
                    ->where('type', $dayType)
                    ->where('class_time_id', ($dayType == 'day' ? $hourId : null))->first();

                if(!$markData){
                    $markData = new Mark();
                }
                else if($mark == 'del'){
                    $markData->delete();
                }

                if($mark != 'del'){
                    $markData->date = $date;
                    $markData->tenant_id = $teacher->tenant_id;
                    $markData->teacher_id = $teacher->id;
                    $markData->student_id = $student;
                    $markData->subject_id = $subjectId;
                    $markData->class_letter_id = $classLetterId;
                    $markData->class_time_id = ($dayType == 'day' ? $hourId : null);
                    $markData->msk_mark_id = ($mark == 'qb' ? null : $mark );
                    $markData->qb = ($mark == 'qb' ? 1 : 0 );
                    $markData->type = $dayType;
                    $markData->save();
                }
                # yi & y
                if(in_array($dayType, ['ksq', 'bsq'])){
                    (new LessonDaysFixer())->fixStudentYIandY($student, $subjectId, $classLetterId);
                }
            }

            return ['status' => 'ok'];
        }
    }

    public function lectureModal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'day' => 'required|integer',
            'hour_id' => 'required|integer|exists:class_times,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $year = $request->get('year', 0);
            $month = $request->get('month', 0);
            $day = $request->get('day', 0);

            $data = [
                'modalId' => $request->get('modalId' ,1),
                'subjectId' => $request->get('subject_id' ,1),
                'classLetterId' => $request->get('class_letter_id' ,1),
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'hourId' => $request->get('hour_id' ,1),
                'classLetter' => ClassLetter::find($request->get('class_letter_id' ,1)),
                'date' => Carbon::create($year, $month, $day)
            ];

            return view('portal.modals.jurnal.lecture', $data);
        }
    }

    public function saveLectureAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'day' => 'required|integer',
            'hour_id' => 'required|integer|exists:class_times,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'paragraph_id' => 'required|integer|exists:season_paragraphs,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'files' => 'nullable|array',
            'files.*' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg,txt,mp4,mp3,pdf,xls,xlsx,ppt,doc,docx',
        ]);

        $year = $request->get('year', 0);
        $month = $request->get('month', 0);
        $day = $request->get('day', 0);
        $hourId = $request->get('hour_id', 0);
        $subjectId = $request->get('subject_id', 0);
        $paragraphId = $request->get('paragraph_id', 0);
        $classLetterId = $request->get('class_letter_id', 0);
        $teacher = Auth::guard('portal')->user();
        $date = Carbon::create($year, $month, $day);
        $error = false;

        $lectureChk = Lecture::realData()->where('date', $date)
            ->where('class_letter_id', $classLetterId)
            ->where('class_time_id', $hourId)
            ->where('season_paragraph_id', $paragraphId)
            ->first();


        if($lectureChk){
            $error = true;
            $validator->getMessageBag()->add('paragraph', 'Eyni saata eyni paragraf daxil edilə bilməz');
        }

        if ($error || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return ['status' => 'error', 'errors' => $errors];
        }
        else
        {
            $newLecture = new Lecture();
            $newLecture->date = $date;
            $newLecture->tenant_id = $teacher->tenant_id;
            $newLecture->teacher_id = $teacher->id;
            $newLecture->subject_id = $subjectId;
            $newLecture->class_letter_id = $classLetterId;
            $newLecture->class_time_id = $hourId;
            $newLecture->season_paragraph_id = $paragraphId;
            $newLecture->save();

            if($request->hasFile('files'))
                foreach ($request->file('files') as $file){
                    $fileName = uniqid('lecture_') . "." . $file->extension();
                    $file->move(public_path(Standarts::$portalLectureFilesDir), $fileName);

                    $newFile = new LectureFile();
                    $newFile->file_name = $fileName;

                    $newLecture->files()->save($newFile);
                }

            return ['status' => 'ok'];
        }
    }

    public function homeworkModal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'day' => 'required|integer',
            'hour_id' => 'required|integer|exists:class_times,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $year = $request->get('year', 0);
            $month = $request->get('month', 0);
            $day = $request->get('day', 0);

            $data = [
                'modalId' => $request->get('modalId' ,1),
                'subjectId' => $request->get('subject_id' ,1),
                'classLetterId' => $request->get('class_letter_id' ,1),
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'hourId' => $request->get('hour_id' ,1),
                'classLetter' => ClassLetter::find($request->get('class_letter_id' ,1)),
                'date' => Carbon::create($year, $month, $day)
            ];

            return view('portal.modals.jurnal.homework', $data);
        }
    }

    public function saveHomeworkAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'day' => 'required|integer',
            'hour_id' => 'required|integer|exists:class_times,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'task' => 'required|string',
            'files' => 'nullable|array',
            'files.*' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg,txt,mp4,mp3,pdf,xls,xlsx,ppt,doc,docx',
        ]);

        $year = $request->get('year', 0);
        $month = $request->get('month', 0);
        $day = $request->get('day', 0);
        $hourId = $request->get('hour_id', 0);
        $subjectId = $request->get('subject_id', 0);
        $classLetterId = $request->get('class_letter_id', 0);
        $teacher = Auth::guard('portal')->user();
        $date = Carbon::create($year, $month, $day);
        $error = false;

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return ['status' => 'error', 'errors' => $errors];
        }
        else
        {
            $newHomework = new Homework();
            $newHomework->date = $date;
            $newHomework->tenant_id = $teacher->tenant_id;
            $newHomework->teacher_id = $teacher->id;
            $newHomework->subject_id = $subjectId;
            $newHomework->class_letter_id = $classLetterId;
            $newHomework->class_time_id = $hourId;
            $newHomework->task = $request->get('task');
            $newHomework->save();

            if($request->hasFile('files'))
                foreach ($request->file('files') as $file){
                    $fileName = uniqid('homework_') . "." . $file->extension();
                    $file->move(public_path(Standarts::$portalLectureFilesDir), $fileName);

                    $newFile = new HomeworkFile();
                    $newFile->file_name = $fileName;

                    $newHomework->files()->save($newFile);
                }

            return ['status' => 'ok'];
        }
    }

    public function smsModal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'student_id' => 'required|integer|exists:users,id,user_type,student,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $year = $request->get('year', 0);
            $month = $request->get('month', 0);
            $day = $request->get('day', 0);
            $studentId = $request->get('student_id', 0);

            $data = [
                'modalId' => $request->get('modalId' ,1),
                'subjectId' => $request->get('subject_id' ,1),
                'classLetterId' => $request->get('class_letter_id' ,1),
                'year' => $year,
                'month' => $month,
                'studentId' => $studentId,
                'student' => User::find($studentId),
                'date' => Carbon::create($year, $month, $day)
            ];

            return view('portal.modals.jurnal.sms', $data);
        }
    }

    public function saveSmsAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_letter_id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'subject_id' => 'required|integer|exists:subjects,id,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'year' => 'required|integer',
            'month' => 'required|integer|digits_between:1,12',
            'student_id' => 'required|integer|exists:users,id,user_type,student,tenant_id,' . Auth::guard('portal')->user()->tenant_id,
            'sms' => 'nullable|integer|in:1',
            'text' => 'required|string',
            'files.*' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg,txt,mp4,mp3,pdf,xls,xlsx,ppt,doc,docx',
        ]);

        $subjectId = $request->get('subject_id', 0);
        $studentId = $request->get('student_id', 0);
        $classLetterId = $request->get('class_letter_id', 0);
        $teacher = Auth::guard('portal')->user();
        $text = $request->get('text');

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return ['status' => 'error', 'errors' => $errors];
        }
        else
        {
            $newHomework = new Sms();
            $newHomework->tenant_id = $teacher->tenant_id;
            $newHomework->teacher_id = $teacher->id;
            $newHomework->subject_id = $subjectId;
            $newHomework->class_letter_id = $classLetterId;
            $newHomework->student_id = $studentId;
            $newHomework->text = $text;
            $newHomework->sms = $request->get('sms', 0);
            $newHomework->save();

            if($request->hasFile('files'))
            {
                foreach ($request->file('files') as $file){
                    $fileName = uniqid('sms_') . "." . $file->extension();
                    $file->move(public_path(Standarts::$portalSmsFilesDir), $fileName);

                    $newFile = new SmsFiles();
                    $newFile->file_name = $fileName;

                    $newHomework->files()->save($newFile);
                }
            }


            $parents = PersonFamily::realData()->where('user_id',$studentId)->get();

            foreach ($parents as $parent){
                $notifyObj = new Notification();
                $notifyObj->user_id = $parent->parent_id;
                $notifyObj->content = strlen($text) > 25 ? substr($text,0,25)."..." : $text;
                $notifyObj->icon = "fa fa-address-book-o";
                $notifyObj->color = "#0368d2";
                $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                $notifyObj->route = route('portal_teacher_request',['sms_id' => $newHomework->id]);
                $notifyObj->save();

//                event(new NotificationEvent($notifyObj));
            }

            return ['status' => 'ok'];
        }
    }

    public function deleteLectureAction(){
        $validator = validator(request()->all(),[
            'lecture_id' => 'required|integer|exists:lectures,id,tenant_id,'.auth('portal')->user()->tenant_id,
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 'error','error' => 'There is not a object with id : '.request('lecture_id')]);
        }
        else{
            $lectureObj = Lecture::realData()->find(request('lecture_id'));
            if(!$lectureObj){
                return response()->json(['status' => 'error','error' => 'There is not a object with id : '.request('lecture_id')]);
            }
            else{
                $lectureObj->delete();
                return response()->json(['status' => 'ok']);
            }
        }
    }
}
