<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\AdvertTask;
use App\Models\Holiday;
use App\Models\LessonDay;
use App\Models\LetterGroup;
use App\Models\Notification;
use App\Models\TblFpInOut;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->guard('portal')->user()->user_type == "parent"){
            HomeController::changeStudent(Helper::getSelectedStudentForParent());
        }

        $user = Auth::guard('portal')->user();
        $toDay = Carbon::now()->format('Y-m-d');
        $currentYear = YearDays::getCurrentYear(false, false, date("Y-m-d"));
        $selectedStudentId = Helper::getSelectedStudentForParent();

        if($user->user_type == 'teacher'){
            $lessons = LessonDay::realData()
                ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->leftJoin('rooms', 'rooms.id', 'lessons.room_id')
                ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                ->leftJoin('subjects', 'subjects.id', 'lessons.subject_id')
                ->where('lesson_days.user_id', $user->id)
                ->where("lesson_days.date", $toDay)
                ->orderBy('class_times.start_time')
                ->select('lesson_days.substitution_lesson_id', 'class_times.start_time','subjects.name as subject_name','class_letters.name as letter_name','msk_classes.name as class_name','rooms.name as room_name')
                ->get();
        }
        else if($user->user_type == 'student'){

            $studentGroup = LetterGroup::realData()
                ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
                ->join('class_letters', 'letter_groups.class_letter_id', 'class_letters.id')
                ->where('letter_group_user.year_id', (int)$currentYear['id'])
                ->where('letter_group_user.user_id', (int)$user->id)
                ->select('letter_groups.*', 'class_letters.msk_class_id')
                ->first();

            $lessons = LessonDay::realData()
                ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->leftJoin('rooms', 'rooms.id', 'lessons.room_id')
                ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                ->leftJoin('subjects', 'subjects.id', 'lessons.subject_id')
                ->where('lesson_days.letter_group_id', $studentGroup['id'])
                ->where("lesson_days.date", $toDay)
                ->orderBy('class_times.start_time')
                ->select('lesson_days.substitution_lesson_id', 'class_times.start_time','subjects.name as subject_name','class_letters.name as letter_name','msk_classes.name as class_name','rooms.name as room_name')
                ->get();
        }
        else if($user->user_type == 'parent')
        {
            $studentGroup = LetterGroup::realData()
                ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
                ->join('class_letters', 'letter_groups.class_letter_id', 'class_letters.id')
                ->where('letter_group_user.year_id', (int)$currentYear['id'])
                ->where('letter_group_user.user_id', $selectedStudentId)
                ->select('letter_groups.*', 'class_letters.msk_class_id')
                ->first();

            $lessons = LessonDay::realData()
                ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->leftJoin('rooms', 'rooms.id', 'lessons.room_id')
                ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                ->leftJoin('subjects', 'subjects.id', 'lessons.subject_id')
                ->where('lesson_days.letter_group_id', $studentGroup['id'])
                ->where("lesson_days.date", $toDay)
                ->orderBy('class_times.start_time')
                ->select('lesson_days.substitution_lesson_id', 'class_times.start_time','subjects.name as subject_name','class_letters.name as letter_name','msk_classes.name as class_name','rooms.name as room_name')
                ->get();
        }


        $groupInOuts = [];
        $lastType = '';
        $index = 0;

        if($user->user_type == 'parent')
        {
            $inOutLog = TblFpInOut::join('tblFpRegisters', 'tblFpRegisters.id', 'tblFpInOuts.reg_id')
                ->where('tblFpRegisters.uid', $selectedStudentId)
                ->whereRaw("CAST(date as DATE)='$toDay'")
                ->orderBy('date', 'DESC')
                ->take(3)->get();
        }
        else{
            $inOutLog = TblFpInOut::join('tblFpRegisters', 'tblFpRegisters.id', 'tblFpInOuts.reg_id')
                ->where('tblFpRegisters.uid', $user->id)
                ->whereRaw("CAST(date as DATE)='$toDay'")
                ->orderBy('date', 'DESC')
                ->take(3)->get();
        }

        foreach ($inOutLog as $log){
            if($log['in_out_type'] === $lastType || $log['in_out_type'] === 'Out'){
                $index++;
                $groupInOuts[$index] = [];
            }

            $groupInOuts[$index][$log['in_out_type']] = Carbon::parse($log['date']);
            $lastType = $log['in_out_type'];
        }

        $tasks = AdvertTask::realData()
            ->join('advert_task_users', function ($join) use($user){
                $join->on('advert_task_users.advert_task_id', 'advert_tasks.id')
                    ->on('advert_task_users.user_id', DB::raw($user->id));
            })
            ->orderByDesc('advert_tasks.id')
            ->take(3)
            ->get();

        $today = Carbon::now()->format('Y-m-d');
        $end = new Carbon('last day of this month');
        $end = $end->format('Y-m-d');

        if($user->user_type == 'parent' || $user->user_type == 'student'){
            $holidays = Holiday::realData()
                ->join('holiday_msk_class', function ($join) use($studentGroup){
                    $join->on('holiday_msk_class.holiday_id', 'holidays.id')
                        ->on('holiday_msk_class.msk_class_id', DB::raw((int)$studentGroup['msk_class_id']));
                })
                ->whereBetween('start_date',[$today,$end])
                ->orWhereBetween('end_date',[$today,$end])
                ->get();
        }
        else{
            $holidays = Holiday::realData()
                ->whereBetween('start_date',[$today,$end])
                ->orWhereBetween('end_date',[$today,$end])
                ->where('year_id',YearDays::getCurrentYear()->id)
                ->get();
        }


        $data = [
            'lessons' => $lessons,
            'tasks' => $tasks,
            'holidays' => $holidays,
            'groupInOuts' => $groupInOuts
        ];

        return view('portal.dashboard.dashboard', $data);
    }

    public function changeLang($locale)
    {
        $cookie = cookie('my_locale',$locale,45000);
        return redirect()->back()->cookie($cookie);
    }

    public function tableModal(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $user = Auth::guard('portal')->user();
            $weekFirstDate = Carbon::now()->modify('this week')->format('Y-m-d');
            $weekLastDate = Carbon::now()->modify('this week +6 days')->format('Y-m-d');
            $currentYear = YearDays::getCurrentYear(false, false, date("Y-m-d"));
            $selectedStudentId = Helper::getSelectedStudentForParent();

            if($user->user_type == 'teacher'){
                $allLessonOfWeek = LessonDay::realData()
                    ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                    ->leftJoin('rooms', 'rooms.id', 'lessons.room_id')
                    ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                    ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                    ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                    ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                    ->leftJoin('subjects', 'subjects.id', 'lessons.subject_id')
                    ->where('lesson_days.user_id', $user->id)
                    ->whereRaw("lesson_days.date >= '$weekFirstDate' AND lesson_days.date <= '$weekLastDate'")
                    ->orderBy('class_times.start_time')
                    ->select('lesson_days.substitution_lesson_id', 'lesson_days.date','class_times.start_time','subjects.name as subject_name','class_letters.name as letter_name','msk_classes.name as class_name','rooms.name as room_name')
                    ->get();
            }
            else if($user->user_type == 'student'){
                $studentGroup = LetterGroup::realData()
                    ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
                    ->where('letter_group_user.year_id', (int)$currentYear['id'])
                    ->where('letter_group_user.user_id', (int)$user->id)
                    ->select('letter_groups.*')
                    ->first();

                $allLessonOfWeek = LessonDay::realData()
                    ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                    ->leftJoin('rooms', 'rooms.id', 'lessons.room_id')
                    ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                    ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                    ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                    ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                    ->leftJoin('subjects', 'subjects.id', 'lessons.subject_id')
                    ->where('lesson_days.letter_group_id', $studentGroup['id'])
                    ->whereRaw("lesson_days.date >= '$weekFirstDate' AND lesson_days.date <= '$weekLastDate'")
                    ->orderBy('class_times.start_time')
                    ->select('lesson_days.substitution_lesson_id', 'lesson_days.date','class_times.start_time','subjects.name as subject_name','class_letters.name as letter_name','msk_classes.name as class_name','rooms.name as room_name')
                    ->get();
            }
            else if($user->user_type == 'parent'){
                $studentGroup = LetterGroup::realData()
                    ->join('letter_group_user', 'letter_group_user.letter_group_id', 'letter_groups.id')
                    ->where('letter_group_user.year_id', (int)$currentYear['id'])
                    ->where('letter_group_user.user_id', $selectedStudentId)
                    ->select('letter_groups.*')
                    ->first();

                $allLessonOfWeek = LessonDay::realData()
                    ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                    ->leftJoin('rooms', 'rooms.id', 'lessons.room_id')
                    ->leftJoin('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                    ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                    ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                    ->leftJoin('class_times', 'class_times.id', 'lessons.class_time_id')
                    ->leftJoin('subjects', 'subjects.id', 'lessons.subject_id')
                    ->where('lesson_days.letter_group_id', $studentGroup['id'])
                    ->whereRaw("lesson_days.date >= '$weekFirstDate' AND lesson_days.date <= '$weekLastDate'")
                    ->orderBy('class_times.start_time')
                    ->select('lesson_days.substitution_lesson_id', 'lesson_days.date','class_times.start_time','subjects.name as subject_name','class_letters.name as letter_name','msk_classes.name as class_name','rooms.name as room_name')
                    ->get();
            }

            $lessonsGroupForWeekDay = [];
            foreach ($allLessonOfWeek as $lesson){
                $date = Carbon::parse($lesson->date);
                $weekDay = Helper::getDayOfWeekIso($date->dayOfWeek);

                if(!isset($lessonsGroupForWeekDay[$weekDay])) $lessonsGroupForWeekDay[$weekDay] = [];

                $lessonsGroupForWeekDay[$weekDay][] = $lesson;
            }

            $data = [
                'modalId' => $request->get('modalId' ,1),
                'lessonsGroupForWeekDay' => $lessonsGroupForWeekDay
            ];

            return view('portal.modals.dashboard.table', $data);
        }
    }

    public static function getTaskTr($tasks)
    {
        $tr = "";
        $statusTr = "";
        foreach ($tasks as $task) {
            $today = \Carbon\Carbon::parse(\Carbon\Carbon::today()->format('Y-m-d'));
            $date_of_show = \Carbon\Carbon::parse($task['date_of_show']);

            $status = strtotime($task->date_of_event) > strtotime(date('d-m-Y')) ?
                2: (strtotime($task->date_of_event) == strtotime(date('d-m-Y')) ?
                    0 : 1);
            if($today->gte($date_of_show)){
                if ($status == 2) {
                    $statusTr = "<div class=\"status-pill green\" data-title=\"Gözlənilən\" data-toggle=\"tooltip\"
                                                 data-original-title=\"\" title=\"Gözlənilən\"></div>";
                } else if ($status == 1) {
                    $statusTr = "<div class=\"status-pill red\" data-title=\"Bitib\" data-toggle=\"tooltip\"
                                                 data-original-title=\"\" title=\"Bitib\"></div>";
                } else {
                    $statusTr = "<div class=\"status-pill yellow\" data-title=\"Son gün\" data-toggle=\"tooltip\"
                                                 data-original-title=\"\" title=\"Son gün\"></div>";
                }

                $tr .= "<tr tr_id=" . $task->id . ">
                                    <td></td>
                                    <td>" . $task->date_of_event . "</td>
                                    <td style='word-break: break-all;'>" . $task->content . "</td>
                                    <td>" . $task->user->name . " " . $task->user->surname . "</td>
                                    <td class=\"text-center\">" . $statusTr . "</td>
                                </tr>";
            }
        }

        $tr = $tr ? $tr : "<tr>
                <th style='background-color: aliceblue;'></th>
                <th style='background-color: aliceblue;'></th>
                <th style='background-color: aliceblue;'>
                <img style='width: 50px;opacity: 0.8;' src='" . asset('portal\img\task_not_found.png') . "' alt='task not found image'> 
                ".trans('system_messages.dashboard.advert_task.task_not_found')."</th>
                <th style='background-color: aliceblue;'></th>
                <th style='background-color: aliceblue;'></th>
                </tr>";

        return $tr;
    }

    public function filterTask(Request $request){

       $user = Auth::guard('portal')->user();

       $tasks = AdvertTask::realData()
           ->join('advert_task_users', function ($join) use($user){
               $join->on('advert_task_users.advert_task_id', 'advert_tasks.id')
                   ->on('advert_task_users.user_id', DB::raw($user->id));
           });

       if ($request->has('date_of_event') && $request->get('date_of_event') != '') {
           $tasks = $tasks->where('date_of_event', '=', date('Y-m-d', strtotime($request->get('date_of_event'))));
       }

        if ($request->has('status') && $request->get('status') != '') {
            $status = $request->get('status');
            if ($status > 0) {
                if ($status == 1) {
                    $tasks->whereDate('date_of_event', '>', date('Y-m-d'));
                } else if ($status == 2) {
                    $tasks->whereDate('date_of_event', '=', date('Y-m-d'));
                } else {
                    $tasks->whereDate('date_of_event', '<', date('Y-m-d'));
                }
            }

        }
        if ($request->has('adding_user') && $request->get('adding_user') != '') {
            $tasks = $tasks->where('adding_user', $request->get('adding_user'));
        }

        if ($request->has('content') && $request->get('content') != '') {
            $tasks = $tasks->where('content','like','%'.$request->get('content').'%');
        }

        $tasks = $tasks->orderByDesc('advert_tasks.id')->paginate(6);
        $tr = self::getTaskTr($tasks);
        $pagination = (string)$tasks->links();
        return response()->json(['status' => 'ok', 'data' => $tr , 'pagination' => $pagination ,'pageCount' => $tasks->lastPage()]);
    }

    public function taskModal(Request $request)
    {
        $user = Auth::guard('portal')->user();

        $tasks = AdvertTask::realData()
            ->join('advert_task_users', function ($join) use($user){
                $join->on('advert_task_users.advert_task_id', 'advert_tasks.id')
                    ->on('advert_task_users.user_id', DB::raw($user->id));
            })->orderByDesc('advert_tasks.id')->paginate(6);

        $data = [
            'modalId' => $request->get('modalId', 1),
            'tasks' => $tasks
        ];

        return view('portal.modals.dashboard.task', $data);
    }

    public function getAddingUsers(Request $request)
    {
        if ($request->has('ne') && $request->get('ne') == "get_all_users") {
            $tasks = DB::table('advert_tasks')
                ->join('users', 'users.id', 'advert_tasks.adding_user')
                ->where('users.name', 'like', '%' . $request->get('q') . '%')
                ->orWhere('users.surname', 'like', '%' . $request->get('q') . '%')
                ->select('users.id as id', 'users.name as name', 'users.surname as surname')
                ->groupBy('users.id', 'users.name', 'users.surname')
                ->get();
            $results = [];
            foreach ($tasks as $task) {
                $results[] = ['id' => $task->id, 'text' => $task->name . " " . $task->surname];
            }
        }
        return response()->json(['results' => $results]);
    }

    public function inOutLogModal(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return $errors;
        }
        else
        {
            $user = Auth::guard('portal')->user();
            $toDay = Carbon::now()->format('Y-m-d');
            $selectedStudentId = Helper::getSelectedStudentForParent();

            $groupInOuts = [];
            $lastType = '';
            $index = 0;
            if($user->user_type == 'parent'){
                $inOutLog = TblFpInOut::join('tblFpRegisters', 'tblFpRegisters.id', 'tblFpInOuts.reg_id')
                    ->where('tblFpRegisters.uid', $selectedStudentId)
                    ->whereRaw("CAST(date as DATE)='$toDay'")
                    ->orderBy('date', 'ASC')
                    ->get();
            }
            else{
                $inOutLog = TblFpInOut::join('tblFpRegisters', 'tblFpRegisters.id', 'tblFpInOuts.reg_id')
                    ->where('tblFpRegisters.uid', $user->id)
                    ->whereRaw("CAST(date as DATE)='$toDay'")
                    ->orderBy('date', 'ASC')
                    ->get();
            }

            foreach ($inOutLog as $log){
                if($log['in_out_type'] === $lastType || $log['in_out_type'] === 'In'){
                    $index++;
                    $groupInOuts[$index] = [];
                }

                $groupInOuts[$index][$log['in_out_type']] = Carbon::parse($log['date']);
                $lastType = $log['in_out_type'];
            }

            $data = [
                'modalId' => $request->get('modalId' ,1),
                'groupInOuts' => $groupInOuts
            ];

            return view('portal.modals.dashboard.in_out_logs', $data);
        }
    }

    public function updateNotifyForSeen(){
        $id = request('id');
        $notifyObj = Notification::find($id);
        if ($notifyObj == null){
            return response()->json(['status' => 'error']);
        }
        else{
            $notifyObj->seen = 1;
            $notifyObj->save();
            return response()->json(['status' => 'ok']);
        }
    }

    public static function changeStudent($id){
        session()->forget('selected_student_for_parent');
        session(['selected_student_for_parent' => $id]);
        return redirect()->back();
    }
}
