<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Library\YearDays;
use App\Models\ClassLetter;
use App\Models\Duty;
use App\Models\Lesson;
use App\Models\LessonDay;
use App\Models\Season;
use App\Models\SeasonParagraph;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    public function subjects(Request $request)
    {
        $results = [];
        $teacher = Auth::guard('portal')->user();

        foreach($teacher->teacher_subjects()->where('subjects.name','like','%'.$request->get('q').'%')->get() as $subject){
            $results[] = ['id' => $subject->id,'text' => $subject->name];
        }

        return response()->json(['results' => $results]);
    }

    public function subjectClasses(Request $request)
    {
        $results = [];
        $teacher = Auth::guard('portal')->user();
        $subjectId = $request->get('subject_id', 0);
        $year = $request->get('year', 0);
        $month = $request->get('month', 0);
        //$currentYear = YearDays::getCurrentYear($month, $year);

        if($subjectId > 0){

            $classes = LessonDay::realData()
                ->join('lessons', 'lessons.id', 'lesson_days.lesson_id')
                ->join('letter_groups', 'letter_groups.id', 'lessons.letter_group_id')
                ->join('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
                ->join('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
                ->where('lessons.subject_id', $subjectId)
                ->whereRaw("YEAR(lesson_days.date)='$year'")->whereRaw("MONTH(lesson_days.date)='$month'")
                ->where('lesson_days.user_id', $teacher->id)
                ->groupBy('letter_groups.id','class_letters.id', 'class_letters.name', 'msk_classes.name')
                ->select('class_letters.id', 'class_letters.name as name2', 'msk_classes.name','letter_groups.id as letter_group_id')
                ->get();

            foreach($classes as $class){
                $results[] = ['id' => $class->id, 'text' => $class->name.$class->name2,'letter_group' => $class->letter_group_id];
            }
        }

        return response()->json(['results' => $results]);
    }

    public function seasons(Request $request)
    {
        $results = [];
        $subjectId = $request->get('subject_id', 0);
        $classLetterId = $request->get('class_letter_id', 0);
        $classLetter = ClassLetter::realData()->where('id', $classLetterId)->first();

        if($classLetterId > 0 && $subjectId > 0){

            $seasons = Season::realData()->where('subject_id', $subjectId)
                ->where('msk_class_id', $classLetter['msk_class_id'])
                ->get();

            foreach($seasons as $season){
                $results[] = ['id' => $season->id, 'text' => $season->name];
            }
        }

        return response()->json(['results' => $results]);
    }

    public function seasonParagraphs(Request $request)
    {
        $results = [];
        $seasonId = $request->get('season_id', 0);

        if($seasonId > 0){

            $paragraphs = SeasonParagraph::realData()->where('season_id', $seasonId)
                ->get();

            foreach($paragraphs as $paragraph){
                $results[] = ['id' => $paragraph->id, 'text' => $paragraph->name];
            }
        }

        return response()->json(['results' => $results]);
    }
}
