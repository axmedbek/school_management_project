<?php

namespace App\Http\Controllers\Portal;

use App\Library\Helper;
use App\Models\TblFpInOut;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function index()
    {
        return view('portal.student_attendance.student_attendace');
    }

    public function getTable(Request $request)
    {
        $user = auth()->guard('portal')->user();
        $start_interval = Carbon::parse($request->get('start_interval'))->subDay()->format('Y-m-d h:i:s');
        $end_interval = Carbon::parse($request->get('end_interval'))->format('Y-m-d h:i:s');
        $selectedStudentId = Helper::getSelectedStudentForParent();


        $data = [
            'inOutLogs' => [],
            'pagination' => 0
        ];


        return view('portal.pages.student_attendance.student_attendance_table', $data);
    }
}
