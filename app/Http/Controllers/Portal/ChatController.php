<?php

namespace App\Http\Controllers\Portal;

use App\Library\Helper;
use App\Library\YearDays;
use App\Models\Chat;
use App\Models\ClassLetter;
use App\Models\LetterGroup;
use App\Models\Notification;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Baku');
    }

    public function index()
    {

        $groups = null;
        $id = auth()->user()->id;

        if (auth()->user()->user_type == 'parent') {
            $users = User::realData()
                ->where('user_type', '!=', 'student')
                ->where('id', '!=', $id);

            $letter_groups = User::realData('parent')
                ->join('person_families', 'person_families.parent_id', 'users.id')
                ->join('letter_group_user', 'letter_group_user.user_id', 'person_families.user_id')
                ->where('users.id', $id)
                ->select('letter_group_user.letter_group_id')
                ->groupBy('letter_group_user.letter_group_id')
                ->get();
        } else if (auth()->user()->user_type == 'student') {

            $users = User::realData()
                ->where('user_type', '!=', 'parent')
                ->where('id', '!=', $id);

            $letter_groups = User::realData('student')
                ->join('letter_group_user', 'letter_group_user.user_id', 'users.id')
                ->where('users.id', $id)
                ->select('letter_group_user.letter_group_id')
                ->groupBy('letter_group_user.letter_group_id')
                ->get();
        } else if (auth()->user()->user_type == 'teacher') {
            $users = User::realData()
                ->where('id', '!=', auth()->user()->id);

            $letter_groups = User::realData('teacher')
                ->join('lessons', 'lessons.user_id', 'users.id')
                ->where('users.id', $id)
                ->select('lessons.letter_group_id')
                ->groupBy('lessons.letter_group_id')
                ->get();
        }

        $users = $users->where('users.user_type','!=','staff');

        if (request('type') == 'ajax') {
            $users = $users->where(DB::raw("concat(name,' ',surname,' ',middle_name,' ')"), 'like', '%' . request('searchedVal') . '%')
                ->get();
            $htmlContent = ChatController::chatUserHtmlCreator($users);
            return response()->json(['status' => 'ok', 'users' => $htmlContent]);
        }

        $users = $users->get();

        $data = [
            'users' => $users,
            'groupUsers' => [],
            'letterGroups' => $letter_groups,
        ];

        return view('portal.chat.chat', $data);
    }

    public function getMessages()
    {
        $sender_id = request('sender_id');
        $recivier_id = request('recivier_id');
        $type = request('type');

        $newMessageCount = ChatController::makeReadedSms($sender_id, $recivier_id, $type);

        if ($type == "private") {
            $messages = Chat::realData()
                ->where(function ($query) use ($sender_id, $recivier_id) {
                    $query->where('sender_id', $sender_id);
                    $query->where('reciever_id', $recivier_id);
                })
                ->orWhere(function ($query) use ($sender_id, $recivier_id) {
                    $query->where('sender_id', $recivier_id);
                    $query->where('reciever_id', $sender_id);
                })
                ->get();
        } else if ($type == "group") {
            $messages = Chat::realData()
                ->where('letter_group_id', $recivier_id)
                ->get();
        }

        $messageContent = "";
        foreach ($messages as $message) {
            if ($sender_id == $message->sender_id) {
                $messageContent .= "<div class=\"chat-message self\">
                                <div class=\"chat-message-content-w\">
                                <div class=\"chat-message-content\" style='word-break: break-all;'>" . $message->message . "</div>
                                </div>
                                <div class=\"chat-message-date\">" . Carbon::parse($message->date)->format('Y-m-d H:i') . "</div>
                                <div class=\"chat-message-avatar\"><img alt=\"\" src='" . asset('images/user/thumb/' . $message->senderUser->thumb) . "'></div>
                                </div>";
            } else {
                $messageContent .= "<div class=\"chat-message\">
                <div class=\"chat-message-content-w\">
                <div class=\"chat-message-content\" style='color: #885050;background-color: #f1dcbe;'>" . $message->senderUser->fullname() . "</div></div>
                <div class=\"chat-message-avatar\"><img alt=\"\" src='" . asset('images/user/thumb/' . $message->senderUser->thumb) . "'>
                </div>
                     <div class=\"chat-message-content\" style='margin-left: 0;margin-bottom: 0;padding: 0;padding-left: 33px;word-break: break-all;'>" . $message->message . "</div><br>
                     <div class=\"chat-message-date\" style='margin-left: 77px;'>" . Carbon::parse($message->date)->format('Y-m-d H:i') . "</div>
                </div>";
            }
        }
        return response()->json(['status' => 'ok', 'messages' => $messageContent, 'newMessagesCount' => $newMessageCount]);
    }

    public function getUserInfo()
    {
        if (!(request()->filled('user_id') && request()->get('user_id') > 0)) {
            return response()->json(['status' => 'error', 'message' => 'Header paramaeters is wrong']);
        }
        $user_id = request('user_id');
        $user = User::realData()->find($user_id);
        if ($user == null) {
            return response()->json(['status' => 'error', 'message' => 'There is not a user with this user_id']);
        }
        $selectedUser = null;
        if ($user->user_type == "teacher") {
            $selectedUser = User::realData('teacher')
                ->join('lessons', 'lessons.user_id', 'users.id')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name', 'lessons.letter_group_id as group_id','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name', 'lessons.letter_group_id','users.user_type')
                ->first();
        } else if ($user->user_type == "student") {
            $selectedUser = User::realData('student')
                ->join('letter_group_user', 'letter_group_user.user_id', 'users.id')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name', 'letter_group_user.letter_group_id as group_id','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name', 'letter_group_user.letter_group_id','users.user_type')
                ->first();
        } else if ($user->user_type == "parent") {
            $selectedUser = User::realData('parent')
                ->join('person_families', 'person_families.parent_id', 'users.id')
                ->join('letter_group_user', 'letter_group_user.user_id', 'person_families.user_id')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name', 'letter_group_user.letter_group_id as group_id','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name', 'letter_group_user.letter_group_id','users.user_type')
                ->first();
        } else if ($user->user_type == "user") {
            $selectedUser = User::realData('user')
                ->where('users.id', $user->id)
                ->select('users.id as id', 'users.name as name', 'users.surname as surname', 'users.thumb as thumb', 'users.middle_name as middle_name','users.user_type as user_type')
                ->groupBy('users.id', 'users.name', 'users.surname', 'users.thumb', 'users.middle_name','users.user_type')
                ->first();
        }

        $selectedUser['school_management_system_session'] = $_COOKIE['school_management_system_session'];
        return response()->json(['status' => 'success', 'user' => $selectedUser]);
    }

    public function saveMessage()
    {
        $validator = validator(request()->all(), [
            'sender_id' => 'required|integer|exists:users,id',
            //'recivier_id' => 'nullable|integer|exists:users,id',
            'message' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error']);
        } else {
            if (request('type') == 'private') {
                $message = new Chat();
                $message->sender_id = request('sender_id');
                $message->reciever_id = request('recivier_id');
                $message->message = request('message');
                $message->tenant_id = auth()->user()->tenant_id;
                $message->date = Carbon::now();
                $message->save();

                //add notification
                $notifyObj = new Notification();
                $notifyObj->user_id = request('recivier_id');
                $notifyObj->content = User::realData()->find(request('sender_id'))->fullname() . " sizə mesaj göndərdi";
                $notifyObj->icon = "fa fa-comment";
                $notifyObj->color = "#333F57";
                $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                $notifyObj->route = route('portal_chat', ['tab' => 'chat', 'rec_id' => request('sender_id')]);
                $notifyObj->save();
            } else if (request('type') == 'group') {
                $message = new Chat();
                $message->sender_id = request('sender_id');
                $message->letter_group_id = request('recivier_id');
                $message->message = request('message');
                $message->tenant_id = auth()->user()->tenant_id;
                $message->date = Carbon::now();
                $message->save();

                //add notification all group users
                foreach (ChatController::getUsersForLetterGroup(request('recivier_id')) as $user) {
                    $letterGroup = LetterGroup::realData()->find(request('recivier_id'));
                    $notifyObj = new Notification();
                    $notifyObj->user_id = $user->id;
                    $notifyObj->content = $letterGroup->class_letter->msk_class->name . "/" . $letterGroup->class_letter->name . " qrupundan mesaj göndərildi";
                    $notifyObj->icon = "fa fa-comment";
                    $notifyObj->color = "#333F57";
                    $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                    $notifyObj->route = route('portal_chat', ['tab' => 'groups', 'rec_id' => request('recivier_id')]);
                    $notifyObj->save();
                }
            }
        }
        return response()->json(['status' => 'success', 'notifyObj' => $notifyObj]);
    }

    public static function chatUserHtmlCreator($users)
    {
        $htmlContent = "";
        foreach ($users as $user) {
            if ($user->user_type == 'user'){
                if ($user->group->getModulePriv('admin_chat') < 2){
                    continue;
                }
            }
            $dateDiff = Carbon::now()->getTimestamp() - Carbon::parse($user->last_active_date)->getTimestamp();
//            ($dateDiff == 0 || $dateDiff > 3000) ? 'Offline' : gmdate("H:i:s",$dateDiff).' san.')
            $htmlContent .= "<div class=\"user-w\" data-user-id=\"" . $user->id . "\">
                                    <div class=\"avatar with-status status-green\"><img alt=\"\"
                                                                                      src=\"" . (asset('images/user/thumb/' . $user->thumb)) . "\">
                                    </div>
                                    <div class=\"user-info\">
                                        <div class=\"user-date\"
                                        style=\"color:" . ($user->is_online ? 'green' : 'red') . "\"
                                        >" . ($user->is_online ? 'Online' : 'Offline') . "</div>
                                        <div class=\"user-name\">" . $user->fullname() . "</div>
                                        <div class=\"last-message\">
                                                   " . ($user->user_type == 'teacher' ?
                    (isset($user->lesson) ? $user->lesson->subject->name . ' müəllimi' : ' Müəllim')
                    : ($user->letter_groups->first() ?
                        'Sinif : ' . ClassLetter::realData()->find($user->letter_groups->first()['class_letter_id'])['msk_class']['name'] .
                        ClassLetter::realData()->find($user->letter_groups->first()['class_letter_id'])['name']
                        : 'Şagird')
                ) . "
                                        </div>
                                    </div>
                                </div>";
        }

        return $htmlContent;
    }

    public function makeOfflineUser()
    {
        $user_id = request('user_id');
        if ($user_id == null || $user_id <= 0) {
            return response()->json(['status' => 'error', 'message' => 'Parameters is incorrect']);
        }
        $user = User::realData()->find($user_id);
        if ($user == null) {
            return response()->json(['status' => 'error', 'message' => 'There is not a user with this user_id']);
        }

        if (auth('portal')->check()){
            Helper::makeOnlineUserOrOffline('portal', Carbon::now(), request('status'));
        }
        else if(auth('web')->check()){
            Helper::makeOnlineUserOrOffline('web', Carbon::now(), request('status'));
        }
        return response()->json(['status' => 'ok']);
    }

    public function makeReadedSms($sender_id, $recivier_id, $type)
    {
        if ($type == "private") {
            $messages = Chat::realData()
                ->where(function ($query) use ($sender_id, $recivier_id) {
                    $query->where('sender_id', $sender_id);
                    $query->where('reciever_id', $recivier_id);
                })
                ->orWhere(function ($query) use ($sender_id, $recivier_id) {
                    $query->where('sender_id', $recivier_id);
                    $query->where('reciever_id', $sender_id);
                })
                ->get();
            foreach ($messages as $message) {
                $message->read = 1;
                $message->save();
            }
        }

        return count(Chat::realData()->where('sender_id', $recivier_id)->where('reciever_id', $sender_id)->where('read', 0)->get());
    }

    public function getGroupUsers()
    {
        $users = self::getUsersForLetterGroup(request('letter_group_id'));
        $data = [
            'modalId' => request('modalId', 1),
            'users' => $users
        ];

        return view('portal.modals.chat.group_user_info', $data);
    }

    public function getUsersForLetterGroup($letter_group_id)
    {
        // group users listing
        $groupUsers = User::realData('teacher')
            ->join('lessons', 'lessons.user_id', 'users.id')
            ->where('lessons.letter_group_id', $letter_group_id)
            ->select('users.id as id',
                'users.name as name',
                'users.surname as surname',
                'users.middle_name as middle_name',
                'users.user_type as user_type',
                'users.thumb as thumb')
            ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            ->unionAll(
                User::realData('student')
                    ->join('letter_group_user', 'letter_group_user.user_id', 'users.id')
                    ->where('letter_group_user.letter_group_id', $letter_group_id)
                    ->select('users.id as id',
                        'users.name as name',
                        'users.surname as surname',
                        'users.middle_name as middle_name',
                        'users.user_type as user_type',
                        'users.thumb as thumb')
                    ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            )->unionAll(
                User::realData('parent')
                    ->join('person_families', 'person_families.parent_id', 'users.id')
                    ->join('letter_group_user', 'letter_group_user.user_id', 'person_families.user_id')
                    ->where('letter_group_user.letter_group_id', $letter_group_id)
                    ->select('users.id as id',
                        'users.name as name',
                        'users.surname as surname',
                        'users.middle_name as middle_name',
                        'users.user_type as user_type',
                        'users.thumb as thumb')
                    ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            )->unionAll(
                        User::realData('user')
                            ->select('users.id as id',
                                'users.name as name',
                                'users.surname as surname',
                                'users.middle_name as middle_name',
                                'users.user_type as user_type',
                                'users.thumb as thumb')
                            ->groupBy('users.id', 'users.name', 'users.surname', 'users.middle_name', 'users.thumb', 'users.user_type')
            )->get();

        return $groupUsers;
    }
}
