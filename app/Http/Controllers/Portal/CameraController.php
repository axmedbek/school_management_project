<?php

namespace App\Http\Controllers\Portal;

use App\Library\Helper;
use App\Library\YearDays;
use App\Models\Lesson;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CameraController extends Controller
{

    public function index(){
        date_default_timezone_set("Asia/Baku");
        $student_id = Helper::getSelectedStudentForParent();
        $year_id = YearDays::getCurrentYear()->id;
        $currentDate = Carbon::now()->format('Y-m-d');
        $currentTime = Carbon::now()->format('H:i:s');
//        $currentTime = Carbon::parse('11:38');

        $nextLesson = null;
        $nLesson = null;
        $camera = null;
        $currentLesson = null;
        $timeUntilStartLesson = 0;

        //dd($currentTime);

        $todayLessons = $currentLesson = DB::table('lesson_days')
            ->join('lessons','lessons.id','lesson_days.lesson_id')
            ->join('class_times','class_times.id','lessons.class_time_id')
            ->join('letter_group_user','letter_group_user.letter_group_id','lessons.letter_group_id')
            ->where('letter_group_user.user_id',$student_id)
            ->where('lessons.year_id',$year_id)
            ->where('lesson_days.date',$currentDate)
            ->whereNull('lessons.deleted_at')
            ->select('class_times.start_time as start_time','class_times.end_time as end_time')
            ->get();

        $currentLesson = DB::table('lesson_days')
            ->join('lessons','lessons.id','lesson_days.lesson_id')
            ->join('class_times','class_times.id','lessons.class_time_id')
            ->join('letter_group_user','letter_group_user.letter_group_id','lessons.letter_group_id')
            ->where('letter_group_user.user_id',$student_id)
            ->where('lessons.year_id',$year_id)
            ->where('lesson_days.date',$currentDate)
            ->whereNull('lessons.deleted_at')
            ->where('class_times.start_time','<=',$currentTime)
            ->where('class_times.end_time','>=',$currentTime)
            ->first();

        if ($currentLesson == null){
            $nextLesson = DB::table('lesson_days')
                ->join('lessons','lessons.id','lesson_days.lesson_id')
                ->join('class_times','class_times.id','lessons.class_time_id')
                ->join('letter_group_user','letter_group_user.letter_group_id','lessons.letter_group_id')
                ->where('letter_group_user.user_id',$student_id)
                ->where('lessons.year_id',$year_id)
                ->where('lesson_days.date',$currentDate)
                ->whereNull('lessons.deleted_at')
                ->where('class_times.start_time','>=',$currentTime)
                ->first();

            $nLesson = $nextLesson != null ? Lesson::realData()->find($nextLesson->lesson_id) : $nextLesson;

            $timeUntilStartLesson = $nLesson != null ? Carbon::parse($nLesson->class_time->start_time) : $timeUntilStartLesson;
        }
        else{
            $camera = Room::realData()
                ->join('equipment', 'equipment.room_id', 'rooms.id')
                ->where('equipment.type', 2)
                ->where('rooms.id',$currentLesson->room_id)
                ->select('equipment.*', 'rooms.name', 'equipment.name as e_name')
                ->first();
        }

        $cLesson = $currentLesson != null ? Lesson::realData()->find($currentLesson->lesson_id) : $currentLesson;

        $timeForEndingLesson = -1;
        if (isset($cLesson)){
            $timeForEndingLesson = (
                    Carbon::parse($cLesson->class_time->end_time)->getTimestamp() -
                    Carbon::now()->getTimestamp()
                )*1000;
        }

       // dd($camera);
        $data = [
            'cLesson' => $cLesson,
            'todayLessons' => $todayLessons,
            'nLesson' => $nLesson != null ? $nLesson : 0,
            'camera' => $camera,
            'timeUntilStartLesson' => $timeUntilStartLesson,
            'timeForEndingLesson' => $timeForEndingLesson < 1 ? 1 : $timeForEndingLesson
        ];

        return view('portal.camera.camera',$data);
    }
}
