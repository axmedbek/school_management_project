<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSearchRequest;
use App\Library\Standarts;
use App\Models\MskCities;
use App\Models\MskRegions;
use App\Models\PersonFamily;
use App\Models\PersonLaborActivity;
use App\Models\PersonLanguage;
use App\Models\PersonStudy;
use App\Models\SmsNumber;
use App\Models\StudentComment;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class StudentsController extends Controller
{
    //

    public function student(UserSearchRequest $request)
    {

        $students = User::realData('student')->orderByDesc('id');

        if ($request->get('type', 'active') == 'deactive') $students->onlyTrashed();

        $this->filter($students, $request);


        $data = [
            'students' => $students->paginate(Standarts::$rowCount),
            'request' => $request
        ];

        return view('students.students', $data);
    }

    public function filter($object, $request)
    {
        if ($request->get('name')) $object->where(DB::raw("concat(name,' ',surname,' ',middle_name,' ')"), 'like', '%' . $request->get('name') . '%');
        if ($request->get('birthday')) $object->where('birthday', date("Y-m-d", strtotime($request->get('birthday'))));
        if ($request->get('enroll_date')) $object->where('enroll_date', date("Y-m-d", strtotime($request->get('enroll_date'))));
        if ($request->get('home_tel')) $object->where('home_tel', 'like', '%' . $request->get('home_tel') . '%');
        if ($request->get('mobil_tel')) $object->where('mobil_tel', 'like', '%' . $request->get('mobil_tel') . '%');
        if ($request->get('email')) $object->where('email', 'like', '%' . $request->get('email') . '%');
    }


    public function students_AddEdit_Modal($student)
    {

        $dataParent = [];
        $addressData = [];
        $studentData = User::realData('student')->withTrashed()->find($student);

        if ($student != 0){
            $personFamily = PersonFamily::where('user_id',$student)->get();

            //get address data
            $livedCity = MskCities::realData()->find((int)$studentData['lived_city']);
            $currentCity = MskCities::realData()->find((int)$studentData['current_city']);
            $livedRegion = MskRegions::realData()->find((int)$studentData['lived_region']);
            $currentRegion = MskRegions::realData()->find((int)$studentData['current_region']);


            foreach ($personFamily as $family){
                $user =  User::realData('parent')->find($family['parent_id']);
                $dataParent[] = ['id' => $family['parent_id'],'text' => $user['name']." ".$user['surname']."(Mob:".$user['mobil_tel']." - Ev:".$user['home_tel'].")",'relation' => $family['msk_relation_id']];
            }

            if($livedCity){
                $addressData['lived_city'] = ['id' =>$livedCity['id'] , 'text' => $livedCity['name']];
            }

            if($currentCity){
                $addressData['current_city'] = ['id' =>$currentCity['id'] , 'text' => $currentCity['name'] ];
            }

            if($livedRegion){
                $addressData['lived_region'] = ['id' =>$livedRegion['id'] , 'text' => $livedRegion['name']];
            }

            if($currentRegion){
                $addressData['current_region'] = ['id' =>$currentRegion['id'] , 'text' => $currentRegion['name'] ];
            }
        }

        $data = [
            'student' => $studentData,
            'personFamily' => $dataParent,
            'addressData' => $addressData,
            'id' => $student
        ];

        return view('modals.students.student_add_edit', $data);
    }

    public function student_AddEdit_Action(Request $request, $user)
    {
        $hasError = false;

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'surname' => 'nullable|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'mobil_tel' => 'nullable|string|max:20',
            'home_tel' => 'nullable|string|max:20',
            'enroll_date' => 'nullable|date_format:d-m-Y',
            'birthday' => 'nullable|date_format:d-m-Y',
            'birth_place' => 'nullable|string',
            'email' => 'nullable|email|unique:users,email,'.$user.',id',
            'username' => 'nullable|string|max:30|unique:users,username,'.$user,
            'gender' => 'required|string|in:m,f',
            'city' => 'nullable|string|max:20',
            'address' => 'nullable|string|max:255',
            'current_address' => 'nullable|string|max:255',
            'position' => 'nullable|string|max:60',
            'thumb' => 'nullable|image|max:3000',
            //comment
            'comment' => 'nullable|array',
            'comment.comment' => 'array',
            'comment.comment.*' => 'required|string|max:255',
            'comment.date' => 'array',
            'comment.date.*' => 'required|date_format:"d-m-Y"',
            'comment.author' => 'array',
            'comment.author.*' => 'required|string|max:60',
            //family
            'family' => 'nullable|array',
            'family.relation' => 'array',
            'family.parents' => 'array',
            //sms_number
            'sms_number' => 'nullable|array',
            'sms_number.number' => 'array',
            'sms_number.number.*' => 'string|required|max:20'
        ]);

        function array_has_dupes($array)
        {
            return is_array($array) && count($array) !== count(array_unique($array));
        }

        if ($request->has('sms_number') && array_has_dupes($request->get('sms_number')['number'])) {
            $validator->errors()->add('error','Eyni nömrəni təkrar daxil etməyin');
            $hasError = true;
        }

        if ($hasError || $validator->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($user == 0) {
                $newUser = new User();
            } else {
                $newUser = User::realData('student')->withTrashed()->find($user);
            }
            $newUser->name = $request->get('name');
            $newUser->surname = $request->get('surname');
            $newUser->middle_name = $request->get('middle_name');
            $newUser->mobil_tel = $request->get('mobil_tel');
            $newUser->home_tel = $request->get('home_tel');
          //  $newUser->enroll_date = !empty($request->get('enroll_date')) ? date("Y-m-d", strtotime($request->get('enroll_date'))) : null;
            $newUser->enroll_date = Carbon::today();
            $newUser->birthday = !empty($request->get('birthday')) ? date("Y-m-d", strtotime($request->get('birthday'))) : null;
            $newUser->birth_place = $request->get('birth_place');
            $newUser->email = $request->get('email');
            $newUser->username = $request->get('username');
            $newUser->gender = $request->get('gender');

            // save address data
            $newUser->lived_city = $request->get('lived_city');
            $newUser->lived_region = $request->get('lived_region');
            $newUser->lived_address = $request->get('lived_address');
            $newUser->current_city = $request->get('current_city');
            $newUser->current_region = $request->get('current_region');
            $newUser->current_address = $request->get('current_address');

            $newUser->user_type = "student";
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->position = $request->get('position');
            if ($request->get('password')) {
                $newUser->password = bcrypt($request->get('password'));
            }
            if ($request->hasFile('thumb')) {
                $newUser->clearPic();
                $newName = uniqid() . "." . $request->file('thumb')->extension();

                Image::make($request->file('thumb')->getRealPath())->resize(150, 150)->save(public_path(Standarts::$userThumbir . $newName));
                $request->file('thumb')->move(public_path(Standarts::$userImageDir), $newName);

                $newUser->thumb = $newName;
            }

            $newUser->save();

            //comment
            $newUser->student_comments()->delete();
            if ($request->has('comment'))
                foreach ($request->get('comment')['comment'] as $key => $value) {
                    $newComment = new StudentComment();
                    $newComment->comment = $value;
                    $newComment->date = date("Y-m-d", strtotime($request->get('comment')['date'][$key]));
                    $newComment->author = $request->get('comment')['author'][$key];
                    $newComment->tenant_id = Auth::user()->tenant_id;

                    $newUser->student_comments()->save($newComment);
                }
            //family
            $personFamily = PersonFamily::where('user_id', $newUser['id']);
            if ($personFamily != null) $personFamily->delete();
            if ($request->has('family')){
                foreach ($request->get('family')['relation'] as $key => $value) {
                    $newFamily = new PersonFamily();
                    $newFamily->msk_relation_id = $request->get('family')['relation'][$key];
                    $newFamily->user_id = $newUser->id;
                    $newFamily->parent_id = $request->get('family')['parents'][$key];
                    $newFamily->tenant_id = Auth::user()->tenant_id;
                    $newFamily->save();
                }
            }
            //sms_number
            $newUser->sms_numbers()->delete();
            if ($request->has('sms_number'))
                foreach ($request->get('sms_number')['number'] as $key => $value) {
                    $newNumber = new SmsNumber();
                    $newNumber->number = $value;
                    $newNumber->tenant_id = Auth::user()->tenant_id;

                    $newUser->sms_numbers()->save($newNumber);
                }

            return response()->json(['status' => 'ok']);
        }
    }

    public function student_info_Modal($student)
    {
        $data = [
            'student' => $student = User::realData('student')->withTrashed()->find($student),
            'id' => $student
        ];

        return view('modals.students.student_info', $data);
    }

    public function student_Delete_Action($student)
    {
        $student = User::realData('student')->withTrashed()->find($student);

        if ($student == null) return response()->view('errors.403', [], 403);

        if ($student->deleted_at != null) {
            $student->deleted_at = null;
            $student->save();
        } else $student->delete();

        return redirect()->back();
    }
}
