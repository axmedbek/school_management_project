<?php

namespace App\Http\Controllers;

use App\Models\ClassLetter;
use App\Models\LetterGroup;
use App\Models\MskClass;
use App\Models\Season;
use App\Models\SeasonParagraph;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ClassGroupController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'request' => $request
        ];

        return view('class_groups.class_groups', $data);
    }

    public function classGroupsPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,' . Auth::user()->tenant_id,
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,' . Auth::user()->tenant_id,
            'year_id' => 'required|integer|exists:years,id,tenant_id,' . Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $letters = ClassLetter::realData()
                ->where('msk_class_id', $request->get('class_id'))
                ->where('corpus_id', $request->get('corpus_id'))
                ->where('year_id', $request->get('year_id'))
                ->get();


            return View::make('pages.class_groups.class_groups', ['letters' => $letters, 'class_id'=> $request->get('class_id'), 'corpus_id'=> $request->get('corpus_id'), 'year_id'=> $request->get('year_id') ])->render();
        }
    }

    public function letterAddEditModal(Request $request, $letter)
    {
        $data = [
            'letter' => ClassLetter::realData()->find($letter),
            'class_id' => $request->get('class_id', 0),
            'year_id' => $request->get('year_id', 0),
            'corpus_id' => $request->get('corpus_id', 0),
            'id' => $letter
        ];

        return view('modals.class_groups.letter_add_edit', $data);
    }

    public function letterAddEditAction(Request $request, $letter)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:60',
            'class_id' => 'required|integer|exists:msk_classes,id,tenant_id,'. Auth::user()->tenant_id,
            'year_id' => 'required|integer|exists:years,id,tenant_id,'. Auth::user()->tenant_id,
            'corpus_id' => 'required|integer|exists:corpuses,id,tenant_id,'. Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($letter == 0)
            {
                $newLetter = new ClassLetter();
            }
            else
            {
                $newLetter = ClassLetter::realData()->find($letter);
            }

            $newLetter->name = $request->get('name');
            $newLetter->msk_class_id = $request->get('class_id');
            $newLetter->year_id = $request->get('year_id');
            $newLetter->corpus_id = $request->get('corpus_id');
            $newLetter->tenant_id = Auth::user()->tenant_id;
            $newLetter->save();

            return response()->json(['status'=>'ok' ]);
        }
    }

    public function letterDelete(Request $request)
    {
        $error = false;
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:class_letters,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if(LetterGroup::realData()->where('class_letter_id', $request->get('id'))->first()){
            $error = true;
            $validator->getMessageBag()->add('exists', 'Gruplar bağlı olduğu üçün silmək mümkün deyil!');
        }

        if ($error || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $season = ClassLetter::realData()->find($request->get('id'));
            $season->delete();

            return response()->json(['status'=>'ok']);
        }
    }

    public function letterGroupDelete(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:letter_groups,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $letterGroup = LetterGroup::realData()->find($request->get('id'));
            $letterGroup->delete();

            return response()->json(['status'=>'ok']);
        }
    }

    public function grouphAddEditAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:60',
            'group_type' => 'required|integer|exists:group_types,id,tenant_id,'. Auth::user()->tenant_id,
            'id' => 'required|integer',
            'letter_id' => 'required|integer|exists:class_letters,id,tenant_id,'. Auth::user()->tenant_id,
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($request->get('id') == 0)
            {
                $newGroup = new LetterGroup();
            }
            else
            {
                $newGroup = LetterGroup::realData()->find($request->get('id'));
            }

            $newGroup->name = $request->get('name');
            $newGroup->class_letter_id = $request->get('letter_id');
            $newGroup->group_type_id = $request->get('group_type');
            $newGroup->tenant_id = Auth::user()->tenant_id;
            $newGroup->save();

            return response()->json(['status'=>'ok', 'data'=> array_merge($newGroup->toArray(), ['group_type'=> $newGroup->group_type->name]) ]);
        }
    }

    public function doMainGroup(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'letter_id' => 'required|integer|exists:class_letters,id,tenant_id,'. Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else {

            $letterGroups = LetterGroup::realData()->where('class_letter_id', $request->get('letter_id'))->get();

            foreach ($letterGroups as $letterGroup) {
                $letterGroupObj = LetterGroup::realData()->find($letterGroup->id);
                $letterGroupObj->isMainGroup = 0;
                $letterGroupObj->save();
            }

            if ($request->get('isMainGroup') == 1) {
                $letterGroupObj = LetterGroup::realData()->find($request->get('id'));
                $letterGroupObj->isMainGroup = 1;
                $letterGroupObj->save();
            }

            return response()->json(['status' => 'ok']);
        }
    }
}
