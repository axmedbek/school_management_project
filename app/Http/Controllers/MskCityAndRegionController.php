<?php

namespace App\Http\Controllers;

use App\Models\MskCities;
use App\Models\MskRegions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskCityAndRegionController extends Controller
{
    public function index(Request $request){

        $data = [
            'cities' => MskCities::realData()->orderByDesc('id')->get(),
            'regions' =>  MskRegions::realData()->where('msk_cities_id',1)->get(),
            'request' => $request
        ];
        return view('msk.city_and_region.city_and_region',$data);
    }

    public function cityAddEditAction(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'name' => 'required|string'
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{
            if ($request->get('id') == 0){
                $objCity = new MskCities();
            }
            else{
                $objCity = MskCities::realData()->find($request->get('id'));
            }

            $objCity->name = $request->get('name');
            $objCity->tenant_id = Auth::user()->tenant_id;
            $objCity->save();

            $checkLivedCity = \App\User::realData()->where('lived_city',$objCity->id)->first();
            $checkCurrentCity = \App\User::realData()->where('current_city',$objCity->id)->first();
            if(!isset($checkLivedCity) && !isset($checkCurrentCity)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }

            $objCity["buttons"] = $buttons;
        }

        return response()->json(['status' => 'ok','data' => $objCity->toArray()]);
    }

    public function cityDeleteAction(Request $request){

        $validator = Validator::make($request->all(),[
           'id' => 'required|integer|exists:msk_cities,id'
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            $objCity = MskCities::realData()->find($request->get('id'));
            $objCity->delete();

            return response()->json(['status' => 'ok']);
        }
    }

    public function getRegions(Request $request){
        $validator = Validator::make($request->all(),[
            'city_id' => 'required|integer|exists:msk_cities,id'
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            $regions = MskRegions::realData()
                ->where('msk_cities_id',$request->get('city_id'))
                ->orderByDesc('id')
                ->get();

            foreach ($regions as $region){
                $checkLivedRegion = \App\User::realData()->where('lived_region',$region->id)->first();
                $checkCurrentRegion = \App\User::realData()->where('current_region',$region->id)->first();
                if(!isset($checkLivedRegion) && !isset($checkCurrentRegion)){
                    $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 editRegion"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 deleteRegion"><i class="fa fa-trash-o"></i></a>';
                }
                else{
                    $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 editRegion"><i class="fa fa-pencil"></i></a>';
                }
                $region["buttons"] = $buttons;
            }

            return response()->json(['status' => 'ok','data' => $regions]);
        }
    }

    public function regionAddEditAction(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'city_id' => 'required|integer|exists:msk_cities,id',
            'name' => 'required|string'
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{
            if ($request->get('id') == 0){
                $objRegion = new MskRegions();
            }
            else{
                $objRegion = MskRegions::realData()->find($request->get('id'));
            }

            $objRegion->name = $request->get('name');
            $objRegion->msk_cities_id = $request->get('city_id');
            $objRegion->tenant_id = Auth::user()->tenant_id;
            $objRegion->save();

            $checkLivedRegion = \App\User::realData()->where('lived_region',$objRegion->id)->first();
            $checkCurrentRegion = \App\User::realData()->where('current_region',$objRegion->id)->first();
            if(!isset($checkLivedRegion) && !isset($checkCurrentRegion)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 editRegion"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 deleteRegion"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 editRegion"><i class="fa fa-pencil"></i></a>';
            }

            $objRegion["buttons"] = $buttons;

        }

        return response()->json(['status' => 'ok','data' => $objRegion->toArray()]);
    }


    public function regionDeleteAction(Request $request){

        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:msk_regions,id'
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            $objRegion = MskRegions::realData()->find($request->get('id'));
            $objRegion->delete();

            return response()->json(['status' => 'ok']);
        }
    }
}
