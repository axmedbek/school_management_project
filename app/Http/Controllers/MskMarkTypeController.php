<?php

namespace App\Http\Controllers;

use App\Models\MarkType;
use App\Models\MskMarks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskMarkTypeController extends Controller
{
    //

    public function index(Request $request){

        $mark_types = MarkType::realData()->orderByDesc('id')->get();
        $marks = MskMarks::realData()->where('mark_type_id',1)->get();

        $data = [
            'mark_types'=>$mark_types,
            'marks' => $marks,
            'request' => $request

        ];

        return view('msk.mark_types.mark_types',$data);
    }


    public function mark_type_AddEdit_Action(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:30|unique:mark_types,name,'.$request->get('id').',id',
            'id'=>'required|integer',
        ],[
            'unique' => 'Bu qiymət tipi artıq daxil edilib'
        ]);

        if($validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{
            if($request->get('id')==0){
                $newMarkType = new MarkType();
            }
            else{
                $newMarkType = MarkType::realData()->find($request->get('id'));
            }

            $newMarkType->name = $request->get('name');
            $newMarkType->tenant_id = Auth::user()->tenant_id;
            $newMarkType->save();


            return response()->json(['status'=>'ok','data'=>$newMarkType->toArray()]);
        }


    }

    public function mark_type_Delete_Action(Request $request){

        $validator = Validator::make($request->all(),[
            'id'=>'required|integer|exists:mark_types,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'ok','errors'=>$errors]);
        }
        else{

            $mark = MarkType::realData()->find($request->get('id'));
            $mark->delete();


            return response()->json(['status'=>'ok']);

        }

    }

    public function getMarks(Request $request){
        $validator = Validator::make($request->all(),[
            'mark_type_id' => 'required|integer|exists:mark_types,id'
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            $marks = MskMarks::realData()
                ->where('mark_type_id',$request->get('mark_type_id'))
                ->orderByDesc('id')
                ->get();

            return response()->json(['status' => 'ok','data' => $marks]);
        }
    }

    public function marks_AddEdit_Action(Request $request){
        $hasError = false;

        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'mark_type_id' => 'required|integer|exists:mark_types,id',
            'name' => 'required|string'
        ]);

        $checkNameIsUnique = MskMarks::realData()
            ->where('mark_type_id',$request->get('mark_type_id'))
            ->where('name',$request->get('name'))
            ->where('id','!=',$request->get('id'))
            ->first();

        if ($checkNameIsUnique) {
            $validator->errors()->add('error','Bu qiymət artıq daxil edilib');
            $hasError = true;
        }

        if ($hasError || $validator->fails()){
            $errors = View::make('modals.modal_errors',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{
            if ($request->get('id') == 0){
                $objMark = new MskMarks();
            }
            else{
                $objMark = MskMarks::realData()->find($request->get('id'));
            }

            $objMark->name = $request->get('name');
            $objMark->mark_type_id = $request->get('mark_type_id');
            $objMark->tenant_id = Auth::user()->tenant_id;
            $objMark->save();
        }

        return response()->json(['status' => 'ok','data' => $objMark->toArray()]);
    }


    public function marks_Delete_Action(Request $request){

        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:msk_marks,id'
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            $objMark = MskMarks::realData()->find($request->get('id'));
            $objMark->delete();

            return response()->json(['status' => 'ok']);
        }
    }

}
