<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSearchRequest;
use App\Library\Helper;
use App\Library\Standarts;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    public function user(UserSearchRequest $request)
    {
        $users = User::realData('user')->orderByDesc('id');

        if( $request->get('type', 'active') == 'deactive' ) $users->onlyTrashed();
        $this->filter($users, $request);

        $data = [
            'users' => $users->paginate(Standarts::$rowCount),
            'request' => $request,
        ];

        return view('users.users', $data);
    }

    public function filter($object,$request)
    {
        if($request->get('name')) $object->where(DB::raw("concat(name,' ',surname)"), 'like', '%'.$request->get('name').'%');
        if($request->get('birthday')) $object->where('birthday', date("Y-m-d", strtotime($request->get('birthday'))) );
        if($request->get('enroll_date')) $object->where('enroll_date', date("Y-m-d", strtotime($request->get('enroll_date'))) );
        if($request->get('home_tel')) $object->where('home_tel', 'like', '%'.$request->get('home_tel').'%');
        if($request->get('mobil_tel')) $object->where('mobil_tel', 'like', '%'.$request->get('mobil_tel').'%');
        if($request->get('email')) $object->where('email', 'like', '%'.$request->get('email').'%');
    }

    public function userAddEditModal($user)
    {
        $data = [
            'user' => User::realData('user')->find($user),
            'id' => $user
        ];

        return view('modals.users.users_add_edit', $data);
    }

    public function infoModal($user)
    {
        $data = [
            'user' => User::realData('user')->find($user),
            'id' => $user
        ];

        return view('modals.users.users_info', $data);
    }

    public function userAddEditAction(Request $request, $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'surname' => 'nullable|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'mobil_tel' => 'nullable|string|max:20',
            'home_tel' => 'nullable|string|max:20',
            'enroll_date' => 'nullable|date_format:d-m-Y',
            'birthday' => 'nullable|date_format:d-m-Y',
            'birth_place' => 'nullable|string',
            'email' => 'nullable|email|unique:users,email,'.$user,
            'username' => 'required|string|max:20|unique:users,username,'.$user,
            'gender' => 'required|string|in:m,f',
            'lived_city' => 'integer|nullable|exists:msk_cities,id,tenant_id,'.Auth::user()->tenant_id,
            'lived_region' => 'integer|nullable|exists:msk_regions,id,tenant_id,'.Auth::user()->tenant_id,
            //'address' => 'nullable|string|max:255',
            'current_address' => 'nullable|string|max:255',
            'password' => 'nullable|string|max:20',
            'thumb'    => 'nullable|image|max:3000',
            'group_id' => 'integer|required|exists:groups,id,tenant_id,'.Auth::user()->tenant_id,
        ], [
            'username.unique' => 'Login(İstifadəçi adı) artıq istifadə edilir.'
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($user == 0)
            {
                $newUser = new User();
            }
            else
            {
                $newUser = User::realData('user')->find($user);
            }

            $newUser->name = $request->get('name');
            $newUser->surname = $request->get('surname');
            $newUser->middle_name = $request->get('middle_name');
            $newUser->mobil_tel = $request->get('mobil_tel');
            $newUser->home_tel = $request->get('home_tel');
//            $newUser->enroll_date = !empty($request->get('enroll_date')) ? date("Y-m-d", strtotime($request->get('enroll_date'))) : null;
            $newUser->enroll_date = Carbon::today();
            $newUser->birthday =  !empty($request->get('birthday')) ? date("Y-m-d", strtotime($request->get('birthday'))) : null;
            $newUser->birth_place = $request->get('birth_place');
            $newUser->email = $request->get('email');
            $newUser->username = $request->get('username');
            $newUser->gender = $request->get('gender');
            $newUser->lived_city = $request->get('lived_city');
            $newUser->lived_region = $request->get('lived_region');
            $newUser->user_type = "user";
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->group_id = $request->get('group_id');
            $newUser->current_address = $request->get('current_address');
            if($request->has('password')) $newUser->password = bcrypt($request->get('password'));

            if($request->hasFile('thumb'))
            {
                //for folder
                //Helper::filePermision(public_path(Standarts::$userImageDir), true);

                $newUser->clearPic();
                $newName = uniqid() . "." . $request->file('thumb')->extension();

                Image::make($request->file('thumb')->getRealPath())->resize(150, 150)->save( public_path(Standarts::$userThumbir . $newName) );
                $request->file('thumb')->move(public_path(Standarts::$userImageDir), $newName);

                $newUser->thumb = $newName;
            }

            $newUser->save();

            return response()->json(['status'=>'ok']);
        }
    }

    public function userDelete($user)
    {
        $user = User::realData('user')->withTrashed()->find($user);

        if($user == null) return response()->view("errors.403",[],403);

        if ($user->deleted_at != null){
            $user->deleted_at = null;
            $user->save();
        }

        else $user->delete();

        return redirect()->back();
    }
}
