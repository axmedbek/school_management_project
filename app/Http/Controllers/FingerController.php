<?php

namespace App\Http\Controllers;

use App\Library\YearDays;
use App\Models\Duty;
use App\Models\Year;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

class FingerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lastYearId = (int)Year::realData()->orderBy('id', 'DESC')->first()['id'];

        return view('finger.finger', ['lastYearId' => $lastYearId]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:users,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $lastYearId = (int)Year::realData()->orderBy('id', 'DESC')->first()['id'];

            $user = User::leftJoin('letter_group_user', function ($join) use($lastYearId) {
                    $join->on('users.id', '=', 'letter_group_user.user_id')->on('letter_group_user.year_id', DB::raw($lastYearId));
                })
                ->leftJoin('letter_groups', 'letter_groups.id', '=', 'letter_group_user.letter_group_id')
                ->leftJoin('class_letters', 'class_letters.id', '=', 'letter_groups.class_letter_id')
                ->leftJoin('msk_classes', 'msk_classes.id', '=', 'class_letters.msk_class_id')
                ->select('users.*', DB::raw("CONCAT(msk_classes.name, class_letters.name) as class_name"))
                ->find($request->get('id'));

            //dd($user->toArray());

            $fingesView = View::make('finger.register', ['user'=> $user])->render();
            $userView = View::make('finger.user', ['user'=> $user])->render();

            return response()->json(['status'=>'ok', 'userView'=> $userView, 'fingesView'=> $fingesView]);
        }
    }
}
