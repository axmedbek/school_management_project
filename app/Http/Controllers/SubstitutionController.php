<?php

namespace App\Http\Controllers;

use App\Library\Standarts;
use App\Models\Lesson;
use App\Models\LetterGroup;
use App\Models\Subject;
use App\Models\Substitution;
use App\Models\SubstitutionLesson;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use JavaScript;

class SubstitutionController extends Controller
{
    public function index(Request $request)
    {
        $subs = Substitution::realData()->orderByDesc('id');

        $data = [
            'subs' => $subs->paginate(Standarts::$rowCount),
            'request' => $request
        ];

        return view('substitutions.substitutions', $data);
    } //get all substitutions

    public function getSubstitutionData(Request $request)
    {


        // subjects
        if ($request->get('ne') == 'get_subjects') {


            $validator = Validator::make($request->all(), [
                'teacher' => 'required|integer|exists:users,id',
                'sub_date' => 'required|date'
            ]);

            if ($validator->fails()) {
                $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

                return response()->json(['status' => 'error', 'errors' => $errors]);
            } else {

                $subDate = new \DateTime($request->get('sub_date'));
                $sub_date_week = $subDate->format('w');
                $teacher = $request->get('teacher');


                $subjects = DB::table('lessons')
                    ->join('subjects', 'lessons.subject_id', '=', 'subjects.id')
                    ->join('years', 'lessons.year_id', '=', 'years.id')
                    ->where('years.start_date', '<=', $subDate)
                    ->where('years.end_date', '>=', $subDate)
                    ->where('lessons.week_day', $sub_date_week)
                    ->where('lessons.user_id', $teacher)
                    ->where('lessons.tenant_id', Auth::user()->tenant_id)
                    ->get(['lessons.id as id', 'subjects.name as name']);

                $results = [];
                foreach ($subjects as $subject)
                    $lesson = Lesson::realData()->find($subject->id);
                    //$class  =  LetterGroup::realData()->find($lesson->letter_group_id)->class_letter;
                     $class = $lesson->letter_group->class_letter;
                    $className = $class->msk_class->name."".$class->name;
                    $lessonStartTime = (new DateTime($lesson->class_time->start_time))->format('H:i');
                    $lessonEndTime = (new DateTime($lesson->class_time->end_time))->format('H:i');
                    $lessonTime = "(".$lessonStartTime."-".$lessonEndTime.")";
                    $lessonInfo = $lesson->room->corpus->name." ".$className." ".$subject->name." ".$lessonTime;
                    $results[] = [
                        'subjectId' => $subject->id,
                        'subjectName' => $lessonInfo,
                    ];
            }

        } //evez eden teachers
        else if ($request->get('ne') == "get_sub_teachers") {

            $validator = Validator::make($request->all(), [
                'lesson_id' => 'required|integer|exists:lessons,id',
                'teacher' => 'required|integer|exists:users,id',
                'sub_date' => 'required|date'
            ]);

            if ($validator->fails()) {
                $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

                return response()->json(['status' => 'error', 'errors' => $errors]);
            } else {

                $subDate = new \DateTime($request->get('sub_date'));
                $sub_date_week = $subDate->format('w');
                $teacher = $request->get('teacher');
                $lesson_id = intval($request->get('lesson_id'));
                $lesson = Lesson::realData()->find($lesson_id);

                //dd($lesson_id);

                $subject_id = intval($lesson->subject_id);
                $class_time_id = intval($lesson->class_time_id);

                $allteach = DB::table((DB::raw("(SELECT
                    [users].[id] AS [id],
                    [users].[name] AS [name],
                    [users].[surname] AS [surname],
                    [users].[middle_name] AS [middle_name],
                    (SELECT class_time_id FROM lessons WHERE id = [substitution_lessons].[lesson_id] ) as class_time
                FROM
                    [users]
                    LEFT JOIN [lessons] ON [users].[id] = [lessons].[user_id]
                    LEFT JOIN [subject_user] ON [users].[id] = [subject_user].[user_id]
                    LEFT JOIN [years] ON [lessons].[year_id] = [years].[id]
                    LEFT JOIN [substitution_lessons] ON [substitution_lessons].[user_id] = [users].[id]
                WHERE
                    [users].[user_type] = 'teacher' 
                    AND [users].[id] != ".$teacher." 
                    AND [subject_user].[subject_id] = ".$subject_id."
                    AND [lessons].[week_day] = ".$sub_date_week."
                    AND [lessons].[class_time_id] != ".$class_time_id."
                    AND [users].[tenant_id] = ".Auth::user()->tenant_id." 
                    AND [lessons].[deleted_at] IS NULL
                    UNION
                SELECT
                    [users].[id] AS [id],
                    [users].[name] AS [name],
                    [users].[surname] AS [surname],
                    [users].[middle_name] AS [middle_name],
                    (SELECT class_time_id FROM lessons WHERE id = [substitution_lessons].[lesson_id] ) as class_time
                FROM
                    [users]
                    LEFT JOIN [lessons] ON [users].[id] = [lessons].[user_id] 
                    LEFT JOIN [subject_user] ON [users].[id] = [subject_user].[user_id]
                    LEFT JOIN [years] ON [lessons].[year_id] = [years].[id]	
                    LEFT JOIN [substitution_lessons] ON [substitution_lessons].[user_id] = [users].[id]
                WHERE
                    [users].[user_type] = 'teacher'
                    AND [subject_user].[subject_id] = ".$subject_id." 
                    AND [lessons].[user_id] IS NULL 
                    AND [users].[tenant_id] = ".Auth::user()->tenant_id."
                    AND [lessons].[deleted_at] IS NULL) as dd")))
                    ->where('class_time','!=',$class_time_id)
                    ->orWhereNull('class_time')
                    ->get();



//                $teacherEmpty = DB::table('users')
//                    ->leftJoin('lessons', 'users.id', '=', 'lessons.user_id')
//                    ->leftJoin('subject_user', 'users.id', '=', 'subject_user.user_id')
//                    ->leftJoin('years', 'lessons.year_id', '=', 'years.id')
//                    ->where('users.user_type', '=', 'teacher')
//                    ->where('subject_user.subject_id', $subject_id)
//                    ->whereNull('lessons.user_id')
//                    ->where('users.tenant_id', Auth::user()->tenant_id)
//                    ->select('users.id as id', 'users.name as name','users.surname as surname','users.middle_name as middle_name');
//
//                $teachersFull = DB::table('users')
//                    ->leftJoin('lessons', 'users.id', '=', 'lessons.user_id')
//                    ->leftJoin('subject_user', 'users.id', '=', 'subject_user.user_id')
//                    ->leftJoin('years', 'lessons.year_id', '=', 'years.id')
//                    ->where('users.user_type', '=', 'teacher')
//                    ->where('users.id', '!=', $teacher)
//                    ->where('subject_user.subject_id', $subject_id)
//                    ->where('lessons.week_day', $sub_date_week)
//                    ->where('lessons.class_time_id', '!=', $class_time_id)
//                    ->where('users.tenant_id', Auth::user()->tenant_id)
//                    ->union($teacherEmpty)->select('users.id as id', 'users.name as name','users.surname as surname','users.middle_name as middle_name')
//                    ->get();

                $results = [];
                foreach ($allteach as $teacher)
                    $results[] = ['id' => $teacher->id, 'text' => $teacher->name." ".$teacher->surname." ".$teacher->middle_name];
            }
        } // //evez edilen teachers
        else if ($request->get('ne') == 'get_teachers') {

            $validator = Validator::make($request->all(), [
                'sub_date' => 'required|date'
            ]);

            if ($validator->fails()) {
                $errors = View::make('modals.modal_errors', ['errors' => $validator->errors()])->render();

                return response()->json(['status' => 'error', 'errors' => $errors]);
            } else {

                $subDate = new \DateTime($request->get('sub_date'));
                $sub_date_week = $subDate->format('w');


                $teachers = DB::table('users')
                    ->join('lessons', 'users.id', '=', 'lessons.user_id')
                    ->join('years', 'lessons.year_id', '=', 'years.id')
                    ->where('years.start_date', '<=', $subDate)
                    ->where('years.end_date', '>=', $subDate)
                    ->where('lessons.week_day', $sub_date_week)
                    ->where('lessons.tenant_id', Auth::user()->tenant_id)
                    ->select('users.id as id', 'users.name as name','users.surname as surname','users.middle_name as middle_name')
                    ->distinct()
                    //->groupBy('users.id')
                    ->get();

                //dd(DB::getQueryLog());


                /* $lessons = Lesson::realData()
                     ->where('week_day', $sub_date_week)
                     ->get();*/

                $results = [];
                foreach ($teachers as $teacher)
                    $results[] = ['id' => $teacher->id, 'text' => $teacher->name." ".$teacher->surname." ".$teacher->middle_name];
            }
        }

        return response()->json(['status' => 'ok', "results" => $results]);

    } // substitution's filters

    public function addEditModal($sub)
    {
        $substitution = Substitution::realData()->find($sub);

        $data = [
            'sub' => $substitution,
            'id' => $sub
        ];

        if ($sub != 0) {

            $dataEvezedenAndLessons = [];
            foreach ( $substitution->substitutionLessons as $sublessons){
                $dataEvezedenAndLesson = [];
                $dataEvezedenAndLesson['lessonId'] = $sublessons->lesson->subject->id;
                $dataEvezedenAndLesson['lessonName'] = $sublessons->lesson->subject->name;
                $dataEvezedenAndLesson['evezEdenTeacherId'] = $sublessons->user->id;
                $dataEvezedenAndLesson['evezEdenTeacherName']=$sublessons->user->name;
                $dataEvezedenAndLessons[] = $dataEvezedenAndLesson;
            }

            $dataJS = [
                'teacherId' => $substitution->user->id,
                'teacherName' => $substitution->user->name,
                'dataEvezedenAndLessons' => $dataEvezedenAndLessons,
            ];

            JavaScript::put([
                'data' => $dataJS
            ]);
        } else {
            JavaScript::put([
                'data' => ''
            ]);
        }

        return view('modals.substitutions.sub_add_edit', $data);
    } // substitution's add or edit modal

    public function infoModal($sub)
    {

        $data = [
            'sub' => Substitution::realData()->find($sub),
            'id' => $sub
        ];

        return view('modals.substitutions.sub_info', $data);
    } // substitution detail information modal

    public function addEditAction(Request $request, $sub)
    {

        $validators = Validator::make($request->all(), [
            'sub_date' => 'required|date',
            'subs' => 'required|array',
            'teacher' => 'required|integer|exists:users,id'
        ]);

        if ($validators->fails()) {
            $errors = View::make('modals.modal_errors', ['errors' => $validators->errors()])->render();

            return response()->json(['status' => 'error', 'errors' => $errors]);
        } else {
            if ($sub == 0) {
                $substitution = new Substitution();
            } else {
                $substitution = Substitution::realData()->find($sub);

                foreach ($substitution->substitutionLessons as $sublessons){
                    $sublessons->delete();
                }
            }


            $substitution->date = date('Y-m-d', strtotime($request->get('sub_date')));
            $substitution->tenant_id = Auth::user()->tenant_id;
            $substitution->user_id = $request->get('teacher');
            $substitution->save();


            foreach ($request->get('subs') as $sub) {
                $substitutionLessons = new SubstitutionLesson();
                $substitutionLessons->lesson_id = $sub[1];
                $substitutionLessons->user_id = $sub[2];
                $substitutionLessons->substitution_id = $substitution->id;
                $substitutionLessons->save();
            }


            return response()->json(['status' => 'ok']);
        }
    } // add or update substitution

    public function Delete(Request $request, $sub)
    {
        $substitution = Substitution::realData()->find($sub);

        $substitution->delete();

        return redirect()->back();

    } // delete substitution
}
