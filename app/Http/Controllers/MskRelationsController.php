<?php

namespace App\Http\Controllers;

use App\Models\MskRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskRelationsController extends Controller
{

    public function index(Request $request){

        $relations = MskRelation::realData()->orderByDesc('id')->get();

        $data = [
            'relations' => $relations,
            'request' => $request
        ];

        return view('msk.relations.relations',$data);
    }



    public function relations_AddEdit_Action(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:30',
            'id' => 'required|integer',
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modals_error',['errors'=>$validator->errors()])->render();
            return response()->json(['status'=>'error','errors'=>$errors]);
        }
        else{

            if($request->get('id')==0){
                $relations = new MskRelation();
            }
            else{
                $relations = MskRelation::realData()->find($request->get('id'));
            }

            $relations->name = $request->get('name');
            $relations->tenant_id = Auth::user()->tenant_id;
            $relations->save();


            return response()->json(['status'=>'ok','data'=>$relations->toArray()]);

        }
    }

    public function relations_Delete_Action(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:msk_relations,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);

        }
        else{

            $relations = MskRelation::realData()->find($request->get('id'));
            $relations->delete();


            return response()->json(['status'=>'ok']);


        }
    }
}
