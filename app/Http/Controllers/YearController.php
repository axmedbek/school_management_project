<?php

namespace App\Http\Controllers;

use App\Library\LessonDaysFixer;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\ClassLetter;
use App\Models\Corpus;
use App\Models\Duty;
use App\Models\GroupType;
use App\Models\Holiday;
use App\Models\LetterGroup;
use App\Models\Year;
use App\Models\YearPeriod;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class YearController extends Controller
{
    public function index(Request $request)
    {
        $years = Year::realData()
            ->orderBy('id', 'DESC')
            ->get();

        $data = [
            'years' => $years,
            'request' => $request
        ];

        return view('years.years', $data);
    }

    public function yearAddEditModal($year)
    {
        $data = [
            'year' => Year::realData()->find($year),
            'id' => $year
        ];

        return view('modals.years.years_add_edit', $data);
    }

    public function yearAddEditAction(Request $request, $year)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date_format:"d-m-Y"',
            'end_date' => 'required|date_format:"d-m-Y"',
            //period
            'period'   => 'required|array',
            'period.name'   => 'array|required',
            'period.name.*' => 'required|string|max:255',
            'period.start_date'   => 'array|required',
            'period.start_date.*'   => 'required|date_format:"d-m-Y"',
            'period.end_date'   => 'array|required',
            'period.end_date.*'   => 'required|date_format:"d-m-Y"',
        ]);

        $error = false;
        $yStart = strtotime($request->get('start_date'));
        $yEnd = strtotime($request->get('end_date'));

        //check start_date and end_date exists in db
        $firstDay = date("Y-m-d", $yStart);
        $lastDay = date("Y-m-d", $yEnd);
        $checkYear = Year::realData()->where('id', '!=', DB::raw((int)$year))->whereRaw("( (start_date <= '{$lastDay}' AND end_date >= '{$firstDay}') OR (start_date >= '{$lastDay}' AND (end_date <= '{$firstDay}' OR end_date is null)))")->first();
        if (!empty($checkYear)){
            $validator->errors()->add('_','Bu tarixlərdə dərs ili daxil edilib');
            $error = true;
        }
        //end_date must be bigger than start_date
        if($yStart >= $yEnd){
            $validator->errors()->add('_','Tarixler duz deyil');
            $error = true;
        }

        $periods = [];
        if($request->has('period'))
            foreach ($request->get('period')['name'] as $key => $value)
            {
                $start = strtotime($request->get('period')['start_date'][$key]);
                $end = strtotime($request->get('period')['end_date'][$key]);
                if($start >= $end){
                    $validator->errors()->add('_','Period duz deyil');
                    $error = true;
                    break;
                }
                foreach ($periods as $period){
                    if(  ($period['end'] < $start || $period['start'] > $end) == false
                        || ( $yStart <= $start && $yEnd >= $end) == false
                    ){
                        $validator->errors()->add('_','Period duz deyil');
                        $error = true;
                        break(2);
                    }
                }

                $periods[] = ['start'=> $start, 'end'=> $end];
            }


        if ($error || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            if($year == 0)
            {
                $newYear = new Year();
            }
            else
            {
                $newYear = Year::realData()->find($year);
            }

            $newYear->start_date = date("Y-m-d", strtotime($request->get('start_date')));
            $newYear->end_date = date("Y-m-d", strtotime($request->get('end_date')));
            $newYear->tenant_id = Auth::user()->tenant_id;
            $newYear->save();

            //period
            $newYear->periods()->delete();
            if($request->has('period'))

                foreach ($request->get('period')['name'] as $key => $value)
                {
                    $newPeriod = new YearPeriod();
                    $newPeriod->name = $value;
                    $newPeriod->start_date = date("Y-m-d", strtotime($request->get('period')['start_date'][$key]));
                    $newPeriod->end_date = date("Y-m-d", strtotime($request->get('period')['end_date'][$key]));
                    $newPeriod->tenant_id = Auth::user()->tenant_id;

                    $newYear->periods()->save($newPeriod);
                }

            $newYear->save();

            // fixer
            (new YearDays($newYear))->calculateDays();
            (new LessonDaysFixer($newYear->id))->fixYearDays();
            (new LessonDaysFixer($newYear->id))->fixLessonDays();

            return response()->json(['status'=>'ok']);
        }
    }

    public function infoModal($corpus)
    {
        $data = [
            'corpus' => Corpus::realData()->find($corpus),
            'id' => $corpus
        ];

        return view('modals.corpuses.corpuses_info', $data);
    }

    public function doYearDeActive(Request $request){
       $validator  = Validator::make($request->all(),[
            'id' => 'required|integer|exists:years,id',
            'active' => 'required|boolean'
        ]);

        if ($validator->fails())
        {
            return response()->json(['status' => 'error']);
        }

       $year = Year::realData()->find($request->get('id'));
       if($year == null )  return response()->json(['status' => 'error']);
       $year->active = $request->get('active');
       $year->save();

       return response()->json(['status' => 'ok','active'=>$year->active]);


    }

    public function yearDelete($year){

        $classLetter = ClassLetter::realData()->where('year_id',$year)->first();
        $holiday = Holiday::realData()->where('year_id',$year)->first();

        if($classLetter != null || $holiday !=null){
            $errorMessage = ['errorMessage' => 'Məlumat silinmədi!.Bu ilə bağlı məlumatlar var'];
            return redirect()->back()->with($errorMessage);
        }

        $year = Year::realData()->find($year);

        if($year == null) return response()->view('errors.403',[],403);

        $year->delete();

        return redirect()->back();
    }

    # wizard

    public function wizard(Request $request)
    {
        $lastYear = Year::realData()->orderBy('id', 'DESC')->first();
        $corpuses = \App\Models\Corpus::realData()->orderBy('id')->get();
        $classes = \App\Models\MskClass::realData()->orderBy('order')->get();
        $groupTypes = GroupType::realData()->get();

        $classOrder = [];
        foreach ($classes as $key => $class){
            $classOrder[$class->id] = isset($classes[$key + 1])? $classes[$key + 1]->id : 0;
        }

        $allLetterGroups = [];
        $corpusLetters = [];
        foreach (ClassLetter::realData()->where('year_id', (int)$lastYear['id'])->get() as $letter){
            if(!isset($allLetterGroups[$letter->corpus_id])) $allLetterGroups[$letter->corpus_id] = [];
            if(!isset($allLetterGroups[$letter->corpus_id][ $classOrder[$letter->msk_class_id] ])) $allLetterGroups[$letter->corpus_id][ $classOrder[$letter->msk_class_id] ] = [];

            $allLetterGroups[$letter->corpus_id][ $classOrder[$letter->msk_class_id] ][] = $letter;

            $keyy = $letter->corpus->name . " " . $letter->msk_class->name . $letter->name;
            $corpusLetters[$keyy] = $letter;
        }
//dd(json_decode(Storage::get('wizard'),true));
        $data = [
            'request' => $request,
            'lastYear' => $lastYear,
            'corpuses' => $corpuses,
            'classes' => $classes,
            'allLetterGroups' => $allLetterGroups,
            'groupTypes' => $groupTypes,
            'corpusLetters' => $corpusLetters,
            'saved' => (Storage::exists('wizard') && is_array(json_decode(Storage::get('wizard'),true)) ? json_decode(Storage::get('wizard'),true) : false),
        ];

        return view('years.wizard', $data);
    }

    public function wizardAddAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date_format:"d-m-Y"',
            'end_date' => 'required|date_format:"d-m-Y"',
            //period
            'period'   => 'required|array',
            'period.*'   => 'required|array',
            'period.*.name' => 'required|string|max:255',
            'period.*.start_date'   => 'required|date_format:"d-m-Y"',
            'period.*.end_date'   => 'required|date_format:"d-m-Y"',
            //deleted teacher
            'deleted_teachers'   => 'nullable|array',
            'deleted_teachers.*'   => 'integer|exists:users,id,tenant_id,'.Auth::user()->tenant_id,
            //deleted students
            'deleted_students'   => 'nullable|array',
            'deleted_students.*'   => 'integer|exists:users,id,tenant_id,'.Auth::user()->tenant_id,
            //classes
            'classes' => 'required|string',
            //notfication
            'program_chk' => 'required|integer|in:0,1',
            'class_times_chk' => 'required|integer|in:0,1',
            'non_working_days_chk' => 'required|integer|in:0,1',
            'program_text' => 'nullable|string|max:255',
            'class_times_text' => 'nullable|string|max:255',
            'non_working_days_text' => 'nullable|string|max:255',
        ]);

        $error = false;
        $yStart = strtotime($request->get('start_date'));
        $yEnd = strtotime($request->get('end_date'));
        if($yStart >= $yEnd){
            $validator->errors()->add('_','Tarixler duz deyil');
            $error = true;
        }

        $periods = [];
        if($request->has('period'))
            foreach ($request->get('period') as $key => $value)
            {
                $start = strtotime($value['start_date']);
                $end = strtotime($value['end_date']);
                if($start >= $end){
                    $validator->errors()->add('_','Period duz deyil');
                    $error = true;
                    break;
                }
                foreach ($periods as $period){
                    if(  ($period['end'] < $start || $period['start'] > $end) == false
                        || ( $yStart <= $start && $yEnd >= $end) == false
                    ){
                        $validator->errors()->add('_','Period duz deyil');
                        $error = true;
                        break(2);
                    }
                }

                $periods[] = ['start'=> $start, 'end'=> $end];
            }


        if ($error || $validator->fails())
        {
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else
        {
            $lastYear = Year::realData()->orderBy('id', 'DESC')->first();

            $newYear = new Year();
            $newYear->start_date = date("Y-m-d", strtotime($request->get('start_date')));
            $newYear->end_date = date("Y-m-d", strtotime($request->get('end_date')));
            $newYear->tenant_id = Auth::user()->tenant_id;
            $newYear->save();

            //period
            if($request->has('period'))
                foreach ($request->get('period') as $key => $value)
                {
                    $newPeriod = new YearPeriod();
                    $newPeriod->name = $value['name'];
                    $newPeriod->start_date = date("Y-m-d", strtotime($value['start_date']));
                    $newPeriod->end_date = date("Y-m-d", strtotime($value['end_date']));
                    $newPeriod->tenant_id = Auth::user()->tenant_id;

                    $newYear->periods()->save($newPeriod);
                }

             //holidays
             foreach ($lastYear->holidays as $holiday){
                 $newHoliday = new Holiday();
                 $newHoliday->name = $holiday->name;
                 $newHoliday->tenant_id = $holiday->tenant_id;
                 $newHoliday->type = $holiday->type;
                 $newHoliday->start_date = $holiday->start_date;
                 $newHoliday->end_date = $holiday->end_date;
                 $newYear->holidays()->save($newHoliday);
             }

            $newYear->save();

            (new YearDays($newYear))->calculateDays();

            if($request->has('deleted_teachers'))
                User::realData('teacher')->whereIn('id', $request->get('deleted_teachers'))->delete();

            if($request->has('deleted_students'))
                User::realData('student')->whereIn('id', $request->get('deleted_students'))->delete();

            //classes
            $classes = @json_decode($request->get('classes', '[]'));

            foreach ($classes as $corpusId => $class)
                foreach ($class as $classId => $letters)
                    foreach ($letters as $letter => $groups){

                        $newLetter = new ClassLetter();
                        $newLetter->tenant_id = Auth::user()->tenant_id;
                        $newLetter->year_id = $newYear->id;
                        $newLetter->corpus_id = $corpusId;
                        $newLetter->msk_class_id = $classId;
                        $newLetter->name = $letter;
                        $newLetter->save();

                        foreach ($groups as $group){
                            $newGroup = new LetterGroup();
                            $newGroup->tenant_id = Auth::user()->tenant_id;
                            $newGroup->group_type_id = $group->group_type;
                            $newGroup->isMainGroup  = $group->isMainGroup;
                            $newGroup->name = $group->group_name;
                            $newLetter->groups()->save($newGroup);

                            $newGroup->students()->attach($group->students, ['year_id' => $newYear->id, 'tenant_id' => Auth::user()->tenant_id]);
                        }
                    }

            //notfication
            if($request->get('program_chk', 0) == 1){
                $newDuty = new Duty();
                $newDuty->tenant_id = Auth::user()->tenant_id;
                $newDuty->type = "year";
                $newDuty->text = "Yeni dərs ilinin programinda dəyişiklik";
                $newDuty->comment = $request->get('program_text');
                $newDuty->save();
            }
            if($request->get('class_times_chk', 0) == 1){
                $newDuty = new Duty();
                $newDuty->tenant_id = Auth::user()->tenant_id;
                $newDuty->type = "year";
                $newDuty->text = "Yeni dərs ilinin dərs saatlarında dəyişiklik!";
                $newDuty->comment = $request->get('class_times_text');
                $newDuty->save();
            }
            if($request->get('non_working_days_chk', 0) == 1){
                $newDuty = new Duty();
                $newDuty->tenant_id = Auth::user()->tenant_id;
                $newDuty->type = "year";
                $newDuty->text = "Yeni dərs ilinin qeyri iş günlərində dəyişiklik!";
                $newDuty->comment = $request->get('non_working_days_text');
                $newDuty->save();
            }

            // fixer
            (new YearDays($newYear))->calculateDays();
            (new LessonDaysFixer($newYear->id))->fixYearDays();

            Storage::delete('wizard');

            return response()->json(['status'=>'ok']);
        }
    }

    public function wizardSaveAction(Request $request)
    {
        $periods = $request->get('period');
        foreach ($periods as $period){
            $startDate = strtotime($period['start_date']);
            $endDate = strtotime($period['end_date']);
            if ($startDate > $endDate){
                return response()->json(['status'=>'error','errors'=>'Başlanğıc tarixi bitmə tarixindən böyük ola bilməz']);
            }
        }

        Storage::put('wizard', json_encode($request->all()));
        return response()->json(['status'=>'ok']);

    }
}
