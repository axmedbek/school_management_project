<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Library\Standarts;
use App\Library\YearDays;
use App\Models\AdvertTask;
use App\Models\AdvertTaskUser;
use App\Models\Notification;
use App\Models\PersonFamily;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AdvertTaskController extends Controller
{
    public function index()
    {
        $tasks = AdvertTask::realData()->get();
        return view('advert_task.advert_task', compact('tasks'));
    }

    public function addAdvertTaskModal($task_id){

        $advertTaskObj = AdvertTask::realData()->find($task_id);
        $currentYear = YearDays::getCurrentYear(false, false, date("Y-m-d"));

        $students = User::realData()
            ->leftJoin('letter_group_user', function ($join) use($currentYear){
                $join->on('letter_group_user.user_id', 'users.id')->on('letter_group_user.year_id', DB::raw((int)$currentYear['id']));
            })
            ->leftJoin('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
            ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->where('user_type', 'student')
            ->select('users.*', 'class_letters.name as letter_name', 'msk_classes.name as class_name')
            ->get();

        $studentGroups = [];
        foreach ($students as $student){
            $cName = $student['class_name'] . $student['letter_name'];
            if(trim($cName) === "") $cName = "Qrupsuz";

            if(!isset($studentGroups[$cName])) $studentGroups[$cName] = [];

            $studentGroups[$cName][] = $student;
        }



        $parents = User::realData()
            ->leftJoin('person_families', 'person_families.parent_id', 'users.id')
            ->leftJoin('letter_group_user', function ($join) use($currentYear){
                $join->on('letter_group_user.user_id', 'person_families.user_id')->on('letter_group_user.year_id', DB::raw((int)$currentYear['id']));
            })
            ->leftJoin('letter_groups', 'letter_groups.id', 'letter_group_user.letter_group_id')
            ->leftJoin('class_letters', 'class_letters.id', 'letter_groups.class_letter_id')
            ->leftJoin('msk_classes', 'msk_classes.id', 'class_letters.msk_class_id')
            ->where('user_type', 'parent')
            ->select('users.*', 'class_letters.name as letter_name', 'msk_classes.name as class_name')
            ->get();

        $parentsGroups = [];
        foreach ($parents as $parent){
            $cName = $parent['class_name'] . $parent['letter_name'];
            if(trim($cName) === "") $cName = "Qrupsuz";

            if(!isset($parentsGroups[$cName])) $parentsGroups[$cName] = [];
            if(!isset($parentsGroups[$cName][$parent->id])) $parentsGroups[$cName][$parent->id] = [];
            $parentsGroups[$cName][$parent->id] = $parent;
        }

        $permitedUsersId = [];
        if($advertTaskObj !== null){
            $permitedUsers = $advertTaskObj->permitedUsers;
            foreach ($permitedUsers as $permitedUser){
                $permitedUsersId[$permitedUser->user_id] = true;
            }
        }


        $data = [
            'task' => $advertTaskObj,
            'task_id' => $task_id,
            'studentGroups' => $studentGroups,
            'parentsGroups' => $parentsGroups,
            'permitedUsersId' => $permitedUsersId
        ];

        return view('modals.advert_task.advert_task_add',$data);
    }

    public function saveAction(Request $request,$task_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'date_of_show' => 'required|date',
            'date_of_event' => 'nullable|date',
            'range' => 'required|string',
            'content' => 'required',
            'teachers' => 'nullable|array',
            'teachers.*' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id,
            'parents' => 'nullable|array',
            'parents.*' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id,
            'students' => 'nullable|array',
            'students.*' => 'required|integer|exists:users,id,tenant_id,' . auth()->user()->tenant_id,
        ]);

        if ($validator->fails()) {

            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();
            return response()->json(['status'=>'error', 'errors' => $errors]);

        } else {
            if ($task_id == 0){
                $advertTaskObj = new AdvertTask();
            }
            else{
                $advertTaskObj = AdvertTask::realData()->find($task_id);
            }
            $advertTaskObj->date_of_show = date('Y-m-d',strtotime($request->get('date_of_show')));
            $advertTaskObj->name = $request->get('name');
            $advertTaskObj->range = $request->get('range');
            $advertTaskObj->content = $request->get('content');
            $advertTaskObj->adding_user = auth()->user()->id;
            $advertTaskObj->link = $request->get('link');
            $advertTaskObj->date_of_event = $request->get('date_of_event') ? date('Y-m-d',strtotime($request->get('date_of_event'))) : NULL;
            $advertTaskObj->tenant_id = auth()->user()->tenant_id;

            if ($request->hasFile('file')){
                $destinationPath = public_path(Standarts::$optionFilesDir);
                $reqFile = $request->file('file');
                $fileName = time()."_".$reqFile->getClientOriginalName();
                $reqFile->move($destinationPath,$fileName);
                $advertTaskObj->file = $fileName;
            }

            $advertTaskObj->save();

            $advertTaskObj->permitedUsers()->delete();
            if($request->has('teachers'))
            foreach ($request->get('teachers') as $teacherId){
                $permitedUser = new AdvertTaskUser();
                $permitedUser->tenant_id = auth()->user()->tenant_id;
                $permitedUser->user_id = $teacherId;

                $advertTaskObj->permitedUsers()->save($permitedUser);

                $notifyObj = new Notification();
                $notifyObj->user_id = $teacherId;
                $notifyObj->content = strlen($request->get('content')) > 25 ? substr($request->get('content'),0,25)."..." : $request->get('content');
                $notifyObj->icon = "fa fa-calendar-plus-o";
                $notifyObj->color = "#69ca9d";
                $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                $notifyObj->route = route('portal_dashboard');
                $notifyObj->save();

//                event(new NotificationEvent($notifyObj));
            }
            if($request->has('parents'))
            foreach ($request->get('parents') as $teacherId){
                $permitedUser = new AdvertTaskUser();
                $permitedUser->tenant_id = auth()->user()->tenant_id;
                $permitedUser->user_id = $teacherId;

                $advertTaskObj->permitedUsers()->save($permitedUser);

                $notifyObj = new Notification();
                $notifyObj->user_id = $teacherId;
                $notifyObj->content = strlen($request->get('content')) > 25 ? substr($request->get('content'),0,25)."..." : $request->get('content');
                $notifyObj->icon = "fa fa-calendar-plus-o";
                $notifyObj->color = "#69ca9d";
                $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                $notifyObj->route = route('portal_dashboard');
                $notifyObj->save();

//                event(new NotificationEvent($notifyObj));
            }
            if($request->has('students'))
            foreach ($request->get('students') as $teacherId){
                $permitedUser = new AdvertTaskUser();
                $permitedUser->tenant_id = auth()->user()->tenant_id;
                $permitedUser->user_id = $teacherId;

                $advertTaskObj->permitedUsers()->save($permitedUser);

                $notifyObj = new Notification();
                $notifyObj->user_id = $teacherId;
                $notifyObj->content = strlen($request->get('content')) > 25 ? substr($request->get('content'),0,25)."..." : $request->get('content');
                $notifyObj->icon = "fa fa-calendar-plus-o";
                $notifyObj->color = "#69ca9d";
                $notifyObj->date = Carbon::now()->format('Y-m-d H:i:s');
                $notifyObj->route = route('portal_dashboard');
                $notifyObj->save();

//                event(new NotificationEvent($notifyObj));
            }

            return response()->json(['status' => 'ok']);

        }
    }

    public function deleteAction(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:advert_tasks,id,tenant_id,' . auth()->user()->tenant_id
        ]);

        if ($validator->fails()){
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();
            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else{
            $advertTaskObj = AdvertTask::realData()->find($request->get('id'));
            $destinationPath = public_path(Standarts::$optionFilesDir);
            @unlink($destinationPath."/".$advertTaskObj['file']);
            $advertTaskObj->delete();
            return response()->json(['status' => 'ok']);
        }
    }
}
