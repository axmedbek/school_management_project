<?php

namespace App\Http\Controllers;

use App\Models\Room;

class CameraController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cameras = Room::realData()
            ->join('equipment', 'equipment.room_id', 'rooms.id')
            ->where('equipment.type', 2)
            ->select('equipment.*', 'rooms.name', 'equipment.name as e_name')
            ->get();

        $data = [
            'cameras' => $cameras
        ];

        return view('camera.camera', $data);
    }

    public function getRooms(){
        $results = [];
        if (request('ne') == "get_all_rooms"){
            $cameras = Room::realData()
                ->join('equipment', 'equipment.room_id', 'rooms.id')
                ->where('rooms.name','like','%'.request('q').'%')
                ->where('equipment.type', 2)
                ->select('equipment.*', 'rooms.name', 'equipment.name as e_name')
                ->get();

            foreach ($cameras as $camera){
                $results[] = ['id' => $camera->ip , 'text' => $camera->name." ( ".$camera->e_name." )"];
            }
        }

        return response()->json(['results' => $results]);
    }
}
