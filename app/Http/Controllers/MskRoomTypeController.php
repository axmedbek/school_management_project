<?php

namespace App\Http\Controllers;

use App\Models\MskRoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MskRoomTypeController extends Controller
{
    public function index(Request $request){

        $data = [
            'room_types' => MskRoomType::realData()->orderByDesc('id')->get(),
            'request' => $request
        ];

        return view('msk.room_type.room_type',$data);
    }

    //

    public function AddEditAction(Request $request){
        $hasError = false;

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:30',
            'id' => 'required|integer',
        ]);

        $checkNameIsUnique = MskRoomType::realData()
            ->where('name',$request->get('name'))
            ->where('id','!=',$request->get('id'))
            ->whereNull('deleted_at')->first();

        if (isset($checkNameIsUnique)) $hasError = true;$validator->errors()->add('error','Bu ad ilə artıq otaq tipi daxil edilib');

        if ($hasError || $validator->fails()){
            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();
            return response()->json(['status'=>'error', 'errors' => $errors]);
        }
        else{

            if($request->get('id')==0){
                $mskRoomType = new MskRoomType();
            }
            else{
                $mskRoomType = MskRoomType::realData()->find($request->get('id'));
            }

            $mskRoomType->name = $request->get('name');
            $mskRoomType->tenant_id = Auth::user()->tenant_id;
            $mskRoomType->save();

            $checkRoomType = \App\Models\Room::realData()->where('type',$mskRoomType->id)->first();
            if(!isset($checkRoomType)){
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>
                            <a type="button" class="btn btn-danger col-lg-4 col-md-12 data_delete"><i class="fa fa-trash-o"></i></a>';
            }
            else{
                $buttons = '<a type="button" class="btn btn-primary col-lg-4 col-md-12 edit_data"><i class="fa fa-pencil"></i></a>';
            }
            $mskRoomType["buttons"] = $buttons;

            return response()->json(['status'=>'ok','data'=>$mskRoomType->toArray()]);

        }
    }

    public function DeleteAction(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer|exists:msk_room_types,id,tenant_id,' . Auth::user()->tenant_id
        ]);

        if ($validator->fails()){

            $errors = View::make('modals.modal_errors', ['errors' => $validator->errors() ])->render();

            return response()->json(['status'=>'error', 'errors' => $errors]);

        }
        else{

            $mskRoomType = MskRoomType::realData()->find($request->get('id'));
            $mskRoomType->delete();


            return response()->json(['status'=>'ok']);


        }
    }
}
