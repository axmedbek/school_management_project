<?php

namespace App\Http\Middleware;

use App\Library\Standarts;
use Closure;
use Illuminate\Support\Facades\Auth;

class PortalPrivilegia
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module)
    {
        if(Auth::guard('portal')->user())
        {
            $privs = Standarts::$portalModules[$module]['priv'];
            $usertype = Auth::guard('portal')->user()->user_type;
            if(in_array($usertype, $privs))
            {
                return $next($request);
            }
        }

        return response()->view("errors.403",[],403);
    }
}
