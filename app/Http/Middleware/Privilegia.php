<?php

namespace App\Http\Middleware;

use App\Library\Helper;
use App\Library\Standarts;
use Closure;
use Illuminate\Support\Facades\Auth;

class Privilegia
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module, $priv)
    {
        if (Helper::has_priv($module, $priv)) {
            return $next($request);
        } else {
            foreach (Standarts::$modules as $key => $item) {
                if (!isset($item['child'])) {
                    if (!Helper::has_priv($item['route'], $item['priv']))
                        continue;
                    return redirect()->route($item['route']);
                } else {
                    foreach ($item['child'] as $ch) {
                        if (!Helper::has_priv($ch['route'], $ch['priv']))
                            continue;
                        return redirect()->route($ch['route']);
                    }
                }
            }
            return response()->view("errors.403", [], 403);
        }
    }
}
