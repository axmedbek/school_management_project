<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check())
        {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|nullable',
            'birthday' => 'string|nullable|date_format:"d-m-Y"',
            'enroll_date' => 'string|nullable|date_format:"d-m-Y"',
            'home_tel' => 'string|nullable',
            'mobil_tel' => 'string|nullable',
            'email' => 'string|nullable',
        ];
    }
}
