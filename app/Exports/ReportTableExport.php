<?php
/**
 * Created by PhpStorm.
 * User: m.ehmed
 * Date: 5/5/2018
 * Time: 9:43 AM
 */

namespace App\Exports;


use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReportTableExport implements FromCollection
{

    /**
     * @return Collection
     */
    public function collection()
    {
        return User::select('id','name','email')->get();
    }

    public function title():string {
        return 'Ayliqqq';
    }
}