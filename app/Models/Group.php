<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    private $availableModules = false;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function getModulePriv($module)
    {
        if( !isset($this->getAvailableModules()[$module]) ) return 1;

       return $this->getAvailableModules()[$module];
    }

    public function getAvailableModules()
    {
        if( $this->available_modules == null ) return [];

        return $this->availableModules ? $this->availableModules : ($this->availableModules = json_decode($this->available_modules, true));
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
