<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Lecture extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("lectures.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function files()
    {
        return $this->hasMany(LectureFile::class);
    }

    public function seasonParagraph()
    {
        return $this->belongsTo(SeasonParagraph::class);
    }
    public function subject(){
        return $this->belongsTo(Subject::class);
    }
}
