<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SubstitutionLesson extends Model
{
    public $timestamps = false;

    public function lesson(){
        return $this->belongsTo(Lesson::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function substitution(){
        return $this->belongsTo(Substitution::class);
    }
}
