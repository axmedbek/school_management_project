<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public $timestamps =false;

    public static function realData()
    {
        $data = self::where("chats.tenant_id", auth()->user()->tenant_id);

        return $data;
    }

    public function senderUser(){
        return $this->belongsTo(User::class,'sender_id');
    }

    public function recivierUser(){
        return $this->belongsTo(User::class,'reciever_id');
    }
}
