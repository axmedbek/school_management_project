<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MskCities extends Model
{
    public $timestamps = false;

    public static function realData(){
        return self::where('tenant_id',Auth::user()->tenant_id);
    }
}
