<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class LetterGroup extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("letter_groups.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function group_type()
    {
        return $this->belongsTo(GroupType::class);
    }

    public function class_letter()
    {
        return $this->belongsTo(ClassLetter::class);
    }

    public function students()
    {
        return $this->belongsToMany(User::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
