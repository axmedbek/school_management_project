<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Subject extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("subjects.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function teachers()
    {
        return $this->belongsToMany(User::class);
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function mskClass(){
        return $this->belongsToMany(MskClass::class);
    }

}
