<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonLaborActivity extends Model
{
    public $timestamps = false;
}
