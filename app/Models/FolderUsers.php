<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FolderUsers extends Model
{
    public $timestamps = false;

    public static function realData(){
        return self::where('tenant_id',auth()->user()->tenant_id);
    }
}
