<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Equipment extends Model
{
    public static function realData(){
        $data = self::where('equipment.tenant_id',Auth::user()->tenant_id);
        return $data;
    }

    public function room(){
        return $this->belongsTo(Room::class);
    }
    public function corpus(){
        return $this->belongsTo(Corpus::class);
    }
}
