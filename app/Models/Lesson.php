<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Lesson extends Model
{
    public $timestamps = false;
    use SoftDeletes;

    public static function realData()
    {
        $data = self::where("lessons.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function class_time(){
        return $this->belongsTo(ClassTime::class);
    }

    public function letter_group(){
        return $this->belongsTo(LetterGroup::class);
    }

    public function lessonDays(){
        return $this->hasMany(LessonDay::class);
    }
}
