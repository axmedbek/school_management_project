<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentComment extends Model
{
    public $timestamps = false;
}
