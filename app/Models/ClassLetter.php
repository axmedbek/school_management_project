<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ClassLetter extends Model
{
    public $timestamps =false;

    public static function realData()
    {
        $data = self::where("class_letters.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function groups()
    {
        return $this->hasMany(LetterGroup::class);
    }

    public function year()
    {
        return $this->belongsTo(Year::class);
    }

    public function corpus()
    {
        return $this->belongsTo(Corpus::class);
    }

    public function msk_class()
    {
        return $this->belongsTo(MskClass::class);
    }
}
