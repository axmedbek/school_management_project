<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Year extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function periods()
    {
        return $this->hasMany(YearPeriod::class);
    }

    public function holidays()
    {
        return $this->hasMany(Holiday::class)->orderByDesc('id');
    }
}
