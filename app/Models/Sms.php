<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Sms extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("sms.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function teacher(){
        return $this->belongsTo(User::class,'teacher_id');
    }

    public function student(){
        return $this->belongsTo(User::class,'student_id');
    }

    public function subject(){
        return $this->belongsTo(Subject::class,'subject_id');
    }

    public function files()
    {
        return $this->hasMany(SmsFiles::class);
    }
}
