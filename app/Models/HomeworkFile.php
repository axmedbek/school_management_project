<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeworkFile extends Model
{
    public $timestamps = false;
}
