<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Homework extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("homeworks.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function files()
    {
        return $this->hasMany(HomeworkFile::class);
    }

    public function subject(){
        return $this->belongsTo(Subject::class);
    }
}
