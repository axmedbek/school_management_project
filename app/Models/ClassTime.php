<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ClassTime extends Model
{
    public $timestamps =false;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function corpus(){
        return $this->belongsTo(Corpus::class);
    }

}
