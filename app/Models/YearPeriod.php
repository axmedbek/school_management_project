<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YearPeriod extends Model
{
    public $timestamps = false;
    protected $dates = ['start_date', 'end_date'];
    protected $dateFormat = 'Y-m-d';
}
