<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Room extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("rooms.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function corpus(){
        return $this->belongsTo(Corpus::class);
    }

    public function room_type(){
        return $this->belongsTo(MskRoomType::class,'type');
    }
}
