<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SmsTemplates extends Model
{
    public $timestamps = false;

    public static function realData(){

        $data = self::where("tenant_id",Auth::user()->tenant_id);
        return $data;
    }
}
