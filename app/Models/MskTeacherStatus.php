<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class MskTeacherStatus extends Model
{

    use SoftDeletes;

    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }
}
