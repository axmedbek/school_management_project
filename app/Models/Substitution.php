<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Substitution extends Model
{
    public $timestamps = false;

    public static function realData(){
        $data = self::where("tenant_id",Auth::user()->tenant_id);

        return $data;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function substitutionLessons(){
        return $this->hasMany(SubstitutionLesson::class);
    }

}
