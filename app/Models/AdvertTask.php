<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AdvertTask extends Model
{
    public $timestamps = false;

    public static function realData(){
        $data = self::where("advert_tasks.tenant_id",auth()->user()->tenant_id);
        return $data;
    }

    public function user(){
        return $this->belongsTo(User::class,'adding_user');
    }

    public function permitedUsers(){
        return $this->hasMany(AdvertTaskUser::class);
    }
}
