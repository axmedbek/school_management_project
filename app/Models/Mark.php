<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Mark extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("marks.tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function mskMark()
    {
        return $this->belongsTo(MskMarks::class);
    }
}
