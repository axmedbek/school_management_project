<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertTaskUser extends Model
{
    public $timestamps = false;

    public static function realData(){
        $data = self::where("advert_task_users.tenant_id", auth()->user()->tenant_id);
        return $data;
    }
}
