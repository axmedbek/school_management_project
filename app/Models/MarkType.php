<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class MarkType extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function marks(){
        return $this->hasMany(MskMarks::class);
    }
}
