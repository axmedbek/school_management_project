<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    public $timestamps = false;

    public static function realData(){
        return self::where('folders.tenant_id',auth()->user()->tenant_id);
    }

    public function images(){
        return $this->hasMany(FolderImages::class);
    }

    public function permitedUsers(){
        return $this->hasMany(FolderUsers::class);
    }
}
