<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class MskClass extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    public static function realData($tenantId = false)
    {
        $data = self::where("msk_classes.tenant_id", ($tenantId ? $tenantId : Auth::user()->tenant_id) );

        return $data;
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function holidays(){
        return $this->belongsToMany(Holiday::class);
    }
}
