<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PersonFamily extends Model
{
    public $timestamps = false;

    public static function realData(){
        $data = self::where("person_families.tenant_id", auth()->user()->tenant_id);
        return $data;
    }

    public function msk_relation()
    {
        return $this->belongsTo(MskRelation::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
