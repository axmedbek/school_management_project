<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LectureFile extends Model
{
    public $timestamps = false;
}
