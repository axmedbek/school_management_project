<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Duty extends Model
{
    public $timestamps = false;
    use SoftDeletes;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }
}
