<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Season extends Model
{
    public $timestamps = false;

    public static function realData()
    {
        $data = self::where("tenant_id", Auth::user()->tenant_id);

        return $data;
    }

    public function group_types()
    {
        return $this->belongsToMany(GroupType::class);
    }

    public function paragraphs()
    {
        return $this->hasMany(SeasonParagraph::class);
    }
}
