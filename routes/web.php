<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('admin/logout', 'Auth\LoginController@logout')->name('admin_logout');

Route::group(['middleware' => ['guest:web'], 'prefix' => 'admin' ], function(){
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin_login');
    Route::post('/login', 'Auth\LoginController@login');
});

Route::group(['middleware' => ['auth:web'], 'prefix' => 'admin' ], function(){
    # modules
    //dashboard
    Route::get('/', 'HomeController@index')->name('dashboard')->middleware('priv:dashboard,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/duty/delete/{duty}', 'HomeController@dutyDelete')->where('duty','[0-9]{1,}')->name('duty_delete')->middleware('priv:dashboard,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //user
    Route::get('/structure/users', 'UserController@user')->name('str_users')->middleware('priv:str_users,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/structure/users/delete/{user}', 'UserController@userDelete')->where('user','[0-9]{1,}')->name('str_users_delete')->middleware('priv:str_users,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //staff
    Route::get('/structure/staffs', 'StaffController@staff')->name('str_staff')->middleware('priv:str_staff,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/structure/staffs/delete/{staff}', 'StaffController@staffDelete')->where('staff','[0-9]{1,}')->name('str_staffs_delete')->middleware('priv:str_staff,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //parents
    Route::get('/structure/parents', 'ParentsController@parent')->name('str_parents')->middleware('priv:str_parents,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/structure/parents/delete/{parent}', 'ParentsController@parent_Delete_Action')->where('parent','[0-9]{1,}')->name('str_parents_delete')->middleware('priv:str_parents,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //teachers
    Route::get('/structure/teachers', 'TeacherController@teacher')->name('str_teacher')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/structure/teachers/delete/{teacher}', 'TeacherController@teacherDelete')->where('teacher','[0-9]{1,}')->name('str_teachers_delete')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/teachers/teacherPriv','TeacherController@teacherPrivModal')->name('str_teachers_journal_editing_priv_modal')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/structure/teachers/teacherPriv','TeacherController@teacherPrivAction')->name('str_teachers_journal_editing_priv_action')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/teachers/getDataForSelect','TeacherController@getDataForSelect')->name('str_teachers_get_data')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //students
    Route::get('/structure/students','StudentsController@student')->name('str_students')->middleware('priv:str_students,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/structure/students/delete/{student}','StudentsController@student_Delete_Action')->name('str_students_delete')->middleware('priv:str_students,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/students/getDataForSelect','StudentsController@getDataForSelect')->name('str_students_get_data')->middleware('priv:str_students,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //reports
        //table
    Route::get('/reports/table','ReportTableController@index')->name('report_table')->middleware('priv:report_table,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/reports/getDataForSelect','ReportTableController@getDataForSelect')->name('reports_table_get_class')->middleware('priv:report_table,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //staff
    Route::get('/reports/staff','Reports\StaffController@index')->name('report_staff')->middleware('priv:report_staff,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //staff statistic
    Route::get('/reports/staff/statistic','Reports\StaffStatisticController@index')->name('report_staff_statistic')->middleware('priv:report_staff_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //teacher statistic
    Route::get('/reports/teacher/statistic','Reports\TeacherStatisticController@index')->name('report_teacher_statistic')->middleware('priv:report_teacher_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //student statistic
    Route::get('/reports/student/statistic','Reports\StudentStatisticController@index')->name('report_student_statistic')->middleware('priv:report_student_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //teacher
    Route::get('/reports/teacher','Reports\TeacherController@index')->name('report_teacher')->middleware('priv:report_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //student
    Route::get('/reports/student','Reports\StudentController@index')->name('report_student')->middleware('priv:report_student,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //tabel
    Route::get('/reports/tabel','Reports\TabelController@index')->name('report_tabel')->middleware('priv:report_tabel,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //davamiyyet tabel
    Route::get('/reports/attendance','Reports\AttendanceController@index')->name('report_attendance')->middleware('priv:report_attendance,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //davamiyyet tabel personel
    Route::get('/reports/attendance/personal','Reports\AttendancePersonalController@index')->name('report_attendance_personal')->middleware('priv:report_attendance_personal,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //jurnal
    Route::get('/reports/jurnal', 'Reports\JurnalController@index')->name('reports_jurnal')->middleware('priv:reports_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //Route::get('/jurnal/students/marks', 'Reports/JurnalController@studentsMarksModal')->name('admin_jurnal_student_marks_modal')->middleware('priv:portal_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //Route::get('/jurnal/lecture', 'Reports/JurnalController@lectureModal')->name('admin_jurnal_lecture_modal')->middleware('priv:admin_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //Route::get('/jurnal/homework', 'Reports/JurnalController@homeworkModal')->name('admin_jurnal_homework_modal')->middleware('priv:admin_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //Route::get('/jurnal/sms', 'Reports/JurnalController@smsModal')->name('admin_jurnal_sms_modal')->middleware('priv:admin_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);

    //topics
    Route::get('/topics','TopicController@index')->name('topics')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //substitution
    Route::get('/substitution','SubstitutionController@index')->name('substitution')->middleware('priv:substitution,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/substitution/getSubstitutionData', 'SubstitutionController@getSubstitutionData')->name('get_substitution_data')->middleware('priv:substitution,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/substitution/info/{sub}', 'SubstitutionController@infoModal')->where('sub','[0-9]{1,}')->name('substitution_info')->middleware('priv:substitution,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/substitution/delete/{sub}', 'SubstitutionController@Delete')->where('sub','[0-9]{1,}')->name('substitution_delete')->middleware('priv:substitution,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/substitution/addEdit/{sub}', 'SubstitutionController@addEditModal')->where('sub','[0-9]{1,}')->name('substitution_add_edit_modal')->middleware('priv:substitution,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/substitution/addEdit/{sub}','SubstitutionController@addEditAction')->where('sub','[0-9]{1,}')->name('substitution_add_edit_action')->middleware('priv:substitution,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //equipment
    Route::get('/msk/equipments','MskEquipmentController@index')->name('msk_equipments')->middleware('priv:msk_equipments,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/msk/equipments/addEdit/{eqp}','MskEquipmentController@addEditAction')->where('eqp','[0-9]{1,}')->name('msk_equipments_add_edit_action')->middleware('priv:msk_equipments,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/msk/equipments/delete/{eqp}','MskEquipmentController@deleteAction')->where('eqp','[0-9]{1,}')->name('msk_equipments_delete')->middleware('priv:msk_equipments,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/msk/equipments/addEdit/{eqp}','MskEquipmentController@addEditModal')->where('eqp','[0-9]{1,}')->name('msk_equipments_add_edit_modal')->middleware('priv:msk_equipments,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/msk/equipments/getEquipmentData','MskEquipmentController@getEquipmentData')->name('msk_equipments_get_data')->middleware('priv:msk_equipments,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/msk/equipments/refreshData/{eqp}','MskEquipmentController@refreshData')->name('msk_equipments_refresh_data')->middleware('priv:msk_equipments,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //corpus
    Route::get('/corpuses', 'CorpusController@corpus')->name('corpuses')->middleware('priv:corpuses,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/corpuses/delete/{corpus}', 'CorpusController@corpusDelete')->where('corpus','[0-9]{1,}')->name('corpuses_delete')->middleware('priv:corpuses,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk room
    Route::get('/msk/rooms', 'MskRoomController@index')->name('msk_rooms')->middleware('priv:msk_rooms,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk class
    Route::get('/msk/classes', 'MskClassController@index')->name('msk_classes')->middleware('priv:msk_classes,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk subject
    Route::get('/msk/subjects', 'MskSunjectController@index')->name('msk_subjects')->middleware('priv:msk_subjects,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk group_type
    Route::get('/msk/group_types', 'MskGroupTypeController@index')->name('msk_group_types')->middleware('priv:msk_group_types,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk mark types
    Route::get('/msk/mark_types','MskMarkTypeController@index')->name('msk_mark_types')->middleware('priv:msk_mark_types,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk relations
    Route::get('/msk/msk_relations','MskRelationsController@index')->name('msk_relations')->middleware('priv:msk_relations,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk_teacher_status
    Route::get('/msk/msk_teacher_statuses','MskTeacherStatusController@index')->name('msk_teacher_status')->middleware('priv:msk_teacher_status,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk_equipments_types
    Route::get('/msk/msk_equipments_types','MskEquipmenTypeController@index')->name('msk_equipments_types')->middleware('priv:msk_equipments_types,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/msk/msk_equipments_types/add_edit','MskEquipmenTypeController@addEditAction')->name('msk_equipments_types_add_edit_action')->middleware('priv:msk_equipments_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/msk/msk_equipments_types/delete','MskEquipmenTypeController@deleteAction')->name('msk_equipments_types_delete')->middleware('priv:msk_equipments_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk_cities_and_regions
    Route::get('/msk/cities_and_regions','MskCityAndRegionController@index')->name('msk_cities_and_regions')->middleware('priv:msk_cities_and_regions,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/msk/cities_and_regions/city_add_edit','MskCityAndRegionController@cityAddEditAction')->name('msk_cities_and_regions_add_edit_action')->middleware('priv:msk_cities_and_regions,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/msk/cities_and_regions/city_delete','MskCityAndRegionController@cityDeleteAction')->name('msk_cities_and_regions_delete')->middleware('priv:msk_cities_and_regions,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/msk/cities_and_regions/get_regions','MskCityAndRegionController@getRegions')->name('msk_cities_and_regions_get_regions')->middleware('priv:msk_cities_and_regions,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/msk/cities_and_regions/region_add_edit','MskCityAndRegionController@regionAddEditAction')->name('msk_cities_and_regions_region_add_edit_action')->middleware('priv:msk_cities_and_regions,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/msk/cities_and_regions/region_delete','MskCityAndRegionController@regionDeleteAction')->name('msk_cities_and_regions_delete_region')->middleware('priv:msk_cities_and_regions,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk group
    Route::get('/msk/group', 'MskGroupController@index')->name('msk_group')->middleware('priv:msk_group,'.\App\Library\Standarts::PRIV_CAN_SEE);

    //msk_translations
    Route::get('/msk/translations', 'MskTranslationController@index')->name('msk_translations')->middleware('priv:msk_translations,'.\App\Library\Standarts::PRIV_CAN_SEE);


    //years
    Route::get('/years', 'YearController@index')->name('years')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/years/wizard', 'YearController@wizard')->name('years_wizard')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //tables
    Route::get('/tables','TableController@index')->name('tables')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //class_groups
    Route::get('/class_groups','ClassGroupController@index')->name('class_groups')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //class_times
    Route::any('/msk/class_times','ClassTimesController@index')->name('msk_class_times')->middleware('priv:msk_class_times,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk_holidays
    Route::get('/msk/holidays','HolidaysController@index')->name('msk_holidays')->middleware('priv:msk_holidays,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //finger
    Route::get('/finger', 'FingerController@index')->name('finger')->middleware('priv:finger,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //sms_templates
    Route::get('/sms_templates','SmsTemplatesController@index')->name('sms_templates')->middleware('priv:sms_templates,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/sms_templates/temp/{smsTemplateId}','SmsTemplatesController@smsTemplateEditModal')->name('sms_templates_edit_modal')->middleware('priv:sms_templates,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/sms_templates/temp/{smsTemplateId}','SmsTemplatesController@smsTemplateEditAction')->name('sms_templates_edit_action')->middleware('priv:sms_templates,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/sms_templates/doDeActiveSmsTemplate','SmsTemplatesController@doSmsTemplateDeActive')->name('sms_template_do_deactive')->middleware('priv:sms_templates,'.\App\Library\Standarts::PRIV_CAN_EDIT);


    # modals
    //user
    Route::get('/structure/users/addEdit/{user}', 'UserController@userAddEditModal')->where('user','[0-9]{1,}')->name('str_users_add_edit')->middleware('priv:str_users,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/users/info/{user}', 'UserController@infoModal')->where('user','[0-9]{1,}')->name('str_users_info')->middleware('priv:str_users,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //staff
    Route::get('/structure/staffs/addEdit/{staff}', 'StaffController@staffAddEditModal')->where('staff','[0-9]{1,}')->name('str_staffs_add_edit')->middleware('priv:str_staff,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/staffs/info/{staff}', 'StaffController@infoModal')->where('staff','[0-9]{1,}')->name('str_staffs_info')->middleware('priv:str_staff,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //parents
    Route::get('/structure/parents/addEdit/{parent}', 'ParentsController@parent_AddEdit_Modal')->where('parent','[0-9]{1,}')->name('str_parents_add_edit')->middleware('priv:str_parents,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/parents/info/{parent}', 'ParentsController@parent_info_modal_Action')->where('parent','[0-9]{1,}')->name('str_parents_info')->middleware('priv:str_parents,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //students
    Route::get('/structure/students/addEdit/{student}', 'StudentsController@students_AddEdit_Modal')->where('student','[0-9]{1,}')->name('str_students_add_edit')->middleware('priv:str_students,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/students/info/{student}', 'StudentsController@student_info_Modal')->where('student','[0-9]{1,}')->name('str_students_info')->middleware('priv:str_students,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //teacher
    Route::get('/structure/teachers/addEdit/{teacher}', 'TeacherController@teacherAddEditModal')->where('teacher','[0-9]{1,}')->name('str_teachers_add_edit')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/structure/teachers/info/{teacher}', 'TeacherController@infoModal')->where('teacher','[0-9]{1,}')->name('str_teachers_info')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //corpus
    Route::get('/corpuses/addEdit/{corpus}', 'CorpusController@corpusAddEditModal')->where('corpus','[0-9]{1,}')->name('corpuses_add_edit')->middleware('priv:corpuses,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/corpuses/info/{corpus}', 'CorpusController@infoModal')->where('corpus','[0-9]{1,}')->name('corpuses_info')->middleware('priv:corpuses,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk group
    Route::get('/msk/groups/addEdit/{group}', 'MskGroupController@groupAddEditModal')->where('group','[0-9]{1,}')->name('msk_groups_add_edit')->middleware('priv:msk_group,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //topics
    Route::get('/topics/season/addEditModal/{season}', 'TopicController@seasonAddEditModal')->where('season','[0-9]{1,}')->name('topic_season_add_edit')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //years
    Route::get('/years/addEdit/{year}', 'YearController@yearAddEditModal')->where('year','[0-9]{1,}')->name('years_add_edit')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //class_groups
    Route::get('/class_groups/letter/addEditModal/{letter}', 'ClassGroupController@letterAddEditModal')->where('letter','[0-9]{1,}')->name('class_group_letter_add_edit')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //tables
    Route::get('/tables/student/addModal', 'TableController@studentAddModal')->name('table_student_add')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/tables/classEditModal', 'TableController@classEditModal')->name('table_class_edit')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/tables/table_copy','TableController@tableCopyModalAction')->name('table_copy_modal')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);

    # pages
    //topics
    Route::post('/topics/topics_page','TopicController@topicPage')->name('topics_page')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/topics/topics_search_paragraph','TopicController@getParagraphs')->name('topics_get_pragraph')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_SEE);

    //class_groups
    Route::post('/class_groups/class_groups_page','ClassGroupController@classGroupsPage')->name('class_groups_page')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //tables
    Route::post('/tables/table_page','TableController@tablePage')->name('table_page')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //report
        // table
    Route::post('/report/tables/table_page','ReportTableController@tablePage')->name('report_table_page')->middleware('priv:report_table,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/tables/exportToExcel','ReportTableController@exportToExcel')->name('all_data_export_to_excel')->middleware('priv:report_table,'.\App\Library\Standarts::PRIV_CAN_SEE);
        // staff
    Route::post('/report/staff/staff_page','Reports\StaffController@staffPage')->name('report_staff_page')->middleware('priv:report_staff,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/staff/export/{type}','Reports\StaffController@exportToExcel')->name('report_staff_export')->middleware('priv:report_staff,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //staff statistic
    Route::post('/report/staff/statistic/page','Reports\StaffStatisticController@staffPage')->name('report_staff_statistic_page')->middleware('priv:report_staff_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/staff/statistic/export/{type}','Reports\StaffStatisticController@exportAction')->name('report_staff_statistic_export')->middleware('priv:report_staff_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //jurnal
    Route::post('/reports/jurnal/students/page', 'Reports\JurnalController@studentsPage')->name('report_jurnal_students_page')->middleware('priv:reports_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //teacher statistic
    Route::post('/report/teacher/statistic/page','Reports\TeacherStatisticController@teacherPage')->name('report_teacher_statistic_page')->middleware('priv:report_teacher_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/teacher/statistic/export/{type}','Reports\TeacherStatisticController@exportAction')->name('report_teacher_statistic_export')->middleware('priv:report_teacher_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //student statistic
    Route::post('/report/student/statistic/page','Reports\StudentStatisticController@studentPage')->name('report_student_statistic_page')->middleware('priv:report_student_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/student/statistic/export/{type}','Reports\StudentStatisticController@exportAction')->name('report_student_statistic_export')->middleware('priv:report_student_statistic,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //teacher
    Route::post('/report/teacher/teacher_page','Reports\TeacherController@teacherPage')->name('report_teacher_page')->middleware('priv:report_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/teacher/export/{type}','Reports\TeacherController@exportToExcel')->name('report_teacher_export')->middleware('priv:report_teacher,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //student
    Route::post('/report/student/student_page','Reports\StudentController@studentPage')->name('report_student_page')->middleware('priv:report_student,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/student/export/{type}','Reports\StudentController@exportToExcel')->name('report_student_export')->middleware('priv:report_student,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //report tabel
    Route::post('/report/tabel/page','Reports\TabelController@tabelPage')->name('report_tabel_page')->middleware('priv:report_tabel,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/report/tabel/export/{type}','Reports\TabelController@tabelExport')->name('report_tabel_export')->middleware('priv:report_tabel,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //report davamiyyet
    Route::post('/report/attendance/page','Reports\AttendanceController@attendancePage')->name('report_attendance_page')->middleware('priv:report_attendance,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/report/attendance/export/{type}','Reports\AttendanceController@attendanceExport')->name('report_attendance_export')->middleware('priv:report_attendance,'.\App\Library\Standarts::PRIV_CAN_SEE);
        //report davamiyyet personel
    Route::post('/reports/attendance/personal/page','Reports\AttendancePersonalController@attendancePage')->name('report_attendance_personal_page')->middleware('priv:report_attendance_personal,'.\App\Library\Standarts::PRIV_CAN_SEE);

    # ajax
    //user
    Route::post('/structure/users/addEdit/{user}', 'UserController@userAddEditAction')->where('user','[0-9]{1,}')->name('str_users_add_edit_action')->middleware('priv:str_users,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //staff
    Route::post('/structure/staffs/addEdit/{staff}', 'StaffController@staffAddEditAction')->where('staff','[0-9]{1,}')->name('str_staffs_add_edit_action')->middleware('priv:str_staff,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //parents
    Route::post('/structure/parents/addEdit/{parent}','ParentsController@parent_Add_Edit_Action')->where('parent','[0-9]{1,}')->name('str_parents_add_edit_action')->middleware('priv:str_parents,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //student
    Route::post('/structure/students/addEdit/{student}', 'StudentsController@student_AddEdit_Action')->where('student','[0-9]{1,}')->name('str_students_add_edit_action')->middleware('priv:str_students,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //teacher
    Route::post('/structure/teachers/addEdit/{teacher}', 'TeacherController@teacherAddEditAction')->where('teacher','[0-9]{1,}')->name('str_teachers_add_edit_action')->middleware('priv:str_teacher,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //corpus
    Route::post('corpuses/addEdit/{corpus}', 'CorpusController@corpusAddEditAction')->where('corpus','[0-9]{1,}')->name('corpuses_add_edit_action')->middleware('priv:corpuses,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //topics
    Route::get('topics/subjects', 'TopicController@classSubjects')->name('get_class_subject')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('topics/season/addEdit/{season}', 'TopicController@seasonAddEditAction')->name('topic_season_add_edit_action')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('topics/season/delete', 'TopicController@seasonDelete')->name('topic_season_delete')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('topics/paragraph/addEdit', 'TopicController@paragraphAddEditAction')->name('topic_paragraph_add_edit_action')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('topics/paragraph/delete', 'TopicController@paragraphDelete')->name('topic_paragraph_delete')->middleware('priv:topics,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk room
    Route::post('msk/rooms/addEdit', 'MskRoomController@roomAddEditAction')->name('msk_rooms_add_edit_action')->middleware('priv:msk_rooms,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/rooms/delete', 'MskRoomController@roomDelete')->name('msk_rooms_delete')->middleware('priv:msk_rooms,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('msk/rooms/getData', 'MskRoomController@roomGetDataForSelect')->name('msk_rooms_get_data')->middleware('priv:msk_rooms,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //msk class
    Route::post('msk/classes/addEdit', 'MskClassController@classAddEditAction')->name('msk_classes_add_edit_action')->middleware('priv:msk_classes,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/classes/delete', 'MskClassController@classDelete')->name('msk_classes_delete')->middleware('priv:msk_classes,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/classes/order', 'MskClassController@classOrder')->name('msk_classes_order')->middleware('priv:msk_classes,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk subject
    Route::post('msk/subjects/addEdit', 'MskSunjectController@subjectAddEditAction')->name('msk_subjects_add_edit_action')->middleware('priv:msk_subjects,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/subjects/delete', 'MskSunjectController@subjectDelete')->name('msk_subjects_delete')->middleware('priv:msk_subjects,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk group_type
    Route::post('msk/group_types/addEdit', 'MskGroupTypeController@group_typeAddEditAction')->name('msk_group_types_add_edit_action')->middleware('priv:msk_group_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/group_types/delete', 'MskGroupTypeController@group_typeDelete')->name('msk_group_types_delete')->middleware('priv:msk_group_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk mark types
    Route::post('msk/mark_types/addEdit','MskMarkTypeController@mark_type_AddEdit_Action')->name('msk_mark_types_add_edit_action')->middleware('priv:msk_mark_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/mark_types/delete','MskMarkTypeController@mark_type_Delete_Action')->name('msk_mark_types_delete')->middleware('priv:msk_mark_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk marks
    Route::post('msk/marks/addEdit','MskMarkTypeController@marks_AddEdit_Action')->name('msk_marks_add_edit_action')->middleware('priv:msk_mark_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/marks/delete','MskMarkTypeController@marks_Delete_Action')->name('msk_marks_delete')->middleware('priv:msk_mark_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/marks/get_marks','MskMarkTypeController@getMarks')->name('msk_mark_types_get_marks')->middleware('priv:msk_mark_types,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk teacher statuses
    Route::post('msk/msk_teacher_statuses/addEdit','MskTeacherStatusController@msk_teacher_statuses_AddEdit_Action')->name('msk_teacher_statuses_add_edit_action')->middleware('priv:msk_teacher_status,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/msk_teacher_statuses/delete','MskTeacherStatusController@msk_teacher_statuses_Delete_Action')->name('msk_teacher_statuses_delete')->middleware('priv:msk_teacher_status,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk relations
    Route::post('msk/msk_relations/addEdit','MskRelationsController@relations_AddEdit_Action')->name('msk_realtions_add_edit_action')->middleware('priv:msk_relations,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/msk_relations/delete','MskRelationsController@relations_Delete_Action')->name('msk_relations_delete')->middleware('priv:msk_relations,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk study_levels
    Route::get('/msk/msk_study_levels','MskStudyLevelController@index')->name('msk_study_levels')->middleware('priv:msk_study_levels,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('msk/msk_study_levels/addEdit','MskStudyLevelController@AddEdit_Action')->name('msk_study_levels_add_edit_action')->middleware('priv:msk_study_levels,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/msk_study_levels/delete','MskStudyLevelController@Delete_Action')->name('msk_study_levels_delete')->middleware('priv:msk_study_levels,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk room_types
    Route::get('/msk/msk_room_type','MskRoomTypeController@index')->name('msk_room_type')->middleware('priv:msk_room_type,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('msk/msk_room_type/addEdit','MskRoomTypeController@AddEditAction')->name('msk_room_type_add_edit_action')->middleware('priv:msk_room_type,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/msk_room_type/delete','MskRoomTypeController@DeleteAction')->name('msk_room_type_delete')->middleware('priv:msk_room_type,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk heyetler
    Route::get('/msk/msk_heyetler','MskHeyetlerController@index')->name('msk_heyetler')->middleware('priv:msk_heyetler,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('msk/msk_heyetler/addEdit','MskHeyetlerController@AddEditAction')->name('msk_heyetler_add_edit_action')->middleware('priv:msk_heyetler,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/msk_heyetler/delete','MskHeyetlerController@DeleteAction')->name('msk_heyetler_delete')->middleware('priv:msk_heyetler,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk group
    Route::post('msk/groups/delete', 'MskGroupController@groupDelete')->name('msk_groups_delete')->middleware('priv:msk_group,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/msk/groups/addEdit/{group}', 'MskGroupController@groupAddEditAction')->where('group','[0-9]{1,}')->name('msk_groups_add_edit_action')->middleware('priv:msk_group,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //year
    Route::post('years/addEdit/{year}', 'YearController@yearAddEditAction')->where('year','[0-9]{1,}')->name('years_add_edit_action')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('years/wizard/add', 'YearController@wizardAddAction')->name('wizard_years_add_action')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('years/wizard/save', 'YearController@wizardSaveAction')->name('wizard_years_save_action')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/years/delete/{year}', 'YearController@yearDelete')->where('year','[0-9]{1,}')->name('years_delete')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/years/dodeactive','YearController@doYearDeActive')->name('years_do_deactive')->middleware('priv:years,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //class_groups
    Route::post('class_groups/letter/addEdit/{letter}', 'ClassGroupController@letterAddEditAction')->name('class_group_letter_add_edit_action')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('class_groups/letter/delete', 'ClassGroupController@letterDelete')->name('class_group_letter_delete')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('class_groups/letter/group/addEdit', 'ClassGroupController@grouphAddEditAction')->name('class_group_letter_group_add_edit_action')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('class_groups/letterGroup/delete', 'ClassGroupController@letterGroupDelete')->name('group_letter_delete')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('class_groups/letterGroup/isMainGroup', 'ClassGroupController@doMainGroup')->name('class_group_letter_is_main_group')->middleware('priv:class_groups,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //class_times
    Route::post('msk/class_times/addEdit', 'ClassTimesController@AddEditAction')->name('msk_class_times_add_edit_action')->middleware('priv:msk_class_times,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/class_times/delete', 'ClassTimesController@Delete')->name('msk_class_times_delete')->middleware('priv:msk_class_times,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //msk_holidays
    Route::post('msk/holidays/addEdit', 'HolidaysController@AddEditAction')->name('msk_holidays_add_edit_action')->middleware('priv:msk_holidays,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('msk/holidays/delete', 'HolidaysController@Delete')->name('msk_holidays_delete')->middleware('priv:msk_holidays,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //tables
    Route::get('tables/class/letters', 'TableController@getClassLetters')->name('get_class_letters')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('tables/class/letter/groups', 'TableController@getLettersGroup')->name('get_letters_group')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('tables/class/letter/students', 'TableController@getStudents')->name('get_table_students')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('tables/save/data/{group}', 'TableController@tableSaveData')->name('table_save_data')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('tables/group/subjects', 'TableController@getGroupSubjects')->name('get_table_group_subjects')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('tables/group/teachers', 'TableController@getGroupTeachers')->name('get_table_group_teachers')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('tables/group/rooms', 'TableController@getGroupRooms')->name('get_table_group_rooms')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('tables/group/create/table', 'TableController@createTable')->name('create_table')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('tables/group/copy/table', 'TableController@copyAction')->name('copy_table_action')->middleware('priv:tables,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //finger
    Route::post('finger/user/info', 'FingerController@getUserInfo')->name('finger_user_info')->middleware('priv:finger,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //search for select
    Route::get('/search_for_select','Search\SearchForSelectController@index')->name('search_for_select');
    //advert&task module routes
    Route::get('/advert_task','AdvertTaskController@index')->name('advert_task')->middleware('priv:advert_task,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/advert_task/save/{task_id}','AdvertTaskController@saveAction')->name('advert_task.save')->middleware('priv:advert_task,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/advert_task/delete','AdvertTaskController@deleteAction')->name('advert_task.delete')->middleware('priv:advert_task,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/advert_task/task/{task_id}','AdvertTaskController@addAdvertTaskModal')->name('advert_task.add.modal')->middleware('priv:advert_task,'.\App\Library\Standarts::PRIV_CAN_SEE);
    //galery module routes
    Route::get('/galery','GaleryController@index')->name('galery')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/galery/folder/{folder_id}','GaleryController@addFolderModal')->name('folder.add.modal')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/galery/save/{folder_id}','GaleryController@saveFolderAction')->name('folder.save')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/galery/images','GaleryController@loadImagesPage')->name('galery.load.images')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/galery/images/save','GaleryController@saveImages')->name('galery.save.images')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/galery/images/delete','GaleryController@deleteImages')->name('galery.delete.images')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::post('/galery/delete','GaleryController@deleteFolder')->name('galery.delete.folder')->middleware('priv:galery,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    //camera
    Route::get('/cameras', 'CameraController@index')->name('cameras')->middleware('priv:cameras,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/camera/get_all_rooms', 'CameraController@getRooms')->name('camera_get_rooms')->middleware('priv:cameras,'.\App\Library\Standarts::PRIV_CAN_EDIT);

    //report
        //jurnal
    Route::get('/reports/jurnal/year/classes','Reports\JurnalController@yearClasses')->name('report_jurnal_year_classes')->middleware('priv:reports_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/reports/jurnal/class/subjects','Reports\JurnalController@classSubjects')->name('report_jurnal_class_subjects')->middleware('priv:reports_jurnal,'.\App\Library\Standarts::PRIV_CAN_SEE);

        //final_evaluation
    Route::get('/reports/final_evaluation','Reports\FinalEvaluationController@index')->name('reports_final_evaluation_table')->middleware('priv:reports_final_evaluation_table,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/reports/final_evaluation_page','Reports\FinalEvaluationController@getTable')->name('reports_final_evaluation_table_page')->middleware('priv:reports_final_evaluation_table,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/report/final_evaluation/export/{type}','Reports\FinalEvaluationController@exportAction')->name('report_final_evaluation_export')->middleware('priv:reports_final_evaluation_table,'.\App\Library\Standarts::PRIV_CAN_SEE);


    //sms_report
    Route::get('/report/sms_report','Reports\SmsReportController@index')->name('reports_sms_table')->middleware('priv:reports_sms_table,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/report/sms_report_page','Reports\SmsReportController@getTable')->name('reports_sms_table_page')->middleware('priv:reports_sms_table,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/report/sms_report/export/{type}','Reports\SmsReportController@tabelExport')->name('reports_sms_table_export')->middleware('priv:reports_sms_table,'.\App\Library\Standarts::PRIV_CAN_SEE);


    // jurnal not writed report
    Route::get('/report/jurnal_not_writed','Reports\JurnalNotWritedController@index')->name('reports_jurnal_not_writed')->middleware('priv:reports_jurnal_not_writed,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/reports/jurnal_not_writed/year/classes','Reports\JurnalNotWritedController@yearClasses')->name('reports_jurnal_not_writed_year_classes')->middleware('priv:reports_jurnal_not_writed,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::get('/reports/jurnal_not_writed/class/subjects','Reports\JurnalNotWritedController@classSubjects')->name('reports_jurnal_not_writed_class_subjects')->middleware('priv:reports_jurnal_not_writed,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/report/jurnal_not_writed_page_load','Reports\JurnalNotWritedController@getTable')->name('reports_jurnal_not_writed_table_page')->middleware('priv:reports_jurnal_not_writed,'.\App\Library\Standarts::PRIV_CAN_EDIT);
    Route::get('/report/jurnal_not_writed/export/{type}','Reports\JurnalNotWritedController@tabelExport')->name('reports_jurnal_not_writed_table_export')->middleware('priv:reports_jurnal_not_writed,'.\App\Library\Standarts::PRIV_CAN_SEE);

    // admin chat
    Route::get('/chat','AdminChatController@index')->name('admin_chat')->middleware('priv:admin_chat,'.\App\Library\Standarts::PRIV_CAN_SEE);
    Route::post('/chat/get_all_messages','AdminChatController@getMessages')->name('admin_chat_messages')->middleware('priv:admin_chat,'.\App\Library\Standarts::PRIV_CAN_SEE);
});

Route::get('/test', 'HomeController@test');
Route::post('/chat/get_my_info','HomeController@getMyInfo')->name('chat_user_info');
Route::post('/chat/save_message','HomeController@saveMessage')->name('chat.save.message');


include 'portal.php';