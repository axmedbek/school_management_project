<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::get('change/lang/{locale}', 'Portal\HomeController@changeLang')->where('locale', '(az)|(en)|(ru)')->name('portal_change_lang');

Route::group(['middleware' => ['lang']], function () {

    Route::get('login', 'Auth\PortalLoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\PortalLoginController@login');
    Route::get('logout', 'Auth\PortalLoginController@logout')->name('logout');
    Route::group(['middleware' => ['auth:portal']], function () {

        # modules
        //dashboard
        Route::get('/', 'Portal\HomeController@index')->name('portal_dashboard')->middleware('portal_priv:portal_dashboard');
        //jurnal
        Route::get('/jurnal', 'Portal\JurnalController@index')->name('portal_jurnal')->middleware('portal_priv:portal_jurnal');
        //topics
        Route::get('/topics', 'Portal\TopicController@index')->name('portal_topics')->middleware('portal_priv:portal_topics');

        //change student
        Route::get('/change/student/{id}', 'Portal\HomeController@changeStudent')->name('portal_change_student')->middleware('portal_priv:portal_change_student');

        // galery
        Route::get('/galery', 'Portal\GaleryController@index')->name('portal_galery')->middleware('portal_priv:portal_galery');
        Route::post('/galery/folder_list', 'Portal\GaleryController@getFolderList')->name('portal_galery_folder_list')->middleware('portal_priv:portal_galery');
        Route::get('/galery/image169', 'Portal\GaleryController@image169')->name('portal_galery_image_169')->middleware('portal_priv:portal_galery');

        //lesson materials
        Route::get('/lesson_materials', 'Portal\LessonMaterialController@index')->name('portal_lesson_materials')->middleware('portal_priv:portal_lesson_materials');

        //portal_teacher_request
        Route::get('/teacher_request','Portal\TeacherRequestForParent@index')->name('portal_teacher_request')->middleware('portal_priv:portal_teacher_request');
        Route::get('/teacher_request/info','Portal\TeacherRequestForParent@infoModal')->name('portal_teacher_request_info')->middleware('portal_priv:portal_teacher_request');


        //video_camera
        Route::get('/video_camera','Portal\CameraController@index')->name('portal_camera')->middleware('portal_priv:portal_camera');


        //reports
        Route::group(['prefix' => 'report'], function () {
            //portal_final_evaluation
            Route::get('/portal-final-evaluation','Portal\FinalEvaluationController@index')->name('portal_final_evaluation')->middleware('portal_priv:portal_final_evaluation');
            Route::post('/portal-final-evaluation-table','Portal\FinalEvaluationController@getTable')->name('portal_final_evaluation_get_table')->middleware('portal_priv:portal_final_evaluation');

        });

        //attendance
        Route::get('/student_attendance', 'Portal\AttendanceController@index')->name('portal_attendance')->middleware('portal_priv:portal_attendance');
        Route::post('/page/student_attendance_get_table', 'Portal\AttendanceController@getTable')->name('portal_attendance_get_table.process')->middleware('portal_priv:portal_attendance');
        Route::get('/page/student_attendance_get_table', 'Portal\AttendanceController@getTable')->name('portal_attendance_get_table')->middleware('portal_priv:portal_attendance');

        //profile
        Route::get('/profile/show', 'Portal\ProfileController@index')->name('portal_profile')->middleware('portal_priv:portal_profile');
        Route::get('/profile/change_password', 'Portal\ProfileController@changePasswordModal')->name('portal_profile_change_password')->middleware('portal_priv:portal_profile');
        Route::post('/profile/change_password_process', 'Portal\ProfileController@changePasswordProcess')->name('portal_profile_change_password_process')->middleware('portal_priv:portal_profile');
        Route::post('/profile/sms_number_add_edit', 'Portal\ProfileController@smsNumberAddEditAction')->name('portal_profile_sms_number_add_edit')->middleware('portal_priv:portal_profile');
        Route::post('/profile/sms_number_delete', 'Portal\ProfileController@smsNumberDeleteAction')->name('portal_profile_sms_number_delete')->middleware('portal_priv:portal_profile');

        # ajax
        //jurnal
        Route::get('/ajax/season/paragraphs', 'Portal\AjaxController@seasonParagraphs')->name('ajax_season_paragraphs')->middleware('portal_priv:portal_jurnal');
        Route::get('/ajax/seasons', 'Portal\AjaxController@seasons')->name('ajax_seasons')->middleware('portal_priv:portal_jurnal');
        Route::get('/ajax/subjects', 'Portal\AjaxController@subjects')->name('ajax_subjects')->middleware('portal_priv:portal_jurnal');
        Route::get('/ajax/subject/classes', 'Portal\AjaxController@subjectClasses')->name('ajax_subject_classes')->middleware('portal_priv:portal_jurnal');
        Route::post('/ajax/jurnal/save/marks', 'Portal\JurnalController@saveMarkAction')->name('ajax_save_mark_action')->middleware('portal_priv:portal_jurnal');
        Route::post('/ajax/jurnal/save/lecture', 'Portal\JurnalController@saveLectureAction')->name('ajax_save_lecture_action')->middleware('portal_priv:portal_jurnal');
        Route::post('/ajax/jurnal/save/homework', 'Portal\JurnalController@saveHomeworkAction')->name('ajax_save_homework_action')->middleware('portal_priv:portal_jurnal');
        Route::post('/ajax/jurnal/save/sms', 'Portal\JurnalController@saveSmsAction')->name('ajax_save_sms_action')->middleware('portal_priv:portal_jurnal');

        # pages
        //jurnal
        Route::post('/jurnal/students/page', 'Portal\JurnalController@studentsPage')->name('portal_jurnal_students_page')->middleware('portal_priv:portal_jurnal');
        //galery
        Route::post('/galery/folder', 'Portal\GaleryController@getFolder')->name('portal_galery.get.folder')->middleware('portal_priv:portal_galery');

        //notification seen
        Route::post('/notification/update', 'Portal\HomeController@updateNotifyForSeen')->name('notify_seen')->middleware('portal_priv:portal_dashboard');
        # modal
        //jurnal
        Route::get('/jurnal/students/marks', 'Portal\JurnalController@studentsMarksModal')->name('jurnal_student_marks_modal')->middleware('portal_priv:portal_jurnal');
        Route::get('/jurnal/lecture', 'Portal\JurnalController@lectureModal')->name('jurnal_lecture_modal')->middleware('portal_priv:portal_jurnal');
        Route::get('/jurnal/homework', 'Portal\JurnalController@homeworkModal')->name('jurnal_homework_modal')->middleware('portal_priv:portal_jurnal');
        Route::get('/jurnal/sms', 'Portal\JurnalController@smsModal')->name('jurnal_sms_modal')->middleware('portal_priv:portal_jurnal');
        Route::post('/jurnal/lecture/delete','Portal\JurnalController@deleteLectureAction')->name('jurnal_lecture_delete')->middleware('portal_priv:portal_jurnal');

        //dashboard
        Route::get('/dashboard/table', 'Portal\HomeController@tableModal')->name('dashboard_table_modal')->middleware('portal_priv:portal_dashboard');
        Route::get('/dashboard/tasks', 'Portal\HomeController@taskModal')->name('dashboard_task_modal')->middleware('portal_priv:portal_dashboard');
        Route::get('/dashboard/task/filtered_tasks', 'Portal\HomeController@filterTask')->name('dashboard_task_filter')->middleware('portal_priv:portal_dashboard');
        Route::get('/dashboard/get_all_adding_user', 'Portal\HomeController@getAddingUsers')->name('dashboard.task.all.users')->middleware('portal_priv:portal_dashboard');
        Route::get('/dashboard/modal/in_out_log', 'Portal\HomeController@inOutLogModal')->name('dashboard_in_out_log_modal')->middleware('portal_priv:portal_dashboard');

        //portal chat routes
        Route::get('/chat','Portal\ChatController@index')->name('portal_chat')->middleware('portal_priv:portal_chat');
        Route::post('/chat_messages','Portal\ChatController@getMessages')->name('chat.message')->middleware('portal_priv:portal_chat');
        Route::get('/chat/getUserInfo','Portal\ChatController@getUserInfo')->name('chat.user')->middleware('portal_priv:portal_chat');
        Route::post('/chat/make_offline_user','Portal\ChatController@makeOfflineUser')->name('chat.user.offline')->middleware('portal_priv:portal_chat');
        Route::get('/chat/getGroupUsers','Portal\ChatController@getGroupUsers')->name('portal_chat_group_user_info')->middleware('portal_priv:portal_chat');

    });

});
